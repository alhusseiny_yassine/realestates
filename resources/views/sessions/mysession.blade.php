<?php
/***********************************************************
mysession.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Aug 18 , 2017
Developed By  : alhusseiny   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

?>

@extends('layouts.layout')
rq_request_status
@section('themes')
   <link href="{{ url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css" />
   <style>
       th{
	      cursor: pointer;
       }
       .mt-step-col{
	       cursor: pointer;
       }
       #ModelPopUp{
	      top:50%;
       	  left:20%;
       	  position: absolute;
       }

   </style>
    <link href="{{ url('admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ url('admin/assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ url('admin/assets/global/plugins/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet" type="text/css" />
   <link href="{{ url('admin/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{ url('js/dhtmlxScheduler/codebase/dhtmlxscheduler.css') }}" type="text/css" media="screen" title="no title" charset="utf-8">
@endsection

@section('plugins')
<script src="{{ url('admin/assets/global/plugins/bootbox.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
    <script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>
    <script src="{{ url('admin/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('js/dhtmlxScheduler/codebase/dhtmlxscheduler.js') }}" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="{{ url('js/modules/fprofile.module.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/trainers/mysession.js') }}"></script>
@endsection
@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<span id="hidden_fields">
<input type="hidden" name="xml_url" id="XML_URL" value="{{ $xml_url }}" />
</span>
<div class="row">
    <div class="col-md-12">
       <div id="SessionScheduler" class="dhx_cal_container" style='width:100%; height:700px;'>
    		<div class="dhx_cal_navline">
    			<div class="dhx_cal_date"></div>
    		</div>
    		<div class="dhx_cal_header">
    		</div>
    		<div class="dhx_cal_data">
    		</div>
        </div>

    </div>
</div>
<!-- END PAGE BASE CONTENT -->
<div id="ModelPopUp"></div>
@endsection
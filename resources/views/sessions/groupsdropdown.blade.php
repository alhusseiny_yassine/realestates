<option value="">Groups</option>
@foreach ($project_classes as $index => $class) 
@if( isset( $project_groups[$class->fk_group_id] ) )
<option value="{{ $class->cls_id }}">{{ $project_groups[$class->fk_group_id]->pg_group_title }}</option>
@endif
@endforeach
<?php
/***********************************************************
sessionsDropdown.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 29, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
Session Dropdown
***********************************************************/

?>


<div>
    <label><b> Sessions : </b></label>
    <select class="bs-select form-control" name="sc_sessions_class" id="SC_SESSIONS_CLASS" data-actions-box="true">
        <option value="">-- Select One -- </option>
        @foreach($SessionClass as $sc_index => $sc_info)
            <option value="{{ $sc_info->sc_id }}">{{ $sc_info->sc_session_date }}</option>
        @endforeach
    </select>
</div>
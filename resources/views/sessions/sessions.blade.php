<?php
/* * *********************************************************
  groups.blade.php
  Product :
  Version : 1.0
  Release : 2
  Date Created : Aug 14 , 2017
  Developed By  : Alhusseiny Yassine   PHP Department
  All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

  Page Description :
  Groups Management Page
 * ********************************************************* */
?>

@extends('layouts.layout')

@section('themes')
<link href="{{ url('admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('admin/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('admin/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
<style>
    th{
        cursor: pointer;
    }
    #ChangeStatus{
        position: absolute;
       	top:40%;left:40%;
       	width:47%;
    }
    .modal-scrollable{
        left: 16% !important;
    }
    #ModelPopUp{
        margin-top: 50px !important;
        width:800px;
    }
    .modal_close {
        color: white;
        margin: 0px 5px;
        font-size: 16px;
        font-weight: 100;
        position: relative;
        top: 0;
        right: 0;
    }
    .modal_close:hover{
        color: white;
    }
</style>
<link href="{{ url('admin/assets/pages/css/search.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('admin/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ url('js/dhtmlxScheduler/codebase/dhtmlxscheduler.css') }}" type="text/css" media="screen" title="no title" charset="utf-8">
@endsection
@section('plugins')
<script src="{{ url('admin/assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>

<script src="{{ url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootbox.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('admin/assets/global/plugins/jquery.tablesorter/jquery.tablesorter.js') }}"></script>

<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>
<script src="{{ url('admin/assets/pages/scripts/ecommerce-orders-view.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('admin/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
<script src="{{ url('js/dhtmlxScheduler/codebase/dhtmlxscheduler.js') }}" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="{{ url('js/modules/projects.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/modules/sessions.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/sessions/sessionsmanagement.js') }}"></script>
@endsection

@section('content')
<div class="page-fixed-main-content">
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="search-page search-content-2">
        <div class="row">
            <span id="hidden_fields">
                {!! csrf_field() !!}
                <input type="hidden" name='session_page_number' id='SESSION_PAGE_NUMBER' value='1' />
                <input type="hidden" name='project_id'   id='PROJECT_ID' value='' />
                <input type="hidden" name='class_id'   id='CLASS_ID' value='' />
                <input type="hidden" name='display_type' id='DISPLAY_TYPE' value='list' />
            </span>
            <div class="col-md-12">
                <div class="search-container bordered">
                    <ul>
                        <li class="search-item-header">
                            <div class="row">
                                <div class="col-sm-6 col-xs-8">
                                    <h3> Select A Project </h3>
                                </div>
                                <div class="col-sm-3 col-xs-4">
                                    <div class="form-group">
                                        <select id="select_project" class="bs-select form-control">
                                            <option value="" > Projects </option>
                                            @foreach( $projects as $index => $project )
                                            <option value="{{ $project->pp_id }}">{{ $project->pp_project_title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-xs-4">
                                    <div class="form-group">
                                        <select id="select_group" class="bs-select form-control">
                                            <option value="">Groups</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <div class="row">
                        <div style="width:100%;" align="center">
                            <ul style="list-style-type: none;position:absolute;left:50%;">
                                <li style="float: left;position: relative;"><a href="#" data-data_type='list' id='LIST'><i class="fa fa-list" aria-hidden="true"></i></a></li>
                                <li style="float: left;position: relative;"><a href="#" data-data_type='calendar' id='CALENDAR'><i class="fa fa-calendar" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <ul style="padding-bottom:0px;">
                        <li class="search-item clearfix">
                            <div class="search-content">
                                <div class="row">
                                    <div class="portlet-body flip-scroll" id="SESSIONS">
                                        
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                     <div class="row">
                        <div class="col-md-12 cols-lg-12">
                            <ul id="SessionsPagination" class="pagination-sm"></ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" align="right">
                            <button type="button" name="btn_create_session" id="BTN_CREATE_SESSION" class="btn btn-info">Create</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>

@endsection
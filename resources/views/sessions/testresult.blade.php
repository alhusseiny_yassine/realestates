<?php
/***********************************************************
testresult.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 30, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/
 
?>

<div class="portlet-body flip-scroll" id="LISTCLASSES">
    <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
        <thead class="flip-content">
        <tr>
            <th> Trainee Id </th>
            <th> Trainee FullName </th>
            <th> Score </th>
            <th> Test Passed </th>
            <th> Retake </th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ( $lst_trainees as $index => $Trainee_info )
        { 
            ?>
               <tr data-trainee_id="{{ $Trainee_info->id }}">
                    <td> 
                        {{ $Trainee_info->id }} 
                        <input type="hidden" name="trainee_id[]" value="{{ $Trainee_info->id }}" />
                    </td>
                    <td> {{ $Trainee_info->u_fullname }} </td>
                    <td> <input type="text" name="trainee_score[{{ $Trainee_info->id }}]" class="form-control" value="{{ isset($Session_trainee_test_result[ $Trainee_info->id ]['score']) ? $Session_trainee_test_result[ $Trainee_info->id ]['score'] : "" }}" /> </td>
                    @if(isset($Session_trainee_test_result[ $Trainee_info->id ]['test_result']))
                    <td> <input type="checkbox" {{ ($Session_trainee_test_result[ $Trainee_info->id ]['test_result'] == 0) ? "" : "checked" }}  name="trainee_test_passed[{{ $Trainee_info->id }}]" value="1" /> </td>
                    @else
                    <td> <input type="checkbox"  name="trainee_test_passed[{{ $Trainee_info->id }}]" value="1" /> </td>
                    @endif
                    @if(isset($Session_trainee_test_result[ $Trainee_info->id ]['retake']))
                    <td> <input type="checkbox" {{ ($Session_trainee_test_result[ $Trainee_info->id ]['retake'] == 0) ? "" : "checked" }} name="trainee_course_retake[{{ $Trainee_info->id }}]" value="1" />  </td>
                    @else
                    <td> <input type="checkbox"  name="trainee_course_retake[{{ $Trainee_info->id }}]" value="1" /> </td>
                    @endif
                    
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>
</div>
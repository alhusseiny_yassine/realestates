<?php
/***********************************************************
beneficiaries.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 26, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
Beneficiaries Management for Add/Edit beneficiaries
***********************************************************/

?>

@extends('layouts.alayout')

@section('plugins')
<script src="{{ url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootbox.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('js/modules/jobroles.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/jobroles/jobrolesmanagement.js') }}"></script>
@endsection

@section('content')
<div class="portlet blue box">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Job Roles Management </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
     <div class="portlet-body">
   <span id="hidden_fields">
            <input type="hidden" name="page_number" value="1" />
        </span>
        <div class="row">
            <div class="col-md-12" align="right">
                    <div class="btn-group pull-right blue">
                        <button class="btn blue btn-outline dropdown-toggle" data-toggle="dropdown">Actions
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="javascript:;" id="PRINT_LIST"> Print </a>
                            </li>
                            <li>
                                <a href="javascript:;" id="EXPORT_TO_EXCELL"> Export to Excel </a>
                            </li>
                        </ul>
                  </div>
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">&nbsp;</div>
        </div>
        <div class="row">
                 <div class="col-md-12">
                     <div style="left:20%">
                        <div class="portlet-body flip-scroll" id="LISTJOBROLES">
                            <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
                                <thead class="flip-content">
                                <tr>
                                    <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_jobroles" id="CK_ALL_JOBROLES" class="group-checkable" data-set="#LISTJOBROLES .checkboxes" value="1" /></th>
                                    <th style="width:2px">#</th>
                                    <th>Job Role </th>
                                    <th style="width:2px" nowrap>Edit</th>
                                    <th style="width:2px" nowrap>Delete</th>
                                </tr>
                            </thead>
                            <tbody  class="ListJobRolesGird" >
                            </tbody>
                           </table>
                    </div>
                 </div>
             </div>
              <div class="row">
             <div class="col-md-10" align="left">
                <ul id="JRPagination" class="pagination-sm"></ul>
             </div>
             <div class="col-md-2" align="right">

             </div>
         </div>
             <div class="row">
                 <div class="col-md-10"></div>
                 <div class="col-md-2" align="right">
                    <button  name="btn_add_jobtitles" id="BTN_ADD_JOBROLES" type="button" class="btn blue capitalize" >ADD Job Role</button>
                 </div>
             </div>
     </div>
</div>
@endsection
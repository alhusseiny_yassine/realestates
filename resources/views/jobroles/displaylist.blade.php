<?php
/************************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

foreach ($jobroles as $index => $jr_info)
{
    ?>
       <tr data-jr_id="{{ $jr_info->jr_id }}">
           <td><input type="checkbox" class="checkboxes" name="ck_jobrole_{{ $jr_info->jr_id }}" id="CK_JOBROLE_{{ $jr_info->jr_id }}" value="1" /></td>
           <td><?php echo $jr_info->jr_id; ?></td>
           <td><?php echo $jr_info->jr_job_role; ?></td>
           <td> <a href="#"  id="EDIT_JOBROLE_{{ $jr_info->jr_id }}" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
           <td><a href="#"  id="DELETE_JOBROLE_{{ $jr_info->jr_id }}" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
       </tr>
    <?php
}
?>
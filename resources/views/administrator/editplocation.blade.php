<?php
/***********************************************************
Editeducationinfo.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Mar 17, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/


?>


@extends('layouts.alayout')

@section('themes')
<link href="{{ url('admin/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('plugins')
<script src="{{ url('admin/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootbox.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('js/modules/users.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/users/saveAproviderlocations.js') }}"></script>
@endsection
@section('content')
<div class="portlet blue box">
<div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Edit User Education Info </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
    <div class="portlet-body" style="min-height: 400px;" >
        <form name="form_save_pl_info" id="FORM_SAVE_PL_INFO">
            <div  class="form-body">
             <span id="hidden_fields">
                {!! csrf_field() !!}
                <input type="hidden" name="user_id" id="USER_ID" value="{{ $providerlocation_info->fk_user_id }}" />
                <input type="hidden" name="pl_id" id="PL_ID" value="{{ $providerlocation_info->pl_id }}" />
            </span>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Location <span class="required"> * </span></label>
                            <input type="text" maxlength="255" name="pl_provider_location" id="PL_PROVIDER_LOCATION" class="form-control" value="{{ $providerlocation_info->pl_provider_location }}" />
                        </div>
                    </div>
                    <div class="col-md-12" align="right">
                        <button name="btn_save_pl" class="btn btn-info" >Save Data </button>
                    </div>
                 </div>
            </div>
         </form>
     </div>
 </div>
@endsection
<?php
/***********************************************************
menu.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 22, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad MantachCOPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

?>

<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
         <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                <li class="nav-item start">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title">Dashboard</span>
                        <span class="selected"></span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item start ">
                            <a data-section="dashboard" href="{{ url('/administrator/dashboard') }}" class="nav-link ">
                                <i class="icon-bar-chart"></i>
                                <span class="title">Main Dashboard</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-database" aria-hidden="true"></i>
                        <span class="title">System Management</span>
                        <span class="selected"></span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item start ">
                            <a data-section="CutomersManagement" href="{{ url('administrator/CutomersManagement') }}" class="nav-link ">
                                <i class="fa fa-users" aria-hidden="true"></i>
                                <span class="title">Customers</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-university" aria-hidden="true"></i>
                        <span class="title">Real Estates Management</span>
                        <span class="selected"></span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item start ">
                            <a data-section="RealEstatesTypesManagement" href="{{ url('administrator/RealEstatesTypesManagement') }}" class="nav-link ">
                                <i class="fa fa-building" aria-hidden="true"></i>
                                <span class="title">Types</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{ url('administrator/Logout') }}" class="nav-link nav-toggle">
                        <i class="icon-power"></i>
                        <span class="title">Log Out</span>
                    </a>
                </li>

         </ul>
    </div>
</div>
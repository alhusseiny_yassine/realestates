<?php
/***********************************************************
displaylistmyrequests.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 3, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

?>
@for($i = 0; $i < count($myrequests); $i++)
<tr style="background-color: {{ $request_statuses_byid[ $myrequests[$i]->fk_status_id ]['status_color'] }}">
    <td><input type="checkbox" class="checkboxes" name="ck_request_{{ $myrequests[$i]->r_id }}" id="CK_REQUEST_{{ $myrequests[$i]->r_id }}" value="{{ $myrequests[$i]->r_id }}" /></td>
    <td>{{ $myrequests[$i]->r_request_code }}</td>
    <td>{{ $myrequests[$i]->r_request_title }}</td>
    <td>{{ $myrequests[$i]->r_request_train_date }}</td>
    <td>{{ $request_statuses_byid[ $myrequests[$i]->fk_status_id ]['status_name'] }}</td>
    <td></td>
</tr>
@endfor
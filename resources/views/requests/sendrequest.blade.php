<?php
/***********************************************************
sendrequest.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 2, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

?>


<div class="portlet blue box" style="height: 100%;width:500px;">
<div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Send Training Request </div>
        <div class="tools">
        </div>
    </div>
    <div class="portlet-body">
        <form name="frm_send_request" id="FRM_SEND_REQUEST">
            <span id="hidden_fields">
                  {!! csrf_field() !!}
                  <input type="hidden" name="course_ids" value="{{ $course_ids }}" />
            </span>
            <div class="row">
                <div class="cols-md-6">
                    <div class="form-field" align="left">
                        <label>Priority :<span style="color:red">*</span></label>
                        <span>
                            <input type="number" name="r_request_order" class="form-control" min="1" max="5" step="1" value="" />
                        </span>
                    </div>
                </div>
                <div class="cols-md-6" align="left">
                    <label>relevance :<span style="color:red">*</span></label>
                        <span>
                            <textarea style="width:100%;height:150px;resize: none" class="form-control" name="r_request_description"></textarea>
                        </span>
                </div>
            </div>
            <div class="row" style="height:10px;"></div>
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <button type="submit" name="btn_send_request" id="BTN_SEND_REQUEST" class="btn btn-info"  style="width:100%;height:50px;">Send Request</button>
                </div>
                <div class="col-md-1"></div>
            </div>
        </form>
    </div>
</div>
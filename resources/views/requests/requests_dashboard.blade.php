<?php
/***********************************************************
requests_dashboard.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 4, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/
//"trainee_info" => $trainee_info,
//"request_statuses_info" => $request_statuses_info
?>

<div class="row">
        <div class="col-md-6 col-sm-12">
            <div class="portlet yellow-crusta box">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Request Details </div>

                </div>
                <div class="portlet-body">
                    <div class="row static-info">
                        <div class="col-md-5 name"> Request Number #: </div>
                        <div class="col-md-7 value">
                        {{ $last_request_info->r_id }}
                        </div>
                    </div>
                    <div class="row static-info">
                        <div class="col-md-5 name"> Request Training Date : </div>
                        <div class="col-md-7 value"> {{ $last_request_info->r_request_train_date }} </div>
                    </div>
                    <div class="row static-info">
                        <div class="col-md-5 name"> Request Status: </div>
                        <div class="col-md-7 value">
                            <span class="label label-success"> {{ $request_statuses_info->rs_status_name }} </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-12">
            <div class="portlet blue-hoki box">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Trainee Information </div>

                </div>
                <div class="portlet-body">
                    <div class="row static-info">
                        <div class="col-md-5 name"> Trainee FullName: </div>
                        <div class="col-md-7 value"> {{ $trainee_info->u_fullname }} </div>
                    </div>
                     <div class="row static-info">
                        <div class="col-md-5 name"> Trainee Beneficiary: </div>
                        <div class="col-md-7 value"> {{ $benef_info->sb_name }} </div>
                    </div>
                    <div class="row static-info">
                        <div class="col-md-5 name"> Trainee Email: </div>
                        <div class="col-md-7 value"> {{ $trainee_info->u_email }} </div>
                    </div>
                    <div class="row static-info">
                        <div class="col-md-5 name"> Trainee Beneficiary Code: </div>
                        <div class="col-md-7 value"> {{ $benef_info->sb_code }} </div>
                    </div>
                    <div class="row static-info">
                        <div class="col-md-5 name"> Trainee Phone Number: </div>
                        <div class="col-md-7 value"> {{ $trainee_info->u_mobile }} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet grey-cascade box">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Courses Details
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th> Course Code </th>
                                    <th> Course Name </th>
                                    <th> Course Type </th>
                                    <th> Course Total Hours </th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($courses_data_array as $course_id => $course_info)
                                <tr>
                                    <td>{{ $course_info->c_code }}</td>
                                    <td>{{ $course_info->c_course_name }}</td>
                                    <td>{{ isset($course_categories_info[ $course_info->fk_category_id ]) ? $course_categories_info[ $course_info->fk_category_id ] : "N/A" }}</td>
                                    <td>{{ $course_info->c_total_hours }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
/************************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>
<div class="portlet-body flip-scroll" id="LISTCOUNTRIES">
    <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
        <thead class="flip-content">
        <tr>
            <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_countries" id="CK_ALL_COUNTRIES" class="group-checkable" data-set="#LISTCOUNTRIES .checkboxes" value="1" /></th>
            <th style="width:2px">#</th>
            <th>Country Code </th>
            <th>Country Name </th>
            <th style="width:2px" nowrap>Edit</th>
            <th style="width:2px" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($countries as $index => $cont_info)
        {
            ?>
               <tr data-country_id="{{ $cont_info->id }}">
                   <td><input type="checkbox" class="checkboxes" name="ck_country_{{ $cont_info->id }}" id="CK_COUNTRY_{{ $cont_info->id }}" value="1" /></td>
                   <td><?php echo $cont_info->id; ?></td>
                   <td><?php echo $cont_info->code; ?></td>
                  <td><?php echo $cont_info->name; ?></td>
                   <td> <a href="#"  id="EDIT_COUNTRY_{{ $cont_info->id }}" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
                   <td><a href="#"  id="DELETE_COUNTRY_{{ $cont_info->id }}" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>
</div>
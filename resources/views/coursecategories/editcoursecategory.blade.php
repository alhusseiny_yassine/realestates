<?php
/***********************************************************
editcoursetype.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 27, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


?>


@extends('layouts.alayout')

@section('plugins')
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/modules/coursecategories.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/coursecategories/editcoursecategory.js') }}"></script>
@endsection
@section('content')

<div class="portlet blue box">
<div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Edit Course Category </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
    <div class="portlet-body">
         <form name="form_save_course_category" id="FORM_SAVE_COURSE_CATEGORY">
    <div class="form-body">
         <span id="hidden_fields">
            {!! csrf_field() !!}
            <input type="hidden" name="cc_id" value="{{ $cur_courseCategory_info->cc_id }}" />
        </span>
        <div class="alert alert-success" style="display:none">
				<strong>Success!</strong> Course Category Information is saved successfully!
			</div>
			<div class="alert alert-danger" style="display:none">
				<strong>Error!</strong> You have some form errors. Please check below.
			</div>
        <div class="row">
            <div class="col-md-6">
                 <div class="form-group">
                    <label class="control-label">Course Category Name <span class="required"> * </span></label>
                    <input type="text" name="cc_course_category" id="CC_COURSE_CATEGORY" class="form-control" required="required" maxlength="500"  value="{{ $cur_courseCategory_info->cc_course_category }}" />
                </div>
            </div>
            <div class="col-md-6">
                    <div class="form-group">
                            <label> Course Category Type <span class="required"> * </span></label>
                            <select class="bs-select form-control" name="fk_course_type" id="FK_COURSE_TYPE" data-actions-box="true">
                                    <option value="">-- Select One -- </option>
                                    @foreach($courseTypes as $ct_index => $ct_info)
                                        <option {{ $cur_courseCategory_info->fk_course_type == $ct_info->ct_id  ? "selected" : "" }} value="{{ $ct_info->ct_id }}">{{ $ct_info->ct_course_type }}</option>
                                    @endforeach
                            </select>
                        </div>
            </div>
         

        </div>
       <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-md-9"></div>
            <div class="col-md-3" align="right">
                 <button type="submit" name="btn_save_course_category" id="BTN_SAVE_COURSE_CATEGORY"  class="btn blue">Save</button>
                <button type="button" id="BACK_FORM" name="back_form" class="btn default">Back</button>
            </div>
        </div>
    </div>
</form>
    </div>
</div>


@endsection
<?php
/***********************************************************
dropdown.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 20, 2017
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/


?>

<select class="bs-select form-control" name="fk_category_id" id="FK_CATEGORY_ID" data-actions-box="true">
    <option value="">No Course Category</option>
    @foreach($course_categories_data as $cc_index => $cc_info)
        <option value="{{ $cc_info->cc_id }}">{{ $cc_info->cc_course_category }}</option>
    @endforeach
</select>
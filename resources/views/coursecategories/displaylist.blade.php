<?php
/************************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>
<div class="portlet-body flip-scroll" id="LISTCOURSECATEGORIES">
    <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
        <thead class="flip-content">
        <tr>
            <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_coursecategory" id="CK_ALL_COURSECATEGORIES" class="group-checkable" data-set="#LISTCOURSECATEGORIES .checkboxes" value="1" /></th>
            <th style="width:2px">#</th>
            <th>Course Category Name </th>
            <th style="width:2px" nowrap>Edit</th>
            <th style="width:2px" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($course_categories as $index => $course_category_info)
        {
            ?>
               <tr data-cc_id="{{ $course_category_info->cc_id }}">
                   <td><input type="checkbox" class="checkboxes" name="ck_course_cat_{{ $course_category_info->cc_id }}" id="CK_COURSE_CATEGORY_{{ $course_category_info->cc_id }}" value="1" /></td>
                   <td><?php echo $course_category_info->cc_id; ?></td>
                   <td><?php echo $course_category_info->cc_course_category; ?></td>
                   <td> <a href="#"  id="EDIT_COURSE_CATEGORY_{{ $course_category_info->cc_id }}" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
                   <td><a href="#"  id="DELETE_COURSE_CATEGORY_{{ $course_category_info->cc_id }}" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>
</div>
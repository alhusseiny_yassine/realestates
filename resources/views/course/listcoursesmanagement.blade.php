<?php
/***********************************************************
listcoursesmanagement.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 30, 2016
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2016

Page Description :
Courses Management Page in trainees section where you can display all course to select and
send request to admin
***********************************************************/


?>


@extends('layouts.layout')

@section('themes')
<link href="{{ url('admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('admin/assets/pages/css/search.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css" />
        <style>
            .bootbox{
	           width:auto !important;
            }
        </style>
@endsection
@section('plugins')
<script src="{{ url('admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootbox.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js') }}" type="text/javascript"></script>
    <script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
    <script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/modules/course.module.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/course/listcourses.js') }}"></script>
@endsection

@section('content')
<div id="SendRequest"></div>
<span id="hidden_fields">
 <input type="hidden" name="page_number" value="1" />
</span>
<div class="portlet light form-fit bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-bubble font-green"></i>
            <span class="caption-subject font-green sbold uppercase">Course Management</span>
        </div>
        <div class="actions">
            <div class="btn-group">
                <a class="btn green btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu pull-right">
                    <li>
                        <a id="TRAINEE_SEND_REQUEST" href="javascript:;"> Send Request </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="search-page search-content-4">
            <div class="search-bar bordered">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="input-group">
                            <input type="text" name="search_query" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                                <button  name="btn_search"  id="BTN_SEARCH"  class="btn green-soft uppercase bold" type="button">Search</button>
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-4 extra-buttons">
                        <button name="btn_reset_search" id="BTN_RESET_SEARCH" class="btn grey-steel uppercase bold" type="button">Reset Search</button>
                        <button name="btn_advanced_search" id="BTN_ADVANCED_SEARCH" class="btn grey-cararra font-blue" type="button">Advanced Search</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 AdvancedSearchSection" style="display:none">
                        <div class="col-md-4">
                            <label>Course Category</label>
                            <select name="c_course_category" id="C_COURSE_CATEGORY" class="form-control">
                                <option value="0">All Categories</option>
                                @foreach($course_categories_data as $cc_index => $cc_info)
                                    <option value="{{ $cc_info->cc_id }}">{{ $cc_info->cc_course_category }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ListCourseGird search-table table-responsive">

            </div>
            <div class="search-pagination pagination-rounded">
                 <ul id="CoursesPagination" class="pagination-sm"></ul>
            </div>
        </div>
    </div>
 </div>
@endsection
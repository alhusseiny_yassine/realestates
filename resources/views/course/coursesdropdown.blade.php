<?php
/***********************************************************
coursesdropdown.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Dec 24, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/
 
?>
<label><b>Course :</b></label>
<select name="pp_course" id="PP_COURSE" class="form-control">
<option value="0">--Select Course--</option>
@foreach($project_course_info  as $index => $info)
<option value="{{ $index }}">{{ $info }}</option>
@endforeach
</select>
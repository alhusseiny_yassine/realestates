<?php
/***********************************************************
beneficiaries.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 26, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
Beneficiaries Management for Add/Edit beneficiaries
***********************************************************/

?>

@extends('layouts.alayout')

@section('plugins')
<script src="{{ url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootbox.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('js/modules/course.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/course/coursemanagement.js') }}"></script>
@endsection

@section('content')
<div class="portlet blue box">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Course Management </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
     <div class="portlet-body">
   <span id="hidden_fields">
            <input type="hidden" name="page_number" value="1" />
        </span>
        <div class="row">
            <div class="col-md-12" align="right">
                    <div class="btn-group pull-right blue">
                        <button class="btn blue btn-outline dropdown-toggle" data-toggle="dropdown">Actions
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                        </ul>
                  </div>
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">&nbsp;</div>
        </div>
         <div class="row">
            <div class="col-md-6">
                <label> Course Type </label>
                <select name="sl_course_type" id="SL_COURSE_TYPE" class="form-control">
                    <option value="">No Course Type</option>
                            @foreach($course_types as $ct_index => $ct_info)
                                <option  value="{{ $ct_info->ct_id }}">{{ $ct_info->ct_course_type }}</option>
                            @endforeach
                </select>
            </div>
            <div class="col-md-6">
                <label> Course Category </label>
                <span class="CoursesCategoriesDropdown">
                
                </span>
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">&nbsp;</div>
        </div>
        <div class="row">
                 <div class="col-md-12">
                     <div style="left:20%">
                        <div class="portlet-body flip-scroll" id="LISTCOURSE">
                            <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
                                <thead class="flip-content">
                                <tr>
                                    <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_courses" id="CK_ALL_COURSES" class="group-checkable" data-set="#LISTCOURSE .checkboxes" value="1" /></th>
                                    <th style="width:2px">#</th>
                                    <th>Course Code </th>
                                    <th>Course Name</th>
                                    <th>Course Description</th>
                                    <th style="width:2px" nowrap>Edit</th>
                                    <th style="width:2px" nowrap>Delete</th>
                                </tr>
                                <tr>
                                    <th  class="table-checkbox" style="width:2px;">&nbsp;</th>
                                    <th style="width:2px">#</th>
                                    <th><input type="text" name="c_course_code" id="C_COURSE_CODE" class="form-control" value="" /></th>
                                    <th><input type="text" name="c_course_name" id="C_COURSE_NAME" class="form-control" value="" /></th>
                                    <th><input type="text" name="c_course_description" id="C_COURSE_DESCRIPTION" class="form-control" value="" /></th>
                                    <th colspan="2"></th>
                                </tr>
                            </thead>
                            <tbody class="ListCourseGird">

                            </tbody>
                        </table>
                        </div>
                    </div>
                 </div>
             </div>
              <div class="row">
             <div class="col-md-10" align="left">
                <ul id="CoursePagination" class="pagination-sm"></ul>
             </div>
             <div class="col-md-2" align="right">

             </div>
         </div>
             <div class="row">
                 <div class="col-md-8"></div>
                 <div class="col-md-4" align="right">
                    <button  name="btn_add_course" id="BTN_ADD_COURSE" type="button" class="btn blue capitalize" >ADD Course</button>
                 </div>
             </div>
     </div>
</div>
@endsection
<?php
/***********************************************************
editcoursetype.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 27, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


?>


@extends('layouts.alayout')

@section('themes')
    <link href="{{ url('admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('plugins')
    <script src="{{ url('admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/modules/course.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/course/editcourse.js') }}"></script>
@endsection
@section('content')

<div class="portlet blue box">
<div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Edit Course </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
    <div class="portlet-body">
         <form name="form_save_course" id="FORM_SAVE_COURSE">
    <div class="form-body">
         <span id="hidden_fields">
            {!! csrf_field() !!}
            <input type="hidden" name="c_id" id="C_ID" value="{{ $cur_course_info->c_id }}" />
        </span>
        <div class="alert alert-success" style="display:none">
				<strong>Success!</strong> Course Information is saved successfully!
			</div>
			<div class="alert alert-danger" style="display:none">
				<strong>Error!</strong> You have some form errors. Please check below.
			</div>
        <div class="row">
            <div class="col-md-6">
                 <div class="form-group">
                    <label class="control-label">Course Code <span class="required"> * </span></label>
                    <input type="text" name="c_code" id="C_CODE" class="form-control" required="required" maxlength="5"  value="{{ $cur_course_info->c_code }}" />
                </div>
            </div>
            <div class="col-md-6">
                 <div class="form-group">
                    <label class="control-label">Course Title <span class="required"> * </span></label>
                    <input type="text" name="c_course_name" id="C_COURSE_NAME" class="form-control" required="required" maxlength="500"  value="{{ $cur_course_info->c_course_name }}" />
                </div>
            </div>
            <div class="col-md-6">
                 <div class="form-group">
                    <label class="control-label">Course Total Hours <span class="required"> * </span></label>
                    <input type="text" name="c_total_hours" id="C_TOTAL_HOURS" class="form-control" required="required" maxlength="500"  value="{{ $cur_course_info->c_total_hours }}" />
                </div>
            </div>
            <div class="col-md-6">
                 <div class="form-group">
                    <label class="control-label">Course Category <span class="required"> * </span></label>
                      <select class="bs-select form-control" name="fk_category_id" id="FK_CATEGORY_ID" data-actions-box="true">
                            <option value="">No Course Category</option>
                            @foreach($course_categories_data as $cc_index => $cc_info)
                                <option {{ $cur_course_info->fk_category_id == $cc_info->cc_id ? "selected" : "" }} value="{{ $cc_info->cc_id }}">{{ $cc_info->cc_course_category }}</option>
                            @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                 <div class="form-group">
                    <label class="control-label">Course Description <span class="required"> * </span></label>
                    <textarea name="c_course_description" id="C_COURSE_DESCRIPTION" class="form-control" style="width:100%;resize:none" rows="10"  >{{ $cur_course_info->c_course_description }}</textarea>
                </div>
            </div>
            <div class="col-md-6">
                 <div class="form-group" style="height: 30px;">
                    <label class="control-label">Enable Course</label>
                     <input type="checkbox" {{ $cur_course_info->c_is_active == 1 ? "checked" : "" }} name="c_is_active" value='1' />
                </div>
            </div>

        </div>
       <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-md-9"></div>
            <div class="col-md-3" align="right">
                 <button type="submit" name="btn_save_course" id="BTN_SAVE_COURSE"  class="btn blue">Save</button>
                <button type="reset" name="reset_form" id="RESET_FORM"  class="btn default">Cancel</button>
            </div>
        </div>
    </div>
</form>
    </div>
</div>
@endsection
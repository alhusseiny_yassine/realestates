<?php
/************************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/



foreach ($courses as $index => $course_info)
{
    ?>
       <tr data-c_id="{{ $course_info->c_id }}">
           <td><input type="checkbox" class="checkboxes" name="ck_course_{{ $course_info->c_id }}" id="CK_COURSE_{{ $course_info->c_id }}" value="1" /></td>
           <td><?php echo $course_info->c_id; ?></td>
           <td><?php echo $course_info->c_code; ?></td>
           <td><?php echo $course_info->c_course_name; ?></td>
           <td><?php echo substr($course_info->c_course_description, 0,10); ?></td>
           <td> <a href="#"  id="EDIT_COURSE_{{ $course_info->c_id }}" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
           <td><a href="#"  id="DELETE_COURSE_{{ $course_info->c_id }}" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
       </tr>
    <?php
}

?>
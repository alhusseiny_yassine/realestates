<?php
/************************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>
<div class="portlet-body flip-scroll" id="LISTBENEFICIARYDEPS">
    <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
        <thead class="flip-content">
        <tr>
            <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_benftype" id="CK_ALL_BENFTYPES" class="group-checkable" data-set="#LISTBENEFICIARYDEPS .checkboxes" value="1" /></th>
            <th style="width:2px">#</th>
            <th>Beneficiary Directorate Name </th>
            <th>Beneficiary Name</th>
            <th style="width:2px" nowrap>Edit</th>
            <th style="width:2px" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($beneficiary_dir as $index => $benfd_info)
        {

            ?>
               <tr data-bd_id="{{ $benfd_info->bd_id }}">
                   <td><input type="checkbox" class="checkboxes" name="ck_benfd_{{ $benfd_info->bd_id }}" id="CK_BENFD_{{ $benfd_info->bd_id }}" value="1" /></td>
                   <td><?php echo $benfd_info->bd_id; ?></td>
                   <td><?php echo $benfd_info->bd_directorate_name; ?></td>
                   <td><?php echo ($benfd_info->fk_benf_id == null) ? "" : $bnf_result_array[$benfd_info->fk_benf_id]; ?></td>
                   <td> <a href="#"  id="EDIT_BENFD_{{ $benfd_info->bd_id }}" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
                   <td><a href="#"  id="DELETE_BENFD_{{ $benfd_info->bd_id }}" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>
</div>
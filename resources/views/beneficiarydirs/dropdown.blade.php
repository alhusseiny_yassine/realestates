<?php
/***********************************************************
dropdown.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 20, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

?>

<select class="bs-select form-control" name="fk_benf_dep_id" id="FK_BENF_DEP_ID" data-actions-box="true">
    <option value="">No Department</option>
    @foreach($beneficiaries_dirs as $bd_index => $bd_info)
        <option value="{{ $bd_info->bd_id }}">{{ $bd_info->bd_directorate_name }}</option>
    @endforeach
</select>
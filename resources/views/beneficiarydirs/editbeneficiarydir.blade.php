<?php
/************************************************************
editrole.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/


?>

@extends('layouts.alayout')

@section('plugins')
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/modules/beneficiarydeps.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/beneficiarydirs/savebeneficiarydirs.js') }}"></script>
@endsection
@section('content')

<div class="portlet blue box">
<div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Edit Beneficiary Directorate </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
    <div class="portlet-body">
         <form name="form_save_benfd" id="FORM_SAVE_BENFD">
    <div class="form-body">
         <span id="hidden_fields">
         <input type="hidden" name="bd_id" id="BD_ID" value="{{ $cur_bendir_info->bd_id }}" />
            {!! csrf_field() !!}
        </span>
        <div class="alert alert-success" style="display:none">
				<strong>Success!</strong> Benficiary Directorate Information is saved successfully!
			</div>
			<div class="alert alert-danger" style="display:none">
				<strong>Error!</strong> You have some form errors. Please check below.
			</div>
        <div class="row">
            <div class="col-md-4">
                 <div class="form-group">
                    <label class="control-label">Benficiary Department Name <span class="required"> * </span></label>
                    <input type="text" name="bd_directorate_name" id="BD_DIRECTORATE_NAME" class="form-control" required="required" maxlength="500"  value="{{ $cur_bendir_info->bd_directorate_name }}" />
                </div>
            </div>
            <div class="col-md-4">
                 <div class="form-group">
                    <label class="control-label">Benficiary <span class="required"> * </span></label>
                    <select name="fk_beneficiary_id" id="FK_BENEFICIARY_ID" class="form-control" style="width:100%">
                        <option value="">-- Select One --</option>
                        @foreach($all_benf as $sb_index => $ben_info)
                            <option {{ $cur_bendir_info->fk_benf_id == $ben_info['sb_id'] ? "selected" : "" }}  value="{{ $ben_info['sb_id'] }}">{{ $ben_info['sb_name'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

        </div>
       <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-md-9"></div>
            <div class="col-md-3" align="right">
                 <button type="submit" name="btn_save_benfdir" id="BTN_SAVE_BENFDIR"  class="btn blue">Save</button>
                <button type="button" id="BACK_FORM" name="back_form" class="btn default">Back</button>
            </div>
        </div>
    </div>
</form>
    </div>
</div>

@endsection
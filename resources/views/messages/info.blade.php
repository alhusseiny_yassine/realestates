<?php
/***********************************************************
info.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 4, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
information style message
***********************************************************/

?>

 <div class="alert alert-info"><strong>Info!</strong> {{ $message }} </div>
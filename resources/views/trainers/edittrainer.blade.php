<?php
/***********************************************************
 edittrainer.blade.php
 Product : 
 Version : 1.0
 Release : 2
 Date Created :  Aug 3, 2017
 Developed By  : Alhusseiny Yassine   PHP Department
 
 Page Description :
 Edit Trainer Form
 ***********************************************************/
?>

@extends('layouts.layout')

@section('themes')
<link
	href="{{ url('admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}"
	rel="stylesheet" type="text/css" />

<link
	href="{{ url('admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}"
	rel="stylesheet" type="text/css" />

<link
	href="{{ url('admin/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}"
	rel="stylesheet" type="text/css" />

<link
	href="{{ url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css') }}"
	rel="stylesheet" type="text/css" />
<link
	href="{{ url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}"
	rel="stylesheet" type="text/css" />
<link
	href="{{ url('admin/assets/global/plugins/datatables/datatables.min.css') }}"
	rel="stylesheet" type="text/css" />
<link
	href="{{ url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}"
	rel="stylesheet" type="text/css" />
<link
	href="{{ url('admin/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}"
	rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="{{ url('js/dhtmlxScheduler/codebase/dhtmlxscheduler.css') }}"
	type="text/css" media="screen" title="no title" charset="utf-8">
<style>
th {
	cursor: pointer;
}

#ModelPopUp {
	width: 800px;
}
</style>
@endsection @section('plugins')

<script
	src="{{ url('js/dhtmlxScheduler/codebase/dhtmlxscheduler.js') }}"
	type="text/javascript" charset="utf-8"></script>
<script src="{{ url('admin/assets/global/scripts/datatable.js') }}"
	type="text/javascript"></script>
<script
	src="{{ url('admin/assets/global/plugins/datatables/datatables.min.js') }}"
	type="text/javascript"></script>
<script
	src="{{ url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}"
	type="text/javascript"></script>
<script
	src="{{ url('admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"
	type="text/javascript"></script>
   
<script
	src="{{ url('admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"
	type="text/javascript"></script>        
        
<script
	src="{{ url('admin/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
	type="text/javascript"></script>

<script
	src="{{ url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js') }}"
	type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootbox.min.js') }}"
	type="text/javascript"></script>
<script
	src="{{ url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}"
	type="text/javascript"></script>
<script
	src="{{ url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js') }}"
	type="text/javascript"></script>
<script type="text/javascript"
	src="{{ url('admin/assets/global/plugins/jquery.tablesorter/jquery.tablesorter.js') }}"></script>

<script type="text/javascript"
	src="{{ url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript"
	src="{{ url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>
        
<script type="text/javascript"
	src="{{ url('admin/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>  
        
<script
	src="{{ url('admin/assets/pages/scripts/ecommerce-orders-view.js') }}"
	type="text/javascript"></script>
<script type="text/javascript"
	src="{{ url('js/modules/projects.module.js') }}"></script>
<script type="text/javascript"
	src="{{ url('js/modules/class.module.js') }}"></script>
<script type="text/javascript"
	src="{{ url('js/modules/trainers.module.js') }}"></script>
<script type="text/javascript"
	src="{{ url('js/trainers/addtrainer.js') }}"></script>
@endsection @section('content')
<?php $session_user_id = session('ut_user_id');?>
<div class="portlet blue box">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Edit Trainer </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
    <div class="portlet-body">
        <form name="form_save_tariner" id="FORM_SAVE_TRAINER">
            <div  class="form-body">
                <span id="hidden_fields">
                    {!! csrf_field() !!}
                    <input type="hidden" name="fb_user_type" id="FB_USER_TYPE" value="5">
                    <input type="hidden" name="u_role" id="U_ROLE" value="5">
                    <input type="hidden" name="fk_provider_id" id="fk_provider_id" value="<?php echo $session_user_id; ?>">
                    <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>">
                    <input type="hidden" name="form_type" id="form_type" value="edit">
                    
                </span>
                <div class="alert alert-success" style="display:none">
                    <strong>Success!</strong> Trainer Information is saved successfully!
                </div>
                <div class="alert alert-danger" style="display:none">
                    <strong>Error!</strong> You have some form errors. Please check below.
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Full Name <span class="required"> * </span></label>
                            <input type="text" maxlength="500" name="fullname" id="FULLNAME" class="form-control" value="{{ $user_info->u_fullname }}" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Email <span class="required"> * </span></label>
                            <input type="email" maxlength="255" name="u_email" id="U_EMAIL" class="form-control" value="{{ $user_info->u_email }}" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">password</label>
                            <input type="password" name="u_password_edit" maxlength="150" id="U_PASSWORD_EDIT" class="form-control" value="" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Re-type password</label>
                            <input type="password" name="retype_u_password_edit" maxlength="150" id="RETYPE_U_PASSWORD_EDIT" class="form-control" value="" />
                        </div>
                    </div>
                </div>
                <div class="row" style="height:5px;"></div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Gender</label><br/>

                            <div class="mt-radio-list" data-error-container="#GenderError">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="radio" {{ ($user_info->u_gender =="m") ? "checked" : "" }} name="u_gender"  value="m" /> Male
                                    </div>
                                    <div class="col-md-6">
                                        <input type="radio" {{ ($user_info->u_gender =="f") ? "checked" : "" }}  name="u_gender"  value="f" /> Female
                                    </div>
                                </div>

                            </div>
                            <div id="GenderError"></div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Residential Area</label>
                            <textarea style="width:100%;height: 100px;resize:none" class="form-control" name="u_residential_area" id="U_RESIDENTIAL_AREA">{{ $user_info->u_residential_area }}</textarea>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Year Of Birth <span class="required"> * </span></label>
                            <input data-provide="datepicker" class="form-control" name="u_date_birth" id="u_date_birth" value="{{ $user_info->u_date_birth }}" />
                            
                        </div>
                    </div>
                    <div class="col-md-3" style="height: 40px;">
                        <label class="control-label">Enable User</label><br>
                        <input type="checkbox" name="u_is_active" {{ $user_info->u_is_active == 1 ? 'checked' : '' }} value='1' />
                    </div>
                </div>
                <div class="row" style="height:5px;"></div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">mobile</label>
                            <input type="text" maxlength="20" name="u_mobile" id="U_MOBILE" class="form-control" value="{{ $user_info->u_mobile }}" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Phone</label>
                            <input type="text" maxlength="20" name="u_phone" id="U_PHONE" class="form-control" value="{{ $user_info->u_phone }}" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Fax</label>
                            <input type="text" maxlength="20" name="u_fax" id="U_FAX" class="form-control" value="{{ $user_info->u_fax }}" />
                        </div>
                    </div>
                </div>
                <div class="row" style="height:5px;"></div>
                <div class="row">
                    <div class="col-md-9"></div>
                    <div class="col-md-3" align="right">
                        <button type="submit" id="BTN_SAVE_TRAINER" name="btn_save_trainer" class="btn blue capitalize">Save</button>&nbsp;&nbsp;<button type="button" id="BACK_FORM" name="back_form" class="btn default">Back</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

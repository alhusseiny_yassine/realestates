<?php
/* * **********************************************************
  displaylisttrainers.blade.php
  Product :
  Version : 1.0
  Release : 0
  Developed By  : Alhusseiny Yassine  PHP Department Softweb S.A.R.L
  All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2017

  Page Description :
  Display list of trainers in providers account
 * ********************************************************** */
$session_user_id = session()->get('ut_user_id');
?>
<table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
    <thead class="flip-content">
        <tr>
            <th style="width:2px;">#</th>
            <th>Email</th>
            <th>Full Name</th>
            <th></th>
            <th style="width:2px;" nowrap>Edit</th>
            <th style="width:2px;" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($trainers as $index => $trainer_info) {
            ?>
            <tr  class="odd gradeX" data-user_id="<?php echo $trainer_info['id']; ?>">
                <td><?php echo $trainer_info['id']; ?></td>
                <td><?php echo $trainer_info['u_email']; ?></td>
                <td><?php echo $trainer_info['u_fullname']; ?></td>

                <td>
                        <?php if ($trainer_info->isOnline()) { ?>
                        <img src="{{ url('imgs/online_user.png') }}" />
                        <?php } else { ?>
                        <img src="{{ url('imgs/offline_user.png') }}" />
                        <?php } ?>
                </td>
                <td>
                    <a href="#"  id="EDIT_TRAINER_<?php echo $trainer_info['id']; ?>" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a>
                </td>
                <?php
                $display = ($session_user_id == $trainer_info['id']) ? "display:none;" : "";
                ?>
                <td>
                    <a href="#" style="{{ $display }}"  id="DELETE_TRAINER_<?php echo $trainer_info['id']; ?>" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a>
                </td>
            </tr>
    <?php
}
?>
    </tbody>
</table>
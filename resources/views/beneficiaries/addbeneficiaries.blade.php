<?php
/***********************************************************
addbeneficiaries.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 27, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

?>


@extends('layouts.alayout')

@section('plugins')
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/modules/beneficiaries.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/beneficiaries/addbeneficiaries.js') }}"></script>
@endsection
@section('content')

<div class="portlet blue box">
<div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Add Beneficiary </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
    <div class="portlet-body">
         <form name="form_save_benf" id="FORM_SAVE_BENF">
    <div class="form-body">
         <span id="hidden_fields">
            {!! csrf_field() !!}
        </span>
        <div class="alert alert-success" style="display:none">
				<strong>Success!</strong> Benficiary Information is saved successfully!
			</div>
			<div class="alert alert-danger" style="display:none">
				<strong>Error!</strong> You have some form errors. Please check below.
			</div>
        <div class="row">
            <div class="col-md-4">
                 <div class="form-group">
                    <label class="control-label">Benficiary Code <span class="required"> * </span></label>
                    <input type="text" name="sb_code" id="SB_CODE" class="form-control" required="required" maxlength="15"  value="" />
                </div>
            </div>
            <div class="col-md-4">
                 <div class="form-group">
                    <label class="control-label">Benficiary Name <span class="required"> * </span></label>
                    <input type="text" name="sb_name" id="SB_NAME" class="form-control" required="required" maxlength="100"  value="" />
                </div>
            </div>
            <div class="col-md-4">
                 <div class="form-group">
                    <label class="control-label">Benficiary Arabic Name <span class="required"> * </span></label>
                    <input type="text" name="sb_arabic_name" id="SB_ARABIC_NAME" class="form-control" required="required" maxlength="100"  value="" />
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label> Beneficiary Type</label>
                    <select class="bs-select form-control" name="sb_benf_type" id="SB_BENF_TYPE" data-actions-box="true">
                            <option value="">No Beneficiary Type</option>
                            @foreach($lst_benf_types as $bt_index => $bt_info)
                                <option value="{{ $bt_info['bt_id'] }}">{{ $bt_info['bt_name'] }}</option>
                            @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label> Beneficiary Parent</label>
                    <select class="bs-select form-control" name="fk_benf_id" id="FK_BENF_ID" data-actions-box="true">
                            <option value="">No Parent</option>
                            @foreach($lst_benf as $b_index => $b_info)
                                <option value="{{ $b_info['sb_id'] }}">{{ $b_info['sb_name'] }}</option>
                            @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label> Telephone </label>
                    <input type="text" name="sb_telephone" id="SB_TELEPHONE" class="form-control" maxlength="20"  value="" />
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label> Fax </label>
                    <input type="text" name="sb_fax" id="SB_FAX" class="form-control" maxlength="20"  value="" />
                </div>
            </div>
        </div>
       <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-md-9"></div>
            <div class="col-md-3" align="right">
                 <button type="submit" name="btn_add_benf" id="BTN_ADD_BENF"  class="btn blue">Save</button>
                <button type="button" id="BACK_FORM" name="back_form" class="btn default">Back</button>
            </div>
        </div>
    </div>
</form>
    </div>
</div>


@endsection
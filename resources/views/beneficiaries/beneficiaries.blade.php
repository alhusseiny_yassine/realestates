<?php
/***********************************************************
beneficiaries.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 26, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
Beneficiaries Management for Add/Edit beneficiaries
***********************************************************/

?>

@extends('layouts.alayout')

@section('plugins')
<script src="{{ url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootbox.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('js/modules/beneficiaries.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/beneficiaries/beneficiariesmanagement.js') }}"></script>
@endsection

@section('content')
<div class="portlet blue box">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Beneficiaries Management </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
     <div class="portlet-body">
   <span id="hidden_fields">
            <input type="hidden" name="page_number" value="1" />
        </span>
        <div class="row">
            <div class="col-md-12" align="right">
                    <div class="btn-group pull-right blue">
                        <button class="btn blue btn-outline dropdown-toggle" data-toggle="dropdown">Actions
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="javascript:;" id="PRINT_LIST"> Print </a>
                            </li>
                            <li>
                                <a href="javascript:;" id="DELETE_BENEFICIARIES" > Delete </a>
                            </li>
                            </li>
                            <li>
                                <a href="javascript:;" id="EXPORT_BENEFICIARIES" > Export to Excel </a>
                            </li>
                        </ul>
                  </div>
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">&nbsp;</div>
        </div>
        <div class="row">
                 <div class="col-md-12">
                     <div style="left:20%">
                        <div class="portlet-body flip-scroll" id="LISTBENEFICIARIES">
                            <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
                                <thead class="flip-content">
                                <tr>
                                    <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_beneficiaries" id="CK_ALL_BENEFICIARIES" class="group-checkable" data-set="#LISTBENEFICIARIES .checkboxes" value="1" /></th>
                                    <th style="width:2px">#</th>
                                    <th>Beneficiary Parent </th>
                                    <th>Beneficiary Name </th>
                                    <th>Beneficiary Name In Arabic</th>
                                    <th>Telephone</th>
                                    <th>Fax</th>
                                    <th style="width:2px" nowrap>Edit</th>
                                    <th style="width:2px" nowrap>Delete</th>
                                </tr>
                                <tr>
                                    <th  class="table-checkbox" style="width:2px;"></th>
                                    <th style="width:2px">#</th>
                                    <th> </th>
                                    <th> <input type="text" class="form-control" name="sb_beneficiary_name" id="SB_BENEFICIARY_NAME" value="" /> </th>
                                    <th> <input type="text" class="form-control" name="sb_beneficiary_arabic_name" id="SB_BENEFICIARY_ARABIC_NAME" value="" /> </th>
                                    <th> <input type="text" class="form-control" name="sb_beneficiary_phone" id="SB_BENEFICIARY_PHONE" value="" /> </th>
                                    <th> <input type="text" class="form-control" name="sb_beneficiary_fax" id="SB_BENEFICIARY_FAX" value="" /> </th>
                                    <th colspan="2" nowrap></th>
                                </tr>

                            </thead>
                            <tbody  class="ListBenefciariesGird" >
                            </tbody>
                           </table>
                    </div>
                 </div>
             </div>
              <div class="row">
             <div class="col-md-10" align="left">
                <ul id="BenfPagination" class="pagination-sm"></ul>
             </div>
             <div class="col-md-2" align="right">

             </div>
         </div>
             <div class="row">
                 <div class="col-md-10"></div>
                 <div class="col-md-2" align="right">
                    <button  name="btn_add_BENEFICIARIES" id="BTN_ADD_BENEFICIARIES" type="button" class="btn blue capitalize" >ADD Beneficiary</button>
                 </div>
             </div>
     </div>
</div>
@endsection
<?php
/************************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

foreach ($beneficiaries as $index => $benf_info)
{
    ?>
       <tr data-benf_id="{{ $benf_info->sb_id }}">
           <td><input type="checkbox" class="checkboxes" name="ck_benf_{{ $benf_info->sb_id }}" id="CK_BENF_{{ $benf_info->sb_id }}" value="1" /></td>
           <td><?php echo $benf_info->sb_id; ?></td>
           <td><?php echo ($benf_info->fk_benf_id  > 0) ? $beneficiaries_array[ $benf_info->fk_benf_id ] : "N/A"; ?></td>
           <td><?php echo $benf_info->sb_name; ?></td>
           <td><?php echo $benf_info->sb_arabic_name; ?></td>
           <td><?php echo $benf_info->sb_telephone; ?></td>
           <td><?php echo $benf_info->sb_fax; ?></td>
           <td> <a href="#"  id="EDIT_BENF_{{ $benf_info->sb_id }}" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
           <td><a href="#"  id="DELETE_BENF_{{ $benf_info->sb_id }}" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
       </tr>
    <?php
}
?>
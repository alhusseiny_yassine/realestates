<?php
/* * **********************************************************
  addsurvey.blade.php
  Product :
  Version : 1.0
  Release : 0
  Date Created : Aug 9, 2017
  Developed By  : Alhusseiny Yassine  PHP Department Softweb S.A.R.L
  All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2017

  Page Description :
  view of display surveys section
 * ********************************************************** */
?>
@extends('layouts.alayout')

@section('themes')
<link href="{{ url('admin/assets/global/plugins/bootstrap-datetimepicker_1/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('admin/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />

@endsection
@section('plugins')
<script src="{{ url('admin/assets/global/plugins/moment/moment.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/moment/moment-with-locale.js') }}"></script>
<script src="{{ url('admin/assets/global/plugins/bootstrap-datetimepicker_1/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('js/modules/surveys.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/surveys/savesurvey.js') }}"></script>
@endsection
@section('content')
<div class="portlet blue box">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Add Survey </div>
    </div>
    <div class="portlet-body">
        <span id="hidden_fields">

        </span>
        <div class="row" style="height:15px;"><div class="col-md-12"></div></div>
        <div class="row">
            <div class="col-md-12">
                <div style="left:20%">
                    <div class="portlet-body flip-scroll">
                        <form name="form_save_survey" id="FORM_SAVE_SURVEY">
                            {!! csrf_field() !!}
                            <div class="alert alert-success" style="display:none">
                                <strong>Success!</strong> Survey Information is saved successfully!
                            </div>
                            <div class="alert alert-danger" style="display:none">
                                <strong>Error!</strong> You have some form errors. Please check below.
                            </div>
                            <div class="row">
                                <div class="col-md-4 form-group" id="listbeneficiaryprojects">
                                    <label class="control-label">Projects<span class="required"> * </span></label>
                                    <select data-type="projects" class="form-control selectpicker" name="fk_project_id" id="fk_project_id">
                                        <option value="" >Select</option>
                                        @foreach( $projects as $index => $project )
                                        <option value="{{ $project->pp_id }}" >{{ $project->pp_project_title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4 form-group" id="listprojectcourses">

                                </div>
                                <div class="col-md-4 form-group" id="coursetrainer">

                                </div>
                            </div>
                            <div class="row" style="height: 15px"></div>
                            <div class="row">
                                <div class="col-md-8"></div>
                                <div class="col-md-4" align="right">
                                    <button type="button" id="BACK_FORM" name="back_form" class="btn default">Back</button>
                                    <button name="add_survey" id="ADD_SURVEY" class="btn green capitalize"  type="submit">Add Survey</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
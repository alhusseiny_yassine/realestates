<?php
/* * **********************************************************
  addsurvey.blade.php
  Product :
  Version : 1.0
  Release : 0
  Date Created : Aug 9, 2017
  Developed By  : Alhusseiny Yassine  PHP Department Softweb S.A.R.L
  All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2017

  Page Description :
  view of display surveys section
 * ********************************************************** */
?>
@extends('layouts.alayout')

@section('themes')
@endsection
@section('plugins')
<script type="text/javascript" src="{{ url('js/modules/surveyquestions.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/surveys/savequestion.js') }}"></script>

@endsection
@section('content')
<div class="portlet blue box">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Add Question </div>
    </div>
    <div class="portlet-body">
        <span id="hidden_fields">

        </span>
        <div class="row" style="height:15px;"><div class="col-md-12"></div></div>
        <div class="row">
            <div class="col-md-12">
                <div style="left:20%">
                    <div class="portlet-body flip-scroll">
                        <form name="form_save_question" id="FORM_SAVE_QUESTION">
                            {!! csrf_field() !!}
                            <input type="hidden" id="survey_id" name="survey_id" value="{{ $survey_id }}" />
                            <div class="alert alert-success" style="display:none">
                                <strong>Success!</strong> Survey Information is saved successfully!
                            </div>
                            <div class="alert alert-danger" style="display:none">
                                <strong>Error!</strong> You have some form errors. Please check below.
                            </div>
                            <div class="row">
                                <div class="col-md-8 form-group">
                                    <label class="control-label">Question<span class="required"> * </span></label>
                                    <textarea name="sq_question" id="sq_question" rows="5" class="form-control" style="resize: none"></textarea>
                                </div>
                                <div class="col-md-2 form-group" >
                                    <label class="control-label">Order<span class="required"> * </span></label>
                                    <select class="form-control" name="sq_order" id="sq_order">
                                        <option value=""></option>
                                        @for( $i=1 ; $i<= $questions_count+1 ; $i++ )
                                        <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-md-2 form-group">
                                    <label class="control-label">Rating<span class="required"> * </span></label>
                                    <input class="form-control" name="sq_rating" id="sq_rating" type="number" max="10" min="0" />
                                </div>
                                </div>
                            <div class="row">
                                
                            </div>
                            <div class="row col-md-12" style="height: 15px"></div>
                            <div class="row">
                                <div class="col-md-10"></div>
                                <div class="col-md-2" >
                                    <button type="button" id="BACK_FORM" name="back_form" class="btn default">Back</button>
                                    <button name="add_question" id="ADD_QUESTION" class="btn green capitalize"  type="submit">Add Question</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
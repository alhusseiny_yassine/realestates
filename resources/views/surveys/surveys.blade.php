<?php
/************************************************************
surveys.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 9, 2017
Developed By  : Alhusseiny Yassine  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2017

Page Description :
view of display surveys section
************************************************************/

?>
@extends('layouts.alayout')

@section('themes')
<link href="{{ url('admin/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('plugins')
<script src="{{ url('admin/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootbox.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('js/modules/surveys.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/surveys/surveysmanagement.js') }}"></script>
@endsection
@section('content')
<div class="portlet blue box">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Surveys List </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
     <div class="portlet-body">
       
        <span id="hidden_fields">
            <input type="hidden" name="page_number" value="1" />
        </span>
        <div class="row" style="height:15px;"><div class="col-md-12"></div></div>
        <div class="row">
            <div class="col-md-12">
                <div style="left:20%">
                    <div class="portlet-body flip-scroll" id="LISTSURVEYS">
                        <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
                            <thead class="flip-content">
                                <tr>
                                    <th style="width:2px;">#</th>
                                    <th>Course</th>
                                    <th>Project</th>
                                    <th>Trainer</th>
                                    <th>Date</th>
                                    <th style="text-align: center">Questions</th>
                                    <th style="text-align: center">Answers</th>
                                    <th style="width:2px;" nowrap>Edit</th>
                                    <th style="width:2px;" nowrap>Delete</th>
                                </tr>
                            </thead>
                            <tbody class="ListSurveys">
                            </tbody>
                        </table>
                     </div>
                </div>
             </div>
         </div>
         <div class="row">
             <div class="col-md-10" align="left">
                <ul id="SurveysPagination" class="pagination-sm"></ul>
             </div>
             <div class="col-md-2" align="right">

             </div>
         </div>
         <div class="row">
             <div class="col-md-10"></div>
             <div class="col-md-2" align="right">
             <button name="btn_add_survey" id="BTN_ADD_SURVEY" class="btn green capitalize"  type="button">Add</button>
             </div>
         </div>
     </div>
</div>
@endsection
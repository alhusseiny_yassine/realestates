<?php
/************************************************************
answers.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 11, 2017
Developed By  : Alhusseiny Yassine  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2017

Page Description :
view of display answers section
************************************************************/

?>
@extends('layouts.alayout')

@section('themes')
@endsection
@section('plugins')
<script src="{{ url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootbox.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('js/modules/surveyquestions.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/surveys/answersmanagement.js') }}"></script>
@endsection
@section('content')
<div class="portlet blue box">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Answers List </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
     <div class="portlet-body">
       
        <span id="hidden_fields">
            <input type="hidden" name="page_number" value="1" />
            <input type="hidden" name="sq_id" id="sq_id" value="{{ $sq_id }}" />
        </span>
        <div class="row" style="height:15px;"><div class="col-md-12"></div></div>
        <div class="row">
            <div class="col-md-12">
                <div style="left:20%">
                    <div class="portlet-body flip-scroll" id="LISTANSWERS">
                        <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
                            <thead class="flip-content">
                                <tr>
                                    <th style="width:5%;">#</th>
                                    <th style="width:50%;">Answer</th>
                                    <th style="width:25%;">Trainee</th>
                                    <th style="width:20%;">Date</th>
                            </thead>
                            <tbody class="ListAnswers">
                            </tbody>
                        </table>
                     </div>
                </div>
             </div>
         </div>
         <div class="row">
             <div class="col-md-10" align="left">
                <ul id="AnswersPagination" class="pagination-sm"></ul>
             </div>
             <div class="col-md-2" align="right">

             </div>
         </div>
         <div class="row">
             <div class="col-md-10"></div>
             <div class="col-md-2" align="right">
             <button type="button" id="BACK_FORM" name="back_form" class="btn default">Back</button>
             </div>
         </div>
     </div>
</div>
@endsection
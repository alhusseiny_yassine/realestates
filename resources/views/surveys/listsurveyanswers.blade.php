<table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
    <thead class="flip-content">
        <tr>
            <th style="width:5%;">#</th>
            <th style="width:25%;">Trainee</th>
            <th style="width:20%;">View</th>
    </thead>
    <tbody class="ListSurveyAnswers">
        @foreach( $lst_trainees as $index => $trainee_info )
        @if( in_array( $trainee_info->id , $trainees_array ) )
        <tr  class="odd gradeX" data-trainee_id="{{ $trainee_info->id }}">
            <td>{{ $trainee_info->id }}</td>
            <td>{{ $trainee_info->u_fullname }}</td>
            <td style="text-align: center"><a name="view_questions_answers_{{ $trainee_info->id }}" id="VIEW_QUESTIONS_ANSWERS_{{ $trainee_info->id }}" data-survey_id="{{ $survey_id }}" href="#"><i class="fa fa-eye" aria-hidden="true" height="16" ></i></a></td>
        </tr>
        @endif
        @endforeach
    </tbody>
</table>
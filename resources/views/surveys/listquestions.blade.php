
@foreach( $questions as $index => $question_info )
<?php $remain_stars = 5 - $question_info->sq_rating?>
<tr  class="odd gradeX" data-question_id="{{ $question_info->sq_id }}">
    <td>{{ $question_info->sq_id }}</td>
    <td>{{ $question_info->sq_question }}</td>
    <td><div style="color: #2ab4c0;font-size: 18px">
            @for($i=1 ; $i <= $question_info->sq_rating ; $i++)
            <i class="fa fa-star" aria-hidden="true"></i>
            @endfor
            @for($i=1 ; $i <= $remain_stars ; $i++)
            <i class="fa fa-star-o" aria-hidden="true"></i>
            @endfor
        </div>
    </td>
    <td>{{ $question_info->sq_order }}</td>
    <td style="text-align: center"><a href="{{ url('/surveys/ViewAnswers/'.$question_info->sq_id ) }}"><i class="fa fa-eye" aria-hidden="true" height="16" ></i></a></td>
    <td style="text-align: center"><a href="{{ url('/surveys/EditQuestion/'.$question_info->sq_id ) }}"  id="EDIT_QUESTION_{{ $question_info->sq_id }}" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
    <td style="text-align: center"><a href="#"  id="DELETE_QUESTION_{{ $question_info->sq_id }}" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
</tr>
@endforeach

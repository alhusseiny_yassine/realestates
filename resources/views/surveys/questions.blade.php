<?php
/************************************************************
surveys.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 9, 2017
Developed By  : Alhusseiny Yassine  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2017

Page Description :
view of display surveys section
************************************************************/

?>
@extends('layouts.alayout')

@section('themes')
@endsection
@section('plugins')
<script src="{{ url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootbox.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('js/modules/surveyquestions.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/surveys/questionsmanagement.js') }}"></script>
@endsection
@section('content')
<div class="portlet blue box">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Questions List </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
     <div class="portlet-body">
       
        <span id="hidden_fields">
            <input type="hidden" name="page_number" value="1" />
            <input type="hidden" name="survey_id" id="survey_id" value="{{ $survey_id }}" />
        </span>
        <div class="row" style="height:15px;"><div class="col-md-12"></div></div>
        <div class="row">
            <div class="col-md-12">
                <div style="left:20%">
                    <div class="portlet-body flip-scroll" id="LISTQUESTIONS">
                        <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
                            <thead class="flip-content">
                                <tr>
                                    <th style="width:5%;">#</th>
                                    <th style="width:40%;">Question</th>
                                    <th style="width:10%;">Rating</th>
                                    <th style="width:5%;">Order</th>
                                    <th style="width:10%;text-align: center">View Answers</th>
                                    <th style="width:5%" nowrap>Edit</th>
                                    <th style="width:5%" nowrap>Delete</th>
                                </tr>
                            </thead>
                            <tbody class="ListQuestions">
                            </tbody>
                        </table>
                     </div>
                </div>
             </div>
         </div>
         <div class="row">
             <div class="col-md-10" align="left">
                <ul id="QuestionsPagination" class="pagination-sm"></ul>
             </div>
             <div class="col-md-2" align="right">

             </div>
         </div>
         <div class="row">
             <div class="col-md-10"></div>
             <div class="col-md-2" align="right">
             <button type="button" id="BACK_FORM" name="back_form" class="btn default">Back</button>
             <button name="btn_add_question" id="BTN_ADD_QUESTION" class="btn green capitalize"  type="button">Add</button>
             </div>
         </div>
     </div>
</div>
@endsection

@foreach( $answers as $index => $answer_info )
<tr  class="odd gradeX" data-question_id="{{ $answer_info->sa_id }}">
    <td>{{ $answer_info->sa_id }}</td>
    <td>{{ $answer_info->sa_answer }}</td>
    <td>{{ ( isset( $trainees[$answer_info->fk_trainee_id] ) ) ? $trainees[$answer_info->fk_trainee_id]->u_fullname : "N/A" }}</td>
    <td>{{ $answer_info->sa_date }}</td>
    </tr>
@endforeach

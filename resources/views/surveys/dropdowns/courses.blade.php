<label class="control-label">Courses<span class="required"> * </span></label>
<input type="hidden" id="selected_project_id" value="{{ $project_id }}">
<select data-type="courses" class="form-control selectpicker" name="fk_course_id" id="fk_course_id">
    <option value="" >Select</option>
    @foreach( $lst_courses as $index => $course )
    @if( in_array( $course->c_id , $courses_array ) )
    <option value="{{ $course->c_id }}" >{{ $course->c_course_name }}</option>
    @endif
    @endforeach
</select>
<script type="text/javascript">
    $(".selectpicker").selectpicker({
        liveSearch: "true"
    });
</script>
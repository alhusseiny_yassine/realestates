@if( empty($trainer) == false)
<label class="control-label">Trainer<span class="required"> * </span></label>
<input type="hidden" value="{{ $trainer->id }}" name="fk_trainer_id" id="fk_trainer_id">
<input type="text" disabled data-type="trainer" class="form-control" value="{{ $trainer->u_fullname }}" name="trainer" id="trainer">
@else
<label class="control-label">No Trainers Available</label>
@endif
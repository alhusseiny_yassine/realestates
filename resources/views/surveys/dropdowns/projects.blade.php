<label class="control-label">Projects<span class="required"> * </span></label>
<select data-type="projects" class="form-control selectpicker" name="fk_project_id" id="fk_project_id">
    <option value="" >Select</option>
    @foreach( $lst_projects as $index => $project )
    @if( in_array( $project->pp_id , $projects_array ) )
    <option value="{{ $project->pp_id }}" >{{ $project->pp_project_title }}</option>
    @endif
    @endforeach
</select>
<script type="text/javascript">
    $(".selectpicker").selectpicker({
        liveSearch: "true"
    });
</script>
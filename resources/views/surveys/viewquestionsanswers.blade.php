<table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
    <thead class="flip-content">
        <tr>
            <th style="width:40%;text-align: left">Question</th>
            <th style="width:60%;">Answer</th>
    </thead>
    <tbody class="ListSurveyAnswers">
        @foreach( $survey_questions as $index => $question )
        <tr  class="odd gradeX">
            <td style="text-align: left">{{ $question->sq_question }}</td>
            <td>{{ ( isset( $survey_answers[$question->sq_id] ) ) ? $survey_answers[$question->sq_id]->sa_answer : '' }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
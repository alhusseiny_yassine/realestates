<?php
/************************************************************
addrole.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
View for add new role form
************************************************************/

?>


@extends('layouts.alayout')

@section('plugins')
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/modules/roles.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/roles/addrole.js') }}"></script>
@endsection
@section('content')

<div class="portlet blue box">
<div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Add Role </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
    <div class="portlet-body">
         <form name="form_add_role" id="FORM_ADD_ROLE">
    <div class="form-body">
         <span id="hidden_fields">
            {!! csrf_field() !!}
        </span>
        <div class="alert alert-success" style="display:none">
				<strong>Success!</strong> Role Information is saved successfully!
			</div>
			<div class="alert alert-danger" style="display:none">
				<strong>Error!</strong> You have some form errors. Please check below.
			</div>
        <div class="row">
            <div class="col-md-9">
                 <div class="form-group">
                    <label class="control-label">Role Name <span class="required"> * </span></label>
                    <input type="text" name="role_name" id="ROLE_NAME" class="form-control" required="required"  value="" />
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
       <div class="row" style="height:5px;"></div>
       <div class="row">
            <div class="col-md-9">
                 <div class="form-group">
                    <label class="control-label">Role Description</label>
                    <textarea style="width:100%;height:150px;resize:none" name="role_description" id="ROLE_DESCRIPTION"  class="form-control"></textarea>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
        <div class="row" style="height:5px;"></div>
        <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12" style="white-space: nowrap;">
                        <div class="portlet-body">
        					<h3>Privileges</h3>
        					<div class="tabbable-line">
        						<ul class="nav nav-tabs ">
        						     <?php
        						      foreach ($pa_result_array as $tabTitle => $value)
        						      {
        						          ?>
        						              <li class="<?php echo str_replace(" ", "", $tabTitle) == 'SystemManagement' ? 'active' : ""; ?>">
                								<a href="#<?php echo str_replace(" ", "", $tabTitle); ?>" data-toggle="tab">
                								<?php echo $tabTitle; ?></a>
                							</li>
        						          <?php
        						      }

        						     ?>

        						</ul>
        						<div class="tab-content">
        						      <?php foreach ($pa_result_array as $tabTitle => $pa_info){ ?>
        						      <div class="tab-pane <?php echo str_replace(" ", "", $tabTitle) == 'SystemManagement' ? 'active' : ""; ?>" id="<?php echo str_replace(" ", "", $tabTitle); ?>">

                                      <div class="portlet-body flip-scroll" id="LISTPRIVILEGES">
                                        <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
                                            <thead class="flip-content">
                                                <tr>
                                                    <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_privileges" id="CK_ALL_PRIVILEGES" class="group-checkable" data-set="#LISTPRIVILEGES .checkboxes" value="1" /></th>
                                                    <th>Description</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                                        foreach ($pa_info as $index => $pa_priv_info ) {
                                                           ?>
                                                            <tr>
                                                               <td style="width:3%;">
                                                                   <input type="checkbox" name="<?php echo $pa_priv_info['code']; ?>" id="<?php echo strtoupper($pa_priv_info['code']); ?>" value="1" />
                                                               </td>
                                                               <td style="width:97%;"><?php echo $pa_priv_info['description']; ?></td>
                                                            </li>
                                                           <?php
                                                        }
                                                   ?>
                                            </tbody>
                                        </table>
                                      </div>
                                       </div>
                                      <?php } ?>
        						</div>
        					</div>
        			     </div>
                    </div>
                </div>
        <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-md-12" align="right"> 
                 <button type="submit" name="btn_add_role" id="BTN_ADD_ROLE"  class="btn blue">Save</button>
                <button type="button" id="BACK_FORM" name="back_form" class="btn default">Back</button>
            </div>
        </div>
    </div>
</form>
    </div>
</div>


@endsection
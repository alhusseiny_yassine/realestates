<?php
/************************************************************
roles.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Page of roles management where we can add/edit and delete roles
************************************************************/

?>

@extends('layouts.alayout')

@section('plugins')
<script src="{{ url('admin/assets/global/plugins/bootbox.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('js/modules/roles.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/roles/rolesmanagement.js') }}"></script>
@endsection

@section('content')
<div class="portlet blue box">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Roles Management </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
     <div class="portlet-body">
        <div class="row">
            <div class="col-md-12" align="right">
                    <div class="btn-group pull-right blue">
                        <button class="btn blue btn-outline dropdown-toggle" data-toggle="dropdown">Actions
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="javascript:;" id="PRINT_LIST"> Print </a>
                            </li>
                            <li>
                                <a href="javascript:;" id="DELETE_ROLES" > Delete </a>
                            </li>
                            </li>
                            <li>
                                <a href="javascript:;" id="EXPORT_TO_EXCELL"> Export to Excel </a>
                            </li>
                        </ul>
                  </div>
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">&nbsp;</div>
        </div>
        <div class="row">
                 <div class="col-md-10"></div>
                 <div class="col-md-2" align="right">
                    <button  name="btn_add_roles" id="BTN_ADD_ROLES" type="button" class="btn blue capitalize" >ADD</button>
                 </div>
             </div>
             <div class="row">
            <div class="col-md-12">&nbsp;</div>
        </div>
        <div class="row">
                 <div class="col-md-12">
                     <div style="left:20%" class="ListRoleGirds">

                    </div>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-10"></div>
                 <div class="col-md-2" align="right">
                    <button  name="btn_add_roles" id="BTN_ADD_ROLES_BOTTOM" type="button" class="btn blue capitalize" >ADD</button>
                 </div>
             </div>
     </div>
</div>
@endsection
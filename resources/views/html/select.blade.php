<?php
/***********************************************************
select.blade.php
 Product : 
 Version : 1.0
 Release : 2
 Date Created :  Aug 18, 2017
 Developed By  : Mohamad Mantach   PHP Department
 All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2017
 
 Page Description :
 {Enter page description Here}
 ***********************************************************/
?>
<label>Project : </label>
<select name="{{ $name }}" id="{{ $id }}" class="form-control" style="width:100%">
    <option value="">--Select One--</option>
     @foreach($projects_dropdown_array as $id => $value)
        <option value="{{ $id }}">{{ $value }}</option>
     @endforeach
</select>
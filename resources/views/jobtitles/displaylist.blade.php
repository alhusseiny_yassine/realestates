<?php
/************************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

foreach ($jobtitles as $index => $jt_info)
{
    ?>
       <tr data-jt_id="{{ $jt_info->jt_id }}">
           <td><input type="checkbox" class="checkboxes" name="ck_jobtitle_{{ $jt_info->jt_id }}" id="CK_JOBTITLE_{{ $jt_info->jt_id }}" value="1" /></td>
           <td><?php echo $jt_info->jt_id; ?></td>
           <td><?php echo $jt_info->jt_job_title; ?></td>
           <td> <a href="#"  id="EDIT_JOBTITLE_{{ $jt_info->jt_id }}" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
           <td><a href="#"  id="DELETE_JOBTITLE_{{ $jt_info->jt_id }}" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
       </tr>
    <?php
}
?>
<?php
/***********************************************************
listimagesSlider.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Jan 25, 2016
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/


?>

@extends('layouts.alayout',['show_menu' => '0'])

@section('plugins')
    <script type="text/javascript" src="<?php echo url('/'); ?>/js/sliders/listimagesSlider.js"></script>
@endsection
@section('content')
<div class="portlet box green">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gears"></i>
			<span class="caption-subject uppercase"> List Slider Images</span>
		</div>
		<div class="tools">
			<a href="javascript:;" class="collapse" data-original-title="" title="">
			</a>
		</div>
	</div>
    <div class="portlet-body">
         <div class="row">
            <div class="col-md-2">
                <select name="slc_action" id="SLC_ACTION" class="form-control" style="width:200px;">
                    <option value="0">--Select One--</option>
                    <option value="delete_image">Delete Items</option>
                   <!--  <option value="edit_image">Edit Items</option> -->
                </select>
            </div>
            <div class="col-md-10"></div>
         </div>
         <div class="row"><div class="col-md-12">&nbsp;</div></div>
        <div class="row">
            <div class="col-md-12">
                <div class="container-fluid" style="height: 100%;background-color: white;" align="center">
                    <span id="hidden_fields">
                        <input type="hidden" name="cms_slide_id" id="CMS_SLIDE_ID" value="{{ $slide_id }}" />
                    </span>
                    <div class="ListAlbumsGrid">

                    </div>
                </div>
            </div>
        </div>
        <div class="row"><div class="col-md-12" align="right">
            <button type="button" class="btn green" name="btn_add_images" id="BTN_ADD_IMAGES">Add Images</button>
        </div></div>
    </div>
</div>
@endsection
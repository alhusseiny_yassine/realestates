<?php
/***********************************************************
editSliderForm.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Jan 23, 2016
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/

?>


@extends('layouts.playout')

@section('plugins')
<script type="text/javascript" src="<?php echo url('/'); ?>/assets/js/sliders/editSliderForm.js"></script>
@endsection


@section('content')
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-cogs font-green-sharp"></i>
			<span class="caption-subject font-green-sharp bold uppercase">Edit Slider</span>
		</div>
		<div class="tools">
			<a href="javascript:;" class="collapse" data-original-title="" title="">
			</a>
		</div>
	</div>
	<div class="portlet-body">
	   <form name="form_edit_slider_form" id="FORM_EDIT_SLIDER_FORM">
                <span id="hidden_fields">
                     {!! csrf_field() !!}
                     <input type="hidden" name="cs_id" value="{{ $slide_info[0]->cs_id }}" />
                </span>
                <div class="container" style="height: 90%;width:100%;" align="center">
                   <div class="row" style="height:14px;"></div>
                     <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" style="float:left">Slider Name<span class="required">*</span></label>
                                    <input type="text" name="cs_slider_title" id="CS_SLIDER_TITLE" style="width:100%;" class="form-control" value="{{ $slide_info[0]->cs_slider_title }}" />
                                </div>
                           </div>
                        </div>
                        <div class="row" style="height:14px;"></div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" style="float:left">Slider Description</label>
                                    <div style="clear: both;"></div>
                                    <textarea style="width:100%;height:250px;resize:none" name="cs_slider_description" id="CS_SLIDER_DESCRIPTION" class="form-control"  >{{ $slide_info[0]->cs_slider_descrition }}</textarea>
                                </div>
                            </div>
                        </div>
                    <div class="row" style="height:14px;"></div>
                    <div class="row">
                        <div class="col-md-12" align="right">
                            <button type="submit" name="btn_edit_slider" id="BTN_EDIT_SLIDER" class="btn green" >Edit Slider</button>
                        </div>
                    </div>
                </div>
            </form>
	</div>
</div>
@endsection
<?php
/***********************************************************
EditImageSlider.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Jan 25, 2016
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/

?>


@extends('layouts.alayout')

@section('themes')
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<!-- blueimp Gallery styles -->
<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="{{ url('js/libraries/jquery_file_upload/css/jquery.fileupload.css') }}">
<link rel="stylesheet" href="{{ url('js/libraries/jquery_file_upload/css/jquery.fileupload-ui.css') }}">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="{{ url('js/libraries/jquery_file_upload/css/jquery.fileupload-noscript.css') }}"></noscript>
<noscript><link rel="stylesheet" href="{{ url('js/libraries/jquery_file_upload/css/jquery.fileupload-ui-noscript.css') }}"></noscript>
@endsection
@section('plugins')
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="{{ url('js/libraries/jquery_file_upload/js/vendor/jquery.ui.widget.js') }}"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<!-- blueimp Gallery script -->
<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="{{ url('js/libraries/jquery_file_upload/js/jquery.iframe-transport.js') }}"></script>
<!-- The basic File Upload plugin -->
<script src="{{ url('js/libraries/jquery_file_upload/js/jquery.fileupload.js') }}"></script>
<!-- The File Upload processing plugin -->
<script src="{{ url('js/libraries/jquery_file_upload/js/jquery.fileupload-process.js') }}"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="{{ url('js/libraries/jquery_file_upload/js/jquery.fileupload-image.js') }}"></script>
<!-- The File Upload audio preview plugin -->
<script src="{{ url('js/libraries/jquery_file_upload/js/jquery.fileupload-audio.js') }}"></script>
<!-- The File Upload video preview plugin -->
<script src="{{ url('js/libraries/jquery_file_upload/js/jquery.fileupload-video.js') }}"></script>
<!-- The File Upload validation plugin -->
<script src="{{ url('js/libraries/jquery_file_upload/js/jquery.fileupload-validate.js') }}"></script>
<!-- The File Upload user interface plugin -->
<script src="{{ url('js/libraries/jquery_file_upload/js/jquery.fileupload-ui.js') }}"></script>
<script type="text/javascript" src="<?php echo url('/'); ?>/js/libraries/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo url('/'); ?>/js/libraries/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="<?php echo url('/'); ?>/js/sliders/editImagelider.js"></script>
@endsection
@section('content')

<form name="form_edit_ia_info" id="FORM_EDIT_IA_INFO">
<span id="hidden_fields">
     {!! csrf_field() !!}
     <input type="hidden" name="cm_id" id="CM_ID" value="<?php echo $cms_media_display[0]->cm_id; ?>" />
     <input type="hidden" name="slider_id" id="SLIDER_ID" value="<?php echo $cms_media_display[0]->fk_slider_id; ?>" />
</span>
    <div class="container" style="height: 100%;width:100%;" align="center">
         <div class="row">
            <div class="col-md-12"><h3>Edit Slide Image Content</h3></div>
        </div>
       <div class="row" style="height:14px;"></div>
         <div class="row">
            <div class="col-md-12">
                <table cellspacing="0" cellpadding="0" style="width:100%">
                    <tr>
                        <td align="left"><h4>Image Title</h4></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <input type="text" value="<?php echo $cms_media_display[0]->cm_media_title; ?>" name="cm_media_title" id="CM_IAMGE_TITLE" style="width:100%;" class="form-control" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row" style="height:14px;"></div>
        <div class="row">
            <div class="col-md-12">
                <table cellspacing="0" cellpadding="0" style="width:100%">
                    <tr>
                        <td align="left"><h4>Image Caption </h4></td>
                    </tr>
                    <tr>
                        <td align="left">
                          <textarea style="width:100%;height:250px;" name="cm_media_caption" id="CM_MEDIA_CAPTION" class="form-control" ><?php echo $cms_media_display[0]->cm_media_caption; ?></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row" style="height:14px;"></div>
         <div class='row'>
        <div class="col-md-12" style="text-align: left;">
            <span class="btn btn-success fileinput-button">
                <i class="glyphicon glyphicon-plus"></i>
                <span>Select files...</span>
                <!-- The file input field used as target for the file upload widget -->
                <input id="fileupload" data-url="{{ url('upload/SliderImages') }}" type="file" name="files" />
            </span>
            <br>
            <br>
            <!-- The global progress bar -->
            <div id="progress" class="progress">
                    <div class="progress-bar progress-bar-success"></div>
            </div>
            <img src="<?php echo $link; ?>" width="100" ID="MEDIA_PICTURE" />
        </div>
    </div>
        <div class="row" style="height:14px;"></div>
        <div class="row">
            <div class="col-md-12" align="right"><input type="button" name="btn_edit_image_album" id="BTN_EDIT_IMAGE_ALBUM" class="btn btn-info" value="SAVE" /> </div>
        </div>
    </div>
</form>
@endsection

<?php
/***********************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Jan 23, 2016
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/

?>

<table id="ListSliders" class="table table-bordered table-striped table-condensed flip-content" style="width:100%">
    <thead>
        <tr>
            <th style="width: 2px;" class="table-checkbox">
				<input type="checkbox" class="group-checkable" data-set="#ListSliders .checkboxes"/>
			</th>
            <th style="width: 2px;">#</th>
            <th>Title</th>
            <th>Images</th>
            <th style="width:2px" nowrap>Edit</th>
            <th style="width:2px" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
       @for($i = 0; $i < count($lst_sliders); $i++)
               <tr data-cs_id="{{ $lst_sliders[$i]->cs_id }}">
                    <td><input type="checkbox" class="checkboxes" value="1"/></td>
                   <td>{{ $lst_sliders[$i]->cs_id }}</td>
                   <td>{{ $lst_sliders[$i]->cs_slider_title }}</td>
                   <td><a target="_blank" href="{{ url('administrator/page/slideimages/' . $lst_sliders[$i]->cs_id ) }}">Images</a></td>
                   <td> <a href="#"  id="EDIT_SLIDE_{{ $lst_sliders[$i]->cs_id }}" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
                   <td><a href="#"  id="DELETE_SLIDE_{{ $lst_sliders[$i]->cs_id }}" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
               </tr>
        @endfor
    </tbody>
</table>
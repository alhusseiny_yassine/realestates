<?php
/***********************************************************
imagesSliderUploader.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Jan 25, 2016
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/
?>



@extends('layouts.alayout')

@section('themes')
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<!-- blueimp Gallery styles -->
<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="{{ url('js/libraries/jquery_file_upload/css/jquery.fileupload.css') }}">
<link rel="stylesheet" href="{{ url('js/libraries/jquery_file_upload/css/jquery.fileupload-ui.css') }}">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="{{ url('js/libraries/jquery_file_upload/css/jquery.fileupload-noscript.css') }}"></noscript>
<noscript><link rel="stylesheet" href="{{ url('js/libraries/jquery_file_upload/css/jquery.fileupload-ui-noscript.css') }}"></noscript>
@endsection
@section('plugins')
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="{{ url('js/libraries/jquery_file_upload/js/vendor/jquery.ui.widget.js') }}"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<!-- blueimp Gallery script -->
<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="{{ url('js/libraries/jquery_file_upload/js/jquery.iframe-transport.js') }}"></script>
<!-- The basic File Upload plugin -->
<script src="{{ url('js/libraries/jquery_file_upload/js/jquery.fileupload.js') }}"></script>
<!-- The File Upload processing plugin -->
<script src="{{ url('js/libraries/jquery_file_upload/js/jquery.fileupload-process.js') }}"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="{{ url('js/libraries/jquery_file_upload/js/jquery.fileupload-image.js') }}"></script>
<!-- The File Upload audio preview plugin -->
<script src="{{ url('js/libraries/jquery_file_upload/js/jquery.fileupload-audio.js') }}"></script>
<!-- The File Upload video preview plugin -->
<script src="{{ url('js/libraries/jquery_file_upload/js/jquery.fileupload-video.js') }}"></script>
<!-- The File Upload validation plugin -->
<script src="{{ url('js/libraries/jquery_file_upload/js/jquery.fileupload-validate.js') }}"></script>
<!-- The File Upload user interface plugin -->
<script src="{{ url('js/libraries/jquery_file_upload/js/jquery.fileupload-ui.js') }}"></script>
<script type="text/javascript" src="{{ url('js/sliders/imagesSliderUploader.js') }}"></script>
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview" id="DISPLAY_FORM_{%=file.cm_id%}" data-cm_id="{%=file.cm_id%}">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}"  title="{%=file.name%}" download="{%=file.name%}" data-gallery><img height="100px" src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}?_token={%=$('input[name=_token]').val()%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<script type="text/x-tmpl" id="form_template">
    <input type="hidden" name="cm_id" id="CM_ID" value="{%=o.cm_id%}" />
    <div class="row">
        <div class="col-md-12">
            <label>Image Title</label>
            <span><input type="text" name="cm_media_title" id="CM_MEDIA_TITLE" style="width:100%;" value="{%=o.cm_media_title%}" class="form-control" /></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            &nbsp;
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label>Image Caption</label>
            <span>
                <textarea class="form-control" name="cm_media_caption" id="CM_MEDIA_CREATION" style="width:100%;height:300px;resize:none" >{%=o.cm_media_caption%}</textarea>
            </span>
        </div>
    </div>
    <div class="row">
        <div align="right" class="col-md-12">&nbsp;</div>
    </div>
    <div class="row">
        <div align="right" class="col-md-12">
            <button type="button" name="btn_save_info" id="BTN_SAVE_INFO" class="btn btn-info" >Save</button>
        </div>
    </div>
</script>
@endsection
@section('content')
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-cogs font-green-sharp"></i>
			<span class="caption-subject font-green-sharp bold uppercase">Add Images </span>
		</div>
		<div class="tools">
			<a href="javascript:;" class="collapse" data-original-title="" title="">
			</a>
		</div>
	</div>
	<div class="portlet-body">
	       <form name="form_add_si_info" id="fileupload" method="POST" enctype="multipart/form-data" action="<?php echo url('/'); ?>/uploads/SliderImages" >
                <noscript><input type="hidden" name="redirect" value="<?php echo url('/'); ?>/uploads/SliderImages"></noscript>
                <span id="hidden_fields">
                     {!! csrf_field() !!}
                     <input type="hidden" name="slider_id" id="SLIDER_ID" value="<?php echo $slide_id; ?>" />
                </span>
                    <div class="container" style="height: 100%;width:100%;" align="center">
                         <div class="row fileupload-buttonbar">
                            <div class="col-lg-12">
                				<span class="btn btn-success fileinput-button">
                                    <i class="glyphicon glyphicon-plus"></i>
                                    <span>Add files...</span>
                                    <input id="file_input"   type="file" name="files[]" multiple>
                                </span>
                                <button type="submit" class="btn btn-primary start">
                                    <i class="glyphicon glyphicon-upload"></i>
                                    <span>Start upload</span>
                                </button>
                                <button type="reset" class="btn btn-warning cancel">
                                    <i class="glyphicon glyphicon-ban-circle"></i>
                                    <span>Cancel upload</span>
                                </button>
                                <button type="button" class="btn btn-danger delete">
                                    <i class="glyphicon glyphicon-trash"></i>
                                    <span>Delete</span>
                                </button>
                                <input type="checkbox" class="toggle">
                                <!-- The global file processing state -->
                                <span class="fileupload-process"></span>
                                
                			</div>
                        </div>
                       <div class='row'>
                         <!-- The global progress bar -->
                            <div class="col-lg-12 fileupload-progress fade">
                                <!-- The global progress bar -->
                                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                </div>
                                <!-- The extended global progress state -->
                                <div class="progress-extended">&nbsp;</div>
                            </div>
                        </div>
                        <div class='FormInformation' id="FORM_INFORMATION"></div>
                        <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
                       <div class="row" style="height:14px;"></div>
                        <div class="row">
                            <div class="col-md-12" align="right"><!-- <input type="button" name="btn_save_album_images" id="BTN_SAVE_ALBUM_IMAGES" class="btn btn-info" value="SAVE" /> --> </div>
                        </div>
                    </div>
                </form>

	</div>
</div>
@endsection
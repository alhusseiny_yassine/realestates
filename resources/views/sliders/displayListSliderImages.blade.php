<?php
/***********************************************************
diayListSliderImages.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Jan 25, 2016
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/

?>

<table class="table table-bordered table-striped table-condensed flip-content" style="width:100%">
    <thead>
        <tr>
            <th style="width:2px;"><input type="checkbox" name="ck_all_media" id="CK_ALL_MEDIA" value="1" /></th>
            <th style="width:72px;">Image</th>
            <th>Image Title</th>
            <th style="width:2%" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        for ($i = 0; $i < count($lstCmsMedia); $i++)
        {
            ?>
               <tr data-cm_id="<?php echo $lstCmsMedia[$i]->cm_id; ?>">
                   <td><input type="checkbox" name="a_media_<?php echo $lstCmsMedia[$i]->cm_id; ?>" id="A_MEDIA_<?php echo $lstCmsMedia[$i]->cm_id; ?>" value="1" /></td>
                   <td><img  src="<?php echo url('/') . "/" . Config::get( 'constants.ALBUMS_PATH') . $lstCmsMedia[$i]->cm_media_base_dir . $lstCmsMedia[$i]->cm_media_file_name  . "." . $lstCmsMedia[$i]->cm_media_file_extention ; ?>" width="72" /></td>
                   <td><?php echo $lstCmsMedia[$i]->cm_media_title; ?></td>
                   <td><a href="#"  id="DELETE_IMAGE_<?php echo $lstCmsMedia[$i]->cm_id; ?>" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>

<?php
/***********************************************************
slidermanagement.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 15, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/


?>




@extends('layouts.alayout')

@section('plugins')
<script src="{{ url('js/colorbox/jquery.colorbox.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('js/sliders/listsliderspage.js') }}"></script>
@endsection
@section('themes')
<link href="{{ url('js/colorbox/example4/colorbox.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="portlet box green">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gears"></i>
			<span class="caption-subject uppercase"> Slider Management Management </span>
		</div>
		<div class="tools">
			<a href="javascript:;" class="collapse" data-original-title="" title="">
			</a>
		</div>
	</div>
	<div class="portlet-body">
	       <div class="container-fluid" style="height: 100%;background-color: white;" align="center">
                 <div class="row">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-8"></div>
                    <div class="col-md-2">
                    </div>
                 </div>
                 <div class="row"><div class="col-md-12">&nbsp;</div></div>
                 <div class="row">
                     <div class="col-md-12">
                         <div style="width:100%" class="ListSlidersGrid"></div>
                     </div>
                 </div>
                 <div class="row">
                     <div class="col-md-10"></div>
                     <div class="col-md-2" align="right">
                         <button type="button" class="btn green" name="btn_add_sliders" id="BTN_ADD_LIDERS" >ADD SLIDER</button>
                     </div>
                 </div>
            </div>
	</div>
</div>
@endsection
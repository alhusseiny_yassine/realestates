<?php
/************************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>
<div class="portlet-body flip-scroll" id="LISTREQUESTSTATUSES">
    <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
        <thead class="flip-content">
        <tr>
            <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_requeststatuses" id="CK_ALL_REQUESTSTATUSES" class="group-checkable" data-set="#LISTREQUESTSTATUSES .checkboxes" value="1" /></th>
            <th style="width:2px">#</th>
            <th>Request Status Name </th>
            <th>Request Status color</th>
            <th style="width:2px" nowrap>Edit</th>
            <th style="width:2px" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($request_statuses as $index => $request_status_info)
        {
            ?>
               <tr data-rs_id="{{ $request_status_info->rs_id }}">
                   <td><input type="checkbox" class="checkboxes" name="ck_request_status_{{ $request_status_info->rs_id }}" id="CK_REQUEST_STATUS_{{ $request_status_info->rs_id }}" value="1" /></td>
                   <td><?php echo $request_status_info->rs_id; ?></td>
                   <td><?php echo $request_status_info->rs_status_name; ?></td>
                   <td style="background-color:<?php echo $request_status_info->rs_status_color; ?>;color:white;"><?php echo $request_status_info->rs_status_color; ?></td>
                   <td> <a href="#"  id="EDIT_REQUEST_STATUS_{{ $request_status_info->rs_id }}" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
                   <td><a href="#"  id="DELETE_REQUEST_STATUS_{{ $request_status_info->rs_id }}" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>
</div>
<?php
/************************************************************
displaylistcustomers.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 29, 2017
Developed By  : Alhusseiny PHP Department A&H S.A.R.L

Page Description :
--
************************************************************/
?>
@foreach( $customers as $index => $customer )
<tr  class="odd gradeX" data-c_id="{{ $customer->c_id }}">
    <td>{{ $customer->c_id }}</td>
    <td>{{ $customer->c_fullname }}</td>
    <td>{{ $customer->c_email }}</td>
    <td>{{ $customer->c_mobile }}</td>
    <td>
        <a href="javascript:;" id="EDIT_CUSTOMER_{{ $customer->c_id }}" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a>
    </td>
    <td>
        <a href="javascript:;" id="DELETE_CUSTOMER_{{ $customer->c_id }}" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a>
    </td>
</tr>
@endforeach
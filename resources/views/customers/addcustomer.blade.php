<?php
/* * **********************************************************
  addcustomer.blade.php
  Product :
  Version : 1.0
  Release : 0
  Date Created : Aug 29, 2017
  Developed By  : Alhusseiny Yassine PHP Department A&H S.A.R.L

  Page Description :
  View of add customer form
 * ********************************************************** */
?>

@extends('layouts.alayout')

@section('themes')
@endsection
@section('plugins')
<script type="text/javascript" src="{{ url('js/modules/customers.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/customers/savecustomer.js') }}"></script>
@endsection
@section('content')
<div class="portlet blue box">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-users"></i> Add Customer 
        </div>
    </div>
    <div class="portlet-body">
        <form name="form_save_customer" id="FORM_SAVE_CUSTOMER">
            <div  class="form-body">
                <span id="hidden_fields">{!! csrf_field() !!}</span>
                <div class="alert alert-success" style="display:none">
                    <strong>Success!</strong> Customer Information is saved successfully!
                </div>
                <div class="alert alert-danger" style="display:none">
                    <strong>Error!</strong> You have some form errors. Please check below.
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Full Name <span class="required"> * </span></label>
                            <input type="text" maxlength="500" name="c_fullname" id="c_fullname" class="form-control" value="" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Email <span class="required"> * </span></label>
                            <input type="email" maxlength="255" name="c_email" id="c_email" class="form-control" value="" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Mobile <span class="required"> * </span></label>
                            <input type="text" maxlength="255" name="c_mobile" id="c_mobile" class="form-control" value="" />
                        </div>
                    </div>
                </div>
                <div class="row" style="height:5px;"></div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label> Customer Type </label>
                            <select class="bs-select form-control" name="c_customer_type" id="c_customer_type" data-actions-box="true">
                                <option value='renter'>Renter</option>
                                <option value='buyer'>Buyer</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Phone </label>
                            <input type="text" maxlength="255" name="c_phone" id="c_phone" class="form-control" value="" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Fax </label>
                            <input type="text" maxlength="255" name="c_fax" id="c_fax" class="form-control" value="" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="control-label">Address</label>
                            <textarea style="width:100%;height: 100px;resize:none" class="form-control" name="c_address" id="c_address"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row" style="height:5px;"></div>
                <div class="row">
                    <div class="col-md-9"></div>
                    <div class="col-md-3" align="right">
                        <button type="submit" id="BTN_SAVE_CUSTOMER" name="btn_save_customer" class="btn blue capitalize">Save</button>&nbsp;&nbsp;<button type="button" id="BACK_FORM" name="back_form" class="btn default">Back</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
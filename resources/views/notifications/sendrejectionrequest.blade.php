<?php
/***********************************************************
sendrejectionrequestb.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 7, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

?>


<div class="portlet blue box" style="height: 100%;width:500px;">
<div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Send Rejection Request </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
    <div class="portlet-body">
        <form name="frm_rejection_request" id="FRM_REJECTION_REQUEST">
            <span id="hidden_fields">
                  {!! csrf_field() !!}
                  <input type="hidden" name="request_ids" value="{{ $request_ids }}" />
            </span>
            <div class="row">
                 <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">Reason Of Rejection</label>
                        <textarea name="r_rejection_comment" rows="10" style="width:100%;resize:none;" class="form-control"  ></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" name="btn_reject_request" id="BTN_REJECT_REQUEST" class="btn btn-info" >Reject Request </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
/***********************************************************
lstnotifications.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Apr 27, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
Display List of all notifications with time
***********************************************************/

?>


@extends('layouts.layout')

@section('content')


<ul class="list-group">
	 <a href="#" class="list-group-item active">
    Notifications
  </a>
  @foreach($LstNotifications as $notif)
  <li class="list-group-item">{!! $notif->nt_notification !!}<span class="badge">{{ \Carbon\Carbon::parse($notif->nt_notification_date)->diffForHumans() }}</span></li>
  @endforeach
  @foreach($RqstNotifications as $notif)
  <li class="list-group-item">{!! $notif->nr_notification_text !!}<span class="badge">{{ \Carbon\Carbon::parse($notif->nr_notification_date)->diffForHumans() }}</span></li>
  @endforeach
</ul>



@endsection
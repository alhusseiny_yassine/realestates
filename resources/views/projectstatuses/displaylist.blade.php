<?php
/************************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>
<div class="portlet-body flip-scroll" id="LISTPROJECTSTATUSES">
    <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
        <thead class="flip-content">
        <tr>
            <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_projectstatuses" id="CK_ALL_PROJECTSTATUSES" class="group-checkable" data-set="#LISTPROJECTSTATUSES .checkboxes" value="1" /></th>
            <th style="width:2px">#</th>
            <th>Project Status Name </th>
            <th>Project Status color</th>
            <th style="width:2px" nowrap>Edit</th>
            <th style="width:2px" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($project_statuses as $index => $project_status_info)
        {
            ?>
               <tr data-ps_id="{{ $project_status_info->ps_id }}">
                   <td><input type="checkbox" class="checkboxes" name="ck_project_status_{{ $project_status_info->ps_id }}" id="CK_PROJECT_STATUS_{{ $project_status_info->ps_id }}" value="1" /></td>
                   <td><?php echo $project_status_info->ps_id; ?></td>
                   <td><?php echo $project_status_info->ps_status_name; ?></td>
                   <td style="background-color:<?php echo $project_status_info->ps_status_color; ?>;color:white;"><?php echo $project_status_info->ps_status_color; ?></td>
                   <td> <a href="#"  id="EDIT_PROJECT_STATUS_{{ $project_status_info->ps_id }}" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
                   <td><a href="#"  id="DELETE_PROJECT_STATUS_{{ $project_status_info->ps_id }}" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>
</div>
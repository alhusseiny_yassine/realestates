<?php
/***********************************************************
activation.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 30, 2016
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2016

Page Description :
Activation Email
***********************************************************/

?>


<div style="width:100%;margin-top:30px;margin-bottom:30px">
<b>Dear {{ $account_name }}</b><br/>
thank you for Signing up ! <br/>
Please Click the following link to activate your account<br/>
<a href="{{ url('/users/activate/' . $activation_key ) }}">Activate Your Account</a>
</div>
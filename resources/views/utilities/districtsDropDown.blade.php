<?php
/***********************************************************
districtsDropDown.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 23, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad MantachCOPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


?>

<label class="control-label">District</label>
<select name="fk_district_id" id="FK_DISTRICT_ID" class="form-control" style="width:100%">
    <option value="">--Select One--</option>
     @foreach($districts_data as $sd_index => $sd_info)
        <option value="{{ $sd_info['sd_id'] }}">{{ $sd_info['sd_district_name'] }}</option>
     @endforeach
</select>
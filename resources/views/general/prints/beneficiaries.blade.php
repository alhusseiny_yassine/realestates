<?php
/***********************************************************
users.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 23, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
Display List of users to print
***********************************************************/

?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title> List Benficiaries </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="OMSAR" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN LAYOUT FIRST STYLES -->
        <link href="//fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
        <!-- END LAYOUT FIRST STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="{{ url('admin/assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('admin/assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{ url('admin/assets/global/css/components-md.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ url('admin/assets/global/css/plugins-md.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{ url('admin/assets/layouts/layout/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('admin/assets/layouts/layout/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
                <script src="{{ url('admin/assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('admin/assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
            <script type="text/javascript">
        $(function(){
            window.print();
        })
        </script>
    </head>
    <!-- END HEAD -->

    <body class="page-md" style="background: none !important">
        <div class="row">
            <div class="col-md-12" align="center">
               <h3> Benficiaries List</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="portlet-body flip-scroll">
                    <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
                        <thead class="flip-content">
                            <tr>
                                <th style="width:2px;">ID</th>
                                <th>Beneficiary Code</th>
                                <th>Beneficiary Name</th>
                                <th>Beneficiary Name In Arabic</th>
                                <th>Telephone</th>
                                <th>Fax</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($lst_benficiaries as $index => $benf_info)
                            {

                                ?>
                                   <tr  class="odd gradeX">
                                       <td><?php echo $benf_info->sb_id; ?></td>
                                       <td><?php echo $benf_info->sb_code; ?></td>
                                       <td><?php echo $benf_info->sb_name; ?></td>
                                       <td><?php echo $benf_info->sb_arabic_name; ?></td>
                                       <td><?php echo $benf_info->sb_telephone; ?></td>
                                       <td><?php echo $benf_info->sb_fax; ?></td>
                                   </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
            </div>
        </div>
        <div class="col-md-2"></div>
    </body>
</html>
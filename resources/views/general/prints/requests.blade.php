<?php
/***********************************************************
requests.blade.php
Product :
Version : 1.0
Release : 2
Developed By  : Alhusseiny PHP Department

Page Description :
Display List of requests to print
***********************************************************/

?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title> Request Status </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="OMSAR" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN LAYOUT FIRST STYLES -->
        <link href="//fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
        <!-- END LAYOUT FIRST STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="{{ url('admin/assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('admin/assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{ url('admin/assets/global/css/components-md.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ url('admin/assets/global/css/plugins-md.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{ url('admin/assets/layouts/layout/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('admin/assets/layouts/layout/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
                <script src="{{ url('admin/assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('admin/assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
            <script type="text/javascript">
        $(function(){
            window.print();
        })
        </script>
    </head>
    <!-- END HEAD -->

    <body class="page-md" style="background: none !important">
        <div class="row">
            <div class="col-md-12" align="center">
               <h3> Request Status</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="portlet-body flip-scroll">
                    <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
                        <thead class="flip-content">
                            <tr>
                                <th style="width:2px;">ID</th>
                                <th>Request Status Name</th>
                                <th>Request Status Color</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($lst_request_status as $index => $status_info) {
                                ?>
                                <tr>
                                    <td><?php echo $status_info->rs_id; ?></td>
                                    <td><?php echo $status_info->rs_status_name; ?></td>
                                    <td style="background-color: <?php echo $status_info->rs_status_color ?>;-webkit-print-color-adjust: exact;  "><?php echo $status_info->rs_status_color ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
            </div>
        </div>
        <div class="col-md-2"></div>
    </body>
</html>
<?php
/* * *********************************************************
  privileges.blade.php
  Product :
  Version : 1.0
  Release : 2
  Developed By  : Alhusseiny PHP Department

  Page Description :
  Display List of roles to print
 * ********************************************************* */
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title> List Role Privileges </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="OMSAR" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN LAYOUT FIRST STYLES -->
        <link href="//fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
        <!-- END LAYOUT FIRST STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="{{ url('admin/assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('admin/assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{ url('admin/assets/global/css/components-md.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ url('admin/assets/global/css/plugins-md.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{ url('admin/assets/layouts/layout/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('admin/assets/layouts/layout/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <script src="{{ url('admin/assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('admin/assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <style>
            .green {
                color: green;
            }
            .red {
                color: red;
            }
        </style>
    </head>
    <!-- END HEAD -->

    <body class="page-md" style="background: none !important">
        <div class="row">
            <div class="col-md-12" align="center">
                <h3> Role Privileges List ( {!! $roles->role_name !!} )</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="portlet-body flip-scroll">
                    <?php foreach ($pa_result_array as $tabTitle => $pa_info) { ?>
                        <h4><?php echo str_replace(" ", "", $tabTitle); ?></h4>
                        <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
                            <thead class="flip-content">
                                <tr>
                                    <th style="width: 90%;text-align: left">Description</th>
                                    <th style="width: 10%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($pa_info as $index => $pa_priv_info) {
                                    ?>
                                    <tr>
                                        <td style="text-align: left" ><?php echo $pa_priv_info['description'] ?></td>
                                        <td style="text-align: center"><i class="fa <?php echo ( count($rp_result_array) > 0 && isset($rp_result_array[$pa_priv_info['code']]) && $rp_result_array[$pa_priv_info['code']] == 'allow' ) ? "fa-check green" : "fa-times red"; ?>" aria-hidden="true"></i></td>

                                    </tr>
                                    <?php
                                }
                                ?>
                                    </tbody>
                    </table>
                                <?php
                            }
                            ?>
                        
                </div>
            </div>
            <div class="col-md-2"></div>
    </body>
</html>
<script type="text/javascript">
$(function () {
    window.print();
})
</script>
<?php
/***********************************************************
days.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 22, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

?>

<select name="sl_days" id="SL_DAYS" class="form-control">
<option value="1" {{ $selected_day == 1 ? "selected" : "" }} >Monday</option>
<option value="2" {{ $selected_day ==2 ? "selected" : "" }} >Tuesday</option>
<option value="3" {{ $selected_day ==3 ? "selected" : "" }}  >Wenesday</option>
<option value="4" {{ $selected_day ==4 ? "selected" : "" }}  >Thursday</option>
<option value="5" {{ $selected_day ==5 ? "selected" : "" }} >Friday</option>
<option value="6" {{ $selected_day ==6 ? "selected" : "" }}  >Saturday</option>
<option value="7" {{ $selected_day ==7 ? "selected" : "" }}  >Sunday</option>
</select>
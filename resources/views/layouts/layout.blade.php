<?php
/***********************************************************
layout.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 30, 2016
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2016

Page Description :
Layout of Front end Application
***********************************************************/

$role_info = session()->get('role_info');
/**if( ( isset($role_info['om_course_management_tab'])  && $role_info['om_course_management_tab'] == 'deny') || ( !isset( $role_info['om_course_management_tab'] ) ) )
{

}*/

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title> OMSAR - Training Management System - Office of the Minister of the State For Administrative Reform  </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content=" OMSAR - Training Management System - Office of the Minister of the State For Administrative Reform" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="{{ url('admin/assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('admin/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('admin/assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{ url('admin/assets/global/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('admin/assets/global/plugins/mapplic/mapplic/mapplic.css') }}" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{ url('admin/assets/global/css/components-md.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ url('admin/assets/global/css/plugins-md.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{ url('admin/assets/layouts/front.layout/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('admin/assets/layouts/front.layout/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> 
              @yield("themes")
			<style>
                .bootbox-alert{
	                   top:40%;
                	   left:20%;
                }
            </style>
        </head>
      
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-md">
      <span id="hidden_fields">
        <input type="hidden" name="base_url" id="BASE_URL" value="{{ url('/') }}" />
        <input type="hidden" name="user_id"  id="USER_ID"  value="{{ Session('ut_user_id') }} ">
        {!! csrf_field() !!}
    </span>
        <!-- BEGIN HEADER -->
        <div class="page-header navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="clearfix">
                <!-- BEGIN BURGER TRIGGER -->
                <div class="burger-trigger">
                    <button class="menu-trigger">
                        <img src="{{ url('admin/assets/layouts/front.layout/img/m_toggler.png') }}" alt=""> </button>
                    <div class="menu-overlay menu-overlay-bg-transparent">
                        <div class="menu-overlay-content">
                            <ul class="menu-overlay-nav text-uppercase">
                                <li>
                                    <a href="{{ url('omsar/dashboard') }}">Dashboard</a>
                                </li>
                                @if(isset( $role_info['om_course_management_tab'] ) && $role_info['om_course_management_tab'] == 'allow')
                                <li>
                                    <a href="{{ url('omsar/courses') }}">Courses</a>
                                </li>
                                @endif
                                @if(isset( $role_info['om_requests_management'] ) && $role_info['om_requests_management'] == 'allow')
                                <li>
                                    <a href="{{ url('omsar/requests') }}">Request</a>
                                </li>
                                @endif
                                @if(isset( $role_info['om_project_management'] ) && $role_info['om_project_management'] == 'allow')
                                <li>
                                    <a href="{{ url('omsar/projects') }}"> Projects </a>
                                </li>
                                @endif
                                @if(isset( $role_info['om_groups_management'] ) && $role_info['om_groups_management'] == 'allow')
                                <li>
                                    <a href="{{ url('omsar/groups') }}"> Groups </a>
                                </li>
                                @endif
                                @if(isset( $role_info['om_add_edit_sessions_management'] ) && $role_info['om_add_edit_sessions_management'] == 'allow')
                                <li>
                                    <a href="{{ url('omsar/sessions') }}"> Sessions </a>
                                </li>
                                @endif
                                @if(isset( $role_info['om_classes_management_section'] ) && $role_info['om_classes_management_section'] == 'allow')
                                <li>
                                    <a href="{{ url('omsar/classes') }}"> Classes </a>
                                </li>
                                 @endif
                                 @if(isset( $role_info['om_trainers_management'] ) && $role_info['om_trainers_management'] == 'allow')
                                <li>
                                    <a href="{{ url('trainers/trainersManagement') }}"> Traineers </a>
                                </li>
                                @endif
                                @if(isset( $role_info['om_course_management_tab'] ) && $role_info['om_course_management_tab'] == 'allow')
                                <li>
                                    <a href="{{ url('project/RunningCourse') }}">Running Courses</a>
                                </li>
                                @endif
                                @if(isset( $role_info['om_class_trainees_attendances'] ) && $role_info['om_class_trainees_attendances'] == 'allow')
                                <li>
                                    <a href="{{ url('trainer/TraineesAttendances') }}">Trainees Attendances</a>
                                </li>
                                @endif
                                @if(isset( $role_info['om_class_test_result'] ) && $role_info['om_class_test_result'] == 'allow')
                                <li>
                                    <a href="{{ url('trainer/TraineesTestResults') }}">Trainees Test Results</a>
                                </li>
                                @endif
                                <li>
                                    <a href="{{ url('users/notifications') }}">Notifications</a>
                                </li>
                                 @if(isset( $role_info['om_view_trainees_attendances'] ) && $role_info['om_view_trainees_attendances'] == 'allow')
                                <li>
                                    <a href="{{ url('omsar/TraineesAttendances') }}">Attendences </a>
                                </li>
                                @endif
                                @if(isset( $role_info['om_view_trainees_test_result'] ) && $role_info['om_view_trainees_test_result'] == 'allow')
                                <li>
                                    <a href="{{ url('omsar/TraineesTestResults') }}">Test Results </a>
                                </li>
                                @endif
                                
                                 @if (Session('ut_user_type')==4)
                                 <li>
                                    <a href="{{ url('account/myclasses') }}">My Classes </a>
                                </li>
                                 @endif
                            </ul>
                        </div>
                    </div>
                    <div class="menu-bg-overlay">
                        <button class="menu-close">&times;</button>
                    </div>
                    <!-- the overlay element -->
                </div>
                <!-- END NAV TRIGGER -->
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="{{ url('/') }}">
                        <img src="{{ url('imgs/logo.jpg') }}" alt="Office of the Minister of the State For Administrative Reform"  class="logo-default" />  </a>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <!-- BEGIN NOTIFICATION DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                            <a href="javascript:;" style="display:none" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="icon-bell"></i>
                                <span class="badge badge-success"> 7 </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="external">
                                    <h3>
                                        <span class="bold" id='count_notif'>12</span> pending notifications</h3>
                                    <a href="{{ url('users/notifications') }}">view all</a>
                                </li>
                                <li>
                                    <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">just now</span>
                                                <span class="details">
                                                    <span class="label label-sm label-icon label-success">
                                                        <i class="fa fa-plus"></i>
                                                    </span> New user registered. </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">3 mins</span>
                                                <span class="details">
                                                    <span class="label label-sm label-icon label-danger">
                                                        <i class="fa fa-bolt"></i>
                                                    </span> Server #12 overloaded. </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">10 mins</span>
                                                <span class="details">
                                                    <span class="label label-sm label-icon label-warning">
                                                        <i class="fa fa-bell-o"></i>
                                                    </span> Server #2 not responding. </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">14 hrs</span>
                                                <span class="details">
                                                    <span class="label label-sm label-icon label-info">
                                                        <i class="fa fa-bullhorn"></i>
                                                    </span> Application error. </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">2 days</span>
                                                <span class="details">
                                                    <span class="label label-sm label-icon label-danger">
                                                        <i class="fa fa-bolt"></i>
                                                    </span> Database overloaded 68%. </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">3 days</span>
                                                <span class="details">
                                                    <span class="label label-sm label-icon label-danger">
                                                        <i class="fa fa-bolt"></i>
                                                    </span> A user IP blocked. </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">4 days</span>
                                                <span class="details">
                                                    <span class="label label-sm label-icon label-warning">
                                                        <i class="fa fa-bell-o"></i>
                                                    </span> Storage Server #4 not responding dfdfdfd. </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">5 days</span>
                                                <span class="details">
                                                    <span class="label label-sm label-icon label-info">
                                                        <i class="fa fa-bullhorn"></i>
                                                    </span> System Error. </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">9 days</span>
                                                <span class="details">
                                                    <span class="label label-sm label-icon label-danger">
                                                        <i class="fa fa-bolt"></i>
                                                    </span> Storage server failed. </span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <!-- END NOTIFICATION DROPDOWN -->
                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <li class="dropdown dropdown-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <div class="dropdown-user-inner">
                                    <span class="username username-hide-on-mobile"> {{ Session('ut_user_fullname') }} </span>
                                    <img alt="" id="MAIN_PROFILE_PIC" src="{{ Session('ut_user_profile_url') }}" /> </div>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <a href="{{ url('omsar/myprofile') }}">
                                        <i class="icon-user"></i> My Profile </a>
                                </li>
                                <li>
                                    <a href="{{ url('omsar/mycalendar') }}">
                                        <i class="icon-calendar"></i> My Calendar </a>
                                </li>
                                @if (Session('ut_user_type')==4)
                                <li>
                                    <a href="{{ url('account/myclasses') }}">
                                        <i class="icon-envelope-open"></i> My Classes
                                        <!--<span class="badge badge-danger"> 3 </span>-->
                                    </a>
                                </li>
                                @endif
                                @if (Session('ut_user_type')==5)
                                <li>
                                    <a href="{{ url('omsar/mysessions') }}">
                                        <i class="icon-envelope-open"></i> Daily Sessions
                                        <!--<span class="badge badge-danger"> 3 </span>-->
                                    </a>
                                </li>
                                @endif
                                <li style="display: none">
                                    <a href="page_todo.html">
                                        <i class="icon-rocket"></i> My Tasks 
                                        <span class="badge badge-success"> 7 </span>
                                    </a>
                                </li>
                  

                                <li class="divider"> </li>
                                <li>
                                    <a href="{{ url('omsar/logout') }}">
                                        <i class="icon-key"></i> Log Out </a>
                                </li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container page-content-inner page-container-bg-solid">
            <!-- BEGIN BREADCRUMBS -->
            <!-- Remove "hide" class from "breadcrumbs hide" DIV and "margin-top-30" class from below "container-fluid container-lf-space margin-top-30" DIV in order to use the page breadcrumbs -->
            <div class="breadcrumbs hide">
                <div class="container-fluid">
                    <h2 class="breadcrumbs-title">Dashboard</h2>
                    <ol class="breadcrumbs-list">
                        <li>
                            <a class="breadcrumbs-item-link" href="#">Home</a>
                        </li>
                        <li>
                            <a class="breadcrumbs-item-link" href="#">Features</a>
                        </li>
                        <li>
                            <a class="breadcrumbs-item-link" href="#">Dashboard</a>
                        </li>
                    </ol>
                </div>
            </div>
            <!-- END BREADCRUMBS -->
            <!-- BEGIN CONTENT -->
            <div class="FrontContainer container-fluid container-lf-space margin-top-30" >
                <!-- BEGIN PAGE BASE CONTENT -->
                @yield("content")
                <!-- END PAGE BASE CONTENT -->
            </div>
            <!-- END CONTENT -->
            <div id="ModelPopUp"></div>
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner container-fluid container-lf-space">
                <p class="page-footer-copyright">  Copyright &copy; {{ date('Y') }}
                   <a target="_blank" href="http://omsar.gov.lb">OMSAR</a> &nbsp;|&nbsp; Office of the Minister of the State For Administrative Reform
                </p>
            </div>
            <div class="go2top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->

        <!--[if lt IE 9]>
        <script src="{{ url('admin/assets/global/plugins/respond.min.js') }}"></script>
        <script src="{{ url('admin/assets/global/plugins/excanvas.min.js') }}"></script>
        <script src="{{ url('admin/assets/global/plugins/ie8.fix.min.js') }}"></script>
        <![endif]-->
        <script src="{{ url('admin/assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('admin/assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('admin/assets/global/plugins/bootbox.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('admin/assets/global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('admin/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('admin/assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('admin/assets/global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('admin/assets/global/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>
        <script src="{{ url('admin/assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('admin/assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('admin/assets/global/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('admin/assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('admin/assets/pages/scripts/dashboard.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('admin/assets/layouts/front.layout/scripts/layout.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('admin/assets/layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('admin/assets/layouts/global/scripts/quick-nav.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('admin/assets/global/scripts/general.js') }}" type="text/javascript"></script>
    <script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
    <script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>
        @yield("plugins")
        <script type="text/javascript" src="http://code.jquery.com/color/jquery.color-2.1.2.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
        <script type="text/javascript">

            
        </script>

        <script type="text/javascript">
        
        /**  const socket = new WebSocket('ws://localhost:3000');

        // Connection opened
         socket.addEventListener('open', function (event) {
            // socket.send('Hello Server!');
         });

        // Listen for messagesalert('');

        socket.addEventListener('message', function (event) {


            console.log('Message from server '+event.data);
            msg=JSON.parse(event.data);
            console.log(msg.UserID+" "+$('#USER_ID').val());
            if(msg.UserID==$('#USER_ID').val()){
                datee=moment(msg.Datee).format('MM D YYYY h:mm:ss');
                console.log(datee);
                console.log(moment(datee).fromNow());
                //Add new List Item with the data received 
                $('.dropdown-menu-list li:eq(0)').before('<li class="New"> <a href="javascript:;"> <span class="time">'+moment(datee).fromNow()+'</span>  <span class="details">   <span class="label label-sm label-icon label-danger">   <i class="fa fa-bolt"></i>  </span> '+msg.NotificationText.substr(0,20)+'... </span>  </a><input type="hidden" id="NR_ID" value="'+msg.nr_ID+'" </input></li>');
                $('.dropdown-menu-list li:eq(0)').css({ 'background-color':'red'});
                console.log(parseInt($('.badge-success').html())+1);
                $('.badge-success').html(parseInt($('.badge-success').html())+1);
                $('#count_notif').html($('.badge-success').html());
            }
        });


         $(function(){
                 //Red Animation of the new notifications
                $('.dropdown-toggle').on('click',function(){
                    //console.log()
                   // $('.New').css('background', '#ffffff').fadeIn(2000);
                    $('.New').animate({backgroundColor: "#ffffff"},3000,function(){
                        console.log($('.New').css('background-color'));
                        $(this).removeClass('New');
                    });

                    //Delete clicked on notificaion
                    $('.dropdown-menu-list li').on('click',function(){
                         socket.send($(this).find('#NR_ID').val());
                         $(this).remove();
                         $('.badge-success').html(parseInt($('.badge-success').html())-1);
                         $('#count_notif').html($('.badge-success').html());
                         console.log($(this).find('#NR_ID').val());
                    })
                })
            })


       
      $(document).ready(function() {
        $('.dropdown-toggle').click(function(){
            console.log("OK");
            console.log($('.dropdown-menu-list li').css('background-color'));
            // if($('.dropdown-menu-list li').css('background-color')=="rgb(255, 0, 0)"){
            //     console.log('detected red');
            //      $('.dropdown-menu-list li').animate({backgroundColor: "#ffffff" }, 4000);
            // }
             $('.dropdown-menu-list li').filter(function() {
                    return $(this).css('background-color') == 'rgb(255, 0, 0)';
             }).each(function() {
                    console.log('detected red');
                    console.log($(this));
                    $(this).animate({display: "none" }, 100);
                });
        });
        });*/
    </script>

    </body>
</html>
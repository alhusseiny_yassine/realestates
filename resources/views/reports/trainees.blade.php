<?php
/************************************************************
users.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 6, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
view of display usermanagement section
************************************************************/

?>
@extends('layouts.alayout')

@section('themes')
<link href="{{ url('admin/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('plugins')
<script src="{{ url('admin/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootbox.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('js/modules/users.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/users/usersmanagement.js') }}"></script>
<style>
img{
	display:none;
}
a[id*=EDIT_USER_]{
	display:none;
}
a[id*=DELETE_USER_]{
	display:none;
}

</style>
@endsection
@section('content')
<div class="portlet blue box">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> User Management </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
     <div class="portlet-body">
       
        <span id="hidden_fields">
            <input type="hidden" name="page_number" value="1" />
            <input type="hidden" name="fb_user_type" value="4" />
        </span>
        
        <div class="row">
            <div class="col-md-3">
                <label>Beneficiaries</label>
                <select class="bs-select form-control" name="u_beneficiary" id="U_BENEFICIARY">
                   <option value="">All Beneficiary</option>
                    @foreach( $beneficiaries as $index => $benf_info )
                        <option value="{{ $benf_info->sb_id }}">{{ $benf_info->sb_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-3"></div>
        </div>
        <div class="row" style="height:15px;"><div class="col-md-12"></div></div>
         
        <div class="row">
             <div class="col-md-12">&nbsp;</div>
         </div>
        <div class="row">
            <div class="col-md-12">
                <div style="left:20%">
                    <div class="portlet-body flip-scroll" id="LISTUSERS">
                        <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
                            <thead class="flip-content">
                                <tr>
                                    <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_users" id="CK_ALL_USERS" class="group-checkable" data-set="#LISTUSERS .checkboxes" value="1" /></th>
                                    <th style="width:2px;">#</th>
                                    <th>Email</th>
                                    <th>Full Name</th>
                                    <th>beneficiary</th>
                                    <th></th>
                                    <th style="width:2px;" nowrap></th>
                                    <th style="width:2px;" nowrap></th>
                                </tr>
                                <tr>
                                    <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_users" id="CK_ALL_USERS" class="group-checkable" data-set="#LISTUSERS .checkboxes" value="1" /></th>
                                    <th style="width:2px;"></th>
                                    <th><input type="text" class="form-control" name="u_search_username" id="u_search_username" value="" /></th>
                                    <th><input type="text" class="form-control" name="u_search_fullname" id="u_search_fullname" value="" /></th>
                                    <th></th>
                                    <th></th>
                                    <th style="width:2px;" nowrap></th>
                                    <th style="width:2px;" nowrap></th>
                                </tr>
                            </thead>
                            <tbody class="ListUserGirds">
                            </tbody>
                        </table>
                     </div>
                </div>
             </div>
         </div>
         <div class="row">
             <div class="col-md-10" align="left">
                <ul id="UsersPagination" class="pagination-sm"></ul>
             </div>
             <div class="col-md-2" align="right">

             </div>
         </div>
          
     </div>
</div>
@endsection
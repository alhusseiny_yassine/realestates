<?php
/***********************************************************
providers.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Mar 2, 2017
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

?>

@extends('layouts.alayout')

@section('themes')
<link href="{{ url('admin/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
<style>
img{
	display:none;
}
a[id*=EDIT_USER_]{
	display:none;
}
a[id*=DELETE_USER_]{
	display:none;
}

</style>
@endsection
@section('plugins')
<script src="{{ url('admin/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootbox.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('js/modules/users.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/users/usersmanagement.js') }}"></script>
 
@endsection
@section('content')
<div class="portlet blue box">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> List Coordinators </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
     <div class="portlet-body">
       
        <span id="hidden_fields">
            <input type="hidden" name="page_number" value="1" />
            <input type="hidden" name="fb_user_type" id="FB_USER_TYPE" value="2" />
        </span>
        <div class="row">
             <div class="col-md-12">&nbsp;</div>
         </div>
        <div class="row">
            <div class="col-md-12">
                <div style="left:20%">
                    <div class="portlet-body flip-scroll" id="LISTUSERS">
                        <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
                            <thead class="flip-content">
                                <tr>
                                    <th style="width:2px;">#</th>
                                    <th style="width:2px;">#</th>
                                    <th>Email</th>
                                    <th>Full Name</th> 
                                    <th>beneficiaries</th> 
                                    <th></th> 
                                    <th></th> 
                                    <th></th> 
                                </tr>
                                <tr>
                                    <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_users" id="CK_ALL_USERS" class="group-checkable" data-set="#LISTUSERS .checkboxes" value="1" /></th>
                                    <th style="width:2px;"></th>
                                    <th><input type="text" class="form-control" name="u_search_username" id="u_search_username" value="" /></th>
                                    <th><input type="text" class="form-control" name="u_search_fullname" id="u_search_fullname" value="" /></th>
                                      <th></th> 
                                    <th></th> 
                                    <th></th> 
                                    <th></th> 
                                </tr>
                            </thead>
                            <tbody class="ListUserGirds">
                            </tbody>
                        </table>
                     </div>
                </div>
             </div>
         </div>
         <div class="row">
             <div class="col-md-10" align="left">
                <ul id="UsersPagination" class="pagination-sm"></ul>
             </div>
             <div class="col-md-2" align="right">

             </div>
         </div>
     </div>
</div>
@endsection
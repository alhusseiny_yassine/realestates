<?php
/* * *********************************************************
  invoice_pdf.blade.php
  Product :
  Version : 1.0
  Release : 1
  Developed By  : alhusseiny PHP Department

  Page Description :
  {Enter page description Here}
 * ********************************************************* */
?>
<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }

    th, td {
        text-align: left;
        padding: 15px 5px;
        border: 1px solid lightblue;
        text-align: center;
    }

    tr.even {
        background-color: #f2f2f2
    }
    tr.odd {
        background-color: white
    }

    th {
        background-color: #3598dc;
        color: white;
    }
    .header{
        width: 100%;
        background-color: #1c2329;
        padding: 20px;
        text-align: center;
        color: white;
    }
    html{
        font-family :'Verdana';
    }
    footer{
        text-align: center;
        position: absolute;
        bottom: 0;
        padding: 10px;
        width: 100%
    }
    .highlight{
        color: lightblue;
    }
    .left{
        width: 50%;
        float: left;
    }
    .right{
        width: 50%;
        float: right;
    }
    .content{
        height: 50px;
        margin: 30px 0px;
    }
</style>

<div class="header">
    <h2>
        Invoice # {{ $invoice->ii_invoice_code }}
    </h2>
</div>
<div>
    <div>
        <div class="content">
            <div style="text-align: center">
                Date: {{ date("F j, Y") }}
            </div>
            <br>
            <div class="">
                <div>
                    <b>Office of the Minister of State for Administrative Reform</b><br>
                    <b>REP# {{ $invoice->ii_invoice_ref }}</b>  -  <b>Loan 495/2006</b><br>
                </div>
            </div><br>
        </div>

        <div>
            <div>
                <table>
                    <thead>
                        <tr role="row">
                            <th > Project ID </th>
                            <th > Status </th>
                            <th > Price </th>
                        </tr>
                    </thead>
                    <tbody> 
                        <tr class="">
                            <td>{{ $invoice_details[0]->fk_project_id }}</td>
                            <td>{{ $invoice_details[0]->id_item_status }}</td>
                            <td>{{ $invoice_details[0]->id_item_price }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <footer>
        <div> Copyright © 2017 <span class="highlight">OMSAR</span>  |  Office of the Minister of the State For Administrative Reform </div>
    </footer>
<?php
/* * *********************************************************
  lump_sum_invoice.blade.php
  Product :
  Version : 1.0
  Release : 2
  Date Created :  Aug 20, 2017
  Developed By  : Mohamad Mantach   PHP Department
  All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2017

  Page Description :
  Display Invoice of Lump Sum  For Current Project
 * ********************************************************* */
$format = new NumberFormatter("en", NumberFormatter::SPELLOUT);
?>
<style>
    .invoice-box{
        max-width:800px;
        margin:auto;
        padding:30px;
        border:1px solid #eee;
        box-shadow:0 0 10px rgba(0, 0, 0, .15);
        font-size:16px;
        font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color:#555;
    }

    .invoice-box table{
        width:100%;
        text-align:left;
    }

    .invoice-box table td{
        padding:5px;
        vertical-align:top;
    }

    .invoice-box table tr td:nth-child(3){
        text-align:right;
    }

    .invoice-box table tr.top table td{
        padding-bottom:20px;
    }

    .invoice-box table tr.top table td.title{
        font-size:20px;
        background:#dbf2f9;
        color:#166a83;
    }

    .invoice-box table tr.information table td{
        padding-bottom:40px;
    }

    .invoice-box table tr.heading td{
        background:#166a83;
        font-weight:bold;
        line-height: 20px;
        color: white;
    }

    .invoice-box table tr.details td{
        padding-bottom:20px;
    }

    .invoice-box table tr.item td{
        line-height: 20px;
    }

    .invoice-box table tr.item.last td{
        border-bottom:none;
    }

    .invoice-box table tr.total td:nth-child(2){
        font-weight:bold;
    }

    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td{
            width:100%;
            display:block;
            text-align:center;
        }

        .invoice-box table tr.information table td{
            width:100%;
            display:block;
            text-align:center;
        }
    }
    .right{
        float: right
    }
    footer{
        position: absolute;
        bottom: 10%;
        font-size: 16px;
        color: #166a83;
    }
    .total_footer{
        position: absolute;
        bottom: 15%;
        font-size: 16px;
        color: #166a83;
    }
    .darkblue{
        background-color: #166a83;
        color:white;
    }
    .lightblue{
        background-color: #dbf2f9;
        color: #166a83;
    }
    .border_dark{
        border-bottom: double 5px #166a83;
        text-align: right
    }
</style>
@if( $is_pdf == 1 )
<?php $count = 10; $count2 = 4; ?>
@else
<?php $count = 5; $count2 = 3; ?>
@endif
<div class="row">
    <input type="hidden" name="invoice_total" id="invoice_total" value="{{ $pp_project_budget }}">
    <input type="hidden" name="invoice_code" id="invoice_code" value="{{ $invoice_code }}">
    <input type="hidden" name="invoice_ref" id="invoice_ref" value="{{ $invoice_ref }}">
</div>
<div class="invoice-box" style="height: 90%">
    <table cellpadding="0" cellspacing="0">
        <tr class="top">
            <td colspan="3">
                <table>
                    <tr>
                       <td class="title" style="width:60%" rowspan="2">
                            Office of the Minister of <br>
                            State for <br>
                            Administrative Reform
                        </td>
                        <td class=" darkblue">Invoice #: {{ $invoice_code }}<br>Created: {{ date("F j, Y") }}<br></td>
                    </tr>
                    <tr>
                        <td class=" darkblue">
                            Loan 495/2006.<br>
                            REP# {{ $invoice_ref }}<br>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        @for( $i = 1 ; $i<$count ; $i++)
        <tr class="information">
            <td  colspan="3">
            </td>
        </tr>
        @endfor
        <tr class="heading">
            <td>Project Name</td>
            <td>Project Status</td>
            <td>Price</td>
        </tr>
        <tr class="item">
            <td>{{ $project_title }}</td>
            <td> {{ $status }}</td>
            <td>{{ $pp_project_budget }} $</td>
        </tr>
        @for( $i = 1 ; $i<=$count2 ; $i++)
        <tr style="" class="item lightblue">
            <td></td>
            <td></td>
            <td></td>
        </tr>
        @endfor
        @for( $i = 1 ; $i<=$count2 ; $i++)
        <tr style="" class="item">
            <td></td>
            <td></td>
            <td></td>
        </tr>
        @endfor
        @for( $i = 1 ; $i<=$count2 ; $i++)
        <tr style="" class="item lightblue">
            <td></td>
            <td></td>
            <td></td>
        </tr>
        @endfor
        @for( $i = 1 ; $i<=$count2 ; $i++)
        <tr style="" class="item">
            <td></td>
            <td></td>
            <td></td>
        </tr>
        @endfor
        @for( $i = 1 ; $i<=$count2 ; $i++)
        <tr style="" class="item lightblue">
            <td></td>
            <td></td>
            <td></td>
        </tr>
        @endfor
        <tr class="total">
            <td></td>
            <td style='text-align:right'>Total : </td>
            <td class='border_dark'>{{ $pp_project_budget }} $</td>
        </tr>
    </table>
    <label class="total_footer"> <p><b> Total : {{ $format->format($pp_project_budget)  }} USD Dollars </b></p></label>
    <footer>Thank you for your business!</footer>
</div>
@if( $is_pdf != 1 )
<div>
    <div class="row">
        <div class="col-md-8"></div>
        <div class="col-md-4" align="right">
            <button type="button" id="BTN_GENERATE_INVOICE" name="btn_generate_invoice" class="btn blue capitalize">Generate Invoice</button>
        </div>
    </div>
</div>
@endif

<?php
/* * *********************************************************
  customers_pdf.blade.php
  Product :
  Version : 1.0
  Release : 1
  Developed By  : alhusseiny PHP Department

  Page Description :
  {Enter page description Here}
 * ********************************************************* */

?>
<head>
<link rel="shortcut icon" href="{{ url('/imgs/favicon.png') }}" />
</head>
<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }

    th, td {
        text-align: left;
        padding: 15px 5px;
        border: 1px solid lightblue;
        text-align: center;
    }

    tr.even {
        background-color: #f2f2f2
    }
    tr.odd {
        background-color: white
    }

    th {
        background-color: #3598dc;
        color: white;
    }
    .header{
        width: 100%;
        background-color: #1c2329;
        padding: 20px;
        text-align: center;
        color: white;
    }
    html{
        font-family :'Verdana';
    }
    footer{
        text-align: center;
        position: absolute;
        bottom: 0;
        padding: 10px;
        width: 100%
    }
    .highlight{
        color: lightblue;
    }
    .left{
        width: 50%;
        float: left;
    }
    .right{
        width: 50%;
        float: right;
    }
    .content{
        height: 50px;
        margin: 30px 0px;
    }
</style>

<div class="header">
    <h2>
        Customers
    </h2>
</div>
<div>
    <div>
    </div>

    <div>
        <div>
            <table>
                <thead>
                    <tr role="row">
                        <th style='width:5%'> # </th>
                        <th style='width:20%'> Full Name </th>
                        <th style='width:20%'> Email </th>
                        <th style='width:20%'> Mobile</th>
                        <th style='width:20%'> Type </th> 
                    </tr>
                </thead>
                <tbody> 
                	@foreach( $customers as $index => $customer )
                        <tr class="{{ ( $index % 2 == 0 ) ? 'even' : 'odd'  }}">
                            <td>{{ $customer->c_id }}</td>
                            <td>{{ $customer->c_fullname }}</td>
                            <td>{{ $customer->c_email }}</td>
                            <td>{{ $customer->c_mobile }}</td>
                            <td>{{ $customer->c_customer_type }}</td>
                        </tr>
                        @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
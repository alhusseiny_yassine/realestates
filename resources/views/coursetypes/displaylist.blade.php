<?php
/************************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>
<div class="portlet-body flip-scroll" id="LISTCOURSETYPES">
    <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
        <thead class="flip-content">
        <tr>
            <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_coursetype" id="CK_ALL_COURSETYPES" class="group-checkable" data-set="#LISTCOURSETYPES .checkboxes" value="1" /></th>
            <th style="width:2px">#</th>
            <th>Course Type Name </th>
            <th>Course Type Description</th>
            <th style="width:2px" nowrap>Edit</th>
            <th style="width:2px" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($course_types as $index => $course_type_info)
        {
            ?>
               <tr data-ct_id="{{ $course_type_info->ct_id }}">
                   <td><input type="checkbox" class="checkboxes" name="ck_courset_{{ $course_type_info->ct_id }}" id="CK_COURSE_TYPE_{{ $course_type_info->ct_id }}" value="1" /></td>
                   <td><?php echo $course_type_info->ct_id; ?></td>
                   <td><?php echo $course_type_info->ct_course_type; ?></td>
                   <td><?php echo substr($course_type_info->ct_description, 0,10); ?></td>
                   <td> <a href="#"  id="EDIT_COURSE_TYPE_{{ $course_type_info->ct_id }}" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
                   <td><a href="#"  id="DELETE_COURSE_TYPE_{{ $course_type_info->ct_id }}" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>
</div>
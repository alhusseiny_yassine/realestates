<?php
/************************************************************
editrole.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/


?>

@extends('layouts.alayout')

@section('plugins')
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/modules/beneficiaryTypes.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/beneficiarytypes/editbeneficiaryTypes.js') }}"></script>
@endsection
@section('content')

<div class="portlet blue box">
<div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Edit Beneficiary Type </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
    <div class="portlet-body">
         <form name="form_save_benft" id="FORM_SAVE_BENFT">
    <div class="form-body">
         <span id="hidden_fields">
            {!! csrf_field() !!}
            <input type="hidden" name="bt_id" id="BT_ID" value="{{ $cur_benType_info->bt_id }}">
        </span>
        <div class="alert alert-success" style="display:none">
				<strong>Success!</strong> Benficiary Type Information is saved successfully!
			</div>
			<div class="alert alert-danger" style="display:none">
				<strong>Error!</strong> You have some form errors. Please check below.
			</div>
        <div class="row">
            <div class="col-md-8">
                 <div class="form-group">
                    <label class="control-label">Benficiary Type Name <span class="required"> * </span></label>
                    <input type="text" name="bt_name" id="BT_NAME" class="form-control" required="required" maxlength="500"  value="{{ $cur_benType_info->bt_name }}" />
                </div>
            </div>
            <div class="col-md-8">
                 <div class="form-group">
                    <label class="control-label">Benficiary Type Description <span class="required"> * </span></label>
                    <textarea name="bt_description" id="BT_DESCRIPTION" class="form-control" style="width:100%;resize:none" rows="10"  >{{ $cur_benType_info->bt_description }}</textarea>
                </div>
            </div>

        </div>
       <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-md-9"></div>
            <div class="col-md-3" align="right">
                 <button type="submit" name="btn_save_benftype" id="BTN_SAVE_BENFTYPE"  class="btn blue">Save</button>
                <button type="button" id="BACK_FORM" name="back_form" class="btn default">Back</button>
            </div>
        </div>
    </div>
</form>
    </div>
</div>


@endsection
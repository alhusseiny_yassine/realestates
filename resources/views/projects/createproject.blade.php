<?php
use App\model\Project\Projects;
/***********************************************************
createproject.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 3, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
Page to Create A New Project
***********************************************************/


?>

@extends('layouts.layout')

@section('themes')
   <link href="{{ url('admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
   <link href="{{ url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}" rel="stylesheet" type="text/css" />
   <link href="{{ url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css" />
   <link href="{{ url('admin/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
   <link href="{{ url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
   <link href="{{ url('admin/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
   <link href="{{ url('admin/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
   <style>
       th{
	      cursor: pointer;
       }
       .container-lf-space{
	       padding-left:2px;
       	    padding-right:2px;
       }
       .portlet-body{
	       min-height:600px;
       }
   </style>
    <link href="{{ url('admin/assets/pages/css/search.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('plugins')
    <script src="{{ url('admin/assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>

    <script src="{{ url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootbox.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ url('admin/assets/global/plugins/jquery.tablesorter/jquery.tablesorter.js') }}"></script>

    <script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
    <script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>
    <script src="{{ url('admin/assets/pages/scripts/ecommerce-orders-view.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ url('js/modules/projects.module.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/projects/createproject.js') }}"></script>
@endsection

@section('content')
<div class="portlet blue box">
<div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Create Project </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
    <div class="portlet-body">
        <form name="form_save_project" id="FORM_SAVE_PROJECT">
             <span id="hidden_fields">
                {!! csrf_field() !!}
                <input type="hidden" name="project_status_id" id="PROJECT_STATUS_ID" value="<?php echo encrypt("1"); ?>" />
            </span>
                    <div class="alert alert-success" style="display:none">
        				<strong>Success!</strong> Project Information is saved successfully!
        			</div>
        			<div class="alert alert-danger" style="display:none">
        				<strong>Error!</strong> You have some form errors. Please check below.
        			</div>
                <div class="row">
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Project Code <span class="required"> * </span></label>
                            <input type="text" name="pp_project_code" readonly="readonly" maxlength="30" id="PP_PROJECT_CODE" class="form-control" value="{{ $project_code }}" />
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Project Name <span class="required"> * </span></label>
                            <input type="text" name="pp_project_title" maxlength="150" id="PP_PROJECT_TITLE" class="form-control" value="" />
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Project Type <span class="required"> * </span></label>
                            <select name="sl_project_type" id="SL_PROJECT_TYPE" class="form-control bs-select">
                                <option value="">No Project Type</option>
                                @foreach($project_types as $pt_id => $pt_info)
                                    <option value="{{ $pt_info->pt_id }}">{{ $pt_info->pt_project_type }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Donors <span class="required"> * </span></label>
                            <select name="sl_donor" id="SL_DONOR" class="form-control bs-select">
                                <option value="">No Donor</option>
                                @foreach($lst_donors as $d_id => $d_info)
                                    <option value="{{ $d_info->d_id }}">{{ $d_info->d_donor_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label"> Project Billing Method <span class="required"> * </span></label>
                            <select name="pp_project_billing_method" id="PP_PROJECT_BILLING_METHOD" class="form-control bs-select">
                                <option value=""> No Billing Method </option>
                                <option value="1">Lump Sum</option>
                                <option value="2">Per Trainee</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Project Budget <span class="required"> * </span></label>
                            <input type="number" min="0" name="pp_project_budget" maxlength="20" id="PP_PROJECT_BUDGET" class="form-control"  min="1" step="any" />
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Project Start Date <span class="required"> * </span></label>
                            <input type="text" name="pp_project_start_date" maxlength="10" id="PP_RPOJECT_START_DATE" class="form-control" value="" />
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Project End Date <span class="required"> * </span></label>
                            <input type="text" name="pp_project_end_date" maxlength="150" id="PP_PROJECT_END_DATE" class="form-control" value="" />
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Project Contract Date <span class="required"> * </span></label>
                            <input type="text" name="pp_contract_date" maxlength="150" id="PP_CONTRACT_DATE" class="form-control" value="" />
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label"><b>Project Contract Number : </b></label>
                              <input type="text" name="pp_contract_number" maxlength="150" id="PP_CONTRACT_NUMBER" class="form-control" value="" />

                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Project Courses <span class="required"> * </span></label>
                            <select name="pc_project_courses[]" class="form-control" id="PC_PROJECT_COURSES" multiple="multiple" style="width:100%;height:200px;">
                                @foreach($lst_courses as $index => $course_info)
                                    <option value="{{ $course_info->c_id }}">{{ $course_info->c_course_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Project Beneficiaries</label>
                            <select name="pc_project_benficiaries[]" class="form-control mt-multiselect" id="PC_PROJECT_BENEFICIARIES" multiple="multiple" style="width:100%;height:200px;">
                                @foreach($lst_beneficiaries as $index => $benf_info)
                                    <option value="{{ $benf_info->sb_id }}">{{ $benf_info->sb_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Project Providers <span class="required"> * </span></label>
                            <select name="pp_project_providers[]" class="form-control" id="PC_PROJECT_PROVIDERS" multiple="multiple" style="width:100%;height:200px;">
                                @foreach($lst_providers as $index => $provider_info)
                                    <option value="{{ $provider_info->id }}">{{ $provider_info->u_fullname }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Project Description </label>
                            <textarea name="pp_project_description"  id="PP_PROJECT_DESCRIPTION" class="form-control" style="width:100%;height:200px;resize:none" ></textarea>
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12" align="right" style="clear: both;">
                        <button type="submit" name="btn_save_project" id="BTN_SAVE_PROJECT" class="btn btn-info">Save</button>
                    </div>
                </div>

        </form>
    </div>
 </div>
@endsection
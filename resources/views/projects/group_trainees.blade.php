<?php
/***********************************************************
group_trainees.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Jan 29, 2017
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

?>



<div class="portlet blue box" style="width:900px;">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Trainees Group </div>
        <div class="tools"></div>
    </div>
    <div class="portlet-body">
        <form name="frm_trainees_group" id="FRM_TRAINEES_GROUP">
            <span id="hidden_fields">
            {!! csrf_field() !!}
            <input type="hidden" name="pg_id" value="{{ $pg_id }}" />
            <input type="hidden" name="project_id" value="{{ $project_id }}" />
            </span>
            <div class="row">
                    <div class="col-md-1 col-xs-1 col-sm-1"></div>
                    <div class="col-md-10 col-xs-10 col-sm-10">
                        <div class="form-group">
                            <label  class="control-label"> Group Trainees <span class="required"> * </span></label>
                             <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
                                <thead class="flip-content">
                                <tr>
                                    <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_users" id="CK_ALL_USERS" class="group-checkable" data-set="#LISTUSERS .checkboxes" value="1" /></th>
                                    <th style="width:2px;">#</th>
                                    <th>Email</th>
                                    <th>Full Name</th>
                                    <th>beneficiary</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($lst_trainees as $index => $lt_info)

                                        <tr  class="odd gradeX" data-user_id="<?php echo $lt_info->id; ?>">
                                       <td><input {{ in_array($lt_info->id,$selected_trainees_array) ? "checked" : "" }}  type="checkbox" name="pc_project_trainees[]" id="CK_USER_<?php echo $lt_info->id; ?>" class="checkboxes" value="<?php echo $lt_info->id; ?>" /></td>
                                       <td><?php echo $lt_info->id; ?></td>
                                       <td><?php echo $lt_info->u_email; ?></td>
                                       <td><?php echo $lt_info->u_fullname; ?></td>

                                       <td><?php echo isset( $lt_info->fk_beneficiary_id ) ? $benf_info[ $lt_info->fk_beneficiary_id ]->sb_name : "" ; ?></td>
                                      </tr>
                                @endforeach
                                </tbody>
                             </table>
                        </div>
                    </div>
                    <div class="col-md-1 col-xs-1 col-sm-1"></div>
                    <div class="col-md-12 col-xs-12 col-sm-12" align="right">
                        <button type="button" name="btn_save_trainees_group" id="BTN_SAVE_TRAINEES_GROUP" class="btn btn-default">Save Trainees Group</button>
                    </div>
            </div>
        </form>
    </div>
</div>
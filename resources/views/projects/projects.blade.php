<?php
/***********************************************************
projects.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 1, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
Project Management Page
***********************************************************/


?>

@extends('layouts.layout')

@section('themes')
   <link href="{{ url('admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
   <link href="{{ url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}" rel="stylesheet" type="text/css" />
   <link href="{{ url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css" />
   <link href="{{ url('admin/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
   <link href="{{ url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
   <link href="{{ url('admin/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
   <style>
       th{
	      cursor: pointer;
       }
       #ChangeStatus{
	       position: absolute;
       	top:40%;left:40%;
       	width:47%;
       }
   </style>
    <link href="{{ url('admin/assets/pages/css/search.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('plugins')
    <script src="{{ url('admin/assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>

    <script src="{{ url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootbox.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ url('admin/assets/global/plugins/jquery.tablesorter/jquery.tablesorter.js') }}"></script>

    <script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
    <script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>
    <script src="{{ url('admin/assets/pages/scripts/ecommerce-orders-view.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ url('js/modules/projects.module.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/projects/projectsmanagement.js') }}"></script>
@endsection

@section('content')
<div class="page-fixed-main-content">
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="search-page search-content-2">
                        <div class="search-bar bordered">
                            <div class="row">
                                <div class="col-md-7">
                                    <form name='frm_search_projects' id='FRM_SEARCH_PROJECTS' >
                                        <span id="hidden_fields">
                                             {!! csrf_field() !!}
                                             <input type="hidden" name='project_page_number' id='PROJECT_PAGE_NUMBER' value='1' />
                                        </span>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="search_project_keywords" placeholder="Search For Projects" name="" />
                                            <span class="input-group-btn">
                                                <button name="btn_search_project" id="BTN_SEARCH_PROJECT" class="btn blue uppercase bold" type="button">Search</button>
                                            </span>
                                        </div>
                                    </form>
                                </div>

                                <div class="col-md-5" align="right">
                                        <div class="actions">
                                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                                <label class="btn btn-transparent blue btn-outline btn-circle btn-sm">
                                                    <input type="radio" name="options" class="toggle" id="option2">Settings</label>
                                            </div>
                                            <div class="btn-group">
                                                <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                                                    <i class="fa fa-share"></i>
                                                    <span class="hidden-xs"> Actions </span>
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li><a class="ChangeProjectStatus" href="javascript:;">Change Project Status</a></li>
                                                    <li><a class="RemoveProject" href="javascript:;">Remove Project</a></li>
                                                    <li><a class="PrintDetails" href="javascript:;">Print Project Details</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <div class="search-container bordered">
                                    <ul>
                                         <li class="search-item-header">
                                            <div class="row">
                                                <div class="col-sm-9 col-xs-8">
                                                    <h3>Search results...</h3>
                                                </div>
                                                <div class="col-sm-3 col-xs-4">
                                                    <div class="form-group">
                                                        <select class="bs-select form-control">
                                                            <option>Courses</option>
                                                            <option>Classes</option>
                                                            <option>Users</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <ul style="padding-bottom:0px;">
                                        <li class="search-item clearfix">
                                            <div class="search-content">
                                                <div class="row">
                                                    <div class="col-sm-6 col-md-5 col-xs-12">
                                                        <h2 class="search-title">
                                                            <input type="checkbox" name="ck_all_project" id="CK_ALL_PROJECT" value="1" />
                                                        </h2>
                                                    </div>
                                                    <div class="col-sm-2 col-md-2 col-xs-4">
                                                         Number of Courses
                                                    </div>
                                                    <div class="col-sm-2 col-md-2 col-xs-4">
                                                        Number of Classes
                                                    </div>
                                                    <div class="col-sm-2 col-md-2 col-xs-4">
                                                         Number of Trainees
                                                    </div>
                                                    <div class="col-sm-2 col-md-1 col-xs-4">
                                                        Actions
                                                    </div>
                                                </div>
                                           </div>
                                        </li>
                                    </ul>
                                    <ul class='SearchResults'>

                                    </ul>
                                    <div class="search-pagination row">
                                         <div class="col-md-12 cols-lg-12">
                                                <ul id="MyProjectsPagination" class="pagination-sm"></ul>
                                        </div>
                                    </div>
                                    </div>
                                    </div>
                                    <div class="col-md-3">
                                        <!-- BEGIN PORTLET-->
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-edit font-dark"></i>
                                                    <span class="caption-subject font-dark bold uppercase">Notes</span>
                                                </div>
                                                <div class="actions">
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-cloud-upload"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-wrench"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <!-- <div class="note note-success">
                                                    <h4 class="block">Success! Some Header Goes Here</h4>
                                                    <p> Duis mollis, est non commodo luctus, nisi erat mattis consectetur purus sit amet porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. </p>
                                                </div>
                                                <div class="note note-info">
                                                    <h4 class="block">Info! Some Header Goes Here</h4>
                                                    <p> Duis mollis, est non commodo luctus, nisi erat porttitor ligula, mattis consectetur purus sit amet eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. </p>
                                                </div>
                                                <div class="note note-danger">
                                                    <h4 class="block">Danger! Some Header Goes Here</h4>
                                                    <p> Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit mattis consectetur purus sit amet.\ Cras mattis consectetur purus sit amet fermentum. </p>
                                                </div>
                                                <div class="note note-warning">
                                                    <h4 class="block">Warning! Some Header Goes Here</h4>
                                                    <p> Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit mattis consectetur purus sit amet. Cras mattis consectetur purus sit amet fermentum. </p>
                                                </div> -->
                                            </div>
                                        </div>
                                        <!-- END PORTLET-->
                                    </div>
                                    </div>
                                    </div>
                                    <!-- END PAGE BASE CONTENT -->
                                    </div>
<div class="row">
    <div class="col-md-12" align="right">
        <button type="button" name="btn_create_project" id="BTN_CREATE_PROJECT" class="btn btn-info">Create</button>
    </div>
</div>
<div id="ChangeStatus"></div>
@endsection
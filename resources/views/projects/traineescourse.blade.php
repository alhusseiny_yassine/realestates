<?php
/***********************************************************
displayprojectcourses.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 13, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
         <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
                            <thead class="flip-content">
                                <tr>
                                    <th style="width:2px;">#</th>
                                    <th>Email</th>
                                    <th>Full Name</th>
                                    <th>beneficiary</th>

                                </tr>
                                </thead>
                                <tbody>
         <?php
         foreach ($lst_trainees as $index => $user_info)
         {

             ?>
                <tr  class="odd gradeX">
                    <td><?php echo $user_info->id; ?></td>
                    <td><?php echo $user_info->u_email; ?></td>
                    <td><?php echo $user_info->u_fullname; ?></td>

                    <td><?php echo isset( $user_info->fk_beneficiary_id ) ? $benfs_array[ $user_info->fk_beneficiary_id ]->sb_name : "" ; ?></td>
                </tr>
             <?php
         }
         ?>
         </tbody>
         </table>
    </div>

</div>
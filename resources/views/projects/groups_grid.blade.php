<?php
/***********************************************************
groups_grid.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 29, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/
 

?>

<div class="Groups">
   <div class="TraineeGroup">
    @foreach($project_group_info as $pg_index => $pg_info)
     <div class="portlet green-sharp box" style="width:300px;position: relative;float: left;margin-left:3px;">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i> {{ $pg_info->pg_group_title }} </div>
               <div class="tools">
                    <a data-pg_id="<?php echo $pg_info->pg_id; ?>" data-toggle="modal" class="config EditGroup"> </a>
                    <a data-pg_id="<?php echo $pg_info->pg_id; ?>" data-toggle="modal" class="remove DeleteGroup"> </a>
                    <a data-pg_id="<?php echo $pg_info->pg_id; ?>" data-toggle="modal" class="Info GroupInfo"> <i class="fa fa-info-circle" aria-hidden="true"></i> </a>
                </div>
            </div>
            <div class="portlet-body ClassArea"  data-pg_id="<?php echo $pg_info->pg_id; ?>" >
                 <?php 
                    if(isset($lst_trainees_group[ $pg_info->pg_id ])){
                        $trainees_array = $lst_trainees_group[ $pg_info->pg_id ];
                        foreach ($trainees_array as $index => $trainee_info) 
                        {
                            $id         = $trainee_info['id'];
                            $full_name  = $trainee_info['full_name'];
                              ?>
                                    <div class="label label-info draggable drag-drop TraineeGroupItem" data-user_id="<?php echo $id; ?>"  ><?php echo  $full_name; ?></div>
                              <?php 
                        }

                      
                    }
                   ?>
            </div>
        </div>
    @endforeach
   </div>
</div>
<?php
/***********************************************************
viewgroup.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Feb 27, 2017
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2017

Page Description :
"group_info" => $group_info,
             "courseInfo" => $courseInfo,
***********************************************************/

?>

<div class="portlet blue box" style="width:900px;">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> View Group</div>
        <div class="tools"></div>
    </div>
    <div class="portlet-body">
         <div class="row">
            <div class="col-md-4 col-xs-12 col-sm-6">
                <div class="form-group">
                    <label  class="control-label"><b>Group Code : </b></label>
                    {{ $group_info->pg_group_code }}
                </div>
            </div>
            <div class="col-md-4 col-xs-12 col-sm-6">
                <div class="form-group">
                    <label  class="control-label"><b>Group Title : </b></label>
                    {{ $group_info->pg_group_title }}
                </div>
            </div>
            <div class="col-md-4 col-xs-12 col-sm-6">
                <div class="form-group">
                    <label  class="control-label"><b>Train Days : </b></label>
                    {{ isset($train_days_array[$group_info->pg_train_days]) ? $train_days_array[$group_info->pg_train_days] : "" }}
                </div>
            </div>
            <div class="col-md-4 col-xs-12 col-sm-6">
                <div class="form-group">
                    <label  class="control-label"><b>Course Name : </b></label>
                    {{ $courseInfo->c_course_name }}
                </div>
            </div>
            <div class="col-md-4 col-xs-12 col-sm-6">
                <div class="form-group">
                    <label  class="control-label"><b>Course Category : </b></label>
                    {{ isset($course_categories_array[$courseInfo->fk_category_id]) ? $course_categories_array[$courseInfo->fk_category_id]['cc_course_category'] : "" }}
                </div>
            </div>
         </div>
    </div>
</div>
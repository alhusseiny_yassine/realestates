<?php
/***********************************************************
results.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Dec 21, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/
 
?>
<table class="table table-striped table-bordered table-hover" id="datatable_projects">
<thead>
    <tr role="row" class="heading">
        <th width="5%">
            <input type="checkbox" class="group-checkable" data-set="#datatable_projects .checkboxes" />
        </th>
        <th width="20%"> Project Id&nbsp;# </th>
        <th width="20%"> Project Title </th>
        <th width="20%"> Project Start Date </th>
        <th width="20%"> Project End Date </th>
        <th width="20%"> Status </th>
        <th width="2px" nowrap> config </th>
        <th width="2px" nowrap> view </th>
        <th width="2px" nowrap> Edit </th>
        <th width="2px" nowrap> Delete </th>
    </tr>
    </thead>
    <tbody>
        @for($i = 0; $i < count($total_projects_obj); $i++)
        <tr data-pp_id="{{ $total_projects_obj[$i]->pp_id }}" style="background-color: {{ $project_statuses[ $total_projects_obj[$i]->fk_project_status_id ]->ps_status_color }}">
            <td><input type="checkbox" class="checkboxes" name="ck_project_{{ $total_projects_obj[$i]->pp_id }}" id="CK_PROJECT_{{ $total_projects_obj[$i]->pp_id }}" value="{{ $total_projects_obj[$i]->pp_id }}" /></td>
            <td>{{ $total_projects_obj[$i]->pp_project_code }}</td>
            <td>{{ $total_projects_obj[$i]->pp_project_title }}</td>
            <td>{{ $total_projects_obj[$i]->pp_project_start_date }}</td>
            <td>{{ $total_projects_obj[$i]->pp_project_end_date }}</td>
            <td>{{ $project_statuses[ $total_projects_obj[$i]->fk_project_status_id ]->ps_status_name }}</td>
            <td><a href="#"  id="CONFIG_PROJECT_{{ $total_projects_obj[$i]->pp_id }}" ><i class="fa fa-cog" aria-hidden="true" height="16" ></i></a></td>
            <td> <a href="#"  id="VIEW_PROJECT_{{ $total_projects_obj[$i]->pp_id }}" ><i class="fa fa-eye" aria-hidden="true"></i></a></td>
            <td> <a href="#"  id="EDIT_PROJECT_{{ $total_projects_obj[$i]->pp_id }}" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
            <td><a href="#"  id="DELETE_PROJECT_{{ $total_projects_obj[$i]->pp_id }}" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
        </tr>
        @endfor
    </tbody>
</table>
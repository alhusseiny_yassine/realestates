<?php
/***********************************************************
view.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Dec 22, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :

***********************************************************/


?>
@extends('layouts.alayout')

@section('themes')
<link href="{{ url('admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css" />

<link href="{{ url('admin/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('admin/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
<style>
th{
    cursor: pointer;
}
</style>
@endsection
@section('plugins')

<script src="{{ url('admin/assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>

<script src="{{ url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootbox.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('admin/assets/global/plugins/jquery.tablesorter/jquery.tablesorter.js') }}"></script>

<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>
<script src="{{ url('admin/assets/pages/scripts/ecommerce-orders-view.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('js/modules/projects.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/projects/adminprojectsmanagement.js') }}"></script>
<<script type="text/javascript">
$(function(){
	$('.BtnBack').on('click',function(){
		history.back();
	});
})
</script>
@endsection
@section('content')
<ul class="nav nav-tabs">
    <li class="active">
        <a href="#ProjectInformation" data-toggle="tab"> Project Information </a>
    </li>
    <li>
        <a href="#ProjectTrainees" data-toggle="tab"> Project Trainees </a>
    </li>
    <li>
        <a href="#ProjectCourses" data-toggle="tab"> Project Courses </a>
    </li>
</ul>
<div class="tab-content">
    <div class="tab-pane fade active in" id="ProjectInformation">
        @include('projects.admin.viewInfo', [ 'project_info' => $project_info , "lst_providers" => $lst_providers, "lst_courses" => $lst_courses, "lst_donors" => $lst_donors, "project_types" => $project_types, "lst_requests" => $lst_requests, "requests_array" => $requests_array,  "project_status" => $project_status, "providers_array" => $providers_array, "courses_array" => $courses_array,  "trainees_array" => $trainees_array])
    </div>
    <div class="tab-pane fade" id="ProjectTrainees">
      @include('projects.admin.traineesInfo' , ["lst_trainees" => $lst_trainees])
    </div>
    <div class="tab-pane fade" id="ProjectCourses">
      @include('projects.admin.coursesInfo', ["courses_array" => $courses_array , "lst_courses" => $lst_courses])
    </div>
</div>
@endsection
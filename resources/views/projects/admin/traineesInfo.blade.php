<?php
/* * *********************************************************
  traineesInfo.blade.php
  Product :
  Version : 1.0
  Release : 2
  Date Created : Dec 24, 2016
  Developed By  : Mohamad Mantach   PHP Department
  All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

  Page Description :
  {Enter page description Here}
 * ********************************************************* */
$session_user_id = session()->get('user_id');
?>
<div class="portlet-body flip-scroll" id="">
    <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
        <thead class="flip-content">
            <tr>
                <th style="width:2px;">#</th>
                <th>Email</th>
                <th>Full Name</th>
                <th>beneficiary</th>
                <th></th>
            </tr>
        </thead>
        <tbody class="">

            @foreach ($lst_trainees as $index => $trainee_info)
            @if( in_array( $trainee_info['id'] , $trainees_array ) )

            <tr>
                <td>{!! $trainee_info['id'] !!}</td>
                <td>{!! $trainee_info['u_email'] !!}</td>
                <td>{!! $trainee_info['u_fullname'] !!}</td>
                <td>{!! ( isset($trainee_info['fk_beneficiary_id']) ) ? $benf_info[$trainee_info['fk_beneficiary_id']]->sb_name : ""; !!}</td>
                <td>
                    @if($trainee_info->isOnline())
                        <img src="{{ url('imgs/online_user.png') }}" />
                    @else
                        <img src="{{ url('imgs/offline_user.png') }}" />
                    @endif
                </td>
            </tr>
            @endif
            @endforeach 
        </tbody>
    </table>
</div>
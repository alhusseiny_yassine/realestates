<?php
/* * *********************************************************
  coursesInfo.blade.php
  Product :
  Version : 1.0
  Release : 2
  Date Created : Aug 8, 2017
  Developed By  : alhusseiny yassine   PHP Department

  Page Description :
  {Enter page description Here}
 * ********************************************************* */
?>
<div class="portlet-body flip-scroll" id="">
    <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
        <thead class="flip-content">
            <tr>
                <th style="width:2px;">#</th>
                <th>Course Code</th>
                <th>Course Name</th>
                <th>Description</th>
                <th>Total Hours</th>
                <th>Category</th>
            </tr>
        </thead>
        <tbody class="">

            @foreach ($lst_courses as $index => $course_info)
            @if( in_array( $course_info['c_id'] , $courses_array ) )

            <tr>
                <td>{!! $course_info['c_id'] !!}</td>
                <td>{!! $course_info['c_code'] !!}</td>
                <td>{!! $course_info['c_course_name'] !!}</td>
                <td>{!! $course_info['c_course_description'] !!}</td>
                <td>{!! $course_info['c_total_hours'] !!}</td>
                <td>{!! ( isset( $category_array[$course_info['fk_category_id']] ) ) ? $category_array[$course_info['fk_category_id']]->cc_course_category : "" !!}</td>
                <td></td>
            </tr>
            
            @endif
            @endforeach 
        </tbody>
    </table>
</div>
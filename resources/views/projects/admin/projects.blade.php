<?php
/***********************************************************
projects.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Dec 20, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
Admin interface for Project Management System 
***********************************************************/

?>
@extends('layouts.alayout')

@section('themes')
<link href="{{ url('admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css" />

<link href="{{ url('admin/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('admin/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />

 <link href="{{ url('admin/assets/global/plugins/bootstrap-multiselect/css/bootstrap-multiselect.css') }}" rel="stylesheet" type="text/css" />
<style>
th{
    cursor: pointer;
}
#ChangeStatus{
	position: absolute;
       	top:40%;left:40%;
       	width:47%;
        width: 500px;
        height: 200px;
       }
</style>
@endsection
@section('plugins')

<script src="{{ url('admin/assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>

 

<script src="{{ url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootbox.min.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}" type="text/javascript"></script>
<script src="{{ url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('admin/assets/global/plugins/jquery.tablesorter/jquery.tablesorter.js') }}"></script>

<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>
<script src="{{ url('admin/assets/pages/scripts/ecommerce-orders-view.js') }}" type="text/javascript"></script>

<script src="{{ url('admin/assets/global/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js') }}" type="text/javascript"></script>

 

<script type="text/javascript" src="{{ url('js/modules/projects.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/projects/adminprojectsmanagement.js') }}"></script>
@endsection

@section('content')

<form name="frm_projects_management" id="FRM_PROJECTS_MANAGEMENT">
    <div class="portlet blue box">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i> Projects Management </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
                <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                <a href="javascript:;" class="reload"> </a>
            </div>
        </div>
        <div class="portlet-body">
            <span id="hidden_fields">
                <input type="hidden" name="page_number" value="1" />
                <input type="hidden" name="is_admin" id="is_admin" value="1">
                {!! csrf_field() !!}
            </span>
            <div class="row">
                <div class="col-md-12" align="right">
                        <div class="btn-group pull-right blue">
                            <button class="btn blue btn-outline dropdown-toggle" data-toggle="dropdown">Actions
                                <i class="fa fa-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                    <li><a class="ChangeProjectsStatus" href="javascript:;"> Change Project Status </a></li>
                                    <li><a class="QuickEditProject" href="javascript:;"> Quick Edit Project </a></li>
                            </ul>
                      </div>
                </div>
            </div>
             <div class="row">
                <div class="col-md-12">&nbsp;</div>
            </div>
            <div class="row">
            <div class="col-md-12">
                     <div class="search-filter">
                        <div class="search-label uppercase">Search By : </div>
                        <div class="col-md-12">
                          <div class="form-group">
                                <label class="control-label">Project Status : </label>
                                <select  name="prj_project_status" id="PRJ_PROJECT_STATUS" class="form-control bs-select">
                                    <option value="0">--Select One--</option>
                                    @foreach( $project_statuses as $index => $pr_info )
                                        <option  value="{{ $pr_info->ps_id }}">{{ $pr_info->ps_status_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                                <label class="control-label">beneficiaries : </label><br/> 
                                <select  name="prj_project_beneficiary[]" id="PRJ_PROJECT_BENEFICIARY" class="mt-multiselect btn btn-default" multiple="multiple" data-label="left" data-select-all="true" data-width="100%" data-filter="true" data-action-onchange="true">
                                    @foreach( $lst_benfs as $index => $bf_info )
                                        <option  value="{{ $bf_info->sb_id }}">{{ $bf_info->sb_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                                <label class="control-label">Project Title : </label>
                                <input type="text" name="prj_project_title" id="PRJ_PROJECT_TITLE" class="form-control" value="" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <button type="button" name="btn_reset_search" class="btn grey bold uppercase btn-block">Reset Search</button>
                            </div>
                            <div class="col-xs-6">
                                <button type="button" name="btn_advanced_search" class="btn grey-cararra font-blue bold btn-block">Advanced Search</button>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
             <div class="row">
                <div class="col-md-12">&nbsp;</div>
            </div>
             <div class="row">
                 <div class="col-md-8"></div>
                 <div class="col-md-4" align="right">
                    <button  name="btn_add_admin_project" id="BTN_ADD_ADMIN_PROJECT" type="button" class="btn blue capitalize" >ADD New Project</button>
                 </div>
             </div>
             <div class="row">
                <div class="col-md-12">&nbsp;</div>
            </div>
            <div class="row">
                 <div class="col-md-12">
                     <div style="left:20%" class="ListProjects">

                    </div>
                 </div>
             </div>
              <div class="row">
                 <div class="col-md-12" align="left">
                    <form name="quick_edit_form" id="QUICK_EDIT_FORM">
                    
                    </form>
                 </div>
            </div>
              <div class="row">
                 <div class="col-md-10" align="left">
                    <ul id="ProjectsPagination" class="pagination-sm"></ul>
                 </div>
                 <div class="col-md-2" align="right">
    
                 </div>
            </div>
             <div class="row">
                 <div class="col-md-8"></div>
                 <div class="col-md-4" align="right">
                    <button  name="btn_add_admin_project" id="BTN_ADD_ADMIN_PROJECT_BOTTOM" type="button" class="btn blue capitalize" >ADD New Project</button>
                 </div>
             </div>
     </div>
</div>

</form>
<div id="ChangeStatus"></div>
 <div id="ProjectManagement" class="modal fade" tabindex="-1"> </div>
@endsection



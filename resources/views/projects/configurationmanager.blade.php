<?php
/***********************************************************
configurationmanager.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 10, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


?>
@extends('layouts.layout')

@section('themes')
   <link href="{{ url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css" />
   <style>
       th{
	      cursor: pointer;
       }
       .mt-step-col{
	       cursor: pointer;
       }
       #ModelPopUp{
	      top:40%;
       	  left:20%;
       	  position: absolute;
       }
   </style>
    <link href="{{ url('admin/assets/pages/css/search.min.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ url('admin/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('admin/assets/global/plugins/jquery-multi-select/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('admin/assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('admin/assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('admin/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{ url('js/dhtmlxScheduler/codebase/dhtmlxscheduler.css') }}" type="text/css" media="screen" title="no title" charset="utf-8">
	<link href="{{ url('admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('admin/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('plugins')
    <script src="{{ url('admin/assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>

    <script src="{{ url('admin/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>

    <script src="{{ url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootbox.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ url('admin/assets/global/plugins/jquery.tablesorter/jquery.tablesorter.js') }}"></script>

    <script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
    <script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>
    <script src="{{ url('admin/assets/pages/scripts/ecommerce-orders-view.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js') }}" type="text/javascript"></script>

    <script src="{{ url('js/dhtmlxScheduler/codebase/dhtmlxscheduler.js') }}" type="text/javascript" charset="utf-8"></script>


    <script src="{{ url('admin/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ url('admin/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/libraries/interact.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/modules/projects.module.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/projects/configurationmanagement.js') }}"></script>
    <script src=/eventsource-polyfill.js></script>
@endsection

@section('content')
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-config"></i>Configuration Management ({{ $projectConfiguration->pp_project_title }}) </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
            <a href="javascript:;" class="remove"> </a>
        </div>
    </div>
    <div class="portlet-body" style="min-height:400px;">
        <!-- done active -->
        <div class="mt-element-step">
            <div class="row step-line">
                    <div class="mt-step-desc">
                        <div class="font-dark bold uppercase"></div>
                        <div class="caption-desc font-grey-cascade">  </div>
                        <br/> </div>
                    <div id="courses_configuration" class="col-md-3 mt-step-col first active" data-tab_name="courses_configuration" >
                        <div class="mt-step-number bg-white">1</div>
                        <div class="mt-step-title uppercase font-grey-cascade">Courses Configuration</div>
                        <div class="mt-step-content font-grey-cascade"></div>
                    </div>
                     <div id="groups_configuration" class="col-md-3 mt-step-col" data-tab_name="groups_configuration" >
                        <div class="mt-step-number bg-white">2</div>
                        <div class="mt-step-title uppercase font-grey-cascade">Groups Configuration</div>
                        <div class="mt-step-content font-grey-cascade"></div>
                    </div>
                    <div id="sessions_configuration"  class="col-md-3 mt-step-col" data-tab_name="sessions_configuration" >
                        <div class="mt-step-number bg-white">3</div>
                        <div class="mt-step-title uppercase font-grey-cascade">Sessions Configuration</div>
                        <div class="mt-step-content font-grey-cascade"></div>
                    </div>
                    <div id="preview_confirmation"  class="col-md-3 mt-step-col last" data-tab_name="preview_confirmation" >
                        <div class="mt-step-number bg-white">4</div>
                        <div class="mt-step-title uppercase font-grey-cascade">Preview & Confirmation</div>
                        <div class="mt-step-content font-grey-cascade"></div>
                    </div>
                </div>
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <form name="frm_configuration" id="FRM_CONFIGURATION">
                            <span id="hidden_fields">
                        <input type="hidden" name="tab_name" id="TAB_NAME" value="courses_configuration" />
                        <input type="hidden" name="project_id" id="PROJECT_ID" value="{{ $project_id }}" />
                        {!! csrf_field() !!}
                    </span>
                    <div class="ConfigurationManager">
                    </div>
                    <div class="row">
                        <div class="col-md-12" align="right">
                            <button type="button" name="btn_save_info" id="BTN_SAVE_INFO" class="btn btn-info" >Next</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
 </div>
@endsection
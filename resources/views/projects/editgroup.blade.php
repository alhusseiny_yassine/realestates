<?php
/***********************************************************
addgroup.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Jan 8, 2017
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

?>

<div class="portlet blue box" style="width:900px;height:600px;">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Group Info </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="javascript:;" class="modal_close"> <i class="fa fa-times" aria-hidden="true"></i> </a>
        </div>
    </div>
    <div class="portlet-body">
        <form name="frm_new_group" id="FRM_NEW_GROUP">
            <span id="hidden_fields">
            {!! csrf_field() !!}
            <input type="hidden" name="pg_id" value="{{ $group_info->pg_id }}" />
            </span>
            <div class="row">
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Group Code </label>
                            <input type="text" name="pg_group_code" readonly="readonly" maxlength="30" id="PG_GROUP_CODE" class="form-control" value="{{ $group_info->pg_group_code }}" />
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Group Title <span class="required"> * </span></label>
                            <input type="text" name="pg_group_title" maxlength="255" id="PG_GROUP_TITLE" class="form-control" value="{{ $group_info->pg_group_title }}" />
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Group Start Date <span class="required"> * </span></label>
                            <input type="text" name="pg_start_date" maxlength="255" id="PG_START_DATE" class="form-control" value="{{ $group_info->pg_start_date }}" />
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Group End Date <span class="required"> * </span></label>
                            <input type="text" name="pg_end_date" maxlength="255" id="PG_END_DATE" class="form-control" value="{{ $group_info->pg_end_date }}" />
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label  class="control-label">Course </label>
                            <select name='sl_course' id="sl_course" class="form-control">
                                <option value="0">-- Select Course -- </option>
                                @for($i = 0; $i < count($ProjectCourses); $i++)
                                    <option {{ $group_info->fk_pcourse_id == $ProjectCourses[$i]->pc_id ? "selected" : "" }}  value="{{ $ProjectCourses[$i]->pc_id }}">{{ $ProjectCourses[$i]->c_course_name }}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label  class="control-label">Train Days <span class="required"> * </span></label>
                            <select name='sl_train_days' id="sl_train_days" class="form-control">
                                <option value="0">-- Select Days -- </option>
                                @foreach($daysInfo as $d_index => $day_info)
                                       <option {{ $group_info->pg_train_days == $day_info->ID ? "selected" : "" }}  value="{{ $day_info->ID }}">{{ $day_info->Name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label  class="control-label"> Time </label>
                            <input type="text" name="pg_trainee_time" id="PG_TRAINEE_TIME" class="form-control" min="00:00" max="23:59" value="{{ $group_info->pg_trainee_time }}" />
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label  class="control-label"> Provider </label>
                            <select name='pg_class_provider' id="PG_CLASS_PROVIDER" class="form-control">
                                <option value="0">-- Select Provider -- </option>

                                @foreach($lst_providers as $key => $provider)
                                    <option {{ $group_info->pg_class_provider == $provider->id ? "selected" : "" }}  value="{{ $provider->id }}">{{ $provider->u_fullname }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label  class="control-label"> Trainer </label>
                            <select name='sl_trainer' id="sl_trainer" class="form-control">
                                <option value="0">-- Select Trainer -- </option>
                                @for($i = 0; $i < count($lst_trainers); $i++)
                                    <option {{ $group_info->pg_class_trainer == $lst_trainers[$i]->id ? "selected" : "" }}  value="{{ $lst_trainers[$i]->id }}" >{{ $lst_trainers[$i]->u_fullname }}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Location</label>
                            <span id="providerLocation">
                           <select name="pg_train_location" id="PG_TRAIN_LOCATION" class="form-control" style="width:100%">
                                <option value="">--Select One--</option>
                            </select>
                            </span>

                            <input type="hidden" name="ini_pg_trainee_location_id" value="{{ $group_info->pg_trainee_location_id }}" />
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12 col-sm-8">
                        <div class="form-group">
                            <label  class="control-label">Group Description <span class="required"> * </span></label>
                            <textarea style="width:100%;height:300px;" id="pg_group_description"  name="pg_group_description" class="form-control" >{{ $group_info->pg_group_description }}</textarea>
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12" align="right">
                        <button name="btn_save_group_info" id="BTN_SAVE_GROUP_INFO" class="btn btn-default">Save Group</button>
                    </div>
            </div>
        </form>
    </div>
</div>
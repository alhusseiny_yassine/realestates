<?php
/***********************************************************
courses_configuration.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 13, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

?>
<div class="row">
    <div class="col-md-4">
        <select name="project_course" id="PROJECT_COURSE" class="form-control" style="width:100%">
            <option value="0">Select Project Course</option>
            @for($i = 0; $i < count($course_info); $i++)
                <option value="{{ $course_info[$i]->c_id }}">{{ $course_info[$i]->c_course_name }}</option>
            @endfor
        </select>
    </div>
    <div class="col-md-8" align="left">
       &nbsp;&nbsp;<label class="course_code"></label>
    </div>
</div>
<div class="row">
    <div class="col-md-12 CourseFormInformation">

    </div>
</div>
<div class="row">
   <div class="col-md-12" align="right">
        <button type="button" name="btn_save_courses" id="BTN_SAVE_COURSES" class="btn btn-success" > Save </button>
    </div>
</div>
<div class="row">
   <div class="col-md-12" style="height:20px;"></div>
</div>
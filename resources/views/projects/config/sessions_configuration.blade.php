<?php
/***********************************************************
sessions_configuration.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Nov 17, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


?>

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
       <div id="SessionScheduler" class="dhx_cal_container" style='width:100%; height:700px;'>
    		<div class="dhx_cal_navline">
    			<div class="dhx_cal_prev_button">&nbsp;</div>
    			<div class="dhx_cal_next_button">&nbsp;</div>
    			<div class="dhx_cal_today_button"></div>
    			<div class="dhx_cal_date"></div>
    			<div class="dhx_cal_tab" name="day_tab" style="right:204px;"></div>
    			<div class="dhx_cal_tab" name="week_tab" style="right:140px;"></div>
    			<div class="dhx_cal_tab" name="month_tab" style="right:76px;"></div>
    		</div>
    		<div class="dhx_cal_header">
    		</div>
    		<div class="dhx_cal_data">
    		</div>
        </div>
           
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
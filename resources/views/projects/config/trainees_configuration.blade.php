<?php
/***********************************************************
trainees_configuration.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Nov 11, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

?>
<div class="row">
    <!-- Tab to display List of Trainees in the system -->
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="portlet-body flip-scroll" id="LISTTRAINEES">
        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="TraineesTable">
            <thead>
                 <tr>
                    <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_beneficiaries" id="CK_ALL_BENEFICIARIES" class="group-checkable" data-set="#LISTBENEFICIARIES .checkboxes" value="1" /></th>
                    <th style="width:2px">#</th>
                    <th>Trainee Name</th>
                    <th>Trainee Username</th>
                    <th>Trainee Email</th>
                    <th>Trainee Favorite Days</th>
                </tr>
            </thead>
            <tbody>
                 @foreach($lst_trainees as $t_index => $trainee_info)
                 <tr>
                    <td><input type="checkbox" {{ in_array($trainee_info->id,$selected_trainees_array) ? "checked" : "" }} class="checkboxes" name="ck_trainees[]" id="CK_TRAINEE_{{ $trainee_info->id }}" value="{{ $trainee_info->id }}" /></td>
                    <td>{{ $trainee_info->id }}</td>
                    <td>{{ $trainee_info->u_fullname }}</td>
                    <td>{{ $trainee_info->u_username }}</td>
                    <td>{{ $trainee_info->u_email }}</td>
                    <td>{{ ($trainee_info->u_preferred_train_days == 0) ? "N/A" : $days_ids[$trainee_info->u_preferred_train_days] }}</td>
                 </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- Tab to display List of Trainees -->
</div>
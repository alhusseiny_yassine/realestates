<?php
/***********************************************************
groups_configuration.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Jan 6, 2017
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

?>
<div class="portlet-title" style="width:100%;" align="right">
    <div class="caption">

    </div>
    <div class="actions">
        <div class="btn-group">
            <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                <i class="fa fa-share"></i>
                <span class="hidden-xs"> Actions </span>
                <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-right">
                 <li><a class="DeleteMultipleGroup" href="javascript:;"> Delete Group </a></li>
            </ul>
        </div>
    </div>
</div>
 <form name="frm_group_configuration" id="FRM_GROUP_CONFIGURATION">
    <span id="hidden_fields">
        {!! csrf_field() !!}
        <input type="hidden" id="PP_NBR_CLASSES" name="pp_nbr_classes" value="{{ $projectInfo->pp_nbr_classes }}" />
        <input type="hidden" id="PG_LIST_TYPE" name="pg_list_type" value="list" />
    </span>
    <div class="row">
        <div class="col-md-4" align="left">
            <select name="sl_project_course" id="SL_PROJECT_COURSE" class="form-control">
                <option value="0">All Courses</option>
                <?php
                    for ($i = 0; $i < count($project_courses); $i++) {
                        ?>
                        <option value="<?php echo $project_courses[$i]->pc_id; ?>"><?php echo $project_courses[$i]->c_course_name; ?></option>
                        <?php
                    }
                ?>
            </select>
        </div>
        <div class="col-md-8" align="left">

        </div>
    </div>
      <div class="row" style="width:100%;height: 30px;"></div>
    <div class="row">
        <div class="col-md-12" align="left">
            <button type="button" name="btn_add_class" id="BTN_ADD_CLASS" class="btn bt-info">Create New Class</button>
        </div>
    </div>
    <div class="row" style="width:100%;height: 30px;"></div>
    <div class="row">
        <div class="col-md-12 lstTrainees">
            <div class="Trainees" style="display: none">
                <?php
                    foreach ($lst_trainees as $index => $trainee_info)
                    {
                        ?>
                            <div class="label label-info draggable OutGroup drag-drop" data-user_id="{{ $trainee_info->id }}"  >{{ $trainee_info->u_fullname }}</div>
                        <?php
                    }
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" style="display: none">
            <a href="#" class="ChangeDisplay" data-type="list"><i class="fa fa-list" aria-hidden="true"></i></a>
            <a href="#" class="ChangeDisplay" data-type="list"><i class="fa fa-th" aria-hidden="true"></i></a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" style="height: 10px;width:100%;">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 lstGroups">

        </div>
    </div>

</form>
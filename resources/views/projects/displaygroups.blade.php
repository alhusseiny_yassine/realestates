@foreach ($groups as $index => $group)
<tr  class="odd gradeX" data-group_id="{{ $group->pd_id }}">
    <td>{{ $group->pg_id }}</td>
    <td>{{ $group->pg_group_title }}</td>
    <td>{{ $group->pg_group_description }}</td>
    <td>{{ $group->pg_start_date }}</td>
    <td>{{ $group->pg_end_date }}</td>
    <td><a href="#"  id="EDIT_TRAINEE_GROUP_{{ $group->pd_id }}" data-pg_id="{{ $group->pg_id }}" ><i class="fa fa-eye" aria-hidden="true" height="16" ></i></a></td>
    <td><a href="#"  id="EDIT_GROUP_{{ $group->pd_id }}" data-pg_id="{{ $group->pg_id }}" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
    <td><a href="#"  id="DELETE_GROUP_{{ $group->pd_id }}" data-pg_id="{{ $group->pg_id }}" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
</tr>
@endforeach
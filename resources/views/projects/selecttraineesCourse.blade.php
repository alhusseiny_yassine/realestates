<?php
/***********************************************************
selecttraineesCourse.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Feb 15, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

?>

<div class="portlet blue box" style="height: 100%;width:600px">
<div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Select Project Trainees </div>
        <div class="tools">
        </div>
    </div>
    <div class="portlet-body">
        <form name="frm_project_trainees" id="FRM_PROJECT_TRAINEES">
            <span id="hidden_fields">
                  {!! csrf_field() !!}
                  <input type="hidden" name="project_id" value="{{ $project_id }}" />
                  <input type="hidden" name="pc_id" value="{{ $pc_id }}" />
            </span>
            <div class="row">
                 <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">Project Trainees <span class="required"> * </span></label><br/>
                        <select name="ps_project_trainees[]" id="PS_PROJECT_TRAINEES" class="form-control" multiple="multiple" style="width:100%;">
                              @foreach($lst_trainees as $index => $trainee_info)
                               <option value="{{ $trainee_info->id }}">{{ $trainee_info->u_fullname }}</option>
                              @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="button" name="btn_add_trainees" id="BTN_ADD_TRAINEES" class="btn btn-info" >Add Trainees</button>
                </div>
            </div>
        </form>
    </div>
</div>
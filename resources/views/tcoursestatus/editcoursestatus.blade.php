<?php
/***********************************************************
editcoursetype.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 27, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

{
     
}

?>


@extends('layouts.alayout')

@section('themes')
  <link href="{{ url('admin/assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('admin/assets/global/plugins/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('plugins')
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>
        <script src="{{ url('admin/assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}" type="text/javascript"></script>
        <script src="{{ url('admin/assets/global/plugins/jquery-minicolors/jquery.minicolors.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('js/modules/tcoursestatus.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/tcoursestatus/editcoursestatus.js') }}"></script>
@endsection
@section('content')

<div class="portlet blue box">
<div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Edit Project Trainee Course Status </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
    <div class="portlet-body">
         <form name="form_save_project_status" id="FORM_SAVE_PROJECT_STATUS">
    <div class="form-body">
         <span id="hidden_fields">
            {!! csrf_field() !!}
            <input type="hidden" name="ucs_id" value="{{ $trainees_course_status->ucs_id }}" />
        </span>
        <div class="alert alert-success" style="display:none">
				<strong>Success!</strong> Project Trainee Course Status Information is saved successfully!
			</div>
			<div class="alert alert-danger" style="display:none">
				<strong>Error!</strong> You have some form errors. Please check below.
			</div>
        <div class="row">
            <div class="col-md-4">
                 <div class="form-group">
                    <label class="control-label">Project Trainee Course Status Name <span class="required"> * </span></label>
                    <input type="text" name="ucs_status_name" id="UCS_STATUS_NAME" class="form-control" required="required" maxlength="500"  value="{{ $trainees_course_status->ucs_status_name }}" />
                </div>
            </div>
            <div class="col-md-4">
                 <div class="form-group">
                    <label class="control-label">Project Trainee Course Status Color <span class="required"> * </span></label>
                    <input type="text" name="ucs_status_color" data-control="brightness" id="USC_STATUS_COLOR" class="form-control" maxlength="10"  value="{{ $trainees_course_status->ucs_status_color }}" />
                </div>
            </div>
            <div class="col-md-4">
                  <div class="form-group">
                    <label class="control-label">Project Trainee Course Status Percentage <span class="required"> * </span></label>
                    <input type="text" name="ucs_percentage_attendance" id="UCS_PERCENTAGE_ATTENDANCE" class="form-control" required="required" maxlength="5"  value="{{ $trainees_course_status->ucs_percentage_attendance }}" />
                </div>
            </div>

        </div>
       <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-md-9"></div>
            <div class="col-md-3" align="right">
                 <button type="submit" name="btn_save_tcourse_status" id="BTN_SAVE_TCOURSE_STATUS"  class="btn blue">Save</button>
                <button type="reset" name="reset_form" id="RESET_FORM"  class="btn default">Cancel</button>
            </div>
        </div>
    </div>
</form>
    </div>
</div>


@endsection
<?php
/************************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>
<div class="portlet-body flip-scroll" id="LISTDISTRICTS">
    <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
        <thead class="flip-content">
        <tr>
            <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_districts" id="CK_ALL_DISTRICTS" class="group-checkable" data-set="#LISTDISTRICTS .checkboxes" value="1" /></th>
            <th style="width:2px">#</th>
            <th>District Name </th>
            <th>Province Name </th>
            <th style="width:2px" nowrap>Edit</th>
            <th style="width:2px" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($districts as $index => $dist_info)
        {
            ?>
               <tr data-sd_id="{{ $dist_info->sd_id }}">
                   <td><input type="checkbox" class="checkboxes" name="ck_district_{{ $dist_info->sd_id }}" id="CK_DISTRICT_{{ $dist_info->sd_id }}" value="1" /></td>
                   <td><?php echo $dist_info->sd_id; ?></td>
                   <td><?php echo $dist_info->sd_district_name; ?></td>
                   <td><?php echo $provinces[$dist_info->fk_province_id]; ?></td>
                   <td> <a href="#"  id="EDIT_DISTRICT_{{ $dist_info->sd_id }}" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
                   <td><a href="#"  id="DELETE_DISTRICT_{{ $dist_info->sd_id }}" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>
</div>
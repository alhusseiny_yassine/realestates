<?php
/***********************************************************
editcoursetype.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 27, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


?>

@extends('layouts.alayout')


@section('plugins')
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/modules/districts.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/districts/editdistricts.js') }}"></script>
@endsection
@section('content')

<div class="portlet blue box">
<div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Edit District </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
    <div class="portlet-body">
         <form name="form_save_district" id="FORM_SAVE_DISTRICT">
    <div class="form-body">
         <span id="hidden_fields">
            {!! csrf_field() !!}
            <input type="hidden" name="sd_id" id="SD_ID" value="{{ $cur_district_info->sd_id }}" />
        </span>
        <div class="alert alert-success" style="display:none">
				<strong>Success!</strong> District Information is saved successfully!
			</div>
			<div class="alert alert-danger" style="display:none">
				<strong>Error!</strong> You have some form errors. Please check below.
			</div>
        <div class="row">
            <div class="col-md-4">
                 <div class="form-group">
                    <label class="control-label">District <span class="required"> * </span></label>
                    <input type="text" name="sd_district_name" id="SP_DISTRICT_NAME" class="form-control" required="required" maxlength="255"  value="{{ $cur_district_info->sd_district_name }}" />
                </div>
            </div>
            <div class="col-md-4">
                 <div class="form-group">
                     <label class="control-label">Provinces <span class="required"> * </span></label>
                    <select name="fk_province_id" id="FK_PROVINCE_ID" class="form-control" style="width:100%">
                        <option value="">--Select One--</option>
                         @foreach($provinces as $sp_index => $sp_info)
                            <option {{ $cur_district_info->fk_province_id == $sp_info['sp_id'] ? "selected" : "" }}  value="{{ $sp_info['sp_id'] }}" >{{ $sp_info['sp_provinces_name'] }}</option>
                         @endforeach
                    </select>
                </div>
            </div>
        </div>
       <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-md-9"></div>
            <div class="col-md-3" align="right">
                 <button type="submit" name="btn_save_district" id="BTN_SAVE_DISTRICT"  class="btn blue">Save</button>
                <button type="button" id="BACK_FORM" name="back_form" class="btn default">Back</button>
            </div>
        </div>
    </div>
</form>
    </div>
</div>
@endsection
<?php
/***********************************************************
editcoursetype.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 27, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


?>

@extends('layouts.alayout')


@section('plugins')
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/modules/provinces.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/provinces/editprovinces.js') }}"></script>
@endsection
@section('content')

<div class="portlet blue box">
<div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Edit Province </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
    <div class="portlet-body">
         <form name="form_save_province" id="FORM_SAVE_PROVINCE">
    <div class="form-body">
         <span id="hidden_fields">
            {!! csrf_field() !!}
            <input type="hidden" name="sp_id" id="SP_ID" value="{{ $cur_province_info->sp_id }}" />
        </span>
        <div class="alert alert-success" style="display:none">
				<strong>Success!</strong> Province Information is saved successfully!
			</div>
			<div class="alert alert-danger" style="display:none">
				<strong>Error!</strong> You have some form errors. Please check below.
			</div>
        <div class="row">
            <div class="col-md-8">
                 <div class="form-group">
                    <label class="control-label">Province <span class="required"> * </span></label>
                    <input type="text" name="sp_provinces_name" id="SP_PROVINCES_NAME" class="form-control" required="required" maxlength="500"  value="{{ $cur_province_info->sp_provinces_name }}" />
                </div>
            </div>
        </div>
       <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-md-9"></div>
            <div class="col-md-3" align="right">
                 <button type="submit" name="btn_save_province" id="BTN_SAVE_PROVINCE"  class="btn blue">Save</button>
                <button type="button" id="BACK_FORM" name="back_form" class="btn default">Back</button>
            </div>
        </div>
    </div>
</form>
    </div>
</div>


@endsection
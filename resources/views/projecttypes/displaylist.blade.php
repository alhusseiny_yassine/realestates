<?php
/************************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>
<div class="portlet-body flip-scroll" id="LISTPROJECTTYPES">
    <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
        <thead class="flip-content">
        <tr>
            <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_coursetype" id="CK_ALL_PROJECTTYPES" class="group-checkable" data-set="#LISTPROJECTTYPES .checkboxes" value="1" /></th>
            <th style="width:2px">#</th>
            <th>Project Type Name </th>
            <th>Project Type Description</th>
            <th style="width:2px" nowrap>Edit</th>
            <th style="width:2px" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($project_types as $index => $project_type_info)
        {
            ?>
               <tr data-pt_id="{{ $project_type_info->pt_id }}">
                   <td><input type="checkbox" class="checkboxes" name="ck_projectt_{{ $project_type_info->pt_id }}" id="CK_PROJECT_TYPE_{{ $project_type_info->pt_id }}" value="1" /></td>
                   <td><?php echo $project_type_info->pt_id; ?></td>
                   <td><?php echo $project_type_info->pt_project_type; ?></td>
                   <td><?php echo mb_substr($project_type_info->pt_project_description, 0,10); ?></td>
                   <td> <a href="#"  id="EDIT_PROJECT_TYPE_{{ $project_type_info->pt_id }}" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
                   <td><a href="#"  id="DELETE_PROJECT_TYPE_{{ $project_type_info->pt_id }}" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>
</div>
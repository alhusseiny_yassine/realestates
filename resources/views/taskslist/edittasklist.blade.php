<?php
/************************************************************
editrole.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/


?>



@extends('layouts.alayout')

@section('plugins')
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/modules/taskslist.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/taskslist/savetasklist.js') }}"></script>
@endsection
@section('content')

<div class="portlet blue box">
<div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Edit Task List </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
    <div class="portlet-body">
         <form name="form_save_tasklist" id="FORM_SAVE_TASKLIST">
    <div class="form-body">
         <span id="hidden_fields">
            {!! csrf_field() !!}
            <input type="hidden" name="tl_id" id="TL_ID" value="{{ $taskslist_info->tl_id }}" />
        </span>
        <div class="alert alert-success" style="display:none">
				<strong>Success!</strong> Task Information is saved successfully!
			</div>
			<div class="alert alert-danger" style="display:none">
				<strong>Error!</strong> You have some form errors. Please check below.
			</div>
        <div class="row">

            <div class="col-md-4">
                 <div class="form-group">
                    <label class="control-label">Task Title <span class="required"> * </span></label>
                    <input type="text" name="tl_task_title" id="TL_TASK_TITLE" class="form-control" required="required" maxlength="100"  value="{{ $taskslist_info->tl_task_title }}" />
                </div>
            </div>
            <div class="col-md-8">
                 <div class="form-group">
                    <label class="control-label">Task Description <span class="required"> * </span></label>
                    <textarea rows="" style="width:100%;height:200px;" name="tl_task_description" id="TL_TASK_DESCRIPTION" class="form-control"  style="resize:none" cols="">{{ $taskslist_info->tl_task_description }}</textarea>
                </div>
            </div>
        </div>
       <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-md-9"></div>
            <div class="col-md-3" align="right">
                 <button type="submit" name="btn_save_TASKLIST" id="BTN_SAVE_TASKLIST"  class="btn blue">Save</button>
                <button type="button" id="BACK_FORM" name="back_form" class="btn default">Back</button>
            </div>
        </div>
    </div>
</form>
    </div>
</div>


@endsection
<?php
/************************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

foreach ($taskslist as $index => $tl_info)
{
    ?>
       <tr data-tl_id="{{ $tl_info->tl_id }}">
           <td><input type="checkbox" class="checkboxes" name="ck_tasklist_{{ $tl_info->tl_id }}" id="CK_TASKLIST_{{  $tl_info->tl_id  }}" value="1" /></td>
           <td><?php echo $tl_info->tl_id; ?></td>
           <td><?php echo  $tl_info->tl_task_title; ?></td>
           <td> <a href="#"  id="EDIT_TASKSLIST_{{ $tl_info->tl_id }}" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
           <td><a href="#"  id="DELETE_TASKSLIST_{{ $tl_info->tl_id }}" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
       </tr>
    <?php
}
?>
<?php
/***********************************************************
addbeneficiaries.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 27, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

?>


@extends('layouts.alayout')

@section('plugins')
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/modules/donors.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/donors/editdonor.js') }}"></script>
@endsection
@section('content')

<div class="portlet blue box">
<div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Edit Donor </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
    <div class="portlet-body">
         <form name="form_save_donor" id="FORM_SAVE_DONOR">
    <div class="form-body">
         <span id="hidden_fields">
            {!! csrf_field() !!}
            <input type="hidden" name="d_id" id="D_ID" value="{{ $cur_donor_info->d_id }}" />
        </span>
        <div class="alert alert-success" style="display:none">
				<strong>Success!</strong> Donor Information is saved successfully!
			</div>
			<div class="alert alert-danger" style="display:none">
				<strong>Error!</strong> You have some form errors. Please check below.
			</div>
        <div class="row">
            <div class="col-md-4">
                 <div class="form-group">
                    <label class="control-label">Donor Name <span class="required"> * </span></label>
                    <input type="text" name="d_donor_name" id="D_DONOR_NAME" class="form-control" required="required" maxlength="250"  value="{{ $cur_donor_info->d_donor_name }}" />
                </div>
            </div>
           <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Donor Country</label>
                    <select name="fk_country_id" id="FK_COUNTRY_ID" class="form-control" style="width:100%">
                        <option value="">--Select One--</option>
                        <?php
                            for ($i=0;$i<count($countries);$i++)
                            {
                                ?>
                                    <option {{ $cur_donor_info->fk_country_id == $countries[$i]->id ? "selected" : "" }} value="<?php echo $countries[$i]->id; ?>"><?php echo $countries[$i]->code . "-" . $countries[$i]->name; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                 <div class="form-group">
                    <label class="control-label">Donor Telephone</label>
                    <input type="text" name="d_telephone" id="D_TELEPHONE" class="form-control" maxlength="25"  value="{{ $cur_donor_info->d_telephone }}" />
                </div>
            </div>
            <div class="col-md-4">
                 <div class="form-group">
                    <label class="control-label">Donor Fax</label>
                    <input type="text" name="d_fax" id="D_FAX" class="form-control" maxlength="25"  value="{{ $cur_donor_info->d_fax }}" />
                </div>
            </div>
            <div class="col-md-4">
                 <div class="form-group">
                    <label class="control-label">Donor Mobile</label>
                    <input type="text" name="d_mobile" id="D_MOBILE" class="form-control" maxlength="25"  value="{{ $cur_donor_info->d_mobile }}" />
                </div>
            </div>
            <div class="col-md-4">
                 <div class="form-group">
                    <label class="control-label">Donor Email</label>
                    <input type="text" name="d_email" id="D_EMAIL" class="form-control" maxlength="255"  value="{{ $cur_donor_info->d_email }}" />
                </div>
            </div>
            <div class="col-md-8">
                 <div class="form-group">
                    <label class="control-label">About Donor</label>
                    <textarea name="d_donor_about" id="D_DONOR_ABOUT" class="form-control" style="width:100%;resize:none" rows="10"  >{{ $cur_donor_info->d_donor_about }}</textarea>
                </div>
            </div>

        </div>
       <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-md-9"></div>
            <div class="col-md-3" align="right">
                 <button type="submit" name="btn_dave_donor" id="BTN_SAVE_DONOR"  class="btn blue">Save</button>
                <button type="button" id="BACK_FORM" name="back_form" class="btn default">Back</button>
            </div>
        </div>
    </div>
</form>
    </div>
</div>


@endsection
<?php
/***********************************************************
lstmyprojects.blade.php
 Product : 
 Version : 1.0
 Release : 2
 Date Created :  Jul 28, 2017
 Developed By  : Mohamad Mantach   PHP Department
 All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2017
 
 Page Description :
 {Enter page description Here}
 ***********************************************************/
?>

<table class="table table-striped table-bordered table-hover" id="datatable_allrequests">
<thead>
    <tr role="row" class="heading">
        <th width="5%">
            <input type="checkbox" class="group-checkable" data-set="#datatable_allrequests .checkboxes" />
        </th>
        <th width="20%"> Project ID </th>
        <th width="20%"> Project Code </th>
        <th width="20%"> Project Name </th> 
        <th width="20%"> Start Date </th> 
        <th width="20%"> End Date </th> 
        <th width="2px" nowrap>  </th>
    </tr>
    </thead>
    <tbody>
        @for($i = 0; $i < count($projects_info); $i++)
        <tr class='ProjectItem' data-pp_id="{{ $projects_info[$i]['pp_id'] }}">
            <td><input type="checkbox" class="checkboxes" name="ck_project_{{ $projects_info[$i]['pp_id'] }}" id="CK_PROJECT_{{ $projects_info[$i]['pp_id'] }}" value="{{ $projects_info[$i]['pp_id'] }}" /></td>
            <td>{{ $projects_info[$i]['pp_id'] }}</td>
            <td>{{ $projects_info[$i]['pp_project_code'] }}</td>
            <td><a class='ViewClasses' href="#">{{ $projects_info[$i]['pp_project_title']  }}</a></td>
            <td><a class='ViewClasses' href="#">{{ $projects_info[$i]['pp_project_start_date']  }}</a></td>
            <td><a class='ViewClasses' href="#">{{ $projects_info[$i]['pp_project_end_date']  }}</a></td>
            <td></td>
        </tr>
        @endfor
    </tbody>
</table>
<?php
/***********************************************************
lstmygroups.blade.php
 Product : 
 Version : 1.0
 Release : 2
 Date Created :  Jul 28, 2017
 Developed By  : Mohamad Mantach   PHP Department
 All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2017
 
 Page Description :
 {Enter page description Here}
 ***********************************************************/

?>

<table class="table table-striped table-bordered table-hover" id="datatable_allrequests">
<thead>
    <tr role="row" class="heading">
        <th width="5%">
            <input type="checkbox" class="group-checkable" data-set="#datatable_allrequests .checkboxes" />
        </th>
        <th width="20%"> Group ID </th>
        <th width="20%"> Group Name </th>
        <th width="10%"> Start Date </th>  
        <th width="10%"> End Date </th> 
        <th width="10%"></th>
        <th width="10%" nowrap>  </th>
    </tr>
    </thead>
    <tbody>
        @for($i = 0; $i < count($groups_info); $i++)
        <tr data-pg_id="{{ $groups_info[$i]['pg_id'] }}">
            <td><input type="checkbox" class="checkboxes" name="ck_group_{{ $groups_info[$i]['pg_id'] }}" id="CK_GROUP_{{ $groups_info[$i]['pg_id'] }}" value="{{ $groups_info[$i]['pg_id'] }}" /></td>
            <td>{{ $groups_info[$i]['pg_id'] }}</td>
            <td><a  href="#" class='ViewSessions'>{{ $groups_info[$i]['pg_group_title'] }}</a></td>
            <td><a  href="#" class='ViewSessions'>{{ $groups_info[$i]['pg_start_date'] }}</a></td>
            <td><a  href="#" class='ViewSessions'>{{ $groups_info[$i]['pg_end_date'] }}</a></td>
            <td><a id="submit_survey_{{ $groups_info[$i]['pg_id'] }}"  href="#">Submit Survey</a></td>
            <td><a  href="{!! url( '/GenerateClassPdf' , $groups_info[$i]['pg_id'] ) !!}" target="_blank"><i class="fa fa-eye" aria-hidden="true"> View PDF</i></a></td>
        </tr>
        @endfor
    </tbody>
</table>
<?php
/***********************************************************
thumbmygroups.blade.php
 Product : 
 Version : 1.0
 Release : 2
 Date Created :  Jul 29, 2017
 Developed By  : Mohamad Mantach   PHP Department
 All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2017
 
 Page Description :
 {Enter page description Here}
 ***********************************************************/

?>
<div style="width:100%">
<ul class="GalleryItems">
	@for($i = 0; $i < count($groups_info); $i++)
		<li  class='ProjectItem'   data-pg_id="{{ $groups_info[$i]['pg_id'] }}" >
			<img src="{{ url('imgs/classes-sessions.png') }}" style="" />
			<div class="ProjectTitle">
			<a  href="#" class='ViewSessions'>{{ $groups_info[$i]['pg_group_title'] }}</a>
			</div>
		</li>
    @endfor
</ul>
</div>

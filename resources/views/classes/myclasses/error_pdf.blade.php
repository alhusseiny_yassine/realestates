<?php
/* * *********************************************************
  class_pdf.blade.php
  Product :
  Version : 1.0
  Release : 1
  Developed By  : alhusseiny PHP Department

  Page Description :
  {Enter page description Here}
 * ********************************************************* */
?>
<style>
    .container{
        margin-top: 20%;
        text-align: center;
        color: #9c9ea2;
        
    }
    html{
        font-family :'Verdana';
    }
    footer{
        text-align: center;
        position: absolute;
        bottom: 0;
        padding: 10px;
        width: 100%
    }
    .highlight{
        color: lightblue;
    }
    body{
        background-image: url("{!! url('/imgs/404.png'); !!}");
        background: no-repeat center center fixed;
        background-size: cover;
    }
</style>
<div class='container'>
    <h1> <?php echo $error_message; ?> </h1>
</div>
<footer>
    <div> Copyright © 2017 <span class="highlight">OMSAR</span>  |  Office of the Minister of the State For Administrative Reform </div>
</footer>
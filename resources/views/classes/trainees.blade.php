<?php
/***********************************************************
trainees.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 29, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/


?>

<div class="portlet-body flip-scroll" id="LISTCLASSES">
    <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
        <thead class="flip-content">
        <tr>
            <th> Trainee Id </th>
            <th> Trainee FullName </th>
            <th> Attendance </th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ( $lst_trainees as $index => $Trainee_info )
        {
            ?>
               <tr data-trainee_id="{{ $Trainee_info->id }}">
                    <th> {{ $Trainee_info->id }} </th>
                    <th> {{ $Trainee_info->u_fullname }} </th>
                    <th> <input type="checkbox" {{ ( isset( $Session_trainee_attendance[ $Trainee_info->id ] ) && $Session_trainee_attendance[ $Trainee_info->id ] == 1 ) ? "checked" : "" }} name="ck_trainee_attendance[]" value="{{ $Trainee_info->id }}" /> </th>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>
</div>
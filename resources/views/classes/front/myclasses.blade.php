<?php


?>

@extends('layouts.layout')

@section('themes')

@endsection
@section('plugins')
	<script src="{{ url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js') }}" type="text/javascript"></script>
	<script type="text/javascript" src="{{ url('js/modules/class.module.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/classes/myclasses.js') }}"></script>
@endsection

@section('content')
<form name="frm_projects_management" id="FRM_PROJECTS_MANAGEMENT">
    <div class="portlet blue box">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i> My Classes </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
                <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                <a href="javascript:;" class="reload"> </a>
            </div>
        </div>
        <div class="portlet-body" style="height:800px;">
            <span id="hidden_fields">
                <input type="hidden" name="page_number" value="1" />
                <input type="hidden" name="display_type" value="list" />
                <input type="hidden" name="data_info" value="projects" />
                <input type="hidden" name="pp_id" value="" />
                {!! csrf_field() !!}
            </span>
            <div style="width:100%;" align="center">
            	<ul style="list-style-type: none;position:absolute;left:50%;">
            		<li style="float: left;position: relative;"><a href="#" data-data_type='list' id='LIST_INFO'><i class="fa fa-list" aria-hidden="true"></i></a></li>
            		<li style="float: left;position: relative;"><a href="#" data-data_type='thumbnails' id='THUMB_INFO'><i class="fa fa-picture-o" aria-hidden="true"></i></a></li>
            	</ul>
            </div>
            <div class='headerLink' align="left" style="clear: both;width:100%;height: 30px;">
            	
            </div>
            <div class="MyClassesInfo">
            
            </div>
            <div class="row">
             <div class="col-md-10" align="left">
                <ul id="Pagination" class="pagination-sm"></ul>
             </div>
             <div class="col-md-2" align="right">

             </div>
        </div>
    </div>
</form>
@endsection
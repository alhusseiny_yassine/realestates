<?php
/***********************************************************
classesDropdown.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 29, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

?>
<div>
    <label><b> Classes : </b></label>
    <select class="bs-select form-control" name="fk_class_id" id="FK_CLASS_ID" data-actions-box="true">
        <option value="">-- Select One -- </option>
        @foreach($ProjectClasses as $pc_index => $pc_info)
            <option value="{{ $pc_info->cls_id }}">{{ $pc_info->cls_code }}</option>
        @endforeach
    </select>
</div>

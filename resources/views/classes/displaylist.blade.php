<?php
/***********************************************************
displaylist.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Dec 25, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

?>


<div class="portlet-body flip-scroll" id="LISTCLASSES">
    <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
        <thead class="flip-content">
        <tr>
            <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_classes" id="CK_ALL_CLASSES" class="group-checkable" data-set="#LISTCLASSES .checkboxes" value="1" /></th>
            <th style="width:2px">#</th>
            <th> Session Code </th>
            <th> Session Date </th>
            <th> Session Start Time </th>
            <th> Session Duration </th>
            <th style="width:2px" nowrap>Edit</th>
            <th style="width:2px" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ( $SessionClasses as $index => $session_info )
        {
            ?>
               <tr data-sc_id="{{ $session_info->sc_id }}">
                   <td><input type="checkbox" class="checkboxes" name="ck_session_{{ $session_info->sc_id }}" id="CK_SESSION_{{ $session_info->sc_id }}" value="1" /></td>
                   <td>{{ $session_info->sc_id }}</td>
                   <td>{{ $session_info->sc_session_code }}</td>
                   <td>{{ $session_info->sc_session_date }}</td>
                   <td>{{ $session_info->sc_session_time }}</td>
                   <td>{{ $session_info->sc_session_duration }}</td>
                   <td> <a href="#"  id="EDIT_SESSION_INFO_{{ $session_info->sc_id }}" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
                   <td><a href="#"  id="DELETE_SESSION_INFO_{{ $session_info->sc_id }}" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>
</div>
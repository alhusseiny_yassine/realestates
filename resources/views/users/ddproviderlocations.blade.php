<?php
/***********************************************************
ddproviderlocations.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Apr 6, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

?>

<select name="pg_train_location" id="PG_TRAIN_LOCATION" class="form-control" style="width:100%">
    <option value="0">--Select One--</option>
     @foreach($lst_locations as $key => $pl_info)
        <option {{ $ini_location == $pl_info->pl_id ? "selected" : "" }} value="{{ $pl_info->pl_id }}">{{ $pl_info->pl_provider_location }}</option>
    @endforeach
</select>
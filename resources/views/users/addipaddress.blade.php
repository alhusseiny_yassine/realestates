<?php
/************************************************************
addmacaddress.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Sep 1, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/


?>

@extends('layouts.playout')

@section('plugins')
<script type="text/javascript" src="{{ url('assets/js/users/addIpAddress.js') }}"></script>
@endsection
@section('content')

<form name="form_add_ip_address" id="FORM_ADD_IP_ADDRESS">
 <span id="hidden_fields">
        <input type="hidden" name="user_id" id="USER_ID" value="<?php echo $user_id; ?>" />
        {!! csrf_field() !!}
    </span>
    <div class="container-fluid" style="">
    <div class="row">
        <div class="col-md-6">
            <label>IP ADDRESS</label>
        </div>
        <div class="col-md-6">
            <input type="text" class="form-control" name="user_mac_address" id="USER_MAC_ADDRESS" value="" />
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
        &nbsp;
        </div>
        <div class="col-md-6" style="float:right">
            <button type="button" class="btn btn-primary" name="btn_save_mac" id="BTN_SAVE_MAC">Add</button>
        </div>
    </div>
</div>
</form>
@endsection
<?php
/***********************************************************
addmultipleUsers.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 25, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

?>

@extends('layouts.alayout')

@section('themes')
<link href="{{ url('admin/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('plugins')
<script src="{{ url('admin/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('js/modules/users.module.js') }}"></script>
<script type="text/javascript" src="{{ url('js/users/addmultipleUsers.js') }}"></script>
@endsection
@section('content')
<div class="portlet blue box">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Add Multiple Users </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
     <div class="portlet-body">
            <form name="form_add_multiple_users" id="Form_ADD_MULTIPLE_USERS">
             <span id="hidden_fields">
                {!! csrf_field() !!}
             </span>
            <div class="row">
                <div class="col-md-12">
                    <div style="left:20%" class="UsersDataGrid">

                        <div class="portlet-body flip-scroll" id="Users">
                            <table id="USERTABLE"  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
                                <thead class="flip-content">
                                    <tr>
                                        <th style="width:2px;">#</th>
                                        <th style="width:15%">Full Name</th>
                                        <th style="width:15%">Username</th>
                                        <th style="width:15%">Password</th>
                                        <th style="width:10%">Role</th>
                                        <th style="width:15%">User Type</th>
                                        <th style="width:15%">beneficiary</th>
                                        <th style="width:15%">Email</th>
                                        <th style="width:2px;" nowrap>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                             </table>
                        </div>

                    </div>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-2"><a href="#" class="AddUserRow"> <i class="fa fa-plus-circle" aria-hidden="true"></i> Add User</a></div>
                 <div class="col-md-8"></div>
                 <div class="col-md-2" align="right">
                 <button name="btn_save_users" id="BTN_SAVE_USERS" class="btn green capitalize"  type="button">Save Users</button>
                 </div>
             </div>
             </form>
     </div>
  </div>

  @endsection
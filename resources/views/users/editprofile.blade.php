<?php
/************************************************************
editprofile.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 6, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/


?>

@extends('layouts.alayout')

@section('content')
<script type="text/javascript" src="{{ url('assets/js/users/editProfile.js') }}"></script>
<form name="form_edit_profile" id="FORM_EDIT_PROFILE">
    <span id="hidden_fields">
        <input type="hidden" name="user_id" id="USER_ID" value="<?php echo $userId; ?>" />
        {!! csrf_field() !!}
    </span>
    <div class="container-fluid">
         <div class="row">
            <div class="col-md-12"><h3>Edit Profile</h3></div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label>Username</label>
                <input type="text" name="username" id="USERNAME" class="form-control" value="<?php echo $users[0]->u_username; ?>" />
            </div>
            <div class="col-md-4">
                <label>FullName</label>
                <input type="text" name="fullname" id="FULLNAME" class="form-control" value="<?php echo $users[0]->u_fullname; ?>" />
            </div>
            <div class="col-md-4">
                <label>password</label>
                <input type="password" name="u_password" id="U_PASSWORD" class="form-control" value="" />
            </div>
        </div>
        <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-md-4">
                <label>retype - password</label>
                <input type="password" name="u_retype_password" id="U_RETYPE_PASSWORD" class="form-control" value="" />
            </div>
            <div class="col-md-4">
                <label>Email</label>
                <input type="email" name="u_email" id="U_EMAIL" class="form-control" value="<?php echo $users[0]->u_email; ?>" />
            </div>
            <div class="col-md-4">
                 <label>Website</label>
                 <input type="url" name="u_website" id="U_WEBSITE" class="form-control" value="<?php echo $users[0]->u_website; ?>" />
            </div>
        </div>
        <div class="row" style="height:5px;"></div>
        <div class="row">
             <div class="col-md-4">
                <label>mobile</label>
                <input type="text" name="u_mobile" id="U_MOBILE" class="form-control" value="<?php echo $users[0]->u_mobile; ?>" />
             </div>
             <div class="col-md-4">
                <label>Phone</label>
                <input type="text" name="u_phone" id="U_PHONE" class="form-control" value="<?php echo $users[0]->u_phone; ?>" />
             </div>
             <div class="col-md-4">
                &nbsp;
             </div>
        </div>
        <div class="row" style="height:14px;"></div>
         <div class='row'>
                <div class="col-md-12">
                    <span class="btn btn-success fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>Select files...</span>
                        <!-- The file input field used as target for the file upload widget -->
                        <input id="fileupload" data-url="{{ url('upload/profile') }}" type="file" name="files" />
                    </span>
                    <br>
                    <br>
                    <!-- The global progress bar -->
                    <div id="progress" class="progress">
                            <div class="progress-bar progress-bar-success"></div>
                    </div>
                    <img src="<?php echo $profileLink; ?>" width="100" ID="PROFILE_PICTURE" />
                </div>
            </div>
        <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-md-10"></div>
            <div class="col-md-2"><input type="button" name="btn_edit_user" id="BTN_EDIT_USER" class="btn btn-primary" value="SAVE" /> </div>
        </div>
    </div>
</form>
@endsection
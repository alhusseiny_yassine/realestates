<?php
/***********************************************************
userRow.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 25, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

?>

<tr>
    <td style="width:2px;">#</td>
    <td><input type="text" name="u_fullname[]" value="" class="form-control" />&nbsp;</td>
    <td><input type="text" name="u_username[]" value="" class="form-control" />&nbsp;</td>
    <td><input type="password" name="password[]" value="" class="form-control" />&nbsp;</td>
    <td>
        <select name="fk_role_id[]"  class="form-control" style="width:100%">
            <option value="">--Select One--</option>
            <?php
                for ($i=0;$i<count($roles);$i++)
                {
                    ?>
                        <option value="<?php echo $roles[$i]->role_id; ?>"><?php echo $roles[$i]->role_name; ?></option>
                    <?php
                }
            ?>
        </select>
    </td>
    <td>
        <select class="bs-select form-control" name="fb_user_type[]" data-actions-box="true">
                <option value="">No User Type</option>
                @foreach($lst_user_types as $ut_index => $ut_info)
                    <option value="{{ $ut_info['ut_id'] }}">{{ $ut_info['ut_user_type'] }}</option>
                @endforeach
        </select>
    </td>
    <td>
        <select name="fk_beneficiary_id[]"  class="form-control" style="width:100%">
                <option value="">-- Select One --</option>
                @foreach($beneficiaries as $sb_index => $ben_info)
                    <option value="{{ $ben_info['sb_id'] }}">{{ $ben_info['sb_name'] }}</option>
                @endforeach
            </select>
    </td>
    <td><input type="email" name="u_email[]" value="" class="form-control" /></td>
    <td style="width:2px;" nowrap><a href="#" class="DeleteUser" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
</tr>
<?php
/***********************************************************
traineehistory.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 23, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

?>
@extends('layouts.alayout')

@section('themes')
    <link href="{{ url('admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('plugins')
    <script src="{{ url('admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
    <script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>

@endsection
@section('content')
<div class="tabbable-line">
    <ul class="nav nav-tabs ">
        <li class="active">
            <a href="#tabTraineeRequests" data-toggle="tab"> Trainee Requests </a>
        </li>
        <li>
            <a href="#tabTraineeProjects" data-toggle="tab"> Trainee Projects </a>
        </li>
        <li>
            <a href="#tabChangingHistory" data-toggle="tab"> Trainee Changing History </a>
        </li>
        
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tabTraineeRequests">
            <table class="table table-striped table-bordered table-hover" style="width:100%">
                <tr role="row" class="heading"> 
                    <th width="20%"> Request Title </th>
                    <th width="20%"> Request&nbsp;Date </th>
                    <th width="20%"> Status </th>
                </tr>
                 @for($i = 0; $i < count($requestsHistory); $i++)
                <tr style="background-color: {{ $request_statuses_byid[ $requestsHistory[$i]->fk_status_id ]['status_color'] }}">
                    <td>{{ $requestsHistory[$i]->r_request_title }}</td>
                    <td>{{ $requestsHistory[$i]->r_request_train_date }}</td>
                    <td>{{ $request_statuses_byid[ $requestsHistory[$i]->fk_status_id ]['status_name'] }}</td>
                </tr>
                @endfor
            </table>
            
            
        </div>
        <div class="tab-pane" id="tabTraineeProjects">
             <table class="table table-striped table-bordered table-hover" style="width:100%">
                <tr role="row" class="heading"> 
                    <th width="5%"></th>
                    <th width="15%">Project Code</th>
                    <th width="15%">Project Title</th>
                    <th width="30%">Project Description</th>
                    <th width="15%">Start Date</th>
                    <th width="15%">Ende Date</th>
                </tr>
                @foreach($lst_projects as $project_id => $project_info)
                @if( in_array( $project_id , $projects_array ) )
                <tr>
                    <td>{!! $project_info['pp_id'] !!}</td>
                    <td>{!! $project_info['pp_project_code'] !!}</td>
                    <td>{!! $project_info['pp_project_title'] !!}</td>
                    <td>{!! $project_info['pp_project_description'] !!}</td>
                    <td>{!! $project_info['pp_project_start_date'] !!}</td>
                    <td>{!! $project_info['pp_project_end_date'] !!}</td>
                </tr>
                @endif
                @endforeach
            </table>
        </div>
        <div class="tab-pane" id="tabChangingHistory">
             <table class="table table-striped table-bordered table-hover" style="width:100%">
                <tr role="row" class="heading"> 
                    <th width="15%">Date</th>
                    <th width="15%">Benificary</th>
                    <th width="15%">Coordinator</th>
                    <th width="10%">EMP Degree</th>
                    <th width="5%">Goverment Grade</th>
                    <th width="15%">Service</th>
                    <th width="15%">Bureau</th>
                    <th width="30%">Residental Area</th>
                </tr>
                @foreach($trainee_history as $index => $history_info)
                <tr>
                    <td>{!! $history_info->th_date_history !!}</td>
                    <td>{!! ( isset($lst_beneficaries[$history_info->fk_benf_id] ) ) ? $lst_beneficaries[$history_info->fk_benf_id]['sb_name'] : '' !!}</td>
                    <td>{!! ( isset($lst_coordinators[$history_info->fk_coordinator_id] ) ) ? $lst_coordinators[$history_info->fk_coordinator_id]['u_fullname'] : '' !!}</td>
                    <td>{!! $history_info->th_emp_degree !!}</td>
                    <td>{!! $history_info->th_gov_grade !!}</td>
                    <td>{!! $history_info->th_trainee_service !!}</td>
                    <td>{!! $history_info->th_trainee_bureau !!}</td>
                    <td>{!! $history_info->th_residential_area !!}</td>
                </tr>
                @endforeach
            </table>
        </div>
        
    </div>
</div>
@endsection
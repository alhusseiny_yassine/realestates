<?php
/***********************************************************
coordinators_efields.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 23, 2016
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/
{
    if(!isset($user_info))
    {
        $user_info = new Users();
    }
}
?>


 <div class="col-md-4">
       <div class="form-group">
        <label class="control-label">Coordinator Code <a class="GenerateCoordinatorCode" href="#">Generate Code</a></label>
        <input type="text" name="u_coordinator_code" id="U_COORDINATOR_CODE" maxlength="32" class="form-control" value="{{ $user_info->u_coordinator_code }}" />
    </div>
</div>
 <div class="col-md-4">
      <div class="form-group">
      <label  class="control-label">Beneficiaries <span class="required"> * </span></label>
            <select name="fk_beneficiary_id" id="FK_BENEFICIARY_ID" class="form-control" style="width:100%">
                <option value="">-- Select One --</option>
                @foreach($beneficiaries as $sb_index => $ben_info)
                    <option {{ $user_info->fk_beneficiary_id ==  $ben_info['sb_id'] ? "selected" : "" }} value="{{ $ben_info['sb_id'] }}">{{ $ben_info['sb_name'] }}</option>
                @endforeach
            </select>
      </div>
</div>

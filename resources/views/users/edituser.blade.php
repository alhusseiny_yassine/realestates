<?php
/************************************************************
edituser.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>

@extends('layouts.alayout')

@section('themes')
<link href="{{ url('admin/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('admin/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('plugins')
    <script src="{{ url('admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
    <script type="text/javascript"  src="{{ url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>

    <script src="{{ url('admin/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ url('js/modules/users.module.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/users/edituser.js') }}"></script>

@endsection
@section('content')
<?php
$image_src_url  = url('/')."/".Config::get('constants.PROFILE_PICTURE_PATH').$user_info->u_avatar_base_src.$user_info->u_avatar_filename.".".$user_info->u_avatar_extentions;
$image_src_path = public_path(). "/" .Config::get('constants.PROFILE_PICTURE_PATH').$user_info->u_avatar_base_src.$user_info->u_avatar_filename.".".$user_info->u_avatar_extentions;
        if(file_exists($image_src_path)){
            $img_src = $image_src_url;
        }else{
            $img_src = url('/'). "/imgs/avatar.png" ;
        }
?>
<div class="portlet blue box">
<div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Edit User </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
    <div class="portlet-body">
        <form name="form_edit_user" id="FORM_SAVE_USER">
            <div  class="form-body">
             <span id="hidden_fields">
                {!! csrf_field() !!}
                <input type="hidden" name="user_id" id="USER_ID" value="{{ $user_id }}" />
                <input type="hidden" name="u_user_type" id="U_USER_TYPE" value="{{ $user_info->u_user_type }}" />
             </span>
                    <div class="alert alert-success" style="display:none">
        				<strong>Success!</strong> User Information is saved successfully!
        			</div>
        			<div class="alert alert-danger" style="display:none">
        				<strong>Error!</strong> You have some form errors. Please check below.
        			</div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                    <img id="PROFILE_PIC" src="{{ $img_src }}" alt="" /> </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>

                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="clearfix margin-top-10">
                                <div>
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new"> Select image </span>
                                        <input type="file" name="u_profile_pic" id="U_PROFILE_PIC" /> </span>
                                </div>
                                <br>
                                <span class="label label-danger"> NOTE! </span><br><br>
                                <span> Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Full Name <span class="required"> * </span></label>
                            <input type="text" maxlength="500" name="fullname" id="FULLNAME" class="form-control" value="{{ $user_info->u_fullname }}" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Email <span class="required"> * </span></label>
                            <input type="email" maxlength="255" name="u_email" id="U_EMAIL" class="form-control" value="{{ $user_info->u_email }}" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">password</label>
                            <input type="password" name="u_password" maxlength="150" id="U_PASSWORD" class="form-control" value="" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Re-type password</label>
                            <input type="password" name="retype_u_password" maxlength="150" id="RETYPE_U_PASSWORD" class="form-control" value="" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label class="control-label">Employee Id </label>
                            <input type="text" maxlength="500" name="u_employee_id" id="U_EMPLOYEE_ID" class="form-control" readonly="readonly" value="{{ $user_id }}" />
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Gender</label><br/>

                            <div class="mt-radio-list" data-error-container="#GenderError">
                                    <div class="row">
                                        <div class="col-md-6">
                                         <input type="radio" {{ $user_info->u_gender == 'm' ? "checked" : "" }} checked="checked" name="u_gender"  value="m" /> Male
                                        </div>
                                        <div class="col-md-6">
                                        <input type="radio" {{ $user_info->u_gender == 'f' ? "checked" : "" }} name="u_gender"  value="f" /> Female
                                        </div>
                                    </div>

                                </div>
                                <div id="GenderError"></div>
                        </div>
                    </div>
                </div>
               <div class="row" style="height:5px;"></div>
               <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label> User Type <span class="required"> * </span></label>
                            <select class="bs-select form-control" name="fb_user_type" id="FB_USER_TYPE" data-actions-box="true">
                                    <option value="">No User Type</option>
                                    @foreach($lst_user_types as $ut_index => $ut_info)
                                        <option {{ $user_info->u_user_type == $ut_info['ut_id'] ? "selected" : "" }} value="{{ $ut_info['ut_id'] }}" > {{ $ut_info['ut_user_type'] }}</option>
                                    @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                         <div class="form-group">
                            <label class="control-label">Role <span class="required"> * </span></label>
                             <select name="u_role" id="U_ROLE" class="form-control" style="width:100%">
                                <option value="">--Select One--</option>
                                <?php
                                    for ($i=0;$i<count($roles);$i++)
                                    {
                                        ?>
                                            <option {{ $user_info->fk_role_id == $roles[$i]->role_id ? "selected" : "" }} value="<?php echo $roles[$i]->role_id; ?>"><?php echo $roles[$i]->role_name; ?></option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Residential Area</label>
                            <textarea style="width:100%;height: 100px;resize:none" class="form-control" name="u_residential_area" id="U_RESIDENTIAL_AREA">{{ $user_info->u_residential_area }}</textarea>
                            <input type="hidden" name="ini_residential_area" value="{{ $user_info->u_residential_area }}" />
                        </div>
                    </div>
                </div>
               <div class="row" style="height:5px;"></div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">mobile</label>
                            <input type="text" maxlength="20" name="u_mobile" id="U_MOBILE" class="form-control" value="{{ $user_info->u_mobile }}" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Phone</label>
                            <input type="text" maxlength="20" name="u_phone" id="U_PHONE" class="form-control" value="{{ $user_info->u_phone }}" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Fax</label>
                            <input type="text" maxlength="20" name="u_fax" id="U_FAX" class="form-control" value="{{ $user_info->u_fax }}" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Year Of Birth <span class="required"> * </span></label>
                            <?php
                                $time = strtotime($user_info->u_date_birth );
                                $year_of_birth = date("Y",$time);
                                $cur = date("Y");
                                $cur = (integer)$cur;
                                $first = $cur - 70;
                                $cur = $cur - 20;
                            ?>
                            <select name='u_date_birth' class="form-control" id="U_DATE_BIRTH" >
                                <?php
                                    for ($i = $first; $i < $cur; $i++)
                                    {
                                        ?>
                                            <option {{ $i == $year_of_birth ? "selected" : "" }} value="{{ $i }}">{{ $i }}</option>
                                        <?php
                                    }
                                ?>
                            </select>
                            <!-- <input style="display:none" type="text" name="u_date_birth" maxlength="10" id="U_DATE_BIRTH" class="form-control" value="{{ $user_info->u_date_birth }}" /> -->
                        </div>
                    </div>
                </div>
                <div class="row" style="height:5px;"></div>
                <div class="row">


                </div>
                <div class="row" style="height:5px;"></div>
                <div class="row">
                    <div class="col-md-4" style="height: 40px;">
                       <label class="control-label">Enable User</label>
                        <input type="checkbox" {{ $user_info->u_is_active == 1 ? 'checked' : '' }} name="u_is_active" value='1' />
                    </div>
                    <div class="col-md-4">

                    </div>
                    <div class="col-md-4">

                    </div>
                </div>
                <div class="row" style="height:10px;"></div>
                @if($user_info->u_user_type  == 4)
                <div class="row">
                    <div class="EducationInfo">

                    </div>
                </div>
                <div class="row" style="height:10px;"></div>
                @endif

                @if($user_info->u_user_type  == 3)
                <div class="row">
                    <div class="ProviderLocation">

                    </div>
                </div>
                <div class="row" style="height:10px;"></div>
                @endif
                <div class="row">
                    <div class="col-md-12 ExtraFieldSection">

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-9" align="right">
                            <button type="button" style="display:none" id="BTN_TRAINEE_HISTORY" name="btn_trainee_history" class="btn green">Trainee History</button>
                            <button type="button" style="display:none" id="BTN_ADD_NEW_TRAINEE_REQUEST" name="btn_add_new_trainee_request" class="btn green">Add New Trainee Request</button>
                            <button type="submit" id="BTN_EDIT_USER" name="btn_edit_user" class="btn blue capitalize">Save Change</button>
                            <button type="button" id="BACK_FORM" name="back_form" class="btn default">Back</button>
                    </div>
                </div>
            </div>
        </form>
     </div>
</div>
@endsection
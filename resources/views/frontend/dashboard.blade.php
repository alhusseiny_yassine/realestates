<?php
/***********************************************************
dashboard.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 30, 2016
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2016

Page Description :
Dashboard For Front end
***********************************************************/
 
?>


@extends('layouts.layout')

@section('plugins')
        <script type="text/javascript" src="{{ url('js/modules/front-dashboard.module.js') }}"></script>

    <script src="{{ url('admin/assets/pages/scripts/front-dashboard.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/amcharts/amcharts/amcharts.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/amcharts/amcharts/serial.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/amcharts/amcharts/pie.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/amcharts/amcharts/radar.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/amcharts/amcharts/themes/light.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/amcharts/amcharts/themes/patterns.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/amcharts/amcharts/themes/chalk.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/amcharts/ammap/ammap.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/amcharts/ammap/maps/js/worldLow.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/amcharts/amstockcharts/amstock.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/fullcalendar/fullcalendar.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/horizontal-timeline/horizontal-timeline.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/flot/jquery.flot.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js') }}" type="text/javascript"></script>
    <script src="{{ url('admin/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js') }}" type="text/javascript"></script>
    


     <script src="{{ url('admin/assets/pages/scripts/dashboard.min.js') }}" type="text/javascript"></script>

@endsection

@section('content')
                {!! $dashboard_header !!}
                <div class="row widget-row">
                    <div class="col-md-6">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption caption-md font-blue">
                                    <i class="icon-share font-blue"></i>
                                    <span class="caption-subject theme-font bold uppercase">Recent Notifications</span>
                                </div>
                                <div class="actions">
                                    <div class="btn-group">
                                        <a class="btn btn-sm btn-default dropdown-toggle" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Filter By
                                            <i class="fa fa-angle-down"></i>
                                        </a>
                                        <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
                                           <!--  <label>
                                                <input type="checkbox" /> Finance</label>
                                            <label>
                                                <input type="checkbox" checked="" /> Membership</label>
                                            <label>
                                                <input type="checkbox" /> Customer Support</label>
                                            <label>
                                                <input type="checkbox" checked="" /> HR</label>
                                            <label>
                                                <input type="checkbox" /> System</label> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="scroller" style="height: 322px;" data-always-visible="1" data-rail-visible="0">
                                    <ul class="feeds">
                                    <!-- lstNotifications -->
                                    @foreach($lstNotifications as $key => $notInfo )
                                        <li>
                                            {!! substr($notInfo->nt_notification,0,50) !!}<span class="badge" style="float:right">{{ \Carbon\Carbon::parse($notInfo->nt_notification_date)->diffForHumans() }}</span>
                                        </li>
										@endforeach
                                    </ul>
                                </div>
                                <div class="scroller-footer">
                                    <div class="btn-arrow-link pull-right">
                                        <a href="{{ url('users/notifications') }}">See All Notifiations</a>
                                        <i class="icon-arrow-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                     @if(Session('ut_user_type') == \App\model\Users\UserTypes::TRAINEES_USER_TYPE_ID  ) 
                <div class="col-md-6 ProjectNotifications">
                   @include('frontend.dashboard.requests_action', ['projects_notifications_pending' => $projects_notifications_pending , 'projects_notifications' => $projects_notifications , 'users_array' => $users_array ])
                </div>
                @endif
                </div>
                
@endsection
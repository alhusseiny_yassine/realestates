<?php
/************************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>
<div class="portlet-body flip-scroll" id="LISTSESSIONSTATUSES">
    <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
        <thead class="flip-content">
        <tr>
            <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_sessionstatuses" id="CK_ALL_SESSIONSTATUSES" class="group-checkable" data-set="#LISTSESSIONSTATUSES .checkboxes" value="1" /></th>
            <th style="width:2px">#</th>
            <th>Session Status Name </th>
            <th>Session Status color</th>
            <th style="width:2px" nowrap>Edit</th>
            <th style="width:2px" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($session_statuses as $index => $session_status_info)
        {
            ?>
               <tr data-ss_id="{{ $session_status_info->ss_id }}">
                   <td><input type="checkbox" class="checkboxes" name="ck_session_status_{{ $session_status_info->ss_id }}" id="CK_SESSION_STATUS_{{ $session_status_info->ss_id }}" value="1" /></td>
                   <td><?php echo $session_status_info->ss_id; ?></td>
                   <td><?php echo $session_status_info->ss_status_name; ?></td>
                   <td style="background-color:<?php echo $session_status_info->ss_status_color; ?>;color:white;"><?php echo $session_status_info->ss_status_color; ?></td>
                   <td> <a href="#"  id="EDIT_SESSION_STATUS_{{ $session_status_info->ss_id }}" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
                   <td><a href="#"  id="DELETE_SESSION_STATUS_{{ $session_status_info->ss_id }}" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>
</div>
<?php
/***********************************************************
ajax.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 22, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :

***********************************************************/

Route::post('request/Login','Auth\LoginController@Login');
Route::post('request/displayListUsers','Users\UsersController@DisplayListUsers');
Route::post('request/GetProvincesDistricts','Utilities\DropDownController@GetDistrictsProvince');
Route::post('request/DisplayUserExtraFields','Users\UsersController@DisplayFieldsByUserType');
Route::post('request/GenerateCoordinatorCode','Utilities\GeneralController@GenerateCoordinatorCode');
Route::post('request/checkUserName','Utilities\GeneralController@CheckIfUsernameExist');
Route::post('request/AddNewUser','Users\UsersController@SaveUserInfo');
Route::post('request/SaveUserInfo','Users\UsersController@SaveUserInfo');
Route::post('request/DeleteUser','Users\UsersController@DeleteUser');
Route::post('request/getUserRow','Users\UsersController@GetUserRow');
Route::post('request/SaveMultipleUsers','Users\UsersController@SaveMultipleUsers');
Route::post('request/DeleteMultipleUsers','Users\UsersController@DeleteMultipleUsers');
Route::post('request/saveprofileinfo','Users\UsersController@SaveProfileInfo');
Route::post('request/changePassword','Users\UsersController@ChangePasswordInfo');
Route::post('request/DisplayListUserEduccation','Users\UsersController@DisplayListUserEduccation');
Route::post('request/SaveEducationInfo','Users\UsersController@SaveEducationInfo');
Route::post('request/DeleteUserEducation','Users\UsersController@DeleteUserEducationInfo');
Route::post('request/GetCoordinatorsDropdown','Users\UsersController@GetCoordinatorsDropdown');
Route::post('request/SendResetAccount','Users\UsersController@SendEmailforResetPassword');

Route::post('request/GenerateDropdown/{key}', 'Utilities\DropDownController@GenerateDropDown');

//Trainers////
Route::post('request/DisplayListTrainers','Users\TrainersController@DisplayListTrainers');
Route::post('request/SaveTrainerInfo','Users\TrainersController@SaveTrainerInfo');
Route::post('request/DeleteTrainer','Users\TrainersController@DeleteTrainer');

Route::post('request/displayListRoles','Roles\RolesController@DisplayListRoles');
Route::post('request/addRole','Roles\RolesController@RequestAddRole');
Route::post('request/RequestEditRole','Roles\RolesController@RequestEditRole');
Route::post('request/DeleteRole','Roles\RolesController@RequestDeleteRole');


Route::post('request/displayListBeneficiaries','Admin\BeneficiariesController@DisplayListBeneficiaries');
Route::post('request/SaveBenfInfo','Admin\BeneficiariesController@SaveBeneficiaryInfo');
Route::post('request/DeleteBeneficary','Admin\BeneficiariesController@DeleteBeneficaryInfo');
Route::post('request/getBeneficiaryDiractorateDropdown','Admin\BeneficiariesController@GetBeneficieryDiractorates');


Route::post('request/displayListJobTitle','Admin\JobTitlesController@DisplayListJobtitles');
Route::post('request/SaveJobTitleInfo','Admin\JobTitlesController@SaveJobTitleInfo');
Route::post('request/DeleteJobTitle','Admin\JobTitlesController@DeleteJobTitleInfo');



Route::post('request/DisplayListProviderLocations','System\ProviderLocationController@DisplayListProviderLocation');
Route::post('request/SaveProviderLocationInfo','System\ProviderLocationController@SaveProviderLocationInfo');
Route::post('request/DeleteProviderLocation','System\ProviderLocationController@DeleteProviderLocationInfo');
Route::post('provider/DisplayProviderLocationDropdown','System\ProviderLocationController@DisplayProviderlocationDropdown');


Route::post('request/displayListJobRole','Admin\JobRolesController@DisplayListJobRoles');
Route::post('request/SaveJobRoleInfo','Admin\JobRolesController@SaveJobRoleInfo');
Route::post('request/DeleteJobRole','Admin\JobRolesController@DeleteJobRoleInfo');

Route::post('request/displayListTaskList','Admin\TasksListController@DisplayListTasksList');
Route::post('request/SaveTaskListInfo','Admin\TasksListController@SaveTasksListInfo');
Route::post('request/DeleteTaskList','Admin\TasksListController@DeleteTaskListInfo');

Route::post('request/displayListEducationDegrees','System\EducationDegreeController@DisplayListEducationDegree');
Route::post('request/SaveEducationDegreeInfo','System\EducationDegreeController@SaveEducationDegreeInfo');
Route::post('request/DeleteEducationDegree','System\EducationDegreeController@DeleteEducationDegreeInfo');


Route::post('request/displayListBeneficiaryTypes','Admin\BeneficiaryTypesController@DisplayListBeneficiaryTypes');
Route::post('request/SaveBenfTypeInfo','Admin\BeneficiaryTypesController@SaveBeneficiaryTypeInfo');
Route::post('request/DeleteBeneficaryType','Admin\BeneficiaryTypesController@DeleteBeneficaryTypeInfo');


Route::post('request/displayListBeneficiaryDirectorates','Admin\BeneficiaryDirectorateController@DisplayListBeneficiaryDirectorates');
Route::post('request/SaveBenfDirInfo','Admin\BeneficiaryDirectorateController@SaveBeneficiaryDirectorateInfo');
Route::post('request/DeleteBeneficaryDirectorate','Admin\BeneficiaryDirectorateController@DeleteBeneficaryDirInfo');


Route::post('request/displaycoursetypes','Courses\CourseTypesController@DisplayListCourseTypes');
Route::post('request/savecoursetype','Courses\CourseTypesController@SaveCourseTypeInfo');
Route::post('request/DeleteCourseType','Courses\CourseTypesController@DeleteCourseTypeInfo');

Route::post('request/displaycoursecategories','Courses\CourseCategoriesController@DisplayListCourseCategories');
Route::post('request/savecoursecategory','Courses\CourseCategoriesController@SaveCourseCategoryInfo');
Route::post('request/DeleteCourseCategory','Courses\CourseCategoriesController@DeleteCourseCategoryInfo');

Route::post('request/displaylistcourses','Courses\CourseController@DisplayListCourses');
Route::post('request/savecourse','Courses\CourseController@SaveCourseInfo');
Route::post('request/DeleteCourse','Courses\CourseController@DeleteCourseInfo');


Route::post('request/displaylistcountries','Configuration\CountryController@DisplayListCountries');
Route::post('request/savecountry','Configuration\CountryController@SaveCountryInfo');
Route::post('request/DeleteCountry','Configuration\CountryController@DeleteCountryInfo');



Route::post('request/displayprojecttypes','Projects\ProjectTypesController@DisplayListProjectTypes');
Route::post('request/saveprojecttype','Projects\ProjectTypesController@SaveProjectTypeInfo');
Route::post('request/DeleteProjectType','Projects\ProjectTypesController@DeleteProjectTypeInfo');


Route::post('request/displayprojectstatuses','Projects\ProjectStatusesController@DisplayListProjectStatuses');
Route::post('request/saveprojectStatus','Projects\ProjectStatusesController@SaveProjectStatusInfo');
Route::post('request/DeleteProjectStatus','Projects\ProjectStatusesController@DeleteProjectStatusInfo');

Route::post('request/displayprovinces','Admin\ProvincesController@DisplayListProvinces');
Route::post('request/saveprovince','Admin\ProvincesController@SaveProvinceInfo');
Route::post('request/DeleteProvince','Admin\ProvincesController@DeleteProvinceInfo');


Route::post('request/displaydistricts','Admin\DistrictsController@DisplayListDistricts');
Route::post('request/savedistrict','Admin\DistrictsController@SaveDistrictInfo');
Route::post('request/DeleteDistrict','Admin\DistrictsController@DeleteDistrictInfo');


Route::post('request/displaysessionstatuses','Sessions\SessionStatusesController@DisplayListSessionStatuses');
Route::post('request/saveSessionStatus','Sessions\SessionStatusesController@SaveSessionStatusInfo');
Route::post('request/DeleteSessionStatus','Sessions\SessionStatusesController@DeleteSessionStatusInfo');

Route::post('request/displayrequeststatuses','Requests\RequestStatusesController@DisplayListRequestStatuses');
Route::post('request/saveRequestStatus','Requests\RequestStatusesController@SaveRequestStatusInfo');
Route::post('request/DeleteRequestStatus','Requests\RequestStatusesController@DeleteRequestStatusInfo');


Route::post('request/displayListDonors','Admin\DonorsController@DisplayListDonors');
Route::post('request/savedonors','Admin\DonorsController@SaveDonorInfo');
Route::post('request/DeleteDonor','Admin\DonorsController@DeleteDonorInfo');

Route::post('request/ajaxsaveConfiguration', 'Utilities\ConfigurationController@AjaxSaveConfiguration');

Route::post('request/SaveRequestInformation','Requests\RequestsController@SaveRequestInfo');


Route::post('omsar/registration','Front\IndexController@TraineesRegistration');
Route::post('omsar/login','Auth\LoginController@TraineesLogin');
Route::post('omsar/dashboard','Front\IndexController@OmsarDashboard');
Route::post('omsar/coursesearchresults','Courses\CourseController@CourseSearchResults');
Route::post('courses/runningcoursesearchresults','Projects\ProjectCoursesController@CourseSearchResults');


Route::post('request/getCourseCategoriesDropdown','Courses\CourseController@CourseCategoriesDropDown');



Route::post('request/sendtrainrequest','Requests\RequestsController@SendRequest');
Route::post('request/displaylistMyRequests','Requests\RequestsController@DisplayMyRequestsList');
Route::post('request/displaylistAllRequests','Requests\RequestsController@DisplayAllRequestsList');
Route::post('request/displayRequestsDashboard','Requests\RequestsController@RequestsDashboard');
Route::post('request/sendrejectionrequest','Requests\RequestsController@SendRejectionNotifications');
Route::post('request/SendApprovalRequest','Requests\RequestsController@SendApprovalRequest');
Route::post('request/changerequeststatus','Requests\RequestsController@SaveChangeRequestStatus');
Route::post('request/DeleteRequest','Requests\RequestsController@DeleteRequestInformation');

Route::post('request/displaylistProjects','Projects\ProjectsController@DisplayListProjects');
Route::post('request/CreateNewProject','Projects\ProjectsController@SaveProjectInfo');
Route::post('project/DisplayProjectConfigurationTab','Projects\ProjectsController@DisplayConfigurationTab');
Route::post('project/SaveProjectConfiguration','Projects\ProjectsController@SaveProjectConfiguration');
Route::post('request/project/DisplayProjectCourseInformation','Projects\ProjectsController@DisplayProjectCourseInformation');
Route::post('request/SaveProjectCourseInformation','Projects\ProjectsController@SaveProjectCourseInformation');
Route::post('request/GetListSessionsCalendar','Projects\ProjectsController@GetListSessionsCalendar');
Route::post('request/changeprojectstatus','Projects\ProjectsController@ChangeProjectStatus');
Route::post('request/deleteProject','Projects\ProjectsController@DeleteProject');
Route::post('session/SaveSessionInformation','Sessions\SessionsController@SaveSessionInformation');
Route::post('request/ChangeSessionTime','Sessions\SessionsController@ChangeSaveInformation');
Route::post('request/DeleteGroupInfo','Projects\ProjectsController@DeleteGroupInfo');
Route::post('request/DisplayListGroups','Projects\GroupsController@DisplayListGroups');
Route::post('request/SaveTraineesGroup','Projects\GroupsController@SaveGroupTrainees');
/// Groups ////
Route::post('request/DisplayGroups','Projects\GroupsController@DisplayGroups');
Route::post('request/DisplayProjectCourses','Projects\GroupsController@DisplayProjectCourses');
/////////
Route::get('request/SelectProjectTrainees/{project_id}/{pc_id}','Projects\ProjectsController@SelectProjectTrainees');
Route::get('request/DisplayGroupInfo/{pg_id}','Projects\GroupsController@DisplayGroupInfo');
Route::post('request/AddCourseTrainees','Courses\CourseController@AddTraineesCourse');


Route::post('request/admin/displaylistProjects','Projects\ProjectsController@DisplayListAdminProjects');
Route::post('request/AdminSaveProjectInfo','Projects\ProjectsController@SaveProjectInfo');
Route::post('project/SaveAdminProjectConfiguration','Projects\ProjectsController@SaveAdminProjectConfiguration');
Route::post('request/GetCoursesDropdown','Classes\ClassesController@GetCoursesDropdown');
Route::post('request/displaylistClassSessions','Classes\ClassesController@DisplayListSessions');
Route::post('request/DeleteSessionClass','Classes\ClassesController@DeleteSessionClass');


Route::post('trainees/DisplayrofileDashboard','Front\ProfileController@DisplayMyProfileDashboard');
Route::post('trainees/DisplayProfileAccountSettings','Front\ProfileController@DisplayMyProfileAccount');
Route::post('trainees/SaveProfileInformation','Front\ProfileController@SaveProfileInformation');
Route::post('trainees/ChangeUserPassword','Front\ProfileController@ChangePassword');


Route::post('request/displaylistprojectcoursestatuses','Projects\TraineesCourseStatusController@DisplayListStauses');
Route::post('request/saveprojectcoursestatus','Projects\TraineesCourseStatusController@SaveCourseStatusInfo');
Route::post('request/deleteprojectcoursestatus','Projects\TraineesCourseStatusController@DeleteTraineesCourseStatusInfo');
Route::post('request/CreateNewGroup','Classes\ClassesController@SaveGroupInformation');
Route::post('request/DisplayListProjectGroups','Classes\ClassesController@DisplayListGroups');
Route::post('request/AddNewTraineeToGroup','Classes\ClassesController@AddNewTraineeToGroup');
Route::post('request/DeleteTraineeFromGroup','Classes\ClassesController@DeleteTraineeFromGroup');


Route::post('request/GetClassesDropdown','Classes\ClassesController@GetClassesDropdown');
Route::post('request/GetClassesDropdownFront','Classes\ClassesController@GetClassesDropdownFront');
Route::post('request/GetSessionDropdown','Sessions\SessionsController@GetSessionsDropdown');
Route::post('request/DisplayListClassTrainees','Sessions\SessionsController@DisplayListClassTrainees');
Route::post('request/SaveTraineeAttendances','Sessions\SessionsController@SaveTraineeAttendances');
Route::post('request/GetSessionsTestDropdown','Sessions\SessionsController@GetSessionsTestDropdown');
Route::post('request/DisplayListClassTraineesTestResult','Sessions\SessionsController@TraineesTestResults');
Route::post('request/SaveTraineeResults','Sessions\SessionsController@SaveTraineeResults');


Route::post('dashboard/GetRequestsDataInfo','Admin\DashboardController@GetRequestsDataInfo');





Route::post('uploads/{key}', 'Utilities\UploaderController@index');



Route::post('administrator/displayListSliders','CMS\CMSSliderController@displayListSliders');
Route::post('administrator/DeleteSliderInfo','CMS\CMSSliderController@ajaxDeleteSliderInfo');
Route::post('request/ajaxAddNewSider','CMS\CMSSliderController@ajaxAddNewSider');
Route::post('request/ajaxEditSliderInfo','CMS\CMSSliderController@ajaxEditSliderInfo');
Route::post('request/displaylistImagesSlider', 'CMS\CMSSliderController@DisplayListImagesSlider');
Route::delete('administrator/DeleteMedia/{id}', 'CMS\CMSSliderController@DeleteMedia');


Route::post('request/DisplayMyClassesInfo', 'Classes\ClassesController@DisplayMyClassesInfo');


Route::post('dashboard/DisplayNotificationsWidget', 'Front\DashboardController@NotificationWidget');
Route::post('dashboard/AcceptNotificationRequest', 'Projects\ProjectNotificationsController@AcceptProjectRequest');
Route::post('dashboard/RejectNotificationRequest', 'Projects\ProjectNotificationsController@RejectProjectRequest');
Route::post('dashboard/PostPoneNotificationRequest', 'Projects\ProjectNotificationsController@PostPoneNotificationRequest');
//// Print privilages
Route::post('administrator/print/{data_type}', 'Utilities\GeneralController@PrintListData');
//// Surveys
Route::post('request/AjaxDisplaySurveys', 'Surveys\SurveysController@AjaxDisplaySurveys');
Route::post('request/AddObjectDropdown', 'Surveys\SurveysController@AddObjectDropdown');
Route::post('request/SaveSurvey', 'Surveys\SurveysController@SaveSurvey');
Route::post('request/DeleteSurvey', 'Surveys\SurveysController@DeleteSurvey');
Route::post('request/AjaxViewQuestions', 'Surveys\SurveyQuestionsController@AjaxViewQuestions');
Route::post('request/CheckQuestion' , 'Surveys\SurveyQuestionsController@CheckQuestion');
Route::post('request/SaveQuestion', 'Surveys\SurveyQuestionsController@SaveQuestion');
Route::post('request/DeleteQuestion', 'Surveys\SurveyQuestionsController@DeleteQuestion');
Route::post('request/AjaxViewAnswers', 'Surveys\SurveyQuestionsController@AjaxViewAnswers');
Route::post('request/SubmitSurvey','Classes\ClassesController@SubmitSurvey');
Route::post('request/ListSurveyAnswers','Surveys\SurveysController@ListSurveyAnswers');
Route::post('request/ViewQuestionsAnswers','Surveys\SurveysController@ViewQuestionsAnswers');
///Sessions ////
Route::post('request/DisplaySessions','Sessions\SessionsController@DisplaySessions');
Route::post('request/DisplayProjectGroups','Sessions\SessionsController@DisplayProjectGroups');
Route::post('request/SaveSession','Sessions\SessionsController@SaveSession');
Route::post('request/DeleteSession','Sessions\SessionsController@DeleteSession');


Route::post('request/DisplayInvoiceData','Financial\InvoicesController@GenerateInvoiceInformation');
Route::post('request/GenerateInvoice','Financial\InvoicesController@GenerateInvoice');
//////////////////////////////////////////
////////////////////////////////////////////

Route::post('request/DisplayListCustomers','Customers\CustomersController@DisplayListCustomers');
Route::post('request/CheckCustomerEmail','Customers\CustomersController@CheckCustomerEmail');
Route::post('request/SaveCustomerInfo','Customers\CustomersController@SaveCustomerInfo');
Route::post('request/DeleteCustomer','Customers\CustomersController@DeleteCustomer');


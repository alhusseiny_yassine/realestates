<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::get('administrator','Users\UsersController@AdminLogin');
Route::get('/','Users\UsersController@FrontLogin');
Route::get('/{message}','Users\UsersController@FrontLogin');


Route::get('users/activate/{activation_key}','Users\UsersController@ActivateAccount');
Route::get('ActivationError',function(){
    return view('errors.activationerror');
});

Route::group(['middleware' => ['auth']], function() {
    Route::get('omsar/dashboard','Front\IndexController@TraineesDashboard');
    Route::get('omsar/logout','Auth\LoginController@TraineeLogout');
    Route::get('omsar/courses','Courses\CourseController@ListCourseManagement');
    Route::get('requests/SendRequestTraining/{course_ids}','Requests\RequestsController@SendRequestTraining');
    Route::get('omsar/requests','Requests\RequestsController@ListRequestsManagement');
    Route::get('omsar/SendRequestsRejection/{request_ids}','Requests\RequestsController@SendRequestRejections');
    Route::get('omsar/ChangeRequestStatus/{request_ids}','Requests\RequestsController@ChangeRequestStatus');
    Route::get('omsar/projects','Projects\ProjectsController@ProjectsManagement');
    Route::get('omsar/projects/CreateNewProject','Projects\ProjectsController@CreateNewProject');
    Route::get('omsar/projects/ProjectConfiguration/{project_id}','Projects\ProjectsController@ProjectConfigurationManagement');
    Route::get('request/ChangeProjectStatus/{project_ids}','Projects\ProjectsController@OpenChangeProjectStatusPopUp');
    Route::get('omsar/mycalendar','Sessions\SessionsController@DisplayMyCalendar');
    Route::get('omsar/mysessions','Sessions\SessionsController@DisplayMySessions');
    Route::get('omsar/TakeAttendance/{sc_id}','Sessions\SessionsController@TakeAttendance');
    Route::get('users/notifications','Utilities\NotificationsController@ListNotifications');
    Route::get('ViewSessionInfo/{session_id}','Sessions\SessionsController@ViewSessionInfo');
    
    ///groups/////
    Route::get('omsar/groups','Projects\GroupsController@GroupsManagement');
    Route::get('groups/AddNewGroup/{project_id}','Classes\ClassesController@CreateNewProjectGroup');
    Route::get('groups/EditGroup/{pg_id}','Classes\ClassesController@EditProjectGroupPopUp');
    /////
    ////Sessions ////
    Route::get('omsar/sessions','Sessions\SessionsController@SessionsManagement');
    Route::get('session/DisplayAddSession','Sessions\SessionsController@DisplayAddSession');
    Route::get('session/DisplayEditSession/{session_id}','Sessions\SessionsController@EditSessionInformation');
    
    Route::get('account/myclasses','Classes\ClassesController@MyClassManagement');

    Route::get('GenerateClassPdf/{pg_id}', 'Classes\ClassesController@GenerateClassPdf');


    Route::get('trainer/TraineesAttendances','Users\TrainersController@TraineesAttendances');
    Route::get('trainer/TraineesTestResults','Users\TrainersController@TraineesTestResults');
    Route::get('Provider/AddProviderLocations/{user_id}','System\ProviderLocationController@AddAProviderLocationForm');
    Route::get('Provider/EditProviderLocations/{pl_id}','System\ProviderLocationController@EditAProviderLocationForm');


    Route::get('project/RunningCourse','Projects\ProjectCoursesController@RunningCoursesManagement');



    Route::get('omsar/myprofile','Front\ProfileController@MyProfileManagement');

    Route::get('administrator/dashboard','Admin\DashboardController@AdminDashboardPanel');
    Route::get('administrator/Logout','Auth\LoginController@logout');
    Route::get('administrator/UsersManagement','Users\UsersController@UsersManagement');
    Route::get('administrator/adduser','Users\UsersController@AddUser');
    Route::get('administrator/editUser/{user_id}','Users\UsersController@EditUserForm');
    Route::get('administrator/AddMultipleUsers','Users\UsersController@MultipleUsersDataGrid');
    Route::get('administrator/myprofile','Users\UsersController@DisplayUserProfile');
    Route::get('administrator/PermissionDenied', 'Utilities\MessageController@PermissionDenied');
    Route::get('User/AddEducationInfo/{user_id}', 'Users\UsersController@AddEducationInfo');
    Route::get('User/EditEducationInfo/{ue_id}', 'Users\UsersController@EditEducationInfo');
    Route::get('administrator/mycalendar','Users\UsersController@DisplayMycalendar');

    Route::get('administrator/AddEducationInfo/{user_id}', 'Users\UsersController@AddAEducationInfo');
    Route::get('administrator/EditEducationInfo/{ue_id}', 'Users\UsersController@EditAEducationInfo');


    Route::get('administrator/RolesManagement','Roles\RolesController@DisplayRolesManagement');
    Route::get('administrator/addRole','Roles\RolesController@AddRoleForm');
    Route::get('administrator/editRole/{role_id}','Roles\RolesController@EditRoleForm');


    Route::get('administrator/Beneficiaries','Admin\BeneficiariesController@BeneficiariesManagement');
    Route::get('administrator/AddNewBeneficiary','Admin\BeneficiariesController@AddBeneficiaryForm');
    Route::get('administrator/editBeneficiaryForm/{benf_id}','Admin\BeneficiariesController@EditBeneficaryForm');



    Route::get('administrator/jobtitles','Admin\JobTitlesController@JobTitlesManagement');
    Route::get('administrator/AddNewJobTitle','Admin\JobTitlesController@AddJobTitlesForm');
    Route::get('administrator/editJobTitleForm/{jt_id}','Admin\JobTitlesController@EditJobTitleForm');


    Route::get('administrator/JobRoles','Admin\JobRolesController@JobRolesManagement');
    Route::get('administrator/AddNewJobRole','Admin\JobRolesController@AddJobRoleForm');
    Route::get('administrator/EditJobRole/{jr_id}','Admin\JobRolesController@EditJobRoleForm');


    Route::get('administrator/taskslist','Admin\TasksListController@TasksListManagement');
    Route::get('administrator/AddNewTaskList','Admin\TasksListController@AddTasksListForm');
    Route::get('administrator/EditTaskList/{tl_id}','Admin\TasksListController@EditTaskListForm');


    Route::get('administrator/EducationDegrees','System\EducationDegreeController@EducationDegreeManagement');
    Route::get('administrator/AddNewEducationDegree','System\EducationDegreeController@AddEducationDegreeForm');
    Route::get('administrator/EditEducationDegree/{ed_id}','System\EducationDegreeController@EditEducationDegreeForm');


    Route::get('administrator/BeneficiaryTypes','Admin\BeneficiaryTypesController@BeneficiaryTypesManagement');
    Route::get('administrator/AddNewBeneficiaryType','Admin\BeneficiaryTypesController@AddBeneficiaryTypeForm');
    Route::get('administrator/editBeneficiaryTypeForm/{bt_id}','Admin\BeneficiaryTypesController@EditBeneficiaryTypeForm');


    Route::get('administrator/BeneficiaryDirectorates','Admin\BeneficiaryDirectorateController@BeneficiaryDirectorateManagement');
    Route::get('administrator/AddNewBeneficiaryDirectorate','Admin\BeneficiaryDirectorateController@AddBeneficiaryDirectorateForm');
    Route::get('administrator/editBeneficiaryDirectorateForm/{bd_id}','Admin\BeneficiaryDirectorateController@EditBeneficiaryDirForm');


    Route::get('administrator/CourseTypes','Courses\CourseTypesController@CourseTypesManagement');
    Route::get('administrator/AddNewCourseType','Courses\CourseTypesController@AddCourseTypeForm');
    Route::get('administrator/editCourseTypeForm/{ct_id}','Courses\CourseTypesController@EditCourseTypeForm');

    Route::get('administrator/CourseCategories','Courses\CourseCategoriesController@CourseCategoriesManagement');
    Route::get('administrator/AddNewCourseCategory','Courses\CourseCategoriesController@AddCourseCategoryForm');
    Route::get('administrator/editCourseCategoryForm/{cc_id}','Courses\CourseCategoriesController@EditCourseCategoryForm');



    Route::get('administrator/countries','Configuration\CountryController@index');
    Route::get('administrator/AddNewCountry','Configuration\CountryController@AddNewCountryForm');
    Route::get('administrator/editCountryForm/{country_id}','Configuration\CountryController@EditCountryForm');


    Route::get('administrator/Courses','Courses\CourseController@CourseManagement');
    Route::get('administrator/AddNewCourse','Courses\CourseController@AddCourseForm');
    Route::get('administrator/editCourseForm/{c_id}','Courses\CourseController@EditCourseForm');


    Route::get('administrator/projecttypes','Projects\ProjectTypesController@ProjectTypesManagement');
    Route::get('administrator/AddNewProjectType','Projects\ProjectTypesController@AddProjectTypeForm');
    Route::get('administrator/editProjectTypeForm/{pt_id}','Projects\ProjectTypesController@EditProjectTypeForm');


    Route::get('administrator/projectstatuses','Projects\ProjectStatusesController@ProjectStatusesManagement');
    Route::get('administrator/AddNewProjectStatus','Projects\ProjectStatusesController@AddProjectStatusForm');
    Route::get('administrator/editProjectStatusForm/{ps_id}','Projects\ProjectStatusesController@EditProjectStatusForm');


    Route::get('administrator/provinces','Admin\ProvincesController@ProvincesManagement');
    Route::get('administrator/AddNewProvince','Admin\ProvincesController@AddProvinceForm');
    Route::get('administrator/editProvinceForm/{sp_id}','Admin\ProvincesController@EditProvinceForm');

    Route::get('administrator/districts','Admin\DistrictsController@DistrictsManagement');
    Route::get('administrator/AddNewDistrict','Admin\DistrictsController@AddDistrictForm');
    Route::get('administrator/editDistrictForm/{sd_id}','Admin\DistrictsController@EditDistrictForm');


    Route::get('administrator/sessionstatuses','Sessions\SessionStatusesController@SessionStatusesManagement');
    Route::get('administrator/AddNewSessionStatus','Sessions\SessionStatusesController@AddSessionStatusForm');
    Route::get('administrator/editSessionStatusForm/{ss_id}','Sessions\SessionStatusesController@EditSessionStatusForm');

    Route::get('administrator/requests','Requests\RequestsController@AdminRequestsManager');
    Route::get('administrator/AddNewRequestForm','Requests\RequestsController@AddNewRequestForm');
    Route::get('administrator/EditRequestInfo/{r_id}','Requests\RequestsController@EditRequestForm');
    Route::get('administrator/DeleteRequest','Requests\RequestsController@EditRequestForm');


    Route::get('administrator/requeststatuses','Requests\RequestStatusesController@RequestStatusesManagement');
    Route::get('administrator/AddNewRequestStatus','Requests\RequestStatusesController@AddRequestStatusForm');
    Route::get('administrator/editRequestStatusForm/{rs_id}','Requests\RequestStatusesController@EditRequestStatusForm');


    Route::get('administrator/donors','Admin\DonorsController@DonorsManagement');
    Route::get('administrator/AddNewDonor','Admin\DonorsController@AddDonorForm');
    Route::get('administrator/editDonorForm/{d_id}','Admin\DonorsController@EditDonorForm');


    Route::get('administrator/projects','Projects\ProjectsController@AdminProjectManagement');
    Route::get('administrator/projects/CreateNewProject','Projects\ProjectsController@AdminCreationProject');
    Route::get('administrator/projects/ProjectConfiguration/{project_id}','Projects\ProjectsController@AdminProjectConfiguration');
    Route::get('administrator/projects/EditProject/{project_id}','Projects\ProjectsController@AdmiEditProjectPage');
    Route::get('administrator/projects/ViewProject/{project_id}','Projects\ProjectsController@ViewProjectPage');



    Route::get('administrator/invoicemanagement','Financial\InvoicesController@InvoiceManagement');
    Route::get('administrator/GenerateInvoicePdf/{invoice_id}','Financial\InvoicesController@GenerateInvoicePdf');
    Route::get('administrator/projects/CreateNewProject','Projects\ProjectsController@AdminCreationProject');
    Route::get('administrator/projects/ProjectConfiguration/{project_id}','Projects\ProjectsController@AdminProjectConfiguration');
    Route::get('administrator/projects/EditProject/{project_id}','Projects\ProjectsController@AdmiEditProjectPage');
    Route::get('administrator/projects/ViewProject/{project_id}','Projects\ProjectsController@ViewProjectPage');


    Route::get('administrator/ClassManagement','Classes\ClassesController@ClassManagement');
    Route::get('administrator/CreateNewProjectClass/{project_id}','Classes\ClassesController@CreateNewProjectGroup');
    Route::get('administrator/EditProjectGroup/{pg_id}','Classes\ClassesController@EditProjectGroupPopUp');



    Route::get('administrator/TraineesCourseStatus','Projects\TraineesCourseStatusController@TraineesCourseStatus');
    Route::get('administrator/AddNewTraineesCourseStatusForm','Projects\TraineesCourseStatusController@AddNewTraineesCourseStatus');
    Route::get('administrator/EditTraineesCourseStatusInfo/{ucs_id}','Projects\TraineesCourseStatusController@EditTraineesCourseStatusForm');


    Route::get('administrator/slidersManagement','CMS\CMSSliderController@SliderManagement');
    Route::get('administrator/addSliderInfo','CMS\CMSSliderController@AddSlideForm');
    Route::get('administrator/editSliderInfo/{sId}','CMS\CMSSliderController@EditSliderForm');
    Route::get('administrator/page/slideimages/{sId}','CMS\CMSSliderController@displayListImages');
    Route::get('AddImagesSlider/{sId}', 'CMS\CMSSliderController@UploadImagesSlider');
    Route::get('administrator/EditImageSlider/{cm_id}', 'CMS\CMSSliderController@EditImageSLiderInformation');

    Route::get('project/EditTraineesGroup/{pg_id}', 'Projects\GroupsController@EditTraineesGroup');


    Route::get('projects/TraineesCalendar/{project_id}/{group_id}/{trainee_id}', 'Sessions\SessionsController@DisplayTraineeCalendar');




    Route::get('administrator/ConfigurationManagement', 'Utilities\ConfigurationController@index');
    Route::get('administrator/print/{data_type}', 'Utilities\GeneralController@PrintListData');
    Route::get('administrator/exportexcel/{data_type}', 'Utilities\GeneralController@ExportToExcel');
    Route::get('administrator/exportprivilegeexcel/{role_id}', 'Utilities\GeneralController@ExportPrivilegesToExcel');
    Route::get('omsar/projects/EditProject/{project_id}', 'Projects\ProjectsController@EditProjectPage');
    Route::get('session/EditSessionInformation/{session_id}','Sessions\SessionsController@EditSessionInformation');

    Route::get('administrator/traineesHistory/{user_id}','Users\UsersController@DisplayTraineeHistory');


    Route::get('omsar/classes','Classes\ClassesController@FrontClassManagement');
    Route::get('administrator/TraineesAttendances','Classes\ClassesController@TraineesAttendances');
    Route::get('omsar/TraineesAttendances','Classes\ClassesController@FrontTraineesAttendances');
    
    
    Route::get('coordinator/ViewTraineesAttendances','Classes\ClassesController@ViewTraineesAttendances');
 



    Route::get('administrator/TraineesTestResults','Classes\ClassesController@TraineesTestResults');
    Route::get('omsar/TraineesTestResults','Classes\ClassesController@FrontTraineesTestResults');


    Route::get('reports/ListCourses','Courses\CourseController@ReportListCourses');
    Route::get('reports/ListProviders','Users\UsersController@ReportListProviders');
    Route::get('reports/ListCoordinators','Users\UsersController@ReportListCoordinators');
    Route::get('reports/ListTrainees','Users\UsersController@ReportListTrainees');
    Route::get('reports/ListProjects','Projects\ProjectsController@ReportListProjects');

    Route::get('User/AddProviderLocations/{user_id}','System\ProviderLocationController@AddProviderLocationForm');
    Route::get('User/EditProviderLocations/{pl_id}','System\ProviderLocationController@EditProviderLocationForm');
    
    
    Route::get('trainers/trainersManagement','Users\TrainersController@TrainersManagement');
    Route::get('trainers/addNewTrainer','Users\TrainersController@AddNewTrainer');
    Route::get('trainers/EditTrainer/{user_id}','Users\TrainersController@EditTrainer');
    ///// Surveys /////
    Route::get('surveys/ListSurveys','Surveys\SurveysController@DisplaySurveys');
    Route::get('surveys/AddSurvey','Surveys\SurveysController@AddSurvey');
    Route::get('surveys/EditSurvey/{survey_id}','Surveys\SurveysController@EditSurvey');
    Route::get('surveys/ViewQuestions/{survey_id}','Surveys\SurveyQuestionsController@ViewQuestions');
    Route::get('surveys/ViewListSurveyAnswers/{survey_id}','Surveys\SurveysController@ViewListSurveyAnswers');
    Route::get('surveys/AddQuestion/{survey_id}','Surveys\SurveyQuestionsController@AddQuestion');
    Route::get('surveys/EditQuestion/{sq_id}','Surveys\SurveyQuestionsController@EditQuestion');
    Route::get('surveys/ViewAnswers/{sq_id}','Surveys\SurveyQuestionsController@ViewAnswers');
    Route::get('surveys/ViewSurvey/{pg_id}','Classes\ClassesController@ViewSurvey');
//////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
    
    Route::get('administrator/GeneratePDF/{object_type}','General\GeneralController@GeneratePDF');
    Route::get('administrator/GenerateExcelSheet/{object_type}', 'General\GeneralController@GenerateExcelSheet');
    
    Route::get('administrator/CutomersManagement','Customers\CustomersController@CutomersManagement');
    Route::get('customers/AddCustomer','Customers\CustomersController@AddCustomer');
    Route::get('customers/EditCustomer/{c_id}','Customers\CustomersController@EditCustomer');
});


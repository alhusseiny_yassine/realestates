<?php
/***********************************************************
InvoiceDetails.php
Product :
Version : 1.0
Release : 2
Date Created : Aug 21, 2017
Developed By  : alhusseiny   PHP Department

Page Description :
{Enter page description Here}
***********************************************************/

namespace App\model\Financials;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetails extends Model
{
    protected   $table          = 'inv_invoice_details';
    public      $timestamps     = false;
    protected   $primaryKey     = "id_id";
}
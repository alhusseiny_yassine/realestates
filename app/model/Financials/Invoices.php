<?php
/***********************************************************
Invoices.php
Product :
Version : 1.0
Release : 2
Date Created : Aug 21, 2017
Developed By  : alhusseiny   PHP Department

Page Description :
{Enter page description Here}
***********************************************************/

namespace App\model\Financials;

use Illuminate\Database\Eloquent\Model;

class Invoices extends Model
{
    protected   $table          = 'inv_invoices';
    public      $timestamps     = false;
    protected   $primaryKey     = "ii_id";
}
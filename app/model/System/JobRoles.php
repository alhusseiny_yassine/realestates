<?php


namespace App\model\System;

use Illuminate\Database\Eloquent\Model;

class JobRoles extends Model
{
    protected $table        = 'sys_job_roles';
    public $timestamps      = false;
    protected $dateFormat   = 'Y-m-d H:i:s';
    protected $primaryKey   = 'jr_id';
}

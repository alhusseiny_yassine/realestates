<?php

namespace App\model\System;

use Illuminate\Database\Eloquent\Model;

class Days extends Model
{
       protected   $table        = 'sys_days';
    public      $timestamps     = false;
    protected   $primaryKey     = "ID";
}

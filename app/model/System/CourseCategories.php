<?php

namespace App\model\System;

use Illuminate\Database\Eloquent\Model;

class CourseCategories extends Model
{
        protected   $table          = 'sys_course_categories';
    public      $timestamps     = false;
    protected   $primaryKey     = "cc_id";
}

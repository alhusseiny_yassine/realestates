<?php

namespace App\model\System;

use Illuminate\Database\Eloquent\Model;

class JobTitles extends Model
{
    protected $table        = 'sys_job_titles';
    public $timestamps      = false;
    protected $dateFormat   = 'Y-m-d H:i:s';
    protected $primaryKey   = 'jt_id';
}

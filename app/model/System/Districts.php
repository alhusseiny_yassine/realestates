<?php

namespace App\model\System;

use Illuminate\Database\Eloquent\Model;

class Districts extends Model
{
   protected   $table          = 'sys_districts';
    public      $timestamps     = false;
    protected   $primaryKey     = "sd_id";
}

<?php

namespace App\model\System;

use Illuminate\Database\Eloquent\Model;

class TasksList extends Model
{
    protected $table        = 'sys_tasks_list';
    public $timestamps      = false;
    protected $dateFormat   = 'Y-m-d H:i:s';
    protected $primaryKey   = 'tl_id';
}

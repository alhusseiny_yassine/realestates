<?php

namespace App\model\System;

use Illuminate\Database\Eloquent\Model;

class CourseType extends Model
{
        protected   $table          = 'sys_course_type';
    public      $timestamps     = false;
    protected   $primaryKey     = "ct_id";
}

<?php
/***********************************************************
EducationDegree.php
Product :
Version : 1.0
Release : 2
Date Created : Mar 27, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

namespace App\model\System;

use Illuminate\Database\Eloquent\Model;

class EducationDegree extends Model
{
    protected   $table          = 'sys_education_degree';
    public      $timestamps     = false;
    protected   $primaryKey     = "ed_id";
}

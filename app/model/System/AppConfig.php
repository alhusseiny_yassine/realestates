<?php

namespace App\model\System;

use Illuminate\Database\Eloquent\Model;

class AppConfig extends Model
{
     protected   $table          = 'sys_appconfig';
    public      $timestamps     = false;
    protected   $primaryKey     = "sa_id";
}

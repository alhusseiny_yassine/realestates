<?php

namespace App\model\System;

use Illuminate\Database\Eloquent\Model;

class Donor extends Model
{
    protected   $table          = 'sys_donor';
    public      $timestamps     = false;
    protected   $primaryKey     = "d_id";
}

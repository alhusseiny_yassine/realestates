<?php

namespace App\model\System;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
     protected   $table          = 'sys_country';
    public      $timestamps     = false;
    protected   $primaryKey     = "id";
}

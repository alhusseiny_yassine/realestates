<?php

namespace App\model\System;

use Illuminate\Database\Eloquent\Model;

class Beneficiaries extends Model
{
   protected   $table          = 'sys_beneficiaries';
    public      $timestamps     = false;
    protected   $primaryKey     = "sb_id";
}

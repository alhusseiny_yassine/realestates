<?php

namespace App\model\System;

use Illuminate\Database\Eloquent\Model;

class Provinces extends Model
{
    protected   $table          = 'sys_provinces';
    public      $timestamps     = false;
    protected   $primaryKey     = "sp_id";
}

<?php

namespace App\model\System;

use Illuminate\Database\Eloquent\Model;

class BeneficiaryType extends Model
{
    protected   $table          = 'sys_beneficiary_type';
    public      $timestamps     = false;
    protected   $primaryKey     = "bt_id";
}

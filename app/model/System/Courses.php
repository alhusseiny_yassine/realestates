<?php

namespace App\model\System;

use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
           protected   $table          = 'sys_courses';
    public      $timestamps     = false;
    protected   $primaryKey     = "c_id";
}

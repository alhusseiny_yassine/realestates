<?php
/***********************************************************
ProviderLocation.php
Product :
Version : 1.0
Release : 2
Date Created : Mar 29, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

namespace App\model\System;

use Illuminate\Database\Eloquent\Model;

class ProviderLocation extends Model
{
    protected   $table          = 'sys_provider_locations';
    public      $timestamps     = false;
    protected   $primaryKey     = "pl_id";
}

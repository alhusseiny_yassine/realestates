<?php
/***********************************************************
SystemConfig.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 2, 2016
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :

***********************************************************/


namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;
use Middleware;
use DB;
use Auth;


class SystemConfig extends Model
{
    protected $table        = 'sys_config';
    public $timestamps      = false;
    protected $dateFormat   = 'Y-m-d H:i:s';
    protected $primaryKey   = 'sc_id';

}
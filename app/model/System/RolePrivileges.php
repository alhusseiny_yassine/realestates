<?php
/***********************************************************
RolePrivileges.php
Product :
Version : 1.0
Release : 2
Date Created : Jan 3, 2016
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/




namespace App\model\System;

use Illuminate\Database\Eloquent\Model;
use Middleware;
use DB;
use Session;

class RolePrivileges extends Model
{
    protected $table        = 'role_privileges';
    public $timestamps      = false;
    protected $dateFormat   = 'Y-m-d H:i:s';
    protected $primaryKey   = 'rp_id';

    public static function saveInfo( $input )
    {
        if( isset($input['id']) )
        {
            $model = self::find($input['id']);
        }
        else
        {
            $model = new RolePrivileges();
            $model->fk_role_r_id = $input['fk_role_r_id'];

        }
        $model->rp_action_code           = $input['rp_action_code'];
        $model->rp_privilege             = $input['rp_privilege'];
        $model->rp_random                = $input['rp_random'];


        $model->save();
        return $model['attributes']['c_id'];
    }


    public static function getPrivileges( $role_id )
    {
        $role_privileges =  DB::table('role_privileges')->where('fk_role_r_id',$role_id)->get();

        $privileges = array();

        for ($i = 0; $i < count($role_privileges); $i++)
        {
            $privileges[ $role_privileges[$i]->rp_action_code ] = $role_privileges[$i]->rp_privilege;
        }

        return $privileges;
    }

    public static function getAll()
    {
        $rp_array = self::all();

        $db_result_array = array();

        foreach ( $rp_array as $role_privileges )
        {
            $db_result_array[] = $role_privileges['attributes'];
        }

        $result_array = array();
        for ($i = 0; $i < count($db_result_array); $i++) {
            $result_array[ $db_result_array[$i]['rp_action_code'] ] = $db_result_array[$i]['rp_privilege'];
        }


        return $result_array;
    }



    public static function DeleteRolePrivileges( $role_id )
    {
        DB::table('users')->where('role_privileges', '=', $role_id)->delete();
    }
}
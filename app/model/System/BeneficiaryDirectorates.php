<?php
/***********************************************************
BeneficiaryDirectorates.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 26, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/



namespace App\model\System;

use Illuminate\Database\Eloquent\Model;

class BeneficiaryDirectorates extends Model
{
    protected   $table          = 'sys_benf_directorate';
    public      $timestamps     = false;
    protected   $primaryKey     = "bd_id";
}

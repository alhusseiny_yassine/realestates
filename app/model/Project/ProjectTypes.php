<?php

namespace App\model\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectTypes extends Model
{
        protected   $table          = 'proj_project_types';
    public      $timestamps     = false;
    protected   $primaryKey     = "pt_id";
}

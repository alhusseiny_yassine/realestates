<?php

namespace App\model\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectSchedule extends Model
{
    protected   $table          = 'proj_project_day_schedule';
    public      $timestamps     = false;
    protected   $primaryKey     = "ps_id";
  
}

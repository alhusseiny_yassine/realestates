<?php


namespace App\model\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectNotifications extends Model
{
    protected   $table          = 'proj_project_notification_requests';
    public      $timestamps     = false;
    protected   $primaryKey     = "nr_id";
}

<?php

namespace App\model\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectClasses extends Model
{
    protected   $table          = 'proj_classes';
    public      $timestamps     = false;
    protected   $primaryKey     = "cls_id";
  
    public function courses()
    {
        return $this->hasOne('App\model\Project\ProjectCourses','fk_pcourse_id');
    }
}

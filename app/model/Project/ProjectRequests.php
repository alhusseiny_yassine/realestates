<?php

namespace App\model\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectRequests extends Model
{
    protected   $table          = 'proj_project_requests';
    public      $timestamps     = false;
}

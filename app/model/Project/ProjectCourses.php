<?php

namespace App\model\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectCourses extends Model
{
    protected   $table          = 'proj_project_courses';
    public      $timestamps     = false;
    protected   $primaryKey     = "pc_id";
    
    public function courses()
    {
        return $this->belongsToMany('App\model\System\Courses');
    }
    
}

<?php

namespace App\model\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectGroups extends Model
{
    protected   $table          = 'proj_projects_groups';
    public      $timestamps     = false;
    protected   $primaryKey     = "pg_id";
  
}

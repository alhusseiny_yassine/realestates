<?php

namespace App\model\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectBeneficiaries extends Model
{
    protected   $table          = 'proj_project_beneficiaries';
    public      $timestamps     = false;
}

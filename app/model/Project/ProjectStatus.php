<?php

namespace App\model\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectStatus extends Model
{

    const PROJECT_STATUS_PENDING                            = 1;
    const PROJECT_STATUS_UNDER_REVIEW                       = 2;
    const PROJECT_STATUS_APPROVED_BY_PROVIDER               = 3;
    const PROJECT_STATUS_APPROVED_BY_COORDINATOR            = 3;
    const PROJECT_STATUS_APPROVED_BY_OMSAR                  = 4;
    const PROJECT_STATUS_DENY                               = 5;
    const PROJECT_STATUS_INPROGRESS                         = 6;
    const PROJECT_STATUS_PAYMENT_PENDING                    = 7;
    const PROJECT_STATUS_CLOSED                             = 8;



    protected   $table          = 'proj_project_status';
    public      $timestamps     = false;
    protected   $primaryKey     = "ps_id";
}

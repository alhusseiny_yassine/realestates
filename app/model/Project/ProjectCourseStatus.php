<?php
/***********************************************************
ProjectCourseStatus.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 1, 2017
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

namespace App\model\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectCourseStatus extends Model
{
    protected   $table          = 'proj_project_user_course_status';
    public      $timestamps     = false;
    protected   $primaryKey     = "ucs_id";

    public function courses()
    {
        return $this->belongsToMany('App\model\System\Courses');
    }

}

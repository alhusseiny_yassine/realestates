<?php

namespace App\model\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectTrainers extends Model
{
       protected   $table          = 'proj_project_trainers';
    public      $timestamps     = false;
    protected   $primaryKey     = "pt_id";
}

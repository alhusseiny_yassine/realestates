<?php
/***********************************************************
ProjectCoursesGroups.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 13, 2017
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2017

Page Description :
Save Groups for every course in selected Project
***********************************************************/


namespace App\model\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectCoursesGroups extends Model
{
    protected   $table          = 'proj_project_course_groups';
    public      $timestamps     = false;

    public function courses()
    {
        return $this->belongsToMany('App\model\System\Courses');
    }

}

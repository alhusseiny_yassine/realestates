<?php

namespace App\model\Project;

use Illuminate\Database\Eloquent\Model;
use App\model\Project\ProjectStatus;
use App\model\Project\Projects;
use App\model\Project\ProjectCourses;


class Projects extends Model
{
    protected   $table          = 'proj_projects';
    public      $timestamps     = false;
    protected   $primaryKey     = "pp_id";
    
    const PROJECT_BILLING_LUMP_SUM      = 1;
    const PROJECT_BILLING_PER_TRAINEE   = 2;


    public function courses()
    {
        return $this->belongsTo('App\model\Projects\ProjectCourses', 'fk_project_id');
    }



    public function classes()
    {
        return $this->belongsTo('App\model\ProjectClasses', 'fk_project_id');
    }

}

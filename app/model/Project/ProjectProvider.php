<?php

namespace App\model\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectProvider extends Model
{
    protected   $table          = 'proj_project_provider';
    public      $timestamps     = false;
    protected   $primaryKey     = "pp_id";
}

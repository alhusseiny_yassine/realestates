<?php

namespace App\model\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectTrainees extends Model
{
    protected   $table          = 'proj_project_trainees';
    public      $timestamps     = false;
    protected   $primaryKey     = "pt_id";
}

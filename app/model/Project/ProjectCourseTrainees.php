<?php
/***********************************************************
ProjectCourseTrainees.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 28, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/




namespace App\model\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectCourseTrainees extends Model
{
    protected   $table          = 'proj_project_course_trainees';
    public      $timestamps     = false;

    public function courses()
    {
        return $this->belongsToMany('App\model\System\Courses');
    }

}

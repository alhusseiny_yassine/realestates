<?php

namespace App\model\Sessions;

use Illuminate\Database\Eloquent\Model;

class SessionClass extends Model
{
     protected   $table          = 'ses_session_class';
    public      $timestamps     = false;
    protected   $primaryKey     = "sc_id";
}

<?php

namespace App\model\Sessions;

use Illuminate\Database\Eloquent\Model;

class SessionTestResult extends Model
{
      protected   $table          = 'ses_session_test_result';
    public      $timestamps     = false;
    protected   $primaryKey     = "sr_id";
}

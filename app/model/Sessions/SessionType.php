<?php
/***********************************************************
SessionType.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 29, 2017
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\model\Sessions;

use Illuminate\Database\Eloquent\Model;

class SessionType extends Model
{
    protected   $table          = 'ses_session_types';
    public      $timestamps     = false;
    protected   $primaryKey     = "ss_id";
    
    
    const SESSION_TYPE_COURSE   = 1;
    const SESSION_TYPE_TEST     = 2;
}

<?php

namespace App\model\Sessions;

use Illuminate\Database\Eloquent\Model;

class SessionAttendance extends Model
{
    protected   $table          = 'ss_session_attendance';
    public      $timestamps     = false;
    protected   $primaryKey     = "sa_id";
}

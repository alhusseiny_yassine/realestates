<?php

namespace App\model\Sessions;

use Illuminate\Database\Eloquent\Model;

class SessionStatus extends Model
{
    protected   $table          = 'ses_session_status';
    public      $timestamps     = false;
    protected   $primaryKey     = "ss_id";
    
    
    const SESSION_STATUS_CREATED  = 1;
}

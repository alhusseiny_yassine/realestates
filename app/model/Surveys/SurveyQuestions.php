<?php
/***********************************************************
Surveys.php
Product :
Version : 1.0
Release : 2
Developed By  : Alhusseiny PHP Department

Page Description :
{Enter page description Here}
***********************************************************/

namespace App\model\Surveys;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestions extends Model
{
    protected   $table          = 'sys_survey_questions';
    public      $timestamps     = false;
    protected   $primaryKey     = "sq_id";
}
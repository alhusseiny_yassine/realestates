<?php
/***********************************************************
SurveyAnswers.php
Product :
Version : 1.0
Release : 2
Developed By  : Alhusseiny PHP Department

Page Description :
{Enter page description Here}
***********************************************************/

namespace App\model\Surveys;

use Illuminate\Database\Eloquent\Model;

class SurveyAnswers extends Model
{
    protected   $table          = 'sys_survey_answers';
    public      $timestamps     = false;
    protected   $primaryKey     = "sa_id";
}
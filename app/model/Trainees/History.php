<?php

namespace App\model\Trainees;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected   $table          = 'trainees_history';
    public      $timestamps     = false;
    protected   $primaryKey     = "th_id";
}

<?php
/***********************************************************
Customers.php
Product :
Version : 1.0
Release : 2
Date Created : Aug 24, 2017
Developed By  : alhusseiny   PHP Department

Page Description :
{Enter page description Here}
***********************************************************/

namespace App\model\Customers;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    protected   $table          = 'customers';
    public      $timestamps     = false;
    protected   $primaryKey     = "c_id";
}
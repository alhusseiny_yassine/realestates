<?php
/***********************************************************
Notifications.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 8, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\model\Notifications;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    protected   $table          = 'not_notifications';
    public      $timestamps     = false;
    protected   $primaryKey     = "nt_id";
}
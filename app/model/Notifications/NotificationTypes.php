<?php
/***********************************************************
NotificationTypes.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 15, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/



namespace App\model\Notifications;

use Illuminate\Database\Eloquent\Model;

class NotificationTypes extends Model
{
    protected   $table          = 'not_notifications_type';
    public      $timestamps     = false;
    protected   $primaryKey     = "nt_id";
}
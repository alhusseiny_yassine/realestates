<?php

namespace App\model\Requests;

use Illuminate\Database\Eloquent\Model;

class RequestCoursesStatus extends Model
{
    protected   $table          = 'req_request_courses_status';
    public      $timestamps     = false;
    protected   $primaryKey     = "rs_id";

}

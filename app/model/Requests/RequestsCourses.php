<?php

namespace App\model\Requests;

use Illuminate\Database\Eloquent\Model;

class RequestsCourses extends Model
{
    protected   $table                 = 'req_requests_courses';
    public      $timestamps             = false;

    public function courses()
    {
        return $this->hasMany('App\model\System\Courses','fk_course_id');
    }
}

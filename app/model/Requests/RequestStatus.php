<?php

namespace App\model\Requests;

use Illuminate\Database\Eloquent\Model;

class RequestStatus extends Model
{
     protected   $table                 = 'req_request_status';
    public      $timestamps             = false;
    protected   $primaryKey             = "rs_id";


    const REQUEST_STATUS_UNDER_REVIEW               = 1;
    const REQUEST_STATUS_PENDING                    = 2;
    const REQUEST_STATUS_APPROVED_BY_OMSAR          = 3;
    const REQUEST_STATUS_APPROVED_BY_COORDINATOR    = 19;
    const REQUEST_STATUS_REJECTED                   = 4;
    const REQUEST_STATUS_REGISTERED                 = 5;
    const REQUEST_STATUS_SCHEDULED                  = 6;
    const REQUEST_STATUS_CONFIRMED                  = 7;
    const REQUEST_STATUS_POSTPONED                  = 8;
    const REQUEST_STATUS_CANCEL                     = 9;
    const REQUEST_STATUS_INCLASS                    = 10;
    const REQUEST_STATUS_ABSENT                     = 11;
    const REQUEST_STATUS_DONE_PASSED                = 12;
    const REQUEST_STATUS_DONE_FAILED                = 13;
    const REQUEST_STATUS_DONE_UNTESTED              = 14;
    const REQUEST_STATUS_DONE_RESCHEDULED           = 15;
    const REQUEST_STATUS_DONE_RETESTED              = 16;
}

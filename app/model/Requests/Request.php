<?php

namespace App\model\Requests;

use Illuminate\Database\Eloquent\Model;
use App\Requests\RequestCoursesStatus;

class Request extends Model
{
    protected   $table          = 'req_request';
    public      $timestamps     = false;
    protected   $primaryKey     = "r_id";


    public function RequestCourses()
    {
        return $this->hasMany('App\model\Requests\RequestsCourses','fk_request_id');
    }


    public function CoursesStatus()
    {
        return $this->hasMany('App\model\Requests\RequestCoursesStatus', 'fk_request_id');
    }
}

<?php

namespace App\model\CMS;

use DB;
use Illuminate\Database\Eloquent\Model;

class CMSMedia extends Model
{
    protected   $table          = 'cms_media';
    public      $timestamps     = false;
    protected   $primaryKey     = "cm_id";


    /**
     * get list of images related to slider
     *
     * @author Moe Mantach
     * @param Integer $sId
     */
    public static function getImagesSlider( $sId )
    {
        $listImages      = DB::table('cms_media')->where('fk_slider_id' , $sId)->where('cm_is_deleted',0)->get();
        return $listImages;
    }

    public static function getImages()
    {
        $listImages      = DB::table('cms_media')->where('cm_is_deleted',0)->get();
        return $listImages;
    }    
}

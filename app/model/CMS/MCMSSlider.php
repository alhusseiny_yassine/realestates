<?php

namespace App\model\CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Session;
use Config;
use App\model\CMSMedia;
use DB;

class MCMSSlider extends Model
{
    protected   $table          = 'cms_sliders';
    public      $timestamps     = false;
    protected   $primaryKey     = "cs_id";


    public static function getIdField()
    {
        $model = new MCMSSlider;

        return $model->primaryKey;
    }

    public static function saveinfo($input)
    {
        if( isset($input['cs_id']) &&  $input['cs_id'] > 0 )
        {
            $cms_slider = MCMSSlider::find($input['cs_id']);
            $cms_slider->cs_slider_title              = $input['cs_slider_title'];
            $cms_slider->cs_slider_descrition         = $input['cs_slider_descrition'];
        }
        else
        {
            $cms_slider = new MCMSSlider;
            $cms_slider->cs_slider_title              = $input['cs_slider_title'];
            $cms_slider->cs_slider_creation_date      = $input['cs_slider_creation_date'];
            $cms_slider->cs_slider_descrition         = $input['cs_slider_descrition'];
        }


        $cms_slider->save();
    }


    public static function getListSliders()
    {
        $listSliders      = DB::table('cms_sliders')->where('cs_is_deleted',0)->get();
        return $listSliders;
    }


    /**
     * get list of images in a array ordering by Slide Id
     *
     * @author Moe Mantach
     * @access public
     * @param Integer $pageId
     */
    public static function getListOfImagesPerSlider( $pageId  )
    {
        $listSliders      = DB::table('cms_sliders')->where('cs_is_deleted',0)->get();
        $slideArray = array();

        for ($i = 0; $i < count($listSliders); $i++)
        {
            $listImagesSlider = CMSMedia::getImagesSlider($listSliders[$i]->cs_id );
            
            for ($j = 0; $j < count( $listImagesSlider ); $j++)
            {
                $imageUrl = url( Config::get( 'constants.ALBUMS_PATH') . $listImagesSlider[$j]->cm_media_base_dir . $listImagesSlider[$j]->cm_media_file_name  . "." . $listImagesSlider[$j]->cm_media_file_extention );
                $slideArray[ $listSliders[$i]->cs_id ]['images'][$j]['url']         = $imageUrl;
                $slideArray[ $listSliders[$i]->cs_id ]['images'][$j]['description'] = $listImagesSlider[$j]->cm_media_caption;
                $slideArray[ $listSliders[$i]->cs_id ]['images'][$j]['title']       = $listImagesSlider[$j]->cm_media_title;
            }
        }

        return $slideArray;
    }


    /**
     * get slide information
     *
     * @author Moe Mantach
     * @param Integer $id
     *
     * @return Obj
     */
    public static function getSlideInfo( $id )
    {
        $slide_info      = DB::table('cms_sliders')->where('cs_id' , $id)->where('cs_is_deleted',0)->get();
        return $slide_info;
    }

    public static function deleteRow($id)
    {
        $cms_slider = self::find($id);
        $cms_slider->cs_is_deleted   = 1;
        $cms_slider->save();
    }
}
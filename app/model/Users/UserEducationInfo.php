<?php
/***********************************************************
UserEducationInfo.php
Product :
Version : 1.0
Release : 2
Date Created : Mar 17, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/



namespace App\model\Users;

use Illuminate\Database\Eloquent\Model;

class UserEducationInfo extends Model
{
    protected   $table          = 'users_eduction_info';
    public      $timestamps     = false;
    protected   $primaryKey     = "ue_id";
}
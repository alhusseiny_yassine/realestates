<?php
/***********************************************************
UserTypes.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 23, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad MantachCOPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

namespace App\model\Users;

use Illuminate\Database\Eloquent\Model;

class UserTypes extends Model
{
    protected   $table          = 'user_types';
    public      $timestamps     = false;
    protected   $primaryKey     = "ut_id";
    
    const    ADMIN_USER_TYPE_ID         = 1;
    const    COORDINATORS_USER_TYPE_ID  = 2;
    const    PROVIDERS_USER_TYPE_ID     = 3;
    const    TRAINEES_USER_TYPE_ID      = 4;
    const    TRAINERS_USER_TYPE_ID      = 5;
}
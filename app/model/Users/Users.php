<?php

namespace App\model\Users;

use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;
use App\model\Trainees\History;

class Users extends Model
{
    use \HighIdeas\UsersOnline\Traits\UsersOnlineTrait;
    protected   $table          = 'users';
    public      $timestamps     = false;
    protected   $primaryKey     = "id";


    public static function SaveUserData( Request $request )
    {

        $user_id                        = $request->input('user_id');
        $username                       = $request->input('u_email');
        $u_password = $request->input('u_password');
        if(strlen($u_password) > 0)
        {
            $u_password                     = Hash::make( $request->input('u_password') );
        }

        $fb_user_type                   = $request->input('fb_user_type');
        $u_role                         = $request->input('u_role');
        $fullname                       = $request->input('fullname');
        $u_gender                       = $request->input('u_gender');
        $fk_province_id                 = 0;
        $fk_district_id                 = 0;
        $u_mobile                       = $request->input('u_mobile');
        $u_email                         = $request->input('u_email');
        $u_phone                        = $request->input('u_phone');
        $u_fax                          = $request->input('u_fax');
        $u_date_birth                   = $request->input('u_date_birth');
        $u_date_birth                   = $u_date_birth . "-1-1";
        $u_residential_area             = $request->input('u_residential_area');
        $fk_beneficiary_id              = $request->input('fk_beneficiary_id');
        $u_coordinator_code             = $request->input('u_coordinator_code');
        $u_registration_code            = $request->input('u_registration_code');
        $u_contact_person               = $request->input('u_contact_person');
        $u_employment_date              = $request->input('u_employment_date');
        $fk_provider_id                 = $request->input('fk_provider_id');
        $fk_provider_id                 = ( strlen($fk_provider_id) > 0 ) ? $fk_provider_id : "0";
        $u_employment_date              = ( strlen($u_employment_date) > 0 ) ? $u_employment_date : date("Y-m-d");
        $fk_coordinator_id              = $request->input('fk_coordinator_id');
        $u_emp_degree                   = $request->input('u_emp_degree');
        $u_gov_grade                    = $request->input('u_gov_grade');
        $u_preferred_train_days         = $request->input('u_preferred_train_days');
        $u_preferred_train_time         = $request->input('u_preferred_train_time');
        $u_preferred_train_location     = $request->input('u_preferred_train_location');
        $u_coordinator_code             = $request->input('u_coordinator_code');
        $u_is_active                    = $request->input('u_is_active');
        $u_trainee_service              = $request->input('u_trainee_service');
        $u_trainee_bureau               = $request->input('u_trainee_bureau');
        $u_trainee_section              = $request->input('u_trainee_section');
        $u_job_title                    = $request->input('u_job_title');
        $u_job_role                     = $request->input('u_job_role');
        $u_grade_type                   = $request->input('u_grade_type');
        $u_trainee_section              = ( strlen($u_trainee_section) > 0 ) ? $u_trainee_section : "";


        $ini_beneficiary_id             = $request->input('ini_beneficiary_id');
        $ini_coordinator_id             = $request->input('ini_coordinator_id');
        $ini_emp_degree                 = $request->input('ini_emp_degree');
        $ini_gov_grade                  = $request->input('ini_gov_grade');
        $ini_trainee_service            = $request->input('ini_trainee_service');
        $ini_trainee_bureau             = $request->input('ini_trainee_bureau');
        $ini_residential_area           = $request->input('ini_residential_area');

        $user_model = new Users();

        if($user_id != null)
        {
            $user_model = Users::find($user_id);
        }

        $u_job_title                    = $request->input('u_job_title');
        $u_job_role                     = $request->input('u_job_role');
        $u_grade_type                   = $request->input('u_grade_type');

        $user_model->u_username = $username;
        if($u_password != '')
        $user_model->password = $u_password;
        $user_model->u_user_type = $fb_user_type;
        $user_model->fk_role_id = $u_role;
        $user_model->u_country = 0;
        $user_model->u_fullname = $fullname;
        $user_model->u_gender = $u_gender;
        $user_model->u_email = $u_email;
        $user_model->fk_province_id = $fk_province_id;
        $user_model->fk_district_id = $fk_district_id;
        $user_model->u_mobile = $u_mobile;
        $user_model->u_phone = $u_phone;
        $user_model->u_fax = $u_fax;
        $user_model->u_date_birth = $u_date_birth;
        $user_model->u_job_title = $u_job_title;
        $user_model->u_job_role = $u_job_role;
        $user_model->u_grade_type = $u_grade_type;
        $user_model->u_residential_area = $u_residential_area;
        $user_model->fk_beneficiary_id = $fk_beneficiary_id;
        $user_model->u_coordinator_code = $u_coordinator_code;
        $user_model->u_registration_code = $u_registration_code;
        $user_model->u_contact_person = $u_contact_person;
        $user_model->u_employment_date = $u_employment_date;
        $user_model->fk_coordinator_id = $fk_coordinator_id;
        $user_model->u_emp_degree = $u_emp_degree;
        $user_model->u_gov_grade = $u_gov_grade;
        $user_model->u_preferred_train_days = $u_preferred_train_days;
        $user_model->u_preferred_train_time = $u_preferred_train_time;
        $user_model->u_preferred_train_location = $u_preferred_train_location;
        $user_model->u_coordinator_code = $u_coordinator_code;
        $user_model->u_trainee_service = $u_trainee_service;
        $user_model->u_trainee_bureau = $u_trainee_bureau;
        $user_model->u_trainee_section = $u_trainee_section;
        $user_model->fk_provider_id = $fk_provider_id;
        $user_model->u_is_active = $u_is_active == 1 ? 1 : 0;


        $user_model->save();



        // we cannot save logs expet it is in edit mode and if the type of user is trainee
        if($user_id != null && $fb_user_type == UserTypes::TRAINEES_USER_TYPE_ID)
        {
            $history = new History();
            $history->fk_trainee_id = $user_id;
            $history->th_date_history = date("Y-m-d H:i:s");
            if( $ini_beneficiary_id != $fk_beneficiary_id )
            {
                $history->fk_benf_id = $ini_beneficiary_id;
            }

            if( $ini_coordinator_id != $fk_coordinator_id )
            {
                $history->fk_coordinator_id = $ini_coordinator_id;
            }

            if( $ini_emp_degree != $u_emp_degree )
            {
                $history->th_emp_degree = $ini_emp_degree;
            }

            if( $ini_gov_grade != $u_gov_grade )
            {
                $history->th_gov_grade = $ini_gov_grade;
            }

            if( $ini_trainee_service != $u_trainee_service )
            {
                $history->th_trainee_service = $ini_trainee_service;
            }

            if( $ini_trainee_bureau != $u_trainee_bureau )
            {
                $history->th_trainee_bureau = $ini_trainee_bureau;
            }


            if( $ini_residential_area != $u_residential_area )
            {
                $history->th_residential_area = $ini_residential_area;
            }

            $history->save();

        }

        $user_id = $user_model->id;
        $result_array = array();

        $result_array['is_error'] = 0;

        $result_array['u_user_type']    = $fb_user_type;
        $result_array['user_id']        = $user_id;
        return $result_array;
    }


    /**
     * Delete User by change flag of is_deleted to 1
     *
     * @author Moe Mantach
     * @access public
     * @param Request $user_id
     */
    public static function deleteUserData( $users_ids )
    {
        $session_user_id = Session('user_id');
        $users_ids_array = explode(",", $users_ids);
        DB::table('users')->whereIn('id', $users_ids_array)->update(['u_is_deleted' => 1 , 'u_deleted_by' => $session_user_id]);

    }

}

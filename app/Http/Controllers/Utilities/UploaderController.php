<?php
/***********************************************************
UploaderController.php
Product :
Version : 1.0
Release : 2
Date Created : May 4, 2016
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/



namespace App\Http\Controllers\Utilities;

use App\User;
use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Config;
use Redirect;
use File;
use DB;
use Crypt;
use App\Http\Controllers\Controller;
use App\Models\CMS\CMSClientsPortfolio;
use Illuminate\Support\Facades\Hash;
use App\Models\Posts\PostsMedia;
use Intervention\Image\ImageManagerStatic as Image;
use App\model\Users\Users;


class UploaderController extends Controller
{
    public function Index( $key , Request $request)
    {
        switch( $key )
        {
            case 'profile':
                {
                     $result_array = array();

                     $file_name = $_FILES['u_profile_pic']['name'];
                     $file_type = $_FILES['u_profile_pic']['type'];
                     $file_tmp  = $_FILES['u_profile_pic']['tmp_name'];

                     $user_id = Session('user_id');
                     $profile_user_info = Users::find($user_id);
                     $u_avatar_base_src = $profile_user_info->u_avatar_base_src;

                  if($u_avatar_base_src == NULL)
                  {
                     $base_dir = date('Y/m/d/');
                  }

                  else
                  {
                     $base_dir = $u_avatar_base_src;
                  }


                     $directory = public_path(). "/" . Config::get( 'constants.PROFILE_PICTURE_PATH') . $base_dir;
                     $main_url =url('/'). "/" . Config::get( 'constants.PROFILE_PICTURE_PATH') . $base_dir;
                     if(!is_dir($directory))
                     {
                         $result = File::makeDirectory($directory, 0777, true);
                     }

                     $file_info = explode(".", $file_name);

                     $extention  = $file_info[count($file_info) - 1];
                     $file_name  = md5(date("Y-m-d H:i:s") ). "_" . date("YmdHis") . "_" . rand(0, 8888888);

                     $file_path = $directory . $file_name . "." . $extention;
                     $image_url = $main_url . $file_name . "." . $extention;


                     if(move_uploaded_file($file_tmp, $file_path))
                     {

                         $profile_user_info->u_avatar_base_src  = $base_dir;
                         $profile_user_info->u_avatar_filename  = $file_name;
                         $profile_user_info->u_avatar_extentions = $extention;

                         $profile_user_info->save();
                     }
                     session()->put('user_profile_url' , $image_url);
                     $result_array = array();
                     $result_array['is_error']  = 0;
                     $result_array['error_msg'] = "operation Complete Successfuly";
                     $result_array['image_url'] = $image_url;
                     echo json_encode($result_array);
                }
            break;
            case 'fprofile':
                {
                     $result_array = array();

                     $file_name = $_FILES['u_profile_pic']['name'];
                     $file_type = $_FILES['u_profile_pic']['type'];
                     $file_tmp  = $_FILES['u_profile_pic']['tmp_name'];

                     $user_id = Session('ut_user_id');
                     $profile_user_info = Users::find($user_id);
                     $u_avatar_base_src = $profile_user_info->u_avatar_base_src;

                  if($u_avatar_base_src == NULL)
                  {
                     $base_dir = date('Y/m/d/');
                  }

                  else
                  {
                     $base_dir = $u_avatar_base_src;
                  }


                     $directory = public_path(). "/" . Config::get( 'constants.PROFILE_PICTURE_PATH') . $base_dir;
                     $main_url =url('/'). "/" . Config::get( 'constants.PROFILE_PICTURE_PATH') . $base_dir;
                     if(!is_dir($directory))
                     {
                         $result = File::makeDirectory($directory, 0777, true);
                     }

                     $file_info = explode(".", $file_name);

                     $extention  = $file_info[count($file_info) - 1];
                     $file_name  = md5(date("Y-m-d H:i:s") ). "_" . date("YmdHis") . "_" . rand(0, 8888888);

                     $file_path = $directory . $file_name . "." . $extention;
                     $image_url = $main_url . $file_name . "." . $extention;


                     if(move_uploaded_file($file_tmp, $file_path))
                     {

                         $profile_user_info->u_avatar_base_src  = $base_dir;
                         $profile_user_info->u_avatar_filename  = $file_name;
                         $profile_user_info->u_avatar_extentions = $extention;

                         $profile_user_info->save();
                     }
                     session()->put('ut_user_profile_url' , $image_url);
                     $result_array = array();
                     $result_array['is_error']  = 0;
                     $result_array['error_msg'] = "operation Complete Successfuly";
                     $result_array['image_url'] = $image_url;
                     echo json_encode($result_array);
                }
            break;
            case "SliderImages":

                {
                    $slide_id    = isset( $_POST['slider_id']) ? $_POST['slider_id'] : 0;
                    $cm_id       = isset( $_POST['cm_id']) ? $_POST['cm_id'] : 0;

                    $file_name   = $_FILES['files']['name'];
                    if(is_array( $_FILES['files']['name']))
                        $file_name = $_FILES['files']['name'][0];
                    $file_type  = $_FILES['files']['type'];
                    if(is_array( $_FILES['files']['type']))
                        $file_type  = $_FILES['files']['type'][0];
                    $file_tmp   = $_FILES['files']['tmp_name'];
                    if(is_array( $_FILES['files']['tmp_name']))
                        $file_tmp   = $_FILES['files']['tmp_name'][0];
                    $file_size  = $_FILES['files']['size'];
                    if(is_array( $_FILES['files']['size']))
                        $file_size  = $_FILES['files']['size'][0];

                    $cms_media_display  = DB::table('cms_media')->where('cm_id', $cm_id)->where('cm_media_is_active', 1)->get();


                    if(count($cms_media_display) > 0)
                        $base_dir = $cms_media_display[0]->cm_media_base_dir;
                    else
                        $base_dir = "";

                    if($base_dir == '')
                    {
                        $base_dir = date('Y/m/d/');
                    }


                    $directory = public_path(). "/" . Config::get( 'constants.ALBUMS_PATH') . $base_dir;
                    $image_url =url('/'). "/" . Config::get( 'constants.ALBUMS_PATH') . $base_dir;

                    if(!is_dir($directory))
                    {
                        $result = File::makeDirectory($directory, 0777, true);

                    }

                    $file_info = explode(".", $file_name);
                    $extention  = $file_info[count($file_info) - 1];
                    $file_name  = md5(date("Y-m-d H:i:s") ). "_" . date("YmdHis") . "_" . rand(0, 8888888);

                    $result_array = array();

                    $file_path = $directory . $file_name . "." . $extention;
                    $image_url = $image_url . $file_name . "." . $extention;

                    if(move_uploaded_file($file_tmp, $file_path))
                    {
                        if($cm_id > 0)
                        {

                            $fields_array = array(
                                'cm_media_base_dir' => $base_dir,
                                'cm_media_file_name' => $file_name,
                                'cm_media_file_extention' => $extention
                            );
                            DB::table('cms_media')->where('cm_id', $cm_id)->update($fields_array);

                            $result_array['files']['deleteType']     = 'DELETE';
                            $result_array['files']['deleteUrl']      =url('/') . '/administrator/DeleteMedia/' . $cm_id;
                            $result_array['files']['name']           = $file_name;
                            $result_array['files']['cm_id']          = $cm_id;
                            $result_array['files']['size']           = $file_size;
                            $result_array['files']['type']           = $file_type;
                            $result_array['files']['url']            = $image_url. "?=v=".rand(0, 99999);
                            $result_array['files']['thumbnailUrl']   = $image_url . "?=v=".rand(0, 99999);

                        }
                        else
                        {
                            $fields_array = array(
                                'fk_slider_id' => $slide_id,
                                'cm_media_is_active' => 1,
                                'cm_date_creation' => date("Y-m-d H:i:s"),
                                'cm_media_base_dir' => $base_dir,
                                'cm_media_file_name' => $file_name,
                                'cm_media_file_extention' => $extention
                            );

                            $cm_id =   DB::table('cms_media')->insertGetId($fields_array);

                            $result_array['files'][0]['deleteType']     = 'DELETE';
                            $result_array['files'][0]['deleteUrl']      =url('/') . '/administrator/DeleteMedia/' . $cm_id;
                            $result_array['files'][0]['name']           = $file_name;
                            $result_array['files'][0]['cm_id']          = $cm_id;
                            $result_array['files'][0]['size']           = $file_size;
                            $result_array['files'][0]['type']           = $file_type;
                            $result_array['files'][0]['url']            = $image_url. "?=v=".rand(0, 99999);
                            $result_array['files'][0]['thumbnailUrl']   = $image_url . "?=v=".rand(0, 99999);

                        }

                    }
                    else
                    {

                        $result_array['is_error'] = 1;
                        $result_array['error_msg'] = "Error During Upload This File";

                    }
                    return Response()->json($result_array);

                }
                break;

        }
    }
}
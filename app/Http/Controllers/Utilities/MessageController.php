<?php
/***********************************************************
MessageController.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 18, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

namespace App\Http\Controllers\Utilities;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use Crypt;
use Mail;
use DB;
use App\model\System\Roles;
use App\model\Users\Users;
use App\Libraries\Users\UserManager;
use App\model\Users\UserTypes;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;



class MessageController extends Controller
{
    public function PermissionDenied()
    {
        
        return Response()->view('errors.permissions');
    }
}
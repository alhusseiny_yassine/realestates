<?php
/***********************************************************
DropDownController.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 23, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad MantachCOPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Http\Controllers\Utilities;


use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\Users\Users;
use App\model\Users\UserTypes;
use App\model\System\Roles;
use App\model\System\Country;
use App\model\System\Beneficiaries;
use App\model\System\Provinces;
use App\model\System\Districts;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\model\Project\ProjectBeneficiaries;
use App\model\Project\Projects;


class DropDownController extends Controller
{

    public function GetDistrictsProvince( Request $request )
    {
        $province_id = $request->input('province_id');

        $districts_data = Districts::whereFkProvinceId($province_id)->get();

        $result_array = array(
            "districts_data" => $districts_data
        );
        $result_array['display'] = view('utilities.districtsDropDown')->render();

        return  Response()->json($result_array);
    }
    
    
    /**
     * Generate Dropdown with data based on sending type
     * @param String $type
     */
    public function GenerateDropDown( $type , Request $request )
    {
        
        $result_array = array();
        
        
        switch($type)
        {
            case "projects":
                {
                    $sb_beneficiary_id      = $request->input('sb_beneficiary_id');
                    $name                       = $request->input('name');
                    $id                             = $request->input('id');
                    $projectBenef = ProjectBeneficiaries::whereFkBenfId($sb_beneficiary_id)->get()->toArray();
                    $project_ids = array();
                    
                    foreach ($projectBenef as $key => $pb_info) {
                        $project_ids[] = $pb_info['fk_project_id'];
                    }
                    
                    $lst_projects = Projects::whereIn('pp_id',$project_ids)->get();
                    
                    $projects_dropdown_array =  CreateDropDownArrayByIndex($lst_projects , "pp_id", "pp_project_title");
                   
                    $data = array(
                        "projects_dropdown_array" => $projects_dropdown_array,
                        "id" => $id,
                        "name" => $name
                    );
                    $result_array['display'] = view('html.select',$data)->render();
                }
            break;
        }
        
        return Response()->json($result_array);
    }

}
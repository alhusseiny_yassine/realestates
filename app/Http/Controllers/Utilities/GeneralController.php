<?php
/***********************************************************
GenerateController.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 24, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Http\Controllers\Utilities;


use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\Users\Users;
use App\model\Users\UserTypes;
use App\model\System\Roles;
use App\model\System\Country;
use App\model\System\Beneficiaries;
use App\model\System\Provinces;
use App\model\System\Districts;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Libraries\System\BeneficiaryManagement;
use App\Libraries\Users\UserManager;
use App\model\System\RolePrivileges;
use App\model\System\PrivilegedActions;
use App\Libraries\Roles\PrivilegedActionsManagament;
use App\Libraries\Roles\RolePrivilegesManagement;
use App\model\Requests\RequestStatus;
use App\model\Project\ProjectStatus;
use App\model\Sessions\SessionStatus;
use App\model\System\JobTitles;
use App\model\System\JobRoles;


class GeneralController extends Controller
{

    public function GenerateCoordinatorCode( Request $request )
    {

        $result_array = array();
        $current_date = date('Y-m-d H:i:s');
        $encoding = md5($current_date);

        $result_array['code'] = substr($encoding, 1,6);
        return  Response()->json($result_array);
    }

    public function CheckIfUsernameExist( Request $request )
    {
        $result_array = array();
        $user_name = $request->input('user_name');
        $user_id   = $request->input('user_id');

        $user_count = Users::whereUUsername($user_name)->where('id','!=',$user_id)->get();
        if(count($user_count) > 0)
        {
            $result_array['exist'] = 1;
        }
        else
        {
            $result_array['exist'] = 0;
        }

        return  Response()->json($result_array);
    }



    public function PrintListData($data_type)
    {
        $benf_info = BeneficiaryManagement::getBeneficiariesById();
        $user_types = UserManager::getUserNameTypes();

        switch ($data_type)
        {
            case "users":
            {
                $lst_users = Users::whereUIsActive(1)->whereUIsDeleted(0)->get();

                $data = array(
                    "lst_users" => $lst_users,
                    "benf_info" => $benf_info,
                    "user_types" => $user_types,
                );
                return Response()->view('general.prints.users',$data);
            }
            case "benficiaries":
            {
                $lst_benficiaries = Beneficiaries::whereSbIsDeleted(0)->get();

                $data = array(
                    "lst_benficiaries" => $lst_benficiaries
                );
                return Response()->view('general.prints.beneficiaries',$data);
            }
            case "roles":
            {
                $lst_roles = Roles::whereRoleIsActive(1)->whereRoleIsDeleted(0)->get();

                $data = array(
                    "lst_roles" => $lst_roles,
                );
                return Response()->view('general.prints.roles',$data);
            }
            case "privileges":
            {
                $request = Request();
                $role_id = $request->input('role_id');
                
                $roles = Roles::find($role_id);
                $pa_result_array = PrivilegedActionsManagament::getAll();
                $rp_result_array = RolePrivilegesManagement::getAll( $role_id );
                
                $data = array(
                    "roles" =>$roles ,
                    "pa_result_array" => $pa_result_array,
                    "rp_result_array" => $rp_result_array
                );
                
                $result_array = array();

                $result_array['is_error'] = 0;
                $result_array['display'] = view("general.prints.privileges",$data)->render();
                return $result_array;
            }
            case "requests":
            {
                $lst_request_status = RequestStatus::whereRsIsDeleted(0)->get();

                $data = array(
                    "lst_request_status" => $lst_request_status,
                );
                return Response()->view('general.prints.requests',$data);
            }
            case "projectstatus":
            {
                $lst_project_status = ProjectStatus::wherePsIsDeleted(0)->get();
                
                $data = array(
                    "lst_project_status" => $lst_project_status
                );
                return Response()->view('general.prints.projectstatus',$data);
            }
            case "sessionstatus":
            {
                $lst_session_status = SessionStatus::whereSsIsDeleted(0)->get();
                
                $data = array(
                    "lst_session_status" => $lst_session_status
                );
                return Response()->view('general.prints.sessionstatus',$data);
            }
            case "jobtitle": {
                    $lst_job_titles = JobTitles::whereJtIsDeleted(0)->get();

                    $data = array(
                        "lst_job_titles" => $lst_job_titles
                    );
                    return Response()->view('general.prints.jobtitles', $data);
                }
            case "jobrole": {
                    $lst_job_roles = JobRoles::whereJrIsDeleted(0)->get();

                    $data = array(
                        "lst_job_roles" => $lst_job_roles
                    );
                    return Response()->view('general.prints.jobroles', $data);
                }    
                break;
        }
    }
    
    public function ExportToExcel($data_type)
    {
        $benf_info = BeneficiaryManagement::getBeneficiariesById();
        $user_types = UserManager::getUserNameTypes();

        switch ($data_type)
        {
            case "users":
            {
                $lst_users = Users::whereUIsActive(1)->whereUIsDeleted(0)->get();
                
                $file = 'usersFile-'.date("Y-M-D")."-".time().'.xls';
                
                    ob_start();
                    echo "<table border='1'> ";
                    echo "<tr><th> ID </th><th> Username </th><th> Full Name </th><th> Email </th><th> User Type </th><th> Beneficiary </th></tr>";
                    foreach ($lst_users as $index => $user_info) {

                        $id = $user_info->id;
                        $u_username = $user_info->u_username;
                        $u_fullname = $user_info->u_fullname;
                        $u_email = $user_info->u_email;
                        $u_user_type = isset($user_types[$user_info->u_user_type]) ? $user_types[$user_info->u_user_type] : "N/A";
                        $fk_beneficiary_id = isset($benf_info[$user_info->fk_beneficiary_id]) ? $benf_info[$user_info->fk_beneficiary_id] : "N/A";

                        echo "<tr><td> $id </td><td> $u_username </td><td> $u_fullname </td><td> $u_email </td><td> $u_user_type </td><td> $fk_beneficiary_id </td></tr>";
                    }
                    echo '</table>';
                    $content = ob_get_contents();
                    ob_end_clean();
                    header("Expires: 0");
                    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
                    header("Cache-Control: no-store, no-cache, must-revalidate");
                    header("Cache-Control: post-check=0, pre-check=0", false);
                    header("Pragma: no-cache");
                    header("Content-type: application/vnd.ms-excel;charset:UTF-8");
                    header('Content-length: ' . strlen($content));
                    header('Content-disposition: attachment; filename=' . basename($file));
                    echo $content;
                
                return;
            }
            case "roles":
            {
                $lst_roles = Roles::whereRoleIsActive(1)->whereRoleIsDeleted(0)->get();
                
                $file = 'rolesFile-'.date("Y-M-D")."-".time().'.xls';
                
                    ob_start();
                    echo "<table border='1'> ";
                    echo "<tr><th> ID </th><th> Role </th><th> Description </th></tr>";
                    
                    foreach ($lst_roles as $index => $role_info) {

                        $id          = $role_info->role_id;
                        $role        = $role_info->role_name;
                        $description = $role_info->role_name;

                        echo "<tr><td> $id </td><td> $role </td><td> $description </td></tr>";
                    }
                    echo '</table>';
                    $content = ob_get_contents();
                    ob_end_clean();
                    header("Expires: 0");
                    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
                    header("Cache-Control: no-store, no-cache, must-revalidate");
                    header("Cache-Control: post-check=0, pre-check=0", false);
                    header("Pragma: no-cache");
                    header("Content-type: application/vnd.ms-excel;charset:UTF-8");
                    header('Content-length: ' . strlen($content));
                    header('Content-disposition: attachment; filename=' . basename($file));
                    echo $content;
                
                return;
            }
            case "requests":
            {
                $lst_request_status = RequestStatus::whereRsIsDeleted(0)->get();

               $file = 'requestsFile-'.date("Y-M-D")."-".time().'.xls';
                
                    ob_start();
                    echo "<table border='1'> ";
                    echo "<tr><th> ID </th><th> Request Status Name </th><th> Request Status Color </th></tr>";
                    
                    foreach ($lst_request_status as $index => $status_info) {

                        echo "<tr><td> $status_info->rs_id </td><td> $status_info->rs_status_name </td><td style='background-color: $status_info->rs_status_color'> $status_info->rs_status_color </td></tr>";
                    }
                    echo '</table>';
                    $content = ob_get_contents();
                    ob_end_clean();
                    header("Expires: 0");
                    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
                    header("Cache-Control: no-store, no-cache, must-revalidate");
                    header("Cache-Control: post-check=0, pre-check=0", false);
                    header("Pragma: no-cache");
                    header("Content-type: application/vnd.ms-excel;charset:UTF-8");
                    header('Content-length: ' . strlen($content));
                    header('Content-disposition: attachment; filename=' . basename($file));
                    echo $content;
                    
                return ;
            }
            case "projectstatus":
            {
                $lst_project_status = ProjectStatus::wherePsIsDeleted(0)->get();
                
                $file = 'projectstatusFile-'.date("Y-M-D")."-".time().'.xls';
                
                    ob_start();
                    echo "<table border='1'> ";
                    echo "<tr><th> ID </th><th> Project Status Name </th><th> Project Status Color </th></tr>";
                    
                    foreach ($lst_project_status as $index => $status_info) {

                        echo "<tr><td> $status_info->ps_id </td><td> $status_info->ps_status_name </td><td style='background-color: $status_info->ps_status_color'> $status_info->ps_status_color </td></tr>";
                    }
                    echo '</table>';
                    $content = ob_get_contents();
                    ob_end_clean();
                    header("Expires: 0");
                    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
                    header("Cache-Control: no-store, no-cache, must-revalidate");
                    header("Cache-Control: post-check=0, pre-check=0", false);
                    header("Pragma: no-cache");
                    header("Content-type: application/vnd.ms-excel;charset:UTF-8");
                    header('Content-length: ' . strlen($content));
                    header('Content-disposition: attachment; filename=' . basename($file));
                    echo $content;
                
                return ;
            }
            case "sessionstatus":
            {
                $lst_session_status = SessionStatus::whereSsIsDeleted(0)->get();
                
                $file = 'sessionstatusFile-'.date("Y-M-D")."-".time().'.xls';
                
                    ob_start();
                    echo "<table border='1'> ";
                    echo "<tr><th> ID </th><th> Session Status Name </th><th> Session Status Color </th></tr>";
                    
                    foreach ($lst_session_status as $index => $status_info) {

                        echo "<tr><td> $status_info->ss_id </td><td> $status_info->ss_status_name </td><td style='background-color: $status_info->ss_status_color'> $status_info->ss_status_color </td></tr>";
                    }
                    echo '</table>';
                    $content = ob_get_contents();
                    ob_end_clean();
                    header("Expires: 0");
                    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
                    header("Cache-Control: no-store, no-cache, must-revalidate");
                    header("Cache-Control: post-check=0, pre-check=0", false);
                    header("Pragma: no-cache");
                    header("Content-type: application/vnd.ms-excel;charset:UTF-8");
                    header('Content-length: ' . strlen($content));
                    header('Content-disposition: attachment; filename=' . basename($file));
                    echo $content;
                
                return;
            }
            case "jobtitle": {
                    $lst_job_titles = JobTitles::whereJtIsDeleted(0)->get();

                    $file = 'jobtitlesFile-'.date("Y-M-D")."-".time().'.xls';
                
                    ob_start();
                    echo "<table border='1'> ";
                    echo "<tr><th> ID </th><th> Job Title </th></tr>";
                    
                    foreach ($lst_job_titles as $index => $job_title) {

                        echo "<tr><td> $job_title->jt_id </td><td> $job_title->jt_job_title </td></tr>";
                    }
                    echo '</table>';
                    $content = ob_get_contents();
                    ob_end_clean();
                    header("Expires: 0");
                    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
                    header("Cache-Control: no-store, no-cache, must-revalidate");
                    header("Cache-Control: post-check=0, pre-check=0", false);
                    header("Pragma: no-cache");
                    header("Content-type: application/vnd.ms-excel;charset:UTF-8");
                    header('Content-length: ' . strlen($content));
                    header('Content-disposition: attachment; filename=' . basename($file));
                    echo $content;
                    
                    return ;
                }
            case "jobrole": {
                    $lst_job_roles = JobRoles::whereJrIsDeleted(0)->get();

                    $file = 'jobrolesFile-'.date("Y-M-D")."-".time().'.xls';
                
                    ob_start();
                    echo "<table border='1'> ";
                    echo "<tr><th> ID </th><th> Job Role </th></tr>";
                    
                    foreach ($lst_job_roles as $index => $job_role) {

                        echo "<tr><td> $job_role->jr_id </td><td> $job_role->jr_job_role </td></tr>";
                    }
                    echo '</table>';
                    $content = ob_get_contents();
                    ob_end_clean();
                    header("Expires: 0");
                    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
                    header("Cache-Control: no-store, no-cache, must-revalidate");
                    header("Cache-Control: post-check=0, pre-check=0", false);
                    header("Pragma: no-cache");
                    header("Content-type: application/vnd.ms-excel;charset:UTF-8");
                    header('Content-length: ' . strlen($content));
                    header('Content-disposition: attachment; filename=' . basename($file));
                    echo $content;
                    
                    return;
                }    
                break;
        }
    }

    public function ExportPrivilegesToExcel($role_id) {
        
        $roles = Roles::find($role_id);
        $pa_result_array = PrivilegedActionsManagament::getAll();
        $rp_result_array = RolePrivilegesManagement::getAll($role_id);

        $file = 'privilegesFile-' . date("Y-M-D") . "-" . time() . '.xls';
        ob_start();

        foreach ($pa_result_array as $tabTitle => $pa_info) {

            echo "<table border='1'> ";
            echo "<tr><th> $tabTitle </th></tr>";
            echo "<tr><th> Description </th><th></th></tr>";

            foreach ($pa_info as $index => $pa_priv_info) {

                $check = ( count($rp_result_array) > 0 && isset($rp_result_array[$pa_priv_info['code']]) && $rp_result_array[$pa_priv_info['code']] == 'allow' ) ? "allow" : "deny";

                echo "<tr><td> " . $pa_priv_info['description'] . " </td><td> " . $check . " </td></tr>";
            }

            echo "<tr></tr>";
            echo '</table>';
        }
        $content = ob_get_contents();
        ob_end_clean();
        header("Expires: 0");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Content-type: application/vnd.ms-excel;charset:UTF-8");
        header('Content-length: ' . strlen($content));
        header('Content-disposition: attachment; filename=' . basename($file));
        echo $content;
        return;
    }

}
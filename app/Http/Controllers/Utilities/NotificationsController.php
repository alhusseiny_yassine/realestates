<?php
/***********************************************************
NotificationsController.php
Product :
Version : 1.0
Release : 2
Date Created : Apr 27, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

namespace App\Http\Controllers\Utilities;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\System\Roles;
use App\model\System\Courses;
use App\model\System\CourseType;
use App\model\System\Days;
use App\Libraries\Courses\CourseManagement;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\model\System\CourseCategories;
use App\model\Project\ProjectCourses;
use App\model\Project\ProjectCourseStatus;
use App\model\Project\ProjectCourseTrainees;
use App\model\Notifications\Notifications;
use App\model\Project\ProjectNotifications;


class NotificationsController extends Controller
{
    
    /**
     * Page to Display List of Notifications sent to current User
     */
    public function ListNotifications()
    {
        $user_id = session('ut_user_id');
        $LstNotifications = Notifications::whereFkUserId($user_id)->get();
        $rqstNotifications = ProjectNotifications::whereFkUserId($user_id)->get();

        $data = array(
            "LstNotifications" => $LstNotifications,
            "RqstNotifications" =>$rqstNotifications
        );
        return Response()->view('notifications.lstnotifications',$data);
    }

}
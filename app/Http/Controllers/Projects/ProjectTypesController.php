<?php
/***********************************************************
ProjectTypesController.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 28, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

namespace App\Http\Controllers\Projects;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\System\Roles;
use App\model\Project\ProjectTypes;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;


class ProjectTypesController extends Controller
{
    public function ProjectTypesManagement()
    {
        $data = array();
        return Response()->view('projecttypes.projecttypes',$data);
    }


    public function DisplayListProjectTypes(Request $request)
    {
        $page_number        = $request->input('page_number');
        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;



        $all_project_types = ProjectTypes::wherePtIsDeleted(0)->count();


        $total_pages = ceil($all_project_types/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);

        $project_types = ProjectTypes::wherePtIsDeleted(0)->skip($skip)->take($nbr_rows_per_pages)->get();
        $data = array(
            "project_types" => $project_types
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("projecttypes.displaylist",$data)->render();

        return Response()->json($result_array);
    }


    public function AddProjectTypeForm()
    {

        $data = array();
        return view('projecttypes.addprojecttype',$data);
    }


    /**
     * Save Project Type information to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function SaveProjectTypeInfo(Request $request)
    {
        $pt_id                          = $request->input('pt_id');
        $pt_project_type                = $request->input('pt_project_type');
        $pt_project_description         = $request->input('pt_project_description');

        $result_array = array();

        $ProjectType = new ProjectTypes();
        if($pt_id !== null)
        {
            $ProjectType = ProjectTypes::find($pt_id);
        }

        $ProjectType->pt_project_type           = $pt_project_type;
        $ProjectType->pt_project_description    = $pt_project_description;
        $ProjectType->save();


        $result_array['is_error']  = 0;
        $result_array['error_msg'] = 'Project Type Information Has been saved';

        return Response()->json($result_array);
    }



    /**
     * Display Edit Project Type Form Page
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $bt_id
     */
    public function EditProjectTypeForm( $pt_id )
    {

        $cur_project_type_info    = ProjectTypes::find( $pt_id );


        $data = array(
            "cur_project_type_info" => $cur_project_type_info
        );
        return view('projecttypes.editprojecttype',$data);
    }


    public function DeleteProjectTypeInfo(Request $request)
    {
        $pt_id = $request->input('pt_id');

        $ProjectType = new ProjectTypes();
        $ProjectType = ProjectTypes::find($pt_id);
        $ProjectType->pt_is_deleted          = 1;
        $ProjectType->pt_deleted_by          = Session('user_id');
        $ProjectType->save();


        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return Response()->json($result_array);
    }

}
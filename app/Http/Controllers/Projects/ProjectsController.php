<?php
/***********************************************************
ProjectsController.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 1, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :

***********************************************************/

namespace App\Http\Controllers\Projects;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\System\Roles;
use App\model\Project\ProjectStatus;
use App\model\Project\Projects;
use App\model\Requests\Request As MRequest;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\model\Requests\RequestsCourses;
use App\model\Project\ProjectTypes;
use App\model\System\Donor;
use App\model\System\Courses;
use App\model\Users\Users;
use App\model\Users\UserTypes;
use App\model\Requests\RequestStatus;
use App\model\Project\ProjectProvider;
use App\model\Project\ProjectCourses;
use App\model\Project\ProjectClasses;
use App\Libraries\Projects\ProjectManagement;
use App\Libraries\Sessions\SessionsManagement;
use App\model\Project\ProjectTrainees;
use App\model\Project\ProjectRequests;
use App\model\Sessions\SessionClass;
use App\Libraries\Users\UserManager;
use App\model\Project\ProjectGroups;
use App\model\Project\ProjectCoursesGroups;
use App\model\System\Beneficiaries;
use App\model\Project\ProjectBeneficiaries;
use App\model\Project\ProjectCourseStatus;
use App\model\Project\ProjectNotifications;
use App\Libraries\Notifications\ProjectNotificationsManager;
use App\model\System\CourseCategories;


class ProjectsController extends Controller
{
    /**
     * Page contain all functionality related to project creation/Delete and edit current Project
     *
     * @author Moe Mantach
     * @access public
     */
    public function ProjectsManagement()
    {
        $data = array();

        return Response()->view('projects.projects',$data);
    }


    public function ReportListProjects()
    {
        $data = array();
        return Response()->view('reports.projects',$data);
    }


    /**
     * Display List of Projects Created In the System
     *
     * @author Moe Mantach
     * @param Request $request
     */
    public function DisplayListProjects( Request $request )
    {
        $search_project_keywords    = $request->input('search_project_keywords');
        $page_number                = $request->input('project_page_number');
        $nbr_rows_per_pages         = Config::get('appconfig.max_rows_per_page');
        $benf_id                    = session('ut_beneficiary_id');
        $ut_user_type               = session("ut_user_type");
        $ut_user_id               = session("ut_user_id");
        $role_info                  = session()->get('role_info');
        $ut_beneficiary_id          = session()->get('ut_beneficiary_id');
        $result_array = array();


        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;
        $project_obj = Projects::wherePpIsDeleted(0);

        if(strlen($search_project_keywords) > 0)
        {
            $project_obj = $project_obj->where('pp_project_title' , 'LIKE' , '%' . $search_project_keywords . '%');
        }

        switch ($ut_user_type)
        {
            case UserTypes::PROVIDERS_USER_TYPE_ID :
                {
                    $provider_projects_obj = ProjectProvider::whereFkProviderId($ut_user_id)->get();
                    $provider_projects_ids = array();
                    foreach ($provider_projects_obj as $index => $proj_prov_info)
                    {
                        $provider_projects_ids[] = $proj_prov_info->fk_project_id;
                    }

                    $project_obj = $project_obj->whereIn('pp_id' , $provider_projects_ids);
                }
             break;
            default:
                {
                    $result_array['total_pages']    = 0;
                    $result_array['display']        = '';
                    return Response()->json($result_array);
                }
            break;
        }


        $nbr_projects = $project_obj->count();

        $total_pages = ceil($nbr_projects/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);

        $total_projects_obj  = $project_obj->skip($skip)->take($nbr_rows_per_pages)->get();


        $project_ids = array();

        for ($i = 0; $i < count($total_projects_obj); $i++) {
            $project_ids[] = $total_projects_obj[$i]->pp_id;
        }

        $nbr_courses = ProjectManagement::GetNbrOfCoursesInProject($project_ids);
        $nbr_classes = ProjectManagement::GetNbrOfClassesInProject($project_ids);
        $nbr_trainees = ProjectManagement::GetNbrOfTraineesInProject($project_ids);


        $data = array(
            "total_projects_obj" => $total_projects_obj,
            "nbr_courses" => $nbr_courses,
            "nbr_classes" => $nbr_classes,
            "nbr_trainees" => $nbr_trainees,
            "role_info" => $role_info,
        );



        $result_array['total_pages']    = $total_pages;
        $result_array['display']        = view("projects.results",$data)->render();

        return Response()->json($result_array);

    }


    /**
     * Create New Project in the OMSAR Training
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function CreateNewProject(Request $request)
    {
        $project_status         = ProjectStatus::wherePsIsDeleted(0)->get();
        $lst_requests           = MRequest::whereRIsDeleted(0)->whereFkStatusId(RequestStatus::REQUEST_STATUS_APPROVED_BY_OMSAR)->get();
        $project_types          = ProjectTypes::wherePtIsDeleted(0)->get();
        $lst_donors             = Donor::whereDIsDeleted(0)->get();
        $lst_courses            = Courses::whereCIsDeleted(0)->get();
        $lst_beneficiaries      = Beneficiaries::whereSbIsDeleted(0)->orderby('sb_name','ASC')->get();
        $lst_providers          = Users::whereUUserType( UserTypes::PROVIDERS_USER_TYPE_ID )->whereUIsDeleted(0)->whereUIsActive(1)->get();


        $request_courses = array();

        foreach ($lst_requests as $r_id => $request_info) {
            $Srequest = MRequest::find($request_info->r_id);
            foreach ($Srequest->RequestCourses as  $RCourses) {
                $request_courses[$request_info->r_id][$RCourses->fk_course_id] = 0;
            }
        }

        foreach ($lst_requests as $r_id => $request_info) {
            $Srequest = MRequest::find($request_info->r_id);
            foreach ($Srequest->RequestCourses as  $RCourses) {
                $request_courses[$request_info->r_id][$RCourses->fk_course_id]++;
            }
        }

        $text_to_encrypt = rand(0,10) . date("YmDHis");
        $text_to_encrypt = md5($text_to_encrypt);
        $text_to_encrypt = substr($text_to_encrypt, 0 , 10);
        $project_code = "OMS-" . $text_to_encrypt;

        $data = array(
            'project_status' => $project_status,
            'lst_courses' => $lst_courses,
            'lst_requests' => $lst_requests,
            'project_code' => $project_code,
            'project_types' => $project_types,
            'request_courses' => $request_courses,
            'lst_providers' => $lst_providers,
            'lst_beneficiaries' => $lst_beneficiaries,
            'lst_donors' => $lst_donors,
        );

        return Response()->view('projects.createproject',$data);
    }


    /**
     * Page to Create New Project with all iformation from the
     * backend
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function AdminCreationProject(Request $request)
    {

        $project_status         = ProjectStatus::wherePsIsDeleted(0)->get();
        $lst_requests           = MRequest::whereRIsDeleted(0)->whereFkStatusId(RequestStatus::REQUEST_STATUS_APPROVED_BY_OMSAR)->get();
        $project_types          = ProjectTypes::wherePtIsDeleted(0)->get();
        $lst_donors             = Donor::whereDIsDeleted(0)->get();
        $lst_courses            = Courses::whereCIsDeleted(0)->get();
        $lst_beneficiaries      = Beneficiaries::whereSbIsDeleted(0)->orderby('sb_name','ASC')->get();
        $lst_providers          = Users::whereUUserType( UserTypes::PROVIDERS_USER_TYPE_ID )->whereUIsDeleted(0)->whereUIsActive(1)->get();

        $request_courses = array();

        foreach ($lst_requests as $r_id => $request_info) {
            $Srequest = MRequest::find($request_info->r_id);
            foreach ($Srequest->RequestCourses as  $RCourses) {
                $request_courses[$request_info->r_id][$RCourses->fk_course_id] = 0;
            }
        }
        foreach ($lst_requests as $r_id => $request_info) {
            $Srequest = MRequest::find($request_info->r_id);
            foreach ($Srequest->RequestCourses as  $RCourses) {
                $request_courses[$request_info->r_id][$RCourses->fk_course_id]++;
            }
        }

        $text_to_encrypt = rand(0,10) . date("YmDHis");
        $text_to_encrypt = md5($text_to_encrypt);
        $text_to_encrypt = substr($text_to_encrypt, 0 , 10);
        $project_code = "OMS-" . $text_to_encrypt;

        $data = array(
            'project_status' => $project_status,
            'lst_courses' => $lst_courses,
            'lst_requests' => $lst_requests,
            'project_code' => $project_code,
            'project_types' => $project_types,
            'request_courses' => $request_courses,
            'lst_providers' => $lst_providers,
            'lst_beneficiaries' => $lst_beneficiaries,
            'lst_donors' => $lst_donors,
        );

        return Response()->view('projects.admin.addproject',$data);
    }


    /**
     * Edit Project Page
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function EditProjectPage( $project_id )
    {
        $project_info       = Projects::find($project_id);
        $project_providers  = ProjectProvider::whereFkProjectId($project_id)->get();
        $project_trainees   = ProjectTrainees::whereFkProjId($project_id)->get();
        $project_courses    = ProjectCourses::whereFkProjectId($project_id)->get();
        $project_requests   = ProjectRequests::whereFkProjectId($project_id)->get();
        $project_benficiaries   = ProjectBeneficiaries::whereFkProjectId($project_id)->get();

        $providers_array = array();
        foreach ($project_providers as $key => $pc_info) {
            $providers_array[] = $pc_info->fk_trainee_id;
        }

        $trainees_array = array();
        foreach ($project_trainees as $key => $pc_info) {
            $trainees_array[] = $pc_info->fk_trainee_id;
        }
        $courses_array = array();
        foreach ($project_courses as $key => $pc_info) {
            $courses_array[] = $pc_info->fk_course_id;
        }
        $requests_array = array();
        foreach ($project_requests as $key => $pr_info) {
            $requests_array[] = $pr_info->fk_request_id;
        }

        $benf_array = array();
        foreach ($project_benficiaries as $key => $benf_info) {
            $benf_array[] = $benf_info->fk_benf_id;
        }

        $project_code  = "";
        $project_status         = ProjectStatus::wherePsIsDeleted(0)->get();
        $project_types          = ProjectTypes::wherePtIsDeleted(0)->get();
        $lst_donors             = Donor::whereDIsDeleted(0)->get();
        $lst_courses            = Courses::whereCIsDeleted(0)->get();
        $lst_beneficiaries      = Beneficiaries::whereSbIsDeleted(0)->orderby('sb_name','ASC')->get();
        $lst_providers          = Users::whereUUserType( UserTypes::PROVIDERS_USER_TYPE_ID )->whereUIsDeleted(0)->whereUIsActive(1)->get();

        $data = array(
            "project_info" => $project_info,
            "lst_providers" => $lst_providers,
            "lst_courses" => $lst_courses,
            "lst_donors" => $lst_donors,
            "project_types" => $project_types,
            "requests_array" => $requests_array,
            "benf_array" => $benf_array,
            "lst_beneficiaries" => $lst_beneficiaries,
            "project_status" => $project_status,
            "providers_array" => $providers_array,
            "courses_array" => $courses_array,
            "trainees_array" => $trainees_array
        );
        return Response()->view('projects.editproject',$data);
    }


    public function AdmiEditProjectPage( $project_id )
    {
        $project_info       = Projects::find($project_id);

        $project_providers  = ProjectProvider::whereFkProjectId($project_id)->get();
        $project_trainees   = ProjectTrainees::whereFkProjId($project_id)->get();
        $project_courses    = ProjectCourses::whereFkProjectId($project_id)->get();
        $project_requests   = ProjectRequests::whereFkProjectId($project_id)->get();
        $project_benficiaries   = ProjectBeneficiaries::whereFkProjectId($project_id)->get();

        $providers_array = array();
        foreach ($project_providers as $key => $pc_info) {
            $providers_array[] = $pc_info->fk_trainee_id;
        }

        $trainees_array = array();
        foreach ($project_trainees as $key => $pc_info) {
            $trainees_array[] = $pc_info->fk_trainee_id;
        }
        $courses_array = array();
        foreach ($project_courses as $key => $pc_info) {
            $courses_array[] = $pc_info->fk_course_id;
        }
        $requests_array = array();
        foreach ($project_requests as $key => $pr_info) {
            $requests_array[] = $pr_info->fk_request_id;
        }

        $benf_array = array();
        foreach ($project_benficiaries as $key => $benf_info) {
            $benf_array[] = $benf_info->fk_benf_id;
        }

        $project_code  = "";
        $project_status         = ProjectStatus::wherePsIsDeleted(0)->get();
        $project_types          = ProjectTypes::wherePtIsDeleted(0)->get();
        $lst_donors             = Donor::whereDIsDeleted(0)->get();
        $lst_courses            = Courses::whereCIsDeleted(0)->get();
        $lst_beneficiaries      = Beneficiaries::whereSbIsDeleted(0)->orderby('sb_name','ASC')->get();
        $lst_providers          = Users::whereUUserType( UserTypes::PROVIDERS_USER_TYPE_ID )->whereUIsDeleted(0)->whereUIsActive(1)->get();

        $data = array(
            "project_info" => $project_info,
            "lst_providers" => $lst_providers,
            "lst_courses" => $lst_courses,
            "lst_donors" => $lst_donors,
            "project_types" => $project_types,
            "requests_array" => $requests_array,
            "project_status" => $project_status,
            "providers_array" => $providers_array,
            "benf_array" => $benf_array,
            "lst_beneficiaries" => $lst_beneficiaries,
            "courses_array" => $courses_array,
            "trainees_array" => $trainees_array
        );
        return Response()->view('projects.admin.editproject',$data);
    }


    /**
     * Save Project info in the database with all related tables of the project module
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function SaveProjectInfo(Request $request)
    {

        $p_id = $request->input('p_id');
        $action = "add";
        if(isset($p_id) && $p_id != null)
        {
            $p_id                       = decrypt($request->input('p_id'));
            $action = "edit";
        }

        $pp_project_code                = $request->input('pp_project_code');
        $pp_project_title               = $request->input('pp_project_title');
        $sl_project_type                = $request->input('sl_project_type');
        $sl_donor                       = $request->input('sl_donor');
        $pp_project_budget              = $request->input('pp_project_budget');
        $project_status_id              = $request->input('project_status_id');
        $pp_project_start_date          = $request->input('pp_project_start_date');
        $pp_project_end_date            = $request->input('pp_project_end_date');
        $pc_project_courses             = $request->input('pc_project_courses');
        $pp_contract_date               = $request->input('pp_contract_date');
        $pp_project_providers           = $request->input('pp_project_providers');
        $pc_project_benficiaries        = $request->input('pc_project_benficiaries');
        $pp_project_description         = $request->input('pp_project_description');
        $pp_contract_number             = $request->input('pp_contract_number');
        $pp_nbr_of_sessions_per_day     = 0;
        $pp_project_billing_method      = $request->input('pp_project_billing_method');


        $request_courses = RequestsCourses::whereIn('fk_course_id',$pc_project_courses)->get();
        $requests_info_array = array();
        foreach ($request_courses as $index => $rq_course_info) {
            $requests_info_array[] = $rq_course_info->fk_request_id;
        }


        // if we didn't select any benf we take all beneficieries available as allowed for this project
        if(count($pc_project_benficiaries) == 0)
        {
            $list_beneficiaries = Beneficiaries::whereSbIsDeleted(0)->get();
            $pc_project_benficiaries = array();

            foreach ( $list_beneficiaries as $key => $benf_info )
            {
                $pc_project_benficiaries[] = $benf_info->sb_id;
            }



        }

        $lst_requests           = MRequest::whereRIsDeleted(0)->whereFkStatusId(RequestStatus::REQUEST_STATUS_APPROVED_BY_OMSAR)->whereIn('fk_beneficiary_id',$pc_project_benficiaries)->whereIn('r_id' , $requests_info_array )->get();


        $pp_project_requests = array();
        $project_trainees_array = array();
        foreach ($lst_requests as $req_index => $req_info)
        {
            $pp_project_requests[] = $req_info->r_id;
        }



        // get trainees from requests selected
        $requests_info_obj = MRequest::whereIn('r_id',$pp_project_requests)->get();




        foreach ($requests_info_obj as $key => $r_info)
        {
            $project_trainees_array[ $r_info->r_id ]['trainee'] = $r_info->fk_trainee_id;
            $request_course_obj = RequestsCourses::whereFkRequestId($r_info->r_id)->get();
            foreach ( $request_course_obj as $key => $rc_info ) {
                $project_trainees_array[ $r_info->r_id ]['course_id'] = $rc_info->fk_course_id;
            }

        }


        $project_obj = new Projects();
        if(isset($p_id) && $p_id != null)
        {
            $project_obj = Projects::find($p_id);
        }

        $project_obj->pp_project_code = $pp_project_code;
        $project_obj->pp_project_title = $pp_project_title;
        $project_obj->fk_ptype_id = $sl_project_type;
        $project_obj->fk_donor_id = $sl_donor;
        $project_obj->pp_project_budget = $pp_project_budget;
        $project_obj->pp_project_start_date = $pp_project_start_date;
        $project_obj->pp_project_end_date = $pp_project_end_date;
        $project_obj->pp_contract_date = $pp_contract_date;
        $project_obj->pp_project_description = $pp_project_description;
        $project_obj->pp_nbr_of_sessions_per_day = $pp_nbr_of_sessions_per_day;
        $project_obj->pp_project_billing_method = $pp_project_billing_method;
        $project_obj->pp_contract_number = $pp_contract_number;
        if(isset($project_status_id) && $project_status_id != null)
        {
            $project_obj->fk_project_status_id = decrypt($project_status_id);
        }

        $project_obj->save();
        $p_id = $project_obj->pp_id;

        {


            foreach ($project_trainees_array as $request_id => $r_info ) {
                    $projectTrainees = new ProjectTrainees();
                    $projectTrainees->fk_proj_id = $p_id;
                    $projectTrainees->fk_trainee_id = $r_info['trainee'];
                    $projectTrainees->fk_course_id = $r_info['course_id'];
                    $projectTrainees->save();
            }


            // get courses in project
            $projectCourses = ProjectCourses::whereFkProjectId($p_id)->get();
            $courses_pc_ids = array();
            foreach ($projectCourses as $index => $pcInfo) {
                $courses_pc_ids[] = $pcInfo->fk_course_id;
            }




            for ($i = 0; $i < count($pc_project_courses); $i++)
            {
                if(!in_array($pc_project_courses[$i], $courses_pc_ids))
                {
                    $projectCourses = new ProjectCourses();
                    $projectCourses->fk_project_id = $p_id;
                    $projectCourses->fk_course_id = $pc_project_courses[$i];
                    $projectCourses->save();
                }

            }


            // get requests in project
            $projectRequests = ProjectRequests::whereFkProjectId($p_id)->get();
            $requests_pr_ids = array();
            foreach ($projectRequests as $index => $prInfo) {
                $requests_pr_ids[] = $prInfo->fk_request_id;
            }

            for ($i = 0; $i < count($pp_project_requests); $i++)
            {
                if(!in_array($pp_project_requests[$i], $requests_pr_ids))
                {
                    $projectRequests = new ProjectRequests();
                    $projectRequests->fk_project_id   = $p_id;
                    $projectRequests->fk_request_id  = $pp_project_requests[$i];
                    $projectRequests->fk_trainee_id   = $project_trainees_array[ $pp_project_requests[$i] ]['trainee'];
                    $projectRequests->save();
                }

            }




            $projectProvider = ProjectProvider::whereFkProjectId($p_id)->get();
            $providers_pv_ids = array();
            foreach ($projectProvider as $index => $prInfo) {
                $providers_pv_ids[] = $prInfo->fk_request_id;
            }

            // save project providers
            for ($i = 0; $i < count($pp_project_providers); $i++)
            {
                if(!in_array($pp_project_providers[$i], $providers_pv_ids))
                {
                    $project_providers = new ProjectProvider();
                    $project_providers->fk_project_id = $p_id;
                    $project_providers->fk_provider_id = $pp_project_providers[$i];
                    $project_providers->save();
                }

            }


            $projectBenf = ProjectBeneficiaries::whereFkProjectId($p_id)->get();
            $benf_pb_ids = array();
            foreach ($projectBenf as $index => $pbInfo) {
                $benf_pb_ids[] = $pbInfo->fk_benf_id;
            }


            for ($i = 0; $i < count($pc_project_benficiaries); $i++)
            {
                if(!in_array($pc_project_benficiaries[$i], $benf_pb_ids))
                {
                    $projectBenf = new ProjectBeneficiaries();
                    $projectBenf->fk_project_id = $p_id;
                    $projectBenf->fk_benf_id = $pc_project_benficiaries[$i];
                    $projectBenf->save();
                }

            }
            // change status of requests to registred when you add it to projects
            for ($i = 0; $i < count($pp_project_requests); $i++)
            {
                $requestsInfo = MRequest::find($pp_project_requests[$i]);
                $requestsInfo->fk_status_id = RequestStatus::REQUEST_STATUS_REGISTERED;
                $requestsInfo->save();
            }


        }

        $result_array = array();

        $result_array['is_error'] = 0;
        $result_array['project_id'] = $p_id;
        $result_array['error_msg'] = 'Project Saved Successfully';

        return Response()->json($result_array);
    }

    /**
     * open change status project popup where
     * we can change status from dropdown
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $project_ids
     */
    public function OpenChangeProjectStatusPopUp( $project_ids )
    {
        $project_statuses = ProjectStatus::wherePsIsDeleted(0)->get();
        $data = array(
            "project_ids" => $project_ids,
            "project_statuses" => $project_statuses
        );
        return Response()->view('projects.changestatus',$data);
    }


    public function SelectProjectTrainees( $project_id , $pc_id )
    {
        $lst_trainees = Users::whereUUserType(UserTypes::TRAINEES_USER_TYPE_ID)->whereUIsActive(1)->whereUIsDeleted(0)->get();

        $data = array(
            "project_id" => $project_id,
            "pc_id" => $pc_id,
            "lst_trainees" => $lst_trainees
        );
        return Response()->view('projects.selecttraineesCourse',$data);
    }

    /**
     *
     * @param Request $request
     */
    public function ChangeProjectStatus(Request $request)
    {
        $project_ids = $request->input('project_ids');
        $ps_project_status = $request->input('ps_project_status');

        $project_ids_array = explode(",", $project_ids);
        for ($i = 0; $i < count($project_ids_array); $i++)
        {
            $project_info = Projects::find($project_ids_array[$i]);
            $project_info->fk_project_status_id = $ps_project_status;
            $project_info->save();
        }

        $result_array = array();
        $result_array['is_error'] = 0;



        return Response()->json($result_array);
    }

    /**
     * Display Page of Project Management to control Project Information and data related
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function AdminProjectManagement(Request $request)
    {
        
        $project_statuses = ProjectStatus::wherePsIsDeleted(0)->get();
        $lst_benfs   = Beneficiaries::whereSbIsDeleted(0)->get();
        
        $data = array(
            "project_statuses" => $project_statuses,
            "lst_benfs" => $lst_benfs,
        );
        return Response()->view('projects.admin.projects',$data);
    }

    public function DisplayListAdminProjects(Request $request)
    { 
        $page_number                    = $request->input('page_number');
        $prj_project_status             = $request->input('prj_project_status');
        $prj_project_title                = $request->input('prj_project_title');
        $prj_project_beneficiary       = $request->input('prj_project_beneficiary');
        
        $nbr_rows_per_pages          = Config::get('appconfig.max_rows_per_page');

        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;
        $project_obj = Projects::wherePpIsDeleted(0);
        
        if($prj_project_status > 0)
        {
            $project_obj = $project_obj->whereFkProjectStatusId($prj_project_status);
        }
        
        
        if(count($prj_project_beneficiary) > 0)
        {
            // get projects related to selected beneficiary
            $Projectbenf = ProjectBeneficiaries::whereIn("fk_benf_id",$prj_project_beneficiary)->get();
            $project_ids = array();
            foreach ($Projectbenf as $key => $pb_info) 
            {
                $project_ids[] = $pb_info->fk_project_id;
            }
            $project_ids = array_values($project_ids);
            
            if(count($project_ids) > 0)
            {
                $project_obj = $project_obj->whereIn("pp_id" ,$project_ids);
            }
            else 
              {
                  $project_obj = $project_obj->where("pp_id" ,"=",0);
            }
           
        }
        
        if(strlen($prj_project_title) > 0)
        {
            $project_obj = $project_obj->where('pp_project_title' , 'LIKE', '%' . $prj_project_title. '%');
        }

        $nbr_projects = $project_obj->count();

        $total_pages = ceil($nbr_projects/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);

        $total_projects_obj  = $project_obj->skip($skip)->take($nbr_rows_per_pages)->get();


        $project_ids = array();

        for ($i = 0; $i < count($total_projects_obj); $i++) {
            $project_ids[] = $total_projects_obj[$i]->pp_id;
        }

        $nbr_courses = \App\Libraries\Projects\ProjectManagement::GetNbrOfCoursesInProject($project_ids);
        $nbr_classes = ProjectManagement::GetNbrOfClassesInProject($project_ids);
        $nbr_trainees = ProjectManagement::GetNbrOfTraineesInProject($project_ids);
        $lst_projects_statuses = ProjectStatus::wherePsIsDeleted(0)->get();
        $project_statuses = CreateDatabaseArrayByIndex($lst_projects_statuses , 'ps_id');

        $data = array(
            "total_projects_obj" => $total_projects_obj,
            "nbr_courses" => $nbr_courses,
            "nbr_classes" => $nbr_classes,
            "project_statuses" => $project_statuses,
            "nbr_trainees" => $nbr_trainees,
        );

        $result_array = array();

        $result_array['total_pages']    = $total_pages;
        $result_array['display']        = view("projects.admin.results",$data)->render();

        return Response()->json($result_array);
    }

    /**
     * Check the status of the Project if it's not begin and approved
     * Delete Project and all information related to the Project
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function DeleteProject( Request $request )
    {

        $project_ids        = $request->input('project_ids');
        $list_project_ids   = array();
        $result_array       = array();
        $project_deleted        = array();
        $project_not_deleted    = array();
        $list_project_ids = explode(",", $project_ids);
        for ($i = 0; $i < count($list_project_ids); $i++)
        {
            // get project info
            $project_info = Projects::find($list_project_ids[$i]);
            $project_status = $project_info->fk_project_status_id;


            if($project_status != ProjectStatus::PROJECT_STATUS_INPROGRESS || $project_status != ProjectStatus::PROJECT_STATUS_CLOSED  || $project_status != ProjectStatus::PROJECT_STATUS_PAYMENT_PENDING )
            {
                // allow to delete Project with all project info
                ProjectManagement::DeleteProjectAndAllInformation( $list_project_ids[$i] );

            }
            else
            {
                $project_not_deleted[] = $project_info->pp_project_code . "-" . $project_info->pp_project_title;
            }
        }

        if(count($project_not_deleted) > 0)
        {
            $result_array['is_error'] = 1;
            $result_array['error_msg'] = "You Cannot Delete Projects ( " . implode(",", $project_not_deleted) . " ) , please change their status before Try Again";
        }
        else
        {
            $result_array['is_error'] = 0;
        }

        return Response()->json($result_array);
    }


    /**
     * Get List Of Sessions in selected project
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function GetListSessionsCalendar( Request $request )
    {
        $project_id = $request->input('project_id');

        $sessions_array = SessionsManagement::getSessionsDatainformation( $project_id );
        $result_array = array();

        //$result_array['session_calendar_path'] = $sessions_array['session_calendar_path'];
        $result_array['project_id'] = $project_id;

        return Response()->json($result_array);

    }


    /**
     * Open Page Contain The Steps Of Configuration A Project "Providers , classes , scheduale and Sessions"
     *
     * @author Moe Mantach
     * @access public
     *
     */
    public function ProjectConfigurationManagement( $project_id )
    {
        $projectConfiguration = Projects::find($project_id);
       $data = array(
           'project_id' => $project_id,
           'projectConfiguration' => $projectConfiguration,
       );
       return Response()->view('projects.configurationmanager',$data);
    }

    /**
     * Page of configration Management Of Projects in Admin Panel
     *
     * @author Moe Mantach
     * @param Integer $project_id
     * @return \Illuminate\Http\Response
     */
    public function AdminProjectConfiguration( $project_id )
    {
        $projectConfiguration = Projects::find($project_id);

       $data = array(
           'project_id' => $project_id,
           'projectConfiguration' => $projectConfiguration,
       );
       return Response()->view('projects.admin.configurationmanager',$data);
    }


    /**
     * Display Tab contain configurations related to the project created
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function DisplayConfigurationTab(Request $request)
    {
        $project_id = $request->input('project_id');
        $tab_name   = $request->input('tab_name');
        $result_array = array();
        $view_name = "";
        $data = array();

        switch ($tab_name)
        {
            case "trainees_configuration":
                {
                    // check if we don't have selected trainees in this project we get data from selected request
                    // else we can get data from trainees already saved in the project
                    $ProjectRequests = ProjectRequests::whereFkProjectId($project_id)->get();
                    // get list ids of requests
                    $trainee_ids = array();
                    $daysInfo = Days::all();
                    $days_ids = array();
                    foreach ($daysInfo as $day => $info) {
                        $days_ids[ $info->ID ] = $info->Name;
                    }

                    $request_ids = array();

                    foreach ($ProjectRequests as $index => $request_info)
                    {
                        $request_ids[] = $request_info->fk_request_id;
                    }

                    $RequestInfo = New MRequest();
                    // requests Info
                    if(count($request_ids) > 0)
                        $RequestInfo = MRequest::whereIn('r_id',$request_ids)->get();



                    foreach ($RequestInfo as $index => $request_info) {
                        if(is_object($request_info))
                         $trainee_ids[] = $request_info->fk_trainee_id;
                    }

                    // get list of trainees set the selected Requests
                    $SenderRequests = new Users();
                    if(count($trainee_ids) > 0)
                        $SenderRequests = Users::whereIn('id',$trainee_ids)->get();


                    $project_trainees = DB::table('proj_project_trainees')->where('fk_proj_id','=',$project_id)->get();
                   // $project_trainees = ProjectTrainees::whereFkProjId($project_id)->get();
                    $selected_trainees_array = array();
                    foreach ($project_trainees as $index => $trainee_info)
                    {
                        $selected_trainees_array[] = $trainee_info->fk_trainee_id;
                    }

                    // get list of Active and trainees Users to displau ih the box
                    $lst_trainees = Users::whereUUserType(UserTypes::TRAINEES_USER_TYPE_ID)->whereUIsActive(1)->whereUIsDeleted(0)->get();

                    $data['SenderRequests'] = $SenderRequests;
                    $data['trainee_ids'] = $trainee_ids;
                    $data['days_ids'] = $days_ids;
                    $data['selected_trainees_array'] = $selected_trainees_array;
                    $data['lst_trainees'] = $lst_trainees;
                    $view_name = "projects/config/trainees_configuration";

                    $result_array['display'] = view($view_name,$data)->render();

                }
            break;
            case "groups_configuration" :
                {
                    $view_name = "projects/config/groups_configuration";

                    $max_number_of_classes  = Config::get('appconfig.sa_max_nbr_of_classes');
                    $project_trainees_group = ProjectManagement::GetLstAllTraineesInProject($project_id);


                    // get selected trainees
                    $project_trainees = ProjectTrainees::whereFkProjId($project_id)->get();
                    $selected_trainees_array = array();
                    foreach ($project_trainees as $index => $trainee_info)
                    {
                        if(!in_array($trainee_info->fk_trainee_id, $project_trainees_group)) // check if a trainee already in a group don't added to the array
                            $selected_trainees_array[] = $trainee_info->fk_trainee_id;
                    }

                    // get information of selected Trainees
                    $lst_trainees = Users::find($selected_trainees_array);


                    // get project information
                    $projectInfo = Projects::find($project_id);
                    $project_courses = ProjectManagement::getLstProjectCourses($project_id);
                    $data = array(
                        "max_number_of_classes" => $max_number_of_classes,
                        "projectInfo" => $projectInfo,
                        "project_courses" => $project_courses,
                        "lst_trainees" => $lst_trainees
                    );
                    $result_array['display'] = view($view_name,$data)->render();
                }
            break;
            case "courses_configuration" :
                {
                    // get courses saved for this project

                    $projectCourses = ProjectCourses::whereFkProjectId($project_id)->get();
                    $course_ids = array();

                     foreach ($projectCourses as $pc_id => $pc_info)
                     {
                         $course_ids[] = $pc_info->fk_course_id;
                     }

                     $course_info = DB::table('sys_courses')->whereIn('c_id',$course_ids)->get();


                    $view_name = "projects/config/courses_configuration";
                    $data = array();
                    $data['course_info']        = $course_info;
                    $result_array['display'] = view($view_name,$data)->render();
                }
            break;
            case "sessions_configuration" :
                {
                    // get courses saved for this project

                    $projectCourses = ProjectCourses::whereFkProjectId($project_id)->get();
                    $project_classes = ProjectClasses::whereFkProjectId($project_id)->get();

                    $class_ids = array();
                    foreach ( $project_classes as $class_index => $class_info ) {
                        $class_ids[] = $class_info->cls_id;
                    }

                    $course_ids = array();

                     foreach ($projectCourses as $pc_id => $pc_info)
                     {
                         $course_ids[] = $pc_info->fk_course_id;
                     }

                     $course_info = DB::table('sys_courses')->whereIn('c_id',$course_ids)->get();
                     $sessions_info = DB::table('ses_session_class')->whereIn('fk_class_id',$class_ids)->get();


                    $view_name = "projects/config/sessions_configuration";
                    $data = array();
                    $data['course_info']        = $course_info;
                    $data['sessions_info']        = $sessions_info;
                    $result_array['display'] = view($view_name,$data)->render();
                }
            break;
            case "preview_confirmation":
                {
                    $view_name = "projects/config/preview_configuration";
                    $data = array();
                    $result_array['display'] = view($view_name,$data)->render();
                }
            break;
        }

        return Response()->json($result_array);
    }

    private function _SaveProjectTraineesInformation( $request )
    {
        $project_id     = $request->input('project_id');
        $ck_trainees    = $request->input('ck_trainees');

        $res = DB::table('proj_project_trainees')->where('fk_proj_id','=',$project_id)->delete();

        $trainees_array = array();
        if($ck_trainees != null)
        {
            // Save Trainees id in the database and related to the project id
            for ($i = 0; $i < count($ck_trainees); $i++)
            {

                $trainees_array[] = array(
                    'fk_proj_id' => $project_id,
                    'fk_trainee_id' => $ck_trainees[$i]
                );
            }

            DB::table('proj_project_trainees')->insert($trainees_array);
        }



        return true;

    }



    /**
     * Save Project Courses Information to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Array $request
     * @return boolean
     */
    public function SaveProjectCourseInformation( Request $request )
    {
        $project_id                 = $request->input('project_id');
        $pc_id                      = $request->input('pc_id');
        $project_course             = $request->input('project_course');
        $pc_max_nbr_trainees        = $request->input('pc_max_nbr_trainees');
        $pc_course_weight           = $request->input('pc_course_weight');
        $pc_course_cost             = $request->input('pc_course_cost');
        $pc_nbr_sessions            = $request->input('pc_nbr_sessions');
        $sl_train_days              = $request->input('sl_train_days');
        $pc_project_trainees        = $request->input('pc_project_trainees');
        $sl_trainer                 = $request->input('sl_trainer');

        $ProjectCourses = ProjectCourses::find($pc_id);
        $ProjectCourses->pc_max_nbr_trainees    = $pc_max_nbr_trainees;
        $ProjectCourses->pc_course_weight       = $pc_course_weight;
        $ProjectCourses->pc_course_cost         = $pc_course_cost;
        $ProjectCourses->pc_nbr_sessions        = $pc_nbr_sessions;
        $ProjectCourses->save();


        // save trainees in the database to the selected course but before he delete the old saved groups
        DB::table('proj_project_course_trainees')->where('fk_pcourse_id','=',$pc_id)->where('fk_project_id','=',$project_id)->delete();
        $trainees_course_array = array();
        if(count($pc_project_trainees) > 0)
        {
            for ($i = 0; $i < count($pc_project_trainees); $i++)
            {
                $trainees_course_array[] = array(
                    'fk_project_id' => $project_id,
                    'fk_trainee_id' => $pc_project_trainees[$i],
                    'fk_pcourse_id' => $pc_id,
                );
            }
        }


        DB::table('proj_project_course_trainees')->insert($trainees_course_array);


        $result_array = array();
        $result_array['is_error'] = 0;
        $result_array['error_msg'] = 'Project Configuration Has Been Saved';


        return Response()->json($result_array);


    }

     private function _GetNextProjectTab( $project_tab_name )
     {
        switch ($project_tab_name)
        {
            case "trainees_configuration":
                {
                    return "courses_configuration";
                }
             break;
            case "courses_configuration":
                {
                    return "groups_configuration";
                }
             break;
            case "groups_configuration":
                {
                    return "sessions_configuration";
                }
             break;
            case "sessions_configuration":
                {
                    return "preview_confirmation";
                }
             break;
        }
     }

    /**
     * Save Project Information to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function SaveProjectConfiguration( Request $request )
    {
        $tab_name = $request->input('tab_name');

        switch ($tab_name)
        {
            case "trainees_configuration":
            {
                $this->_SaveProjectTraineesInformation( $request );
                $result_array['next_tab'] = $this->_GetNextProjectTab($tab_name);
            }
            break;
            case "courses_configuration":
            {

                $project_id     = $request->input('project_id');
                $project_course = $request->input('project_course');

                /** $params_array = array(
                 'project_id' => $project_id
                );

                $projectManagement = new ProjectManagement();

                $projectManagement->GenerateClassesProject( $params_array );
                $projectManagement->GenerateSessionsCourses( $params_array );*/

                // Generate Groups and Insert all trainees in those Groups
                $params_array = array(
                    'project_id' => $project_id
                );
                $projectManagement = new ProjectManagement();
                $projectManagement->GenerateGroupsProject( $params_array );

                // change request statuses to registered

                $projectRequests = ProjectRequests::whereFkProjectId($project_id)->get();
                $requests_ids = array();

                foreach ($projectRequests as $index => $pr_info)
                {
                    $requests_id = $pr_info->fk_request_id;
                    $Request_Obj = new MRequest();
                    $Request_Obj = MRequest::find($requests_id);
                    $Request_Obj->fk_status_id = RequestStatus::REQUEST_STATUS_REGISTERED;
                    $Request_Obj->save();
                }


                $result_array['next_tab'] = $this->_GetNextProjectTab($tab_name);
            }
            break;
            case "groups_configuration":
            {

                $project_id     = $request->input('project_id');
                $params_array = array(
                    'project_id' => $project_id
                );


                $projectGroups_obj = ProjectGroups::whereFkProjectId($project_id)->get();
                foreach ( $projectGroups_obj as $pg_index => $pg_info )
                {
                    if($pg_info->pg_train_days == null || $pg_info->pg_train_days == 0 || $pg_info->pg_class_trainer == null || $pg_info->pg_class_trainer == 0 || $pg_info->fk_pcourse_id == null || $pg_info->fk_pcourse_id == 0 )
                    {
                        $result_array['is_error'] = 1;
                        $result_array['error_msg'] = 'Please complete Configuration before Generate The Sessions';
                        return Response()->json($result_array);
                    }
                }


                $projectManagement = new ProjectManagement();

                $projectManagement->GenerateClassesProject( $params_array );
                $projectManagement->GenerateSessionsCourses( $params_array );


                $projectRequests = ProjectRequests::whereFkProjectId($project_id)->get();
                $requests_ids = array();

                foreach ($projectRequests as $index => $pr_info)
                {
                    $requests_id = $pr_info->fk_request_id;
                    $Request_Obj = new MRequest();
                    $Request_Obj = MRequest::find($requests_id);
                    $Request_Obj->fk_status_id = RequestStatus::REQUEST_STATUS_SCHEDULED;
                    $Request_Obj->save();
                }

                $result_array['next_tab'] = $this->_GetNextProjectTab($tab_name);
            }
            break;

            case "preview_confirmation":
            {
                 $project_id             = $request->input('project_id');



                /**$approved_by_provider   = ProjectStatus::PROJECT_STATUS_APPROVED_BY_OMSAR;*/

                $projectInfo = Projects::find($project_id);



                // send requests for approval to the OMSAR AND COORDINATOR
                $ProjectNotifications = ProjectNotifications::whereFkProjectId($project_id)->get();


                // check if we already send notification we don't send again
                if(count($ProjectNotifications) == 0)
                {
                    // check the user type of creator and generator configuration of project
                    // if admin the project going directly to inprogress else it's need approval of omsar administrator

                    $user_type      = session('user_type');
                    if($user_type == null)
                    {
                        $user_type   = session('ut_user_type');
                    }


                    if( $user_type == UserTypes::ADMIN_USER_TYPE_ID )
                    {
                        $projectInfo->fk_project_status_id = ProjectStatus::PROJECT_STATUS_INPROGRESS;
                        $projectInfo->save();
                    }
                    else if( $user_type == UserTypes::COORDINATORS_USER_TYPE_ID )
                    {
                        $projectInfo->fk_project_status_id = ProjectStatus::PROJECT_STATUS_APPROVED_BY_COORDINATOR;
                        $projectInfo->save();

                        // send notification to administrator for approval
                        $params_array = array(
                            'project_id' => $project_id
                        );
                        $result_array = ProjectNotificationsManager::SendOmsarNotifications($params_array);


                    }


                    // send notifications to all trainees About the project and scheduler to approve
                    $params_array = array(
                        'project_id' => $project_id
                    );
                    $result_array = ProjectNotificationsManager::SendTraineesNotifications($params_array);



                }


                $result_array['next_tab'] = "";

            }
            break;
        }


        $result_array['is_error'] = 0;
        $result_array['error_msg'] = 'Project Configuration Has Been Saved';



        return Response()->json($result_array);
    }


    public function SaveAdminProjectConfiguration( Request $request )
    {
        $tab_name = $request->input('tab_name');
        $result_array = array();

        switch ($tab_name)
        {
            case "trainees_configuration":
            {
                $this->_SaveProjectTraineesInformation( $request );
                $result_array['next_tab'] = $this->_GetNextProjectTab($tab_name);
            }
            break;
            case "sessions_configuration":
            {
                // save

                $result_array['next_tab'] = $this->_GetNextProjectTab($tab_name);
            }
            break;
            case "groups_configuration":
            {
                $project_id     = $request->input('project_id');
                $params_array = array(
                    'project_id' => $project_id
                );

                // check groups configuration before begin generate classes and session
                $projectGroups_obj = ProjectGroups::whereFkProjectId($project_id)->get();
                foreach ( $projectGroups_obj as $pg_index => $pg_info )
                {
                   if($pg_info->pg_train_days == null || $pg_info->pg_train_days == 0 || $pg_info->pg_class_trainer == null || $pg_info->pg_class_trainer == 0 || $pg_info->fk_pcourse_id == null || $pg_info->fk_pcourse_id == 0 )
                   {
                       $result_array['is_error'] = 1;
                       $result_array['error_msg'] = 'Please complete Configuration before Generate The Sessions';
                       return Response()->json($result_array);
                   }
                }


                $projectManagement = new ProjectManagement();

                $projectManagement->GenerateClassesProject( $params_array );
                $projectManagement->GenerateSessionsCourses( $params_array );

                $result_array['next_tab'] = $this->_GetNextProjectTab($tab_name);
            }
            break;
            case "courses_configuration":
            {

                $project_id     = $request->input('project_id');
                $project_course = $request->input('project_course');

                $params_array = array(
                    'project_id' => $project_id
                );

                $projectManagement = new ProjectManagement();

                $projectManagement->GenerateGroupsProject( $params_array );
                $result_array['next_tab'] = $this->_GetNextProjectTab($tab_name);

            }
            break;
            case "preview_confirmation":
            {

                 $project_id             = $request->input('project_id');



                /**$approved_by_provider   = ProjectStatus::PROJECT_STATUS_APPROVED_BY_OMSAR;*/

                $projectInfo = Projects::find($project_id);



                // send requests for approval to the OMSAR AND COORDINATOR
                $ProjectNotifications = ProjectNotifications::whereFkProjectId($project_id)->get();


                // check if we already send notification we don't send again
                if(count($ProjectNotifications) == 0)
                {
                    // check the user type of creator and generator configuration of project
                    // if admin the project going directly to inprogress else it's need approval of omsar administrator

                    $user_type      = session('user_type');
                    if($user_type == null)
                    {
                        $user_type   = session('ut_user_type');
                    }


                    if( $user_type == UserTypes::ADMIN_USER_TYPE_ID )
                    {
                        $projectInfo->fk_project_status_id = ProjectStatus::PROJECT_STATUS_INPROGRESS;
                        $projectInfo->save();
                    }
                    else if( $user_type == UserTypes::COORDINATORS_USER_TYPE_ID )
                    {
                        $projectInfo->fk_project_status_id = ProjectStatus::PROJECT_STATUS_APPROVED_BY_COORDINATOR;
                        $projectInfo->save();

                        // send notification to administrator for approval
                        $params_array = array(
                            'project_id' => $project_id
                        );
                        $result_array = ProjectNotificationsManager::SendOmsarNotifications($params_array);
                        echo "data:".$params_array;


                    }


                    // send notifications to all trainees About the project and scheduler to approve
                    $params_array = array(
                        'project_id' => $project_id
                    );
                    $result_array = ProjectNotificationsManager::SendTraineesNotifications($params_array);
                    echo "data:".$params_array;



                }


                $result_array['next_tab'] = "";

            }
            break;
        }


        $result_array['is_error'] = 0;
        $result_array['error_msg'] = 'Project Configuration Has Been Saved';



        return Response()->json($result_array);
    }

    /**
     * Display Page Contain all Project Information
     *
     * @author Moe Mantach
     * @access public
     * @param Integer $project_id
     */
    public function ViewProjectPage( $project_id )
    {
        $project_info       = Projects::find($project_id);
        $project_providers  = ProjectProvider::whereFkProjectId($project_id)->get();
        $project_trainees   = ProjectTrainees::whereFkProjId($project_id)->get();
        $project_courses    = ProjectCourses::whereFkProjectId($project_id)->get();
        $project_requests   = ProjectRequests::whereFkProjectId($project_id)->get();
        $beneficiaries      = Beneficiaries::whereSbIsDeleted(0)->get();
        $benf_info          = CreateDatabaseArrayByIndex($beneficiaries , 'sb_id');
        $category_array     = CourseCategories::whereCcIsDeleted(0)->get();
        $category_array     = CreateDatabaseArrayByIndex($category_array , 'cc_id');

        $providers_array = array();
        foreach ($project_providers as $key => $pc_info) {
            $providers_array[] = $pc_info->fk_trainee_id;
        }

        $trainees_array = array();
        foreach ($project_trainees as $key => $pc_info) {
            $trainees_array[] = $pc_info->fk_trainee_id;
        }
        $courses_array = array();
        foreach ($project_courses as $key => $pc_info) {
            $courses_array[] = $pc_info->fk_course_id;
        }
        $requests_array = array();
        foreach ($project_requests as $key => $pr_info) {
            $requests_array[] = $pr_info->fk_request_id;
        }

        $project_code  = "";
        $project_status         = ProjectStatus::wherePsIsDeleted(0)->get();
        $lst_requests           = MRequest::whereRIsDeleted(0)->whereFkStatusId(RequestStatus::REQUEST_STATUS_APPROVED_BY_OMSAR)->get();
        $project_types          = ProjectTypes::wherePtIsDeleted(0)->get();
        $project_types          = CreateDatabaseArrayByIndex($project_types , 'pt_id');
        $lst_donors             = Donor::whereDIsDeleted(0)->get();
        $lst_donors             = CreateDatabaseArrayByIndex($lst_donors , 'd_id');
        $lst_courses            = Courses::whereCIsDeleted(0)->get();
        $lst_courses            = CreateDatabaseArrayByIndex($lst_courses , 'c_id');
        $lst_providers          = Users::whereUUserType( UserTypes::PROVIDERS_USER_TYPE_ID )->whereUIsDeleted(0)->whereUIsActive(1)->get();
        $lst_providers          = CreateDatabaseArrayByIndex($lst_providers , 'id');

        $lst_trainees          = Users::whereUUserType( UserTypes::TRAINEES_USER_TYPE_ID )->whereUIsDeleted(0)->whereUIsActive(1)->get();
        $lst_trainees          = CreateDatabaseArrayByIndex($lst_trainees , 'id');


        $data = array(
            "project_info"    => $project_info,
            "lst_providers"   => $lst_providers,
            "lst_trainees"    => $lst_trainees,
            "lst_courses"     => $lst_courses,
            "lst_donors"      => $lst_donors,
            "project_types"   => $project_types,
            "lst_requests"    => $lst_requests,
            "requests_array"  => $requests_array,
            "project_status"  => $project_status,
            "providers_array" => $providers_array,
            "courses_array"   => $courses_array,
            "trainees_array"  => $trainees_array,
            "benf_info"       => $benf_info,
            "category_array"  => $category_array
        );


        return Response()->view('projects.admin.view',$data);
    }

    /**
     * Display Form contain all information related to Certain course in certain project
     *
     * @author Moe Mantac
     * @access public
     * @param Request $request
     *
     */
    public function DisplayProjectCourseInformation( Request $request )
    {
        $project_course             = $request->input('project_course');
        $project_id                 = $request->input('project_id');
        $result_array               = array();
        $trainees_array             = array();
        $course_groups_array        = array();

        $lst_benfs                  = Beneficiaries::whereSbIsDeleted(0)->get();
        $benfs_array                = CreateDatabaseArrayByIndex($lst_benfs , 'sb_id');
        $project_course_statuses    = ProjectCourseStatus::whereUcsIsDeleted(0)->get();
        $project_groups             = ProjectGroups::whereFkProjectId($project_id)->get();




        if($project_course > 0)
        {
            $project_course_info    = ProjectCourses::whereFkProjectId($project_id)->whereFkCourseId($project_course)->get();
            $course_id              = $project_course_info[0]->fk_course_id;
            $pc_id                  = $project_course_info[0]->pc_id;


            $project_course_groups  = ProjectCoursesGroups::whereFkProjectId($project_id)->whereFkCourseId($pc_id)->get();
            foreach ($project_course_groups as $index => $info)
            {
                $course_groups_array[] = $info->fk_group_id;
            }





            // get trainees of this course
            $projectTraineeCourse = ProjectTrainees::whereFkProjId($project_id)->whereFkCourseId($course_id)->get();


            foreach ($projectTraineeCourse as $key => $ProjectTraineeInfo)
            {
                $trainees_array[] = $ProjectTraineeInfo->fk_trainee_id;
            }


            // get list of Active and trainees Users to displau ih the box
            $lst_trainees = DB::table('users')->whereIn('id',$trainees_array)->get();




            $data = array(
                "project_course_info" => $project_course_info,
                "project_course_statuses" => $project_course_statuses,
                "lst_trainees" => $lst_trainees,
                "benfs_array" => $benfs_array,
                "project_groups" => $project_groups,
                "course_groups_array" => $course_groups_array,
            );
            $result_array['display'] = view('projects.displayprojectcourses' , $data)->render();
            $result_array['course_code'] = $pc_id;
            return Response()->json($result_array);
        }
        else
        {
            $project_course_info = ProjectCourses::whereFkProjectId($project_id)->get();


            // get trainees of this course
            $projectTraineeCourse = ProjectTrainees::whereFkProjId($project_id)->get();


            foreach ($projectTraineeCourse as $key => $ProjectTraineeInfo)
            {
                $trainees_array[] = $ProjectTraineeInfo->fk_trainee_id;
            }

            $lst_trainees = DB::table('users')->whereIn('id',$trainees_array)->get();

            $data = array(
                "lst_trainees" => $lst_trainees,
                "benfs_array" => $benfs_array
            );
            $result_array['display']    = view('projects.traineescourse' , $data)->render();
            $result_array['course_code'] = 0;
            return Response()->json($result_array);
        }

    }

    /**
     * Delete Group with the trainees related
     *
     * @author Moe Mantach
     * @access
     * @param Request $request
     */
    public function DeleteGroupInfo( Request $request )
    {
        $pg_id       = $request->input('pg_id');
        $group_array = explode(',', $pg_id);

        for ($i = 0; $i < count($group_array); $i++)
        {
            $id = $group_array[$i];

            // delete trainees in group
            DB::table('proj_projects_group_trainees')->where('fk_group_id',$id)->delete();
            // delete group in table
            DB::table('proj_projects_groups')->where('pg_id',$id)->delete();
        }


        $result_array = array();

        $result_array['is_error']   = 0;
        $result_array['error_msg']  = 'Group Deleted Successfully';
        return Response()->json($result_array);
    }

}
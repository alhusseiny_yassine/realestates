<?php
/***********************************************************
TraineesCourseStatusController.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 1, 2017
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Http\Controllers\Projects;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\model\Project\ProjectCourseStatus;


class TraineesCourseStatusController extends Controller
{
    
    public function TraineesCourseStatus()
    {
        $data = array();
        return Response()->view('tcoursestatus.traineesCourseStatus',$data);
    }


    public function DisplayListStauses(Request $request)
    {
        $page_number        = $request->input('page_number');
        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;



        $all_course_statuses = ProjectCourseStatus::whereUcsIsDeleted(0)->count();


        $total_pages = ceil($all_course_statuses/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);

        $course_statuses = ProjectCourseStatus::whereUcsIsDeleted(0)->skip($skip)->take($nbr_rows_per_pages)->get();
        $data = array(
            "course_statuses" => $course_statuses
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("tcoursestatus.displaylist",$data)->render();

        return Response()->json($result_array);
    }


    public function AddNewTraineesCourseStatus()
    {

        $data = array();
        return view('tcoursestatus.addcoursestatus',$data);
    }


    /**
     * Save Project Type information to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function SaveCourseStatusInfo(Request $request)
    {
        $ucs_id                             = $request->input('ucs_id');
        $ucs_status_name                    = $request->input('ucs_status_name');
        $ucs_status_color                   = $request->input('ucs_status_color');
        $ucs_percentage_attendance          = $request->input('ucs_percentage_attendance');

        $result_array = array();

        $ProjectCourseStatus = new ProjectCourseStatus();
        if($ucs_id !== null)
        {
            $ProjectCourseStatus = ProjectCourseStatus::find($ucs_id);
        }

        $ProjectCourseStatus->ucs_status_name                   = $ucs_status_name;
        $ProjectCourseStatus->ucs_status_color                  = $ucs_status_color;
        $ProjectCourseStatus->ucs_percentage_attendance         = $ucs_percentage_attendance;
        $ProjectCourseStatus->save();


        $result_array['is_error']  = 0;
        $result_array['error_msg'] = 'Project Course Status Information Has been saved';

        return Response()->json($result_array);
    }



    /**
     * Display Edit Trainees Course Status Form Page
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $bt_id
     */
    public function EditTraineesCourseStatusForm( $ucs_id )
    {

        $trainees_course_status    = ProjectCourseStatus::find( $ucs_id );


        $data = array(
            "trainees_course_status" => $trainees_course_status
        );
        return view('tcoursestatus.editcoursestatus',$data);
    }

    
    
    /**
     * Delete Trainees Course Status by change value of flag ucs_is_deleted from 0 to 1
     * and Save the user that do this operation
     * 
     * @author Moe Mantach
     * @access public
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function DeleteTraineesCourseStatusInfo(Request $request)
    {
        $ucs_id = $request->input('ucs_id');

        $ProjectCourseStatus = new ProjectCourseStatus();
        $ProjectCourseStatus = ProjectCourseStatus::find($ucs_id);
        $ProjectCourseStatus->ucs_is_deleted          = 1;
        $ProjectCourseStatus->ucs_deleted_by          = Session('user_id');
        $ProjectCourseStatus->save();


        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return Response()->json($result_array);
    }

}
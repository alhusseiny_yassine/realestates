<?php
/***********************************************************
ProjectNotificationsController.php
Product :
Version : 1.0
Release : 2
Date Created : Apr 17, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/



namespace App\Http\Controllers\Projects;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\System\Roles;
use App\model\Project\ProjectStatus;
use App\model\Project\Projects;
use App\model\Requests\Request As MRequest;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\model\Requests\RequestsCourses;
use App\model\Project\ProjectTypes;
use App\model\System\Donor;
use App\model\System\Courses;
use App\model\Users\Users;
use App\model\Users\UserTypes;
use App\model\Requests\RequestStatus;
use App\model\Project\ProjectProvider;
use App\model\Project\ProjectCourses;
use App\model\Project\ProjectClasses;
use App\Libraries\Projects\ProjectManagement;
use App\Libraries\Sessions\SessionsManagement;
use App\model\Project\ProjectTrainees;
use App\model\Project\ProjectRequests;
use App\model\Sessions\SessionClass;
use App\Libraries\Users\UserManager;
use App\model\Project\ProjectGroups;
use App\model\Project\ProjectCoursesGroups;
use App\model\System\Beneficiaries;
use App\model\Project\ProjectBeneficiaries;
use App\model\Project\ProjectCourseStatus;
use App\model\Project\ProjectNotifications;
use App\Libraries\Notifications\ProjectNotificationsManager;
use App\Libraries\Notifications\NotifcationsManager;


class ProjectNotificationsController extends Controller
{

    /**
     * Accept request trainee to going to class
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function AcceptProjectRequest(Request $request)
    {
        $nr_id = $request->input('nr_id');

        $ProjectRequestNot = new ProjectNotifications();
        $ProjectRequestNot = ProjectNotifications::find($nr_id);
        $ProjectRequestNot->nr_notification_status = 1;
        $ProjectRequestNot->nr_notificaiton_approved_by = Session('ut_user_id');
        $ProjectRequestNot->nr_notificaiton_approved_date = date('Y-m-d H:i:s');
        $ProjectRequestNot->save();

        // Send Notifications to Administrator and coordinator that this user approve the request and schedule
        {
            // coordinators
            $trainee_id              = Session('ut_user_id');
            $trainee_info            = Users::find($trainee_id);
            $benf_id                  = $trainee_info->fk_beneficiary_id;
            $ut_coordinator_id     = Session('ut_coordinator_id');
            $project_id               = $ProjectRequestNot['attributes']['fk_project_id'];
            $request_id               = $ProjectRequestNot['attributes']['fk_request_id'];
            
            $cordinator_info           = Users::find($ut_coordinator_id);
            $ut_user_fullname        =  Session('ut_user_fullname');
            $ut_user_email            =  Session('ut_user_email');
            $benf_info                  = Beneficiaries::find($benf_id);
            $project_info              = Projects::find($project_id);
            $user_id                    = $ut_coordinator_id;
            
            $params_array = array(
                "notification_type" => NotifcationsManager::NOT_APPROVE_SCHEDULE,
                "coordinator_name"  => $cordinator_info['attributes']['u_fullname'],
                "project_name"  => $project_info['attributes']['pp_project_title'],
                "fk_project_id"  => $project_id,
                "full_name"         => $ut_user_fullname,
                "user_name"         => $ut_user_email,
                "fk_request_id"     => $request_id,
                "beneficiary"       => $benf_info->sb_name,
                "coordinator_id"    => $ut_coordinator_id,
                "user_id"        => $user_id
            );
            NotifcationsManager::SendNotification($params_array);
            
            // send request to Admins
            $lst_admin_users = Users::whereUUserType(UserTypes::ADMIN_USER_TYPE_ID)->get();
            
            foreach ($lst_admin_users as $index => $u_info ) 
            {
                $user_id = 	$u_info->id;
                
                $params_array = array(
                    "notification_type" => NotifcationsManager::NOT_APPROVE_SCHEDULE,
                    "coordinator_name"  => $cordinator_info['attributes']['u_fullname'],
                    "project_name"  => $project_info['attributes']['pp_project_title'],
                    "fk_project_id"  => $project_id,
                    "full_name"         => $ut_user_fullname,
                    "user_name"         => $ut_user_email,
                    "fk_request_id"     => $request_id,
                    "beneficiary"       => $benf_info->sb_name,
                    "coordinator_id"    => $ut_coordinator_id,
                    "user_id"        => $user_id
                );
                NotifcationsManager::SendNotification($params_array);
            }
            
        }
        
        
        $result_array = array();
        $result_array['is_error'] = 0;
        $result_array['error_msg'] = "The Request Has Been Approved";
        
        return Response()->json($result_array);
    }


    /**
     * Reject the request for training to going to class
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function RejectProjectRequest(Request $request)
    {
        $nr_id = $request->input('nr_id');

        $ProjectRequestNot = new ProjectNotifications();
        $ProjectRequestNot = ProjectNotifications::find($nr_id);
        $ProjectRequestNot->nr_notification_status = 2;
        $ProjectRequestNot->nr_notificaiton_approved_by = 0;
        $ProjectRequestNot->nr_notificaiton_approved_date = date('Y-m-d H:i:s');
        $ProjectRequestNot->save();
        
        // Send Notifications to Administrator and coordinator that this user approve the request and schedule
        {
            // coordinators
            $trainee_id              = Session('ut_user_id');
            $trainee_info            = Users::find($trainee_id);
            $benf_id                  = $trainee_info->fk_beneficiary_id;
            $ut_coordinator_id     = Session('ut_coordinator_id');
            $project_id               = $ProjectRequestNot['attributes']['fk_project_id'];
            $request_id               = $ProjectRequestNot['attributes']['fk_request_id'];
            
            $cordinator_info           = Users::find($ut_coordinator_id);
            $ut_user_fullname        =  Session('ut_user_fullname');
            $ut_user_email            =  Session('ut_user_email');
            $benf_info                  = Beneficiaries::find($benf_id);
            $project_info              = Projects::find($project_id);
            $user_id                    = $ut_coordinator_id;
            
            $params_array = array(
                "notification_type" => NotifcationsManager::NOT_APPROVE_SCHEDULE,
                "coordinator_name"  => $cordinator_info['attributes']['u_fullname'],
                "project_name"  => $project_info['attributes']['pp_project_title'],
                "fk_project_id"  => $project_id,
                "full_name"         => $ut_user_fullname,
                "user_name"         => $ut_user_email,
                "fk_request_id"     => $request_id,
                "beneficiary"       => $benf_info->sb_name,
                "coordinator_id"    => $ut_coordinator_id,
                "user_id"        => $user_id
            );
            NotifcationsManager::SendNotification($params_array);
            
            // send request to Admins
            $lst_admin_users = Users::whereUUserType(UserTypes::ADMIN_USER_TYPE_ID)->get();
            
            foreach ($lst_admin_users as $index => $u_info )
            {
                $user_id = 	$u_info->id;
                
                $params_array = array(
                    "notification_type" => NotifcationsManager::NOT_APPROVE_SCHEDULE,
                    "coordinator_name"  => $cordinator_info['attributes']['u_fullname'],
                    "project_name"  => $project_info['attributes']['pp_project_title'],
                    "fk_project_id"  => $project_id,
                    "full_name"         => $ut_user_fullname,
                    "user_name"         => $ut_user_email,
                    "fk_request_id"     => $request_id,
                    "beneficiary"       => $benf_info->sb_name,
                    "coordinator_id"    => $ut_coordinator_id,
                    "user_id"        => $user_id
                );
                NotifcationsManager::SendNotification($params_array);
            }
            
        }
        
        
        
        $result_array = array();
        $result_array['is_error'] = 0;
        $result_array['error_msg'] = "The Request Has Been Approved";


        return Response()->json($result_array);
    }
    
    
    public function PostPoneNotificationRequest(Request $request)
    {
        $nr_id = $request->input('nr_id');

        $ProjectRequestNot = new ProjectNotifications();
        $ProjectRequestNot = ProjectNotifications::find($nr_id);
        $ProjectRequestNot->nr_notification_status = 3;
        $ProjectRequestNot->nr_notificaiton_approved_by = 0;
        $ProjectRequestNot->nr_notificaiton_approved_date = date('Y-m-d H:i:s');
        $ProjectRequestNot->save();
        
        // Send Notifications to Administrator and coordinator that this user approve the request and schedule
        {
            // coordinators
            $trainee_id              = Session('ut_user_id');
            $trainee_info            = Users::find($trainee_id);
            $benf_id                  = $trainee_info->fk_beneficiary_id;
            $ut_coordinator_id     = Session('ut_coordinator_id');
            $project_id               = $ProjectRequestNot['attributes']['fk_project_id'];
            $request_id               = $ProjectRequestNot['attributes']['fk_request_id'];
            
            $cordinator_info           = Users::find($ut_coordinator_id);
            $ut_user_fullname        =  Session('ut_user_fullname');
            $ut_user_email            =  Session('ut_user_email');
            $benf_info                  = Beneficiaries::find($benf_id);
            $project_info              = Projects::find($project_id);
            $user_id                    = $ut_coordinator_id;
            
            $params_array = array(
                "notification_type" => NotifcationsManager::NOT_APPROVE_SCHEDULE,
                "coordinator_name"  => $cordinator_info['attributes']['u_fullname'],
                "project_name"  => $project_info['attributes']['pp_project_title'],
                "fk_project_id"  => $project_id,
                "full_name"         => $ut_user_fullname,
                "user_name"         => $ut_user_email,
                "fk_request_id"     => $request_id,
                "beneficiary"       => $benf_info->sb_name,
                "coordinator_id"    => $ut_coordinator_id,
                "user_id"        => $user_id
            );
            NotifcationsManager::SendNotification($params_array);
            
            // send request to Admins
            $lst_admin_users = Users::whereUUserType(UserTypes::ADMIN_USER_TYPE_ID)->get();
            
            foreach ($lst_admin_users as $index => $u_info )
            {
                $user_id = 	$u_info->id;
                
                $params_array = array(
                    "notification_type" => NotifcationsManager::NOT_APPROVE_SCHEDULE,
                    "coordinator_name"  => $cordinator_info['attributes']['u_fullname'],
                    "project_name"  => $project_info['attributes']['pp_project_title'],
                    "fk_project_id"  => $project_id,
                    "full_name"         => $ut_user_fullname,
                    "user_name"         => $ut_user_email,
                    "fk_request_id"     => $request_id,
                    "beneficiary"       => $benf_info->sb_name,
                    "coordinator_id"    => $ut_coordinator_id,
                    "user_id"        => $user_id
                );
                NotifcationsManager::SendNotification($params_array);
            }
            
        }
        
        
        $result_array = array();
        $result_array['is_error'] = 0;
        $result_array['error_msg'] = "The Request Has Been Approved";


        return Response()->json($result_array);
    }
}
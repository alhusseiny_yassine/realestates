<?php
/***********************************************************
ProjectCoursesController.php
Product :
Version : 1.0
Release : 2
Date Created : Mar 31, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/




namespace App\Http\Controllers\Projects;


use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\System\Roles;
use App\model\Project\ProjectStatus;
use App\model\Project\Projects;
use App\model\Requests\Request As MRequest;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\model\Requests\RequestsCourses;
use App\model\Project\ProjectTypes;
use App\model\System\Donor;
use App\model\System\Courses;
use App\model\Users\Users;
use App\model\Users\UserTypes;
use App\model\Requests\RequestStatus;
use App\model\Project\ProjectProvider;
use App\model\Project\ProjectCourses;
use App\model\Project\ProjectClasses;
use App\Libraries\Projects\ProjectManagement;
use App\Libraries\Sessions\SessionsManagement;
use App\model\Project\ProjectTrainees;
use App\model\Project\ProjectRequests;
use App\model\Sessions\SessionClass;
use App\Libraries\Users\UserManager;
use App\model\Project\ProjectGroups;
use App\model\Project\ProjectCoursesGroups;
use App\model\System\Beneficiaries;
use App\model\Project\ProjectBeneficiaries;
use App\model\System\CourseType;
use App\model\Project\ProjectCourseTrainees;
use App\Libraries\Courses\CourseManagement;


class ProjectCoursesController extends Controller
{

    public function RunningCoursesManagement()
    {
        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_course_management_tab'])  && $role_info['om_course_management_tab'] == 'deny') || ( !isset( $role_info['om_course_management_tab'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }

        $course_types = CourseType::whereCtIsDeleted(0)->get();
        //$course_categories_data  = CourseCategories::whereCcIsDeleted(0)->get();

        $data = array(
            "course_types" => $course_types
        );
        return Response()->view('course.runningcourse',$data);
    }


    /**
     * Display List of courses assign to a projects
     *
     * @author Moe Mantach
     * @access public
     *
     *
     */
    public function CourseSearchResults(Request $request)
    {
        $search_query            = $request->input('search_query');
        $c_course_category       = $request->input('c_course_category');
        $page_number             = $request->input('page_number');
        $nbr_rows_per_pages      = Config::get('appconfig.max_rows_per_page');
        $ut_beneficiary_id       = session('ut_beneficiary_id');
        $projects_benf_array = array();

        $projects_benf_obj = ProjectBeneficiaries::whereFkBenfId($ut_beneficiary_id)->get();


        foreach ( $projects_benf_obj as $key => $pb_info )
        {
            $projects_benf_array[] = $pb_info->fk_project_id;
        }


        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;
        $current_date = date('Y-m-d');
        //->where('pp_project_end_date','<',$current_date)
        // get projects status in progress and current date between start and end date
        $project_courses_obj = Projects::wherePpIsDeleted(0)->whereIn('pp_id',$projects_benf_array)->where('pp_project_start_date','<=',$current_date)->where('pp_project_end_date','>',$current_date)->whereFkProjectStatusId(ProjectStatus::PROJECT_STATUS_INPROGRESS)->get();

        $project_ids = array();

        foreach ( $project_courses_obj as $key => $project_info )
        {
            $project_ids[] = $project_info->pp_id;
        }

        // get courses for all projects


        $pcourses_obj = ProjectCourses::whereIn('fk_project_id',$project_ids)->get();

        $cours_ids = array();

        foreach ( $pcourses_obj as $key => $course_info )
        {
            $cours_ids[] = $course_info->fk_course_id;
        }
        $cours_ids = array_unique($cours_ids);


        $all_courses = Courses::whereCIsDeleted(0);
        if(strlen($search_query) > 0)
        {
           $all_courses = $all_courses->where('c_course_name' , 'like' , '%' . $search_query . '%')->orWhere('c_code' , 'like' , '%' . $search_query . '%');
        }

        if($c_course_category > 0)
        {
            $all_courses = $all_courses->where('fk_category_id' , '=' ,$c_course_category);
        }
        $all_courses = $all_courses->whereIn('c_id',$cours_ids);

        $all_courses =  $all_courses->count();


        $total_pages = ceil($all_courses/$nbr_rows_per_pages);

        $total_pages = intval($total_pages);

        $courses = Courses::whereCIsDeleted(0);
        if(strlen($search_query) > 0)
        {
            $courses = $courses->where('c_course_name' , 'like' , '%' . $search_query . '%')->orWhere('c_code' , 'like' , '%' . $search_query . '%');
        }

        if($c_course_category > 0)
        {
            $courses = $courses->where('fk_category_id' , '=' ,$c_course_category);
        }

        $courses = $courses->whereIn('c_id',$cours_ids);
        $courses =  $courses->skip($skip)->take($nbr_rows_per_pages)->get();


        $lstCourseCategories = CourseManagement::getCourseCategoriesArrayById();
        $data = array(
            "courses" => $courses,
            "lstCourseCategories" => $lstCourseCategories
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("course.rsearch_results",$data)->render();

        return Response()->json($result_array);
    }

}
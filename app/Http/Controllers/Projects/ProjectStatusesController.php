<?php
/***********************************************************
ProjectStatusesController.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 28, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Http\Controllers\Projects;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\System\Roles;
use App\model\Project\ProjectStatus;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;


class ProjectStatusesController extends Controller
{
    public function ProjectStatusesManagement()
    {
        $data = array();
        return Response()->view('projectstatuses.projectstatus',$data);
    }


    public function DisplayListProjectStatuses(Request $request)
    {
        $page_number        = $request->input('page_number');
        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;



        $all_project_statuses = ProjectStatus::wherePsIsDeleted(0)->count();


        $total_pages = ceil($all_project_statuses/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);

        $project_statuses = ProjectStatus::wherePsIsDeleted(0)->skip($skip)->take($nbr_rows_per_pages)->get();
        $data = array(
            "project_statuses" => $project_statuses
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("projectstatuses.displaylist",$data)->render();

        return Response()->json($result_array);
    }


    public function AddProjectStatusForm()
    {
        $lst_project_statuses = ProjectStatus::wherePsIsDeleted(0)->get();
        
        $data = array(
            "lst_project_statuses" => $lst_project_statuses
        );
        return view('projectstatuses.addprojectstatus',$data);
    }


    /**
     * Save Project Status information to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function SaveProjectStatusInfo(Request $request)
    {
        $ps_id                          = $request->input('ps_id');
        $ps_status_name                 = $request->input('ps_status_name');
        $ps_status_color                = $request->input('ps_status_color');
        $ps_status_depandancy           = $request->input('ps_status_depandancy');

        $result_array = array();

        $ProjectStatus = new ProjectStatus();
        if($ps_id !== null)
        {
            $ProjectStatus = ProjectStatus::find($ps_id);
        }

        $ProjectStatus->ps_status_name      = $ps_status_name;
        $ProjectStatus->ps_status_color     = $ps_status_color;
        $ProjectStatus->ps_status_depandancy     = $ps_status_depandancy;
        $ProjectStatus->save();


        $result_array['is_error']  = 0;
        $result_array['error_msg'] = 'Project Status Information Has been saved';

        return Response()->json($result_array);
    }



    /**
     * Display Edit Project Status Form Page
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $bt_id
     */
    public function EditProjectStatusForm( $ps_id )
    {

        $cur_project_status_info    = ProjectStatus::find( $ps_id );
        
        $lst_project_statuses = ProjectStatus::wherePsIsDeleted(0)->where('ps_id','!=',$ps_id)->get();
        $data = array(
            "cur_project_status_info" => $cur_project_status_info,
            "lst_project_statuses" => $lst_project_statuses
        );
        return view('projectstatuses.editprojectstatus',$data);
    }


    public function DeleteProjectStatusInfo(Request $request)
    {
        $ps_id = $request->input('ps_id');

        $ProjectStatus = new ProjectStatus();
        $ProjectStatus = ProjectStatus::find($ps_id);
        $ProjectStatus->ps_is_deleted          = 1;
        $ProjectStatus->ps_deleted_by          = Session('user_id');
        $ProjectStatus->save();


        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return Response()->json($result_array);
    }

}
<?php
/***********************************************************
GroupsController.php
Product :
Version : 1.0
Release : 2
Date Created : Jan 29, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Http\Controllers\Projects;


use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\System\Roles;
use App\model\Project\ProjectStatus;
use App\model\Project\Projects;
use App\model\Requests\Request As MRequest;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\model\Requests\RequestsCourses;
use App\model\Project\ProjectTypes;
use App\model\System\Donor;
use App\model\System\Courses;
use App\model\Users\Users;
use App\model\Users\UserTypes;
use App\model\Requests\RequestStatus;
use App\model\Project\ProjectProvider;
use App\model\Project\ProjectCourses;
use App\model\Project\ProjectClasses;
use App\Libraries\Projects\ProjectManagement;
use App\Libraries\Sessions\SessionsManagement;
use App\model\Project\ProjectTrainees;
use App\model\Project\ProjectRequests;
use App\model\Sessions\SessionClass;
use App\Libraries\Users\UserManager;
use App\model\Project\ProjectGroups;
use App\model\Project\ProjectCoursesGroups;
use App\model\System\Beneficiaries;
use App\model\Project\ProjectBeneficiaries;
use App\model\System\CourseCategories;
use App\model\Project\ProjectCourseTrainees;


class GroupsController extends Controller
{

     /**
      * Display all groups depand on type selected
      * by list of by grids
      *
      * @author Moe Mantach
      * @access public
      * @param Request $request
      */
     public function DisplayListGroups( Request $request )
     {
         $project_id = $request->input('project_id');
         $display_type = $request->input('display_type');
         $project_course = $request->input('project_course');

         if($project_course == 0)
         {
             $projectGroups = ProjectGroups::whereFkProjectId($project_id)->get();
         }
         else
         {
             $projectGroups = ProjectGroups::whereFkProjectId($project_id)->whereFkPcourseId($project_course)->get();
         }

         $result_array = array();
         switch ($display_type)
         {
             case "list":
                 {
                     $sys_days = Days::all();
                     $sys_days = CreateDatabaseArrayByIndex($sys_days , 'ID');


                     $data = array(
                         "project_group_info" => $projectGroups,
                         "sys_days" => $sys_days,
                     );
                     $result_array['display'] = view("projects/lst_groups",$data)->render();
                 }
             break;
             case "grids":
                 {

                     $max_number_of_classes  = Config::get('appconfig.sa_max_nbr_of_classes');

                     $project_trainees_group = ProjectManagement::GetLstAllTraineesInProject($project_id);
                     // get selected trainees
                     $project_trainees = ProjectTrainees::whereFkProjId($project_id)->get();
                     $selected_trainees_array = array();
                     foreach ($project_trainees as $index => $trainee_info)
                     {
                         if(!in_array($trainee_info->fk_trainee_id, $project_trainees_group)) // check if a trainee already in a group don't added to the array
                             $selected_trainees_array[] = $trainee_info->fk_trainee_id;
                     }

                     // get information of selected Trainees
                     $lst_trainees = Users::find($selected_trainees_array);

                     $project_group_info = ProjectGroups::whereFkProjectId($project_id)->get();

                     $lst_trainees_group = ProjectManagement::GetLstTraineesInProjectGroup($project_id);
                     // get project information
                     $projectInfo = Projects::find($project_id);

                     $data = array(
                         "max_number_of_classes" => $max_number_of_classes,
                         "projectInfo" => $projectInfo,
                         "lst_trainees" => $lst_trainees,
                         "lst_trainees_group" => $lst_trainees_group,
                         "project_group_info" => $projectGroups
                     );

                     $result_array['display'] = view("projects/groups_grid",$data)->render();
                 }
             break;

         }

         return Response()->json($result_array);
     }

     public function DisplayGroupInfo(  $pg_id )
     {
         $group_info = ProjectGroups::find($pg_id);
         $pc_id = $group_info->fk_pcourse_id;

         $ProjectCourses = ProjectCourses::find($pc_id);

         $course_id = $ProjectCourses->fk_course_id;

         $courseInfo = Courses::find($course_id);

         // get list and orde them by array
         $train_days = Days::all();
         $train_days_array =  CreateDatabaseArrayByIndex($train_days , 'ID' );
         $course_categories = CourseCategories::whereCcIsDeleted(0)->get();
         $course_categories_array = CreateDatabaseArrayByIndex($course_categories , 'cc_id' );

         $data = array(
             "group_info" => $group_info,
             "courseInfo" => $courseInfo,
             "course_categories_array" => $course_categories_array,
             "train_days_array" => $train_days_array
         );
         return response()->view('projects.viewgroup',$data);
     }


     /**
      * Display Page where we can change trainees in certain group and add other from list of all trainees
      *
      * @author Moe Mantach
      * @access public
      * @param unknown $pg_id
      */
     public function EditTraineesGroup( $pg_id )
     {
         $ProjectGroups = ProjectGroups::find($pg_id);
         $project_id = $ProjectGroups->fk_project_id;
         
         // get selected trainees
         $project_trainees = DB::table('proj_projects_group_trainees')->where('fk_group_id','=',$pg_id)->get();
         $selected_trainees_array = array();
         foreach ($project_trainees as $index => $trainee_info)
         {
             $selected_trainees_array[] = $trainee_info->fk_trainee_id;
         }

         // get trainees in selected courses
         $project_course_id = $ProjectGroups->fk_pcourse_id;

         $project_course_info = ProjectCourses::find($project_course_id);
         $course_id = $project_course_info->fk_course_id;

         
         $projectRequests = ProjectRequests::whereFkProjectId($project_id)->get();

         $requests_courses_array = array();

         foreach ( $projectRequests as $index => $rp_info ) {
             $requests_courses_array[] = $rp_info->fk_request_id;
         }


        $RequestCourse = RequestsCourses::whereFkCourseId($course_id)->whereIn('fk_request_id',$requests_courses_array)->get();
        $requests_array = array();
        foreach ($RequestCourse as $key => $r_info) {
            $requests_array[] = $r_info->fk_request_id ;
        }


         $RequestsInfo = MRequest::whereIn('r_id',$requests_array)->get();




         $trainee_ids = array();

         foreach ($RequestsInfo as $index => $r_info)
         {
             $trainee_ids[] = $r_info->fk_trainee_id;
         }

         $beneficiaries          = Beneficiaries::whereSbIsDeleted(0)->get();
         $benf_info              = CreateDatabaseArrayByIndex($beneficiaries , 'sb_id');
         $lst_trainees = Users::whereIn('id',$trainee_ids)->get();
         $data = array(
             "project_id" => $project_id,
             "pg_id" => $pg_id,
             "selected_trainees_array" => $selected_trainees_array,
             "benf_info" => $benf_info,
             "lst_trainees" => $lst_trainees
         );
         return Response()->view('projects.group_trainees',$data);
     }

     /**
      * Save Group Trainees in the database
      * @param Request $request
      * @return \Illuminate\Http\JsonResponse
      */
     public function SaveGroupTrainees( Request $request )
     {
         $pg_id         = $request->input('pg_id');
         $project_id    = $request->input('project_id');
         $pc_project_trainees    = $request->input('pc_project_trainees');


         DB::table('proj_projects_group_trainees')->where('fk_group_id','=',$pg_id)->where('fk_project_id','=',$project_id)->delete();
         $trainees_group_array = array();
         if(count($pc_project_trainees) > 0)
         {
             for ($i = 0; $i < count($pc_project_trainees); $i++)
             {
                 $trainees_group_array[] = array(
                     'fk_project_id' => $project_id,
                     'fk_trainee_id' => $pc_project_trainees[$i],
                     'fk_group_id' => $pg_id,
                 );
             }
          }
          DB::table('proj_projects_group_trainees')->insert($trainees_group_array);

          $response_array = array();

          $response_array['is_error'] = 0;
          $response_array['error_msg'] = "Group Trainees Saved Successfully";
          return Response()->json($response_array);
     }
     ///// Provider Front End Groups Management
     /**
      * 
      * @return type
      */
     public function GroupsManagement(){
        
        $projects = Projects::wherePpIsDeleted(0)->get();
        $courses  = Courses::whereCIsDeleted(0)->whereCIsActive(1)->get();
        $data     = array(
            "projects" => $projects,
            "courses"  => $courses
        );
        
        return Response()->view('projects.groups',$data);
        
    }
    
    public function DisplayProjectCourses(Request $request){
        
        $project_id = $request->input("project_id");
        $project_courses = ProjectManagement::getLstProjectCourses($project_id);
        $data = array(
            "project_courses" => $project_courses
        );
        $result_array['display'] = view("projects.pcoursesdropdown", $data)->render();
        return Response()->json($result_array);
    }
    /**
     * 
     * @param Request $request
     * @return type
     */
    public function DisplayGroups(Request $request){
        
        {// initialisation block
            $project_id         = $request->input('project_id');
            $pcourse_id         = $request->input('course_id');
            $page_number        = $request->input('page_number');
            $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        }

        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;

        $groups_obj = ProjectGroups::wherePgIsDeleted(0);

        if($project_id != '')
        {
            $groups_obj = $groups_obj->whereFkProjectId($project_id);
        }

        if($pcourse_id != '')
        {
            $groups_obj = $groups_obj->whereFkPcourseId($pcourse_id);
        }


        $all_groups = $groups_obj->count();




        $total_pages = ceil($all_groups/$nbr_rows_per_pages);


         if($total_pages > 1)
                $groups = $groups_obj->skip($skip)->take($nbr_rows_per_pages)->get();
            else
                $groups = $groups_obj->get();



        $data = array(
            "groups" => $groups
        );

        $result_array = array();
        $result_array['project_id']  = $project_id;
        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("projects.displaygroups",$data)->render();

        return Response()->json($result_array);
        
    }
    /**
     * 
     */
    public function DisplayAddGroup(){
        
    }

}
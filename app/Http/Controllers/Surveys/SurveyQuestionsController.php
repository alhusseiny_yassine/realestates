<?php

namespace App\Http\Controllers\Surveys;


use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Helper;
use Auth;
use DB;
use App\model\System\Provinces;
use App\model\System\Districts;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use App\model\Surveys\Surveys;
use App\model\Surveys\SurveyQuestions;
use App\model\Surveys\SurveyAnswers;
use App\model\Users\Users;
use App\model\Users\UserTypes;




class SurveyQuestionsController extends Controller
{
    /**
     * 
     * @param type $survey_id
     * @return Object View 
     * @description List questions page
     */
    public function ViewQuestions($survey_id){
        $data = array(
            "survey_id" => $survey_id
        );
        return view('surveys.questions',$data);
    }
    /**
     * 
     * @param Request $request
     * @return object view
     * @description return list of questions for a survey
     */
    public function AjaxViewQuestions(Request $request){
        
        $page_number = $request->input('page_number');
        $survey_id   = $request->input('survey_id');
        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        $questions_obj      = SurveyQuestions::whereFkSurveyId($survey_id)->whereSqIsDeleted(0);
        
        if ($page_number > 1) {
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages;
        } else {
            $skip = 0;
        }
        
        $all_questions = $questions_obj->count();

        $total_pages = ceil($all_questions / $nbr_rows_per_pages);

        if ($total_pages > 1) {
            $questions = $questions_obj->skip($skip)->take($nbr_rows_per_pages)->get();
        } else {
            $questions = $questions_obj->get();
        }
        
        $data = array(
            "survey_id"   => $survey_id,
            "questions"   => $questions
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("surveys.listquestions", $data)->render();

        return Response()->json($result_array);
    }
    /**
     * 
     * @param type $survey_id
     * @return object view
     * @description ADD question page
     */
    public function AddQuestion($survey_id){
        
        $questions_count = SurveyQuestions::whereSqIsDeleted(0)->whereFkSurveyId($survey_id)->count();
        
        $data = array(
            "questions_count" => $questions_count,
            "survey_id"       => $survey_id
        );
         return Response()->view("surveys.addquestion",$data);
    }
    /**
     * 
     * @param Request $request
     * @return json response
     * @description check if question already exists in this survey
     */
    public function CheckQuestion(Request $request){
        
        $survey_id   = $request->input('survey_id');
        $question    = $request->input('question');
        $question_id = $request->input('sq_id');
        $response    = array();
        
        $count = SurveyQuestions::whereSqIsDeleted(0)
                ->whereSqQuestion($question)
                ->whereFkSurveyId($survey_id)
                ->where("sq_id","!=",$question_id)
                ->count();
        
        $response['is_error'] = 0;
        
        if($count != 0 ){
            $response['is_error'] = 1;
            $response['error_message'] = 'Question already exists for this survey';
        }
        return Response()->json($response);
    }
    /**
     * 
     * @param Request $request
     * @return json response
     */
    public function SaveQuestion(Request $request){
        
        $fk_survey_id      = $request->input('survey_id');
        $sq_id             = $request->input('sq_id');
        $sq_question       = $request->input('sq_question');
        $sq_rating         = $request->input('sq_rating');
        $sq_order          = $request->input('sq_order');;
        
        $question = new SurveyQuestions();
        if($sq_id != ''){
            $question = SurveyQuestions::find($sq_id);
        }
        
        $question->fk_survey_id  = $fk_survey_id;
        $question->sq_question   = $sq_question;
        $question->sq_rating     = $sq_rating;
        $question->sq_order      = $sq_order;
        $question->save();
        
        $response = array();
        $response['is_error']=0;
        
        return Response()->json($response);
    }
    /**
     * 
     * @param type $sq_id
     * @return response
     */
    public function EditQuestion($sq_id){
        
        $question = SurveyQuestions::find($sq_id);
        $questions_count = SurveyQuestions::whereSqIsDeleted(0)->whereFkSurveyId($question->fk_survey_id)->count();
        
        $data = array(
            "question"        => $question,
            "questions_count" => $questions_count
        );
         return Response()->view("surveys.editquestion",$data);
    }
    /**
     * 
     * @param Request $request
     * @return json
     * @description delete question
     */
    public function DeleteQuestion(Request $request){
        
        $sq_id = $request->input('sq_id');
        SurveyQuestions::whereSqId($sq_id)->delete();
        
        $response = array();
        $response['is_error'] = 0;
        
        return Response()->json($response);
    }
    
    public function ViewAnswers($sq_id){
        
        $data = array(
            "sq_id" => $sq_id
        );
        
        return Response()->view('surveys.answers',$data);
    }
    
    public function AjaxViewAnswers(Request $request){
        
        $page_number = $request->input('page_number');
        $sq_id       = $request->input('sq_id');
        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        $answers_obj      = SurveyAnswers::whereFkQuestionId($sq_id)->whereSaIsDeleted(0);
        $trainees         = Users::whereUIsDeleted(0)->whereUUserType(UserTypes::TRAINEES_USER_TYPE_ID)->get();
        $trainees         = CreateDatabaseArrayByIndex($trainees, 'id' );
        
        if ($page_number > 1) {
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages;
        } else {
            $skip = 0;
        }
        
        $all_answers = $answers_obj->count();

        $total_pages = ceil($all_answers / $nbr_rows_per_pages);

        if ($total_pages > 1) {
            $answers = $answers_obj->skip($skip)->take($nbr_rows_per_pages)->get();
        } else {
            $answers = $answers_obj->get();
        }
        
        $data = array(
            "answers"   => $answers,
            "trainees"  => $trainees
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("surveys.listanswers", $data)->render();

        return Response()->json($result_array);
    }
}
?>
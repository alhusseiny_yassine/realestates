<?php
/***********************************************************
SurveysController.php
Product :
Version : 1.0
Date Created : Aug 9, 2017
Developed By  : Alhusseiny Yassine PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/



namespace App\Http\Controllers\Surveys;


use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Helper;
use Auth;
use DB;
use App\model\Users\Users;
use App\model\Users\UserTypes;
use App\model\System\Beneficiaries;
use App\model\System\Provinces;
use App\model\System\Districts;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use App\model\Project\Projects;
use App\model\System\Courses;
use App\model\Surveys\Surveys;
use App\model\Project\ProjectBeneficiaries;
use App\model\Project\ProjectCourses;
use App\model\Surveys\SurveyQuestions;
use App\model\Surveys\SurveyAnswers;



class SurveysController extends Controller
{
    
    
    /**
     * Display Survey Management Page
     * 
     * @author Moe Mantach
     * @access public
     * @return unknown
     */
    public function DisplaySurveys()
    {
        
        return Response()->view('surveys.surveys');
    }
    
    
    /**
     * 
     * @param Request $request
     * @return Object View
     * @description display list of surveys
     */
    public function AjaxDisplaySurveys(Request $request){
     {
            $page_number                = $request->input('page_number');
            $nbr_rows_per_pages       = Config::get('appconfig.max_rows_per_page');
            $surveys_obj                  = Surveys::whereSIsDeleted(0);
            $lst_beneficaries              = Beneficiaries::whereSbIsDeleted(0)->get();
            $lst_beneficaries               = CreateDatabaseArrayByIndex($lst_beneficaries, 'sb_id');
            $lst_courses                    = Courses::whereCIsDeleted(0)->get();
            $lst_courses                    = CreateDatabaseArrayByIndex($lst_courses, 'c_id' );
            $lst_trainers                   = Users::whereUIsDeleted(0)->whereUUserType(UserTypes::TRAINERS_USER_TYPE_ID)->get();
            $lst_trainers                   = CreateDatabaseArrayByIndex($lst_trainers, 'id');
            $lst_projects                   = Projects::wherePpIsDeleted(0)->get();
            $lst_projects                   = CreateDatabaseArrayByIndex($lst_projects, 'pp_id' );
        }

        if ($page_number > 1) {
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages;
        } else {
            $skip = 0;
        }
        
        $all_surveys = $surveys_obj->count();

        $total_pages = ceil($all_surveys / $nbr_rows_per_pages);

        if ($total_pages > 1) {
            $surveys = $surveys_obj->skip($skip)->take($nbr_rows_per_pages)->get();
        } else {
            $surveys = $surveys_obj->get();
        }
        
        $data = array(
            "surveys"           => $surveys,
            "lst_cources"       =>$lst_courses,
            "lst_beneficiaries" => $lst_beneficaries,
            "lst_trainers"      => $lst_trainers,
            "lst_projects"      => $lst_projects
        );

        

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("surveys.listsurveys", $data)->render();

        return Response()->json($result_array);
    }
    
    public function AddSurvey(){
        
        $projects      = Projects::wherePpIsDeleted(0)->get();
        
        $data = array(
            "projects"      => $projects
                );
        
        return Response()->view("surveys.addsurvey", $data);
        
    }
    
    public function AddObjectDropdown(Request $request){
        
        $id   = $request->input("id");
        $type = $request->input("type");
        $response = array();
        
        switch ($type){
            case 'projects':
            {
                    $project_courses = ProjectCourses::wherePcIsDeleted(0)->whereFkProjectId($id)->get();

                    $courses_array = array();
                    foreach ($project_courses as $key => $pc_info) {
                        $courses_array[] = $pc_info->fk_course_id;
                    }
                    $lst_courses = Courses::whereCIsActive(1)->whereCIsDeleted(0)->get();

                    $data = array(
                        'courses_array' => $courses_array,
                        'lst_courses'   => $lst_courses,
                        'project_id'    => $id
                    );
                    
                    $response['is_error'] = 0;
                    $response['display']  = view("surveys.dropdowns.courses", $data)->render();
                    
                    return Response()->json($response);
            }
            break;
            case 'courses':
            {
                    $project_id = $request->input('project_id');
                    $project_course = ProjectCourses::wherePcIsDeleted(0)->whereFkProjectId($project_id)->whereFkCourseId($id)->get();
                    $trainer = Users::find($project_course[0]->fk_trainer_id);

                    $data = array(
                        'trainer' => $trainer
                    );

                    $response['is_error'] = 0;
                    $response['display'] = view("surveys.dropdowns.trainer", $data)->render();

                    return Response()->json($response);
            }
                break;
        }
    }
    
    public function SaveSurvey(Request $request){
        
        $fk_project_id     = $request->input('fk_project_id');
        $fk_course_id      = $request->input('fk_course_id');
        $fk_trainer_id     = $request->input('fk_trainer_id'); 
        $survey_id         = $request->input('survey_id');
        
        $survey = new Surveys();
        if($survey_id != ''){
            $survey = Surveys::find($survey_id);
        }
        
        $survey->fk_project_id     = $fk_project_id;
        $survey->fk_course_id      = $fk_course_id;
        $survey->s_date            = date("Y-m-d");
        
        if ($fk_trainer_id != null) {
            $survey->fk_trainer_id = $fk_trainer_id;
        } else {
            $survey->fk_trainer_id = 22;
        }
        $survey->save();
        
        $response = array();
        $response['is_error']=0;
        
        return Response()->json($response);
    }
    /**
     * 
     * @param type $survey_id
     * @return type
     */
    public function EditSurvey($survey_id){
        
        $survey_obj        = Surveys::find($survey_id);
        $lst_projects      = Projects::wherePpIsDeleted(0)->get();
        $lst_projects      = CreateDatabaseArrayByIndex($lst_projects,'pp_id');
        $lst_courses       = Courses::whereCIsDeleted(0)->get();
        $lst_courses       = CreateDatabaseArrayByIndex($lst_courses,'c_id');
        $lst_trainers      = Users::whereUIsDeleted(0)->whereUUserType(UserTypes::TRAINERS_USER_TYPE_ID)->get();
        $lst_trainers      = CreateDatabaseArrayByIndex($lst_trainers,'id');
        
        $data = array(
            "survey"            => $survey_obj,
            "lst_projects"      => $lst_projects,
            "lst_courses"       => $lst_courses,
            "lst_trainers"      => $lst_trainers,
            "survey_id"         => $survey_id
            
        );
        return Response()->view("surveys.editsurvey", $data);
    }
    /**
     * 
     * @param Request $request
     * @return type
     */
    public function DeleteSurvey(Request $request){
        
        $survey_id = $request->input('survey_id');
        
        Surveys::whereSId($survey_id)->delete();
        SurveyQuestions::whereFkSurveyId($survey_id)->delete();
        SurveyAnswers::whereFkSurveyId($survey_id)->delete();
        
        $result_array['is_error'] = 0;
        $result_array['error_msg'] = "Order deleted successfully!";

        return Response()->json($result_array);
        
    }
    /**
     * 
     * @param type $survey_id
     * @return Object view
     * @description Display page of lists of trainees who answered the survey 
     */
    public function ViewListSurveyAnswers( $survey_id ){
        
        $data = array(
            "survey_id" => $survey_id
        );
        
        return Response()->view('surveys.displaysurveyanswers',$data);
    }
    /**
     * 
     * @param Request $request
     * @description Display list of trainees who submitted the survey
     */
    public function ListSurveyAnswers( Request $request ){
        
        $survey_id          = $request->input('survey_id');
        $page_number        = $request->input('page_number');
        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');       
        $lst_trainees       = Users::whereUUserType( UserTypes::TRAINEES_USER_TYPE_ID )->get();
        $survey_answers_obj = SurveyAnswers::whereFkSurveyId($survey_id)->whereSaIsDeleted(0);
        $trainees_array     = array();
        
        if ($page_number > 1) {
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages;
        } else {
            $skip = 0;
        }
        
        $all_survey_answers = $survey_answers_obj->count();

        $total_pages = ceil($all_survey_answers / $nbr_rows_per_pages);

        if ($total_pages > 1) {
            $survey_answers = $survey_answers_obj->skip($skip)->take($nbr_rows_per_pages)->get();
        } else {
            $survey_answers = $survey_answers_obj->get();
        }
        
        foreach($survey_answers as $index => $survey_answer){
            $trainees_array[] = $survey_answer->fk_trainee_id;
        }
        
        $data = array(
            "survey_answers" => $survey_answers,
            "lst_trainees"   => $lst_trainees,
            "trainees_array" => $trainees_array,
            "survey_id"      => $survey_id
        );
        
        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("surveys.listsurveyanswers", $data)->render();

        return Response()->json($result_array);
    }
    /**
     * 
     * @param Request $request
     * @return json
     * @description View each question of the survey with the answer of the selected trainee
     */
    public function ViewQuestionsAnswers(Request $request){
        
        $fk_trainee_id = $request->input('trainee_id');
        $fk_survey_id  = $request->input('survey_id');
        
        $survey_questions = SurveyQuestions::whereFkSurveyId($fk_survey_id)->whereSqIsDeleted(0)->get();
        $survey_answers   = SurveyAnswers::whereFkSurveyId($fk_survey_id)->whereFkTraineeId($fk_trainee_id)->whereSaIsDeleted(0)->get();
        $survey_answers   = CreateDatabaseArrayByIndex($survey_answers,'fk_question_id');
        
        $data = array(
            "survey_questions" => $survey_questions,
            "survey_answers"   => $survey_answers,
        );
        $result_array = array();
        $result_array['display'] = view("surveys.viewquestionsanswers", $data)->render();

        return Response()->json($result_array);
    }
    
}
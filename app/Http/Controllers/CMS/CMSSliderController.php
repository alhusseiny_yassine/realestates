<?php
/***********************************************************
CMSSliderController.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 15, 2017
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/



namespace App\Http\Controllers\CMS;

use App\User;
use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use DB;
use App\model\CMS\MCMSSlider;
use App\model\CMS\CMSMedia;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\View\View;     



class CMSSliderController extends Controller
{
    
    
    public function SliderManagement()
    {
        return view('sliders.slidermanagement');
    }
    /**
     * Dispay list of sliders define in the database
     *
     * @author Moe Mantach
     * @param Request $request
     *
     * @return
     */
    public function displayListSliders( Request $request )
    { 
        $lst_sliders = MCMSSlider::getListSliders();

        $data = array(
            "lst_sliders" => $lst_sliders
        );

        return Response()->view('sliders.displaylist',$data);
    }


    /**
     * display form of AddSlide form
     * @return Ambigous <\Illuminate\View\View, mixed, \Illuminate\Foundation\Application, \Illuminate\Container\static>
     */
    public function AddSlideForm( )
    {

        $data = array();
        return view("sliders.addSliderForm",$data);
    }


     public function EditSliderForm( $sId )
    {
        $slide_info = MCMSSlider::getSlideInfo($sId);
        $data = array(
            "slide_info" => $slide_info
        );
        return view("sliders.editSliderForm",$data);
    }


    public function ajaxAddNewSider(Request $request)
    {
        $cs_slider_title            = $request->input('cs_slider_title');
        $cs_slider_creation_date    = date('Y-m-d H:i:s');
        $cs_slider_descrition       = $request->input('cs_slider_descrition');


        $result_array = array();
         $fields_array = array(
            'cs_slider_title' => $cs_slider_title,
            'cs_slider_creation_date' => $cs_slider_creation_date,
            'cs_slider_descrition' => $cs_slider_descrition
        );

        $id = MCMSSlider::saveInfo($fields_array);

        $result_array['is_error'] = 0;
        $result_array['error_msg'] = "Operation Complete successfully";

      return Response()->json($result_array);
    }

      public function ajaxEditSliderInfo(Request $request)
      {
            $cs_id                      = $request->input('cs_id');
            $cs_slider_title            = $request->input('cs_slider_title');
            $cs_slider_descrition       = $request->input('cs_slider_descrition');
            $fields_array = array(
                  'cs_id' => $cs_id,
                  'cs_slider_title' => $cs_slider_title,
                  'cs_slider_descrition' => $cs_slider_descrition
            );
          MCMSSlider::saveInfo($fields_array);


          $result_array               = array();
          $result_array['is_error']   = 0;
          $result_array['error_msg']  = "Operation Complete Successfully";

          return response()->json($result_array);
      }

      /**
      * Delete Slider with all information
      *
      * @author Moe Mantach
      * @access public
      */
      public function ajaxDeleteSliderInfo(Request $request)
      {
          $cs_id = $request->input('cs_id');

          MCMSSlider::deleteRow($cs_id);


          $result_array               = array();
          $result_array['is_error']   = 0;
          $result_array['error_msg']  = "Operation Complete Successfully";

          return response()->json($result_array);
       }



       /**
        * Display List of images in slideshow
        *
        * @author Moe Mantach
        * @access public
        * @param Integer $sId
        */
       public function displayListImages( $sId )
       {
            $listImagesSlider = CMSMedia::getImagesSlider( $sId );

            $data = array(
                "listImagesSlider" => $listImagesSlider,
                "slide_id" => $sId
            );
            return response()->view('sliders.listimagesSlider', $data);
       }
       
       
       
       public function DisplayListImagesSlider( Request $request )
       {
           $cms_slide_id = $request->input('cms_slide_id');
           $page_number = $request->input('page_number');
           $number_pages = $request->input('number_pages');
            $listImagesSlider = CMSMedia::getImagesSlider( $cms_slide_id );
      
            $data = array(
                "lstCmsMedia" => $listImagesSlider,
                "slide_id" => $cms_slide_id,
                "page_number" => $page_number,
                "number_pages" => $number_pages
            );
            return response()->view('sliders.displayListSliderImages', $data);
       }


       public function UploadImagesSlider($sId )
       {
           $data = array(
               "slide_id" => $sId
           );
           return response()->view('sliders.imagesSliderUploader', $data);
       }


       public function EditImageSLiderInformation( $cm_id )
       {
           $cms_media_display  = DB::table('cms_media')->where('cm_id', $cm_id)->get();


           $link =url('/') . "/assets/imgs/noimage.gif";
           $directory = public_path(). "/" . Config::get( 'constants.ALBUMS_PATH') . $cms_media_display[0]->cm_media_base_dir . $cms_media_display[0]->cm_media_file_name . "." . $cms_media_display[0]->cm_media_file_extention ;

           if(is_file($directory))
           {
               $link =url('/') . "/" . Config::get( 'constants.ALBUMS_PATH') . $cms_media_display[0]->cm_media_base_dir . $cms_media_display[0]->cm_media_file_name  . "." . $cms_media_display[0]->cm_media_file_extention ;
           }


           $data = array(
               "cms_media_display" => $cms_media_display,
               "cm_id" => $cm_id,
               "link" => $link
           );


           return view("sliders.EditImageSlider",$data);
       }
       
       
       public function DeleteMedia( $cm_id )
       {
           $webmedia = CMSMedia::find($cm_id);
           $webmedia->cm_is_deleted   = 1;
           $webmedia->save();
       }
 
}
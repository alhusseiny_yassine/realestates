<?php
/***********************************************************
EducationDegreeController.php
Product :
Version : 1.0
Release : 2
Date Created : Mar 26, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Http\Controllers\System;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\Users\Users;
use App\model\Users\UserTypes;
use App\model\System\Roles;
use App\model\System\Country;
use App\model\System\Beneficiaries;
use App\model\System\BeneficiaryType;
use App\model\System\Provinces;
use App\model\System\Districts;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\model\System\EducationDegree;


class EducationDegreeController extends Controller
{

    public function EducationDegreeManagement()
    {
        $data = array();
        return Response()->view('edegree.edegree',$data);
    }


    public function DisplayListEducationDegree(Request $request)
    {

       /** $role_info = session()->get('role_info');
        if( ( isset($role_info['om_beneficiary_management'])  && $role_info['om_beneficiary_management'] == 'deny') || ( !isset( $role_info['om_beneficiary_management'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }*/


        $page_number                    = $request->input('page_number');





        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;

        $educationdegree_obj = EducationDegree::whereEdIsDeleted(0);




        $all_educationdegrees = $educationdegree_obj->count();


        $total_pages = ceil($all_educationdegrees/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);

        $sys_educationdegree = $educationdegree_obj->skip($skip)->take($nbr_rows_per_pages)->get();
        $data = array(
            "educationdegree" => $sys_educationdegree,
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("edegree.displaylist",$data)->render();

        return Response()->json($result_array);
    }




    public function AddEducationDegreeForm()
    {

        $data = array();
        return view('edegree.addedegree',$data);
    }


    /**
     * Save Education Degree Info to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     *
     * @return Response Json
     */
    public function SaveEducationDegreeInfo(Request $request)
    {
        $ed_id                      = $request->input('ed_id');
        $ed_degree_name             = $request->input('ed_degree_name');

        $result_array = array();

        $EducationDegree = new EducationDegree();

        if(is_numeric($ed_id))
        {
            $EducationDegree = EducationDegree::find($ed_id);
        }

        $EducationDegree->ed_degree_name          = $ed_degree_name;
        $EducationDegree->save();


        $result_array['is_error']  = 0;
        $result_array['error_msg'] = 'Education Degree Information Has been saved';

        return Response()->json($result_array);
    }



    /**
     * Display Edit Education Degree Form Page
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $user_id
     */
    public function EditEducationDegreeForm( $ed_id )
    {

        /**$role_info = session()->get('role_info');
        if( ( isset($role_info['om_edit_existing_beneficiary'])  && $role_info['om_edit_existing_beneficiary'] == 'deny') || ( !isset( $role_info['om_edit_existing_beneficiary'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }*/

        $educationdegree_info    = EducationDegree::find($ed_id);


        $data = array(
            "educationdegree_info" => $educationdegree_info
        );
        return view('edegree.editedegree',$data);
    }


    /**
     * Delete Education Degree inforation from the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function DeleteEducationDegreeInfo(Request $request)
    {

        $ed_id = $request->input('ed_id');

        $EducationDegree = new EducationDegree();
        $EducationDegree = EducationDegree::find($ed_id);
        $EducationDegree->ed_is_deleted          = 1;
        $EducationDegree->d_deleted_by          = Session('user_id');
        $EducationDegree->save();


        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return Response()->json($result_array);
    }

}
<?php
/***********************************************************
ProviderLocationController.php
Product :
Version : 1.0
Release : 2
Date Created : Mar 29, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Http\Controllers\System;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\Users\Users;
use App\model\Users\UserTypes;
use App\model\System\Roles;
use App\model\System\Country;
use App\model\System\Beneficiaries;
use App\model\System\BeneficiaryType;
use App\model\System\Provinces;
use App\model\System\Districts;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\model\System\ProviderLocation;


class ProviderLocationController extends Controller
{

    public function ProviderLocationManagement()
    {
        $data = array();
        return Response()->view('plocations.plocations',$data);
    }


    public function DisplayListProviderLocation(Request $request)
    {

        $providerlocation_obj = ProviderLocation::wherePlIsDeleted(0);



        $all_providerlocations = $providerlocation_obj->count();



        $sys_providerlocations = ProviderLocation::wherePlIsDeleted(0)->get();

        $data = array(
            "providerlocations" => $sys_providerlocations,
        );

        $result_array = array();

        $result_array['display'] = view("users.plocations",$data)->render();

        return Response()->json($result_array);
    }




    public function AddProviderLocationForm( $user_id )
    {

        $data = array(
           "user_id" => $user_id
        );
        return view('users.addplocation',$data);
    }

    public function AddAProviderLocationForm( $user_id )
    {

        $data = array(
           "user_id" => $user_id
        );
        return view('administrator.addplocation',$data);
    }


    /**
     * Save Provider Location Info to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     *
     * @return Response Json
     */
    public function SaveProviderLocationInfo(Request $request)
    {
        $pl_id                              = $request->input('pl_id');
        $fk_user_id                         = $request->input('user_id');
        $pl_provider_location               = $request->input('pl_provider_location');

        $result_array = array();

        $ProviderLocation = new ProviderLocation();

        if(is_numeric($pl_id))
        {
            $ProviderLocation = ProviderLocation::find($pl_id);
        }

        $ProviderLocation->fk_user_id               = $fk_user_id;
        $ProviderLocation->pl_provider_location     = $pl_provider_location;
        $ProviderLocation->save();


        $result_array['is_error']  = 0;
        $result_array['error_msg'] = 'Provider Location Information Has been saved';

        return Response()->json($result_array);
    }



    /**
     * Display Edit Provider Location  Form Page
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $pl_id
     */
    public function EditProviderLocationForm( $pl_id )
    {

        /**$role_info = session()->get('role_info');
         if( ( isset($role_info['om_edit_existing_beneficiary'])  && $role_info['om_edit_existing_beneficiary'] == 'deny') || ( !isset( $role_info['om_edit_existing_beneficiary'] ) ) )
         {
         return Redirect::to('administrator/PermissionDenied');
         }*/

        $providerlocation_info    = ProviderLocation::find($pl_id);


        $data = array(
            "providerlocation_info" => $providerlocation_info
        );
        return view('users.editplocation',$data);
    }


    public function EditAProviderLocationForm( $pl_id )
    {

        /**$role_info = session()->get('role_info');
         if( ( isset($role_info['om_edit_existing_beneficiary'])  && $role_info['om_edit_existing_beneficiary'] == 'deny') || ( !isset( $role_info['om_edit_existing_beneficiary'] ) ) )
         {
         return Redirect::to('administrator/PermissionDenied');
         }*/

        $providerlocation_info    = ProviderLocation::find($pl_id);


        $data = array(
            "providerlocation_info" => $providerlocation_info
        );
        return view('administrator.editplocation',$data);
    }


    /**
     * Delete Provider Location  inforation from the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function DeleteProviderLocationInfo(Request $request)
    {

        $pl_id = $request->input('pl_id');

        $ProviderLocation = new ProviderLocation();
        $ProviderLocation = ProviderLocation::find($pl_id);
        $ProviderLocation->pl_is_deleted          = 1;
        $ProviderLocation->pl_deleted_by          = Session('user_id');
        $ProviderLocation->save();


        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return Response()->json($result_array);
    }



    public function DisplayProviderlocationDropdown( Request $request )
    {
        $pg_class_provider = $request->input('pg_class_provider');
        $ini_location = $request->input('ini_location');

        $lst_locations = ProviderLocation::whereFkUserId($pg_class_provider)->get();

        $result_array = array();

        $result_array['is_error'] = 0;

        $data = array(
            "lst_locations" => $lst_locations,
            "ini_location" => $ini_location,
        );
        $result_array['display']  = view('users.ddproviderlocations',$data)->render();

        return Response()->json($result_array);
    }

}
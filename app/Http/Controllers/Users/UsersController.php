<?php
/***********************************************************
UsersController.php
Product :
Version : 1.0
Release : 1
Date Created : Sep 22, 2016
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2016

Page Description :
Controller contain all pages related to user Table
***********************************************************/


namespace App\Http\Controllers\Users;


use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Helper;
use Auth;
use DB;
use App\model\Users\Users;
use App\model\Users\UserTypes;
use App\model\System\Roles;
use App\model\System\Country;
use App\model\System\Beneficiaries;
use App\model\System\Provinces;
use App\model\System\Districts;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Libraries\Users\UserManager;
use App\model\Project\Projects;
use App\model\Requests\Request As MRequest;
use Illuminate\Support\Facades\Cookie;
use App\model\CMS\MCMSSlider;
use App\model\CMS\CMSMedia;
use App\Libraries\Requests\RequestsManager;
use App\model\Users\UserEducationInfo;
use App\model\System\JobTitles;
use App\model\System\JobRoles;
use App\model\System\EducationDegree;
use App\model\Project\ProjectTrainees;
use App\model\Trainees\History;


class UsersController extends Controller
{

    public function FrontLogin( $message = '' )
    {
        $ut_remember_user = isset($_COOKIE['ut_remember_user']) ? $_COOKIE['ut_remember_user'] : "";
        
        if ($ut_remember_user != "") {
            
            $user_info = decrypt($ut_remember_user);
            $user_info_array = explode('-', $user_info);
            $id = $user_info_array[1];

            $user_info          = Auth::user();
            if($user_info != null)
            {
                $log_in_user_type   = $user_info->u_user_type;
                // Save information in the session
                UserManager::SaveTraineeSessionInformaiton($user_info , false);
                
                return Redirect::to('omsar/dashboard'); 
            }
           
        }

 
        
        
       

        $slideImages    = CMSMedia::whereFkSliderId(1)->whereCmIsDeleted(0)->get();
        $beneficiaries  = Beneficiaries::whereSbIsDeleted(0)->get();

        $data = array(
            "beneficiaries" => $beneficiaries,
            "slideImages" => $slideImages,
            "message" => $message
        );
        return Response()->view('frontend.login',$data);
    }



    public function SendEmailforResetPassword(Request $request)
    {
        $fuser_email = $request->input('fuser_email');
        $ProfileUser = Users::where('u_email',$fuser_email)->get();
        $user_id = $ProfileUser[0]['attributes']['id'];
        
        $result_array = array();
        
        if(count($ProfileUser) == 0)
        {
            $result_array['is_error'] = 1;
            $result_array['error_msg'] = "Please Insert A valid Email";
            return Response()->json($result_array);
        }
        
        $u_fullname = $ProfileUser[0]['attributes']['u_fullname'];
        $u_email = $ProfileUser[0]['attributes']['u_email'];
        $org_password = "123123123";
        $new_password = Hash::make($org_password); 
        $SaveObj = new Users();
        $SaveObj = Users::find($user_id);
        $SaveObj->password =$new_password;
        $SaveObj->save();

        $view_params = array(
            'account_name' => $u_fullname,
            'new_password' => $org_password
        );


        $params_array = array(
            'view' => 'emails.forgot_password',
            'email_subject' => 'Reset Email Email',
            'email_params' => $view_params,
            'account_email' => $u_email,
            'account_name' => $u_fullname
        );
        SendEmail($params_array);


        $result_array['is_error'] = 0;
        $result_array['error_msg'] = "Email Has Been Sent to your mailbox check it out to know the new password";
        return Response()->json($result_array);
    }


    public function AdminLogin(Request $request)
    {
        $ua_remember_user = isset($_COOKIE['ua_remember_user']) ? $_COOKIE['ua_remember_user'] : "";
        $user_admin_info = array();
        
        // check if session of user admin exist we redirect to dashboard
        if(session()->has('user_id'))
        {
            return Redirect::to('administrator/dashboard');
        }
        
       
        
        if ($ua_remember_user != "") 
        {
            $user_info              = decrypt($ua_remember_user);
            $user_info_array      = explode('-', $user_info);
            $id                        = $user_info_array[1];
 
            $remember_user = Users::find($id);
            $user_info          = Auth::user();
            if($user_info != null)
            {
                $log_in_user_type   = $user_info->u_user_type;
                // Save information in the session
                UserManager::SaveSessionInformaiton($user_info , false);
                
                return Redirect::to('administrator');
            }
            
        }

        /*if(session()->has('user_id'))
        {
            dd("in");
            return Redirect::to('administrator/dashboard');
        }*/


        return Response()->view('administrator.login');
    }


    /**
     * Display Page of Trainee Activity
     *
     * @author Moe Mantach
     * @access public
     * @param Integer $user_id
     */
    public function DisplayTraineeHistory( $user_id )
    {
        $requestsHistory       = MRequest::whereFkTraineeId($user_id)->get();
        $request_statuses_byid = RequestsManager::GetRequestStatusInfoById();
        $trainee_projects      = ProjectTrainees::whereFkTraineeId($user_id)->get();
        $lst_projects          = Projects::wherePpIsDeleted(0)->get();
        $lst_projects          = CreateDatabaseArrayByIndex($lst_projects , 'pp_id');
        $trainee_history       = History::whereFkTraineeId($user_id)->get();
        $lst_coordinators      = Users::whereUUserType( UserTypes::COORDINATORS_USER_TYPE_ID )->whereUIsDeleted(0)->get();
        $lst_coordinators      = CreateDatabaseArrayByIndex($lst_coordinators , 'id');
        $lst_beneficaries      = Beneficiaries::whereSbIsDeleted(0)->get();
        $lst_beneficaries      = CreateDatabaseArrayByIndex($lst_beneficaries, 'sb_id');
        
        $projects_array = array();
        foreach ($trainee_projects as $key => $pc_info) {
            $projects_array[] = $pc_info->fk_proj_id;
        }

        $data = array(
            "requestsHistory"      => $requestsHistory,
            "request_statuses_byid"=> $request_statuses_byid,
            "projects_array"       => $projects_array,
            "lst_projects"         => $lst_projects,
            "trainee_history"      => $trainee_history,
            "lst_coordinators"     => $lst_coordinators,
            "lst_beneficaries"     => $lst_beneficaries
        );
        return Response()->view('users.traineehistory',$data);
    }


    /**
     * DIsplay User Profile and timeline of a User
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function DisplayUserProfile(Request $request)
    {

        $user_id = Session('user_id');
        $myprofile_info = Users::find($user_id);
        // get latest Project Created by thi user
        $my_projects = Projects::wherePpIsDeleted(0)->wherePpCreatedBy($user_id)->get();



        $data = array(
            "myprofile_info" => $myprofile_info,
            "my_projects" => $my_projects
        );
        return Response()->view('users.myprofile',$data);
    }


    /**
     * Save Profile Info in the database
     *
     * @author Mantach
     * @access public
     * @param Request $request
     */
    public function SaveProfileInfo(Request $request)
    {
        $u_fullname     = $request->input('u_fullname');
        $u_email        = $request->input('u_email');
        $u_phone        = $request->input('u_phone');
        $u_mobile       = $request->input('u_mobile');
        $u_address      = $request->input('u_address');
        $u_website      = $request->input('u_website');
        $user_id        = Session('user_id');

        $user_save_info                 = Users::find($user_id);
        $user_save_info->u_fullname     = $u_fullname;
        $user_save_info->u_email        = $u_email;
        $user_save_info->u_phone        = $u_phone;
        $user_save_info->u_mobile       = $u_mobile;
        $user_save_info->u_address      = $u_address;
        $user_save_info->u_website      = $u_website;
        $user_save_info->save();

        $result_array = array();

        $result_array['error_msg'] = "Operation Completed Successfully";

        return Response()->json($result_array);
    }


    /**
     * Change Password to current user
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function ChangePasswordInfo(Request $request)
    {
        $result_array = array();

        $u_current_password     = $request->input('u_current_password');
        $u_new_password         = $request->input('u_new_password');
        $u_re_new_password      = $request->input('u_re_new_password');
        $user_id                = Session('user_id');

        $current_profile_info = Users::find($user_id);

        if (!Hash::check($u_current_password, $current_profile_info->password))
        {
            $result_array['is_error'] = 1;
            $result_array['error_msg'] = "Please Insert Your Correct Old Password";

            return Response()->json($result_array);
        }

        $current_profile_info->password = Hash::make($u_new_password);
        $current_profile_info->save();

        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Completed Successfully";

        return Response()->json($result_array);
    }

    /**
     * Page where we can manage all users saved in the database
     *
     * @author Moe Mantach
     * @access public
     *
     */
    public function UsersManagement()
    {
        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_user_management'])  && $role_info['om_user_management'] == 'deny') || ( !isset( $role_info['om_user_management'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }

        $beneficiaries  = Beneficiaries::whereSbIsDeleted(0)->get();
        $data = array(
            "beneficiaries" => $beneficiaries
        );
        return Response()->view('users.users',$data);
    }


    public function ReportListProviders()
    {

        $beneficiaries  = Beneficiaries::whereSbIsDeleted(0)->get();
        $data = array(
            "beneficiaries" => $beneficiaries
        );
        return Response()->view('reports.providers',$data);
    }

    public function ReportListCoordinators()
    {

        $beneficiaries  = Beneficiaries::whereSbIsDeleted(0)->get();
        $data = array(
            "beneficiaries" => $beneficiaries
        );
        return Response()->view('reports.coordinators',$data);
    }
    public function ReportListTrainees()
    {

        $beneficiaries  = Beneficiaries::whereSbIsDeleted(0)->get();
        $data = array(
            "beneficiaries" => $beneficiaries
        );
        return Response()->view('reports.trainees',$data);
    }



    public function DisplayListUsers(Request $request)
    {
        {// initialisation block
            $fb_user_type           = $request->input('fb_user_type');
            $u_beneficiary          = $request->input('u_beneficiary');
            $u_search_username      = $request->input('u_search_username');
            $u_search_fullname      = $request->input('u_search_fullname');
            $u_search_email         = $request->input('u_search_email');
            $page_number            = $request->input('page_number');
            $nbr_rows_per_pages     = Config::get('appconfig.max_rows_per_page');
            $beneficiaries          = Beneficiaries::whereSbIsDeleted(0)->get();
            $benf_info              = CreateDatabaseArrayByIndex($beneficiaries , 'sb_id');
        }

        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;

        $users_obj = Users::whereUIsDeleted(0);

        if($fb_user_type != '')
        {
            $users_obj = $users_obj->whereUUserType($fb_user_type);
        }

        if($u_beneficiary != '')
        {
            $users_obj = $users_obj->whereFkBeneficiaryId($u_beneficiary);
        }

        if(strlen($u_search_username) > 0)
        {
            $users_obj = $users_obj->where('u_username', 'LIKE' , '%' . $u_search_username . '%');
        }

        if(strlen($u_search_fullname) > 0)
        {
            $users_obj = $users_obj->where('u_fullname', 'LIKE' , '%' . $u_search_fullname . '%');
        }

        if(strlen($u_search_email) > 0)
        {
            $users_obj = $users_obj->where('u_email', 'LIKE' , '%' . $u_search_email . '%');
        }


        $all_users = $users_obj->count();




        $total_pages = ceil($all_users/$nbr_rows_per_pages);


         if($total_pages > 1)
                $users = $users_obj->skip($skip)->take($nbr_rows_per_pages)->get();
            else
                $users = $users_obj->get();



        $data = array(
            "users" => $users,
            "benf_info" => $benf_info,
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("users.displaylist",$data)->render();

        return Response()->json($result_array);
    }


    /**
     * Display All Extra Fields Related to selected User Type
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function DisplayFieldsByUserType( Request $request )
    {
        $fb_user_type = $request->input('fb_user_type');
        $user_id        = $request->input('user_id');
        $user_info = new Users();
        if($user_id != null)
                $user_info = Users::find($user_id);
        $result_array = array();

        switch($fb_user_type)
        {
            case UserTypes::ADMIN_USER_TYPE_ID :
                {
                    $beneficiaries  = Beneficiaries::whereSbIsDeleted(0)->orderby('sb_name','ASC')->get();
                    $data = array(
                        'beneficiaries' => $beneficiaries,
                        'user_info' => $user_info
                    );
                    $result_array['display'] = view('users.admin_efields',$data)->render();
                }
            break;
            case UserTypes::COORDINATORS_USER_TYPE_ID :
                {
                    $beneficiaries  = Beneficiaries::whereSbIsDeleted(0)->get();
                    $data = array(
                        'beneficiaries' => $beneficiaries,
                        'user_info' => $user_info
                    );

                    $result_array['display']  = view('users.coordinators_efields',$data)->render();
                }
            break;
            case UserTypes::PROVIDERS_USER_TYPE_ID :
                {
                    $data = array(
                        'user_info' => $user_info
                    );
                    $result_array['display']  = view('users.provider_efields',$data)->render();
                }
            break;
            case UserTypes::TRAINEES_USER_TYPE_ID :
                {
                    $beneficiaries  = Beneficiaries::whereSbIsDeleted(0)->orderby('sb_name','ASC')->get();
                    $preferred_days  = Days::all();
                    $coordinators   = Users::whereUUserType(UserTypes::COORDINATORS_USER_TYPE_ID)->whereUIsActive(1)->get();
                    $lst_districts  = Districts::all();
                    $jobTitles = JobTitles::whereJtIsDeleted(0)->get();
                    $jobRoles = JobRoles::whereJrIsDeleted(0)->get();

                    $data = array(
                        'beneficiaries' => $beneficiaries,
                        'coordinators' => $coordinators,
                        'lst_districts' => $lst_districts,
                        'user_info' => $user_info,
                        'jobTitles' => $jobTitles,
                        'jobRoles' => $jobRoles,
                        'preferred_days' => $preferred_days
                    );

                    $result_array['display']  = view('users.trainees_efields',$data)->render();
                }
            break;
            case UserTypes::TRAINERS_USER_TYPE_ID :
                {

                    $providers   = Users::whereUUserType(UserTypes::PROVIDERS_USER_TYPE_ID)->whereUIsActive(1)->get();
                    $data = array(
                        "providers" => $providers,
                        'user_info' => $user_info
                    );
                    $result_array['display']  = view('users.trainers_efields',$data)->render();
                }
            break;
        }


        return Response()->json($result_array);
    }


    public function AddUser()
    {

        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_add_new_user'])  && $role_info['om_add_new_user'] == 'deny') || ( !isset( $role_info['om_add_new_user'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }

        // get list of countries
        $countries = Country::all();

        // get list of roles
        $roles = Roles::whereRoleIsActive(1)->whereRoleIsDeleted(0)->get();
        $lst_user_types = UserTypes::all();
        $lst_provinces  = Provinces::all();
        $lst_districts  = Districts::all();

        $data = array(
            "countries" => $countries,
            "lst_user_types" => $lst_user_types,
            "lst_provinces" => $lst_provinces,
            "lst_districts" => $lst_districts,
            "roles" => $roles
        );
        return view('users.adduser',$data);
    }


    /**
     * Save User Info to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     *
     * @return Response Json
     */
    public function SaveUserInfo(Request $request)
    {
        $result_array                   = array();
        $u_profile_pic                  = $request->input('u_profile_pic');
        $user_id                        = $request->input('user_id');
        if ($u_profile_pic != "") {

            $response = UserManager::UploadUserProfile($user_id);

            if ($response['is_error'] != 0) {

                $result_array['is_error'] = 1;
                $result_array['error_msg'] = 'An Error occured when uploading the file';
                return Response()->json($result_array);
            }

            $u_avatar_base_src   = $response['data']['u_avatar_base_src'];
            $u_avatar_filename   = $response['data']['u_avatar_filename'];
            $u_avatar_extentions = $response['data']['u_avatar_extentions'];
        }

        
        $username                       = $request->input('u_email');
        $u_password = $request->input('u_password');
        if(strlen($u_password) > 0)
        {
            $u_password                     = Hash::make( $request->input('u_password') );
        }

        $fb_user_type                   = $request->input('fb_user_type');
        $u_role                         = $request->input('u_role');
        $fullname                       = $request->input('fullname');
        $u_gender                       = $request->input('u_gender');
        $fk_province_id                 = 0;
        $fk_district_id                 = 0;
        $u_mobile                       = $request->input('u_mobile');
        $u_email                         = $request->input('u_email');
        $u_phone                        = $request->input('u_phone');
        $u_fax                          = $request->input('u_fax');
        $u_date_birth                   = $request->input('u_date_birth');
        $u_date_birth                   = $u_date_birth . "-1-1";
        $u_residential_area             = $request->input('u_residential_area');
        $fk_beneficiary_id              = $request->input('fk_beneficiary_id');
        $u_coordinator_code             = $request->input('u_coordinator_code');
        $u_registration_code            = $request->input('u_registration_code');
        $u_contact_person               = $request->input('u_contact_person');
        $u_employment_date              = $request->input('u_employment_date');
        $fk_provider_id                 = $request->input('fk_provider_id');
        $fk_provider_id                 = ( strlen($fk_provider_id) > 0 ) ? $fk_provider_id : "0";
        $u_employment_date              = ( strlen($u_employment_date) > 0 ) ? $u_employment_date : date("Y-m-d");
        $fk_coordinator_id              = $request->input('fk_coordinator_id');
        $u_emp_degree                   = $request->input('u_emp_degree');
        $u_gov_grade                    = $request->input('u_gov_grade');
        $u_preferred_train_days         = $request->input('u_preferred_train_days');
        $u_preferred_train_time         = $request->input('u_preferred_train_time');
        $u_preferred_train_location     = $request->input('u_preferred_train_location');
        $u_coordinator_code             = $request->input('u_coordinator_code');
        $u_is_active                    = $request->input('u_is_active');
        $u_trainee_service              = $request->input('u_trainee_service');
        $u_trainee_bureau               = $request->input('u_trainee_bureau');
        $u_trainee_section              = $request->input('u_trainee_section');
        $u_job_title                    = $request->input('u_job_title');
        $u_job_role                     = $request->input('u_job_role');
        $u_grade_type                   = $request->input('u_grade_type');
        $u_trainee_section              = ( strlen($u_trainee_section) > 0 ) ? $u_trainee_section : "";


        $ini_beneficiary_id             = $request->input('ini_beneficiary_id');
        $ini_coordinator_id             = $request->input('ini_coordinator_id');
        $ini_emp_degree                 = $request->input('ini_emp_degree');
        $ini_gov_grade                  = $request->input('ini_gov_grade');
        $ini_trainee_service            = $request->input('ini_trainee_service');
        $ini_trainee_bureau             = $request->input('ini_trainee_bureau');
        $ini_residential_area           = $request->input('ini_residential_area');

        $user_model = new Users();

        if($user_id != null)
        {
            $user_model = Users::find($user_id);
        }

        $u_job_title                    = $request->input('u_job_title');
        $u_job_role                     = $request->input('u_job_role');
        $u_grade_type                   = $request->input('u_grade_type');

        $user_model->u_username = $username;
        if($u_password != '')
        $user_model->password = $u_password;
        $user_model->u_user_type = $fb_user_type;
        $user_model->fk_role_id = $u_role;
        $user_model->u_country = 0;
        $user_model->u_fullname = $fullname;
        $user_model->u_gender = $u_gender;
        $user_model->u_email = $u_email;
        $user_model->fk_province_id = $fk_province_id;
        $user_model->fk_district_id = $fk_district_id;
        $user_model->u_mobile = $u_mobile;
        $user_model->u_phone = $u_phone;
        $user_model->u_fax = $u_fax;
        $user_model->u_date_birth = $u_date_birth;
        $user_model->u_job_title = $u_job_title;
        $user_model->u_job_role = $u_job_role;
        $user_model->u_grade_type = $u_grade_type;
        $user_model->u_residential_area = $u_residential_area;
        $user_model->fk_beneficiary_id = $fk_beneficiary_id;
        $user_model->u_coordinator_code = $u_coordinator_code;
        $user_model->u_registration_code = $u_registration_code;
        $user_model->u_contact_person = $u_contact_person;
        $user_model->u_employment_date = $u_employment_date;
        $user_model->fk_coordinator_id = $fk_coordinator_id;
        $user_model->u_emp_degree = $u_emp_degree;
        $user_model->u_gov_grade = $u_gov_grade;
        $user_model->u_preferred_train_days = $u_preferred_train_days;
        $user_model->u_preferred_train_time = $u_preferred_train_time;
        $user_model->u_preferred_train_location = $u_preferred_train_location;
        $user_model->u_coordinator_code = $u_coordinator_code;
        $user_model->u_trainee_service = $u_trainee_service;
        $user_model->u_trainee_bureau = $u_trainee_bureau;
        $user_model->u_trainee_section = $u_trainee_section;
        $user_model->fk_provider_id = $fk_provider_id;
        $user_model->u_is_active = $u_is_active == 1 ? 1 : 0;
        
        if ($u_profile_pic != "") {
            $user_model->u_avatar_base_src = $u_avatar_base_src;
            $user_model->u_avatar_filename = $u_avatar_filename;
            $user_model->u_avatar_extentions = $u_avatar_extentions;
        }

        $user_model->save();



        // we cannot save logs expet it is in edit mode and if the type of user is trainee
        if($user_id != null && $fb_user_type == UserTypes::TRAINEES_USER_TYPE_ID)
        {
            /**$history = new Users();
            $history->fk_trainee_id = $user_id;
            $history->th_date_history = date("Y-m-d H:i:s");
            if( $ini_beneficiary_id != $fk_beneficiary_id )
            {
                $history->fk_benf_id = $ini_beneficiary_id;
            }

            if( $ini_coordinator_id != $fk_coordinator_id )
            {
                $history->fk_coordinator_id = $ini_coordinator_id;
            }

            if( $ini_emp_degree != $u_emp_degree )
            {
                $history->th_emp_degree = $ini_emp_degree;
            }

            if( $ini_gov_grade != $u_gov_grade )
            {
                $history->th_gov_grade = $ini_gov_grade;
            }

            if( $ini_trainee_service != $u_trainee_service )
            {
                $history->th_trainee_service = $ini_trainee_service;
            }

            if( $ini_trainee_bureau != $u_trainee_bureau )
            {
                $history->th_trainee_bureau = $ini_trainee_bureau;
            }


            if( $ini_residential_area != $u_residential_area )
            {
                $history->th_residential_area = $ini_residential_area;
            }

            $history->save();*/

        }

        $user_id = $user_model->id;
        

        $result_array['is_error'] = 0;

        $result_array['u_user_type']    = $fb_user_type;
        $result_array['user_id']        = $user_id;

        return Response()->json($result_array);
    }


    /**
     * Get Coordinators dropdown depand on selected beneficiary
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function GetCoordinatorsDropdown(Request $request)
    {
        $beneficiary_id = $request->input('beneficiary_id');
        $ini_coordinator_id = $request->input('ini_coordinator_id');

        $lstCoordinators = Users::whereUUserType(UserTypes::COORDINATORS_USER_TYPE_ID)->whereFkBeneficiaryId($beneficiary_id)->get();

        $result_array = array();

        $data = array(
            "lstCoordinators" => $lstCoordinators,
            "ini_coordinator_id" => $ini_coordinator_id,
        );

        $result_array['is_error'] = 0;
        $result_array['display'] = view('users.coordinatorsdropdown',$data)->render();
        return Response()->json($result_array);
    }

    /**
     * Display Edit User Page
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $user_id
     */
    public function EditUserForm( $user_id )
    {
        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_edit_user'])  && $role_info['om_edit_user'] == 'deny') || ( !isset( $role_info['om_edit_user'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }

        // get list of countries
        $countries = Country::all();

        // get list of roles
        $roles = Roles::whereRoleIsActive(1)->whereRoleIsDeleted(0)->get();
        $lst_user_types = UserTypes::all();
        $lst_provinces  = Provinces::all();
        $lst_districts  = Districts::all();
        $user_info = Users::find($user_id);


        $data = array(
            "countries" => $countries,
            "lst_user_types" => $lst_user_types,
            "lst_provinces" => $lst_provinces,
            "lst_districts" => $lst_districts,
            "user_info" => $user_info,
            "user_id" => $user_id,
            "roles" => $roles
        );
        return Response()->view('users.edituser',$data);
    }


    /**
     * Display list of education info to saved in the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function DisplayListUserEduccation(Request $request)
    {
        $user_id = $request->input('user_id');

        $userEducationInfo = UserEducationInfo::whereFkUserId($user_id)->get();

        $result_array = array();

        $data = array(
                'userEducationInfo' => $userEducationInfo
            );

            $result_array['is_error'] = 0;
            $result_array['display'] = view('users.educationinfo',$data)->render();

        return Response()->json($result_array);
    }


    /**
     * Add User Education info
     *
     * @author Moe Mantach
     * @access public
     * @param Integer $user_id
     */
    public function AddEducationInfo($user_id)
    {
        $EducationDegrees = EducationDegree::whereEdIsDeleted(0)->get();

        $data = array(
            "user_id" => $user_id,
            "EducationDegrees" => $EducationDegrees,
        );
        return Response()->view('users.addeducationinfo',$data);
    }


    public function EditEducationInfo($ue_id)
    {
        $userEducationInfo = UserEducationInfo::find($ue_id);
        $EducationDegrees = EducationDegree::whereEdIsDeleted(0)->get();
        $data = array(
            "userEducationInfo" => $userEducationInfo,
            "EducationDegrees" => $EducationDegrees,
        );
        return Response()->view('users.editeducationinfo',$data);
    }



    public function AddAEducationInfo($user_id)
    {
        $EducationDegrees = EducationDegree::whereEdIsDeleted(0)->get();

        $data = array(
            "user_id" => $user_id,
            "EducationDegrees" => $EducationDegrees,
        );
        return Response()->view('administrator.addAeducationinfo',$data);
    }


    public function EditAEducationInfo($ue_id)
    {
        $userEducationInfo = UserEducationInfo::find($ue_id);
        $EducationDegrees = EducationDegree::whereEdIsDeleted(0)->get();
        $data = array(
            "userEducationInfo" => $userEducationInfo,
            "EducationDegrees" => $EducationDegrees,
        );
        return Response()->view('administrator.editAeducationinfo',$data);
    }

    public function DeleteUserEducationInfo( Request $request )
    {
        $ue_id = $request->input('ue_id');
        UserEducationInfo::destroy($ue_id);
        $result_array = array(
            'is_error' => 0
        );

        return Response()->json($result_array);
    }


    /**
     * Save Education Info and put it in the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function SaveEducationInfo(Request $request)
    {
        $ue_education_name      = $request->input('ue_education_name');
        $ue_education_details   = $request->input('ue_education_details');
        $fk_degree_id   = $request->input('fk_degree_id');
        $user_id   = $request->input('user_id');
        $ue_id   = $request->input('ue_id');

        $UserEducationInfo = new UserEducationInfo();

        if(is_numeric($ue_id))
        {
            $UserEducationInfo = UserEducationInfo::find($ue_id);
        }

        $UserEducationInfo->fk_user_id = $user_id;
        $UserEducationInfo->ue_education_name = $ue_education_name;
        $UserEducationInfo->fk_degree_id = $fk_degree_id;
        $UserEducationInfo->ue_education_details = $ue_education_details;
        $UserEducationInfo->save();

        $result_array = array(
            'is_error' => 0,
            'error_msg' => 'operation Complete Succesfully',
        );
        return Response()->json($result_array);
    }


    public function DeleteUser(Request $request)
    {
        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_edit_user'])  && $role_info['om_edit_user'] == 'deny') || ( !isset( $role_info['om_edit_user'] ) ) )
        {
             $result_array['is_error']   = 1;
             $result_array['error_msg']  = "Access Denied You Cannot Delete A User ";
             return Response()->json($result_array);
        }
        
        $user_id = $request->input('user_id');

        UserManager::DeleteUserProfile($user_id);///Delete Profile Photo

        Users::deleteUserData($user_id);

        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return Response()->json($result_array);
    }


    public function DeleteMultipleUsers( Request $request )
    {
        $users_ids = $request->input('users_ids');


        Users::deleteUserData($users_ids);

        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return Response()->json($result_array);
    }

    /**
     * Display Data Grid Of Multiple Users where we can add multiple Users
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function MultipleUsersDataGrid( Request $request )
    {
        $data = array(

        );
        return Response()->view('users.addmultipleUsers',$data);
    }


    /**
     * get view of user form in a table row to add it to data grid of multiple user adding
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function GetUserRow(Request $request)
    {
        $result_array = array();
        $roles = Roles::all();
        $lst_user_types = UserTypes::all();
        $beneficiaries  = Beneficiaries::all();

        $data = array(
            "roles" => $roles,
            "lst_user_types" => $lst_user_types,
            "beneficiaries" => $beneficiaries
        );
        $result_array['display'] = view('users.userRow',$data)->render();
        return Response()->json($result_array);
    }


    public function SaveMultipleUsers(Request $request)
    {
        $u_fullname             = $request->input('u_fullname');
        $u_username             = $request->input('u_username');
        $password               = $request->input('password');
        $fk_role_id             = $request->input('fk_role_id');
        $fb_user_type           = $request->input('fb_user_type');
        $fk_beneficiary_id      = $request->input('fk_beneficiary_id');
        $u_email                = $request->input('u_email');

        for ($i = 0; $i < count($u_fullname); $i++)
        {
            $users = new Users();
            $users->u_fullname              = $u_fullname[$i];
            $users->u_username              = $u_username[$i];
            $users->password                = Hash::make($password[$i]);
            $users->fk_role_id              = $fk_role_id[$i];
            $users->u_user_type            = $fb_user_type[$i];
            $users->fk_beneficiary_id       = $fk_beneficiary_id[$i];
            $users->u_email                 = $u_email[$i];
            $users->u_employment_date        = date('Y-m-d');
            $users->u_is_active             = 1;
            $users->save();
        }

        $result_array = array();

        $result_array['is_error'] = 0;
        return Response()->json($result_array);
    }


    /**
     * Activate Registred Account by change Flag  u_is_active from 0 to 1 and
     * redirect to login page
     *
     * @author Moe Mantach
     * @access public
     * @param String $activation_key
     */
    public function ActivateAccount( $activation_key )
    {
        $result_array = array();
        $user_info = UserManager::GetUserInfoByActivationKey( $activation_key );
        $user_id = $user_info[0]['attributes']['id'];


        if($user_id != null)
        {
            // change flag is_active to 1
            $model = new Users();
            $model = Users::find($user_id);
            $model->u_is_active = 1;
            $model->save();

            return redirect('/omsar/myprofile');
        }
        else
        {
            return  redirect('ActivationError');
        }


    }

}
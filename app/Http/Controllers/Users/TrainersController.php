<?php
/***********************************************************
TrainersController.php
Product :
Version : 1.0
Release : 2
Date Created : Apr 4, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/



namespace App\Http\Controllers\Users;


use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Helper;
use Auth;
use DB;
use App\model\Users\Users;
use App\model\Users\UserTypes;
use App\model\System\Roles;
use App\model\System\Country;
use App\model\System\Beneficiaries;
use App\model\System\Provinces;
use App\model\System\Districts;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Libraries\Users\UserManager;
use App\model\Project\Projects;
use App\model\Requests\Request As MRequest;
use Illuminate\Support\Facades\Cookie;
use App\model\CMS\MCMSSlider;
use App\model\CMS\CMSMedia;
use App\Libraries\Requests\RequestsManager;
use App\model\Users\UserEducationInfo;
use App\model\System\JobTitles;
use App\model\System\JobRoles;
use App\model\System\EducationDegree;
use App\model\Project\ProjectBeneficiaries;


class TrainersController extends Controller
{
    /**
     * Display Trainees Attendances Management where every trainer can take attendances for
     *
     * @author Moe Mantach
     * @access public
     */
    public function TraineesAttendances()
    {
        $ut_user_type                     = Session('ut_user_type');
        $ut_beneficiary_id                = Session('ut_beneficiary_id');
       
        $lst_projects = Projects::wherePpIsDeleted(0);
        
        if($ut_user_type == UserTypes::COORDINATORS_USER_TYPE_ID)
        {
            $benf = ProjectBeneficiaries::whereFkBenfId($ut_beneficiary_id)->get();
            $project_ids = array();
            foreach ($benf as $key => $info) {
                $project_ids[] = $info->fk_project_id;
            } 
            
            
            if(count($project_ids) > 0)
            {
                $lst_projects= $lst_projects->whereIn('pp_id',$project_ids);
            }
            else 
              {
                  $lst_projects= $lst_projects->where('fk_beneficiary_id',0);
            }
        
        }
        $lst_projects = $lst_projects->get();
        
        $data = array(
            'lst_projects' => $lst_projects
        );
        return Response()->view('trainers.traineesAttendances',$data);
    }



    public function TraineesTestResults()
    {
        $lst_projects = Projects::wherePpIsDeleted(0)->get();
        $data = array(
            'lst_projects' => $lst_projects
        );
        return Response()->view('trainers.TraineesTestResults',$data);
    }
    
    
    /**
     * Trainers Management contain list of all trainers related to selected Provider
     * 
     * @author Moe Mantach
     * @access public
     */
    public function TrainersManagement()
    {
        
        $data = array();
        return Response()->view('trainers.trainers',$data);
    }
    /**
     * 
     * @param Request $request
     * @return Object View
     * @description display list of trainers related to selected Provider
     */
    public function DisplayListTrainers(Request $request){
        
        {// initialisation block
            $page_number        = $request->input('page_number');
            $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
            $session_user_id    = session()->get('ut_user_id');
        }

        if ($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages;
        else
            $skip = 0;

        $trainers_obj = Users::whereUIsDeleted(0);

        $trainers_obj = $trainers_obj->whereUUserType(UserTypes::TRAINERS_USER_TYPE_ID);
        $trainers_obj = $trainers_obj->where('fk_provider_id','=',$session_user_id);
        
        
        $all_trainers = $trainers_obj->count();

        $total_pages = ceil($all_trainers / $nbr_rows_per_pages);


        if ($total_pages > 1)
            $trainers = $trainers_obj->skip($skip)->take($nbr_rows_per_pages)->get();
        else
            $trainers = $trainers_obj->get();



        $data = array(
            "trainers"  => $trainers
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("trainers.displaylisttrainers", $data)->render();

        return Response()->json($result_array);
    }
    /**
     * @return object View
     */
    public function AddNewTrainer(){
        
        return view('trainers.addnewtrainer');
    }
    /**
     * 
     * @param Request $request
     * @return Json Response
     * Functionality of add/edit user(trainer)
     */
    public function SaveTrainerInfo(Request $request){
        
        $form_type                      = $request->input('form_type');
        $user_id                        = $request->input('user_id');
        $username                       = $request->input('u_email');
        $fb_user_type                   = $request->input('fb_user_type');
        $u_role                         = $request->input('u_role');
        $fk_provider_id                 = $request->input('fk_provider_id');
        $fk_provider_id                 = ( strlen($fk_provider_id) > 0 ) ? $fk_provider_id : "0";
        $fullname                       = $request->input('fullname');
        $u_email                        = $request->input('u_email');
        $u_password                     = ( $form_type == 'edit' && empty($request->input('u_password_edit')) == false ) ? $request->input('u_password_edit') : $request->input('u_password') ;
        
        if(strlen($u_password) > 0)
        {
            $u_password                 = Hash::make( $request->input('u_password') );
        }
        
        $u_gender                       = $request->input('u_gender');
        $u_residential_area             = $request->input('u_residential_area');
        $u_date_birth                   = $request->input('u_date_birth');
        $u_date_birth                   = $u_date_birth ;
        $u_is_active                    = $request->input('u_is_active');
        $u_mobile                       = $request->input('u_mobile');
        $u_phone                        = $request->input('u_phone');
        $u_fax                          = $request->input('u_fax');
        $u_employment_date              = $request->input('u_employment_date');
        $u_employment_date              = ( strlen($u_employment_date) > 0 ) ? $u_employment_date : date("Y-m-d");
        $fk_province_id                 = 0;
        $fk_district_id                 = 0;

        $user_model = new Users();

        if($user_id != null)
        {
            $user_model = Users::find($user_id);
        }

        $user_model->u_username = $username;
        
        if($u_password != ''){
            $user_model->password = $u_password;
        }
        
        $user_model->u_user_type        = $fb_user_type;
        $user_model->fk_role_id         = $u_role;
        $user_model->u_country          = 0;
        $user_model->u_fullname         = $fullname;
        $user_model->u_gender           = $u_gender;
        $user_model->u_employment_date  = $u_employment_date;
        $user_model->fk_beneficiary_id  = NULL;
        $user_model->u_email            = $u_email;
        $user_model->fk_province_id     = $fk_province_id;
        $user_model->fk_district_id     = $fk_district_id;
        $user_model->u_mobile           = $u_mobile;
        $user_model->u_phone            = $u_phone;
        $user_model->u_fax              = $u_fax;
        $user_model->u_date_birth       = $u_date_birth;
        $user_model->u_residential_area = $u_residential_area;
        $user_model->fk_provider_id     = $fk_provider_id;
        $user_model->u_is_active        = $u_is_active == 1 ? 1 : 0;

        $user_model->save();
        
        $user_id      = $user_model->id;
        $result_array = array();

        $result_array['is_error'] = 0;

        $result_array['u_user_type']    = $fb_user_type;
        $result_array['user_id']        = $user_id;

        return Response()->json($result_array);
    }
    /**
     * 
     * @param int $user_id
     * @return Object view
     * @description view edit form
     */
    public function EditTrainer($user_id){
        
        $user_info      = Users::find($user_id);

        $data = array(
            "user_info" => $user_info,
            "user_id" => $user_id
        );
        return Response()->view('trainers.edittrainer',$data);
    }
    
    public function DeleteTrainer(Request $request){
        
        $user_id = $request->input('user_id');


        Users::deleteUserData($user_id);

        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return Response()->json($result_array);
    }
}
<?php
/***********************************************************
SessionStatusesController.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 28, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/



namespace App\Http\Controllers\Sessions;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\System\Roles;
use App\model\Sessions\SessionStatus;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;


class SessionStatusesController extends Controller
{
    public function SessionStatusesManagement()
    {
        $data = array();
        return Response()->view('sessionstatuses.sessionstatus',$data);
    }


    public function DisplayListSessionStatuses(Request $request)
    {
        $page_number        = $request->input('page_number');
        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;



        $all_session_statuses = SessionStatus::whereSsIsDeleted(0)->count();


        $total_pages = ceil($all_session_statuses/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);

        $session_statuses = SessionStatus::whereSsIsDeleted(0)->skip($skip)->take($nbr_rows_per_pages)->get();
        $data = array(
            "session_statuses" => $session_statuses
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("sessionstatuses.displaylist",$data)->render();

        return Response()->json($result_array);
    }


    public function AddSessionStatusForm()
    {

        $data = array();
        return view('sessionstatuses.addsessionstatus',$data);
    }


    /**
     * Save Project Status information to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function SaveSessionStatusInfo(Request $request)
    {
        $ss_id                          = $request->input('ss_id');
        $ss_status_name                 = $request->input('ss_status_name');
        $ss_status_color                = $request->input('ss_status_color');

        $result_array = array();

        $SessionStatus = new SessionStatus();
        if($ss_id !== null)
        {
            $SessionStatus = SessionStatus::find($ss_id);
        }

        $SessionStatus->ss_status_name      = $ss_status_name;
        $SessionStatus->ss_status_color     = $ss_status_color;
        $SessionStatus->save();


        $result_array['is_error']  = 0;
        $result_array['error_msg'] = 'Session Status Information Has been saved';

        return Response()->json($result_array);
    }



    /**
     * Display Edit Session Status Form Page
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $bt_id
     */
    public function EditSessionStatusForm( $ss_id )
    {

        $cur_session_status_info    = SessionStatus::find( $ss_id );

        $data = array(
            "cur_session_status_info" => $cur_session_status_info
        );
        return view('sessionstatuses.editsessionstatus',$data);
    }


    public function DeleteSessionStatusInfo(Request $request)
    {
        $ss_id = $request->input('ss_id');

        $SessionStatus = new SessionStatus();
        $SessionStatus = SessionStatus::find($ss_id);
        $SessionStatus->ss_is_deleted          = 1;
        $SessionStatus->ss_deleted_by          = Session('user_id');
        $SessionStatus->save();


        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return Response()->json($result_array);
    }

}
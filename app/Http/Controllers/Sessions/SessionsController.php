<?php
/***********************************************************
SessionsController.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 25, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/



namespace App\Http\Controllers\Sessions;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use DateTime;
use App\model\System\Roles;
use App\model\Sessions\SessionStatus;
use App\model\Sessions\SessionClass;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\model\Project\ProjectClasses;
use App\model\Project\ProjectCourses;
use App\model\System\Courses;
use App\model\Sessions\SessionType;
use App\model\Project\ProjectCourseTrainees;
use App\model\Users\Users;
use App\model\Sessions\SessionAttendance;
use App\model\Sessions\SessionTestResult;
use App\model\Users\UserTypes;
use App\model\System\ProviderLocation;
use App\model\Project\ProjectGroupTrainees;
use App\model\Project\Projects;
use App\model\Project\ProjectGroups;


class SessionsController extends Controller
{
    /**
     * Open Popup model page to save sessions to the database
     *
     * @author Moe Mantach
     * @acess public
     * @param unknown $session_id
     * @return \Illuminate\Http\Response
     */
    public function EditSessionInformation($session_id)
    {
        $session_class_info     = SessionClass::find($session_id);
        $sessions_status_info   = SessionStatus::whereSsIsDeleted(0)->get();
        $class_id               = $session_class_info['attributes']['fk_class_id'];
        $project_class_info     = ProjectClasses::find($class_id);
        $project_course_id      = $project_class_info['attributes']['fk_pcourse_id'];
        $projectCourseInfo      = ProjectCourses::find($project_course_id);
        $course_id              = $projectCourseInfo['attributes']['fk_course_id'];
        $course_info            = Courses::find($course_id);
        $list_session_types     = SessionType::whereStIsDeleted(0)->get();
        $trainer_info           = Users::whereUUserType(UserTypes::TRAINERS_USER_TYPE_ID)->whereUIsDeleted(0)->whereUIsActive(1)->get();
        $lst_location           = ProviderLocation::wherePlIsDeleted(0)->get();
        $data = array(
            "session_class_info" => $session_class_info,
            "sessions_status_info" => $sessions_status_info,
            "trainer_info" => $trainer_info,
            "list_session_types" => $list_session_types,
            "course_info" => $course_info,
            "lst_location" => $lst_location,
            "session_id" => $session_id,
        );
        return Response()->view('sessions.editsessioninformation',$data);
    }

    /**
     * Save Session Information to the database and
     * return the result of the save
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function SaveSessionInformation( Request $request )
    {
        $session_id                 = $request->input('session_id');
        $fk_session_status_id       = $request->input('fk_session_status_id');
        $sc_session_date            = $request->input('sc_session_date');
        $sc_session_time            = $request->input('sc_session_time');
        $sc_session_duration        = $request->input('sc_session_duration');
        $sc_session_location        = $request->input('sc_session_location');
        $sc_session_description     = $request->input('sc_session_description');
        $sc_session_room_number     = $request->input('sc_session_room_number');
        $fk_session_trainer_id      = $request->input('fk_session_trainer_id');
        $sc_session_type = $request->input('sc_session_type');

        $sessionClass = new SessionClass();

        if($session_id > 0)
        {
            $sessionClass = SessionClass::find($session_id);
            $sessionClass->fk_session_status_id     = $fk_session_status_id;
            $sessionClass->sc_session_date          = $sc_session_date;
            $sessionClass->sc_session_time          = $sc_session_time;
            $sessionClass->sc_session_duration      = $sc_session_duration;
            $sessionClass->sc_session_location      = $sc_session_location;
            $sessionClass->sc_session_type          = $sc_session_type;
            $sessionClass->sc_session_description   = $sc_session_description;
            $sessionClass->sc_session_room_number   = $sc_session_room_number;
            $sessionClass->fk_session_trainer_id    = $fk_session_trainer_id;
            $sessionClass->save();
        }

        $result_array = array();
        $result_array['is_error'] = 0;
        $result_array['error_msg'] = "Operation Completed Successfully";


        return Response()->json($result_array);
    }


    /**
     * Change Information of Session time and date
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function ChangeSaveInformation( Request $request )
    {
        $session_id = $request->input('session_id');
        $start_date = $request->input('start_date');
        $end_date   = $request->input('end_date');
        if($start_date != null)
            $start_date_array = explode(" ", $start_date);
        else
            $start_date_array = array();
        if($end_date != null)
            $end_date_array = explode(" ", $end_date);
        else
            $end_date_array = array();

        if($start_date != null)
            $start  = $start_date_array[3] . "-" . $start_date_array[1] . "-" . $start_date_array[2] . " " . $start_date_array[4] ;
        else
            $start = date("Y-m-d");

        if($end_date != null)
            $end    = $end_date_array[3] . "-" . $end_date_array[1] . "-" . $end_date_array[2] . " " . $end_date_array[4] ;
        else
            $end = date("Y-m-d");


        $sc_session_date = date("Y-m-d",strtotime($start));

        $time = strtotime($end) - strtotime($start);

        $start_time = $start_date_array[4];

        $session_obj = SessionClass::find($session_id);
        $session_obj->sc_session_date = $sc_session_date;
        $session_obj->sc_session_time = $start_time;
        $session_obj->sc_session_duration = ($time/3600);
        $session_obj->save();

        $result_array = array();

        $result_array['is_error'] = 0;
        $result_array['error_msg'] = "Operation Complete Successfully";
        return Response()->json($result_array);
    }

    /**
     * Get Dropdown of Sessions for selected Class
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function GetSessionsDropdown( Request $request )
    {
        $class_id = $request->input('class_id');

        $SessionClass = SessionClass::whereFkClassId($class_id)->get();


        $result_array = array();
        $result_array['is_error'] = 0;
        $data = array(
            "SessionClass" => $SessionClass
        );
        $result_array['display'] = view('sessions.sessionsDropdown',$data)->render();
        return Response()->json($result_array);

    }

    /**
     * Get Dropdown list of all test sessions related to selected class
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function GetSessionsTestDropdown( Request $request )
    {
        $class_id = $request->input('class_id');

        $SessionClass = SessionClass::whereFkClassId($class_id)->whereScSessionType(2)->get();


        $result_array = array();
        $result_array['is_error'] = 0;
        $data = array(
            "SessionClass" => $SessionClass
        );
        $result_array['display'] = view('sessions.sessionsDropdown',$data)->render();
        return Response()->json($result_array);

    }


    /**
     * Display List of Trainees in selected class and Session
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function DisplayListClassTrainees( Request $request )
    {
        $class_id   = $request->input('class_id');
        $session_id = $request->input('session_id');

        $ProjectClass   = ProjectClasses::find($class_id);
        $pcourse_id     = $ProjectClass->fk_pcourse_id;
        $ProjectCourseTrainees = ProjectCourseTrainees::whereFkPcourseId($pcourse_id)->get();

        $trainee_ids = array();
        foreach ($ProjectCourseTrainees as $ct_id => $ct_info)
        {
            $trainee_ids[] = $ct_info->fk_trainee_id;
        }

        $lst_trainees = Users::whereIn('id',$trainee_ids)->get();
        $session_attendance = SessionAttendance::whereFkSessionId($session_id)->get();
        $Session_trainee_attendance = array();

        foreach ( $session_attendance as $index => $sa_info )
        {
            $Session_trainee_attendance[ $sa_info->fk_trainee_id ] = $sa_info->sa_trainee_attendance;
        }

        $result_array = array();
        $result_array['is_error'] = 0;
        $data = array(
            "lst_trainees" => $lst_trainees,
            "Session_trainee_attendance" => $Session_trainee_attendance
        );
        $result_array['display'] = view('classes.trainees',$data)->render();

        return Response()->json($result_array);
    }

    /**
     * Page to Save Trainee Test Results
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function TraineesTestResults( Request $request  )
    {
        $class_id   = $request->input('class_id');
        $session_id = $request->input('session_id');

        $ProjectClass   = ProjectClasses::find($class_id);
        $pcourse_id     = $ProjectClass->fk_pcourse_id;
        $ProjectCourseTrainees = ProjectCourseTrainees::whereFkPcourseId($pcourse_id)->get();

        $trainee_ids = array();
        foreach ($ProjectCourseTrainees as $ct_id => $ct_info)
        {
            $trainee_ids[] = $ct_info->fk_trainee_id;
        }

        $lst_trainees = Users::whereIn('id',$trainee_ids)->get();

        $session_test_result = SessionTestResult::whereFkSessionId($session_id)->get();
        $Session_trainee_test_result = array();

        foreach ( $session_test_result as $index => $sr_info )
        {
            $Session_trainee_test_result[ $sr_info->fk_trainee_id ]['score'] = $sr_info->sr_score;
            $Session_trainee_test_result[ $sr_info->fk_trainee_id ]['test_result'] = $sr_info->sr_test_result;
            $Session_trainee_test_result[ $sr_info->fk_trainee_id ]['retake'] = $sr_info->sr_retake;
        }


        $data = array(
            "lst_trainees" => $lst_trainees,
            "Session_trainee_test_result" => $Session_trainee_test_result,
        );
        $result_array['display'] = view('sessions.testresult',$data)->render();

        return Response()->json($result_array);
    }


    /**
     * Open Page of my calender contain all sessions for the logged in
     * Trainee
     *
     * @author Moe Mantach
     * @access public
     *
     *
     */
    public function DisplayMyCalendar()
    {
        $user_id = session('ut_user_id');
        // get list of classes and projects you are requered in

        $ProjectGroupTrainees = ProjectGroupTrainees::whereFkTraineeId($user_id)->get();

        $group_ids = array();

        foreach ($ProjectGroupTrainees as $pg_index => $pg_info ) {
            $group_ids[] = $pg_info->fk_group_id;
        }

        $projectClasses = ProjectClasses::whereIn('fk_group_id',$group_ids)->get();


        $class_ids = array();
        foreach ($projectClasses as $key => $info) {
            $class_ids[] = $info->cls_id;
        }

        $sessions_info = DB::table('ses_session_class')->leftJoin('proj_classes','ses_session_class.fk_class_id','=','proj_classes.cls_id')
        ->leftJoin('proj_projects','proj_classes.fk_project_id','=','proj_projects.pp_id')
        ->leftJoin('proj_project_courses','proj_classes.fk_pcourse_id','=','proj_project_courses.pc_id')
        ->leftJoin('sys_courses','proj_project_courses.fk_course_id','=','sys_courses.c_id')->whereIn('fk_class_id',$class_ids)->get();

        // generate all events for this class


        $xml_session_data = "<data>";

        for ($i = 0; $i < count($sessions_info); $i++)
        {

        $session_title = $sessions_info[$i]->pp_project_title . " - " .  $sessions_info[$i]->c_course_name;

        $start_session_date = $sessions_info[$i]->sc_session_date . " " . $sessions_info[$i]->sc_session_time;

        //$end_date = strtotime($sessions_info[$i]->sc_session_date . " " . $sessions_info[$i]->sc_session_time . " + " . $sessions_info[$i]->sc_session_duration . " hours");
        $time_array = explode(":", $sessions_info[$i]->sc_session_time);

        $duration = $sessions_info[$i]->sc_session_duration;

        $time = strtotime($sessions_info[$i]->sc_session_time) + $sessions_info[$i]->sc_session_duration * 3600;
        $time = date("H:i:s" , $time);
        $end_session_date = $sessions_info[$i]->sc_session_date . " " . $time;

        $xml_session_data .= "<event id='" . $sessions_info[$i]->sc_id . "'>\n";
        $xml_session_data .= "<start_date>" . date("Y-m-d H:i:s" , strtotime($start_session_date)) . "</start_date>\n";
        $xml_session_data .= "<end_date>" . date("Y-m-d H:i:s" , strtotime($end_session_date)) . "</end_date>\n";
                $xml_session_data .= "<text><![CDATA[ " . $session_title . " ]]></text>\n";
                    $xml_session_data .= "<details><![CDATA[  " . $session_title . " ]]></details>\n";
                    $xml_session_data .= "</event>\n";


        }
                        $xml_session_data .= "</data>";

                    $xml_path = public_path("resources/calenders/mycalendars/");

                        if(!is_dir($xml_path))
                            CreateDirectory($xml_path);
                            $filename = $xml_path . "mycalendar_" . $user_id  . ".xml";
                            $fp = fopen($filename, "w+");
                                fwrite($fp, $xml_session_data);
                            fclose($fp);

                            $xml_url = url('resources/calenders/mycalendars/'   .  "mycalendar_" . $user_id  . ".xml");



        $data = array(
                    'xml_url' => $xml_url
                            );
                            return Response()->view('sessions.mycalendar',$data);

    }
    /**
     * 
     * @return Object view
     * Open Page of my session contain all sessions for the current day of the logged in trainer in
     */
    public function DisplayMySessions()
    {
        $user_id = session('ut_user_id');
        $date = new DateTime();
        $now  = $date->format("Y-m-d"); // Get current Date

        $sessions_info = DB::table('ses_session_class')->leftJoin('proj_classes','ses_session_class.fk_class_id','=','proj_classes.cls_id')
        ->leftJoin('proj_projects','proj_classes.fk_project_id','=','proj_projects.pp_id')
        ->leftJoin('proj_project_courses','proj_classes.fk_pcourse_id','=','proj_project_courses.pc_id')
        ->leftJoin('sys_courses','proj_project_courses.fk_course_id','=','sys_courses.c_id')
        ->where('fk_session_trainer_id','=',$user_id)->where('sc_session_date','=',$now)->get();

        // generate all events for this class

        $xml_session_data = "<data>";

        for ($i = 0; $i < count($sessions_info); $i++) {

            $session_title = $sessions_info[$i]->pp_project_title . " - " . $sessions_info[$i]->c_course_name;

            $start_session_date = $sessions_info[$i]->sc_session_date . " " . $sessions_info[$i]->sc_session_time;

            //$end_date = strtotime($sessions_info[$i]->sc_session_date . " " . $sessions_info[$i]->sc_session_time . " + " . $sessions_info[$i]->sc_session_duration . " hours");
            $time_array = explode(":", $sessions_info[$i]->sc_session_time);

            $duration = $sessions_info[$i]->sc_session_duration;

            $time = strtotime($sessions_info[$i]->sc_session_time) + $sessions_info[$i]->sc_session_duration * 3600;
            $time = date("H:i:s", $time);
            $end_session_date = $sessions_info[$i]->sc_session_date . " " . $time;

            $xml_session_data .= "<event id='" . $sessions_info[$i]->sc_id . "'>\n";
            $xml_session_data .= "<start_date>" . date("Y-m-d H:i:s", strtotime($start_session_date)) . "</start_date>\n";
            $xml_session_data .= "<end_date>" . date("Y-m-d H:i:s", strtotime($end_session_date)) . "</end_date>\n";
            $xml_session_data .= "<text><![CDATA[ " . $session_title . " ]]></text>\n";
            $xml_session_data .= "<details><![CDATA[  " . $session_title . " ]]></details>\n";
            $xml_session_data .= "</event>\n";
        }
        $xml_session_data .= "</data>";

        $xml_path = public_path("resources/sessions/mysessions/");

        if (!is_dir($xml_path)) {
            CreateDirectory($xml_path);
        }
        
        $filename = $xml_path . "mysession_" . $user_id . ".xml";
        $fp = fopen($filename, "w+");
        fwrite($fp, $xml_session_data);
        fclose($fp);

        $xml_url = url('resources/sessions/mysessions/' . "mysession_" . $user_id . ".xml");



        $data = array(
            'xml_url' => $xml_url
        );
        return Response()->view('sessions.mysession', $data);
    }
    
    /**
     *  View selected Session Information in a popup window
     *  
     *  @author Moe Mantach
     *  @access public
     *  @param number $session_id
     */
    public function ViewSessionInfo($session_id)
    {
        $SessionInfo    = SessionClass::find($session_id);
        $ClassInfo      = ProjectClasses::find($SessionInfo->fk_class_id);
        $GroupInfo      = ProjectGroups::find($ClassInfo->fk_group_id);
        $ProjectInfo = Projects::find($ClassInfo->fk_project_id);
        $course_project_id =  $ClassInfo->fk_pcourse_id;
        $projectCourse = ProjectCourses::find($course_project_id);
        
        $course_id = $projectCourse->fk_course_id;
        
        $course_info = Courses::find($course_id);
        
        $data = array(
            "SessionInfo" => $SessionInfo,
            "ClassInfo" => $ClassInfo,
            "ProjectInfo" => $ProjectInfo,
            "GroupInfo" => $GroupInfo,
            "course_info" => $course_info
        );
        return Response()->view('sessions.viewSession',$data);
    }



    public function SaveTraineeResults( Request $request )
    {
        $sc_sessions_class          = $request->input('sc_sessions_class');
        $class_id                   = $request->input('fk_class_id');
        $trainee_id                 = $request->input('trainee_id');

        $projectClass       = ProjectClasses::find($class_id);
        $pcourse_id         = $projectClass->fk_pcourse_id;

        for ($i = 0; $i < count($trainee_id); $i++)
        {
           $trainee_score           = $request->input("trainee_score");
           $trainee_test_passed     = $request->input("trainee_test_passed");
           $trainee_course_retake   = $request->input("trainee_course_retake");

           $SessionTestResults = new SessionTestResult();
           $SessionTestResults->fk_trainee_id = $trainee_id[$i];
           $SessionTestResults->fk_pc_id = $pcourse_id;
           $SessionTestResults->fk_session_id = $sc_sessions_class;
           $SessionTestResults->sr_score = $trainee_score[$trainee_id[$i]];
           $SessionTestResults->sr_test_result = isset($trainee_test_passed[$trainee_id[$i]]) ? 1 : 0;
           $SessionTestResults->sr_retake = isset( $trainee_course_retake[$trainee_id[$i]]) ? 1 : 0;
           $SessionTestResults->sr_result_date = date("Y-m-d");
           $SessionTestResults->sr_result_certification = "";
           $SessionTestResults->save();
        }


        $result_array = array();

        $result_array['is_error'] = 0;
        $result_array['error_msg'] = 'Test Result Saved Successfully';
        return Response()->json($result_array);
    }

    /**
     * Save Attendances for the Selected Session
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function SaveTraineeAttendances( Request $request )
    {
        $sc_sessions_class          = $request->input('sc_sessions_class');
        $class_id                   = $request->input('fk_class_id');
        $ck_trainee_attendance      = $request->input('ck_trainee_attendance');


        $ProjectClass   = ProjectClasses::find($class_id);
        $pcourse_id     = $ProjectClass->fk_pcourse_id;
        $ProjectCourseTrainees = ProjectCourseTrainees::whereFkPcourseId($pcourse_id)->get();

        $trainee_ids = array();
        foreach ($ProjectCourseTrainees as $ct_id => $ct_info)
        {
            $trainee_ids[] = $ct_info->fk_trainee_id;
        }

        $lst_trainees = Users::whereIn('id',$trainee_ids)->get();

        foreach ($lst_trainees as $index => $trainee_info)
        {
            $SessionAttendance = new SessionAttendance();
            $SessionAttendance->fk_session_id           = $sc_sessions_class;
            $SessionAttendance->fk_trainee_id           = $trainee_info->id;
            $SessionAttendance->sa_trainee_attendance   =  (in_array($trainee_info->id, $ck_trainee_attendance)) ? 1 : 0;
            $SessionAttendance->save();
        }

        $result_array = array();

        $result_array['is_error'] = 0;
        $result_array['error_msg'] = 'Attendance Saved Successfully';
        return Response()->json($result_array);
    }


    /**
     * Display Calendar for selected trainees in selected group
     * @param unknown $project_id
     * @param unknown $group_id
     * @param unknown $trainee_id
     */
    public function DisplayTraineeCalendar($project_id , $group_id , $trainee_id )
    {
        // get sessions for selected user in selected group

        $ClassSessionInfo = ProjectClasses::whereFkProjectId($project_id)->whereFkGroupId($group_id)->get();
        $class_id = 0;

        foreach ($ClassSessionInfo as $index => $class_info) {
            $class_id = $class_info->cls_id;
        }

          $sessions_info = DB::table('ses_session_class')->leftJoin('proj_classes','ses_session_class.fk_class_id','=','proj_classes.cls_id')
        ->leftJoin('proj_project_courses','proj_classes.fk_pcourse_id','=','proj_project_courses.pc_id')
        ->leftJoin('sys_courses','proj_project_courses.fk_course_id','=','sys_courses.c_id')->where('fk_class_id','=',$class_id)->get();

        // generate all events for this class


        $xml_session_data = "<data>";

        for ($i = 0; $i < count($sessions_info); $i++)
        {

        $session_title = $sessions_info[$i]->c_course_name;

        $start_session_date = $sessions_info[$i]->sc_session_date . " " . $sessions_info[$i]->sc_session_time;

        //$end_date = strtotime($sessions_info[$i]->sc_session_date . " " . $sessions_info[$i]->sc_session_time . " + " . $sessions_info[$i]->sc_session_duration . " hours");
        $time_array = explode(":", $sessions_info[$i]->sc_session_time);

        $duration = $sessions_info[$i]->sc_session_duration;

        $time = strtotime($sessions_info[$i]->sc_session_time) + $sessions_info[$i]->sc_session_duration * 3600;
        $time = date("H:i:s" , $time);
        $end_session_date = $sessions_info[$i]->sc_session_date . " " . $time;

        $xml_session_data .= "<event id='" . $sessions_info[$i]->sc_id . "'>\n";
        $xml_session_data .= "<start_date>" . date("Y-m-d H:i:s" , strtotime($start_session_date)) . "</start_date>\n";
        $xml_session_data .= "<end_date>" . date("Y-m-d H:i:s" , strtotime($end_session_date)) . "</end_date>\n";
            $xml_session_data .= "<text><![CDATA[ " . $session_title . " ]]></text>\n";
                $xml_session_data .= "<details><![CDATA[  " . $session_title . " ]]></details>\n";
                $xml_session_data .= "</event>\n";


        }
        $xml_session_data .= "</data>";

            $xml_path = public_path('resources/projects/calenders/' . $project_id . "/");

                if(!is_dir($xml_path))
            CreateDirectory($xml_path);
                    $filename = $xml_path . "sessions_" . $project_id . "_" . $trainee_id . ".xml";
        $fp = fopen($filename, "w+");
            fwrite($fp, $xml_session_data);
            fclose($fp);

       $xml_url = url('resources/projects/calenders/' . $project_id . "/sessions_" . $project_id . "_" . $trainee_id . ".xml");



        $data = array(
            'xml_url' => $xml_url
        );
        return Response()->view('sessions.mycalendar',$data);
    }
    /**
     * @description Open Session Page in providers account
     * @return Object View
     */
    public function SessionsManagement(){
        
        $projects = Projects::wherePpIsDeleted(0)->get();
        $data = array(
            "projects" => $projects
        );
        
        return Response()->view('sessions.sessions',$data);
    }
    /**
     * 
     * @param Request $request
     * @return json Response
     * @description Display Dropdown of groups according to the selected project
     */
    public function DisplayProjectGroups(Request $request){
        
        $result_array    = array();
        $is_error = 0;
        $project_id      = $request->input('project_id');
        $project_classes = ProjectClasses::whereFkProjectId($project_id)->whereClsIsDeleted(0)->get();
        
        if(count($project_classes) == 0){
            $is_error = 1;
        }
        $project_groups   = ProjectGroups::wherePgIsDeleted(0)->get();
        $project_groups   = CreateDatabaseArrayByIndex($project_groups , 'pg_id' );
        
        $data = array (
            "is_error"        => $is_error,
            "project_classes" => $project_classes,
            "project_groups"  => $project_groups
        );
        $result_array['display'] = view('sessions.groupsdropdown',$data)->render();
        return Response()->json($result_array);
    }
    /**
     * 
     * @param Request $request
     * @return json response(contains object view)
     * @description Display List of Sessions according to the selected class
     */
    public function DisplaySessions(Request $request) { 
        {/// initialise variable
             $result_array = array();
             $display_type = $request->input('display_type');
             $project_id   = $request->input('project_id');
             $class_id     = $request->input('class_id');
             $page_number  = $request->input('page_number');
             $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
             
             if ($page_number > 1)
                $skip = ( $page_number - 1 ) * $nbr_rows_per_pages;
            else
                $skip = 0;
        }
        switch ($display_type) {
            case 'list': {
                
                    $sessions_obj  = SessionClass::whereFkClassId($class_id);
                    
                    $sessions_count= $sessions_obj->count();
                    $total_pages   = ceil($sessions_count/$nbr_rows_per_pages);
                    
                    if ($total_pages > 1)
                        $sessions_info = $sessions_obj->skip($skip)->take($nbr_rows_per_pages)->get();
                    else
                        $sessions_info = $sessions_obj->get();

                    $trainers      = Users::whereUUserType(UserTypes::TRAINERS_USER_TYPE_ID)->whereUIsDeleted(0)->get();
                    $trainers      = CreateDatabaseArrayByIndex($trainers , 'id' );
                    $session_status= SessionStatus::whereSsIsDeleted(0)->get();
                    $session_status= CreateDatabaseArrayByIndex($session_status, 'ss_id');
                    


                    $view_name = "sessions.lst_sessions";
                    $data = array();
                    $data['session_status']  = $session_status;
                    $data['sessions_info']   = $sessions_info;
                    $data['trainers']        = $trainers;
                    $result_array['total_pages'] = $total_pages;
                    $result_array['display'] = view($view_name, $data)->render();
                }
                break;
            case 'calendar': {
                
                    $projectCourses = ProjectCourses::whereFkProjectId($project_id)->get();
                    $project_classes = ProjectClasses::whereFkProjectId($project_id)->get();

                    $class_ids = array();
                    foreach ($project_classes as $class_index => $class_info) {
                        $class_ids[] = $class_info->cls_id;
                    }

                    $course_ids = array();

                    foreach ($projectCourses as $pc_id => $pc_info) {
                        $course_ids[] = $pc_info->fk_course_id;
                    }

                    $course_info = DB::table('sys_courses')->whereIn('c_id', $course_ids)->get();
                    $sessions_info = DB::table('ses_session_class')->whereIn('fk_class_id', $class_ids)->get();


                    $view_name = "projects/config/sessions_configuration";
                    $data = array();
                    $data['course_info'] = $course_info;
                    $data['sessions_info'] = $sessions_info;
                    $result_array['display'] = view($view_name, $data)->render();
                }
                break;
        }
        
        return Response()->json($result_array);
    }
    /**
     * 
     * @return Object View
     * @description AddSession Popup
     */
    public function DisplayAddSession()
    {
        $code = md5(date("YmdHis"));// create a code for the session code
        $sc_session_code        = "SC-" . $code;
        $sessions_status_info   = SessionStatus::whereSsIsDeleted(0)->get();
        $list_session_types     = SessionType::whereStIsDeleted(0)->get();
        $trainer_info           = Users::whereUUserType(UserTypes::TRAINERS_USER_TYPE_ID)->whereUIsDeleted(0)->whereUIsActive(1)->get();
        $lst_location           = ProviderLocation::wherePlIsDeleted(0)->get();
        $data = array(
            "sessions_status_info" => $sessions_status_info,
            "trainer_info" => $trainer_info,
            "list_session_types" => $list_session_types,
            "lst_location" => $lst_location,
            "sc_session_code"=>$sc_session_code
        );
        return Response()->view('sessions.addsessioninformation',$data);
    }
    /**
     * 
     * @param Request $request
     * @return json Response
     * @description Handles the saving of add edit session in the providers account
     */
    public function SaveSession( Request $request )
    {
        $session_id             = $request->input('session_id');
        $class_id               = $request->input('class_id');
        $fk_session_status_id   = $request->input('fk_session_status_id');
        $sc_session_date        = $request->input('sc_session_date');
        $sc_session_time        = $request->input('sc_session_time');
        $sc_session_duration    = $request->input('sc_session_duration');
        $sc_session_location    = $request->input('sc_session_location');
        $sc_session_description = $request->input('sc_session_description');
        $sc_session_room_number = $request->input('sc_session_room_number');
        $fk_session_trainer_id  = $request->input('fk_session_trainer_id');
        $sc_session_type        = $request->input('sc_session_type');
        $sc_session_code        = $request->input('sc_session_code');
        
        $sessionClass = new SessionClass();

        if ($session_id > 0) {
            $sessionClass = SessionClass::find($session_id);
        }
        
        $sessionClass->fk_session_status_id = $fk_session_status_id;
        $sessionClass->sc_session_date = $sc_session_date;
        $sessionClass->sc_session_time = $sc_session_time;
        $sessionClass->sc_session_duration = $sc_session_duration;
        $sessionClass->sc_session_location = $sc_session_location;
        $sessionClass->sc_session_type = $sc_session_type;
        $sessionClass->sc_session_description = $sc_session_description;
        $sessionClass->sc_session_room_number = $sc_session_room_number;
        $sessionClass->fk_session_trainer_id = $fk_session_trainer_id;
        $sessionClass->fk_class_id = $class_id;
        $sessionClass->sc_session_code = $sc_session_code;
        $sessionClass->save();

        $result_array = array();
        $result_array['is_error'] = 0;
        $result_array['error_msg'] = "Operation Completed Successfully";


        return Response()->json($result_array);
    }
    /**
     * 
     * @param Request $request
     * @return json response
     * @description Delete the selected session and the rows where its id has been used
     */
    public function DeleteSession(Request $request){
        
        $session_id = $request->input('session_id');
        
        SessionTestResult::whereFkSessionId($session_id)->delete();
        SessionAttendance::whereFkSessionId($session_id)->delete();
        SessionClass::whereScId($session_id)->delete();
        
        $result_array['is_error'] = 0;
        $result_array['error_msg'] = "Session Deleted Successfully";
         return Response()->json($result_array);
    }
    /**
     * 
     * @param type $sc_id
     * @return object view
     * @description Open Page of trainees attendance for the selected session
     */
    public function TakeAttendance($sc_id) {
        
        $session = SessionClass::find($sc_id);
        $cls_id  = $session->fk_class_id;
        $class   = ProjectClasses::find($cls_id);
        
        $data    = array(
            "session" => $session,
            "class"   => $class
        );
        
        return Response()->view('sessions.viewtraineesattendance',$data);
        
    }
}




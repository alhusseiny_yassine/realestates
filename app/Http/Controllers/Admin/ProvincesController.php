<?php
/***********************************************************
ProvincesController.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 28, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/




namespace App\Http\Controllers\Admin;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\Users\Users;
use App\model\Users\UserTypes;
use App\model\System\Roles;
use App\model\System\Country;
use App\model\System\Beneficiaries;
use App\model\System\BeneficiaryType;
use App\model\System\Provinces;
use App\model\System\Districts;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;


class ProvincesController extends Controller
{

    public function ProvincesManagement()
    {
        $data = array();
        return Response()->view('provinces.provinces',$data);
    }


    public function DisplayListProvinces(Request $request)
    {
        $page_number        = $request->input('page_number');
        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;



        $all_provinces = Provinces::whereSpIsDeleted(0)->count();


        $total_pages = ceil($all_provinces/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);

        $provinces = Provinces::whereSpIsDeleted(0)->skip($skip)->take($nbr_rows_per_pages)->get();
        $data = array(
            "provinces" => $provinces
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("provinces.displaylist",$data)->render();

        return Response()->json($result_array);
    }


    public function AddProvinceForm()
    {

        $data = array();
        return view('provinces.addprovince',$data);
    }


    /**
     * Save Province Info to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     *
     * @return Response Json
     */
    public function SaveProvinceInfo(Request $request)
    {
        $sp_id              = $request->input('sp_id');
        $sp_provinces_name  = $request->input('sp_provinces_name');

        $result_array = array();

        $Province = new Provinces();
        if($sp_id !== null)
        {
            $Province = Provinces::find($sp_id);
        }

        $Province->sp_provinces_name   = $sp_provinces_name;
        $Province->save();


        $result_array['is_error']  = 0;
        $result_array['error_msg'] = 'Province Information Has been saved';

        return Response()->json($result_array);
    }



    /**
     * Display Edit Province Form Page
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $bt_id
     */
    public function EditProvinceForm( $sp_id )
    {

        $cur_province_info    = Provinces::find($sp_id);


        $data = array(
            "cur_province_info" => $cur_province_info
        );
        return view('provinces.editprovince',$data);
    }


    public function DeleteProvinceInfo(Request $request)
    {
        $sp_id = $request->input('sp_id');

        $Province = new Provinces();
        $Province = Provinces::find($sp_id);
        $Province->sp_is_deleted          = 1;
        $Province->sp_deleted_by          = Session('user_id');
        $Province->save();


        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return Response()->json($result_array);
    }

}
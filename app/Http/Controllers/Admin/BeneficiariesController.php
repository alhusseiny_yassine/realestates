<?php
/***********************************************************
BeneficiariesController.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 26, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

namespace App\Http\Controllers\Admin;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\Users\Users;
use App\model\Users\UserTypes;
use App\model\System\Roles;
use App\model\System\Country;
use App\model\System\Beneficiaries;
use App\model\System\BeneficiaryType;
use App\model\System\Provinces;
use App\model\System\Districts;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\model\System\BeneficiaryDepartments;
use App\model\System\BeneficiaryDirectorates;


class BeneficiariesController extends Controller
{

    public function BeneficiariesManagement()
    {
        $data = array();
        return Response()->view('beneficiaries.beneficiaries',$data);
    }


    public function DisplayListBeneficiaries(Request $request)
    { 
        
        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_beneficiary_management'])  && $role_info['om_beneficiary_management'] == 'deny') || ( !isset( $role_info['om_beneficiary_management'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }
        
        
        $page_number                    = $request->input('page_number');
        $sb_beneficiary_name            = $request->input('sb_beneficiary_name');
        $sb_beneficiary_arabic_name     = $request->input('sb_beneficiary_arabic_name');
        $sb_beneficiary_phone           = $request->input('sb_beneficiary_phone');
        $sb_beneficiary_fax             = $request->input('sb_beneficiary_fax');
 
        $lst_beneficiaries = Beneficiaries::whereSbIsDeleted(0)->get();
        
        $beneficiaries_array = array();
        
        foreach ($lst_beneficiaries as $index => $benf_info) {
            $beneficiaries_array[ $benf_info->sb_id ] = $benf_info->sb_name;
        }
        
        
        
        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;

        $beneficiaries_obj = Beneficiaries::whereSbIsDeleted(0);

        if(strlen($sb_beneficiary_name) > 0 )
        {
            $beneficiaries_obj = $beneficiaries_obj->where('sb_name','like','%' . $sb_beneficiary_name . '%');
        }

        if(strlen($sb_beneficiary_arabic_name) > 0 )
        {
            $beneficiaries_obj = $beneficiaries_obj->where('sb_arabic_name','like','%' . $sb_beneficiary_arabic_name . '%');
        }

        if(strlen($sb_beneficiary_phone) > 0 )
        {
            $beneficiaries_obj = $beneficiaries_obj->where('sb_phone','like','%' . $sb_beneficiary_phone . '%');
        }

        if(strlen($sb_beneficiary_fax) > 0 )
        {
            $beneficiaries_obj = $beneficiaries_obj->where('sb_fax','like','%' . $sb_beneficiary_fax . '%');
        }


        $all_beneficiaries = $beneficiaries_obj->count();


        $total_pages = ceil($all_beneficiaries/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);

        $beneficiaries = $beneficiaries_obj->skip($skip)->take($nbr_rows_per_pages)->get();
        $data = array(
            "beneficiaries" => $beneficiaries,
            "beneficiaries_array" => $beneficiaries_array
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("beneficiaries.displaylist",$data)->render();

        return Response()->json($result_array);
    }

    
    /**
     * get dropdown of all Diractorates in this  beneficiery
     * 
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function GetBeneficieryDiractorates( Request $request )
    {
        $beneficiary_id         = $request->input('beneficiary_id');
        $ini_benf_department    = $request->input('ini_benf_department');
        $beneficiaries_dirs = BeneficiaryDirectorates::whereFkBenfId($beneficiary_id)->whereBdIsDeleted(0)->get();
        $result_array = array();
        
        
        $result_array['is_error'] = 0;
        $data = array(
            'beneficiaries_dirs' => $beneficiaries_dirs,
            'ini_benf_department' => $ini_benf_department
        );
        $result_array['display'] = view('beneficiarydirs.dropdown',$data)->render();
        
        return Response()->json($result_array);
    }
    
    public function AddBeneficiaryForm()
    {
        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_add_new_beneficiary'])  && $role_info['om_add_new_beneficiary'] == 'deny') || ( !isset( $role_info['om_add_new_beneficiary'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }
        
        
        $lst_benf_types = BeneficiaryType::all();
        $lst_benf       = Beneficiaries::all();


        $data = array(
            "lst_benf_types" => $lst_benf_types,
            "lst_benf" => $lst_benf
        );
        return view('beneficiaries.addbeneficiaries',$data);
    }


    /**
     * Save User Info to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     *
     * @return Response Json
     */
    public function SaveBeneficiaryInfo(Request $request)
    {
        $sb_id              = $request->input('sb_id');
        $sb_code            = $request->input('sb_code');
        $sb_name            = $request->input('sb_name');
        $sb_arabic_name     = $request->input('sb_arabic_name');
        $sb_benf_type       = $request->input('sb_benf_type');
        $fk_benf_id         = $request->input('fk_benf_id');
        $sb_telephone       = $request->input('sb_telephone');
        $sb_fax             = $request->input('sb_fax');
        $result_array = array();

        $Benf = new Beneficiaries();
        if($sb_id !== null)
        {
            $Benf = Beneficiaries::find($sb_id);
        }

        $Benf->sb_code          = $sb_code;
        $Benf->sb_name          = $sb_name;
        $Benf->sb_arabic_name   = $sb_arabic_name;
        $Benf->sb_benf_type     = $sb_benf_type;
        $Benf->fk_benf_id       = $fk_benf_id;
        $Benf->sb_telephone     = $sb_telephone;
        $Benf->sb_fax           = $sb_fax;
        $Benf->save();


        $result_array['is_error']  = 0;
        $result_array['error_msg'] = 'Beneficiary Information Has been saved';

        return Response()->json($result_array);
    }



    /**
     * Display Edit Beneficiary Form Page
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $user_id
     */
    public function EditBeneficaryForm( $benf_id )
    {
        
        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_edit_existing_beneficiary'])  && $role_info['om_edit_existing_beneficiary'] == 'deny') || ( !isset( $role_info['om_edit_existing_beneficiary'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }

        $cur_ben_info    = Beneficiaries::find($benf_id);
        $lst_benf_types  = BeneficiaryType::all();
        $lst_benf        = Beneficiaries::all();


        $data = array(
            "cur_ben_info" => $cur_ben_info,
            "lst_benf_types" => $lst_benf_types,
            "lst_benf" => $lst_benf
        );
        return view('beneficiaries.editbeneficiaries',$data);
    }


    public function DeleteBeneficaryInfo(Request $request)
    {
        
        $benf_id = $request->input('benf_id');

        $Benf = new Beneficiaries();
        $Benf->find($benf_id);
        $Benf->sb_is_deleted          = 1;
        $Benf->sb_deleted_by          = Session('user_id');
        $Benf->save();


        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return Response()->json($result_array);
    }

}
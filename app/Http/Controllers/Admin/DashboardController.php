<?php
/***********************************************************
DashboardController.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 22, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad MantachCOPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/



namespace App\Http\Controllers\Admin;


use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\model\Users\Users;
use App\model\Users\UserTypes;
use App\model\Requests\Request As MRequests;
use App\model\Requests\RequestStatus;
use App\model\Project\Projects;
use App\model\Project\ProjectStatus;
use App\model\System\Beneficiaries;
use App\Libraries\Users\UserManager;
use App\Libraries\Requests\RequestsManager;
use App\model\Requests\RequestsCourses;
use App\model\System\Courses;


class DashboardController extends Controller
{

    public function AdminDashboardPanel()
    { 
        // get trainees , projects , requests
        $trainees_count = Users::whereUUserType(UserTypes::TRAINEES_USER_TYPE_ID)->whereUIsDeleted(0)->count();

        $approved_by_omsar_requests_count = MRequests::whereRIsDeleted(0)->whereFkStatusId(RequestStatus::REQUEST_STATUS_APPROVED_BY_OMSAR)->count();
        $approved_by_coordinator_requests_count = MRequests::whereRIsDeleted(0)->whereFkStatusId(RequestStatus::REQUEST_STATUS_APPROVED_BY_COORDINATOR)->count();
        $running_projects_count = Projects::wherePpIsDeleted(0)->whereFkProjectStatusId(ProjectStatus::PROJECT_STATUS_INPROGRESS)->count();

        $most_recent_trainees = Users::whereUUserType(UserTypes::TRAINEES_USER_TYPE_ID)->whereUIsDeleted(0)->orderBy('id', 'DESC')->take(5)->get();
        $most_recent_requests = MRequests::whereRIsDeleted(0)->whereFkStatusId(RequestStatus::REQUEST_STATUS_UNDER_REVIEW)->orderBy('r_id', 'DESC')->take(5)->get();

        $lst_users_data     = UserManager::getallUsersById();


        $lst_requests_array = array();
        foreach ($most_recent_requests as $index => $req_info) {
            $lst_requests_array[] = $req_info->r_id;
        }

        $RequestCourse = RequestsCourses::whereIn('fk_request_id',$lst_requests_array)->get();
        $course_ids = array(); //
        $request_courses_array = array();
        foreach ($RequestCourse as $index => $rc_info) {
            $course_ids[] = $rc_info->fk_course_id;
            $sys_course_info = Courses::find($rc_info->fk_course_id);

            $request_courses_array[ $rc_info->fk_request_id  ] = $sys_course_info->c_course_name;
        }



        $recent_users_data            = Users::whereUIsDeleted(0)->orderBy('id', 'desc')->take(3)->get();
        $beneficiaries                    = Beneficiaries::whereSbIsDeleted(0)->get();
        $benf_info                        = CreateDatabaseArrayByIndex($beneficiaries , 'sb_id');
        $request_statuses_byid      = RequestsManager::GetRequestStatusInfoById();
        $ProjectInfo                     = Projects::wherePpIsDeleted(0)->take(3)->get();
        $ProjectStatus                  = ProjectStatus::wherePsIsDeleted(0)->get();
        $ProjectStatus                  = CreateDatabaseArrayByIndex($ProjectStatus, 'ps_id' );
        
        $data = array(
            'trainees_count' => $trainees_count,
            'approved_by_omsar_count' => $approved_by_omsar_requests_count,
            'approved_by_coordinator_count' => $approved_by_coordinator_requests_count,
            'running_projects_count' => $running_projects_count,
            'benf_info' => $benf_info,
            'most_recent_requests' => $most_recent_requests,
            'most_recent_trainees' => $most_recent_trainees,
            'request_statuses_byid' => $request_statuses_byid,
            'request_courses_array' => $request_courses_array,
            'ProjectInfo' => $ProjectInfo,
            'ProjectStatus' => $ProjectStatus,
            'lst_users_data' => $lst_users_data,
            'recent_users_data' => $recent_users_data,
        );
        return Response()->view('dashboard.main',$data);
    }


    /**
     * Get list of requests order by
     * @param Request $request
     */
    public function GetRequestsDataInfo(Request $request)
    {
        $lst_requests = DB::table('req_request')->selectRaw('fk_status_id,COUNT(r_id) As `nbr_requests`')->groupBy('fk_status_id')->get();


        $lst_request_statuses = RequestStatus::whereRsIsDeleted(0)->get();
        $request_statuses_array = CreateDatabaseArrayByIndex($lst_request_statuses, 'rs_id');

        $result_array = array();

        for ($i = 0; $i < count($lst_requests); $i++)
        {
            $result_array[] = array(
                "nbr_requests" =>  $lst_requests[$i]->nbr_requests,
                "StatusName" =>  $request_statuses_array[ $lst_requests[$i]->fk_status_id ]->rs_status_name
            );
        }

        return Response()->json($result_array);
    }

}
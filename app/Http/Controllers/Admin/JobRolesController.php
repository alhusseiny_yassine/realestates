<?php
/***********************************************************
JobRolesController.php
Product :
Version : 1.0
Release : 2
Date Created : Mar 12, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Http\Controllers\Admin;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\Users\Users;
use App\model\Users\UserTypes;
use App\model\System\Roles;
use App\model\System\Country;
use App\model\System\Beneficiaries;
use App\model\System\BeneficiaryType;
use App\model\System\Provinces;
use App\model\System\Districts;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\model\System\JobRoles;



class JobRolesController extends Controller
{

    public function JobRolesManagement()
    {
        $data = array();
        return Response()->view('jobroles.jobroles',$data);
    }


    public function DisplayListJobRoles(Request $request)
    {

        /** $role_info = session()->get('role_info');
         if( ( isset($role_info['om_beneficiary_management'])  && $role_info['om_beneficiary_management'] == 'deny') || ( !isset( $role_info['om_beneficiary_management'] ) ) )
         {
         return Redirect::to('administrator/PermissionDenied');
         }*/


        $page_number                    = $request->input('page_number');





        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;

        $jobroles_obj = JobRoles::whereJrIsDeleted(0);




        $all_jobroles = $jobroles_obj->count();


        $total_pages = ceil($all_jobroles/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);

        $sys_jobroles = $jobroles_obj->skip($skip)->take($nbr_rows_per_pages)->get();
        $data = array(
            "jobroles" => $sys_jobroles,
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("jobroles.displaylist",$data)->render();

        return Response()->json($result_array);
    }




    public function AddJobRoleForm()
    {
        /**$role_info = session()->get('role_info');
         if( ( isset($role_info['om_add_new_beneficiary'])  && $role_info['om_add_new_beneficiary'] == 'deny') || ( !isset( $role_info['om_add_new_beneficiary'] ) ) )
         {
         return Redirect::to('administrator/PermissionDenied');
         }
         */

        $data = array();
        return view('jobroles.addjobrole',$data);
    }


    /**
     * Save Job Title Info to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     *
     * @return Response Json
     */
    public function SaveJobRoleInfo(Request $request)
    {
        $jr_id              = $request->input('jr_id');
        $jr_job_role       = $request->input('jr_job_role');

        $result_array = array();

        $JobRoles = new JobRoles();

        if(is_numeric($jr_id))
        {
            $JobRoles = JobRoles::find($jr_id);
        }

        $JobRoles->jr_job_role          = $jr_job_role;
        $JobRoles->save();


        $result_array['is_error']  = 0;
        $result_array['error_msg'] = 'Job Role Information Has been saved';

        return Response()->json($result_array);
    }



    /**
     * Display Edit Job Role Form Page
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $user_id
     */
    public function EditJobRoleForm( $jr_id )
    {

        /**$role_info = session()->get('role_info');
         if( ( isset($role_info['om_edit_existing_beneficiary'])  && $role_info['om_edit_existing_beneficiary'] == 'deny') || ( !isset( $role_info['om_edit_existing_beneficiary'] ) ) )
         {
         return Redirect::to('administrator/PermissionDenied');
         }*/

        $JobRole_info    = JobRoles::find($jr_id);


        $data = array(
            "JobRole_info" => $JobRole_info
        );
        return view('jobroles.editjobrole',$data);
    }


    public function DeleteJobRoleInfo(Request $request)
    {

        $jr_id = $request->input('jr_id');

        $JobRole = new JobRoles();
        $JobRole = JobRoles::find($jr_id);
        $JobRole->jr_is_deleted          = 1;
        $JobRole->jr_deleted_by          = Session('user_id');
        $JobRole->save();


        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return Response()->json($result_array);
    }

}
<?php
/***********************************************************
TasksListController.php
Product :
Version : 1.0
Release : 2
Date Created : Mar 23, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/





namespace App\Http\Controllers\Admin;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\Users\Users;
use App\model\Users\UserTypes;
use App\model\System\Roles;
use App\model\System\Country;
use App\model\System\Beneficiaries;
use App\model\System\BeneficiaryType;
use App\model\System\Provinces;
use App\model\System\Districts;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\model\System\BeneficiaryDepartments;
use App\model\System\BeneficiaryDirectorates;
use App\model\System\TasksList;


class TasksListController extends Controller
{

    public function TasksListManagement()
    {
        $data = array();
        return Response()->view('taskslist.taskslist',$data);
    }


    public function DisplayListTasksList(Request $request)
    {

       /** $role_info = session()->get('role_info');
        if( ( isset($role_info['om_beneficiary_management'])  && $role_info['om_beneficiary_management'] == 'deny') || ( !isset( $role_info['om_beneficiary_management'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }*/


        $page_number                    = $request->input('page_number');





        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;

        $taskslist_obj = TasksList::whereTlIsDeleted(0);




        $all_taskslist = $taskslist_obj->count();


        $total_pages = ceil($all_taskslist/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);

        $sys_taskslist = $taskslist_obj->skip($skip)->take($nbr_rows_per_pages)->get();
        $data = array(
            "taskslist" => $sys_taskslist,
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("taskslist.displaylist",$data)->render();

        return Response()->json($result_array);
    }




    public function AddTasksListForm()
    {
       /**$role_info = session()->get('role_info');
        if( ( isset($role_info['om_add_new_beneficiary'])  && $role_info['om_add_new_beneficiary'] == 'deny') || ( !isset( $role_info['om_add_new_beneficiary'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }
        */

        $data = array();
        return view('taskslist.addtasklist',$data);
    }


    /**
     * Save Task List Info to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     *
     * @return Response Json
     */
    public function SaveTasksListInfo(Request $request)
    {
        $tl_id              = $request->input('tl_id');
        $tl_task_title       = $request->input('tl_task_title');
        $tl_task_description       = $request->input('tl_task_description');

        $result_array = array();

        $TasksList = new TasksList();

        if(is_numeric($tl_id))
        {
            $TasksList = TasksList::find($tl_id);
        }

        $TasksList->tl_task_title          = $tl_task_title;
        $TasksList->tl_task_description          = $tl_task_description;
        $TasksList->save();


        $result_array['is_error']  = 0;
        $result_array['error_msg'] = 'Task List Information Has been saved';

        return Response()->json($result_array);
    }



    /**
     * Display Edit Task List Form Page
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $user_id
     */
    public function EditTaskListForm( $tl_id )
    {

        /**$role_info = session()->get('role_info');
        if( ( isset($role_info['om_edit_existing_beneficiary'])  && $role_info['om_edit_existing_beneficiary'] == 'deny') || ( !isset( $role_info['om_edit_existing_beneficiary'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }*/

        $taskslist_info    = TasksList::find($tl_id);


        $data = array(
            "taskslist_info" => $taskslist_info
        );
        return view('taskslist.edittasklist',$data);
    }


    public function DeleteTaskListInfo(Request $request)
    {

        $tl_id = $request->input('tl_id');

        $TasksList = new TasksList();
        $TasksList = TasksList::find($tl_id);
        $TasksList->tl_is_deleted          = 1;
        $TasksList->tl_deleted_by          = Session('user_id');
        $TasksList->save();


        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return Response()->json($result_array);
    }

}
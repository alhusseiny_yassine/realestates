<?php
/***********************************************************
BeneficiaryDirectorateController.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 26, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/



namespace App\Http\Controllers\Admin;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\Users\Users;
use App\model\Users\UserTypes;
use App\model\System\Roles;
use App\model\System\Country;
use App\model\System\Beneficiaries;
use App\libraries\System\BeneficiaryManagement;
use App\model\System\Provinces;
use App\model\System\Districts;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\model\System\BeneficiaryDirectorates;


class BeneficiaryDirectorateController extends Controller
{

    public function BeneficiaryDirectorateManagement()
    {
        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_beneficiary_departments'])  && $role_info['om_beneficiary_departments'] == 'deny') || ( !isset( $role_info['om_beneficiary_departments'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }
        
        $data = array();
        return Response()->view('beneficiarydirs.beneficiarydirs',$data);
    }


    public function DisplayListBeneficiaryDirectorates(Request $request)
    {
        $page_number        = $request->input('page_number');
        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;

        $bnf_result_array = BeneficiaryManagement::getBeneficiariesById();

        $all_beneficiary_deps = BeneficiaryDirectorates::whereBdIsDeleted(0)->count();


        $total_pages = ceil($all_beneficiary_deps/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);

        $beneficiary_dir = BeneficiaryDirectorates::whereBdIsDeleted(0)->skip($skip)->take($nbr_rows_per_pages)->get();
        $data = array(
            "beneficiary_dir" => $beneficiary_dir,
            "bnf_result_array" => $bnf_result_array
        );

        $result_array = array();

        $result_array['total_pages']    = $total_pages;
        $result_array['display']        = view("beneficiarydirs.displaylist",$data)->render();

        return Response()->json($result_array);
    }


    public function AddBeneficiaryDirectorateForm()
    {
        
        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_add_beneficiary_department'])  && $role_info['om_add_beneficiary_department'] == 'deny') || ( !isset( $role_info['om_add_beneficiary_department'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }
        
        $all_benf = Beneficiaries::whereSbIsDeleted(0)->get();


        $data = array(
            'all_benf' => $all_benf
        );
        return Response()->view('beneficiarydirs.addbeneficiarydir',$data);
    }


    /**
     * Save Beneficiary Department Info to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     *
     * @return Response Json
     */
    public function SaveBeneficiaryDirectorateInfo(Request $request)
    {
        $bd_id                      = $request->input('bd_id');
        $bd_directorate_name         = $request->input('bd_directorate_name');
        $fk_beneficiary_id          = $request->input('fk_beneficiary_id');

        $result_array = array();

        $BenfDir = new BeneficiaryDirectorates();
        if($bd_id !== null)
        {
            $BenfDir = BeneficiaryDirectorates::find($bd_id);
        }

        $BenfDir->fk_benf_id            = $fk_beneficiary_id;
        $BenfDir->bd_directorate_name    = $bd_directorate_name;
        $BenfDir->save();


        $result_array['is_error']  = 0;
        $result_array['error_msg'] = 'Beneficiary Directorate Information Has been saved';

        return Response()->json($result_array);
    }



    /**
     * Display Edit Beneficiary Department Form Page
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $bt_id
     */
    public function EditBeneficiaryDirForm( $bd_id )
    {
        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_edit_beneficiary_department'])  && $role_info['om_edit_beneficiary_department'] == 'deny') || ( !isset( $role_info['om_edit_beneficiary_department'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }
        
        $cur_bendir_info    = BeneficiaryDirectorates::find($bd_id);
        $all_benf           = Beneficiaries::whereSbIsDeleted(0)->get();
 
        $data = array(
            "cur_bendir_info" => $cur_bendir_info,
            "all_benf" => $all_benf,
        );
        return view('beneficiarydirs.editbeneficiarydir',$data);
    }




    public function DeleteBeneficaryDirInfo(Request $request)
    {
        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_delete_beneficiary_department'])  && $role_info['om_delete_beneficiary_department'] == 'deny') || ( !isset( $role_info['om_delete_beneficiary_department'] ) ) )
        {
            $result_array['is_error']   = 1;
            $result_array['error_msg']  = "You Don't Have Privilege to Delete Beneficiary Department";
            
            return Response()->json($result_array);
        }
        
        $bd_id = $request->input('bd_id');

        $BenfDir = new BeneficiaryDirectorates();
        $BenfDir = BeneficiaryDirectorates::find($bd_id);
        $BenfDir->bd_is_deleted          = 1;
        $BenfDir->bd_deleted_by          = Session('user_id');
        $BenfDir->save();


        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return Response()->json($result_array);
    }

}
<?php
/***********************************************************
DonorsController.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 30, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
Controller to control the sys_donor table in the database
***********************************************************/



namespace App\Http\Controllers\Admin;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\Users\Users;
use App\model\Users\UserTypes;
use App\model\System\Donor;
use App\model\System\Roles;
use App\model\System\Country;
use App\model\System\Beneficiaries;
use App\model\System\BeneficiaryType;
use App\model\System\Provinces;
use App\model\System\Districts;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;


class DonorsController extends Controller
{

    public function DonorsManagement()
    {
        $data = array();
        return Response()->view('donors.donors',$data);
    }


    public function DisplayListDonors(Request $request)
    {
        $page_number        = $request->input('page_number');
        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;



        $all_donors = Donor::whereDIsDeleted(0)->count();


        $total_pages = ceil($all_donors/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);

        $donors = Donor::whereDIsDeleted(0)->skip($skip)->take($nbr_rows_per_pages)->get();
        $data = array(
            "donors" => $donors
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("donors.displaylist",$data)->render();

        return Response()->json($result_array);
    }


    public function AddDonorForm()
    {
        $countries = Country::all();
        $data = array(
            "countries" => $countries
        );
        return view('donors.adddonor',$data);
    }


    /**
     * Save Donor Info to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     *
     * @return Response Json
     */
    public function SaveDonorInfo(Request $request)
    {
        $d_id               = $request->input('d_id');
        $fk_country_id      = $request->input('fk_country_id');
        $d_donor_name       = $request->input('d_donor_name');
        $d_nbr_project      = $request->input('d_nbr_project');
        $d_donor_about      = $request->input('d_donor_about');
        $d_telephone        = $request->input('d_telephone');
        $d_email            = $request->input('d_email');
        $d_fax              = $request->input('d_fax');
        $d_mobile           = $request->input('d_mobile');

        $result_array = array();

        $module = new Donor();
        if($d_id !== null)
        {
            $module = Donor::find($d_id);
        }

        $module->d_donor_name           = $d_donor_name;
        if($d_nbr_project > 0 && $d_nbr_project !== null )
            $module->d_nbr_project      = $d_nbr_project;
        $module->fk_country_id          = $fk_country_id;
        $module->d_donor_about          = $d_donor_about;
        $module->d_telephone            = $d_telephone;
        $module->d_email                = $d_email;
        $module->d_fax                  = $d_fax;
        $module->d_mobile               = $d_mobile;
        $module->save();


        $result_array['is_error']  = 0;
        $result_array['error_msg'] = 'Donor Information Has been saved';

        return Response()->json($result_array);
    }



    /**
     * Display Edit Donor Form Page
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $bt_id
     */
    public function EditDonorForm( $bt_id )
    {
        $countries = Country::all();
        $cur_donor_info    = Donor::find($bt_id);


        $data = array(
            "cur_donor_info" => $cur_donor_info,
            "countries" => $countries
        );
        return view('donors.editdonors',$data);
    }


    public function DeleteDonorInfo(Request $request)
    {
        $d_id = $request->input('d_id');

        $Donor = new Donor();
        $Donor = Donor::find($d_id);
        $Donor->d_is_deleted          = 1;
        $Donor->d_deleted_by          = Session('user_id');
        $Donor->save();


        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return Response()->json($result_array);
    }

}
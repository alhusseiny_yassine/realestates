<?php
/***********************************************************
JobTitlesController.php
Product :
Version : 1.0
Release : 2
Date Created : Mar 21, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/



namespace App\Http\Controllers\Admin;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\Users\Users;
use App\model\Users\UserTypes;
use App\model\System\Roles;
use App\model\System\Country;
use App\model\System\Beneficiaries;
use App\model\System\BeneficiaryType;
use App\model\System\Provinces;
use App\model\System\Districts;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\model\System\BeneficiaryDepartments;
use App\model\System\BeneficiaryDirectorates;
use App\model\System\JobTitles;


class JobTitlesController extends Controller
{

    public function JobTitlesManagement()
    {
        $data = array();
        return Response()->view('jobtitles.jobtitles',$data);
    }


    public function DisplayListJobtitles(Request $request)
    {

       /** $role_info = session()->get('role_info');
        if( ( isset($role_info['om_beneficiary_management'])  && $role_info['om_beneficiary_management'] == 'deny') || ( !isset( $role_info['om_beneficiary_management'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }*/


        $page_number                    = $request->input('page_number');





        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;

        $jobtitles_obj = JobTitles::whereJtIsDeleted(0);




        $all_jobtitles = $jobtitles_obj->count();


        $total_pages = ceil($all_jobtitles/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);

        $sys_jobtitles = $jobtitles_obj->skip($skip)->take($nbr_rows_per_pages)->get();
        $data = array(
            "jobtitles" => $sys_jobtitles,
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("jobtitles.displaylist",$data)->render();

        return Response()->json($result_array);
    }




    public function AddJobTitlesForm()
    {
       /**$role_info = session()->get('role_info');
        if( ( isset($role_info['om_add_new_beneficiary'])  && $role_info['om_add_new_beneficiary'] == 'deny') || ( !isset( $role_info['om_add_new_beneficiary'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }
        */

        $data = array();
        return view('jobtitles.addjobtitle',$data);
    }


    /**
     * Save Job Title Info to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     *
     * @return Response Json
     */
    public function SaveJobTitleInfo(Request $request)
    {
        $jt_id              = $request->input('jt_id');
        $jt_job_title       = $request->input('jt_job_title');

        $result_array = array();

        $JobTitles = new JobTitles();

        if(is_numeric($jt_id))
        {
            $JobTitles = JobTitles::find($jt_id);
        }

        $JobTitles->jt_job_title          = $jt_job_title;
        $JobTitles->save();


        $result_array['is_error']  = 0;
        $result_array['error_msg'] = 'Job Title Information Has been saved';

        return Response()->json($result_array);
    }



    /**
     * Display Edit Job Title Form Page
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $user_id
     */
    public function EditJobTitleForm( $jt_id )
    {

        /**$role_info = session()->get('role_info');
        if( ( isset($role_info['om_edit_existing_beneficiary'])  && $role_info['om_edit_existing_beneficiary'] == 'deny') || ( !isset( $role_info['om_edit_existing_beneficiary'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }*/

        $JobTitle_info    = JobTitles::find($jt_id);


        $data = array(
            "JobTitle_info" => $JobTitle_info
        );
        return view('jobtitles.editjobtitle',$data);
    }


    public function DeleteJobTitleInfo(Request $request)
    {

        $jt_id = $request->input('jt_id');

        $JobTitle = new JobTitles();
        $JobTitle = JobTitles::find($jt_id);
        $JobTitle->jt_is_deleted          = 1;
        $JobTitle->jt_deleted_by          = Session('user_id');
        $JobTitle->save();


        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return Response()->json($result_array);
    }

}
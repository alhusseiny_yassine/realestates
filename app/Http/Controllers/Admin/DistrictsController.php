<?php
/***********************************************************
DistrictsController.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 28, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/





namespace App\Http\Controllers\Admin;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\Users\Users;
use App\model\Users\UserTypes;
use App\model\System\Roles;
use App\model\System\Country;
use App\model\System\Beneficiaries;
use App\model\System\BeneficiaryType;
use App\Libraries\System\ProvinceManagement;
use App\model\System\Provinces;
use App\model\System\Districts;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;


class DistrictsController extends Controller
{

    public function DistrictsManagement()
    {
        $provinces = Provinces::whereSpIsDeleted(0)->get();


        $data = array(
          "provinces" => $provinces
        );
        return Response()->view('districts.districts',$data);
    }


    public function DisplayListDistricts(Request $request)
    {
        $page_number        = $request->input('page_number');
        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;



        $all_districts = Districts::whereSdIsDeleted(0)->count();


        $total_pages = ceil($all_districts/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);

        $districts = Districts::whereSdIsDeleted(0)->skip($skip)->take($nbr_rows_per_pages)->get();
        $provinces = ProvinceManagement::getProvincesById();
        $data = array(
            "districts" => $districts,
            "provinces" => $provinces,
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("districts.displaylist",$data)->render();

        return Response()->json($result_array);
    }


    public function AddDistrictForm()
    {
        $provinces = Provinces::whereSpIsDeleted(0)->get();
        $data = array(
            "provinces" => $provinces
        );
        return view('districts.adddistrict',$data);
    }


    /**
     * Save District Info to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     *
     * @return Response Json
     */
    public function SaveDistrictInfo(Request $request)
    {
        $sd_id              = $request->input('sd_id');
        $fk_province_id     = $request->input('fk_province_id');
        $sd_district_name   = $request->input('sd_district_name');

        $result_array = array();

        $Districts = new Districts();
        if($sd_id !== null)
        {
            $Districts = Districts::find($sd_id);
        }

        $Districts->fk_province_id      = $fk_province_id;
        $Districts->sd_district_name    = $sd_district_name;
        $Districts->save();


        $result_array['is_error']  = 0;
        $result_array['error_msg'] = 'District Information Has been saved';

        return Response()->json($result_array);
    }



    /**
     * Display Edit District Form Page
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $sd_id
     */
    public function EditDistrictForm( $sd_id )
    {

        $cur_district_info    = Districts::find($sd_id);
        $provinces = Provinces::whereSpIsDeleted(0)->get();

        $data = array(
            "cur_district_info" => $cur_district_info,
            "provinces" => $provinces,
        );
        return view('districts.editdistrict',$data);
    }


    public function DeleteDistrictInfo(Request $request)
    {
        $sd_id = $request->input('sd_id');

        $District = new Districts();
        $District = Districts::find($sd_id);
        $District->sd_is_deleted          = 1;
        $District->sd_deleted_by          = Session('user_id');
        $District->save();


        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return Response()->json($result_array);
    }

}
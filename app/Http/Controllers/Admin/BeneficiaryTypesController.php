<?php
/***********************************************************
BeneficiaryTypesController.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 27, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Http\Controllers\Admin;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\Users\Users;
use App\model\Users\UserTypes;
use App\model\System\Roles;
use App\model\System\Country;
use App\model\System\Beneficiaries;
use App\model\System\BeneficiaryType;
use App\model\System\Provinces;
use App\model\System\Districts;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;


class BeneficiaryTypesController extends Controller
{

    public function BeneficiaryTypesManagement()
    {
        $data = array();
        return Response()->view('beneficiarytypes.beneficiarytypes',$data);
    }


    public function DisplayListBeneficiaryTypes(Request $request)
    {
        $page_number        = $request->input('page_number');
        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;



        $all_beneficiary_types = BeneficiaryType::whereBtIsDeleted(0)->count();


        $total_pages = ceil($all_beneficiary_types/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);

        $beneficiary_types = BeneficiaryType::whereBtIsDeleted(0)->skip($skip)->take($nbr_rows_per_pages)->get();
        $data = array(
            "beneficiary_types" => $beneficiary_types
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("beneficiarytypes.displaylist",$data)->render();

        return Response()->json($result_array);
    }


    public function AddBeneficiaryTypeForm()
    {

        $data = array();
        return view('beneficiarytypes.addbeneficiaryType',$data);
    }


    /**
     * Save BeneficiaryType Info to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     *
     * @return Response Json
     */
    public function SaveBeneficiaryTypeInfo(Request $request)
    {
        $bt_id              = $request->input('bt_id');
        $bt_name            = $request->input('bt_name');
        $bt_description     = $request->input('bt_description');

        $result_array = array();

        $BenfType = new BeneficiaryType();
        if($bt_id !== null)
        {
            $BenfType = BeneficiaryType::find($bt_id);
        }

        $BenfType->bt_name          = $bt_name;
        $BenfType->bt_description   = $bt_description;
        $BenfType->save();


        $result_array['is_error']  = 0;
        $result_array['error_msg'] = 'Beneficiary Type Information Has been saved';

        return Response()->json($result_array);
    }



    /**
     * Display Edit Beneficiary Type Form Page
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $bt_id
     */
    public function EditBeneficiaryTypeForm( $bt_id )
    {

        $cur_benType_info    = BeneficiaryType::find($bt_id);


        $data = array(
            "cur_benType_info" => $cur_benType_info
        );
        return view('beneficiarytypes.editbeneficiarytypes',$data);
    }


    public function DeleteBeneficaryTypeInfo(Request $request)
    {
        $bt_id = $request->input('bt_id');

        $BenfType = new BeneficiaryType();
        $BenfType = BeneficiaryType::find($bt_id);
        $BenfType->bt_is_deleted          = 1;
        $BenfType->bt_deleted_by          = Session('user_id');
        $BenfType->save();


        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return Response()->json($result_array);
    }

}
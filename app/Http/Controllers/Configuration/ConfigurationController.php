<?php
/***********************************************************
ConfigurationController.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 26, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Http\Controllers\Configuration;

use App\User;
use Validator;
use Input;
use Request;
use Session;
use Config;
use Redirect;
use File;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;


class ConfigurationController extends Controller
{

    public function index()
    {
        
        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_system_configuration_management'])  && $role_info['om_system_configuration_management'] == 'deny') || ( !isset( $role_info['om_system_configuration_management'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }
        
        
        $sys_configurations = DB::table('sys_appconfig')->orderBy('sa_id', 'ASC')->get();
        $data = array(
            "sys_configurations" => $sys_configurations
        );
        return view('system.listconfigurations',$data);
    }


    /**
     * Save Configuration to the database and to the config file
     */
    public function AjaxSaveConfiguration()
    {
        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_generate_system_configuration'])  && $role_info['om_generate_system_configuration'] == 'deny') || ( !isset( $role_info['om_generate_system_configuration'] ) ) )
        {
            $result_array['is_error'] = 0;
            $result_array['error_msg'] = "Permission Denied";
            
            return $result_array;
        }
        
        
        $sa_id                  = $_POST['sa_id'];
        $sa_config_value        = $_POST['sa_config_value'];
        $sa_config_description  = $_POST['sa_config_description'];
        $sa_config_index        = $_POST['sa_config_index'];

        $str_save = "<?php \n return [ \n\n\t";
        for ($i = 0; $i < count($sa_id); $i++)
        {
            $fields_array = array(
                'sa_config_value' => $sa_config_value[$i],
                'sa_config_description' => $sa_config_description[$i]
            );
            $config_file[ $sa_config_index[$i] ] = $sa_config_value[$i];
            DB::table('sys_appconfig')->where('sa_id', $sa_id[$i])->update($fields_array);
            $str_save .= '"' . $sa_config_index[$i] . '"=>"' . $sa_config_value[$i] . '"';
            if($i < count($sa_id) - 1)
            {
                $str_save .= ", \n\t ";
            }
        }
        $str_save .= " \n ]; \n ?>";

        $dir_config_path = config_path() . "\appconfig.php";
        $fp = fopen($dir_config_path,"w");
        fwrite($fp,$str_save);
        fclose($fp);

        $result_array = array();
        $result_array['is_error'] = 0;
        $result_array['error_msg'] = "Operation Complete Succesfully";

        return $result_array;

    }
}
<?php
/***********************************************************
CompanyDetailsController.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 27, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Http\Controllers\Configuration;

use App\User;
use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Cookie;
use Session;
use Config;
use Redirect;
use File;
use DB;
use App\Http\Controllers\Controller;
use App\Models\SYSTEM\SYSTEMCompanyDetails;
use Illuminate\Support\Facades\Hash;
use App\Models\SYSTEM\SYSTEMCountries;
use App\Models\SYSTEM\City;

class CompanyDetailsController extends Controller
{


    public function index()
    {
        $sys_company_details        = SYSTEMCompanyDetails::all();


        $companyLogoLink =url("/assets/imgs/NOLOGO.png");
        if(count($sys_company_details) > 0 )
        {
            $directory = public_path(). "/" . Config::get( 'constants.COMPANY_LOGO_IMAGE_PATH') . $sys_company_details[0]->cd_company_logo_base_src . $sys_company_details[0]->cd_company_logo_filename . "." . $sys_company_details[0]->cd_company_logo_extention ;

            if(is_file($directory))
            {
                $companyLogoLink = url( Config::get( 'constants.COMPANY_LOGO_IMAGE_PATH') . $sys_company_details[0]->cd_company_logo_base_src . $sys_company_details[0]->cd_company_logo_filename  . "." . $sys_company_details[0]->cd_company_logo_extention ) ;
            }
        }



        $lstCmsCountries            = SYSTEMCountries::all();
        $lstCmsCities               = City::all();



        $data = array(
            "sys_company_details" => $sys_company_details,
            "lstCmsCountries" => $lstCmsCountries,
            "lstCmsCities" => $lstCmsCities,
            "companyLogoLink" => $companyLogoLink
        );
        return view('administrator.companydetails',$data);
    }


    /**
     * change cookies information already saved with new modification data
     *
     * @author Moe mantach
     * @access public
     *
     * @return boolean
     */
    private function ChangeCookiesCompanyInfo( $company_information = array() )
    {
        // if we didn't pass the information of the company we get from the database
        if(count($company_information)  == 0)
        {
            $sys_company_info       = SYSTEMCompanyDetails::find(1);
            $company_information    = $sys_company_info['attributes'];
        }

        $cookies_array = array();

        $cookies_array['sw_company_name'] =  $company_information['cd_company_name'];
        $cookies_array['sw_company_website'] = $company_information['cd_company_website'];
        $cookies_array['sw_company_email' ] = $company_information['cd_company_email'];
        $cookies_array['sw_comapny_phone' ] = $company_information['cd_comapny_phone'];
        $cookies_array['sw_company_mobile'] = $company_information['cd_company_mobile'];
        $cookies_array['sw_fk_country_id'] = $company_information['fk_country_id'];

        $image_url = url( Config::get( 'constants.COMPANY_LOGO_IMAGE_PATH') . $company_information['cd_company_logo_base_src'] . "" . $company_information['cd_company_logo_filename'] . "." . $company_information['cd_company_logo_extention'] );

        $cookies_array['sw_image_url'] = $image_url;

        return $cookies_array;
    }


    /**
     * Save Configuration to the database and to the config file
     *
     * @author Moe Mantach
     * @access public
     *
     * @return Json response
     */
    public function SaveCompanyDetails(Request $request)
    {
        $cd_id                  = $request->input('cd_id');
        $cd_company_name        = $request->input('cd_company_name');
        $cd_company_brief       = $request->input('cd_company_brief');
        $cd_company_website     = $request->input('cd_company_website');
        $cd_company_email       = $request->input('cd_company_email');
        $cd_comapny_phone       = $request->input('cd_comapny_phone');
        $cd_company_mobile      = $request->input('cd_company_mobile');
        $fk_country_id          = $request->input('fk_country_id');

        $cookies_array = $this->ChangeCookiesCompanyInfo();
        $fields_array = array(
            'cd_company_name' => $cd_company_name,
            'cd_company_brief' => $cd_company_brief,
            'cd_company_website' => $cd_company_website,
            'cd_company_email' => $cd_company_email,
            'cd_comapny_phone' => $cd_comapny_phone,
            'cd_company_mobile' => $cd_company_mobile,
            'fk_country_id' => $fk_country_id
        );

        $res = DB::table('sys_company_details')->where('cd_id', $cd_id)->update($fields_array);

        $result_array = array();

        $result_array['is_error'] = 0;
        $result_array['error_msg'] = "Operation Complete Successfully";
        $res = response()->json($result_array);
        foreach ( $cookies_array as $cookie_key => $cookie_value )
        {
            $res->withCookie( cookie()->forever($cookie_key, $cookie_value) );
        }
        return $res;
    }
}
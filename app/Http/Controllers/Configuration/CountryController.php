<?php
/***********************************************************
CountryController.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 23, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/



namespace App\Http\Controllers\Configuration;

use App\User;
use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use DB;
use App\Http\Controllers\Controller;
use App\model\System\Country;
use Illuminate\Support\Facades\Hash;


class CountryController extends Controller
{


    public function index()
    {
        return Response()->view('countries.countries',array());
    }

    /**
     * Display for for add a new country
     *
     * @author Moe Mantach
     * @access public
     * @return Ambigous <\Illuminate\View\View, mixed, \Illuminate\Foundation\Application, \Illuminate\Container\static>
     */
    public function AddNewCountryForm()
    {
        return Response()->view('countries.addcountry',array());
    }


    /**
     * Display form
     * @param unknown $id
     * @return Ambigous <\Illuminate\View\View, mixed, \Illuminate\Foundation\Application, \Illuminate\Container\static>
     */
    public function EditCountryForm( $country_id )
    {
      $countries = Country::find( $country_id );
       return Response()->view('countries.editcountry',array("countries" =>$countries , 'country_id' => $country_id));
    }

    /**
     * Display list of roles saved in the database
     *
     * @author Moe Mantach
     * @access public
     *
     * @return Array $result_array
     */
    public function DisplayListCountries(Request $request)
    {
        $page_number        = $request->input('page_number');
        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;
        
        
        
        $all_countries = Country::count();
        
        
        $total_pages = ceil($all_countries/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);
        
        $countries = Country::skip($skip)->take($nbr_rows_per_pages)->get();
        $data = array(
            "countries" => $countries
        );
        
        $result_array = array();
        
        $result_array['total_pages']    = $total_pages;
        $result_array['display']        = view("countries.displaylist",$data)->render();
        
        return Response()->json($result_array);
    }

    
    
    public function SaveCountryInfo(Request $request)
    {
        $id         = $request->input('id');
        $code       = $request->input('code');
        $name       = $request->input('name');
        
        $Country = New Country();
        if(isset($id))
        {
            $Country = Country::find($id);
        }
        $Country->code = $code;
        $Country->name = $name;
        $Country->save();
        
        
        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";
        
        return Response()->json($result_array);
        
    }
    
    
    /**
     * function to delete role from the database
     *
     * @author Moe Mantach
     * @access public
     *
     * @return Array $result_array
     */
    public function DeleteCountryInfo(Request $request)
    {
        $id = $request->input('country_id');
        
         $country = Country::find($id);
         $country->delete();

        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return Response()->json($result_array);
    }

}

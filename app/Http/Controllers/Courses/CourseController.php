<?php
/***********************************************************
CourseController.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 27, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Http\Controllers\Courses;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\System\Roles;
use App\model\System\Courses;
use App\model\System\CourseType;
use App\model\System\Days;
use App\Libraries\Courses\CourseManagement;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\model\System\CourseCategories;
use App\model\Project\ProjectCourses;
use App\model\Project\ProjectCourseStatus;
use App\model\Project\ProjectCourseTrainees;


class CourseController extends Controller
{

    public function CourseManagement()
    {

        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_course_management_tab'])  && $role_info['om_course_management_tab'] == 'deny') || ( !isset( $role_info['om_course_management_tab'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }

        $course_types = CourseType::whereCtIsDeleted(0)->get();
        //$course_categories_data  = CourseCategories::whereCcIsDeleted(0)->get();

        $data = array(
            "course_types" => $course_types
        );
        return Response()->view('course.course',$data);
    }


    public function ReportListCourses()
    {

        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_course_management_tab'])  && $role_info['om_course_management_tab'] == 'deny') || ( !isset( $role_info['om_course_management_tab'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }

        $course_types = CourseType::whereCtIsDeleted(0)->get();
        //$course_categories_data  = CourseCategories::whereCcIsDeleted(0)->get();

        $data = array(
            "course_types" => $course_types
        );
        return Response()->view('reports.course',$data);
    }





    public  function CourseCategoriesDropDown(Request $request)
    {
        $course_type = $request->input('course_type');
        $course_categories_data  = CourseCategories::whereCcIsDeleted(0)->whereFkCourseType($course_type)->get();

        $result_array = array();


        $data = array(
            "course_categories_data" => $course_categories_data
        );
        $result_array['display'] = view('coursecategories.dropdown',$data)->render();
        return Response()->json($result_array);
    }



    /**
     * List Course Displayed in frontend
     *
     * @author Moe Mantach
     * @access public
     */
    public function ListCourseManagement()
    {

        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_course_management_tab'])  && $role_info['om_course_management_tab'] == 'deny') || ( !isset( $role_info['om_course_management_tab'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }

        //$course_types = CourseType::whereCtIsDeleted(0)->get();
        $course_categories_data  = CourseCategories::whereCcIsDeleted(0)->get();

        $data = array(
            "course_categories_data" => $course_categories_data
        );
        return Response()->view('course.listcoursesmanagement',$data);
    }





    public function DisplayListCourses(Request $request)
    {
        $page_number                    = $request->input('page_number');
        $c_course_code                  = $request->input('c_course_code');
        $c_course_name                  = $request->input('c_course_name');
        $c_course_description           = $request->input('c_course_description');
        $fk_category_id                 = $request->input('fk_category_id');
        $sl_course_type                 = $request->input('sl_course_type');

        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;

        $courses_obj = Courses::whereCIsDeleted(0);

        if(strlen($c_course_code) > 0)
        {
            $courses_obj = $courses_obj->where('c_code','like','%' . $c_course_code . '%');
        }

        if(strlen($c_course_name) > 0)
        {
            $courses_obj = $courses_obj->where('c_course_name','like','%' . $c_course_name . '%');
        }

        if(is_numeric($fk_category_id)  && $sl_course_type > 0)
        {
            $courses_obj = $courses_obj->where('fk_category_id','=',$fk_category_id);
        }

        if(!is_numeric($fk_category_id) && $sl_course_type > 0)
        {
            $course_categories_obj = CourseCategories::whereFkCourseType($sl_course_type)->get();
            $course_categories_array = array();

            foreach ($course_categories_obj as $cc_index => $cc_info)
            {
                $course_categories_array[] = $cc_info->cc_id;
            }
           $courses_obj = $courses_obj->whereIn('fk_category_id',$course_categories_array);
        }

        if(strlen($c_course_description) > 0)
        {
            $courses_obj = $courses_obj->where('c_course_description','like','%' . $c_course_description . '%');
        }

        $all_courses = $courses_obj->count();


        $total_pages = ceil($all_courses/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);

        $courses = $courses_obj->skip($skip)->take($nbr_rows_per_pages)->get();

        $data = array(
            "courses" => $courses
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("course.displaylist",$data)->render();

        return Response()->json($result_array);
    }


    public function CourseSearchResults(Request $request)
    {
        $search_query            = $request->input('search_query');
        $c_course_category       = $request->input('c_course_category');
        $page_number             = $request->input('page_number');
        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;



        $all_courses = Courses::whereCIsDeleted(0);
        if(strlen($search_query) > 0)
        {
           $all_courses = $all_courses->where('c_course_name' , 'like' , '%' . $search_query . '%')->orWhere('c_code' , 'like' , '%' . $search_query . '%');
        }

        if($c_course_category > 0)
        {
            $all_courses = $all_courses->where('fk_category_id' , '=' ,$c_course_category);
        }


        $all_courses =  $all_courses->count();


        $total_pages = ceil($all_courses/$nbr_rows_per_pages);

        $total_pages = intval($total_pages);

        $courses = Courses::whereCIsDeleted(0);
        if(strlen($search_query) > 0)
        {
            $courses = $courses->where('c_course_name' , 'like' , '%' . $search_query . '%')->orWhere('c_code' , 'like' , '%' . $search_query . '%');
        }

        if($c_course_category > 0)
        {
            $courses = $courses->where('fk_category_id' , '=' ,$c_course_category);
        }


        $courses =  $courses->skip($skip)->take($nbr_rows_per_pages)->get();


        $lstCourseCategories = CourseManagement::getCourseCategoriesArrayById();
        $data = array(
            "courses" => $courses,
            "lstCourseCategories" => $lstCourseCategories
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("course.search_results",$data)->render();

        return Response()->json($result_array);
    }


    public function AddCourseForm()
    {

        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_add_course'])  && $role_info['om_add_course'] == 'deny') || ( !isset( $role_info['om_add_course'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }


        $course_categories_data  = CourseCategories::whereCcIsDeleted(0)->get();
        $data = array(
            "course_categories_data" => $course_categories_data
        );
        return view('course.addcourse',$data);
    }



    /**
     * Display Edit Course Form Page
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $bt_id
     */
    public function EditCourseForm( $c_id )
    {

        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_edit_existing_course'])  && $role_info['om_edit_existing_course'] == 'deny') || ( !isset( $role_info['om_edit_existing_course'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }

        $cur_course_info    = Courses::find( $c_id );
         $course_categories_data  = CourseCategories::whereCcIsDeleted(0)->get();

        $data = array(
            "cur_course_info" => $cur_course_info,
            "course_categories_data" => $course_categories_data
        );
        return view('course.editcourse',$data);
    }

    /**
     * Save Course Info to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     *
     * @return Response Json
     */
    public function SaveCourseInfo(Request $request)
    {
        $c_id                       = $request->input('c_id');
        $c_code                     = $request->input('c_code');
        $c_course_name              = $request->input('c_course_name');
        $c_course_description       = $request->input('c_course_description');
        $fk_category_id             = $request->input('fk_category_id');
        $c_total_hours              = $request->input('c_total_hours');
        $c_is_active                = $request->input('c_is_active');
        $c_is_active = $c_is_active== null ? "0" : 1;
        $result_array = array();

        $Course = new Courses();
        if($c_id !== null)
        {
            $Course = Courses::find($c_id);
        }

        $Course->c_code                 = $c_code;
        $Course->c_course_name          = $c_course_name;
        $Course->c_course_description   = $c_course_description;
        $Course->fk_category_id            = $fk_category_id;
        $Course->c_total_hours          = $c_total_hours;
        $Course->c_is_active            = $c_is_active;
        $Course->save();


        $result_array['is_error']  = 0;
        $result_array['error_msg'] = 'Course Information Has been saved';

        return Response()->json($result_array);
    }


    public function DeleteCourseInfo(Request $request)
    {

        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_delete_existing_course'])  && $role_info['om_delete_existing_course'] == 'deny') || ( !isset( $role_info['om_delete_existing_course'] ) ) )
        {
            $result_array['is_error']   = 1;
           $result_array['error_msg']  = "Permission Denied";

            return Response()->json($result_array);
        }

        $c_id       = $request->input('c_id');

        $Course = new Courses();
        $Course = Courses::find($c_id);
        $Course->c_is_deleted          = 1;
        $Course->c_deleted_by          = Session('user_id');
        $Course->save();


        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return Response()->json($result_array);
    }

    /**
     * Add Trainees To Selected Courses
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function AddTraineesCourse(Request $request)
    {
        $ps_project_trainees    = $request->input('ps_project_trainees');
        $project_id             = $request->input('project_id');
        $pc_id                  = $request->input('pc_id');

        $project_course_trainees = ProjectCourseTrainees::whereFkPcourseId($pc_id)->whereFkProjectId($project_id)->get();

        $trainees_exist_array = array();
        foreach ($project_course_trainees as $tc_index => $tc_info)
        {
            $trainees_exist_array[] = $tc_info->fk_trainee_id;
        }

        $db_array = array();
        $index = 0;
        for ($i = 0; $i < count($ps_project_trainees); $i++)
        {
            if(!in_array($ps_project_trainees[$i], $trainees_exist_array))
            {
                $db_array[$index]['fk_project_id'] = $project_id;
                $db_array[$index]['fk_pcourse_id'] = $pc_id;
                $db_array[$index]['fk_trainee_id'] = $ps_project_trainees[$i];
                $index++;
            }

        }

        if(count($db_array) > 0)
        {
            $res = ProjectCourseTrainees::insert($db_array);
        }

        $result_array = array();

        $result_array['is_error'] = 0;

        return Response()->json($result_array);
    }

}
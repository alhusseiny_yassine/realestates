<?php
/***********************************************************
CourseTypesController.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 27, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Http\Controllers\Courses;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\System\Roles;
use App\model\System\Courses;
use App\model\System\CourseType;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;


class CourseTypesController extends Controller
{

    public function CourseTypesManagement()
    {
        $data = array();
        return Response()->view('coursetypes.coursetypes',$data);
    }


    public function DisplayListCourseTypes(Request $request)
    {
        $page_number        = $request->input('page_number');
        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;



        $all_course_types = CourseType::whereCtIsDeleted(0)->count();


        $total_pages = ceil($all_course_types/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);

        $course_types = CourseType::whereCtIsDeleted(0)->skip($skip)->take($nbr_rows_per_pages)->get();
        $data = array(
            "course_types" => $course_types
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("coursetypes.displaylist",$data)->render();

        return Response()->json($result_array);
    }


    public function AddCourseTypeForm()
    {

        $data = array();
        return view('coursetypes.addcoursetype',$data);
    }


    /**
     * Save CourseType Info to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     *
     * @return Response Json
     */
    public function SaveCourseTypeInfo(Request $request)
    {
        $ct_id                      = $request->input('ct_id');
        $ct_course_type             = $request->input('ct_course_type');
        $ct_description             = $request->input('ct_description');

        $result_array = array();

        $CourseType = new CourseType();
        if($ct_id !== null)
        {
            $CourseType = CourseType::find($ct_id);
        }

        $CourseType->ct_course_type   = $ct_course_type;
        $CourseType->ct_description   = $ct_description;
        $CourseType->save();


        $result_array['is_error']  = 0;
        $result_array['error_msg'] = 'Course Type Information Has been saved';

        return Response()->json($result_array);
    }



    /**
     * Display Edit Course Type Form Page
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $bt_id
     */
    public function EditCourseTypeForm( $ct_id )
    {

        $cur_courseType_info    = CourseType::find( $ct_id );


        $data = array(
            "cur_courseType_info" => $cur_courseType_info
        );
        return view('coursetypes.editcoursetype',$data);
    }


    public function DeleteCourseTypeInfo(Request $request)
    {
        $ct_id = $request->input('ct_id');

        $CourseType = new CourseType();
        $CourseType = CourseType::find($ct_id);
        $CourseType->ct_is_deleted          = 1;
        $CourseType->ct_deleted_by          = Session('user_id');
        $CourseType->save();


        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return Response()->json($result_array);
    }

}
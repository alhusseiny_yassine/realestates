<?php
/***********************************************************
CourseCategoriesController.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 18, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/



namespace App\Http\Controllers\Courses;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\System\Roles;
use App\model\System\Courses;
use App\model\System\CourseType;
use App\model\System\CourseCategories;
use App\model\System\Days;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;


class CourseCategoriesController extends Controller
{

    public function CourseCategoriesManagement()
    {
        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_course_categories_management'])  && $role_info['om_course_categories_management'] == 'deny') || ( !isset( $role_info['om_course_categories_management'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }
        
        $data = array();
        return Response()->view('coursecategories.coursecategory',$data);
    }


    public function DisplayListCourseCategories(Request $request)
    {
        $page_number        = $request->input('page_number');
        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;



        $all_course_categories = CourseCategories::whereCcIsDeleted(0)->count();


        $total_pages = ceil($all_course_categories/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);

        $course_categories = CourseCategories::whereCcIsDeleted(0)->skip($skip)->take($nbr_rows_per_pages)->get();
        $data = array(
            "course_categories" => $course_categories
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("coursecategories.displaylist",$data)->render();

        return Response()->json($result_array);
    }


    public function AddCourseCategoryForm()
    {
        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_add_course_category'])  && $role_info['om_add_course_category'] == 'deny') || ( !isset( $role_info['om_add_course_category'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }
        
        $courseTypes = CourseType::whereCtIsDeleted(0)->get();
        $data = array(
          "courseTypes" => $courseTypes
        );
        return view('coursecategories.addcoursecategory',$data);
    }


    /**
     * Save Course Category Info to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     *
     * @return Response Json
     */
    public function SaveCourseCategoryInfo(Request $request)
    {
        $cc_id                      = $request->input('cc_id');
        $fk_course_type             = $request->input('fk_course_type');
        $cc_course_category         = $request->input('cc_course_category');

        $result_array = array();

        $courseCategory = new CourseCategories();
        if($cc_id !== null)
        {
            $courseCategory = CourseCategories::find($cc_id);
        }

        $courseCategory->fk_course_type   = $fk_course_type;
        $courseCategory->cc_course_category   = $cc_course_category;
        $courseCategory->save();


        $result_array['is_error']  = 0;
        $result_array['error_msg'] = 'Course Category Information Has been saved';

        return Response()->json($result_array);
    }



    /**
     * Display Edit Course Category Form Page
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $bt_id
     */
    public function EditCourseCategoryForm( $cc_id )
    {

        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_edit_existing_course_category'])  && $role_info['om_edit_existing_course_category'] == 'deny') || ( !isset( $role_info['om_edit_existing_course_category'] ) ) )
        {
            return Redirect::to('administrator/PermissionDenied');
        }
        
        $cur_courseCategory_info    = CourseCategories::find( $cc_id );

        $courseTypes = CourseType::whereCtIsDeleted(0)->get();
        
        $data = array(
            "cur_courseCategory_info" => $cur_courseCategory_info,
            "courseTypes" => $courseTypes
        );
        return view('coursecategories.editcoursecategory',$data);
    }


    public function DeleteCourseCategoryInfo(Request $request)
    {
        $role_info = session()->get('role_info');
        if( ( isset($role_info['om_delete_existing_course_category'])  && $role_info['om_delete_existing_course_category'] == 'deny') || ( !isset( $role_info['om_delete_existing_course_category'] ) ) )
        {
             $result_array['is_error']   = 1;
            $result_array['error_msg']  = "You canno't Delete the Course Category";
    
            return Response()->json($result_array);
        }
        
        $cc_id = $request->input('cc_id');

        $CourseCategory = new CourseCategories();
        $CourseCategory = CourseCategories::find($cc_id);
        $CourseCategory->cc_is_deleted          = 1;
        $CourseCategory->cc_deleted_by          = Session('user_id');
        $CourseCategory->save();


        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return Response()->json($result_array);
    }

}
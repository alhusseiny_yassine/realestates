<?php
/***********************************************************
RequestStatusesController.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 29, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/



namespace App\Http\Controllers\Requests;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\Requests\RequestStatus;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;


class RequestStatusesController extends Controller
{
    public function RequestStatusesManagement()
    {
        $data = array();
        return Response()->view('requeststatuses.requeststatus',$data);
    }


    public function DisplayListRequestStatuses(Request $request)
    {
        $page_number        = $request->input('page_number');
        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;



        $all_request_statuses = RequestStatus::whereRsIsDeleted(0)->count();


        $total_pages = ceil($all_request_statuses/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);

        $request_statuses = RequestStatus::whereRsIsDeleted(0)->skip($skip)->take($nbr_rows_per_pages)->get();
        $data = array(
            "request_statuses" => $request_statuses
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("requeststatuses.displaylist",$data)->render();

        return Response()->json($result_array);
    }


    public function AddRequestStatusForm()
    {

        $data = array();
        return view('requeststatuses.addrequeststatus',$data);
    }


    /**
     * Save Request Status information to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function SaveRequestStatusInfo(Request $request)
    {
        $rs_id                          = $request->input('rs_id');
        $rs_status_name                 = $request->input('rs_status_name');
        $rs_status_color                = $request->input('rs_status_color');

        $result_array = array();

        $RequestStatus = new RequestStatus();
        if($rs_id !== null)
        {
            $RequestStatus = RequestStatus::find($rs_id);
        }

        $RequestStatus->rs_status_name      = $rs_status_name;
        $RequestStatus->rs_status_color     = $rs_status_color;
        $RequestStatus->save();


        $result_array['is_error']  = 0;
        $result_array['error_msg'] = 'Request Status Information Has been saved';

        return Response()->json($result_array);
    }



    /**
     * Display Edit Request Status Form Page
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $bt_id
     */
    public function EditRequestStatusForm( $rs_id )
    {

        $cur_request_status_info    = RequestStatus::find( $rs_id );

        $data = array(
            "cur_request_status_info" => $cur_request_status_info
        );
        return view('requeststatuses.editrequeststatus',$data);
    }


    public function DeleteRequestStatusInfo(Request $request)
    {
        $rs_id = $request->input('rs_id');

        $RequestStatus = new RequestStatus();
        $RequestStatus = RequestStatus::find($rs_id);
        $RequestStatus->rs_is_deleted          = 1;
        $RequestStatus->rs_deleted_by          = Session('user_id');
        $RequestStatus->save();


        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return Response()->json($result_array);
    }

}
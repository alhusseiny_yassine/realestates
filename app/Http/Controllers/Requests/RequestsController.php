<?php
/***********************************************************
RequestsController.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 1, 2016
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

namespace App\Http\Controllers\Requests;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\model\Requests\RequestsCourses;
use App\model\Requests\RequestStatus;
use App\Libraries\Requests\RequestsManager;
use App\Libraries\Courses\CourseManagement;
use App\model\Requests\Request As MRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\model\System\Courses;
use App\model\Users\Users;
use App\model\System\Beneficiaries;
use App\model\Notifications\Notifications;
use App\Libraries\Users\UserManager;
use App\model\Notifications\NotificationTypes;
use App\Libraries\Notifications\NotifcationsManager;
use App\model\Users\UserTypes;
use App\model\Project\ProjectCourses;
use App\model\Project\ProjectRequests;
use App\model\Project\Projects;
use App\model\Project\ProjectStatus;


class RequestsController extends Controller
{

    /**
     * Open Send Request Popup to save form data in the database
     *
     * @author Moe Mantach
     * @access public
     * @param String $course_ids
     */
    public function SendRequestTraining( $course_ids )
    {
        $course_ids = urldecode($course_ids);
        $course_ids_array = explode(",", $course_ids);
        $course_info_array = array();
        $courses_info = array();

        // get all courses information
        $course_info = Courses::whereCIsDeleted(0)->whereCIsActive(1)->get();

        foreach ($course_info as $key => $course) {
           $course_info_array[ $course->c_id ] = $course->c_course_name;
        }


        $data = array(
            "course_info_array" => $course_info_array,
            "course_ids_array" => $course_ids_array,
            "course_ids" => $course_ids,
        );
        return Response()->view('requests.sendrequest',$data);
    }



    /**
     * Save Request Data in the database and notifie coordinator that we have a new request
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function SendRequest( Request $request )
    {
        $course_ids                     = $request->input('course_ids');
        $r_request_title                = "";
        $r_request_train_date           = $request->input('r_request_train_date');
        $r_request_order                = $request->input('r_request_order');
        $r_request_description          = $request->input('r_request_description');
        $result_array = array();
        $course_ids_array = explode(",", $course_ids);
        $trainee_id = Session('ut_user_id');
        $trainee_info = Users::find($trainee_id);
        $benf_id = $trainee_info->fk_beneficiary_id;
        for ($i = 0; $i < count($course_ids_array); $i++)
        {

            $sys_course = Courses::find($course_ids_array);

            // check if trainee has already send request for this course avoid it
            $request_courses = RequestsCourses::whereFkCourseId($course_ids_array[$i])->get();

            if(count($request_courses) > 0 )
            {
                $request_ids = array();
                foreach ($request_courses as $key => $rc_info)
                {
                    $request_ids[] = $rc_info->fk_request_id;
                }
                // get projects requests
                $ProjectRequests = ProjectRequests::whereIn('fk_request_id',$request_ids)->get();

                $project_ids = array();
                foreach ($ProjectRequests as $key => $pr_info)
                {
                    $project_ids[$pr_info->fk_request_id][] = $pr_info->fk_project_id;
                }



                foreach ( $request_ids as $index => $r_id )
                {
                    if(!isset($project_ids[$r_id]))// this request not belong to any project
                    {
                        $countRequest = MRequest::whereFkTraineeId($trainee_id)->whereRId($r_id)->count();

                        if($countRequest > 0)
                        {
                            $result_array['is_error']   = 1;
                            $result_array['error_msg']  = "You cannot Send Request to Same Course Twice";
                            return json_encode($result_array);
                        }
                    }



                }
            }
            
            // check  number of request already created 
            $ut_user_id         = Session('ut_user_id');
            $max_number_of_requests =  Config::get('appconfig.max_requests_per_user');
            $allowed_statuses = array( RequestStatus::REQUEST_STATUS_DONE_FAILED , RequestStatus::REQUEST_STATUS_DONE_PASSED , RequestStatus::REQUEST_STATUS_DONE_RESCHEDULED , RequestStatus::REQUEST_STATUS_DONE_RETESTED , RequestStatus::REQUEST_STATUS_DONE_UNTESTED);
            $nbr_requests = MRequest::whereFkTraineeId($ut_user_id)->whereNotIn('fk_status_id',$allowed_statuses)->count();
            if($nbr_requests == $max_number_of_requests)
            {
                $result_array['is_error']         = 1;
                $result_array['error_msg']      = "You reach the Max Number of  Requests Allowed";
                return Response()->json($result_array);
            }
            
            $request = new MRequest();
            $r_request_code = md5(date("YMDHiS"));
            $r_request_code = "COUR-" . substr($r_request_code, 4 , 10);;
            $request->r_request_code                = $r_request_code;
            $request->r_request_title               = $sys_course[0]->c_course_name . " - " . $trainee_info->u_fullname;
            $request->fk_trainee_id                 = Session('ut_user_id');
            $request->fk_status_id                  = RequestStatus::REQUEST_STATUS_UNDER_REVIEW;
            $request->r_request_creation_date       = date("Y-m-d H:i:s");
            $request->fk_beneficiary_id             = $benf_id;
            $request->r_request_creation_by         = 0;
            $request->r_request_train_date          = date("Y-m-d H:i:s");
            $request->r_request_order               = $r_request_order;
            $request->r_request_description         = $r_request_description;
            $request->save();

            $request_id = $request->r_id;

            // send notification to coordinator and administrator that we have a new request
            $ut_coordinator_id  = Session('ut_coordinator_id');
            
            $cordinator_info    = Users::find($ut_coordinator_id); 
            $ut_user_fullname   =  Session('ut_user_fullname');
            $ut_user_email      =  Session('ut_user_email');
            $benf_info          = Beneficiaries::find($benf_id);
            $course_info        = Courses::find($course_ids_array[$i]);
            
                $params_array = array(
                    "notification_type" => NotifcationsManager::NOT_SEND_REQUEST,
                    "coordinator_name"  => $cordinator_info['attributes']['u_fullname'],
                    "full_name"         => $ut_user_fullname,
                    "user_name"         => $ut_user_email,
                    "fk_request_id"     => $request_id,
                    "fk_course_id"      => $course_ids_array[$i],
                    "beneficiary"       => $benf_info->sb_name,
                    "coordinator_id"    => $ut_coordinator_id,
                    "ut_user_id"        => $ut_user_id,
                    "course_name"        => $course_info->c_course_name,
                );
                NotifcationsManager::SendNotification($params_array);

           $reqCourse = new RequestsCourses();
           $reqCourse->fk_course_id      = $course_ids_array[$i];
           $reqCourse->fk_request_id     = $request_id;
           $reqCourse->save();
        }

        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Request For Training Has been Sent";


        return Response()->json($result_array);
    }


    /**
     * Page to display list of requests with statuses and ability to approve and deny or create
     * a new training project with list of Requests
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function ListRequestsManagement( Request $request )
    {
        // get list of requests statuses
         $request_statuses = RequestStatus::whereRsIsDeleted(0)->get();
         $role_info = session()->get('role_info');
        $data = array(
            'request_statuses' => $request_statuses,
            'role_info' => $role_info,
        );
       return view('requests.requestsmanagement',$data);
    }

    
    
    public function SendApprovalRequest( Request $request )
    {
        $request_ids                   = $request->input('request_ids'); 
        $request_ids_array           = explode(",", $request_ids);
        $login_user_id                  = Session('ut_user_id');
        
        for ($i = 0; $i < count($request_ids_array); $i++)
        {
            // for every request send rejection notification and change request status to reject
            $request_id     = $request_ids_array[$i];
            $request_info   = MRequest::find($request_id); 
            
            $request_info->fk_status_id             = RequestStatus::REQUEST_STATUS_APPROVED_BY_COORDINATOR;
            $request_info->save();
            $trainee_id = $request_info->fk_trainee_id;
            $trainee_info = Users::find($trainee_id);
            $coordinator_info = Users::find($login_user_id);
            
            
            // Send Notification to the trainee user for Approval of request training
            $data_array = array(
                'notification_type' => NotifcationsManager::NOT_APPROVE_REQUEST,
                'request_id' => $request_id,
                'user_id' => $request_info->fk_trainee_id,
                'trainee_uname' => $trainee_info->u_username,
                'coordinator_id' => $login_user_id,
                'coordinator_uname' => $coordinator_info->u_username,
                'coordinator_name' => $coordinator_info->u_fullname,
                'trainee_name' => $coordinator_info->u_fullname,
            );
            $notification_manager = new NotifcationsManager();
            $notification_manager->SendNotification($data_array);
        }
        
        $result_array['is_error'] = 0;
        $result_array['error_msg'] = 'Notification Sent Successfully';
        
        return Response()->json($result_array);
    }

    /**
     * Admin Page to control requests Sent from the trainees
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function AdminRequestsManager(Request $request)
    {

        // get list of information can be used in filtering
        $lst_courses = Courses::whereCIsActive(1)->whereCIsDeleted(0)->get();
        $lst_benfs   = Beneficiaries::whereSbIsDeleted(0)->get();
        $lst_request_statuses   = RequestStatus::whereRsIsDeleted(0)->get();
        $lst_trainees   = Users::whereUIsDeleted(0)->whereUUserType( UserTypes::TRAINEES_USER_TYPE_ID )->whereUIsActive(1)->get();

        $data = array(
            "lst_courses" => $lst_courses,
            "lst_benfs" => $lst_benfs,
            "lst_request_statuses" => $lst_request_statuses,
            "lst_trainees" => $lst_trainees
        );
        return view('requests.adminrequestsmanagement',$data);
    }


    /**
     * Get list of Sent Request from logged in user
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function DisplayMyRequestsList( Request $request )
    {

 
        $page_number                    = $request->input('page_number');
        $my_request_status              = $request->input('my_request_status');
        $my_request_date                = $request->input('my_request_date');
        $my_request_title               = $request->input('my_request_title');
        $my_request_no                  = $request->input('my_request_no');
        $nbr_rows_per_pages             = Config::get('appconfig.max_rows_per_page');
        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;

        $current_user_id = Session('ut_user_id');

        $request_object =  MRequest::whereRIsDeleted(0)->whereFkTraineeId($current_user_id);

        // get all search condition
        if($my_request_title != '')
        {
            $request_object = $request_object->where('r_request_title','like','%' . $my_request_title . '%');
        }
        if($my_request_no != '')
        {
            $request_object = $request_object->where('r_request_code','like','%' . $my_request_no . '%');
        }
        if($my_request_status != '')
        {
            $request_object = $request_object->whereFkStatusId($my_request_status);
        }
        if(strlen($my_request_date) > 0)
        {
            $request_object = $request_object->whereRRequestTrainDate($my_request_date);
        }

        $allmyrequests = $request_object->count();

        $total_pages = ceil($allmyrequests/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);

        $myrequests = $request_object->skip($skip)->take($nbr_rows_per_pages)->get();
        $request_statuses_byid = RequestsManager::GetRequestStatusInfoById();
        

        $data = array(
            "myrequests" => $myrequests,
            "request_statuses_byid" => $request_statuses_byid,
        );

        $result_array = array();

        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("requests.displaylistmyrequests",$data)->render();

        return Response()->json($result_array);
    }

    /**
     * Send rejection request to the trainee with
     * notification
     *
     * @author Moe Mantach
     * @access public
     * @param String $request_ids
     *
     * @return view
     */
    public function SendRequestRejections( $request_ids )
    {
        $data = array(
            'request_ids' => $request_ids
        );
       return Response()->view('notifications.sendrejectionrequest',$data);
    }



    public function SendRejectionNotifications(Request $request)
    {
        $request_ids            = $request->input('request_ids');
        $r_rejection_comment    = $request->input('r_rejection_comment');
        $request_ids_array      = explode(",", $request_ids);
        $login_user_id          = Session('user_id');

        for ($i = 0; $i < count($request_ids_array); $i++)
        {
            // for every request send rejection notification and change request status to reject
            $request_id     = $request_ids_array[$i];
            $request_info   = MRequest::find($request_id);
            $request_info->r_request_deny_comment   = $r_rejection_comment;
            $request_info->fk_status_id             = RequestStatus::REQUEST_STATUS_REJECTED;
            $request_info->save();


            // Send Notification to the trainee user for rejection of request training
            $data_array = array(
                'notification_type' => NotifcationsManager::NOT_REJECTION_REQUEST,
                'request_id' => $request_id
            );
            $notification_manager = new NotifcationsManager();
            $notification_manager->SendNotification($data_array);
        }

        $result_array['is_error'] = 0;
        $result_array['error_msg'] = 'Notification Sent Successfully';


        return Response()->json($result_array);
    }

    /**
     * Display list of all requests and pagination
     *
     * @author Moe Mantach
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function DisplayAllRequestsList( Request $request )
    {

        $page_number                 = $request->input('page_number');
        $is_admin                        = $request->input('is_admin');
        $rq_request_status           = $request->input('rq_request_status');
        $rq_request_trainee          = $request->input('rq_request_trainee');
        $sl_courses                      = $request->input('sl_courses');
        $sb_benf                         = $request->input('sb_benf');
        $ut_user_type                  = Session('ut_user_type');
        $ut_beneficiary_id             = Session('ut_beneficiary_id');
        $nbr_rows_per_pages        = Config::get('appconfig.max_rows_per_page');
        
        DB::enableQueryLog();
        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;

        $list_request_ids = array();
        // get selected courses
        if($sl_courses > 0)
        {
            $request_courses = RequestsCourses::whereFkCourseId($sl_courses)->get();
            foreach ($request_courses as $key => $req_info) {
               $list_request_ids[] = $req_info->fk_request_id;
            }
        }

        $role_info = UserManager::GetListOfRequestStatusPrivilege();
        $request_status_ids = array();


        if($is_admin == 0)
        {
            foreach ($role_info as $r_id => $status) {
                if($status == 'allow')
                {
                    $request_status_ids[] = $r_id;
                }
            }
        }


        $lst_users_data = UserManager::getallUsersById();
        $allmyrequests_obj = MRequest::whereRIsDeleted(0);

        if($is_admin == 0)
        {
            if(count($request_status_ids) > 0)
            {
                $allmyrequests_obj = $allmyrequests_obj->whereIn('fk_status_id' , $request_status_ids);
            }
        }
        else
        {

            if($rq_request_status != 0 )
            {
                $allmyrequests_obj = $allmyrequests_obj->whereFkStatusId( $rq_request_status);
            }
        }


        if($rq_request_trainee > 0)
        {
            $allmyrequests_obj = $allmyrequests_obj->whereFkTraineeId($rq_request_trainee);
        }

        if($sl_courses > 0)
        {
            $allmyrequests_obj = $allmyrequests_obj->whereIn('r_id',$list_request_ids);
        }
        
        if($ut_user_type == UserTypes::COORDINATORS_USER_TYPE_ID)
        {
            
            $allmyrequests_obj = $allmyrequests_obj->where('fk_beneficiary_id',$ut_beneficiary_id);
        }
        
        if($sb_benf > 0)
        {
            $allmyrequests_obj = $allmyrequests_obj->whereIn('fk_beneficiary_id',$sb_benf);
        }





        $allmyrequests = $allmyrequests_obj->count();
 

        $total_pages = ceil($allmyrequests/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);

        $myrequests = MRequest::whereRIsDeleted(0);

        if($is_admin == 0)
        {
            if(count($request_status_ids) > 0)
            {
                $myrequests = $myrequests->whereIn('fk_status_id' , $request_status_ids);
            }
        }
        else
        {
            if($rq_request_status != 0 )
            {
                $myrequests = $myrequests->whereFkStatusId( $rq_request_status);
            }
        }

        if($rq_request_trainee > 0)
        {
            $myrequests = $myrequests->whereFkTraineeId($rq_request_trainee);
        }
        
        if($ut_user_type == UserTypes::COORDINATORS_USER_TYPE_ID)
        {
            
            $myrequests= $myrequests->where('fk_beneficiary_id',$ut_beneficiary_id);
        }
        
        if($sl_courses > 0)
        {
            $myrequests = $myrequests->whereIn('r_id',$list_request_ids);
        }

       $myrequests =  $myrequests->skip($skip)->take($nbr_rows_per_pages)->get();

       // get list request Ids
       $lst_requests_array = array();
       foreach ($myrequests as $index => $req_info) {
           $lst_requests_array[] = $req_info->r_id;
       }

       $RequestCourse = RequestsCourses::whereIn('fk_request_id',$lst_requests_array)->get();
       $course_ids = array(); //
       $request_courses_array = array();
       foreach ($RequestCourse as $index => $rc_info) {
           $course_ids[] = $rc_info->fk_course_id;
           $sys_course_info = Courses::find($rc_info->fk_course_id);

           $request_courses_array[ $rc_info->fk_request_id  ] = $sys_course_info->c_course_name;
       }

        $request_statuses_byid = RequestsManager::GetRequestStatusInfoById();
        $benf_data_obj = Beneficiaries::whereSbIsDeleted(0)->get();
        $benf_data_obj = CreateDatabaseArrayByIndex($benf_data_obj , 'sb_id' );

        // get course info

        // get projects related to list requests
        $request_ids = array();
        foreach ( $myrequests as $index => $request_info )
        {
            $request_ids[] = $request_info->r_id;
        }

        $ProjectRequests        = ProjectRequests::whereIn('fk_request_id' , $request_ids)->get();
        $project_request_array  = CreateDatabaseArrayByIndex($ProjectRequests , 'fk_request_id' );

        $project_status_array = ProjectStatus::wherePsIsDeleted(0)->get();

        $project_ids = array();
          foreach ($project_request_array as $index => $p_info) {
              $project_ids[] = $p_info->fk_project_id;
          }

        $role_info                    = Session('role_info');
        $projet_info                 = Projects::whereIn('pp_id' , $project_ids)->get();
        $projects_array            = CreateDatabaseArrayByIndex($projet_info , 'pp_id' );
        
        $data = array(
            "myrequests" => $myrequests,
            "lst_users_data" => $lst_users_data,
            "role_info" => $role_info,
            "is_admin" => $is_admin,
            "projects_array" => $projects_array,
            "project_request_array" => $project_request_array,
            "request_courses_array" => $request_courses_array,
            "project_status_array" => $project_status_array,
            "benf_data_obj" => $benf_data_obj,
            "request_statuses_byid" => $request_statuses_byid,
        );

        $result_array = array();

        $result_array['total_pages']  = $total_pages;
        $result_array['display']         = view("requests.displaylistrequests",$data)->render();

        return Response()->json($result_array);
    }


    /**
     * new Request Form where we can create a new request and assign it to a trainee
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function AddNewRequestForm(Request $request)
    {
        $trainees_lst_users = Users::whereUIsActive(1)->whereUIsDeleted(0)->whereUUserType( UserTypes::TRAINEES_USER_TYPE_ID )->get();
        $lst_courses = Courses::whereCIsActive(1)->whereCIsDeleted(0)->get();
        $lst_rq_statuses = RequestStatus::whereRsIsDeleted(0)->get();
        $lst_benf = Beneficiaries::whereSbIsDeleted(0)->get();
        $benf_array = CreateDatabaseArrayByIndex($lst_benf,'sb_id');
        $trainees_array = array();

        foreach ($trainees_lst_users as $index => $trainee_info ) {
            $trainees_array[$trainee_info->id] = $trainee_info->u_fullname . "-" . $trainee_info->id . "-" . $benf_array[$trainee_info->fk_beneficiary_id]['attributes']['sb_name'];
        }
        $trainee_id = $request->input('trainee_id');

        $data = array(
            'trainees_array' => $trainees_array,
            'lst_courses' => $lst_courses,
            'trainee_id' => $trainee_id,
            'lst_rq_statuses' => $lst_rq_statuses
        );
        return Response()->view('requests.addrequest',$data);
    }


    /**
     * Edit Request Form contain all submited information and have ability to change it from the admin section
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function EditRequestForm($r_id)
    {
        $request_info = MRequest::find($r_id);
        $request_courses_info =   RequestsCourses::whereFkRequestId($r_id)->get();
        $trainees_lst_users = Users::whereUIsActive(1)->whereUIsDeleted(0)->whereUUserType( UserTypes::TRAINEES_USER_TYPE_ID )->get();
        $lst_courses = Courses::whereCIsActive(1)->whereCIsDeleted(0)->get();
        $lst_rq_statuses = RequestStatus::whereRsIsDeleted(0)->get();
        $courses_selected = array();

        foreach ($request_courses_info as $key => $value) {
            $courses_selected[] = $value->fk_course_id;
        }

        $lst_benf = Beneficiaries::whereSbIsDeleted(0)->get();
        $benf_array = CreateDatabaseArrayByIndex($lst_benf,'sb_id');
        $trainees_array = array();

        foreach ($trainees_lst_users as $index => $trainee_info ) {
            $trainees_array[$trainee_info->id] = $trainee_info->u_fullname . "-" . $trainee_info->id . "-" . $benf_array[$trainee_info->fk_beneficiary_id]['attributes']['sb_name'];
        }

        $data = array(
           'request_info' => $request_info,
           'courses_selected' => $courses_selected,
           'trainees_array' => $trainees_array,
           'lst_courses' => $lst_courses,
           'lst_rq_statuses' => $lst_rq_statuses,
        );
        return Response()->view('requests.editrequest',$data);
    }


    /**
     * Save Request Information and courses related in the form coming and open from the admin section
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function SaveRequestInfo(Request $request)
    {
        $course_ids             = $request->input('sl_courses');
        $r_request_title        = $request->input('r_request_title');
        $r_id                   = $request->input('r_id');
        $r_request_train_date   = $request->input('r_request_train_date');
        $rq_request_trainee     = $request->input('rq_request_trainee');
        $rq_request_status      = $request->input('rq_request_status');
        $r_request_code         = $request->input('r_request_code');
        $r_request_order         = $request->input('r_request_order');
        
        
        // check  number of request already created if is the same of trainees 
        $ut_user_id         = $rq_request_trainee;
        $max_number_of_requests =  Config::get('appconfig.max_requests_per_user');
        $allowed_statuses = array( RequestStatus::REQUEST_STATUS_DONE_FAILED , RequestStatus::REQUEST_STATUS_DONE_PASSED , RequestStatus::REQUEST_STATUS_DONE_RESCHEDULED , RequestStatus::REQUEST_STATUS_DONE_RETESTED , RequestStatus::REQUEST_STATUS_DONE_UNTESTED);
        $nbr_requests = MRequest::whereFkTraineeId($ut_user_id)->whereNotIn('fk_status_id',$allowed_statuses)->count();
        if($nbr_requests == $max_number_of_requests)
        {
            $result_array['is_error']         = 1;
            $result_array['error_msg']      = "You reach the Max Number of  Requests Allowed";
            return Response()->json($result_array);
        }
        
        
        
        
        $r_request_code = "rq-" . rand(9, 9999);
        $trainee_info = Users::find($rq_request_trainee);
        $benf_id = $trainee_info->fk_beneficiary_id;
        if(is_array($course_ids))
        {
            $course_ids_array = $course_ids;
        }
        else
        {
            $course_ids_array[] = $course_ids;
        }


        $sys_course = Courses::whereIn('c_id',$course_ids_array)->get();

        $trainee_info = Users::find($rq_request_trainee);

        $result_array = array();

        if($r_id === NULL)
        {

            foreach ( $sys_course as $index => $sc_info )
            {


            // check if trainee has already send request for this course avoid it
            $request_courses = RequestsCourses::whereFkCourseId($sc_info->c_id)->get();

            if(count($request_courses) > 0 )
            {
                $request_ids = array();
                foreach ($request_courses as $key => $rc_info)
                {
                    $request_ids[] = $rc_info->fk_request_id;
                }


                $TRequest = MRequest::whereFkTraineeId($trainee_info->id)->whereIn('r_id',$request_ids)->get();
                if(count($TRequest) > 0)
                {
                    $t_request = array();
                    foreach ($TRequest as $key => $r_info) {
                        $t_request[] = $r_info->r_id;
                    }

                    // get projects requests
                    $ProjectRequests = ProjectRequests::whereIn('fk_request_id',$t_request)->get();

                    $project_ids = array();
                    foreach ($ProjectRequests as $key => $pr_info)
                    {
                        $project_ids[$pr_info->fk_request_id][] = $pr_info->fk_project_id;
                    }



                    foreach ( $t_request as $index => $r_id )
                    {
                        if(isset($project_ids[$r_id]))
                        {
                            $project_array = $project_ids[$r_id];

                            foreach ($project_array as $key => $project_id)
                            {
                                // check project if close we can add a new request else we cannnot
                                $project_info = Projects::find($project_id);
                                if($project_info->fk_project_status_id != ProjectStatus::PROJECT_STATUS_CLOSED)
                                {
                                    $result_array['is_error']   = 1;
                                    $result_array['error_msg']  = "You cannot Send Request to Same Course Twice";
                                    return json_encode($result_array);
                                }

                            }

                        }
                        else // this request not belong to any project
                        {
                            $countRequest = MRequest::whereFkTraineeId($trainee_info->id)->whereRId($r_id)->count();

                            if($countRequest > 0)
                            {
                                $result_array['is_error']   = 1;
                                $result_array['error_msg']  = "You cannot Send Request to Same Course Twice";
                                return json_encode($result_array);
                            }
                        }



                    }
                }

            }

                $request = new MRequest();
                $request->r_request_code                = $r_request_code . "-" . $index;

                $r_request_order = ( $r_request_order == null || strlen($r_request_order) == 0 ) ? 1 : $r_request_order;

                $request->r_request_title                   = $sc_info->c_course_name . " - " . $trainee_info->u_fullname;
                $request->fk_trainee_id                     = $rq_request_trainee;
                $request->fk_status_id                      = $rq_request_status;
                $request->r_request_creation_by             = Session('user_id');
                $request->r_request_creation_date           = date("Y-m-d H:i:s");
                $request->r_request_train_date              = $r_request_train_date;
                $request->r_request_order                   = $r_request_order;
                $request->r_request_approval_date              = NULL;
                $request->fk_beneficiary_id                 = $benf_id;
                $request->save();

                $request_id = $request->r_id;
                $reqCourse = new RequestsCourses();
                $reqCourse->fk_course_id = $sc_info->c_id;
                $reqCourse->fk_request_id =$request_id;
                $reqCourse->save();
            }
        }
        else
        {
            foreach ( $sys_course as $index => $sc_info )
            {

                // check if trainee has already send request for this course avoid it
                $request_courses = RequestsCourses::whereFkRequestId($sc_info->c_id)->get();
                if(count($request_courses) > 0 )
                {
                    $request_ids = array();
                    foreach ($request_courses as $key => $rc_info)
                    {
                        $request_ids[] = $rc_info->fk_request_id;
                    }

                    $countRequest = MRequest::whereFkTraineeId($rq_request_trainee)->whereIn('r_id',$request_ids)->count();

                    if($countRequest > 0)
                    {
                        $result_array['is_error']   = 1;
                        $result_array['error_msg']  = "You cannot Send Request to Same Course Twice";

                        return json_encode($result_array);
                    }
                }

                $request = new MRequest();
                $request = MRequest::find($r_id);
                $request->r_request_code                = $r_request_code;

                $request->r_request_title               = $sc_info->c_course_name . " - " . $trainee_info->u_fullname;
                $request->fk_trainee_id                 = $rq_request_trainee;
                $request->fk_status_id                  = $rq_request_status;
                $request->r_request_creation_by         = Session('user_id');
                $request->r_request_order               = $r_request_order;
                $request->fk_beneficiary_id              = $benf_id;
                $request->r_request_train_date          = $r_request_train_date;
                $request->save();

                $request_id = $request->r_id;


                RequestsCourses::whereFkRequestId($request_id)->delete();

                $reqCourse = new RequestsCourses();
                $reqCourse->fk_course_id = $sc_info->c_id;
                $reqCourse->fk_request_id =$request_id;
                $reqCourse->save();
            }



        }

        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Request For Training Has been Sent";


        return Response()->json($result_array);
    }

    /**
     * function to delete all information related to request selected by
     * changing flag is_deleted to 1 and log deleted by  current user
     *
     * @author MOe Mantach
     * @access public
     * @param Request $request
     *
     * @return
     */
    public function DeleteRequestInformation(Request $request)
    {
        $r_id = $request->input('r_id');

        $mrequest = MRequest::find($r_id);
        $mrequest->r_is_deleted = 1;
        $mrequest->r_deleted_by = Session('user_id');
        $mrequest->save();

        $result_array = array();

        $result_array['is_error']   = 0;
        $result_array['error_msg']  = 'Operation Completed Successfully';
        return Response()->json($result_array);
    }

    /**
     * Display Requests Dashboard
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function RequestsDashboard(Request $request)
    {

        $result_array = array();
        $ut_user_id = Session('ut_user_id');

        // get information for the last request sent
        $last_request = MRequest::whereRIsDeleted(0)->get();
        $data = array();

        // if the number of requests is 0 we show a message no results found
        if(count($last_request) == 0)
        {
            $data = array(
               'message' => 'No Results Found'
            );
            $result_array['display'] = view("messages.info",$data)->render();
            return Response()->json($result_array);
        }

        $last_request_info = $last_request[count($last_request) - 1];
        $request_statuses_info = RequestStatus::find($last_request_info->fk_status_id);


        // get trainee information
        $trainee_info = Users::find($last_request_info->fk_trainee_id);

        // get beneficary of the trainee
        $benef_info = Beneficiaries::find($trainee_info->fk_beneficiary_id);

        $request_courses_ids = RequestsCourses::whereFkRequestId($last_request_info->r_id)->get();

        $course_categories_info = CourseManagement::getCourseCategoriesArrayById();

        $courses_data_array  = array();

        for ($i = 0; $i < count($request_courses_ids); $i++)
        {
            $courses_data_array[ $request_courses_ids[$i]->fk_course_id ] =  Courses::find( $request_courses_ids[$i]->fk_course_id );
        }

        $data = array(
            "trainee_info" => $trainee_info,
            "benef_info" => $benef_info,
            "courses_data_array" => $courses_data_array,
            "course_categories_info" => $course_categories_info,
            "last_request_info" => $last_request_info,
            "request_statuses_info" => $request_statuses_info
        );
        $result_array['display'] = view("requests.requests_dashboard",$data)->render();

        return Response()->json($result_array);
    }


    /**
     * change status of selected request
     * @param Request $request
     */
    public function ChangeRequestStatus($request_ids)
    {
        $roles_info = session()->get('role_info');
       
        $requests_statuses = RequestStatus::whereRsIsDeleted(0)->get();
        // get ids of request allowed
        $request_status_ids = array();
        foreach ($requests_statuses as $index  => $rs_info ) 
        { 
          
        
            if(isset($roles_info['request_status_' . $rs_info['attributes']['rs_id']]) && $roles_info['request_status_' . $rs_info['attributes']['rs_id']] == 'allow')
                $request_status_ids[] = $rs_info['attributes']['rs_id'];
        }  
        
        
        if(count($request_status_ids) > 0)
        {
            $allowed_request_statuses = RequestStatus::whereRsIsDeleted(0)->whereIn('rs_id' ,$request_status_ids)->get();
        }
        else 
         {
             $allowed_request_statuses = RequestStatus::whereRsIsDeleted(0)->where('rs_id' , 0)->get();
        }
        
        
        $data = array(
            'request_ids' => $request_ids,
            'requests_statuses' => $allowed_request_statuses,
        );
        return Response()->view('requests.changerequeststatus',$data);
    }


    /**
     * change request status for all request ids selected in the main page
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function SaveChangeRequestStatus(Request $request)
    {
        $request_ids = $request->input('request_ids');
        $rs_request_status = $request->input('rs_request_status');
        $request_ids_array = explode(",", $request_ids);
       
        
        for ($i = 0; $i < count($request_ids_array); $i++)
        {
            $request_info = MRequest::find($request_ids_array[$i]);
            
            // check old status if it's  greater priority then the new status soo we ignor changing
            $rs_old_request_status = $request_info->fk_status_id;
            if($rs_old_request_status < $rs_request_status)
            {
                $result_array = array();
                
                $result_array['is_error']     = 1;
                $result_array['error_msg']  = "You Canno't go back in Request Status Please change To a New Status";
                return Response()->json($result_array);
            }
            
            $request_info->fk_status_id = $rs_request_status;
            $request_info->save();
        }
        
        $result_array = array();
        $result_array['is_error'] = 0;
        return Response()->json($result_array);

    }
}
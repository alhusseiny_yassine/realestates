<?php
/***********************************************************
CustomersController.php
Product :
Version : 1.0
Release : 2
Date Created : Aug 29, 2017
Developed By  : Alhusseiny Yassine   PHP Department A&H S.A.R.L

Page Description :
{Enter page description Here}
***********************************************************/

namespace App\Http\Controllers\Customers;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use App\Http\Controllers\Controller;
use App\model\Customers\Customers;


class CustomersController extends Controller
{
    /**
     * 
     * @return Object View 
     * Open Page Of Customers
     */
    public function CutomersManagement(){
        return Response()->view('customers.customers');
    }
    /**
     * 
     * @param Request $request
     * @return json List of customers with pagination
     */
    public function DisplayListCustomers(Request $request) { 
        {
            $page_number        = $request->input('page_number');
            $c_customer_type    = $request->input('c_customer_type');
            $c_fullname         = $request->input('c_fullname');
            $c_email            = $request->input('c_email');
            $c_mobile           = $request->input('c_mobile');
            $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');
        }
        
        if ($page_number > 1) {
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages;
        } else {
            $skip = 0;
        }
        
        $customers_obj = Customers::whereCIsDeleted(0);
        
        if ($c_customer_type != '') {
            $customers_obj = $customers_obj->whereCCustomerType($c_customer_type);
        }

        if (strlen($c_fullname) > 0) {
            $customers_obj = $customers_obj->whereCFullname($c_fullname);
        }

        if (strlen($c_email) > 0) {
            $customers_obj = $customers_obj->whereCEmail($c_email);
        }

        if (strlen($c_mobile) > 0) {
            $customers_obj = $customers_obj->whereCMobile($c_mobile);
        }
        
        $all_customers = $customers_obj->count();
        $total_pages   = ceil($all_customers/$nbr_rows_per_pages);
        
        if ($total_pages > 1) {
            $customers = $customers_obj->skip($skip)->take($nbr_rows_per_pages)->get();
        } else {
            $customers = $customers_obj->get();
        }
        
        $data = array(
            "customers" => $customers,
        );

        $result_array = array();
        $result_array['total_pages'] = $total_pages;
        $result_array['display'] = view("customers.displaylistcustomers",$data)->render();

        return Response()->json($result_array);
    }
    
    public function AddCustomer() {
        return Response()->view('customers.addcustomer');
    }
    
    public function EditCustomer($c_id) {
        $customer = Customers::find($c_id);
        $data = array (
            "customer" => $customer
        );
        return Response()->view('customers.editcustomer',$data);
    }
    
    public function CheckCustomerEmail(Request $request){
        $c_id             = $request->input('c_id');
        $c_email          = $request->input('c_email');
        $customers_count  = Customers::where('c_id','!=',$c_id)->whereCEmail($c_email)->count();
        $result_array     = array();
        $result_array['is_error'] = 0;
        
        if( $customers_count > 0 ) {
            $result_array['is_error'] = 1;
            $result_array['error_message'] = 'Email Already Exists';
        }
        
        return Response()->json($result_array);
    }
    
    public function SaveCustomerInfo(Request $request) {
        
        $c_customer_type = $request->input('c_customer_type');
        $c_email         = $request->input('c_email');
        $c_id            = $request->input('c_id');
        $c_fullname      = $request->input('c_fullname');
        $c_mobile        = $request->input('c_mobile');
        $c_phone         = $request->input('c_phone');
        $c_fax           = $request->input('c_fax');
        $c_address       = $request->input('c_address');
        
        $customer = new Customers();
        if( $c_id != ''){
            $customer = Customers::find($c_id);
        }
        $customer->c_customer_type = $c_customer_type;
        $customer->c_email         = $c_email;
        $customer->c_fullname      = $c_fullname;
        $customer->c_mobile        = $c_mobile;
        $customer->c_phone         = $c_phone;
        $customer->c_fax           = $c_fax;
        $customer->c_address       = $c_address;
        $customer->save();
        
        $result_array     = array();
        $result_array['is_error'] = 0;
        return Response()->json($result_array);
    }
    
    public function DeleteCustomer(Request $request) {
        $c_id     = $request->input('customer_id');
        $customer = Customers::find($c_id);
        $customer->delete();
        
        $result_array     = array();
        $result_array['is_error'] = 0;
        return Response()->json($result_array);
    }

//////////////////////////////////////////////////////////////////////////
}
<?php
/***********************************************************
CustomersController.php
Product :
Version : 1.0
Release : 2
Date Created : Aug 29, 2017
Developed By  : Alhusseiny Yassine   PHP Department A&H S.A.R.L

Page Description :
{Enter page description Here}
***********************************************************/

namespace App\Http\Controllers\General;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use DB;
use PDF;
use App\Http\Controllers\Controller;
use App\model\Customers\Customers;


class GeneralController extends Controller
{
    /**
     * 
     * @param type $object_type
     * @return PDF
     */
    public function GeneratePDF( $object_type ){
        
        
        switch ($object_type){
            case 'customers':
            {
                $customers = Customers::all();
                $data      = [
                    'customers' => $customers
                ];
                $view      = 'PDF.customers_pdf';
                $stream    = 'customers_pdf.pdf';
            }
            break;
        }
        
        $pdf = PDF::loadView($view,$data);
        return $pdf->stream($stream);
    }
    
    public function GenerateExcelSheet( $object_type ){
        
        switch ($object_type)
        {
            case "customers":
            {
                $customers = Customers::all();
                
                $file = 'customersFile-'.date("Y-M-D")."-".time().'.xls';
                
                    ob_start();
                    echo "<table border='1'> ";
                    echo "<tr><th> ID </th><th> Full Name </th><th> Email </th><th> Mobile </th><th> Type </th></tr>";
                    foreach ( $customers as $index => $customer ) {

                        $id         = $customer->c_id;
                        $c_fullname = $customer->c_fullname;
                        $c_email    = $customer->c_email;
                        $c_mobile   = $customer->c_mobile;
                        $c_customer_type = $customer->c_customer_type;

                        echo "<tr><td> $id </td><td> $c_fullname </td><td> $c_email </td><td> $c_mobile </td><td> $c_customer_type </td></tr>";
                    }
                    echo '</table>';
                    $content = ob_get_contents();
                    ob_end_clean();
                    header("Expires: 0");
                    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
                    header("Cache-Control: no-store, no-cache, must-revalidate");
                    header("Cache-Control: post-check=0, pre-check=0", false);
                    header("Pragma: no-cache");
                    header("Content-type: application/vnd.ms-excel;charset:UTF-8");
                    header('Content-length: ' . strlen($content));
                    header('Content-disposition: attachment; filename=' . basename($file));
                    echo $content;
                
                return;
            }
            break;
        }
        
    }
}


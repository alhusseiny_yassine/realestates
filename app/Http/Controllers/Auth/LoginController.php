<?php

namespace App\Http\Controllers\Auth;


use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use Crypt;
use DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\model\System\Roles;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Libraries\Users\UserManager;
use App\model\Users\UserTypes;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
   // protected $redirectTo = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Logout From Admin Section Reset Session And Authonticaiton
     *
     * @author Moe Mantach
     * @access public
     */
    public function logout()
    {
        if (isset($_COOKIE['ua_remember_user'])) {
            setcookie("ua_remember_user", "", time() - (86400 * 30) , "/");
            unset($_COOKIE['ua_remember_user']);
            
        }
        
       Auth::logout();
       Session::flush();
       return Redirect::to('administrator');
    }


    /**
     * Logout From trainee Section By Reset Session And Authontication Object
     *
     * @author Moe Mantach
     * @access public
     */
    public function TraineeLogout()
    {
        if (isset($_COOKIE['ut_remember_user'])) {
            setcookie("ut_remember_user", "", time() - (86400 * 30) , "/");
            unset($_COOKIE['ut_remember_user']);
            
        }
        Auth::logout();
        Session::flush();
        
        return Redirect::to('/');
    }


    /**
     * Trainees Login and save sesssion
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function TraineesLogin(Request $request)
    {
        $result_array = array();

        $ut_username    = $request->input('ut_username');
        $ut_password    = $request->input('ut_password');
        $user_type      = $request->input('ut_user_type');
        $ut_user_type   = Crypt::deCrypt( $user_type );
        $ut_remember    = $request->input('ut_remember');
        $ut_remember    = ($ut_remember == null) ? false : true;
         
        $ss_user_type = Session('ut_user_type');
        if( $ss_user_type == UserTypes::ADMIN_USER_TYPE_ID )
        {
            Auth::logout();
            Session::flush();
        }
        
        if (Auth::attempt(array('u_username' => $ut_username, 'password' => $ut_password),$ut_remember))
        {
            $user_info          = Auth::user();
            $log_in_user_type   = $user_info->u_user_type;
            // if the user access 
            if( $log_in_user_type == UserTypes::ADMIN_USER_TYPE_ID )
            {
                $result_array['is_error'] = 1;
                $result_array['error_msg'] = "error occurred during Login Please try again later";
                Auth::logout();
               
                
                return Response()->json($result_array);
            }

            // Save Session Information
            UserManager::SaveTraineeSessionInformaiton($user_info , $ut_remember);
            $result_array['is_error'] = 0;

        }
        else
        {
            $result_array['is_error'] = 1;
            $result_array['error_msg'] = "Invalid Username And/Or Password";
        }


        return Response()->json($result_array);
    }


    /**
     * Login Request check authentication Username and password
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function Login(Request $request)
    {
        $result_array = array();

        $username = $request->input('username');
        $password = $request->input('password');
        $user_type = $request->input('user_type');
        $user_type = Crypt::deCrypt( $user_type );
        $ua_remember    = $request->input('ua_remember');
        $ua_remember    = ($ua_remember == null) ? false : true;
        
        $ss_user_type = Session('ut_user_type');

        
       if (Auth::attempt(array('u_username' => $username, 'password' => $password),$ua_remember))
        {
            $user_info          = Auth::user();
            $log_in_user_type   = $user_info->u_user_type;

            // check if user type is administrator to be able to Enter the Admin Section
            /**if( $log_in_user_type != 1 || $log_in_user_type != $user_type )
            {
                $result_array['is_error'] = 1;
                $result_array['error_msg'] = "No Privilege to Enter In this Section";

                return Response()->json($result_array);
            }*/


            // Save information in the session
            UserManager::SaveSessionInformaiton($user_info , $ua_remember);
            $result_array['is_error'] = 0;
        }
        else
        {
            $result_array['is_error'] = 1;
            $result_array['error_msg'] = "Invalid Username And/Or Password";
        }

        return Response()->json($result_array);
    }
}

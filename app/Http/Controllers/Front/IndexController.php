<?php
/***********************************************************
IndexController.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 29, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Http\Controllers\Front;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use Crypt;
use Mail;
use DB;
use App\model\System\Roles;
use App\model\Users\Users;
use App\Libraries\Users\UserManager;
use App\model\Users\UserTypes;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Libraries\System\DashboardManagement;
use App\Libraries\Notifications\NotifcationsManager;
use App\model\Project\ProjectNotifications;
use App\model\System\Beneficiaries;
use App\model\Notifications\Notifications; 



class IndexController extends Controller
{

    /**
     * Trainee Reggistration function and send activation link
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function TraineesRegistration(Request $request)
    {
        $g_captcha_response       = $request->input('g-recaptcha-response');

        $result_array = array();
        $data = array(
            'secret' => "6LcQ_AcUAAAAALrNlgaEGgEp9zMqnIJoF8vuUdR8",
            'response' => $g_captcha_response
        );

        $verify = curl_init();
        curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($verify, CURLOPT_POST, true);
        curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
        $response = json_decode(curl_exec($verify), true);


        if( $response['success']== true )
        {
            $ur_email                 = $request->input('ur_email');
            $ur_password              = $request->input('ur_password');
            $ur_retype_password       = $request->input('ur_retype_password');
            $u_fullname               = $request->input('u_fullname');
            $ur_beneficiary_id        = $request->input('ur_beneficiary_id');
            $ur_coordinator_code      = $request->input('ur_coordinator_code');

            $cordinator_info = Users::whereUCoordinatorCode($ur_coordinator_code)->whereUUserType(UserTypes::COORDINATORS_USER_TYPE_ID)->get();
            $coordinator_id = $cordinator_info[0]['attributes']['id'];
             
            $coordinator_exist = UserManager::CheckCoordinatorCodeExist($ur_coordinator_code);
            if($coordinator_exist == 0)
            {
                $result_array['is_error'] = 1;
                $result_array['error_msg'] = "Invalid Coordinator Code please Check with Administration";
                return Response()->json($result_array);
            }

            $Users = new Users();
            $Users->u_user_type         = UserTypes::TRAINEES_USER_TYPE_ID;
            $Users->fk_role_id          = Roles::TRAINEES_ROLE_ID;
            $Users->u_username          = $ur_email;
            $Users->password            = Hash::make($ur_password);
            $Users->u_fullname          = $u_fullname;
            $Users->u_email             = $ur_email;
            $Users->fk_beneficiary_id   = $ur_beneficiary_id;
            $Users->u_coordinator_code  = $ur_coordinator_code;
            $Users->fk_coordinator_id   = $coordinator_id;
            $Users->u_employment_date   = date("Y-m-d");
            $Users->u_is_active  = 0;
            $current_date = date('Y-m-d H:i:s');
            $u_activation_key = encrypt($current_date);
            $u_activation_key = substr($u_activation_key, 0,32);
            $Users->u_activation_key  = $u_activation_key;
            $Users->save();
            $user_id = $Users->id;

            $view_params = array(
                'account_name' => $u_fullname,
                'activation_key' => $u_activation_key
            );

            $params_array = array(
                'view' => 'emails.activation',
                'email_subject' => 'Activation Email',
                'email_params' => $view_params,
                'account_email' => $ur_email,
                'account_name' => $u_fullname
            );
            SendEmail($params_array);
            
            
            
            // send notificaiton to the admmin and coordinator
            
            {// coordinator notifications
                
                
                $coordinator_fullname = $cordinator_info[0]['attributes']['u_fullname'];
                $benf_info = Beneficiaries::find($ur_beneficiary_id);    
                $params_array = array(
                    "notification_type" => NotifcationsManager::NOT_INFORMATION,
                    "coordinator" => $coordinator_fullname,
                    "full_name" => $u_fullname,
                    "user_name" => $ur_email,
                    "beneficiary" => $benf_info->sb_name,
                    "user_id" => $coordinator_id
                );
                NotifcationsManager::SendNotification($params_array);
            }
            
            
            {//administrator notifications
                $lst_users_admin = Users::whereUUserType(UserTypes::ADMIN_USER_TYPE_ID)->get();
                
                foreach ( $lst_users_admin as $index => $user_info ) 
                {
                    $params_array = array(
                        "notification_type" => NotifcationsManager::NOT_INFORMATION,
                        "coordinator" => $coordinator_fullname,
                        "full_name" => $u_fullname,
                        "user_name" => $ur_email,
                        "beneficiary" => $benf_info->sb_name,
                        "user_id" => $user_info->id
                    );
                    NotifcationsManager::SendNotification($params_array);
                }
            }
            

            $result_array['is_error'] = 0;
            $result_array['error_msg'] = "Pleace Check Your Email to activate your account";
        }
        else
        {
            $result_array['is_error'] = 1;
            $result_array['error_msg'] = "error occurred during Registration Please try again later";
        }


        return Response()->json($result_array);

    }

    /**
     * get all information related to the datashboard
     * and display it in the dahboard
     *
     * @author Moe Mantach
     * @access public
     * @return \Illuminate\Http\Response
     */
    public function TraineesDashboard()
    {
        $user_id = Session()->get('ut_user_id');
  
        if($user_id == null)
        {
            Auth::logout();
            Session::flush();
            return Redirect::to('/');
        }
        
        $params_array = array(
            'user_id' => $user_id
        );
        $dashboard_header = DashboardManagement::getHeaderCounter($params_array);

        // get list of requests Training to approve
        $projects_notifications_pending = ProjectNotifications::whereFkUserId($user_id)->whereNrNotificationStatus(0)->get();
        $projects_notifications             = ProjectNotifications::whereFkUserId($user_id)->whereIn('nr_notification_status',array(1,2))->get();
     
        $LstNotifications                      = Notifications::whereFkUserId($user_id)->get(); 
        
        $lst_users_obj   = Users::whereUIsDeleted(0)->whereUIsActive(1)->get();
        $users_array     = CreateDatabaseArrayByIndex($lst_users_obj , 'id');


        $data = array(
            "dashboard_header" => $dashboard_header['display'],
            "projects_notifications_pending" => $projects_notifications_pending,
            "lstNotifications" => $LstNotifications,
            "users_array" => $users_array,
            "projects_notifications" => $projects_notifications
        );
        return Response()->view('frontend.dashboard',$data);
    }
}
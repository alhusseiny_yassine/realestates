<?php
/***********************************************************
DashboardController.php
Product :
Version : 1.0
Release : 2
Date Created : Apr 17, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Http\Controllers\Front;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use Crypt;
use Mail;
use DB;
use App\model\System\Roles;
use App\model\Users\Users;
use App\Libraries\Users\UserManager;
use App\model\Users\UserTypes;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Libraries\System\DashboardManagement;
use App\model\Project\ProjectNotifications;



class DashboardController extends Controller
{

    /**
     * DIsplay Notification Widget for request to approve and list of requests approved and denied
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function NotificationWidget(Request $request)
    {

        $user_id = Session()->get('ut_user_id');


        // get list of requests Training to approve
        $projects_notifications_pending = ProjectNotifications::whereFkUserId($user_id)->whereNrNotificationStatus(0)->get();
        $projects_notifications = ProjectNotifications::whereFkUserId($user_id)->whereIn('nr_notification_status',array(1,2))->get();

        $result_array = array();
        $lst_users_obj   = Users::whereUIsDeleted(0)->whereUIsActive(1)->get();
        $users_array     = CreateDatabaseArrayByIndex($lst_users_obj , 'id');

        $params = array(
            "projects_notifications_pending" => $projects_notifications_pending,
            "projects_notifications" => $projects_notifications,
            "users_array" => $users_array,
        );
        $result_array['display'] = view('frontend.dashboard.requests_action',$params)->render();
        $result_array['is_error'] = 0;

        return Response()->json($result_array);
    }
}
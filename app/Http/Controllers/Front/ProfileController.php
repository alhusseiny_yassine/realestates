<?php
/***********************************************************
ProfileController.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 30, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Http\Controllers\Front;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use Crypt;
use Mail;
use DB;
use App\model\System\Roles;
use App\model\Requests\Request as MRequest;
use App\model\Users\Users;
use App\Libraries\Users\UserManager;
use App\model\Users\UserTypes;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\model\Requests\RequestStatus;
use App\model\Project\ProjectGroupTrainees;
use App\model\Project\Projects;
use App\model\System\Days;
use App\model\System\Districts;
use App\model\Project\ProjectClasses;
use App\model\Sessions\SessionClass;
use App\model\Project\ProjectGroups;

class ProfileController
{

    /**
     * Display page of my profile in front end
     *
     * @author Moe Mantach
     * @access public
     * @return \Illuminate\Http\Response
     */
    public function MyProfileManagement()
    {
        $ut_user_id             = Session('ut_user_id');
        $ut_user_type         = Session('ut_user_type');
        $user_info = Auth()->user();
        $Projects = new Projects();
        $Requests = new MRequest();

        switch($ut_user_type)
        {
            case UserTypes::TRAINEES_USER_TYPE_ID:
            {
                $Requests = MRequest::whereRIsDeleted(0)->whereFkTraineeId($ut_user_id)->count();

                $ProjectGroupTrainees = ProjectGroupTrainees::whereFkTraineeId($ut_user_id)->get();

                $lst_project_ids = array();

                foreach ($ProjectGroupTrainees as $index => $p_info)
                {
                    $lst_project_ids[] = $p_info->fk_project_id;
                }
                array_unique($lst_project_ids);
                $Projects = count($lst_project_ids);
                
                // get group ids
                $Trainees_groups = ProjectGroupTrainees::whereFkTraineeId($ut_user_id)->get();
                $group_ids = array();
                
                foreach ( $Trainees_groups as $key => $tg_info ) {
                    $group_ids[] = $tg_info->fk_group_id;
                }
                
                if(count($group_ids) > 0)
                    $my_classes_info = ProjectClasses::whereIn("fk_group_id",$group_ids)->get();
                    else
                        $my_classes_info = ProjectClasses::where("fk_group_id",0)->get();
                        
                        
                // get class Ids
                $class_ids = array();
                
                foreach ($my_classes_info as $key => $cls_info)
                {
                    $class_ids[] = $cls_info->cls_id;
                }
                
                $todays_date = date("Y-m-d");
                
                if(count($class_ids) > 0)
                    $todays_sessions_info = SessionClass::whereIn('fk_class_id',$class_ids)->whereScSessionDate($todays_date)->count();
                    else
                        $todays_sessions_info = SessionClass::where('fk_class_id',0)->count();
                    
                $info = array(
                    "sessions" => $todays_sessions_info,
                    "classes" => count($class_ids),
                    "requests" => $Requests,
                );
                
            }
            break;
            case UserTypes::COORDINATORS_USER_TYPE_ID:
            {
                $info = array(
                    "sessions" => 0,
                    "classes" => 0,
                    "requests" => 0,
                );
            }
            break;
            case UserTypes::PROVIDERS_USER_TYPE_ID:
            {
                $info = array(
                    "sessions" => 0,
                    "classes" => 0,
                    "requests" => 0,
                );
            }
            break;
            case UserTypes::TRAINERS_USER_TYPE_ID:
            {
                // get classes for this trainer
                $trainers_group_count = ProjectGroups::wherePgClassTrainer($ut_user_id)->count();
                $todays_sessions_info = SessionClass::whereFkSessionTrainer_id($todays_date)->count();
                $info = array(
                    "sessions" => 0,
                    "classes" => $trainers_group_count,
                    "requests" => $todays_sessions_info,
                );
            }
            break;
        }
        
        

        /**$count_accepted_requests = MRequest::whereRIsDeleted(0)->whereFkStatusId(RequestStatus::REQUEST_STATUS_APPROVED_BY_OMSAR)->count();
        $count_project_group_trainees = ProjectGroupTrainees::whereFkTraineeId();*/

        // get information depand of user type



        $data = array(
            "user_info" => $user_info, 
            "info" => $info
        );
        return Response()->view('omsar.myprofile',$data);
    }


    /**
     * Display dashboard of my profile
     *
     * @author Moe Mantach
     * @access public
     */
    public function DisplayMyProfileDashboard()
    {
        $result_array = array();


        $data = array();
        $result_array['display'] = view('frontend.profile.dashboard',$data)->render();
        return Response()->json($result_array);
    }


    /**
     * Display My Profile Account Form
     *
     * @author Moe Mantach
     * @access public
     */
    public function DisplayMyProfileAccount()
    {
        $result_array = array();

        $user_id = Session('ut_user_id');

        $front_user_info = Users::find($user_id);
        $preferred_days  = Days::all();
        $lst_districts  = Districts::all();

        $data = array(
          "front_user_info" => $front_user_info,
          "preferred_days" => $preferred_days,
          "lst_districts" => $lst_districts,
        );
        $result_array['display'] = view('frontend.profile.settings',$data)->render();
        return Response()->json($result_array);
    }


    /**
     * Save Main Profile Information to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function SaveProfileInformation(Request $request)
    {
        $user_id = Session('ut_user_id');
        $u_fullname                                     = $request->input('u_fullname');
        $u_email                                        = $request->input('u_email');
        $u_phone                                        = $request->input('u_phone');
        $u_mobile                                       = $request->input('u_mobile');
        $u_address                                      = $request->input('u_address');
        $u_website                                      = $request->input('u_website');
        $u_emp_degree                                   = $request->input('u_emp_degree');
        $u_trainee_section                              = $request->input('u_trainee_section');
        $u_trainee_service                              = $request->input('u_trainee_service');
        $u_trainee_bureau                               = $request->input('u_trainee_bureau');
        $u_gov_grade                                    = $request->input('u_gov_grade');
        $u_grade_type                                   = $request->input('u_grade_type');
        $u_preferred_train_days                         = $request->input('u_preferred_train_days');
        $u_preferred_train_location                     = $request->input('u_preferred_train_location');


        $user_save_info = Users::find($user_id);
        $user_save_info->u_fullname                     = $u_fullname;
        $user_save_info->u_email                        = $u_email;
        $user_save_info->u_phone                        = $u_phone;
        $user_save_info->u_mobile                       = $u_mobile;
        $user_save_info->u_address                      = $u_address;
        $user_save_info->u_website                      = $u_website;
        $user_save_info->u_emp_degree                   = $u_emp_degree;
        $user_save_info->u_trainee_section              = $u_trainee_section;
        $user_save_info->u_trainee_service              = $u_trainee_service;
        $user_save_info->u_trainee_bureau               = $u_trainee_bureau;
        $user_save_info->u_gov_grade                    = $u_gov_grade;
        $user_save_info->u_grade_type                   = $u_grade_type;
        $user_save_info->u_preferred_train_days         = $u_preferred_train_days;
        $user_save_info->u_preferred_train_location     = $u_preferred_train_location;
        $user_save_info->save();

        $result_array = array();

        $result_array['error_msg'] = "Operation Completed Successfully";

        return Response()->json($result_array);
    }


    /**
     * Change Password for current User
     * @param Request $request
     */
    public function ChangePassword(Request $request)
    {
        $user_id = Session('ut_user_id');

        $result_array = array();
        $u_current_password     = $request->input('current_password');
        $u_new_password         = $request->input('new_password_password');
        $u_re_new_password      = $request->input('confirm_new_password_password');

        $current_profile_info = Users::find($user_id);

        if (!Hash::check($u_current_password, $current_profile_info->password))
        {
            $result_array['is_error'] = 1;
            $result_array['error_msg'] = "Please Insert Your Correct Old Password";

            return Response()->json($result_array);
        }

        $current_profile_info->password = Hash::make($u_new_password);
        $current_profile_info->save();

        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Completed Successfully";

        return Response()->json($result_array);
    }
}
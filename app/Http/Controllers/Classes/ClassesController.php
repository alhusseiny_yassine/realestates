<?php
/***********************************************************
ClassesController.php
Product :
Version : 1.0
Release : 2
Date Created : Dec 24, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

namespace App\Http\Controllers\Classes;

use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use Auth;
use Crypt;
use Mail;
use DB;
use App\model\System\Roles;
use App\model\Users\Users;
use App\Libraries\Users\UserManager;
use App\model\Users\UserTypes;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\model\Project\Projects;
use App\model\Project\ProjectCourses;
use App\model\System\Courses;
use App\model\Sessions\SessionClass;
use App\model\Project\ProjectClasses;
use App\Libraries\Sessions\SessionsManagement;
use App\model\Project\ProjectGroups;
use App\model\Project\ProjectGroupTrainees;
use App\Libraries\Projects\ProjectManagement;
use App\model\System\Days;
use App\model\System\Districts;
use App\model\Project\ProjectProvider;
use App\Libraries\Classes\ClassesManagement;
use App\model\Project\ProjectBeneficiaries;
use PDF;
use App\model\Surveys\Surveys;
use App\model\Surveys\SurveyQuestions;
use App\model\Surveys\SurveyAnswers;
use DateTime;


class ClassesController extends Controller
{

    /**
     * Page to display in the admin a class management where we can check all
     * trainees with every class we can see trainer responsible on this course
     *
     * @author Moe Mantach
     * @access public
     */
    public function ClassManagement()
    {
        $lst_projects = Projects::wherePpIsDeleted(0)->get();
        $data = array(
            'lst_projects' => $lst_projects
        );
        return Response()->view('classes.class',$data);
    }



    public function FrontClassManagement()
    {
        $lst_projects = Projects::wherePpIsDeleted(0)->get();
        $data = array(
            'lst_projects' => $lst_projects
        );
        return Response()->view('classes.front.class',$data);
    }


    /**
     * Display Page of My Classes Contain all classes
     * assign or related to this user
     * 
     * @author Moe Mantach
     * @access public
     * 
     * @return view Description
     */
    public function MyClassManagement()
    {
        $user_id = Session('ut_user_id');
        
      
        
       return Response()->view('classes.front.myclasses');
        
    }
    
    
    /**
     * Depand of parameters sent we display the data
     * 
     * @author Moe Mantach
     * @access public
     * @param Request $request
     * 
     * 
     * @return Array Json
     */
    public function DisplayMyClassesInfo(Request $request){
        $display_type 		= $request->input('display_type');
        $data_info 			= $request->input('data_info');
        $result_array       =  array();
        
        $data = array(
            'display_type' => $display_type,
            'data_info' => $data_info,
            'request' => $request
        );
        
        $result_array = ClassesManagement::DisplayMyClassesInfo($data);
        
        
        return Response()->json($result_array);
    }
            


    /**
     * get list of courses related to the selected Project
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function GetCoursesDropdown( Request $request )
    {

        $project_id = $request->input('project_id');


        $project_course_info = SessionsManagement::getLstCoursesInProject($project_id);


        $result_array = array();
        $data = array(
           "project_course_info" => $project_course_info
        );

        $result_array = view('course.coursesdropdown',$data)->render();
        return Response()->json($result_array);
    }


    /**
     * Display List of Sessions in the selected course of selected project
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function DisplayListSessions( Request $request )
    {
        $project_id         = $request->input('project_id');
        $project_course_id  = $request->input('course_id');
        $display_type       = $request->input('display_type');
        $page_number        = $request->input('page_number');
        $nbr_rows_per_pages = Config::get('appconfig.max_rows_per_page');


        $project_classes = ProjectClasses::whereFkPcourseId($project_course_id)->whereFkProjectId($project_id)->get();

        // check if we have class created to the selected course in the selected project
        if(count($project_classes) == 0)
        {
            $result_array = array();

            $result_array['is_error'] = 1;
            $result_array['error_msg'] = 'No Sessions In this Course Please Get Back to OMSAR Administrator';
            return Response()->json($result_array);
        }

        $cls_id = $project_classes[0]->cls_id;
        $class_start_date = $project_classes[0]->cls_start_date;



        if($page_number > 1)
            $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
        else
            $skip = 0;



        $all_session_classes = SessionClass::whereFkClassId($cls_id)->count();


        $total_pages = ceil($all_session_classes/$nbr_rows_per_pages);
        $total_pages = intval($total_pages);


        // get list of Session for this project courses
        $SessionClasses = SessionClass::whereFkClassId($cls_id)->skip($skip)->take($nbr_rows_per_pages)->get();

        $result_array   = array();

        switch ($display_type)
        {
            case "list":
                {
                    $data = array(
                        "SessionClasses" => $SessionClasses
                    );
                    $result_array['display'] = view('classes.displaylist',$data)->render();
                    $result_array['total_pages'] = $total_pages;
                }
            break;
            case "calendar":
                {
                    $data_info = SessionsManagement::getSessionsDatainformation($project_id , $project_course_id);

                    $calendar_url = urldecode($data_info['session_calendar_path']);

                    $data = array();
                    $result_array['calendar_url']           = $calendar_url;
                    $result_array['display']                = view('classes.sessioncalendar',$data)->render();
                    $result_array['class_start_date']       = $class_start_date;
                }
            break;
        }

        return Response()->json($result_array);
    }


    /**
     * Delete Session Class from the Project Class
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function DeleteSessionClass( Request $request )
    {
        $sc_id = $request->input('sc_id');

        // Delete Session from the database
        $SessionClass = new SessionClass();
        $SessionClass->sc_id = $sc_id;
        $SessionClass->delete();

        $result_array = array();

        $result_array['is_error'] = 0;
        $result_array['error_msg'] = 'Operation Complete Successfully';

        return Response()->json($result_array);
    }


    /**
     * Open popup to Create New Project Group
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function CreateNewProjectGroup( $project_id )
    {
        $group_code = md5(date("Y-m-d H:i:s"));
        $group_code = "GRP-" . substr($group_code, 0,10);
        $daysInfo = Days::all();
        $lst_trainers = UserManager::getListofTrainers();
        $ProjectCourses = ProjectManagement::getLstProjectCourses($project_id);
        $lst_districts  = Districts::all();
        $project_providers = ProjectProvider::whereFkProjectId($project_id)->get();
        $providers_array = array();

        foreach ($project_providers as $key => $pp_info) {
            $providers_array[] = $pp_info->fk_provider_id;
        }


        $lst_providers = Users::whereIn('id',$providers_array)->get();

        $data = array(
            "group_code" => $group_code,
            "daysInfo" => $daysInfo,
            "lst_trainers" => $lst_trainers,
            "lst_providers" => $lst_providers,
            "lst_districts" => $lst_districts,
            "ProjectCourses" => $ProjectCourses
        );
        return Response()->view('projects.addgroup',$data);
    }


    /**
     * Save Group information in the database
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function SaveGroupInformation( Request $request )
    {
        $pg_id                = $request->input('pg_id');
        $pg_group_code        = $request->input('pg_group_code');
        $project_id           = $request->input('project_id');
        $pg_group_title       = $request->input('pg_group_title');
        $pg_group_description = $request->input('pg_group_description');
        $sl_train_days        = $request->input('sl_train_days');
        $sl_trainer           = $request->input('sl_trainer');
        $pg_class_provider    = $request->input('pg_class_provider');
        $fk_pcourse_id        = $request->input('sl_course');
        $pg_train_location    = $request->input('pg_train_location');
        $pg_train_location    = strlen($pg_train_location) > 0 ? $pg_train_location : 0;
        $pg_trainee_time      = $request->input('pg_trainee_time');
        $pg_class_trainer     = $request->input('pg_class_trainer');
        $pg_start_date        = $request->input('pg_start_date');
        $pg_end_date          = $request->input('pg_end_date');
        $generate_class       = $request->input('generate_class');

        $project_groups = new ProjectGroups();

        if($pg_id === null)
        {
            $project_groups->pg_group_code = $pg_group_code;
        }
        else
        {
            $project_groups = ProjectGroups::find($pg_id);

        }

        $project_groups->fk_project_id          = $project_id;
        $project_groups->pg_group_title         = $pg_group_title;
        $project_groups->pg_group_description   = $pg_group_description;
        $project_groups->pg_train_days          = $sl_train_days;
        $project_groups->pg_class_trainer       = $sl_trainer;
        $project_groups->pg_class_provider      = $pg_class_provider;
        $project_groups->pg_start_date          = $pg_start_date;
        $project_groups->pg_end_date            = $pg_end_date;
        $project_groups->fk_pcourse_id          = $fk_pcourse_id;
        $project_groups->pg_trainee_time        = $pg_trainee_time;
        $project_groups->pg_trainee_location_id = $pg_train_location;   
        $project_groups->save();
        
        if ($generate_class == 1) {
            $params_array = array(
                'project_id' => $project_id
            );
            
            $projectManagement = new ProjectManagement();

            $projectManagement->GenerateClassesProject($params_array);
            $projectManagement->GenerateSessionsCourses($params_array);
        }

        $result_array = array();
        $result_array['is_error'] = 0;
        $result_array['error_msg'] = "Save Trainees Group information";


        return Response()->json($result_array);
    }


    public function DisplayListGroups( Request $request )
    {
        $project_id = $request->input('project_id');

        $project_group_info = ProjectGroups::whereFkProjectId($project_id)->get();

        $lst_trainees_group = ProjectManagement::GetLstTraineesInProjectGroup($project_id);


        $result_array = array();

        $data = array(
            "project_group_info" => $project_group_info,
            "lst_trainees_group" => $lst_trainees_group,
        );
        $result_array['display'] = view('projects.config.groups',$data)->render();
        return Response()->json($result_array);
    }

    /**
     * Display Popup to edit project group
     * @param unknown $pg_id
     * @return \Illuminate\Http\Response
     */
    public function EditProjectGroupPopUp( $pg_id )
    {
        $group_info = ProjectGroups::find($pg_id);

        $daysInfo = Days::all();
        $lst_trainers = UserManager::getListofTrainers();
        $ProjectCourses = ProjectManagement::getLstProjectCourses( $group_info->fk_project_id );
        $lst_districts  = Districts::all();
        $project_providers = ProjectProvider::whereFkProjectId( $group_info->fk_project_id )->get();

        $providers_array = array();

        foreach ($project_providers as $key => $pp_info) {
            $providers_array[] = $pp_info->fk_provider_id;
        }


        $lst_providers = Users::whereIn('id',$providers_array)->get();

        $result_array = array();

        $data = array(
            "group_info" => $group_info,
            "lst_trainers" => $lst_trainers,
            "lst_providers" => $lst_providers,
            "ProjectCourses" => $ProjectCourses,
            "daysInfo" => $daysInfo,
            "lst_districts" => $lst_districts,
            "pg_id" => $pg_id
        );

        return Response()->view('projects.editgroup',$data);
    }

    /**
     * Add New Trainee to Group in project
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function AddNewTraineeToGroup( Request $request )
    {

        $pg_id      = $request->input('pg_id');
        $project_id = $request->input('project_id');
        $user_id    = $request->input('user_id');

        // delete trainee if existin other group
        DB::table('proj_projects_group_trainees')->where('fk_project_id','=',$project_id)->where('fk_trainee_id','=',$user_id)->delete();

        // add the new trainee to the group
        $projectGroupTrainees = new ProjectGroupTrainees();
        $projectGroupTrainees->fk_project_id = $project_id;
        $projectGroupTrainees->fk_group_id = $pg_id;
        $projectGroupTrainees->fk_trainee_id = $user_id;
        $projectGroupTrainees->save();

        $result_array = array();

        $result_array['is_error'] = 0;
        $result_array['error_msg'] = "Trainee Added to selected Group";

        return Response()->json($result_array);
    }

    /**
     * Delete Trainee from a group in a current Project
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function DeleteTraineeFromGroup( Request $request )
    {
        $project_id = $request->input('project_id');
        $user_id    = $request->input('user_id');


        DB::table('proj_projects_group_trainees')->where('fk_project_id','=',$project_id)->where('fk_trainee_id','=',$user_id)->delete();


        $result_array = array();

        $result_array['is_error'] = 0;
        $result_array['error_msg'] = "Trainee Removed From Group";

        return Response()->json($result_array);
    }


    /**
     * Display Trainees Attendances Management where every trainer can take attendances for
     *
     * @author Moe Mantach
     * @access public
     */
    public function TraineesAttendances()
    {
        $lst_projects = Projects::wherePpIsDeleted(0)->get();
        $data = array(
            'lst_projects' => $lst_projects
        );
        return Response()->view('classes.traineesAttendances',$data);
    }

     public function FrontTraineesAttendances()
    {   
       
        $user_id                      = Auth::id();
        $user_type                  = Session('ut_user_type');
        $benf_id                      = Session('ut_beneficiary_id');
        
        $lst_projects = new Projects();
        switch ($user_type)
        {
            case UserTypes::TRAINEES_USER_TYPE_ID :
                {
                    $project_group_trainees = ProjectGroupTrainees::whereFkTraineeId($user_id)->get();
                    $project_ids = array();
                    
                    foreach ( $project_group_trainees as $key => $pg_info )
                    {
                        $project_ids[] = $pg_info->fk_project_id;
                    }
                    
                    
                    if(count($project_ids) > 0)
                    {
                        $lst_projects                = Projects::wherePpIsDeleted(0)->whereIn('pp_id',$project_ids)->get();
                    }
                    
                    
                }
                break;
            case UserTypes::COORDINATORS_USER_TYPE_ID :
                {
                    // get projects of this beneficier
                    $ProjectBenf = ProjectBeneficiaries::whereFkBenfId($benf_id)->get();
                    $project_ids = array();
                    
                    foreach ( $ProjectBenf as $key => $pb_info )
                    {
                        $project_ids[] = $pb_info->fk_project_id;
                    }
                    
                    if(count($project_ids) > 0)
                    {
                        $lst_projects                = Projects::wherePpIsDeleted(0)->whereIn('pp_id',$project_ids)->get();
                    }
                }
                break;
            case UserTypes::TRAINERS_USER_TYPE_ID :
                {
                    $ProjectGroups = ProjectGroups::wherePgClassTrainer($user_id)->get();
                    
                    $project_ids = array();
                    
                    foreach ( $ProjectGroups as $key => $pg_info )
                    {
                        $project_ids[] = $pg_info->fk_project_id;
                    }
                    
                    if(count($project_ids) > 0)
                    {
                        $lst_projects                = Projects::wherePpIsDeleted(0)->whereIn('pp_id',$project_ids)->get();
                    }
                    
                    
                }
                break;
        }
        
        
        $data = array(
            'lst_projects' => $lst_projects
        );
        return Response()->view('classes.front.traineesAttendances',$data);
    }


    public function TraineesTestResults()
    {
        $lst_projects = Projects::wherePpIsDeleted(0)->get();
        $data = array(
            'lst_projects' => $lst_projects
        );
        return Response()->view('classes.TraineesTestResults',$data);
    }

    public function FrontTraineesTestResults()
    {
        $user_id                      = Auth::id();
        $user_type                  = Session('ut_user_type');
        $benf_id                      = Session('ut_beneficiary_id');
        
        $lst_projects = new Projects();
        switch ($user_type)
        {
            case UserTypes::TRAINEES_USER_TYPE_ID :
                {
                    $project_group_trainees = ProjectGroupTrainees::whereFkTraineeId($user_id)->get();
                    $project_ids = array();
                    
                    foreach ( $project_group_trainees as $key => $pg_info ) 
                    {
                        $project_ids[] = $pg_info->fk_project_id;
                    }
                    
                    
                    if(count($project_ids) > 0)
                    {
                        $lst_projects                = Projects::wherePpIsDeleted(0)->whereIn('pp_id',$project_ids)->get();
                    }
                    
                    
                }
             break;
            case UserTypes::COORDINATORS_USER_TYPE_ID :
                {
                    // get projects of this beneficier
                    $ProjectBenf = ProjectBeneficiaries::whereFkBenfId($benf_id)->get();
                    $project_ids = array();
                    
                    foreach ( $ProjectBenf as $key => $pb_info ) 
                    {
                        $project_ids[] = $pb_info->fk_project_id;
                    }
                    
                    if(count($project_ids) > 0)
                    {
                        $lst_projects                = Projects::wherePpIsDeleted(0)->whereIn('pp_id',$project_ids)->get();
                    }
                }
             break;
            case UserTypes::TRAINERS_USER_TYPE_ID :
                {
                    $ProjectGroups = ProjectGroups::wherePgClassTrainer($user_id)->get();
                    
                    $project_ids = array();
                    
                    foreach ( $ProjectGroups as $key => $pg_info )
                    {
                        $project_ids[] = $pg_info->fk_project_id;
                    }
                    
                    if(count($project_ids) > 0)
                    {
                        $lst_projects                = Projects::wherePpIsDeleted(0)->whereIn('pp_id',$project_ids)->get();
                    }
                    
                    
                }
             break;
        }
        
        $data = array(
            'lst_projects' => $lst_projects
        );
        return Response()->view('classes.front.TraineesTestResults',$data);
    }


    /**
     * Get Classes Dropdown
     *
     * @author Moe Mantach
     * @access public
     * @param Request $request
     */
    public function GetClassesDropdown( Request $request )
    {
        $project_id                 = $request->input('project_id');
        $ProjectClasses = ProjectClasses::whereFkProjectId($project_id)->get();

        $result_array = array();
        $result_array['is_error'] = 0;
        $data = array(
            "ProjectClasses" => $ProjectClasses
        );
        $result_array['display'] = view('classes.classesDropdown',$data)->render();
        return Response()->json($result_array);
    }

    
    public function GetClassesDropdownFront( Request $request )
    {
        $project_id                 = $request->input('project_id');
        $user_id                    =  $request->input('user_id');
        
        $ut_user_id                 = Auth::id();
        $user_type                  = Session('ut_user_type');
        $benf_id                      = Session('ut_beneficiary_id');
        
        $lst_classes = new ProjectClasses();
        
        
        switch ($user_type)
        {
            case UserTypes::TRAINEES_USER_TYPE_ID :
                {
                    $ProjectGroupTrainees = ProjectGroupTrainees::whereFkTraineeId($user_id)->whereFkProjectId($project_id)->get();
                    $group_ids = array();
                    
                    foreach ($ProjectGroupTrainees as $key => $pg_info ) {
                        $group_ids[] = $pg_info->fk_group_id;
                    }
                    
                    $lst_classes = ProjectClasses::whereIn('fk_group_id',$group_ids)->get();
                    
                }
                break;
            case UserTypes::COORDINATORS_USER_TYPE_ID :
                {
                    // get projects of this beneficier
                    $ProjectBenf = ProjectBeneficiaries::whereFkBenfId($benf_id)->get();
        
                    
                    $lst_trainees = Users::whereFkCoordinatorId($ut_user_id)->whereUUserType(UserTypes::TRAINEES_USER_TYPE_ID)->get();
                    
                    $trainee_ids = array();
                    foreach ( $lst_trainees as $key => $tr_info ) 
                    {
                        $trainee_ids[] = $tr_info->id;
                    }
                    
                    
                    $ProjectGroupTrainees = ProjectGroupTrainees::whereIn('fk_trainee_id',$trainee_ids)->whereFkProjectId($project_id)->get();
                    $group_ids = array();
                    
                    foreach ($ProjectGroupTrainees as $key => $pg_info ) {
                        $group_ids[] = $pg_info->fk_group_id;
                    } 
                  
                    $lst_classes = ProjectClasses::whereIn('fk_group_id',$group_ids)->whereFkProjectId($project_id)->get();
 
                }
                break;
            case UserTypes::TRAINERS_USER_TYPE_ID :
                {
                    $ProjectGroups = ProjectGroups::where('fk_project_id',$project_id)->where('pg_class_trainer',$ut_user_id)->get();
                    
                    $group_ids = array();
                    
                    foreach ($ProjectGroups as $key => $pg_info ) {
                        $group_ids[] = $pg_info->pg_id;
                    }
                    $lst_classes = ProjectClasses::whereIn('fk_group_id',$group_ids)->whereFkProjectId($project_id)->get();
                    
                    
                }
                break;
        }
        
        
   

        $result_array = array();
        $result_array['is_error'] = 0;
        $data = array(
            "ProjectClasses" => $lst_classes
        );
        $result_array['display'] = view('classes.classesDropdown',$data)->render();
        return Response()->json($result_array);
    }
    
    public function GenerateClassPdf(Request $request, $pg_id) {
        
        $project_classes = ProjectClasses::whereFkGroupId($pg_id)->get()->toArray();
        $class_array = array();
        
        for ($i = 0; $i < count($project_classes); $i++) 
        {
            $class_array[] = $project_classes[$i]['cls_id'];
        }
        
        if (count($class_array) == 0) 
        {
            $error_pdf = PDF::loadView('classes.myclasses.error_pdf', [
                        'error_message' => 'No Classes Assigned !'
            ]);
            return $error_pdf->stream('error_pdf.pdf');
        }
        
        $class_sessions = SessionClass::whereFkClassId($class_array[0])->get()->toArray();
        if (count($class_sessions) == 0) 
        {
            $error_pdf = PDF::loadView('classes.myclasses.error_pdf', [
                        'error_message' => 'No Sessions Assigned !'
            ]);
            return $error_pdf->stream('error_pdf.pdf');
        }

        $project_group = ProjectGroups::wherePgId($pg_id)->get()->toArray();
        
        if (count($project_group) == 0)
        {
            $error_pdf = PDF::loadView('classes.myclasses.error_pdf', [
                        'error_message' => 'No Groups Assigned !'
            ]);
            return $error_pdf->stream('error_pdf.pdf');
        }
        
        $fk_pcourse_id = $project_group[0]['fk_pcourse_id'];
        $project_course = ProjectCourses::wherePcId($fk_pcourse_id)->get()->toArray();
        
        if (count($project_course) == 0) 
        {
            $error_pdf = PDF::loadView('classes.myclasses.error_pdf', [
                        'error_message' => 'No Course Assigned !'
            ]);
            return $error_pdf->stream('error_pdf.pdf');
        }
       
        $fk_course_id   = $project_course[0]['fk_course_id'];
        $crs_trainer_id = $project_course[0]['fk_trainer_id'];
     
        $session_trainers_array = array();
        for ($i = 0; $i < count($class_sessions); $i++) 
        {
            $session_trainers_array[] = $class_sessions[$i]['fk_session_trainer_id'];
        }
        
        $session_trainers = Users::whereIn('id', $session_trainers_array)->get()->toArray();
        $course_trainer   = Users::whereId($crs_trainer_id)->get()->toArray();
      
        if (count($course_trainer) == 0)
        {
            $error_pdf = PDF::loadView('classes.myclasses.error_pdf', [
                        'error_message' => 'No Trainers Assigned !'
            ]);
          
            return $error_pdf->stream('error_pdf.pdf');
        }
       
        $course_objct = Courses::whereCId($fk_course_id)->get()->toArray();
        if (count($course_objct) == 0) 
        {
            $error_pdf = PDF::loadView('classes.myclasses.error_pdf', [
                        'error_message' => 'No Course Assigned !'
            ]);
            return $error_pdf->stream('error_pdf.pdf');
        }
        
        
        $data = [
            'project_class' => $project_classes[0],
            'class_sessions' => $class_sessions,
            'course' => $course_objct[0],
            'course_trainer' => $course_trainer[0],
            'session_trainers' => $session_trainers
        ];
        
        $class_pdf = PDF::loadView('classes.myclasses.class_pdf',$data);
        return $class_pdf->stream('class_pdf.pdf');
    }
    /**
     * 
     * @param type $pg_id
     * @return Object View 
     * @description View List of of questions of a survey in frontend
     */
    public function ViewSurvey($pg_id){
        
        $project_group = ProjectGroups::wherePgId($pg_id)->get()->toArray();
        
        if (count($project_group) == 0)
        {
            return Response()->view('classes.myclasses.error',array("error" => "No groups Assigned"));
        }
        
        $fk_pcourse_id = $project_group[0]['fk_pcourse_id'];
        $project_course = ProjectCourses::wherePcId($fk_pcourse_id)->get()->toArray();
        
        if (count($project_course) == 0) 
        {
            return Response()->view('classes.myclasses.error',array("error" => 'No Course Assigned !'));
        }
        
        $fk_project_id  = $project_course[0]['fk_project_id'];
        $fk_course_id   = $project_course[0]['fk_course_id'];
        
        $survey           = Surveys::whereFkProjectId($fk_project_id)
                ->whereFkCourseId($fk_course_id)->whereSIsDeleted(0)->get();
        
        if (count($survey) == 0) 
        {
            return Response()->view('classes.myclasses.error',array("error" => 'No Survey For this Course !'));
        }
        
        $survey_questions = SurveyQuestions::whereFkSurveyId($survey[0]->s_id)
                ->whereSqIsDeleted(0)->orderBy('sq_order','asc')->get();
        
        $data = array(
            'survey_questions' => $survey_questions,
            's_id'            => $survey[0]->s_id
        );
        
        return Response()->view('classes.myclasses.viewsurvey',$data);
        
    }
    /**
     * 
     * @param Request $request
     * @return json
     * save answer submitted for the survey
     */
    public function SubmitSurvey(Request $request){
        
        $answers      = $request->input('sq_question');
        $fk_survey_id = $request->input('s_id');
        $fk_trainee_id = Auth::user()->id;
        
        foreach($answers as $fk_question_id => $sa_answer )
        {
            $answer = new SurveyAnswers();
            $answer->fk_survey_id = $fk_survey_id;
            $answer->fk_trainee_id = $fk_trainee_id;
            $answer->fk_question_id = $fk_question_id;
            $answer->sa_answer = $sa_answer;
            $answer->sa_date = new DateTime();
            $answer->save();
            
        }
        
         $result_array['is_error'] = 0;
         return Response()->json($result_array);
         
    }

}
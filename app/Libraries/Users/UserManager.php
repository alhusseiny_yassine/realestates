<?php
/***********************************************************
UserUtilities.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 22, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad MantachCOPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Libraries\Users;


use Validator;
use Input;
use Config;
use Session;
use Redirect;
use Crypt;
use Cookie;
use Auth;
use DB;
use File;
use App\model\Users\Users;
use Illuminate\Support\Facades\Hash;
use App\model\Users\UserTypes;
use App\model\System\RolePrivileges;
use App\model\Requests\RequestStatus;


class UserManager
{
    /**
     * Save Session Information to the database
     *
     * @author Moe mantach
     * @access public
     * @param unknown $user_info
     */
    public static function SaveSessionInformaiton( $user_info , $ua_remember )
    {
        $user_id = $user_info->id;
        $role_id = $user_info->fk_role_id;

        $profile_path     = public_path().'/'.Config::get('constants.PROFILE_PICTURE_PATH') . $user_info->u_avatar_base_src . $user_info->u_avatar_filename . "." . $user_info->u_avatar_extentions;
        $profile_url = url('/').'/'.Config::get('constants.PROFILE_PICTURE_PATH') . $user_info->u_avatar_base_src . $user_info->u_avatar_filename . "." . $user_info->u_avatar_extentions;
        if(!is_file($profile_path))
        {
            $profile_url = url('imgs/avatar.png');
        }
     
        session()->put('user_id' , $user_id );
        session()->put('user_profile_url' , $profile_url);
        session()->put('beneficiary_id' , $user_info->fk_beneficiary_id);
        session()->put('user_fullname' , $user_info->u_fullname);
        session()->put('user_email' , $user_info->u_email);
        session()->put('user_name' , $user_info->u_username);
        session()->put('user_type' , $user_info->u_user_type);

        $role_info = RolePrivileges::getPrivileges($role_id);

        session()->put('role_info' ,$role_info);
        // save cookie to remember user id and information
  
        if($ua_remember == true)
        {
            if($user_info->u_user_type == UserTypes::ADMIN_USER_TYPE_ID )
            {

                $cookie_value =  $user_info->u_user_type . "-" . $user_id . "-" . $profile_url . "-" . $user_info->fk_beneficiary_id . "-" . $user_info->u_fullname . "-" . $user_info->u_email;
                $cookie_value = encrypt($cookie_value);

                setcookie('ua_remember_user', $cookie_value, time() + (86400 * 30), "/");

            }

        }
    }


    public static function SaveTraineeSessionInformaiton( $user_info , $ut_remember )
    {
        $user_id = $user_info->id;
        $role_id = $user_info->fk_role_id;
        $profile_path     = public_path().'/'.Config::get('constants.PROFILE_PICTURE_PATH') . $user_info->u_avatar_base_src . $user_info->u_avatar_filename . "." . $user_info->u_avatar_extentions;
        $profile_url = url('/').'/'.Config::get('constants.PROFILE_PICTURE_PATH') . $user_info->u_avatar_base_src . $user_info->u_avatar_filename . "." . $user_info->u_avatar_extentions;
        if(!is_file($profile_path))
        {
            $profile_url = url('imgs/avatar.png');
        }
        session()->put('ut_user_id' , $user_id );
        session()->put('ut_user_profile_url' , $profile_url);
        session()->put('ut_beneficiary_id' , $user_info->fk_beneficiary_id);
        session()->put('ut_user_fullname' , $user_info->u_fullname);
        session()->put('ut_user_type' , $user_info->u_user_type);
        session()->put('ut_user_email' , $user_info->u_email);
        session()->put('ut_coordinator_id' , $user_info->fk_coordinator_id);
        $role_info = RolePrivileges::getPrivileges($role_id);
        session()->put('role_info' ,$role_info);
        // save cookie to remember user id and information
        if($ut_remember == true)
        {
            if($user_info->u_user_type != UserTypes::ADMIN_USER_TYPE_ID )
            {

              $cookie_value =  $user_info->u_user_type . "-" . $user_id . "-" . $profile_url . "-" . $user_info->fk_beneficiary_id . "-" . $user_info->u_fullname . $user_info->u_email;
              $cookie_value = encrypt($cookie_value);
              setcookie('ut_remember_user', $cookie_value, time() + (86400 * 30), "/");
            }

        }

    }


    /**
     * get list of privilege for Request Statuses
     *
     * @author Moe Mantach
     * @access public
     * @static
     */
    public static function GetListOfRequestStatusPrivilege()
    {
        $RequestStatuses = RequestStatus::whereRsIsDeleted(0)->get();
        $role_info       = session('role_info');

        $request_status_array = array();
        foreach ($RequestStatuses as $index => $info) {
            $request_status_array[$info->rs_id] = isset($role_info[ "request_status_" . $info->rs_id]) ? $role_info[ "request_status_" . $info->rs_id] : "deny";
        }

        return $request_status_array;
    }

    /**
     *
     * @param unknown $ur_coordinator_code
     * @return number
     */
    public static function CheckCoordinatorCodeExist( $ur_coordinator_code )
    {
        $coordinator = Users::whereUCoordinatorCode( $ur_coordinator_code )->whereUIsActive(1)->whereUUserType( UserTypes::COORDINATORS_USER_TYPE_ID )->count();
        return $coordinator > 0 ? 1 : 0;
    }


    /**
     * get user information from activation key
     *
     * @author Moe Mantach
     * @access public
     * @param String $activation_key
     *
     * @return Object $user_info
     */
    public static function GetUserInfoByActivationKey( $activation_key )
    {
        $user_info = Users::whereUActivationKey($activation_key)->whereUIsDeleted(0)->get();
        return $user_info;
    }


    public static function getallUsersById()
    {
        $user_info = Users::whereUIsActive(1)->whereUIsDeleted(0)->get();
        $lst_users_data = array();

        foreach ($user_info as $index => $user) {
            $lst_users_data[ $user->id ]['username'] = $user->u_username;
            $lst_users_data[ $user->id ]['fullname'] = $user->u_fullname;
        }


        return $lst_users_data;
    }

    /**
     * Get list of all user types
     */
    public static function getUserTypes()
    {
        $user_types = UserTypes::all();
        $ut_array = array();
        foreach ($user_types as $index => $type_info) {
            $ut_array[] = $type_info->ut_id;
        }

        return $ut_array;
    }
    
    /**
     * Get list of all user types
     */
    public static function getUserNameTypes()
    {
        $user_types = UserTypes::all();
        $ut_array = array();
        foreach ($user_types as $index => $type_info) {
            $ut_array[$type_info->ut_id] = $type_info->ut_user_type;
        }

        return $ut_array;
    }


    /**
     * get list of trainers saved in the database
     *
     * @author Moe Mantach
     * @access public
     * @param Array $params
     */
    public static function getListofTrainers( $params = array() )
    {
        $list_trainers = DB::table('users')->where('u_user_type','=',UserTypes::TRAINERS_USER_TYPE_ID)->get();

        return $list_trainers;
    }


    public static function UserLogin( $params_array )
    {
        $result_array = array();

        $username = $params_array['username'];
        $password = $params_array['password'];
        if (Auth::attempt(array('u_username' => $username, 'password' => $password)))
        {
            $user_info          = Auth::user();
            $log_in_user_type   = $user_info->u_user_type;


            // Save information in the session
            UserManager::SaveSessionInformaiton($user_info , 0);
            $result_array['is_error'] = 0;
        }
        else
        {
            $result_array['is_error'] = 1;
            $result_array['error_msg'] = "Invalid Username And/Or Password";
        }

        return json_encode($result_array,true);
    }
    /**
     * 
     * @param type $user_id
     * @return array
     * @description Function that handles the upload of profile picture
     */
    public static function UploadUserProfile($user_id){

        if($user_id != null){
            
            self::DeleteUserProfile($user_id);////Delete The old Profile in case of edit
            
        }
        
        $response  = array();
        $file_name = $_FILES['u_profile_pic']['name'];
        $file_type = $_FILES['u_profile_pic']['type'];
        $file_tmp  = $_FILES['u_profile_pic']['tmp_name'];
        $base_dir  = date('Y/m/d/');
        $directory = public_path() . "/" . Config::get('constants.PROFILE_PICTURE_PATH') . $base_dir;
        $main_url  = url('/') . "/" . Config::get('constants.PROFILE_PICTURE_PATH') . $base_dir;
        
        if (!is_dir($directory)) {
            $result = File::makeDirectory($directory, 0777, true);
        }

        $file_info = explode(".", $file_name);
        $extention = $file_info[count($file_info) - 1];
        $file_name = md5(date("Y-m-d H:i:s")) . "_" . date("YmdHis") . "_" . rand(0, 8888888);
        $file_path = $directory . $file_name . "." . $extention;
        $image_url = $main_url . $file_name . "." . $extention;


        if (move_uploaded_file($file_tmp, $file_path)) {

            $u_avatar_base_src   = $base_dir;
            $u_avatar_filename   = $file_name;
            $u_avatar_extentions = $extention;
            $response['is_error']= 0;
        }
        
        if ($response['is_error'] == 0) {

            $data = array(
                "u_avatar_base_src" => $u_avatar_base_src,
                "u_avatar_filename" => $u_avatar_filename,
                "u_avatar_extentions" => $u_avatar_extentions,
            );
            $response['data'] = $data;
        }
        
        return $response;
    }
    /**
     * 
     * @param type $user_id
     * @description delete the existing profile picture
     */
    public static function DeleteUserProfile($user_id){
        
        $user_info = Users::find($user_id);
        $u_avatar_base_src   = $user_info->u_avatar_base_src;
        $u_avatar_filename   = $user_info->u_avatar_filename;
        $u_avatar_extentions = $user_info->u_avatar_extentions;
        $image_src_path= public_path() . "/" .Config::get('constants.PROFILE_PICTURE_PATH').$u_avatar_base_src.$u_avatar_filename.".".$u_avatar_extentions;
        
        if(file_exists($image_src_path)){/////Find the related image to the product
            unlink($image_src_path);
        }
        
    }

}
<?php
/***********************************************************
ProjectManagement.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 8, 2016
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Libraries\Projects;


use Validator;
use Input;
use Config;
use Session;
use Redirect;
use Crypt;
use Auth;
use DB;
use App\model\Requests\RequestStatus;
use App\model\Requests\Request as MRequests;
use Illuminate\Support\Facades\Hash;
use App\model\Project\Projects;
use App\model\Project\ProjectCourses;
use App\model\Project\ProjectClasses;
use App\model\System\Courses;
use App\model\System\Days;
use App\model\Sessions\SessionClass;
use App\model\Sessions\SessionStatus;
use App\model\Project\ProjectGroups;
use App\model\Project\ProjectGroupTrainees;
use App\model\Project\ProjectCourseTrainees;
use App\model\Project\ProjectCoursesGroups;
use App\model\Project\ProjectTrainees;
use App\model\Project\ProjectRequests;
use App\model\Requests\RequestsCourses;
use App\model\Users\Users;


class ProjectManagement
{

    /**
     * get list of nbr of classes in every project we have
     *
     * @author Moe Mantach
     * @access public
     * @param Array $project_ids
     */
    public static function GetNbrOfCoursesInProject( $project_ids )
    {
         $nbr_courses = DB::table('proj_project_courses')->select(DB::raw('COUNT(fk_course_id) As nbr_of_courses') , 'fk_project_id')->whereIn('fk_project_id',$project_ids)->groupBy('fk_project_id')->get();
         $project_courses = array();
         for($i=0;$i<count($nbr_courses);$i++)
         {
             $project_courses[ $nbr_courses[$i]->fk_project_id ] = $nbr_courses[$i]->nbr_of_courses;
         }

         return $project_courses;
    }


    public static function GenerateProjectGroupsData( $params_array )
    {
        $project_id = $params_array['project_id'];
        
        $project_info = Projects::find($project_id);
        
        // get trainees
        $projectRequests = ProjectRequests::whereFkProjectId($project_id)->get();
        $requests_ids = array();
        
        foreach ($projectRequests as $index => $pr_info)
        {
            $requests_ids[] = $pr_info->fk_request_id;
        }
        
       
        $request_management_Obj = MRequests::whereIn('r_id',$requests_ids)->get();
 
        foreach ($request_management_Obj as $index => $rm_info)
        {
            $fk_trainee_id      = $rm_info->fk_trainee_id;
            $r_id               = $rm_info->r_id;
            $request_course_obj = RequestsCourses::whereFkRequestId($r_id)->get();
            $fk_course_id       = $request_course_obj[0]['attributes']['fk_course_id'];
            
            $trainee_obj                = Users::find($fk_trainee_id);
            $preferred_location         = $trainee_obj->u_preferred_train_location;
            $preferred_day              = $trainee_obj->u_preferred_train_days;
            $preferred_time_request     = $trainee_obj->u_preferred_train_time;
            $preferred_time             = CheckTimeFrame($preferred_time_request);
            $course_info                = Courses::find($fk_course_id);
            $course_name                = $course_info->c_course_name;
            $project_course_info        = ProjectCourses::whereFkCourseId($fk_course_id)->whereFkProjectId($project_id)->get();
            
            // if count is 0 we create a new group to fill in it all trainees
            $project_course_id         = strlen($project_course_info[0]['attributes']['pc_id']) > 0 ? $project_course_info[0]['attributes']['pc_id'] : 0;
            $pc_max_nbr_trainees       = $project_course_info[0]['attributes']['pc_max_nbr_trainees'];
            $preferred_location        = strlen($preferred_location) > 0 ? $preferred_location : 0;
            
            // check number of groups in current project and courses
            $ProjectCourseGroups = ProjectGroups::whereFkProjectId($project_id)->whereFkPcourseId($project_course_id)->wherePgIsDeleted(0)->get();
            
            
            
            $time = "";
            switch ($preferred_time)
            {
                case 1:
                    {
                        $time = "08:00:00";
                    }
                    break;
                case 2:
                    {
                        $time = "12:00:00";
                    }
                    break;
                case 3:
                    {
                        $time = "16:00:00";
                    }
                    break;
            }
            
            if(count($ProjectCourseGroups) == 0)
            {
                $count = count($ProjectCourseGroups) + 1;
                $pg_group_code = "GRP" . "-" . $project_course_id . "-" . $count;
                $projectGroup = new ProjectGroups();
                $projectGroup->fk_project_id            = $project_id;
                $projectGroup->fk_pcourse_id            = $project_course_id;
                $projectGroup->pg_group_code            = $pg_group_code;
                $projectGroup->pg_group_title           = $course_name;
                $projectGroup->pg_start_date            = $project_info->pp_project_start_date;
                $projectGroup->pg_end_date              = $project_info->pp_project_end_date;
                $projectGroup->pg_group_description     = $course_name;
                $projectGroup->pg_train_days            = $preferred_day;
                $projectGroup->pg_trainee_time          = $time;
                $projectGroup->pg_trainee_location_id   = $preferred_location;
                $projectGroup->save();
                $group_id = $projectGroup->pg_id;
                
                
                $project_groups_data[$project_course_id][$preferred_location][$preferred_day][$preferred_time][$group_id][]=  $fk_trainee_id;
            }
            else 
               {
                   $course_groups_array = array();
             
                   foreach ($ProjectCourseGroups as $key => $pg_info) 
                   {
                       $pg_group_id          = $pg_info->pg_id;
                       $fk_pcourse_id        = $pg_info->fk_pcourse_id;
                       $location_id          = $pg_info->pg_trainee_location_id;
                       $location_id          = ($location_id != null) ? $location_id : 0;
                       $train_day_id         = $pg_info->pg_train_days;
                       $train_day_id         = ($train_day_id != null) ? $train_day_id : 0;
                       $train_time_id        = $pg_info->pg_trainee_time;
                       $train_time_id        = ($train_time_id != null) ? $train_time_id : "8:00:00";
                       $train_time_id        = (string)$train_time_id;
                       
                       $time = "";
                       switch ($train_time_id)
                       {
                           case "08:00:00":
                               {
                                   $time =1;
                               }
                               break;
                           case "12:00:00":
                               {
                                   $time = 2;
                               }
                               break;
                           case "16:00:00":
                               {
                                   $time = 3;
                               }
                               break;
                       }
                       
                       
                       $CourseProjectInfo    = ProjectCourses::find($fk_pcourse_id);
                       $GroupTrainees        =  ProjectGroupTrainees::whereFkProjectId($project_id)->whereFkGroupId($pg_group_id)->count();
                       $pc_max_nbr_trainees  = $CourseProjectInfo->pc_max_nbr_trainees;
                        
                       
                       $trainees_array = isset( $project_groups_data[$fk_pcourse_id][$location_id][$train_day_id][$time][$pg_group_id] ) ? $project_groups_data[$fk_pcourse_id][$location_id][$train_day_id][$time][$pg_group_id] : array();

                       
                       if(count($trainees_array) <= $pc_max_nbr_trainees )
                       {
                           // check if this trainee already exist in the current group
                           $IsGroupTrainee        =  ProjectGroupTrainees::whereFkProjectId($project_id)->whereFkGroupId($pg_group_id)->whereFkTraineeId($fk_trainee_id)->count();
                           if($IsGroupTrainee == 0 || in_array($fk_trainee_id, $trainees_array) !== null)
                           {
                               $project_groups_data[$fk_pcourse_id][$location_id][$train_day_id][$time][$pg_group_id][] = $fk_trainee_id;
                              
                           }
                           else
                                  {
                               continue;
                           }
                       }
                       else 
                            { 
                                $count = count($ProjectCourseGroups) + 1;
                                $pg_group_code = "GRP" . "-" . $project_course_id . "-" . $count;
                                // if we reach the max number of trainees per group we will Create a new Group and Save the Trainees There
                                $projectGroup = new ProjectGroups();
                                $projectGroup->fk_project_id            = $project_id;
                                $projectGroup->fk_pcourse_id            = $project_course_id;
                                $projectGroup->pg_group_code            = $pg_group_code;
                                $projectGroup->pg_group_title           = $course_name;
                                $projectGroup->pg_start_date            = $project_info->pp_project_start_date;
                                $projectGroup->pg_end_date              = $project_info->pp_project_end_date;
                                $projectGroup->pg_group_description     = $course_name;
                                $projectGroup->pg_train_days            = $preferred_day;
                                $projectGroup->pg_trainee_time          = $train_time_id;
                                $projectGroup->pg_trainee_location_id   = $location_id;
                                $projectGroup->save();
                                $group_id = $projectGroup->pg_id;
                                $project_groups_data[$project_course_id][$preferred_location][$preferred_day][$time][$group_id][] = $fk_trainee_id;
                       }
                       
                       
                    }
                    
                    
            }
            
           
            
        } 
       
        return $project_groups_data;
        
    }
    
    /**
     * Generate Array list of all trainees and re-order
     * @param unknown $params_array
     */
    public static function GenerateProjectGroupsData1( $params_array )
    {
        $project_id = $params_array['project_id'];

        $project_info = Projects::find($project_id);

        // get trainees
        $projectRequests = ProjectRequests::whereFkProjectId($project_id)->get();
        $requests_ids = array();

        foreach ($projectRequests as $index => $pr_info)
        {
           $requests_ids[] = $pr_info->fk_request_id;
        }



        $request_management_Obj = MRequests::whereIn('r_id',$requests_ids)->get();

        $project_groups_data = array();
        $trainees_count = array();
        $group_index = array();
        $count = 0 ;
        $group_id = 0;
      foreach ($request_management_Obj as $index => $rm_info)
        {
            
            
            $fk_trainee_id      = $rm_info->fk_trainee_id;
            $r_id               = $rm_info->r_id;
            $request_course_obj = RequestsCourses::whereFkRequestId($r_id)->get();
            $fk_course_id       = $request_course_obj[0]['attributes']['fk_course_id'];

            $trainee_obj                = Users::find($fk_trainee_id);
            $preferred_location         = $trainee_obj->u_preferred_train_location;
            $preferred_day              = $trainee_obj->u_preferred_train_days;
            $preferred_time_request     = $trainee_obj->u_preferred_train_time;
            $preferred_time             = CheckTimeFrame($preferred_time_request);
            $course_info                = Courses::find($fk_course_id);
            $course_name                = $course_info->c_course_name;
            $project_course_info        = ProjectCourses::whereFkCourseId($fk_course_id)->whereFkProjectId($project_id)->get();

            // if count is 0 we create a new group to fill in it all trainees
            $project_course_id         = strlen($project_course_info[0]['attributes']['pc_id']) > 0 ? $project_course_info[0]['attributes']['pc_id'] : 0;
            $pc_max_nbr_trainees       = $project_course_info[0]['attributes']['pc_max_nbr_trainees'];
            $preferred_location        = strlen($preferred_location) > 0 ? $preferred_location : 0;

            // check number of groups in current project and courses
            $ProjectCourseGroups = ProjectGroups::whereFkProjectId($project_id)->whereFkPcourseId($project_course_id)->wherePgIsDeleted(0)->get();



            //if($count == 0)
            {
                $pg_group_code = "GRP" . "-" . $project_course_id;
                $time = "";
                switch ($preferred_time)
                {
                    case 1:
                        {
                            $time = "08:00:00";
                        }
                        break;
                    case 2:
                        {
                            $time = "12:00:00";
                        }
                        break;
                    case 3:
                        {
                            $time = "16:00:00";
                        }
                        break;
                }


                if(count($ProjectCourseGroups) == 0)
                {
                    $projectGroup = new ProjectGroups();
                    $projectGroup->fk_project_id            = $project_id;
                    $projectGroup->fk_pcourse_id            = $project_course_id;
                    $projectGroup->pg_group_code            = $pg_group_code;
                    $projectGroup->pg_group_title           = $course_name;
                    $projectGroup->pg_start_date            = $project_info->pp_project_start_date;
                    $projectGroup->pg_end_date              = $project_info->pp_project_end_date;
                    $projectGroup->pg_group_description     = $course_name;
                    $projectGroup->pg_train_days            = $preferred_day;
                    $projectGroup->pg_trainee_time          = $time;
                    $projectGroup->pg_trainee_location_id   = $preferred_location;
                    $projectGroup->save();
                    $group_id = $projectGroup->pg_id;

                    if(count($project_course_info) > 0)
                    {
                        $project_groups_data[$project_course_id][$preferred_location][$preferred_day][$preferred_time][$group_id][] = $fk_trainee_id;
                        $GroupTrainees = count($project_groups_data[$project_course_id][$preferred_location][$preferred_day][$preferred_time][$group_id]);

                    }
                    else
                         {
                        continue;
                    }
                }
                else
                    {

                      // for loop on all groups exist if have count == $pc_max_nbr_trainees
                      foreach ($ProjectCourseGroups as $key => $pg_info)
                      {
                          $pg_group_id          = $pg_info->pg_id;
                          $fk_pcourse_id        = $pg_info->fk_pcourse_id;
                          $location_id          = $pg_info->pg_trainee_location_id;
                          $location_id          = ($location_id != null) ? $location_id : 0;
                          $train_day_id         = $pg_info->pg_train_days;
                          $train_day_id         = ($train_day_id != null) ? $train_day_id : 0;
                          $train_time_id        = $pg_info->pg_trainee_time;
                          $train_time_id        = ($train_time_id != null) ? $train_time_id : "8:00:00";
                          $train_time_id        = (string)$train_time_id;
                          $CourseProjectInfo    = ProjectCourses::find($fk_pcourse_id);
                          $GroupTrainees        =  ProjectGroupTrainees::whereFkProjectId($project_id)->whereFkGroupId($pg_group_id)->count();
                          $pc_max_nbr_trainees  = $CourseProjectInfo->pc_max_nbr_trainees;


                          $trainees_array = isset( $project_groups_data[$fk_pcourse_id][$location_id][$train_day_id][$train_time_id][$pg_group_id] ) ? $project_groups_data[$fk_pcourse_id][$location_id][$train_day_id][$train_time_id][$pg_group_id] : array();
                            
                    
                          
                          if(count($trainees_array) <= $pc_max_nbr_trainees )
                          {
                              // check if this trainee already exist in the current group
                              $IsGroupTrainee        =  ProjectGroupTrainees::whereFkProjectId($project_id)->whereFkGroupId($pg_group_id)->whereFkTraineeId($fk_trainee_id)->count();
                              if($IsGroupTrainee == 0 || in_array($fk_trainee_id, $trainees_array) !== null)
                              {
                                  $project_groups_data[$fk_pcourse_id][$location_id][$train_day_id][$train_time_id][$pg_group_id][] = $fk_trainee_id;
                              }
                              else
                                     {
                                 continue;
                              }
                          }
                          else
                                {
                              $projectGroup = new ProjectGroups();
                              $projectGroup->fk_project_id            = $project_id;
                              $projectGroup->fk_pcourse_id            = $project_course_id;
                              $projectGroup->pg_group_code            = $pg_group_code;
                              $projectGroup->pg_group_title           = $course_name;
                              $projectGroup->pg_start_date            = $project_info->pp_project_start_date;
                              $projectGroup->pg_end_date              = $project_info->pp_project_end_date;
                              $projectGroup->pg_group_description     = $course_name;
                              $projectGroup->pg_train_days            = $preferred_day;
                              $projectGroup->pg_trainee_time          = $time;
                              $projectGroup->pg_trainee_location_id   = $location_id;
                              $projectGroup->save();
                              $group_id = $projectGroup->pg_id;
                              $project_groups_data[$project_course_id][$preferred_location][$preferred_day][$preferred_time][$group_id][] = $fk_trainee_id;

                          }
                      }

                }


            }


        }

        return $project_groups_data;
    }

    /**
     * get list course Project and return within array
     *
     * @author Moe Mantach
     * @param Integer $project_id
     */
    public static function getLstProjectCourses( $project_id )
    {
        $project_courses_result = DB::table('proj_project_courses')->leftJoin('sys_courses', 'proj_project_courses.fk_course_id', '=', 'sys_courses.c_id')->where('fk_project_id',$project_id)->where('pc_is_deleted',0)->get();
        return $project_courses_result;
    }

    public static  function GetNbrOfTraineesInProject( $project_ids )
    {
        $nbr_trainees = DB::table('proj_project_trainees')->select(DB::raw('COUNT(fk_trainee_id) As nbr_of_trainees') , 'fk_proj_id')->whereIn('fk_proj_id',$project_ids)->groupBy('fk_proj_id')->get();
        $project_trainees = array();
        for($i=0;$i<count($nbr_trainees);$i++)
        {
            $project_trainees[ $nbr_trainees[$i]->fk_proj_id ] = $nbr_trainees[$i]->nbr_of_trainees;
        }

        return $project_trainees;
    }



    public static  function GetNbrOfClassesInProject( $project_ids )
    {
        $nbr_classes = DB::table('proj_classes')->select(DB::raw('COUNT(fk_pcourse_id) As nbr_of_classes') , 'fk_project_id')->whereIn('fk_project_id',$project_ids)->groupBy('fk_project_id')->get();
        $project_classes = array();
        for($i=0;$i<count($nbr_classes);$i++)
        {
         $project_classes[ $nbr_classes[$i]->fk_project_id ] = $nbr_classes[$i]->nbr_of_classes;
        }

        return $project_classes;
    }


    public function SaveProjectInformation( $request )
    {
        $project_id = $request->input('project_id');

    }


    /**
     * Generate By Default groups for every courses and put all trainees
     * in the same course
     *
     * @author Moe Mantach
     * @access public
     * @param Array $params
     *
     *
     */
    public function GenerateGroupsProject( $params )
    {
       $project_id = $params['project_id'];

       $ProjectInfo = Projects::find($project_id);
       $params_array = array(
            "project_id" => $project_id
       );
       $project_data_array = ProjectManagement::GenerateProjectGroupsData($params_array);

       // get list of courses in this project
       $project_courses = ProjectCourses::whereFkProjectId($project_id)->get();
       $project_courses_array = array();

       foreach ($project_courses as $index => $pc_info) {
          $project_courses_array[$pc_info->pc_id]['pc_max_nbr_trainees']    = $pc_info->pc_max_nbr_trainees;
          $project_courses_array[$pc_info->pc_id]['pc_course_weight']       = $pc_info->pc_course_weight;
          $project_courses_array[$pc_info->pc_id]['pc_nbr_sessions']        = $pc_info->pc_nbr_sessions;
       }
       // save groups based on the groups array generated

       foreach ( $project_data_array as $course_id => $course_info )
       {
            foreach ($course_info as $location_id => $location_info)
            {
                foreach ($location_info as $day_id => $day_info )
                {
                    foreach ($day_info as $time_index => $trainees_group_info )
                    {

                        foreach ($trainees_group_info as $group_id => $trainees_info)
                        {
                            foreach ($trainees_info as $index => $trainee_id)
                            {

                                $CheckExist = ProjectGroupTrainees::whereFkProjectId($project_id)->whereFkGroupId($group_id)->whereFkTraineeId($trainee_id)->get();
                                if(count($CheckExist) == 0)
                                {
                                    $ProjectGroupsTrainee = new ProjectGroupTrainees();
                                    $ProjectGroupsTrainee->fk_project_id = $project_id;
                                    $ProjectGroupsTrainee->fk_group_id = $group_id;
                                    $ProjectGroupsTrainee->fk_trainee_id = $trainee_id;
                                    $ProjectGroupsTrainee->save();
                                }

                            }
                        }
                    }
                }
            }
       }

    }

    /**
     * Generate Classes for every course selected in
     * this Project
     *
     * @author Moe Mantach
     * @param Array $params
     * @return boolean
     */
    public function GenerateClassesProject( $params )
    {
        $project_id   =     $params['project_id'];

        $project_info           = Projects::find($project_id);


        $project_start_date = $project_info->pp_project_start_date;
        $project_end_date   = $project_info->pp_project_end_date;
        $pp_project_title   = $project_info->pp_project_title;
        $pp_project_title = str_replace(" ", "", $pp_project_title);
        $pp_project_title = substr($pp_project_title, 0,5);

        // get groups related to this Project
        $projectGroups = ProjectGroups::whereFkProjectId($project_id)->get();

        $group_ids = array();

        foreach ($projectGroups as $pg_index => $pg_info)
        {
            $group_ids[] = $pg_info->pg_id;
        }



        $ProjectCourseGroups = DB::table('proj_project_course_groups')->whereIn('fk_group_id',$group_ids)->get();
        //$ProjectCourseGroups = ProjectCoursesGroups::whereIn('fk_group_id',$group_ids)->where('fk_project_id',$project_id)->get();

        $course_ids = array();
        $project_courses_info = new ProjectCourses();
        if(count($ProjectCourseGroups)  > 0)
        {
            for ($i = 0; $i < count($ProjectCourseGroups); $i++)
            {
                $course_ids[] = $ProjectCourseGroups[$i]->fk_course_id;
            }
            $project_courses_info = ProjectCourses::whereIn('pc_id',$course_ids)->get();
        }
        else
        {
            $project_courses_info = ProjectCourses::where('fk_project_id',$project_id)->get();
        }


        // create classes for all groups in the project
        foreach ($project_courses_info as $pc_index => $pc_info)
        {
            $group_info = ProjectGroups::whereFkPcourseId($pc_info->pc_id)->wherePgIsDeleted(0)->get();
            //If the group has no classes
            if(count($group_info)<=0) continue;

            $group_id = 0;
            $group_start_date = "";
            $group_end_date = "";
            foreach ($group_info as $key => $grp_info) {
                $group_id = $grp_info->pg_id;
                $group_start_date   = $grp_info->pg_start_date;
                $group_end_date     = $grp_info->pg_end_date;
            }


            $course_id = $pc_info->fk_course_id;
            $md5 = md5(date("YmdHis"));
            $md5 = substr($md5, 0 , 5);
            $class_code = "CLS" . $pp_project_title . "-" . $md5;
            $class_code = (String)$class_code;


            // check if we have class for current group if we don't have we create a new class else we ignore
            $old_project_class = ProjectClasses::whereFkProjectId($project_id)->whereFkGroupId($group_id)->count();

            if($old_project_class > 0)
                continue;

            $project_class_info = new ProjectClasses();
            $project_class_info->fk_pcourse_id      = $pc_info->pc_id;
            $project_class_info->fk_project_id      = $project_id;
            $project_class_info->fk_group_id        = $group_id;
            $project_class_info->cls_code           = $class_code;
            $project_class_info->cls_start_date     = $group_start_date;
            $project_class_info->cls_end_date       = $group_end_date ;
            $project_class_info->save();

        }

        return true;
    }



    /**
     * Generate Classes and Sessions Related to this Project
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $params
     */
    public function GenerateSessionsCourses( $params )
    {
        $project_id = $params['project_id'];

        {// get data information

            $project_info           = Projects::find($project_id);
            $project_courses_info   = ProjectCourses::whereFkProjectId($project_id)->get();
            $project_classes_info   = ProjectClasses::whereFkProjectId($project_id)->get();

            $project_start_date = $project_info->pp_project_start_date;
            $project_end_date   = $project_info->pp_project_end_date;

            $difference         = GetNumberOfDays($project_end_date, $project_start_date);
            $project_days       = $difference->days;
        }
        // for loop on all selected courses

        foreach ( $project_classes_info as $pc_index => $pc_info )
        {
             $group_id              = $pc_info->fk_group_id;
            $pcourse_id             = $pc_info->fk_pcourse_id;
            $class_id               = $pc_info->cls_id;
            $proj_group_info       = ProjectGroups::find($group_id);

            $proj_course_info       = ProjectCourses::find($pcourse_id);
            $course_id              = $proj_course_info->fk_course_id;
            // get course information
            $course_info = Courses::find($course_id);

            $session_numbers        = $proj_course_info->pc_nbr_sessions;
            $schedule_days          = $proj_group_info->pg_train_days;
            $days_info              = Days::find($schedule_days);

            $days_allowed = GetAllowedDays( $days_info );
            for ($i = 1; $i <= $project_days; $i++)
            {
                $timestamp      = strtotime($project_start_date ." + " . $i . " days");
                $current_date   = date("Y-m-d" ,$timestamp);
                $current_index  = date('w',$timestamp);

                // check if the current day is from days allowed to do a Training Session
                if($days_allowed[$current_index + 1] && $session_numbers > 0)
                {
                    $sessionClass = New SessionClass();
                    $sessionClass->fk_class_id              = $class_id;
                    $code = md5(date("YmdHis"));
                    $sessionClass->sc_session_code          = "SC-" . $code;
                    $sessionClass->fk_session_status_id     = SessionStatus::SESSION_STATUS_CREATED;
                    $sessionClass->sc_session_location      = " ";
                    $sessionClass->sc_session_date          = $current_date;
                    $sessionClass->sc_session_time          = $proj_group_info->pg_trainee_time;
                    $sessionClass->sc_session_location      = $proj_group_info->pg_trainee_location_id;
                    $sessionClass->fk_session_trainer_id    = $proj_group_info->pg_class_trainer;
                    $sessionClass->sc_session_duration      = 2;
                    $sessionClass->save();
                    $session_numbers--;
                }
            }


        }
    }

    /**
     * Delete All Project Information and Remove Project Row from the database
     *
     * @author Moe Mantach
     * @access public
     * @param Integer $project_id
     */
    public static function DeleteProjectAndAllInformation( $project_id )
    {
        DB::table('proj_project_provider')->where('fk_project_id','=',$project_id)->delete();
        DB::table('proj_project_courses')->where('fk_project_id','=',$project_id)->delete();
        DB::table('proj_project_requests')->where('fk_project_id','=',$project_id)->delete();
        DB::table('proj_project_trainees')->where('fk_proj_id','=',$project_id)->delete();
        DB::table('proj_project_trainers')->where('fk_prj_id','=',$project_id)->delete();
        DB::table('proj_projects')->where('pp_id','=',$project_id)->delete();
    }



    public static function GetLstTraineesInProjectGroup( $project_id )
    {
        $lst_groups_trainees = DB::table('proj_projects_group_trainees')->leftJoin('users', 'proj_projects_group_trainees.fk_trainee_id', '=', 'users.id')->where('fk_project_id','=',$project_id)->get();
        $project_trainees_group = array();

        for ($i = 0; $i < count($lst_groups_trainees); $i++) {
            $project_trainees_group[ $lst_groups_trainees[$i]->fk_group_id ][$i]['id'] = $lst_groups_trainees[$i]->fk_trainee_id;
            $project_trainees_group[ $lst_groups_trainees[$i]->fk_group_id ][$i]['full_name'] = $lst_groups_trainees[$i]->u_fullname;
        }

        return $project_trainees_group;
    }

    public static function GetLstAllTraineesInProject( $project_id )
    {
        $lst_groups_trainees = DB::table('proj_projects_group_trainees')->where('fk_project_id','=',$project_id)->get();

        $project_trainees_group = array();

        for ($i = 0; $i < count($lst_groups_trainees); $i++) {
            $project_trainees_group[] = $lst_groups_trainees[$i]->fk_trainee_id;
        }

        return $project_trainees_group;
    }
}
<?php
/***********************************************************
ProvinceManagement.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 28, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

namespace App\Libraries\System;


use Validator;
use Input;
use Config;
use Session;
use Redirect;
use Crypt;
use Auth;
use DB;
use App\model\System\Provinces;
use Illuminate\Support\Facades\Hash;


class ProvinceManagement
{

    /**
     * Get list of Provinces in array with Id Index
     *
     * @author Moe Mantach
     * @access public
     * @return multitype:NULL
     */
    public static function getProvincesById()
    {
        $prov_result_array = array();
        $all_provinces = Provinces::whereSpIsDeleted(0)->get();
        foreach ($all_provinces as $bf_index => $sb_info )
        {
           $prov_result_array[ $sb_info->sp_id ] = $sb_info->sp_provinces_name;
        }


        return $prov_result_array;
    }
}
<?php
/***********************************************************
UserUtilities.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 22, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad MantachCOPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Libraries\System;


use Validator;
use Input;
use Config;
use Session;
use Redirect;
use Crypt;
use Auth;
use DB;
use App\model\System\Beneficiaries;
use Illuminate\Support\Facades\Hash;


class BeneficiaryManagement
{

    /**
     * Get list of beneficiaries in array with Id Index
     *
     * @author Moe Mantach
     * @access public
     * @return multitype:NULL
     */
    public static function getBeneficiariesById()
    {
        $bnf_result_array = array();
        $all_beneficiaries = Beneficiaries::whereSbIsDeleted(0)->get();
        foreach ($all_beneficiaries as $bf_index => $sb_info )
        {
           $bnf_result_array[ $sb_info->sb_id ] = $sb_info->sb_name;
        }


        return $bnf_result_array;
    }
}
<?php
/***********************************************************
DashboardManagement.php
Product : 
Version : 1.0
Release : 2
Date Created : Nov 23, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
Dashboard class contain all functions related to the Dashboard
***********************************************************/


namespace App\Libraries\System;


use Validator;
use Input;
use Config;
use Session;
use Redirect;
use Crypt;
use Auth;
use DB;
use App\model\System\Beneficiaries;
use Illuminate\Support\Facades\Hash;
use App\model\Users\Users;
use App\model\Users\UserTypes;
use App\model\Requests\Request As MRequests;
use App\model\Requests\RequestStatus;
use App\model\Project\ProjectGroupTrainees;
use App\model\Project\ProjectClasses;
use App\model\Sessions\SessionClass;


class DashboardManagement
{

    /**
     * get counter of all information already sent
     * 
     * @author Moe Mantach
     * @access public
     * @param Array $params_array
     */
    public static function getHeaderCounter( $params_array )
    {
      $user_id = $params_array['user_id'];
        
      $user_info = Users::find($user_id);
      $user_type = isset( $user_info->u_user_type ) ? $user_info->u_user_type : 0;
      $result_array = array();
        
      switch($user_info->u_user_type)
      {
          case UserTypes::PROVIDERS_USER_TYPE_ID :
              {
                  $my_projects = DB::table('proj_project_provider')->where('fk_provider_id','=',$user_id)->get();
                  
                  $projects_array = array();
                  
                  for ($i = 0; $i < count($my_projects); $i++) 
                  {
                      $projects_array[] = $my_projects[$i]->fk_project_id;
                  }
                  
                  $provider_projects = DB::table('proj_projects')->whereIn('pp_id',$projects_array)->where('pp_is_deleted',0)->get();
                  
                   // get all projects budget  
                  $project_budget = 0;
                  
                  for ($i = 0; $i < count($provider_projects); $i++) {
                      $project_budget = $project_budget + $provider_projects[$i]->pp_project_budget;
                  }
                  // change format to be displayed in the money format 
                  $project_budget = strval($project_budget);
                  $project_budget = number_format($project_budget, 0);
                  
                  
                  // get number of projects
                  $nbr_of_projects = count($my_projects);

                 // get number of courses in all projects
                 $projects_courses = DB::table('proj_project_courses')->whereIn('fk_project_id',$projects_array)->count();
                 
                 
                 // get total number of trainees in all projects
                 $projects_trainees = DB::table('proj_project_trainees')->whereIn('fk_proj_id',$projects_array)->count();
                 
                   
                  $data = array(
                      'project_budget' => $project_budget,
                      'projects_courses' => $projects_courses,
                      'projects_trainees' => $projects_trainees,
                      'nbr_of_projects' => $nbr_of_projects
                  );
                  $result_array['display'] = view('frontend.dashboard.providers_header',$data)->render();
              }
           break;
          case UserTypes::TRAINEES_USER_TYPE_ID :
              {
                  // get requests Approved ,get requests Denied ,classes Registred , Today Sessions
                  
                  $my_approval_requests  = MRequests::whereFkTraineeId($user_id)->whereFkStatusId(RequestStatus::REQUEST_STATUS_APPROVED_BY_OMSAR)->count();
                  $my_denied_requests    = MRequests::whereFkTraineeId($user_id)->whereFkStatusId(RequestStatus::REQUEST_STATUS_CANCEL)->count();
                  
                  // get group ids 
                  $Trainees_groups = ProjectGroupTrainees::whereFkTraineeId($user_id)->get();
                  $group_ids = array();
                  
                  foreach ( $Trainees_groups as $key => $tg_info ) {
                      $group_ids[] = $tg_info->fk_group_id;
                  }
                  
                  if(count($group_ids) > 0)
                  $my_classes_info = ProjectClasses::whereIn("fk_group_id",$group_ids)->get();
                  else 
                      $my_classes_info = ProjectClasses::where("fk_group_id",0)->get();
                  
                      
                 // get class Ids
                 $class_ids = array();
                        
                  foreach ($my_classes_info as $key => $cls_info) 
                  {
                      $class_ids[] = $cls_info->cls_id;
                   }
                 
                   $todays_date = date("Y-m-d");
                   
                   if(count($class_ids) > 0)
                       $todays_sessions_info = SessionClass::whereIn('fk_class_id',$class_ids)->whereScSessionDate($todays_date)->count();
                   else 
                            $todays_sessions_info = SessionClass::where('fk_class_id',0)->count();
                      
                  $data = array(
                      "my_approval_requests" => $my_approval_requests,
                      "my_denied_requests" => $my_denied_requests,
                      "my_classes_info" => $my_classes_info,
                      "todays_sessions_info" => $todays_sessions_info,
                  );
                  
                  $result_array['display'] = view('frontend.dashboard.trainees_header',$data)->render();
              }
           break;
          case UserTypes::TRAINERS_USER_TYPE_ID :
              {
                  $result_array['display'] = "";
              }
           break;
          case UserTypes::COORDINATORS_USER_TYPE_ID :
              {
                  $result_array['display'] = "";
              }
           break;
      }
      
      return $result_array; 
    }
    
}
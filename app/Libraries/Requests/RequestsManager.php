<?php
/***********************************************************
RequestsManager.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 4, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :

class contain all functionality to manage Requests Data in the database
***********************************************************/


namespace App\Libraries\Requests;


use Validator;
use Input;
use Config;
use Session;
use Redirect;
use Crypt;
use Auth;
use DB;
use App\model\Requests\RequestStatus;
use Illuminate\Support\Facades\Hash;


class RequestsManager
{

    /**
     * Get list of request Statuses Array indexes by id
     *
     * @author Moe Mantach
     * @access public
     * static
     *
     * @return Array $db_result_array
     */
    public static function GetRequestStatusInfoById()
    {
        $all_request_statuses = RequestStatus::whereRsIsDeleted(0)->get();

        $db_result_array = array();

        for ($i = 0; $i < count($all_request_statuses); $i++) {
            $db_result_array[ $all_request_statuses[$i]->rs_id ]['status_name'] = $all_request_statuses[$i]->rs_status_name;
            $db_result_array[ $all_request_statuses[$i]->rs_id ]['status_color'] = $all_request_statuses[$i]->rs_status_color;
        }


        return $db_result_array;
    }
}
<?php
/***********************************************************
 ClassesManagement.php
 Product :
 Version : 1.0
 Release : 2
 Date Created : Oct 1, 2016
 Developed By  : Mohamad Mantach   PHP Department
 All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2017
 
 Page Description :
 {Enter page description Here}
 ***********************************************************/



namespace App\Libraries\Classes;


use Validator;
use Input;
use Config;
use Session;
use Redirect;
use Crypt;
use Auth;
use DB;
use App\model\System\Courses;
use App\model\System\CourseType;
use Illuminate\Support\Facades\Hash;
use App\model\Users\UserTypes;
use App\model\System\CourseCategories;
use App\model\Project\ProjectGroupTrainees;
use App\model\Project\Projects;
use App\model\Project\ProjectGroups;
use App\model\Project\ProjectClasses;
use App\model\Sessions\SessionClass;


class ClassesManagement
{
    /**
     * get data of my class page depand on what we need
     * 
     * @author Moe Mantach
     * @access public
     * @param unknown $data
     */
    public static function DisplayMyClassesInfo( $data )
    {
        $ut_user_id = Session('ut_user_id');
        $result_array = array();
        
        $project_trainees_group = ProjectGroupTrainees::whereFkTraineeId($ut_user_id)->get()->toArray();
        
        switch($data['data_info'])
        {
            case "projects":
            {
                $result_array['header_link'] = "";
                $project_array = array();
                
                for ($i = 0; $i < count($project_trainees_group); $i++) 
                {
                    $project_array[] = $project_trainees_group[$i]['fk_project_id'];
                }
                
                if(count($project_array) == 0)
                {
                    $result_array['is_error'] = 0;
                    $result_array['error_msg'] = 'No Project Assigned For You';
                    
                    return $result_array;
                }
                
                $projects_info = Projects::whereIn('pp_id',$project_array)->get()->toArray();
                $array = array(
                    'projects_info' => $projects_info
                );
                
                switch($data['display_type'])
                {
                    case "list":
                    {
                       
                        $result_array['display'] = view('classes.myclasses.lstmyprojects',$array)->render();
                    }
                    break;
                    case "thumbnails":
                    {
                        $result_array['display'] = view('classes.myclasses.thumbmyprojects',$array)->render();
                    }
                    break;
                }
            }
            break;
            case "classes":
            {
                $pp_id              = $data['request']->input('pp_id');
              
                $params = array(
                    'pp_id' => $pp_id,
                    'data_info' => $data['data_info']
                );
                $result_array['header_link'] = self::DisplayMyClassesHeaderLink($params);
                
                $groups_array = array();
                
                
               
                
                for ($i = 0; $i < count($project_trainees_group); $i++)
                {
                    $groups_array[] = $project_trainees_group[$i]['fk_group_id'];
                }
                
                $groups_info = ProjectGroups::whereIn('pg_id',$groups_array)->get()->toArray();
                
                if(count($groups_info) == 0)
                {
                    $result_array['is_error'] = 0;
                    $result_array['error_msg'] = 'No Classes Assigned Found For You';
                    
                    return $result_array;
                }
                
                
                
                $array = array(
                    'groups_info' => $groups_info
                );
                
                switch($data['display_type'])
                {
                    case "list":
                        {
                            
                            $result_array['display'] = view('classes.myclasses.lstmygroups',$array)->render();
                        }
                        break;
                    case "thumbnails":
                        {
                            $result_array['display'] = view('classes.myclasses.thumbmygroups',$array)->render();
                        }
                        break;
                }
                
            }
            break;
            case "sessions":
            {
                    $pg_id = $data['request']->input('pg_id');

                    $params = array(
                        'pg_id'     => $pg_id,
                        'data_info' => $data['data_info']
                    );
                    $result_array['header_link'] = self::DisplayMyClassesHeaderLink($params);

                    $project_classes             = ProjectClasses::whereFkGroupId($pg_id)->get()->toArray();
                    $class_array = array();
                    for ($i = 0; $i < count($project_classes); $i++) {
                        $class_array[] = $project_classes[$i]['cls_id'];
                    }
                    
                    if (count($class_array) == 0) {
                        $result_array['is_error'] = 1;
                        $result_array['error_msg'] = 'No Classes Assigned Found For You';
                        return $result_array;
                    }
                    
                    $class_sessions = SessionClass::whereFkClassId($class_array[0])->get()->toArray();
                    if (count($class_sessions) == 0) {
                       $result_array['is_error'] = 1;
                       $result_array['error_msg'] = 'No Sessions Assigned Found For You';
                       return $result_array;
                    }
                    
                    $result_array['is_error'] = 0;

                    $array = array(
                        'class_sessions' => $class_sessions
                    );

                    switch ($data['display_type']) {
                        case "list": {

                                $result_array['display'] = view('classes.myclasses.lstmysessions', $array)->render();
                            }
                            break;
                        case "thumbnails": {
                                $result_array['display'] = view('classes.myclasses.thumbmysessions', $array)->render();
                            }
                            break;
                    }
                }
                break;
        }
        
        return $result_array;
    }
    
    /**
     * Display My classes Header Link
     * 
     * @author Moe Mantach
     * @access public
     * @param unknown $params
     */
    public static function DisplayMyClassesHeaderLink( $params )
    {
        
        $data_info = $params['data_info'];
        
        $link_display = "";
        
        switch($data_info)
        {
            case 'classes':
            {
                $pp_id              = $params['pp_id'];
     
                $ProjectInfo              = Projects::find($pp_id);
                
                
                $link_display = "<a href='#' class='AllProjects'> All Projects </a> &nbsp;>&nbsp;&nbsp;<label>" . $ProjectInfo->pp_project_title . "</label>";
            }
            break;
            case 'sessions':
            {
                $pg_id        = $params['pg_id'];
                $Groupinfo    = ProjectGroups::find($pg_id);
                
                $project_id = $Groupinfo->fk_project_id;
                
                $ProjectInfo              = Projects::find($project_id);
                
                $link_display = "<a href='#' class='AllGroups'>All Projects</a>&nbsp;>&nbsp;&nbsp;<label>" . $Groupinfo->pg_group_title . "</label>";
            }
            break;
        }
        
       
        
        return $link_display;
        
    }
}
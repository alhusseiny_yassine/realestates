<?php
/***********************************************************
SessionsManagement.php
Product : 
Version : 1.0
Release : 2
Date Created : Nov 17, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

namespace App\Libraries\Sessions;


use Validator;
use Input;
use Config;
use Session;
use Redirect;
use Crypt;
use Auth;
use DB;
use App\model\System\Beneficiaries;
use App\model\Project\ProjectClasses;
use App\model\Sessions\SessionClass;
use Illuminate\Support\Facades\Hash;


class SessionsManagement
{
    public static function getSessionsDatainformation( $project_id , $project_course_id = 0 )
    {

        $project_obj = ProjectClasses::whereFkProjectId($project_id);
        if($project_course_id != 0)
        {
            $project_obj = $project_obj->whereFkPcourseId($project_course_id);
        }
        $project_classes = $project_obj->get();
        
        $classes_ids = array();
        
        foreach ( $project_classes as $cls_index => $project_classes_info ) 
        {
            $classes_ids[] = $project_classes_info->cls_id;
        }
        
        // get list of Sessions Saved in the database for this list of classess
        $sessions_info = DB::table('ses_session_class')->leftJoin('proj_classes','ses_session_class.fk_class_id','=','proj_classes.cls_id')
        ->leftJoin('proj_project_courses','proj_classes.fk_pcourse_id','=','proj_project_courses.pc_id')
        ->leftJoin('sys_courses','proj_project_courses.fk_course_id','=','sys_courses.c_id')->whereIn('fk_class_id',$classes_ids)->get();
        
        $sessions_class_array = array();
        
        
        $colors_cours = array();
        for ($i = 0; $i < count($sessions_info); $i++)
        {
            $colors_cours[ $sessions_info[$i]->c_code ] = rand_color();
        }
         
        $xml_session_data = "<data>";
       
        
        for ($i = 0; $i < count($sessions_info); $i++) 
        {

            $session_title = $sessions_info[$i]->c_course_name;
            
            $start_session_date = $sessions_info[$i]->sc_session_date . " " . $sessions_info[$i]->sc_session_time;
            
            //$end_date = strtotime($sessions_info[$i]->sc_session_date . " " . $sessions_info[$i]->sc_session_time . " + " . $sessions_info[$i]->sc_session_duration . " hours");
            $time_array = explode(":", $sessions_info[$i]->sc_session_time);
            
            $duration = $sessions_info[$i]->sc_session_duration;
            
            $time = strtotime($sessions_info[$i]->sc_session_time) + $sessions_info[$i]->sc_session_duration * 3600;
            $time = date("H:i:s" , $time);
            $end_session_date = $sessions_info[$i]->sc_session_date . " " . $time;
            
            $xml_session_data .= "<event id='" . $sessions_info[$i]->sc_id . "'>\n";
            $xml_session_data .= "<start_date>" . date("Y-m-d H:i:s" , strtotime($start_session_date)) . "</start_date>\n";
            $xml_session_data .= "<end_date>" . date("Y-m-d H:i:s" , strtotime($end_session_date)) . "</end_date>\n";
            $xml_session_data .= "<text><![CDATA[ " . $session_title . " ]]></text>\n";
            $xml_session_data .= "<details><![CDATA[  " . $session_title . " ]]></details>\n";
            $xml_session_data .= "</event>\n";
            
            
        }
        $xml_session_data .= "</data>";
        
        $xml_path = public_path('resources/projects/calenders/' . $project_id . "/");
       
        if(!is_dir($xml_path))
            CreateDirectory($xml_path);
        $filename = $xml_path . "session_calender.xml";
        $fp = fopen($filename, "w+");
        fwrite($fp, $xml_session_data);
        fclose($fp);
        
       $xml_url = url('resources/projects/calenders/' . $project_id . '/session_calender.xml');
        
        $result_array = array(); 
        $result_array['session_calendar_path'] = urlencode($xml_url);
        
        return $result_array;
         
    }
    
    
    public static function getLstCoursesInProject($project_id)
    {
        $project_courses =  DB::table('proj_project_courses')->leftjoin('sys_courses','fk_course_id','c_id')->where('fk_project_id','=',$project_id)->get();
        $lst_courses_project = array();
        
        foreach ($project_courses as $index => $course_info) {
            if(!isset($lst_courses_project[ $course_info->pc_id ]))
            $lst_courses_project[ $course_info->pc_id ] = $course_info->c_course_name;
        }
        
        
        return $lst_courses_project;
    }
}
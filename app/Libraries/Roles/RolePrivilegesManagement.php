<?php
/***********************************************************
RolePrivilegesManagement.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 26, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Libraries\Roles;


use Validator;
use Input;
use Config;
use Session;
use Redirect;
use Crypt;
use Auth;
use DB;
use App\model\System\PrivilegedActions;
use App\model\System\RolePrivileges;
use App\model\System\Roles;
use Illuminate\Support\Facades\Hash;


class RolePrivilegesManagement
{

    public static function getAll( $role_id )
    {
        $rp_array = RolePrivileges::whereFkRoleRId($role_id)->get();
        
        $db_result_array = array();

        foreach ( $rp_array as $role_privileges )
        {
            $db_result_array[] = $role_privileges['attributes'];
        }
            

        $result_array = array();
        for ($i = 0; $i < count($db_result_array); $i++) {
            $result_array[ $db_result_array[$i]['rp_action_code'] ] = $db_result_array[$i]['rp_privilege'];
        } 

        return $result_array;
    }


    public static function ResetRolePrivileges($role_id)
    {
        DB::table('role_privileges')->where('fk_role_r_id', '=', $role_id)->delete();
    }
}
<?php
/***********************************************************
PrivilegedActionsManagament.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 26, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

namespace App\Libraries\Roles;


use Validator;
use Input;
use Config;
use Session;
use Redirect;
use Crypt;
use Auth;
use DB;
use App\model\System\PrivilegedActions;
use App\model\System\RolePrivileges;
use App\model\System\Roles;
use Illuminate\Support\Facades\Hash;
use App\model\Requests\RequestStatus;
use App\model\Project\ProjectStatus;


class PrivilegedActionsManagament
{
    /**
     * get list of all privileges
     *
     * @author Moe Mantach
     * @access public
     * static
     *
     * @return Array $db_result_array
     */
    public static function getAll()
    {
        $pa_array = PrivilegedActions::all();

        $db_result_array = array();

        foreach ( $pa_array as $privilage_actions )
        {
            $db_result_array[] = $privilage_actions['attributes'];
        }

        $result_array = array();
        for ($i = 0; $i < count($db_result_array); $i++) {
            $result_array[ $db_result_array[$i]['pa_group'] ][ $i ]['id']            = $db_result_array[$i]['pa_id'];
            $result_array[ $db_result_array[$i]['pa_group'] ][ $i ]['code']          = $db_result_array[$i]['pa_code'];
            $result_array[ $db_result_array[$i]['pa_group'] ][ $i ]['name']          = $db_result_array[$i]['pa_name'];
            $result_array[ $db_result_array[$i]['pa_group'] ][ $i ]['description']   = $db_result_array[$i]['pa_description'];
        }

        // add to the array the list of requests status , project status

        $request_status_obj = RequestStatus::whereRsIsDeleted(0)->get();
        $request_status_array  = CreateDatabaseArrayByIndex($request_status_obj , 'rs_id');
        $i = 0;
        foreach ($request_status_array as $key => $value) {
            $result_array[ 'Requests Statuses' ][ $i ]['id']            = $value['attributes']['rs_id'];
            $result_array[ 'Requests Statuses' ][ $i ]['code']          = 'request_status_' . $value['attributes']['rs_id'];
            $result_array[ 'Requests Statuses' ][ $i ]['name']          = $value['attributes']['rs_status_name'];
            $result_array[ 'Requests Statuses' ][ $i ]['description']   = $value['attributes']['rs_status_name'];
            $i++;
        }

        $projects_status_obj = ProjectStatus::wherepsIsDeleted(0)->get();
        $projects_status_array  = CreateDatabaseArrayByIndex($projects_status_obj , 'ps_id');
        $i = 0;
        foreach ($projects_status_array as $key => $value) {
            $result_array[ 'Project Statuses' ][ $i ]['id']            = $value['attributes']['ps_id'];
            $result_array[ 'Project Statuses' ][ $i ]['code']          = 'project_status_' . $value['attributes']['ps_id'];
            $result_array[ 'Project Statuses' ][ $i ]['name']          = $value['attributes']['ps_status_name'];
            $result_array[ 'Project Statuses' ][ $i ]['description']   = $value['attributes']['ps_status_name'];
            $i++;
        }

        return $result_array;
    }



}
<?php
/***********************************************************
NotiifcationsManager.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 15, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :

***********************************************************/



namespace App\Libraries\Notifications;


use Validator;
use Input;
use Config;
use Session;
use Redirect;
use Crypt;
use Auth;
use DB;
use App\model\Users\Users;
use Illuminate\Support\Facades\Hash;
use App\model\Users\UserTypes;
use App\model\Notifications\NotificationTypes;
use App\model\Requests\Request As ORequest;
use App\model\Notifications\Notifications;



class NotifcationsManager
{
    const NOT_REJECTION_REQUEST          = 1;
    const NOT_INFORMATION                    = 2;
    const NOT_SEND_REQUEST                  = 3;
    const NOT_APPROVE_REQUEST              = 4;
    const NOT_DENY_REQUEST                  = 5;
    const NOT_CREATE_CLASS                   = 6;
    const NOT_APPROVE_SCHEDULE            = 7;
    const NOT_DENY_SCHEDULE                 = 8;
    const NOT_POSTPONE_SCHEDULE           = 9;

    /**
     * Send Notification the user system
     *
     * @author Moe Mantach
     * @access public
     * @param Array $param_array
     */
    public static function SendNotification( $param_array )
    {
        // get variables used in all types of notifications
        $fk_user_id             = Session('ut_user_id');
        $user_auth_info         = Auth::user();
        $notification_type      = $param_array['notification_type'];
        $not_temp_info          = NotificationTypes::whereNtNotificationType($notification_type)->get();
        
        $notification_template = "";
        $notification_template  = isset($not_temp_info[0]['attributes']['nt_notification_text_template']) ? $not_temp_info[0]['attributes']['nt_notification_text_template'] : "";
       
        switch($notification_type)
        {
            case self::NOT_APPROVE_REQUEST:
            {
                $fk_request_id = isset($param_array['request_id']) ? $param_array['request_id'] : 0;
                $user_id          = isset($param_array['user_id']) ? $param_array['user_id'] : 0;
                $coordinator_name  = isset($param_array['coordinator_name']) ? $param_array['coordinator_name'] : '';
                $coordinator_uname = isset($param_array['coordinator_uname']) ? $param_array['coordinator_uname'] : '';
                
               
                
                
                //get request information
                $request_info = ORequest::find($fk_request_id);
                
                $notification_template = str_replace("%coordinator_name%",$user_auth_info->u_fullname, $notification_template);
                $notification_template = str_replace("%request_name%",$request_info->r_request_title, $notification_template);
                $notification_template = str_replace("%coordinator_name%",$coordinator_name, $notification_template);
                $notification_template = str_replace("%coordinator_uname%",$coordinator_uname, $notification_template);
                
                
                $notification_data = new Notifications();
                $notification_data->fk_user_id                  = $user_id;
                $notification_data->nt_creator_id              = $fk_user_id;
                $notification_data->fk_request_id              = $fk_request_id;
                $notification_data->nt_notification_type     = $notification_type;
                $notification_data->nt_notification_code     = "RQT";
                $notification_data->nt_notification             = $notification_template;
                $notification_data->nt_notification_date     = date("Y-m-d H:i:s");
                $notification_data->nt_notification_is_new  = 1;
                $notification_data->save();
            }
            break;
            case self::NOT_REJECTION_REQUEST :
            {
                $fk_request_id = isset($param_array['request_id']) ? $param_array['request_id'] : 0;
                $user_id          = isset($param_array['user_id']) ? $param_array['user_id'] : 0;
                
                //get request information
                $request_info = ORequest::find($fk_request_id);

                $notification_template = str_replace("%user_name%",$user_auth_info->u_fullname, $notification_template);
                $notification_template = str_replace("%request_name%",$request_info->r_request_title, $notification_template);


                $notification_data = new Notifications();
                $notification_data->fk_user_id                  = $user_id;
                $notification_data->nt_creator_id              = $fk_user_id;
                $notification_data->fk_request_id              = $fk_request_id;
                $notification_data->nt_notification_type     = $notification_type;
                $notification_data->nt_notification_code     = "RQT";
                $notification_data->nt_notification             = $notification_template;
                $notification_data->nt_notification_date     = date("Y-m-d H:i:s");
                $notification_data->nt_notification_is_new  = 1;
                $notification_data->save();

            }
            break;
            case self::NOT_INFORMATION:
            {
                $user_id = $param_array['user_id'];
                
                
                $notification_template = str_replace("%full_name%",$param_array['full_name'], $notification_template);
                $notification_template = str_replace("%user_name%",$param_array['full_name'], $notification_template);
                $notification_template = str_replace("%coordinator%",$param_array['coordinator'], $notification_template);
                $notification_template = str_replace("%beneficiary%",$param_array['beneficiary'], $notification_template);


                $notification_data = new Notifications();
                $notification_data->fk_user_id              = $user_id;
                $notification_data->nt_creator_id           = $fk_user_id;
                $notification_data->fk_request_id           = 0;
                $notification_data->nt_notification_type    = $notification_type;
                $notification_data->nt_notification_code    = "INFO";
                $notification_data->nt_notification         = $notification_template;
                $notification_data->nt_notification_date    = date("Y-m-d H:i:s");
                $notification_data->nt_notification_is_new  = 1;
                $notification_data->save();
                
            }
            break;
            case self::NOT_SEND_REQUEST :
            {
            
                 $user_id = $param_array['ut_user_id'];
                               

                $notification_template = str_replace("%full_name%",$param_array['full_name'], $notification_template);
                $notification_template = str_replace("%user_name%",$param_array['user_name'], $notification_template);
                $notification_template = str_replace("%beneficiary%",$param_array['beneficiary'], $notification_template);
                $notification_template = str_replace("%coordinator%",$param_array['coordinator_name'], $notification_template);
                $notification_template = str_replace("%course_name%",$param_array['course_name'], $notification_template);
                    
                

                $notification_data = new Notifications();
                $notification_data->fk_user_id              = $user_id;
                $notification_data->nt_creator_id           = $fk_user_id;
                $notification_data->fk_request_id           = $param_array['fk_request_id'];
                $notification_data->fk_course_id            = $param_array['fk_course_id'];
                $notification_data->nt_notification_type    = $notification_type;
                $notification_data->nt_notification_code    = "RQT";
                $notification_data->nt_notification         = $notification_template;
                $notification_data->nt_notification_date    = date("Y-m-d H:i:s");
                $notification_data->nt_notification_is_new  = 1;
                $notification_data->save();
            }
            break;
            case self::NOT_APPROVE_SCHEDULE :
            case self::NOT_DENY_SCHEDULE :
            case self::NOT_REJECTION_REQUEST :
            {
            
                 $user_id = $param_array['user_id'];
                 $project_id = $param_array['fk_project_id'];
                 $request_id = $param_array['fk_request_id'];
                               

                $notification_template = str_replace("%full_name%",$param_array['full_name'], $notification_template);
                $notification_template = str_replace("%user_name%",$param_array['user_name'], $notification_template);
                $notification_template = str_replace("%beneficiary%",$param_array['beneficiary'], $notification_template);
                $notification_template = str_replace("%coordinator%",$param_array['coordinator_name'], $notification_template);
                $notification_template = str_replace("%project_name%",$param_array['project_name'], $notification_template);
                    
                

                $notification_data = new Notifications();
                $notification_data->fk_user_id                       = $user_id;
                $notification_data->nt_creator_id                   = $fk_user_id;
                $notification_data->fk_project_id                   = $param_array['fk_project_id'];
                $notification_data->fk_request_id                   = $param_array['fk_request_id'];
                $notification_data->fk_course_id                    = 0;
                $notification_data->nt_notification_type          = $notification_type;
                $notification_data->nt_notification_code         = "PRJ";
                $notification_data->nt_notification                 = $notification_template;
                $notification_data->nt_notification_date         = date("Y-m-d H:i:s");
                $notification_data->nt_notification_is_new      = 1;
                $notification_data->save();
            }
            break;
        }
    }


    public static function getCurrentNotifications()
    {

    }
}
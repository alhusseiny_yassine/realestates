<?php
/***********************************************************
ProjectNotificationsManager.php
Product :
Version : 1.0
Release : 2
Date Created : Apr 6, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

namespace App\Libraries\Notifications;


use Validator;
use Input;
use Config;
use Session;
use Redirect;
use Crypt;
use Auth;
use DB;
use App\model\Users\Users;
use Illuminate\Support\Facades\Hash;
use App\model\Users\UserTypes;
use App\model\Notifications\NotificationTypes;
use App\model\Requests\Request As ORequest;
use App\model\Notifications\Notifications;
use App\model\Project\Projects;
use App\model\Project\ProjectNotifications;
use App\model\Project\ProjectGroupTrainees;
use App\model\Project\ProjectRequests;



class ProjectNotificationsManager
{
    /**
     * Send Notification to Omsar to approve the project configuration and scheduler
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $params_array
     * @return multitype:array
     */
    public static function SendOmsarNotifications( $params_array )
    {
        {/** get logged In User Information */
            $user_full_name = Session('ut_user_fullname');
            if($user_full_name == null)
            {
                $user_full_name = Session('user_fullname');
            }
            $user_id        = Session('ut_user_id');
            if($user_id == null)
            {
                $user_id = Session('user_id');
            }
        }
         

        // get list of omsar administrator
        $lst_administrators = Users::whereUUserType(UserTypes::ADMIN_USER_TYPE_ID)->whereUIsDeleted(0)->get();


        $project_id     = $params_array['project_id'];
        $project_info   = Projects::find($project_id);

        $not_description = $user_full_name . " Create A new Project <a href='" . url('administrator/projects/ViewProject/' . $project_id ) . "'>" . $project_info->pp_project_title . "</a> Click on the title to Check the content";

        foreach ($lst_administrators as $key => $user_info) {
             $projectNotifications = new ProjectNotifications();
            $projectNotifications->fk_project_id = $project_id;
            $projectNotifications->fk_created_by = $user_id;
            $projectNotifications->fk_user_id = $user_info->id;
            $projectNotifications->nr_notification_text = $not_description;
            $projectNotifications->nr_notification_status = 0;
            $projectNotifications->nr_notification_date = date('Y-m-d H:i:s');
            $result = $projectNotifications->save();

        }


        $result_array = array();
        $result_array['is_error'] = 0;


        return $result_array;
    }

    /**
     * Send Tranees Notifications to show the scheduler of every trainees
     * to approve
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $params_array
     */
    public static function SendTraineesNotifications( $params_array )
    {
        $project_id     = $params_array['project_id'];
        $project_info    = Projects::find($project_id);

        {/** get logged In User Information */
            $user_full_name = Session('ut_user_fullname');
            if($user_full_name == null)
            {
                $user_full_name = Session('user_fullname');
            }
            $user_id        = Session('ut_user_id');
            if($user_id == null)
            {
                $user_id = Session('user_id');
            }
        }


        $project_group_trainees = ProjectGroupTrainees::whereFkProjectId($project_id)->get();

        $project_trainees_array = array();

        foreach ( $project_group_trainees as $key => $pgt_info ) {
            $project_trainees_array[$pgt_info->fk_group_id][] = $pgt_info->fk_trainee_id;
        }


        foreach ( $project_trainees_array as $group_id => $lst_trainees_info )
        {
            for ($i = 0; $i < count($lst_trainees_info); $i++)
            {
                $not_description = $user_full_name . " Create A new Project <b>" . $project_info->pp_project_title . "</b> Please check the <a  target='_blank'  href='" . url('projects/TraineesCalendar/' . $project_id . '/' . $group_id . '/' .$lst_trainees_info[$i] ) . "' >scheduler </a> and approve your attendance ";
                $projectNotifications = new ProjectNotifications();
                $projectNotifications->fk_project_id = $project_id;
                $projectNotifications->fk_created_by = $user_id;
                $projectNotifications->fk_user_id = $lst_trainees_info[$i];
                $projectNotifications->nr_notification_text = $not_description;
                $projectNotifications->nr_notification_status = 0;
                $projectNotifications->nr_notification_date = date('Y-m-d H:i:s');
               // dd($projectNotifications);
                $result = $projectNotifications->save();
            }
        }

    }
}
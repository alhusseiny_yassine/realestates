<?php
/***********************************************************
CourseManagement.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 1, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/



namespace App\Libraries\Courses;


use Validator;
use Input;
use Config;
use Session;
use Redirect;
use Crypt;
use Auth;
use DB;
use App\model\System\Courses;
use App\model\System\CourseType;
use Illuminate\Support\Facades\Hash;
use App\model\Users\UserTypes;
use App\model\System\CourseCategories;


class CourseManagement
{
    public static function getCourseTypeArrayById()
    {
        $course_result_array = array();
        $all_coursetypes = CourseType::whereCtIsDeleted(0)->get();
        foreach ($all_coursetypes as $ct_index => $ct_info )
        {
            $course_result_array[ $ct_info->ct_id ] = $ct_info->ct_course_type;
        }


        return $course_result_array;
    }
    
    
    public static function getCourseCategoriesArrayById()
    {
        $course_result_array = array();
        $all_coursecategories = CourseCategories::whereCcIsDeleted(0)->get();
        foreach ($all_coursecategories as $cc_index => $cc_info )
        {
            $course_result_array[ $cc_info->cc_id ] = $cc_info->cc_course_category;
        }


        return $course_result_array;
    }
}
<?php

namespace App\Providers;




use Illuminate\Support\ServiceProvider;
use Validator;
use Input;
use Request;
use Redirect;
use Auth;
use DB;
use Session;
use Config;
use App\Libraries\Notifications\NotifcationsManager;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        $data = array(
        );

        return view()->share('data', $data);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

/**
 *
 */


$(function(){
	course_module.displayListCourses();
	$("#BTN_ADD_COURSE").on('click',course_module.AddNewCourseForm);
	$("#SL_COURSE_TYPE").on('change',course_module.DisplayCourseCategoriesDropdown);
	$(".CoursesCategoriesDropdown").on('change',"#FK_CATEGORY_ID",course_module.displayListCourses);
	$('input[name=c_course_code]').on('keyup',course_module.displayListCourses);
	$('input[name=c_course_name]').on('keyup',course_module.displayListCourses);
	$('input[name=c_course_description]').on('keyup',course_module.displayListCourses);
	$(".ListCourseGird").on('click',"a[id*=EDIT_COURSE_]",course_module.DisplayEditCourseForm);
	$(".ListCourseGird").on('click',"a[id*=DELETE_COURSE_]",course_module.DeleteCourseData);
})
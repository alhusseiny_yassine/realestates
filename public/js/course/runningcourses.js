/**
 *
 */

$(function(){
	course_module.RunningCoursesManagement();
	$("#SL_COURSE_TYPE").on('change',course_module.RDisplayCourseCategoriesDropdown);
	$("#TRAINEE_SEND_REQUEST").on('click',course_module.OpenSendRequestPopup);
	$("#BTN_SEARCH").on('click',course_module.RunningCoursesManagement);
	$("#BTN_ADVANCED_SEARCH").on('click',course_module.ShowAdvancedSearchPanel);
})
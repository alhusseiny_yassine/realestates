/**
 *
 */

$(function(){
	course_module.CoursesManagement();
	$("#TRAINEE_SEND_REQUEST").on('click',course_module.OpenSendRequestPopup);
	$("#BTN_SEARCH").on('click',course_module.CoursesManagement);
	$("#BTN_RESET_SEARCH").on('click',course_module.ResetCourseSearch);
	$("#BTN_ADVANCED_SEARCH").on('click',course_module.ShowAdvancedSearchPanel);
})
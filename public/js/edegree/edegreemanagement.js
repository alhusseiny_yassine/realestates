/**
 *
 */


$(function(){
	edegree_module.displayListEducationDegree();
	$("#BTN_ADD_EDEGREE").on('click',edegree_module.AddNewEducationDegree);
	$(".ListEducationDegreeGird").on('click',"a[id*=EDIT_EDEGREE_]",edegree_module.DisplayEditEducationDegreeForm);
	$(".ListEducationDegreeGird").on('click',"a[id*=DELETE_EDEGREE_]",edegree_module.DeleteEducationDegreeData);
})
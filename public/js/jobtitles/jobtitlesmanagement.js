/**
 *
 */


$(function(){
	jobtitle_module.displayListJobTitle();
	$("#BTN_ADD_JOBTITLES").on('click',jobtitle_module.AddNewJobTitle);
	$(".ListJobTitlesGird").on('click',"a[id*=EDIT_JOBTITLE_]",jobtitle_module.DisplayEditJobTitleForm);
	$(".ListJobTitlesGird").on('click',"a[id*=DELETE_JOBTITLE_]",jobtitle_module.DeleteJobTitleData);
        $("#PRINT_LIST").on('click',jobtitle_module.DisplayPrintList);
        $("#EXPORT_TO_EXCELL").on('click',jobtitle_module.ExportToExcell);
})
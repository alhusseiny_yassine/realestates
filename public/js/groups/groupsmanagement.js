$(function(){
    
    $("#select_project").on("change",groups_module.DisplayProjectCourses);
    $("#select_course").on("change",groups_module.DisplayGroups);
    $("#BTN_CREATE_PROJECT").on("click",groups_module.DisplayAddGroup);
    $(".ListGroups").on('click',"a[id*=EDIT_GROUP_]",groups_module.DisplayEditGroup);
    $(".ListGroups").on('click',"a[id*=DELETE_GROUP_]",groups_module.DeleteGroup);
    $(".ListGroups").on('click',"a[id*=EDIT_TRAINEE_GROUP_]",groups_module.EditTraineesGroup);
});
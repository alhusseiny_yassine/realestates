/**
 *
 */


$(function(){
	districts_module.displayListDistricts();
	$("#BTN_ADD_DISTRICT").on('click',districts_module.AddNewDistrictForm);
	$(".ListDistrictsGird").on('click',"a[id*=EDIT_DISTRICT_]",districts_module.DisplayEditDistrictForm);
	$(".ListDistrictsGird").on('click',"a[id*=DELETE_DISTRICT_]",districts_module.DeleteDistrictData);
})
$(function(){
   $("#u_date_birth").datepicker({
		endDate :'-18y',
		keepOpen : false,
		format : "yyyy-mm-dd"
	});

   $('input[name=u_is_active]').bootstrapSwitch();

   $("#BTN_SAVE_TRAINER").on('click',trainers_module.SaveTrainerInfo);
   $("input[name=u_email]").on('blur',trainers_module.CheckIfUserExit);
   $("input[name=fullname]").on('blur',trainers_module.GenerateEmailDefaultEmailAddress);

})
/**
 *
 */


$(function(){
    var session_xml_url = $('#XML_URL').val();
    var $modelPopup = $('#ModelPopUp');
    scheduler.attachEvent("onEventclick", function (id, ev) {
        return false;
    });
    scheduler.attachEvent("onClick", function (id, ev) {
        return false;
    });
    scheduler.attachEvent("onEventChanged", function (id, ev) {
        return false;
    });
    scheduler.attachEvent("onDblClick", function (id, e) {
        $('body').modalmanager('loading');
        var base_url = $("#BASE_URL").val();
        var url = base_url + "/ViewSessionInfo/" + id;
        setTimeout(function () {
            $modelPopup.load(url, '', function () {
                $modelPopup.modal();
                $(".modal_close").on("click", function () {
                    $modelPopup.modal('toggle');
                });
            });
        }, 1000);
    });
	scheduler.config.xml_date="%Y-%m-%d %H:%i";
	scheduler.init('SessionScheduler',new Date(),"day");
	scheduler.load(session_xml_url);
        scheduler.config.dblclick_create = false;
})
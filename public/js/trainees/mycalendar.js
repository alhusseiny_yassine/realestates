/**
 *
 */


$(function(){
	var session_xml_url = $('#XML_URL').val();
	scheduler.attachEvent("onEventclick",function(id,ev){
		return false;
	});
	scheduler.attachEvent("onClick",function(id,ev){
		return false;
	});
	scheduler.attachEvent("onEventChanged", function(id,ev){
		return false;
	});
	scheduler.attachEvent("onDblClick", function (id, e){
		var $modelPopup = $('#ModelPopUp');
		$('body').modalmanager('loading');
		var base_url = $("#BASE_URL").val();
		var url = base_url + "/ViewSessionInfo/" + id;
        setTimeout(function(){
        	$modelPopup.load(url, '', function(){
        		$modelPopup.modal();
        		/**$("input[name=pg_start_date],input[name=pg_end_date]").datepicker({
        			keepOpen : false,
        			format : "yyyy-mm-dd"
        		});
        		projects_module.DisplayProviderLocationsDropDown();
        		$("#PG_CLASS_PROVIDER").on('change',projects_module.DisplayProviderLocationsDropDown);
        		   $("#BTN_SAVE_GROUP_INFO").on('click',projects_module.SaveClassProjectInfo);*/

          });
        }, 1000);
  });
	scheduler.config.xml_date="%Y-%m-%d %H:%i";
	scheduler.init('SessionScheduler',new Date(),"week");
	scheduler.load(session_xml_url);
        scheduler.config.dblclick_create = false;
})
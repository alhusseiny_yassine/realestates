/**
 *
 */


$(function(){
	project_type_module.displayListProjectTypes();
	$("#BTN_ADD_PROJECT_TYPE").on('click',project_type_module.AddNewProjectTypeForm);
	$(".ListProjectTypesGird").on('click',"a[id*=EDIT_PROJECT_TYPE_]",project_type_module.DisplayEditProjectTypeForm);
	$(".ListProjectTypesGird").on('click',"a[id*=DELETE_PROJECT_TYPE_]",project_type_module.DeleteProjectTypeData);
})
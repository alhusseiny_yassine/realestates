/**
 * 
 */

$(function(){
	$('#PP_PROJECT').on('change',class_module.DisplayCoursesDropdown);
	$('.CoursesDropdown').on('change','#PP_COURSE',class_module.DisplaySessionsData); 
	$('.ChangeType').on('click',class_module.SwitchSessionsView);
	$('.ListClasses').on('click',"a[id*=EDIT_SESSION_INFO_]",class_module.EditSessionInfo);
	$('.ListClasses').on('click',"a[id*=DELETE_SESSION_INFO_]",class_module.DeleteSessionInfo);
        $("#BTN_SUBMIT").on('click',class_module.SubmitSurvey);
})
/**
 *
 */


$(function(){
	session_statuses_module.displayListSessionStatuses();
	$("#BTN_ADD_SESSION_STATUS").on('click',session_statuses_module.AddNewSessionStatusForm);
	$(".ListSessionStatusesGird").on('click',"a[id*=EDIT_SESSION_STATUS_]",session_statuses_module.DisplayEditSessionStatusForm);
	$(".ListSessionStatusesGird").on('click',"a[id*=DELETE_SESSION_STATUS_]",session_statuses_module.DeleteSessionStatusData);
        $("#PRINT_LIST").on('click',session_statuses_module.DisplayPrintList);
        $("#EXPORT_TO_EXCELL").on('click',session_statuses_module.ExportToExcell);
})
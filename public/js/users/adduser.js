/**
 *
 */
var module 		= {
		CancelForm : function(){
			 window.history.back();
		},
		GetDate : function(){
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear() - 18;

			if(dd<10) {
			    dd='0'+dd
			}

			if(mm<10) {
			    mm='0'+mm
			}

			today =  dd + "-" + mm+'-' + yyyy;
			return today;
		}
};
var response 	= {};
$(function(){
   $("input[name=u_date_birth]").datepicker({
		endDate :'-18y',
		keepOpen : false,
		format : "dd-mm-yyyy"
	});

   $('input[name=u_is_active]').bootstrapSwitch();

   $("#BTN_ADD_USER").on('click',users_module.AddUserInfo);
   $("input[name=u_email]").on('blur',users_module.CheckIfUserExit);
   $("#FK_PROVINCE_ID").on('change',users_module.GetDistrictsProvince);
   $("input[name=fullname]").on('blur',users_module.GenerateEmailDefaultEmailAddress);
   $(".ExtraFieldSection").on('click','a.GenerateCoordinatorCode',users_module.GenerateCoordinatorCode);
   $("#FB_USER_TYPE").on('change',users_module.DisplayUserTypeForm);
   $('.ExtraFieldSection').on('change',"#FK_BENEFICIARY_ID",users_module.DisplayBeneficiaryDepartments);
   
    $('#U_PROFILE_PIC').on('change', function () {
        var countFiles   = $(this)[0].files.length;
        var imgPath      = $(this)[0].value;
        var extn         = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        var image_holder = $(".ListFiles");
        image_holder.empty();

        if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
            if (typeof (FileReader) != "undefined") {
                //loop for each file selected for uploaded.
                for (var i = 0; i < countFiles; i++)
                {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var base_url = $('#BASE_URL').val();

                        $("#PROFILE_PIC").attr('src', e.target.result);
                    }
                    image_holder.show();
                    reader.readAsDataURL($(this)[0].files[i]);
                }

            }
        }
    });

});
/**
 *
 */
$(function(){
   $("input[name=u_date_birth]").datepicker({
		endDate :'-18y',
		keepOpen : false,
		format : "yyyy-mm-dd"
	});
   $('input[name=u_is_active]').bootstrapSwitch();
   $('.EducationInfo').on('click','button[name=btn_add_education_info]',users_module.OpenAEducationInfoPage);
   $('.EducationInfo').on('click','a[id*=EDIT_UE_]',users_module.OpenAEditEducationInfoPage);
   $('.EducationInfo').on('click','a[id*=DELETE_UE_]',users_module.DeleteEducationInfo);

   $('.ProviderLocation').on('click','button[name=btn_add_provider_location]',users_module.OpenAProviderLocationPage);
   $('.ProviderLocation').on('click','a[id*=EDIT_PL_]',users_module.OpenAEditProviderLocationPage);
   $('.ProviderLocation').on('click','a[id*=DELETE_PL_]',users_module.DeleteProviderLocation);

   $("#BTN_EDIT_USER").on('click',users_module.EditUserInfo);
   $("input[name=u_email]").on('blur',users_module.CheckIfUserExit);
   $("#FK_PROVINCE_ID").on('change',users_module.GetDistrictsProvince);
   $(".ExtraFieldSection").on('click','a.GenerateCoordinatorCode',users_module.GenerateCoordinatorCode);
   $("#FB_USER_TYPE").on('change',users_module.DisplayUserTypeForm);
   $("#BTN_TRAINEE_HISTORY").on('click',users_module.OpenTraineeHistoryPage);
   $("#BTN_ADD_NEW_TRAINEE_REQUEST").on('click',users_module.OpenNewTraineeRequest);
   $('.ExtraFieldSection').on('change',"#FK_BENEFICIARY_ID",users_module.DisplayBeneficiaryDepartments);
   $("#CANCEL_FORM").on('click',users_module.CancelForm);
   users_module.DisplayUserTypeForm();
   if($("#U_USER_TYPE").val() == 4)
   {
	   users_module.DisplayUserEducationInfoList();
   }


   if($("#U_USER_TYPE").val() == 3)
   {
	   users_module.DisplayUserProviderLocationsList();
   }
   
   $('#U_PROFILE_PIC').on('change', function () {
        var countFiles   = $(this)[0].files.length;
        var imgPath      = $(this)[0].value;
        var extn         = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        var image_holder = $(".ListFiles");
        image_holder.empty();

        if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
            if (typeof (FileReader) != "undefined") {
                //loop for each file selected for uploaded.
                for (var i = 0; i < countFiles; i++)
                {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var base_url = $('#BASE_URL').val();

                        $("#PROFILE_PIC").attr('src', e.target.result);
                    }
                    image_holder.show();
                    reader.readAsDataURL($(this)[0].files[i]);
                }

            }
        }
    });

})
/**
 *
 */
$(function(){
	$('.MyprofileDashboard').on('click',fprofile_module.DisplayProfileDashboard);
	$('.MyProfileAccountSettings').on('click',fprofile_module.DisplayProfileAccountSettings);
	$('.profile-content').on('click','.btnSaveInfo',fprofile_module.SaveProfileInfo);
	$('.profile-content').on('click','#BTN_CHANGE_PASSWORD',fprofile_module.ChangePassword);

	$('.profile-content').on('change','#U_PROFILE_PIC',function(){
		var countFiles = $(this)[0].files.length;
        var imgPath = $(this)[0].value;
        var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        var image_holder = $(".ListFiles");
        image_holder.empty();

        if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
          if (typeof(FileReader) != "undefined") {
            //loop for each file selected for uploaded.
            for (var i = 0; i < countFiles; i++)
            {
              var reader = new FileReader();
              reader.onload = function(e) {
              var base_url = $('#BASE_URL').val();


              $("#PROFILE_PIC").attr('src',e.target.result);


              }
              image_holder.show();
              reader.readAsDataURL($(this)[0].files[i]);
            }

          }
          }
	});
	$('.profile-content').on('submit','#FRM_PROFILE_PIC',fprofile_module.UploadProfilePic);


})
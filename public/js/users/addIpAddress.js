/**
 *
 */
$(function(){

	$('#BTN_SAVE_MAC').on('click',function(){
		var str_params = $('#FORM_ADD_IP_ADDRESS').serialize();
		var base_url = $('#BASE_URL').val();
		 $.ajax
	        ({
	            url : base_url + "/ajaxSaveIp",
	            data : str_params,
	            dataType : "json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  window.parent.$('#ALLOW_IP_ADDRESS').append('<option value="'  + response.ua_ip_address + '">'  + response.ua_ip_address + '</option>');
	            	  window.parent.$.colorbox.close();

	              }
	            }
	        });
	});
})
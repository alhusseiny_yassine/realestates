/**
 *
 */
var modules 	= {

		AddUser : function(){
			var base_url = $("#BASE_URL").val();
		    window.location.href = base_url + "/administrator/adduser";
		},
		AddMultipleUser : function(){

		},
		EditUser : function(){
			 var user_id = $(this).parents('tr').data('user_id');
		      var base_url = $("#BASE_URL").val();
		      window.location.href = base_url + "/editUser/" + user_id;
		},

		CheckAllFields : function(){

		}

};
var response 	= {};

$(function(){

	$('.bs-select').selectpicker({
        iconBase: 'fa',
        tickIcon: 'fa-check'
    });
	users_module.displayListUsers();
	$('#FB_USER_TYPE').on('change',users_module.displayListUsers);
	$('#U_BENEFICIARY').on('change',users_module.displayListUsers);
	$('input[name=u_search_username]').on('keyup',users_module.displayListUsers);
	$('input[name=u_search_fullname]').on('keyup',users_module.displayListUsers);
	$('input[name=u_search_email]').on('keyup',users_module.displayListUsers);
	$("#BTN_ADD_USER").on('click',modules.AddUser);
	$("#BTN_ADD_USER_BOTTOM").on('click',modules.AddUser);
	$("#MULTIPLE_USERS").on('click',users_module.AddMultipleUsersLayout);
	$("#MULTIPLE_EDIT_USERS").on('click',users_module.EditMultipleUsersLayout);
	$("#MULTIPLE_DELETE_USERS").on('click',users_module.MultipleDeleteUsersLayout);
	$("#PRINT_LIST").on('click',users_module.DisplayPrintList);
        $("#EXPORT_TO_EXCELL").on('click',users_module.ExportToExcell);

	$(".ListUserGirds").on('click',"a[id*=EDIT_USER_]",users_module.DisplayEditUser);
	$(".ListUserGirds").on('click',"a[id*=DELETE_USER_]",users_module.DeleteUserData);
	//$(".ListUserGirds").on('click',"input[id=CK_ALL_USERS]",modules.CheckAllFields);
});
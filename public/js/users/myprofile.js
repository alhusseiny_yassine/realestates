/**
 *
 */
$(function(){
	$('#BTN_SAVE_PROFILE_INFO').on('click',users_module.SaveProfileInfo);
	$('#BTN_SAVE_CHANGE_PASSWORD').on('click',users_module.SubmitChangePassword);
	$('#U_PROFILE_PIC').on('change',function(){
		var countFiles = $(this)[0].files.length;
        var imgPath = $(this)[0].value;
        var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        var image_holder = $(".ListFiles");
        image_holder.empty();

        if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
          if (typeof(FileReader) != "undefined") {
            //loop for each file selected for uploaded.
            for (var i = 0; i < countFiles; i++)
            {
              var reader = new FileReader();
              reader.onload = function(e) {
              var base_url = $('#BASE_URL').val();


              $("#PROFILE_PIC").attr('src',e.target.result);


              }
              image_holder.show();
              reader.readAsDataURL($(this)[0].files[i]);
            }

          }
          }
	});
	$('#FRM_PROFILE_PIC').on('submit',users_module.UploadProfilePic);
})
/**
 *
 */
var module 		= {
  EditUser : function(){
		var frm_profile = $("#FORM_EDIT_PROFILE").serialize();
		var base_url = $('#BASE_URL').val();
		$.ajax
        ({
            url : base_url + "/editProfile",
            data : frm_profile,
            dataType : "json",
            type : "POST",
            success : function(response){
            	 $.alert({
                     title: 'Alert!',
                     content: response.error_msg,
                     confirm: function(){
                         window.location.href = base_url + "/dashboard";
                     }
                 });
            }
        });
	},
	DisplayOverview : function(){
		var base_url = $('#BASE_URL').val();
		var _token = $('input[name=_token]').val();
		$.ajax
        ({
            url : base_url + "/administrator/displayProfileDashboard",
            data : { _token : _token },
            dataType : "json",
            type : "POST",
            success : function(response){
            	$('#PageContent').html(response.display);
            }
        });
	},
	DisplayAccountSettings : function(){
		var base_url = $('#BASE_URL').val();
		var _token = $('input[name=_token]').val();
		var user_id = $('input[name=user_id]').val();
		$.ajax
        ({
            url : base_url + "/administrator/displayAccountSettings",
            data : { _token : _token  , user_id : user_id},
            dataType : "json",
            type : "POST",
            success : function(response){
            	$('#PageContent').html(response.display);
            	$('input[name=u_date_birth]').datepicker({
                     rtl: Metronic.isRTL(),
                     orientation: "left",
                     autoclose: true
                 });
            	module.UploadProfile();
            }
        });
	},
	SubmitPersonalInfo : function(e){
		return module.HandleSubmitProfile();
	},
	ChangePersonalPassword : function(e){
		return module.HandleSubmitPasswordProfile();
	},
	HandleSubmitPasswordProfile : function(){
		 var ProfileForm = $('#FORM_CHANGE_PASSWORD');
         var error3 = $('.alert-danger', ProfileForm);
         var success3 = $('.alert-success', ProfileForm);

         ProfileForm.validate({
             errorElement: 'span', //default input error message container
             errorClass: 'help-block help-block-error', // default input error message class
             focusInvalid: false, // do not focus the last invalid input
             ignore: "", // validate all fields including form hidden input
             rules: {
            	 current_password: {
                     minlength: 2,
                     required: true
                 },
                 new_password: {
                	 minlength: 2,
                     required: true
                 },
                 confirm_new_password: {
                	 minlength: 2,
                     required: true
                 }
             },

             messages: { // custom messages for radio buttons and checkboxes

             },
             errorPlacement: function (error, element) { // render error placement for each input type
                 if (element.parent(".input-group").size() > 0) {
                     error.insertAfter(element.parent(".input-group"));
                 } else if (element.attr("data-error-container")) {
                     error.appendTo(element.attr("data-error-container"));
                 } else if (element.parents('.radio-list').size() > 0) {
                     error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                 } else if (element.parents('.radio-inline').size() > 0) {
                     error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                 } else if (element.parents('.checkbox-list').size() > 0) {
                     error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                 } else if (element.parents('.checkbox-inline').size() > 0) {
                     error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                 } else {
                     error.insertAfter(element); // for other inputs, just perform default behavior
                 }
             },
             invalidHandler: function (event, validator) { //display error alert on form submit
                 success3.hide();
                 error3.show();
             },
             success: function (label) {
                 label
                     .closest('.form-group').removeClass('has-error'); // set success class to the control group
             },
             highlight: function (element) { // hightlight error inputs
                 $(element)
                     .closest('.form-group').addClass('has-error'); // set error class to the control group
             },

             unhighlight: function (element) { // revert the change done by hightlight
                 $(element)
                     .closest('.form-group').removeClass('has-error'); // set error class to the control group
             },
             submitHandler: function (form) {
                var base_url = $('#BASE_URL').val();
        		var _token = $('input[name=_token]').val();
        		var personal_info = $("#FORM_CHANGE_PASSWORD").serialize();
        		$.ajax
                ({
                    url : base_url + "/administrator/ChangeUserPassword",
                    data : personal_info,
                    dataType : "json",
                    type : "POST",
                    success : function(response){
                    	if(response.is_error == 1)
                    	{
                    		$('.alert-success').css({ display : 'none' });
                    		$('.Msg').html(response.error_msg);
                    		$('.alert-danger').css({ display : "" });
                    	}
                    	else
                		{
                    		$('.alert-danger').css({ display : 'none' });
                    		$('.alert-success').css({ display : "" });
                		}
                    }
                });
             }

         });
	},
	HandleSubmitProfile : function(){
		 var ProfileForm = $('#FORM_PERSONAL_INFO');
         var error3 = $('.alert-danger', ProfileForm);
         var success3 = $('.alert-success', ProfileForm);

         ProfileForm.validate({
             errorElement: 'span', //default input error message container
             errorClass: 'help-block help-block-error', // default input error message class
             focusInvalid: false, // do not focus the last invalid input
             ignore: "", // validate all fields including form hidden input
             rules: {
            	 fullname: {
                     minlength: 2,
                     required: true
                 },
                 email: {
                 	email : true
                 },
                 website: {
                     url : true
                 }
             },

             messages: { // custom messages for radio buttons and checkboxes

             },
             errorPlacement: function (error, element) { // render error placement for each input type
                 if (element.parent(".input-group").size() > 0) {
                     error.insertAfter(element.parent(".input-group"));
                 } else if (element.attr("data-error-container")) {
                     error.appendTo(element.attr("data-error-container"));
                 } else if (element.parents('.radio-list').size() > 0) {
                     error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                 } else if (element.parents('.radio-inline').size() > 0) {
                     error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                 } else if (element.parents('.checkbox-list').size() > 0) {
                     error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                 } else if (element.parents('.checkbox-inline').size() > 0) {
                     error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                 } else {
                     error.insertAfter(element); // for other inputs, just perform default behavior
                 }
             },
             invalidHandler: function (event, validator) { //display error alert on form submit
                 success3.hide();
                 error3.show();
             },
             success: function (label) {
                 label
                     .closest('.form-group').removeClass('has-error'); // set success class to the control group
             },
             highlight: function (element) { // hightlight error inputs
                 $(element)
                     .closest('.form-group').addClass('has-error'); // set error class to the control group
             },

             unhighlight: function (element) { // revert the change done by hightlight
                 $(element)
                     .closest('.form-group').removeClass('has-error'); // set error class to the control group
             },
             submitHandler: function (form) {
                success3.show();
                error3.hide();

                var base_url = $('#BASE_URL').val();
        		var _token = $('input[name=_token]').val();
        		var personal_info = $("#FORM_PERSONAL_INFO").serialize();
        		$.ajax
                ({
                    url : base_url + "/administrator/SaveProfileInfo",
                    data : personal_info,
                    dataType : "json",
                    type : "POST",
                    success : function(response){
                    	if(response.is_error == 1)
                    	{
                    		$('.alert-success').css({ display : 'none' });
                    		$('.alert-danger').css({ display : "" });
                    	}
                    	else
                		{
                    		$('.alert-danger').css({ display : 'none' });
                    		$('.alert-success').css({ display : "" });
                		}
                    }
                });
             }

         });
	},
	UploadProfile : function(){
		var url = $('#fileupload').data('url');
	    $('#fileupload').fileupload({
	        url: url,
	        dataType: 'json',
	        autoUpload: true,
	        maxFileSize: 5000000, // 5 MB
	        // Enable image resizing, except for Android and Opera,
	        // which actually support image resizing, but fail to
	        // send Blob objects via XHR requests:
	        disableImageResize: /Android(?!.*Chrome)|Opera/
	            .test(window.navigator.userAgent),
	        previewMaxWidth: 100,
	        previewMaxHeight: 100,
	        previewCrop: true
	    }).on('fileuploadadd', function (e, data) {
	        data.context = $('<div/>').appendTo('#files');
	        $.each(data.files, function (index, file) {
	            var node = $('<p/>')
	                    .append($('<span/>').text(file.name));
	            node.appendTo(data.context);
	        });

			$('#progress .progress-bar').css(
	            'width',
	            '0%'
	        );
	    }).on('fileuploadprocessalways', function (e, data) {
	        var index = data.index,
	            file = data.files[index],
	            node = $(data.context.children()[index]);
	        if (file.preview) {
	            node
	                .prepend('<br>')
	                .prepend(file.preview);
	        }
	        if (file.error) {
	            node
	                .append('<br>')
	                .append($('<span class="text-danger"/>').text(file.error));
	        }
	        if (index + 1 === data.files.length) {
	            data.context.find('button')
	                .text('Upload')
	                .prop('disabled', !!data.files.error);
	        }
	    }).on('fileuploadprogressall', function (e, data) {
	        var progress = parseInt(data.loaded / data.total * 100, 10);
	        $('#progress .progress-bar').css(
	            'width',
	            progress + '%'
	        );
	    }).on('fileuploaddone', function (e, data) {
	    	$("#PROFILE_PICTURE").attr("src",data.result.image_url);
	    	$("#AVATAR_PROFILE_PIC").attr("src",data.result.image_url);
	    	$(".img-circle").attr("src",data.result.image_url);
			$(".profile-userpic img").attr("src",data.result.image_url);

	    }).on('fileuploadfail', function (e, data) {
	        alert('File upload failed.');
	    }).prop('disabled', !$.support.fileInput)
	        .parent().addClass($.support.fileInput ? undefined : 'disabled');
	}

};
var response 	= {

};
$(function(){
	$('#BTN_EDIT_USER').on('click',module.EditUser);
	$('.ProfileDashboard').on('click',module.DisplayOverview);
	$('.AccountSettings').on('click',module.DisplayAccountSettings);
	$('#PageContent').on('click','#BTN_PERSONAL_INFO',module.SubmitPersonalInfo);
	$('#PageContent').on('click','#BTN_CHANGE_PASSWORD',module.ChangePersonalPassword);
	$('#PageContent').on('click','#fileupload',module.UploadProfile);

});
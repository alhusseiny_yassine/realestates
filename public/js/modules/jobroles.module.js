/**
 *
 */


jobrole_module = {
		displayListJobRole : function(){
		var base_url = $('input[name=base_url]').val();
		var _token = $('input[name=_token]').val()
	    var page_number 				= $('input[name=page_number]').val();
	    $.ajax
	    ({
	        url : base_url + "/request/displayListJobRole",
	        data : { _token : _token , page_number : page_number },
	        dataType : "json",
	        type : "POST",
	        success : function(response){
	            $('.ListJobRolesGird').html(response.display);
                         $('.group-checkable').change(function() {
                            var set = $('table').find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
                            var checked = $(this).prop("checked");
                            $(set).each(function() {
                                $(this).prop("checked", checked);
                            });
                            $.uniform.update(set);
                        });

                         $('#JRPagination').twbsPagination({
                             totalPages: response.total_pages,
                             visiblePages: 7,
                             onPageClick: function (event, page) {
                                  $('input[name=page_number]').val(page);
                                  jobrole_module.displayListJobRole();
                             }
                         });
	        }
	    });
	},
	AddNewJobRole : function(){
		var base_url = $("#BASE_URL").val();
      window.location.href = base_url + "/administrator/AddNewJobRole";
	},
	SaveJobRoleInfo : function(){
		return jobrole_module.SaveJobRoleSubmitHandler();
	},
	SaveJobRoleSubmitHandler : function(){
		 var JobRoleForm = $('#FORM_SAVE_JOBROLE');
         var error3 = $('.alert-danger', JobRoleForm);
         var success3 = $('.alert-success', JobRoleForm);

         JobRoleForm.validate({
             errorElement: 'span', //default input error message container
             errorClass: 'help-block help-block-error', // default input error message class
             focusInvalid: false, // do not focus the last invalid input
             ignore: "", // validate all fields including form hidden input
             rules: {
            	 jr_job_role: {
            		 minlength: 4,
            		 required: true
            	 }
             },

             messages: { // custom messages for radio buttons and checkboxes

             },
             errorPlacement: function (error, element) { // render error placement for each input type
                 if (element.parent(".input-group").size() > 0) {
                     error.insertAfter(element.parent(".input-group"));
                 } else if (element.attr("data-error-container")) {
                     error.appendTo(element.attr("data-error-container"));
                 } else if (element.parents('.radio-list').size() > 0) {
                     error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                 } else if (element.parents('.radio-inline').size() > 0) {
                     error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                 } else if (element.parents('.checkbox-list').size() > 0) {
                     error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                 } else if (element.parents('.checkbox-inline').size() > 0) {
                     error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                 } else {
                     error.insertAfter(element); // for other inputs, just perform default behavior
                 }
             },
             invalidHandler: function (event, validator) { //display error alert on form submit
                 success3.hide();
                 error3.show();
             },
             success: function (label) {
                 label
                     .closest('.form-group').removeClass('has-error'); // set success class to the control group
             },
             highlight: function (element) { // hightlight error inputs
                 $(element)
                     .closest('.form-group').addClass('has-error'); // set error class to the control group
             },

             unhighlight: function (element) { // revert the change done by hightlight
                 $(element)
                     .closest('.form-group').removeClass('has-error'); // set error class to the control group
             },
             submitHandler: function (form) {
                success3.show();
                error3.hide();
                var base_url = $('#BASE_URL').val();
    	        var _token = $('input[name=_token]').val();
    	        var str_params = $("#FORM_SAVE_JOBROLE").serialize();
    	         $.ajax
    	        ({
    	            url : base_url + "/request/SaveJobRoleInfo",
    	            data : str_params,
    	            dataType : "json",
    	            type : "POST",
    	            success : function(response){
    	              if(response.is_error == 0)
    	              {
    	                 window.location.href = base_url + "/administrator/JobRoles";
    	              }
    	            }
    	        });
             }

         });
	},
	DeleteJobRoleData : function(){
		 var jr_id = $(this).parents('tr').data('jr_id');
		bootbox.confirm("Are you sure you want to delete ?", function(result){
			//result
			if(result == true)
			{
			      var base_url = $('#BASE_URL').val();
			      var _token = $('input[name=_token]').val();
			        var str_params ={jr_id : jr_id , _token : _token};
			         $.ajax
			        ({
			            url : base_url + "/request/DeleteJobRole",
			            data : str_params,
			            dataType : "Json",
			            type : "POST",
			            success : function(response){
			              if(response.is_error == 0)
			              {
			            	  jobrole_module.displayListJobRole();
			              }
			            }
			        });
			}
		});




	},
	DisplayEditJobRoleForm : function(){
		var jr_id = $(this).parents('tr').data('jr_id');
	    var base_url = $("#BASE_URL").val();
	    window.location.href = base_url + "/administrator/EditJobRole/" + jr_id;
	},
	CancelForm : function(){
		 window.history.back();
	},
        DisplayPrintList: function (){
            var base_url = $("#BASE_URL").val();
	    var url  = base_url + "/administrator/print/jobrole";
            window.open(url,'_blank');
        },
        ExportToExcell: function(){
            var base_url = $("#BASE_URL").val();
            var url = base_url + "/administrator/exportexcel/jobrole";
            window.open(url,'_blank');
        }
};
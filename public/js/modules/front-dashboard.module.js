/**
 *
 */

fdashboard_module = {
	DisplayListRequestsNotifications : function(){
		var _token = $('input[name=_token]').val();
		var params = { _token : _token };
		var base_url = $('input[name=base_url]').val();

		$.ajax
        ({
            url : base_url + "/dashboard/DisplayNotificationsWidget",
            data : params,
            dataType : "json",
            type : "POST",
            success : function(response){
            	 $('.ProjectNotifications').html(response.display);
            }
        });


	},
	ApproveUserRequest : function(){
		var nr_id = $(this).parents('.NotificationRequest').data('nr_id');
		var _token = $('input[name=_token]').val();
		var params = {_token : _token , nr_id : nr_id};
		var base_url = $('input[name=base_url]').val();
		$.ajax
		({
			url : base_url + "/dashboard/AcceptNotificationRequest",
			data : params,
			dataType : "json",
			type : "POST",
			success : function(response){

				fdashboard_module.DisplayListRequestsNotifications();
			}
		});
	},
	RejectUserRequest : function(){
		var nr_id = $(this).parents('.NotificationRequest').data('nr_id');
		var _token = $('input[name=_token]').val();
		var params = {_token : _token , nr_id : nr_id};
		var base_url = $('input[name=base_url]').val();
		 $.ajax
	        ({
	            url : base_url + "/dashboard/RejectNotificationRequest",
	            data : params,
	            dataType : "json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  fdashboard_module.DisplayListRequestsNotifications();
	              }
	            }
	        });
	},
	PostPoneUserRequest : function(){
		var nr_id = $(this).parents('.NotificationRequest').data('nr_id');
		var _token = $('input[name=_token]').val();
		var params = {_token : _token , nr_id : nr_id};
		var base_url = $('input[name=base_url]').val();
		 $.ajax
	        ({
	            url : base_url + "/dashboard/PostPoneNotificationRequest",
	            data : params,
	            dataType : "json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  fdashboard_module.DisplayListRequestsNotifications();
	              }
	            }
	        });
	}

};
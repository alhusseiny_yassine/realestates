var surveys_module = {
    AjaxDisplaySurveys: function () {
        
        var base_url = $('input[name=base_url]').val();
        var _token = $('input[name=_token]').val();
        var page_number = $('input[name=page_number]').val();
        $.ajax
                ({
                    url: base_url + "/request/AjaxDisplaySurveys",
                    data: {
                        _token      : _token,
                        page_number : page_number
                    },
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        $('.ListSurveys').html(response.display);
                        $('#SurveysPagination').twbsPagination({
                            totalPages: response.total_pages,
                            visiblePages: 7,
                            onPageClick: function (event, page) {
                                $('input[name=page_number]').val(page);
                                surveys_module.AjaxDisplaySurveys();
                            }
                        });
                    }
                });

    },
    AddSurvey: function() {
        
        var base_url = $('input[name=base_url]').val();
        window.location.href = base_url + "/surveys/AddSurvey";
    },
    AddObjectDropdown: function(id,type) {
        
        switch (type) {
            case 'projects':
                $("#listprojectcourses").html('');
                $("#coursetrainer").html('');
                
                break;
            case 'courses':
                    $("#coursetrainer").html('');
                    
                break;    
        }
        if (id == '') {
            return;
        }
        var base_url = $('input[name=base_url]').val();
        var _token   = $('input[name=_token]').val();
        var project_id = $("#selected_project_id").val();
        $.ajax({
            url: base_url + "/request/AddObjectDropdown",
            data:{
                _token     : _token,
                id         : id,
                type       : type,
                project_id : project_id
            },
            dataType: "json",
            type: "POST",
            success: function (response) {

                switch (type) {
                    case 'projects':
                        if (response.is_error == 0) {
                            $("#listprojectcourses").html(response.display);
                        } else {
                            $("#listprojectcourses").html('');
                        }
                    break;
                case 'courses':
                        if (response.is_error == 0) {
                            $("#coursetrainer").html(response.display);
                        } else {
                            $("#coursetrainer").html('');
                        }
                }

            }
            
        });
        
    },
    SaveSurvey: function(){
       return surveys_module.HandleSaveSurvey();
    },
    HandleSaveSurvey: function(){
        var SurveyForm = $('#FORM_SAVE_SURVEY');
        var error      = $( '.alert-danger' , SurveyForm );
        var success    = $( '.alert-success', SurveyForm );
        
        SurveyForm.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                fk_project_id: {
                    required: true
                },
                fk_course_id: {
                    required: true
                }
            },

            messages: {
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {

                    error.appendTo("#GenderError");
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            invalidHandler: function (event, validator) {//display error alert on form submit
                success.hide();
                error.show();
            },
            success: function (label) {
                label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            submitHandler: function (form) {
                success.show();
                error.hide();
                var base_url   = $('#BASE_URL').val();
                var _token     = $('input[name=_token]').val();
                var str_params = $("#FORM_SAVE_SURVEY").serialize();
                $.ajax
                        ({
                            url: base_url + "/request/SaveSurvey",
                            data: str_params,
                            dataType: "json",
                            type: "POST",
                            success: function (response) {
                                if (response.is_error == 0)
                                {
                                    window.location.href = base_url + "/surveys/ListSurveys";
                                }
                            }
                        });
            }

        });
    },
    EditSurvey: function(){
        var base_url = $('input[name=base_url]').val();
        var survey_id= $(this).parents('tr').data('survey_id');
        window.location.href = base_url + "/surveys/EditSurvey/"+survey_id;
    },
    DeleteSurvey: function () {
        var base_url = $('input[name=base_url]').val();
        var survey_id = $(this).parents('tr').data('survey_id');
        var _token = $('input[name=_token]').val();
        bootbox.confirm({
            size: "small",
            message: "Are you sure you want to delete ?",
            callback: function (result) {
                if (result == true) {
                    $.ajax
                            ({
                                url: base_url + "/request/DeleteSurvey",
                                data: {
                                    _token: _token,
                                    survey_id: survey_id
                                },
                                dataType: "json",
                                type: "POST",
                                success: function (response) {
                                    if (response.is_error == 0)
                                    {
                                        surveys_module.AjaxDisplaySurveys();
                                    }
                                }
                            });
                }
            }
        });

    },
    ListSurveyAnswers: function () {
        
        var base_url = $('input[name=base_url]').val();
        var _token = $('input[name=_token]').val();
        var page_number = $('input[name=page_number]').val();
        var survey_id   = $("#survey_id").val();
        $.ajax
                ({
                    url: base_url + "/request/ListSurveyAnswers",
                    data: {
                        _token      : _token,
                        page_number : page_number,
                        survey_id   : survey_id
                    },
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        $('#LISTSURVEYANSWERS').html(response.display);
                        $('#AnswersPagination').twbsPagination({
                            totalPages: response.total_pages,
                            visiblePages: 7,
                            onPageClick: function (event, page) {
                                $('input[name=page_number]').val(page);
                                surveys_module.ListSurveyAnswers();
                            }
                        });
                    }
                });

    },
    ViewQuestionsAnswers: function () {
        
        var base_url = $('input[name=base_url]').val();
        var _token = $('input[name=_token]').val();
        var survey_id   = $(this).data('survey_id');
        var trainee_id  = $(this).parents('tr').data('trainee_id');
        $.ajax
                ({
                    url: base_url + "/request/ViewQuestionsAnswers",
                    data: {
                        _token      : _token,
                        survey_id   : survey_id,
                        trainee_id  : trainee_id
                        
                    },
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        $('#LISTSURVEYANSWERS').html(response.display);
                        $('#AnswersPagination').html('');
                    }
                });
    }
};
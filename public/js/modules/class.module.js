/**
 * 
 */

var $modelPopup = $('#ModelPopUp');
class_module = { 
	DisplayCoursesDropdown : function(){
		var base_url = $('input[name=base_url]').val();
	    var _token = $('input[name=_token]').val();
	    var project_id = $(this).val(); 
	    var params = {project_id : project_id , _token : _token };
	    $.ajax
        ({
            url : base_url + "/request/GetCoursesDropdown",
            data : params,
            dataType : "Json",
            type : "POST",
            success : function(response){
            	 $('.CoursesDropdown').html(response.display);
            	 console.log("OK");
            },
             error: function (jqXHR, exception) {
             	 console.log(exception);
             	},
        });
	},
	DisplaySessionsData : function(){
		var base_url 	= $('input[name=base_url]').val();
		var _token 		= $('input[name=_token]').val()
	    var page_number 		= $('input[name=page_number]').val()
	    var project_id 	= $('#PP_PROJECT').val();
	    var course_id 	= $('#PP_COURSE').val();
	    var display_type 	= $('input[name=display_type]').val();
    	$('.ListClasses').html('');
	    var params = {project_id : project_id , page_number : page_number , display_type : display_type , course_id : course_id , _token : _token };
	    
	    $.ajax
	    ({
	        url : base_url + "/request/displaylistClassSessions",
	        data : params,
	        dataType : "json",
	        type : "POST",
	        success : function(response){ 
	        	if(response.is_error == 1)
	    		{
	        		bootbox.alert(response.error_msg);
	    		}
	        	else
        		{ 
	        		$('.ListClasses').html(response.display); 
	        		 if(display_type == 'list')
        			 {
	        			 $('.group-checkable').change(function() {
	                         var set = $('table').find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
	                         var checked = $(this).prop("checked");
	                         $(set).each(function() {
	                             $(this).prop("checked", checked);
	                         });
	                         $.uniform.update(set);
	                     });

	                      $('#ClassesPagination').twbsPagination({
	                          totalPages: response.total_pages,
	                          visiblePages: 7,
	                          onPageClick: function (event, page) {
	                               $('input[name=page_number]').val(page);
	                               class_module.DisplaySessionsData();
	                          }
	                      });
        			 }
	        		 else
        			 {
	        			 var project_id 				= response.project_id;
	 	            	scheduler.attachEvent("onEventclick",function(id,ev){
	 						return false;
	 					});
	 					scheduler.attachEvent("onClick",function(id,ev){
	 						return false;
	 					});
	 					scheduler.attachEvent("onEventChanged", function(id,ev){
	 						return false;
	 					});
	 					scheduler.attachEvent("onDblClick", function (id, e){
	 						var session_id = id;
							
							var base_url = $("#BASE_URL").val();
					        var url = base_url + "/session/EditSessionInformation/" + session_id;
					        $('body').modalmanager('loading');
					        setTimeout(function(){
					        	$modelPopup.load(url, '', function(){
					        		$modelPopup.modal();
					        		   $("input[id=SC_SESSION_DATE]").datepicker({ 
					        				orientation : 'bottom',
					        				keepOpen : false,
					        				format : "yyyy-mm-dd"
					        			});
					        		   $("#BTN_SAVE_SESSION").on('click',projects_module.SaveSessionInformation);

					          });
					        }, 1000);
							
					       return false;
	 				  });
	 	            	scheduler.config.xml_date="%Y-%m-%d %H:%i";
	 	        		scheduler.init('SessionScheduler',new Date(response.class_start_date),"week");
	 	        		var calendar_url = response.calendar_url;
	 	     
	 	        		scheduler.load(calendar_url);
                                        scheduler.config.dblclick_create = false;
        			 }
        		}
	           
	        }
	    });
	},
	SwitchSessionsView : function(){
		var type = $(this).data('type');
		$('input[name=display_type]').val(type);
		$('input[name=page_numbe]').val(1);
		class_module.DisplayListSessions();
		
	},
	EditSessionInfo : function(){
		var session_id = $(this).parents('tr').data('sc_id');
		
		var base_url = $("#BASE_URL").val();
        var url = base_url + "/session/EditSessionInformation/" + session_id;
       
        $('body').modalmanager('loading');
        setTimeout(function(){ 
        	console.log($modelPopup.size());
        	$modelPopup.load(url, '', function(){
        		$modelPopup.modal();
        		
        		   $("input[id=SC_SESSION_DATE]").datepicker({ 
        				orientation : 'bottom',
        				keepOpen : false,
        				format : "yyyy-mm-dd"
        			});
        		   $("#BTN_SAVE_SESSION").on('click',projects_module.SaveSessionInformation);

          });
        }, 1000);
		
       return false;
	},
	DeleteSessionInfo : function(){
		var sc_id = $(this).parents('tr').data('sc_id');
		bootbox.confirm("Are you sure you want to delete ?", function(result){
			//result
			if(result == true)
			{
			      var base_url = $('#BASE_URL').val();
			      var _token = $('input[name=_token]').val();
			        var str_params ={sc_id : sc_id , _token : _token};
			         $.ajax
			        ({
			            url : base_url + "/request/DeleteSessionClass",
			            data : str_params,
			            dataType : "Json",
			            type : "POST",
			            success : function(response){
			              if(response.is_error == 0)
			              {
			            	  class_module.DisplayListSessions();
			              }
			            }
			        });
			}
		});
	},
	DisplayClassesDropdown : function(){
		var base_url = $('input[name=base_url]').val();
	    var _token = $('input[name=_token]').val()
	    var project_id = $(this).val();
	    var params = {project_id : project_id , _token : _token };
	    $.ajax
        ({
            url : base_url + "/request/GetClassesDropdown",
            data : params,
            dataType : "Json",
            type : "POST",
            success : function(response){
            	 $('.ClassesDropdown').html(response.display);
            }
        });
	},
	OK : function(){
			console.log('ok');
		},

		DisplayClassesDropdownFront : function(){
		var base_url = $('input[name=base_url]').val();
	    var _token = $('input[name=_token]').val()
	    var project_id = $(this).val();
	    var user_id=$('input[name=user_id]').val() ;
	   	console.log(user_id);
	    var params = {project_id : project_id , _token : _token, user_id : user_id };
	    $.ajax
        ({
            url : base_url + "/request/GetClassesDropdownFront",
            data : params,
            dataType : "Json",
            type : "POST",
            success : function(response){
            	 $('.ClassesDropdown').html(response.display);
            }
        });
	},
	DisplayListSessions : function(){
		var base_url = $('input[name=base_url]').val();
	    var _token = $('input[name=_token]').val();
	    var class_id = $(this).val();
	    var params = {class_id : class_id , _token : _token };
	    $.ajax
        ({
            url : base_url + "/request/GetSessionDropdown",
            data : params,
            dataType : "Json",
            type : "POST",
            success : function(response){
            	 $('.SessionDropdown').html(response.display);
            }
        });
	},
	DisplayListClassTrainees : function(){
		var base_url = $('input[name=base_url]').val();
	    var _token = $('input[name=_token]').val();
	    var class_id = $('#FK_CLASS_ID').val();
	    var session_id = $('#SC_SESSIONS_CLASS').val();
	    var params = {class_id : class_id , session_id : session_id , _token : _token };
	    $.ajax
        ({
            url : base_url + "/request/DisplayListClassTrainees",
            data : params,
            dataType : "Json",
            type : "POST",
            success : function(response){
            	 $('.ListAttendances').html(response.display);
            }
        });
	},
	SaveClassAttendances : function(){
		var base_url = $('input[name=base_url]').val();
	    var _token = $('input[name=_token]').val();
	    var from_params = $("#FRM_TRAINEES_ATTENDANCES").serialize();
	    $.ajax
        ({
            url : base_url + "/request/SaveTraineeAttendances",
            data : from_params,
            dataType : "Json",
            type : "POST",
            success : function(response){
            	 bootbox.alert(response.error_msg);
            }
        });
	},
	SaveTestResult : function(){
		var base_url = $('input[name=base_url]').val();
	    var _token = $('input[name=_token]').val();
	    var from_params = $("#FRM_TRAINEES_RESULTS").serialize();
	    $.ajax
        ({
            url : base_url + "/request/SaveTraineeResults",
            data : from_params,
            dataType : "Json",
            type : "POST",
            success : function(response){
            	 bootbox.alert(response.error_msg);
            }
        });
	},
	DisplayListSessionsTest : function(){
		var base_url = $('input[name=base_url]').val();
	    var _token = $('input[name=_token]').val();
	    var class_id = $(this).val();
	    var params = {class_id : class_id , _token : _token };
	    $.ajax
        ({
            url : base_url + "/request/GetSessionsTestDropdown",
            data : params,
            dataType : "Json",
            type : "POST",
            success : function(response){
            	 $('.SessionDropdown').html(response.display);
            }
        });
	},
	DisplayListClassTraineesResults : function(){
		var base_url = $('input[name=base_url]').val();
	    var _token = $('input[name=_token]').val();
	    var class_id = $('#FK_CLASS_ID').val();
	    var session_id = $('#SC_SESSIONS_CLASS').val();
	    var params = {class_id : class_id , session_id : session_id , _token : _token };
	    $.ajax
        ({
            url : base_url + "/request/DisplayListClassTraineesTestResult",
            data : params,
            dataType : "Json",
            type : "POST",
            success : function(response){
            	 $('.ListTestResults').html(response.display);
            }
        });
	},
	DisplayListDataMyClasses : function(){
		var base_url 			= $('input[name=base_url]').val();
		var _token 				= $('input[name=_token]').val();
		var display_type 		= $('input[name=display_type]').val();
		var data_info 			= $('input[name=data_info]').val();
	    var pp_id 			= $('input[name=pp_id]').val();
	    var params = {data_info : data_info , display_type : display_type , _token : _token , pp_id : pp_id };
	    $.ajax
        ({
            url : base_url + "/request/DisplayMyClassesInfo",
            data : params,
            dataType : "Json",
            type : "POST",
            success : function(response){
            	$('.MyClassesInfo').html(response.display);
            	 $('.headerLink').html(response.header_link);
            }
        });
	},
	ChangeViewTypeDataMyClasses : function(){
		var data_type = $(this).data('data_type');
		$('input[name=display_type]').val(data_type);
		class_module.DisplayListDataMyClasses();
	},
	DisplayLstMyClasses : function(){
		$('input[name=data_info]').val('classes');
		$('input[name=display_type]').val('list');
		
		
		var base_url 			= $('input[name=base_url]').val();
		var _token 				= $('input[name=_token]').val();
		var display_type 		= $('input[name=display_type]').val();
		var data_info 			= $('input[name=data_info]').val();
	    var pp_id 				= $(this).parents('.ProjectItem').data('pp_id'); 
	    $('input[name=pp_id]').val(pp_id);
	    var params = {data_info : data_info , display_type : display_type , _token : _token , pp_id : pp_id };
	    $.ajax
        ({
            url : base_url + "/request/DisplayMyClassesInfo",
            data : params,
            dataType : "Json",
            type : "POST",
            success : function(response){
            	 $('.MyClassesInfo').html(response.display);
            	 $('.headerLink').html(response.header_link);
            }
        });
	},
	DisplayListMySessions : function(){
		$('input[name=data_info]').val('sessions');
		$('input[name=display_type]').val('list');
		
		
		var base_url 			= $('input[name=base_url]').val();
		var _token 				= $('input[name=_token]').val();
		var display_type 		= $('input[name=display_type]').val();
		var data_info 			= $('input[name=data_info]').val();
	    var pg_id 				= $(this).parents('tr').data('pg_id'); 
	    $('input[name=pg_id]').val(pg_id);
	    var params = {data_info : data_info , display_type : display_type , _token : _token , pg_id : pg_id };
	    $.ajax
        ({
            url : base_url + "/request/DisplayMyClassesInfo",
            data : params,
            dataType : "Json",
            type : "POST",
            success : function(response){
                if(response.is_error == 1){
                    bootbox.alert(response.error_msg);
                }else{
                    $('.MyClassesInfo').html(response.display);
            	    $('.headerLink').html(response.header_link);
                }
            	 
            }
        });
	},
	DisplayAllMyProjects : function(){
		$('input[name=data_info]').val('projects');
		$('input[name=pp_id]').val('');
		class_module.DisplayListDataMyClasses();
	},
        ViewSurvey: function(){
            var pg_id = $(this).parents('tr').data('pg_id');
            var base_url = $('input[name=base_url]').val();
            window.open( base_url + "/surveys/ViewSurvey/"+pg_id ,'_blank'); 
        },
        SubmitSurvey: function(){
             return class_module.HandleSubmitSurvey();
    },
    HandleSubmitSurvey: function () {
        var SurveyForm = $('#FRM_SUBMIT_SURVEY');
        var error = $('.alert-danger', SurveyForm);
        var success = $('.alert-success', SurveyForm);

        SurveyForm.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                "sq_question[]": {
                    required: true
                }
            },

            messages: {
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {

                    error.appendTo("#GenderError");
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            invalidHandler: function (event, validator) {//display error alert on form submit
                success.hide();
                error.show();
            },
            success: function (label) {
                label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            submitHandler: function (form) {

                var is_error = 0;

                $(".sq_question").each(function () {
                    if ($(this).val() == '') {
                        $(this).closest('.form-group').addClass('has-error');
                        is_error = 1;
                    } else {
                        $(this).closest('.form-group').removeClass('has-error');
                    }
                });

                if (is_error == 1) {
                    success.hide();
                    error.show();
                    return;
                }

                success.show();
                error.hide();

                var base_url = $('#BASE_URL').val();
                var _token = $('input[name=_token]').val();
                var str_params = $("#FRM_SUBMIT_SURVEY").serialize();
                $.ajax
                        ({
                            url: base_url + "/request/SubmitSurvey",
                            data: str_params,
                            dataType: "json",
                            type: "POST",
                            success: function (response) {
                                if (response.is_error == 0)
                                {
                                    setTimeout(function () {
                                        window.close();
                                    }, 5000);
                                }
                            }
                        });
            }

        });
    }
};
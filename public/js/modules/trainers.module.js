$.validator.addMethod("mbcheck",
    function(value,element)
    {
        return /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/.test(value);
    });
trainers_module = {
    DisplayListTrainers: function () {
        var base_url    = $('input[name=base_url]').val();
        var _token      = $('input[name=_token]').val();
        var page_number = $('input[name=page_number]').val();
        $.ajax
                ({
                    url: base_url + "/request/DisplayListTrainers",
                    data: {_token: _token, page_number: page_number},
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        $('.ListTrainers').html(response.display);

                        $('#TrainersPagination').twbsPagination({
                            totalPages: response.total_pages,
                            visiblePages: 7,
                            onPageClick: function (event, page) {
                                $('input[name=page_number]').val(page);
                                trainers_module.DisplayListTrainers();
                            }
                        });
                    }
                });
    },
    AddNewTrainer: function () {
       var base_url = $("#BASE_URL").val();
       window.location.href = base_url + "/trainers/addNewTrainer"; 
    },
    CheckIfUserExit: function () {
        var base_url  = $('#BASE_URL').val();
        var _token    = $('input[name=_token]').val();
        var user_name = $('#U_EMAIL').val();
        var user_id   = $("#user_id").val()
        
        if (user_name.length == 0)
            return false;
        
        var params = {_token: _token, user_name: user_name, user_id: user_id};
        $.ajax
                ({
                    url: base_url + "/request/checkUserName",
                    data: params,
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        $('#U_EMAIL').parents('.form-group').removeClass('has-error');
                        $('#U_EMAIL').parents('.form-group').find('.help-block').remove();
                        $('#U_EMAIL').removeClass('SuccessCheck')
                        var field_container = $('#U_EMAIL').parents('.form-group');
                        if (response.exist == 1)
                        {
                            $('#U_EMAIL').parents('.form-group').addClass('has-error');
                            $('<span />', {
                                'class': 'help-block help-block-error'
                            }).appendTo(field_container);
                            $('#U_EMAIL').parents('.form-group').find('.help-block').html('Email Already Exist');
                            $("#BTN_SAVE_TRAINER").attr('disabled', 'disabled');
                        } else
                        {
                            $('#U_EMAIL').addClass('SuccessCheck');
                            $("#BTN_SAVE_TRAINER").removeAttr('disabled');
                        }
                    }
                });
    },
    GenerateEmailDefaultEmailAddress: function () {
        var full_name     = $(this).val();
        var FullnameArray = full_name.split(" ");
        var email = "";
        for (var int = 0; int < FullnameArray.length; int++) {
            if (int == 0)
            {
                email = FullnameArray[int];
            } else
            {
                email = email + "-" + FullnameArray[int];
            }

        }

        $("#U_EMAIL").val(email + "@omsar.gov.lb");

    },
    SaveTrainerInfo: function () {
        return trainers_module.SaveTrainerInfoHandler();
    },
    SaveTrainerInfoHandler: function () {
        var TrainerForm = $('#FORM_SAVE_TRAINER');
        var error3 = $('.alert-danger', TrainerForm);
        var success3 = $('.alert-success', TrainerForm);

        TrainerForm.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                fullname: {
                    minlength: 2,
                    required: true
                },
                u_email: {
                    email: true,
                    minlength: 5,
                    required: true
                },
                u_password: {
                    minlength: 8,
                    required: true
                },
                retype_u_password: {
                    minlength: 8,
                    equalTo: "#U_PASSWORD",
                    required: true
                },
                u_gender: {
                    required: true
                },
                u_date_birth: {
                    required: true
                },
                u_password_edit: {
                    minlength: 8,
                },
                retype_u_password_edit: {
                    minlength: 8,
                    equalTo: "#U_PASSWORD_EDIT",
                },
                u_mobile:{
                    mbcheck: true
                },
                u_phone: {
                    mbcheck: true
                }
                
            },

            messages: {// custom messages for radio buttons and checkboxes
                u_mobile : {
                        mbcheck: "Provide a valid number format.",
                    },
                u_phone : {
                        mbcheck: "Provide a valid number format.",
                    }    
                    

            },
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {

                    error.appendTo("#GenderError");
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
            },
            success: function (label) {
                label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            submitHandler: function (form) {
                success3.show();
                error3.hide();
                var base_url   = $('#BASE_URL').val();
                var _token     = $('input[name=_token]').val();
                var str_params = $("#FORM_SAVE_TRAINER").serialize();
                $.ajax
                        ({
                            url: base_url + "/request/SaveTrainerInfo",
                            data: str_params,
                            dataType: "json",
                            type: "POST",
                            success: function (response) {
                                if (response.is_error == 0)
                                {
                                    window.location.href = base_url + "/trainers/trainersManagement";
                                }
                            }
                        });
            }

        });
    },
    EditTrainer: function () {
       var base_url = $("#BASE_URL").val();
       var user_id  = $(this).parents('tr').data('user_id');
       window.location.href = base_url + "/trainers/EditTrainer/"+user_id; 
    },
    DeleteTrainer: function () {
        if(!confirm('Are you sure do you want to delete ?'))
	           return false;
	      var user_id = $(this).parents('tr').data('user_id');
	      var base_url = $('#BASE_URL').val();
	      var _token = $('input[name=_token]').val();
	        var str_params ={user_id : user_id , _token : _token};
	         $.ajax
	        ({
	            url : base_url + "/request/DeleteTrainer",
	            data : str_params,
	            dataType : "Json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  trainers_module.DisplayListTrainers();
	              }
	            }
	        });
    }
};
/**
 *
 */
roles_module = {
	displayListRoles : function(){
		var base_url = $('#BASE_URL').val();
	    var _token = $('input[name=_token]').val();

	    $.ajax
	    ({
	        url : base_url + "/request/displayListRoles",
	        data : {_token : _token},
	        dataType : "html",
	        type : "POST",
	        success : function(response){
	            $('.ListRoleGirds').html(response);
	            $('.group-checkable').change(function() {
                    var set = $('table').find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
                    var checked = $(this).prop("checked");
                    $(set).each(function() {
                        $(this).prop("checked", checked);
                    });
                    $.uniform.update(set);
                });
	        }
	    });
	},
	CheckAllPrivileges : function(){
		/** var checkbox = $(this).is(':checked');
		 alert(checkbox);
		 if(checkbox == true)
		 {
			 var CheckBoxes = $(this).parents('table').find('input[type=checkbox]');
			 CheckBoxes.each(function(){
				 $(this).attr('checked',true);
			 });
		 }
		 else
		 {
			 var CheckBoxes = $(this).parents('table').find('input[type=checkbox]');
			 CheckBoxes.each(function(){
				 $(this).removeAttr('checked');
			 });
		 }*/
	},
	AddNewRoleForm : function(){
		var base_url = $("#BASE_URL").val();
      window.location.href = base_url + "/administrator/addRole";
	},
	AddRoleInfo : function(){
		return roles_module.HandleSubmitForm();
	},
	HandleSubmitForm : function(){
		 var ProfileForm = $('#FORM_ADD_ROLE');
         var error3 = $('.alert-danger', ProfileForm);
         var success3 = $('.alert-success', ProfileForm);

         ProfileForm.validate({
             errorElement: 'span', //default input error message container
             errorClass: 'help-block help-block-error', // default input error message class
             focusInvalid: false, // do not focus the last invalid input
             ignore: "", // validate all fields including form hidden input
             rules: {
            	 role_name: {
            		 minlength: 4,
            		 required: true
            	 }
             },

             messages: { // custom messages for radio buttons and checkboxes

             },
             errorPlacement: function (error, element) { // render error placement for each input type
                 if (element.parent(".input-group").size() > 0) {
                     error.insertAfter(element.parent(".input-group"));
                 } else if (element.attr("data-error-container")) {
                     error.appendTo(element.attr("data-error-container"));
                 } else if (element.parents('.radio-list').size() > 0) {
                     error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                 } else if (element.parents('.radio-inline').size() > 0) {
                     error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                 } else if (element.parents('.checkbox-list').size() > 0) {
                     error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                 } else if (element.parents('.checkbox-inline').size() > 0) {
                     error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                 } else {
                     error.insertAfter(element); // for other inputs, just perform default behavior
                 }
             },
             invalidHandler: function (event, validator) { //display error alert on form submit
                 success3.hide();
                 error3.show();
             },
             success: function (label) {
                 label
                     .closest('.form-group').removeClass('has-error'); // set success class to the control group
             },
             highlight: function (element) { // hightlight error inputs
                 $(element)
                     .closest('.form-group').addClass('has-error'); // set error class to the control group
             },

             unhighlight: function (element) { // revert the change done by hightlight
                 $(element)
                     .closest('.form-group').removeClass('has-error'); // set error class to the control group
             },
             submitHandler: function (form) {
                success3.show();
                error3.hide();

                var base_url = $('#BASE_URL').val();
    	        var _token = $('input[name=_token]').val();
    	        var str_params = $("#FORM_ADD_ROLE").serialize();
    	        str_params +=  "&_token=" + _token;
    	         $.ajax
    	        ({
    	            url : base_url + "/request/addRole",
    	            data : str_params,
    	            dataType : "json",
    	            type : "POST",
    	            success : function(response){
    	              if(response.is_error == 0)
    	              {
    	                  window.location.href = base_url + "/administrator/RolesManagement";
    	              }
    	            }
    	        });
             }

         });
	},
	EditRoleForm : function(){
	      var role_id = $(this).parents('tr').data('role_id');
	      var base_url = $("#BASE_URL").val();
	      window.location.href = base_url + "/administrator/editRole/" + role_id;
	},
	DeleteRoleInfo : function(){

		 var role_id 	= $(this).parents('tr').data('role_id');
		bootbox.confirm("Are you sure you want to delete ?", function(result){
			if(result == true)
			{

			      var base_url 	= $('#BASE_URL').val();
			      var _token = $('input[name=_token]').val();
			        var str_params ={role_id : role_id , _token : _token};
			         $.ajax
			        ({
			            url : base_url + "/request/DeleteRole",
			            data : str_params,
			            dataType : "Json",
			            type : "POST",
			            success : function(response){
			              if(response.is_error == 0)
			              {
			            	  roles_module.displayListRoles();
			              }
			            }
			        });
			}


		});


	},
        DisplayPrintList: function (){
            var base_url = $("#BASE_URL").val();
	    var url  = base_url + "/administrator/print/roles";
            window.open(url,'_blank');
        },
        ExportToExcell: function(){
            var base_url = $("#BASE_URL").val();
            var url = base_url + "/administrator/exportexcel/roles";
            window.open(url,'_blank');
        }
};
/**
 *
 */

invoices_module = {
    DisplayProjectsDropDown: function () {
        var base_url          = $('input[name=base_url]').val();
        var _token            = $('input[name=_token]').val();
        var sb_beneficiary_id = $(this).val();
        var name              = "fk_project_id";
        var id                = "FK_PROJECT_ID";
        $.ajax
                ({
                    url: base_url + "/request/GenerateDropdown/projects",
                    data: {_token: _token, sb_beneficiary_id: sb_beneficiary_id, name: name, id: id},
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        $('.ProjectDropDown').html(response.display);
                    }
                });
    },
    DisplayInvoiceInfo: function () {
        var base_url   = $('input[name=base_url]').val();
        var _token     = $('input[name=_token]').val();
        var project_id = $("#FK_PROJECT_ID").val();
        var params     = {_token: _token, project_id: project_id};
        $('.ListInvoicesGrid').html("");
        $.ajax
                ({
                    url: base_url + "/request/DisplayInvoiceData",
                    data: params,
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        $('.ListInvoicesGrid').html(response.display);
                    }
                });
    },
    GenerateInvoice: function() {
        $(this).prop("disabled",true);
        var base_url          = $('input[name=base_url]').val();
        var _token            = $('input[name=_token]').val();
        var fk_project_id     = $("#FK_PROJECT_ID").val();
        var fk_beneficiary_id = $("#FK_BENEFICIARY_ID").val();
        var invoice_total     = $("#invoice_total").val();
        var invoice_code      = $("#invoice_code").val();
        var invoice_ref       = $("#invoice_ref").val();
        
        var params            = {
            _token            : _token, 
            fk_project_id     : fk_project_id,
            fk_beneficiary_id : fk_beneficiary_id,
            invoice_total     : invoice_total,
            invoice_code      : invoice_code,
            invoice_ref       : invoice_ref
        };
        $.ajax
                ({
                    url : base_url + "/request/GenerateInvoice",
                    data: params,
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        if(response.is_error == 0){
                            var url = base_url + "/administrator/GenerateInvoicePdf/" + response.invoice_id;
                            window.open(url,'_blank');
                        }
                    }
                });
    }
};
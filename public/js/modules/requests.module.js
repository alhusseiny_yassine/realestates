/**
 *
 */
var $SendRequest = $('#SendRequest');
request_module = {
		DisplayListMyRequests : function(){
			var base_url = $('input[name=base_url]').val();
		    var _token = $('input[name=_token]').val()
		    var page_number = $('input[name=myrequests_page_number]').val();
		    var my_request_no = $('input[name=my_request_no]').val();
		    var my_request_title = $('input[name=my_request_title]').val();
		    var my_request_date = $('input[name=my_request_date]').val();
		    var my_request_status = $('select[name=my_request_status]').val();
		    $.ajax
		    ({
		        url : base_url + "/request/displaylistMyRequests",
		        data : { _token : _token , page_number : page_number , my_request_status : my_request_status , my_request_date : my_request_date , my_request_title : my_request_title , my_request_no : my_request_no },
		        dataType : "json",
		        type : "POST",
		        success : function(response){
		            $('.ListMyRequests').html(response.display);
		            $("#datatables_myrequests").tablesorter();
                             $('.group-checkable').change(function() {
                                var set = $('table').find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
                                var checked = $(this).prop("checked");
                                $(set).each(function() {
                                    $(this).prop("checked", checked);
                                });
                                $.uniform.update(set);
                            });

                             if(response.total_pages > 1)
                        	 {
                            	  $('#MyRequestsPagination').twbsPagination({
                                      totalPages: response.total_pages,
                                      visiblePages: 7,
                                      onPageClick: function (event, page) {
                                           $('input[name=myrequests_page_number]').val(page);
                                           request_module.DisplayListMyRequests();
                                      }
                                  });
                        	 }

		        }
		    });
		},
		ResetMyRequestsSearchForm : function(){
		    $('input[name=my_request_no]').val('');
		    $('input[name=my_request_title]').val('');
		    $('input[name=my_request_date]').val('');
		    $('select[name=my_request_status]').val('');
		    request_module.DisplayListMyRequests();
		},
		DisplayEditRequestForm : function(){
			var r_id = $(this).parents('tr').data('r_id');
			var base_url = $("#BASE_URL").val();
			window.location.href = base_url  + "/administrator/EditRequestInfo/"  + r_id;
		},
		SaveRequestInfo : function(){
			request_module.SaveRequestSubmitHandler();
		},
		SaveRequestSubmitHandler : function(){
			var SaveRequestForm = $('#FRM_SAVE_REQUEST');

	        var error3 = $('.alert-danger', SaveRequestForm);
	        var success3 = $('.alert-success', SaveRequestForm);

	        SaveRequestForm.validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block help-block-error', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "", // validate all fields including form hidden input
	            rules: {
	            	r_request_code: {
	            		required: true
	            	},
	            	r_request_train_date: {
	            		required: true
	            	},
	            	rq_request_trainee: {
	            		required: true
	            	},
	           	 	r_request_status: {
	           		 	required: true
	           	 	},
	           	 r_request_order : {
	           		 number : true,
	           		required: true
	           	 }
	            },
	            messages: { // custom messages for radio buttons and checkboxes

	            },
	            errorPlacement: function (error, element) { // render error placement for each input type
	                if (element.parent(".input-group").size() > 0) {
	                    error.insertAfter(element.parent(".input-group"));
	                } else if (element.attr("data-error-container")) {
	                    error.appendTo(element.attr("data-error-container"));
	                } else if (element.parents('.radio-list').size() > 0) {
	                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
	                } else if (element.parents('.radio-inline').size() > 0) {
	                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
	                } else if (element.parents('.checkbox-list').size() > 0) {
	                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
	                } else if (element.parents('.checkbox-inline').size() > 0) {
	                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
	                } else {
	                    error.insertAfter(element); // for other inputs, just perform default behavior
	                }
	            },
	            invalidHandler: function (event, validator) { //display error alert on form submit
	                success3.hide();
	                error3.show();
	            },
	            success: function (label) {
	                label
	                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
	            },
	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            unhighlight: function (element) { // revert the change done by hightlight
	                $(element)
	                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
	            },
	            submitHandler: function (form) {

	               var base_url = $('#BASE_URL').val();
	   	        var _token = $('input[name=_token]').val();
	   	        var str_params = $("#FRM_SAVE_REQUEST").serialize();
	   	         $.ajax
	   	        ({
	   	            url : base_url + "/request/SaveRequestInformation",
	   	            data : str_params,
	   	            dataType : "json",
	   	            type : "POST",
	   	            success : function(response){
	   	            	if(response.is_error == 1)
   	            		{
	   	            		success3.hide();
	   		               error3.show();
   	            			alert(response.error_msg);
   	            		}
	   	            	else
   	            		{
	   	            		success3.show();
	   		               error3.hide();
	   	            		var base_url = $("#BASE_URL").val();
		   	            	window.location.href = base_url + "/administrator/requests";
   	            		}

	   	            }
	   	        });
	            }

	        });
		},
		DisplayListAllRequests : function(){
			var base_url 	= $('input[name=base_url]').val();
		    var _token 		= $('input[name=_token]').val()
		    var page_number = $('input[name=allrequests_page_number]').val();
		    var is_admin 	= $('input[name=is_admin]').val();
		    var sl_courses 	= $('select[name=sl_courses]').val();
		    var sb_benf 	= $('select[name=sb_benf]').val();
		    var rq_request_trainee 	= $('select[name=rq_request_trainee]').val();
		    var rq_request_status 	= $('select[name=rq_request_status]').val();
		    $.ajax
		    ({
		        url : base_url + "/request/displaylistAllRequests",
		        data : { _token : _token , page_number : page_number , is_admin : is_admin , rq_request_status : rq_request_status , sb_benf : sb_benf , rq_request_trainee : rq_request_trainee , sl_courses : sl_courses},
		        dataType : "json",
		        type : "POST",
		        success : function(response){
		            $('.ListAllRequests').html(response.display);
                     $('.group-checkable').change(function() {
                        var set = $('table').find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
                        var checked = $(this).prop("checked");
                        $(set).each(function() {
                            $(this).prop("checked", checked);
                        });
                        $.uniform.update(set);
                    });
                     //$("#datatable_allrequests").tablesorter();
                     if(response.total_pages > 1)
                	 {

                    	 //$('#AllRequestsPagination').twbsPagination('destroy');
                    	 $('#AllRequestsPagination').twbsPagination({
                             totalPages: response.total_pages,
                             visiblePages: 7,
                             onPageClick: function (event, page) {
                                  $('input[name=allrequests_page_number]').val(page);
                                  request_module.DisplayListAllRequests();
                             }
                         });
                	 }
                     else
                     {
                    	// $('#AllRequestsPagination').twbsPagination('destroy');
                     }



		        }
		    });
		},
		DisplayRequestsDashboard : function(){
			var base_url = $('input[name=base_url]').val();
		    var _token = $('input[name=_token]').val()
		    $.ajax
		    ({
		        url : base_url + "/request/displayRequestsDashboard",
		        data : { _token : _token },
		        dataType : "json",
		        type : "POST",
		        success : function(response){
		            $('#RequestsDashboard').html(response.display);
		        }
		    });
		},
		SendRequestRejection : function(){
			return request_module.SendRequestRejectionSubmitHandler();
		},
		SendRequestRejectionSubmitHandler : function(){
			var RejectionForm = $('#FRM_REJECTION_REQUEST');
	        var error3 = $('.alert-danger', RejectionForm);
	        var success3 = $('.alert-success', RejectionForm);

	        RejectionForm.validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block help-block-error', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "", // validate all fields including form hidden input
	            rules:{},
	            messages: { // custom messages for radio buttons and checkboxes

	            },
	            errorPlacement: function (error, element) { // render error placement for each input type
	                if (element.parent(".input-group").size() > 0) {
	                    error.insertAfter(element.parent(".input-group"));
	                } else if (element.attr("data-error-container")) {
	                    error.appendTo(element.attr("data-error-container"));
	                } else if (element.parents('.radio-list').size() > 0) {
	                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
	                } else if (element.parents('.radio-inline').size() > 0) {
	                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
	                } else if (element.parents('.checkbox-list').size() > 0) {
	                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
	                } else if (element.parents('.checkbox-inline').size() > 0) {
	                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
	                } else {
	                    error.insertAfter(element); // for other inputs, just perform default behavior
	                }
	            },
	            invalidHandler: function (event, validator) { //display error alert on form submit
	                success3.hide();
	                error3.show();
	            },
	            success: function (label) {
	                label
	                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
	            },
	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            unhighlight: function (element) { // revert the change done by hightlight
	                $(element)
	                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
	            },
	            submitHandler: function (form) {
	               success3.show();
	               error3.hide();
	               var base_url = $('#BASE_URL').val();
	   	        var _token = $('input[name=_token]').val();
	   	        var str_params = $("#FRM_REJECTION_REQUEST").serialize();
	   	         $.ajax
	   	        ({
	   	            url : base_url + "/request/sendrejectionrequest",
	   	            data : str_params,
	   	            dataType : "json",
	   	            type : "POST",
	   	            success : function(response){
	   	              if(response.is_error == 0)
	   	              {
	   	                 $('.modal-scrollable').trigger('click');
	   	                 request_module.DisplayListAllRequests();
	   	              }
	   	            }
	   	        });
	            }

	        });
		},
		ApproveRequests : function(){
			    var base_url = $('#BASE_URL').val();
	   	        var _token = $('input[name=_token]').val();
	   	        
	   	     var checked_requests = '';

		        if($('input[id*=CK_REQUEST_]:checked').size() == 0)
		    	{
		        	bootbox.alert('Select Request Before you You send Rejection');
		        	return false;
		    	}

		        $('input[id*=CK_REQUEST_]:checked').each(function(n){
		    		if(n == 0)
					{
		    			checked_requests = $(this).val();
					}
		    		else
					{
		    			checked_requests += "," + $(this).val();
					}
		        });

		        // reset checkbox buttons
		        $('input[id*=CK_REQUEST_]').each(function(){
		        	$(this).removeAttr('checked');
		        });
	   	        
		        
		        bootbox.confirm({
		            message: "Are you Sure you want to Approve This Requests ?",
		            buttons: {
		                confirm: {
		                    label: 'Yes',
		                    className: 'btn-success'
		                },
		                cancel: {
		                    label: 'No',
		                    className: 'btn-danger'
		                }
		            },
		            callback: function (result) {
		                if(result == true)
	                	{
		                	var params = { request_ids : checked_requests , _token : _token };
		    	   	        $.ajax
		    	   	        ({
		    	   	            url : base_url + "/request/SendApprovalRequest",
		    	   	            data : params,
		    	   	            dataType : "json",
		    	   	            type : "POST",
		    	   	            success : function(response){
			    	   	              if(response.is_error == 0)
			    	   	              { 
			    	   	                   request_module.DisplayListAllRequests();
			    	   	              }
			    	   	              else
		    	   	            	  {
				    	   	            	bootbox.alert(response.error_msg);
				    			        	return false;
		    	   	            	  }
		    	   	            }
		    	   	        });
	                	}
		            }
		        });
		},
		OpenRejectionRequestPopup : function(){
			var el = $(this);
	        var checked_requests = '';

	        if($('input[id*=CK_REQUEST_]:checked').size() == 0)
	    	{
	        	bootbox.alert('Select Request Before you You send Rejection');
	        	return false;
	    	}

	        $('input[id*=CK_REQUEST_]:checked').each(function(n){
	    		if(n == 0)
				{
	    			checked_requests = $(this).val();
				}
	    		else
				{
	    			checked_requests += "," + $(this).val();
				}
	        });

	        // reset checkbox buttons
	        $('input[id*=CK_REQUEST_]').each(function(){
	        	$(this).removeAttr('checked');
	        });


	        var base_url = $("#BASE_URL").val();
	        var url = base_url + "/omsar/SendRequestsRejection/" + encodeURIComponent(checked_requests);
	        $('body').modalmanager('loading');

	        setTimeout(function(){
	        	$SendRequest.load(url, '', function(){
	        		$SendRequest.modal();
	        		$('#BTN_REJECT_REQUEST').on('click',request_module.SendRequestRejection);
	          });
	        }, 1000);
		},
		ChangeRequestStatusPopup : function(){
			var el = $(this);
	        var checked_requests = '';

	        if($('input[id*=CK_REQUEST_]:checked').size() == 0)
	    	{
	        	bootbox.alert('Select Request Before you You Change Status');
	        	return false;
	    	}

	        $('input[id*=CK_REQUEST_]:checked').each(function(n){
	    		if(n == 0)
				{
	    			checked_requests = $(this).val();
				}
	    		else
				{
	    			checked_requests += "," + $(this).val();
				}
	        });

	        // reset checkbox buttons
	        $('input[id*=CK_REQUEST_]').each(function(){
	        	$(this).removeAttr('checked');
	        });


	        var base_url = $("#BASE_URL").val();
	        var url = base_url + "/omsar/ChangeRequestStatus/" + encodeURIComponent(checked_requests);
	        $('body').modalmanager('loading');

	        setTimeout(function(){
	        	$SendRequest.load(url, '', function(){
	        		$SendRequest.modal();
	        		$('#BTN_CHANGE_STATUS').on('click',request_module.ChangeRequestStatus);
	          });
	        }, 1000);
		},
		ChangeRequestStatus : function(){
			return request_module.ChangeRequestStatusSubmitHandler();
		},
		ChangeRequestStatusSubmitHandler : function(){
			var RequestStatusForm = $('#FRM_REQUEST_STATUS');
	        var error3 = $('.alert-danger', RequestStatusForm);
	        var success3 = $('.alert-success', RequestStatusForm);

	        RequestStatusForm.validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block help-block-error', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "", // validate all fields including form hidden input
	            rules: {
	            	rs_request_status: {
	           		 required: true
	           	 }
	            },
	            messages: { // custom messages for radio buttons and checkboxes

	            },
	            errorPlacement: function (error, element) { // render error placement for each input type
	                if (element.parent(".input-group").size() > 0) {
	                    error.insertAfter(element.parent(".input-group"));
	                } else if (element.attr("data-error-container")) {
	                    error.appendTo(element.attr("data-error-container"));
	                } else if (element.parents('.radio-list').size() > 0) {
	                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
	                } else if (element.parents('.radio-inline').size() > 0) {
	                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
	                } else if (element.parents('.checkbox-list').size() > 0) {
	                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
	                } else if (element.parents('.checkbox-inline').size() > 0) {
	                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
	                } else {
	                    error.insertAfter(element); // for other inputs, just perform default behavior
	                }
	            },
	            invalidHandler: function (event, validator) { //display error alert on form submit
	                success3.hide();
	                error3.show();
	            },
	            success: function (label) {
	                label
	                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
	            },
	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            unhighlight: function (element) { // revert the change done by hightlight
	                $(element)
	                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
	            },
	            submitHandler: function (form) {
	               success3.show();
	               error3.hide();
	               var base_url = $('#BASE_URL').val();
	   	        var _token = $('input[name=_token]').val();
	   	        var str_params = $("#FRM_REQUEST_STATUS").serialize();
	   	         $.ajax
	   	        ({
	   	            url : base_url + "/request/changerequeststatus",
	   	            data : str_params,
	   	            dataType : "json",
	   	            type : "POST",
	   	            success : function(response){
	   	              if(response.is_error == 0)
	   	              {
	   	            	  request_module.DisplayListAllRequests();
	   	            	  request_module.DisplayListMyRequests();
	   	                 $('.modal-scrollable').trigger('click');
	   	              }
	   	            }
	   	        });
	            }

	        });
		},
		AddNewRequestForm : function(){
			var base_url = $("#BASE_URL").val();
		      window.location.href = base_url + "/administrator/AddNewRequestForm";
		},
		DeleteRequestData : function(){
			 var r_id = $(this).parents('tr').data('r_id');
				bootbox.confirm("Are you sure you want to delete ?", function(result){
					//result
					if(result == true)
					{
					      var base_url = $('#BASE_URL').val();
					      var _token = $('input[name=_token]').val();
					        var str_params ={r_id : r_id , _token : _token};
					         $.ajax
					        ({
					            url : base_url + "/request/DeleteRequest",
					            data : str_params,
					            dataType : "Json",
					            type : "POST",
					            success : function(response){
					            	request_module.DisplayListAllRequests();
					            }
					        });
					}
				});
		}
};
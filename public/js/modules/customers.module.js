var customers_module = {

    DisplayListCustomers: function () {
        var base_url        = $('input[name=base_url]').val();
	var _token          = $('input[name=_token]').val();
        var c_customer_type = $("#CUSTOMER_TYPE").val();
        var c_fullname      = $("#c_search_fullname").val();
        var c_email         = $("#c_search_email").val();
        var c_mobile        = $("#c_search_mobile").val();
        var page_number     = $("#page_number").val();
        
        $.ajax({
            url : base_url + "/request/DisplayListCustomers",
            data: {
                _token          : _token,
                page_number     : page_number,
                c_customer_type : c_customer_type,
                c_fullname      : c_fullname,
                c_email         : c_email,
                c_mobile        : c_mobile
            },
            dataType: "json",
            type: "POST",
            success: function (response) {
                $('.ListCustomers').html(response.display);
                
                if (response.total_pages > 1) {
                    $('#CustomersPagination').twbsPagination({
                        totalPages   : response.total_pages,
                        visiblePages : 5,
                        onPageClick: function (event, page) {
                            $('input[name=page_number]').val(page);
                            customers_module.DisplayListCustomers();
                        }
                    });
                } else {
                    $('#CustomersPagination').twbsPagination('destroy');
                }
                
            }
        });
    },
    AddCustomer: function () {
        var base_url         = $('input[name=base_url]').val();
        window.location.href = base_url + "/customers/AddCustomer";
    },
    CheckCustomerEmail: function () {
        var base_url        = $('input[name=base_url]').val();
        var _token          = $('input[name=_token]').val();
        var c_id            = $('#c_id').val();
        var c_email         = $('#c_email').val();   
        
        $.ajax({
            url : base_url + "/request/CheckCustomerEmail",
            data: {
                _token  : _token,
                c_email : c_email,
                c_id    : c_id
            },
            dataType: "json",
            type: "POST",
            success: function (response) {
                if( response.is_error == 1 ) {
                    $(".alert-success").hide();
                    $(".alert-danger").html(response.error_message);
                    $(".alert-danger").show();
                    $("#c_email").val('');
                } else {
                    $(".alert-danger").hide();
                    $(".alert-danger").html("<strong>Error!</strong> You have some form errors. Please check below.");
                }
            }
        });
    },
    SaveCustomerInfo: function() {
        customers_module.HandleSaveCustomerInfo();
    },
    HandleSaveCustomerInfo: function() {
        var SaveCustomer = $('#FORM_SAVE_CUSTOMER');
        var error        = $('.alert-danger', SaveCustomer);
        var success      = $('.alert-success', SaveCustomer);

        SaveCustomer.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                c_fullname: {
                    required: true
                },
                c_email: {
                    required: true,
                    email : true
                },
                c_mobile: {
                    required: true
                },
                c_customer_type: {
                    required: true
                },
                sc_session_date: {
                    required: true
                }

            },

            messages: {// custom messages for radio buttons and checkboxes

            },
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success.hide();
                error.show();
            },
            success: function (label) {
                label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            submitHandler: function (form) {
                success.show();
                error.hide();
                var base_url = $("#BASE_URL").val();
                var frm_info = $("#FORM_SAVE_CUSTOMER").serialize();
                $.ajax
                        ({
                            url: base_url + "/request/SaveCustomerInfo",
                            data: frm_info,
                            dataType: "json",
                            type: "POST",
                            success: function (response) {
                                $.alert({
                                    title: 'Success!',
                                    content: 'Customer Information Saved Successfuly',
                                    confirm: function () {
                                        window.location.href = base_url + "/administrator/CutomersManagement";
                                    }
                                });
                            }
                        });
            }

        });
    },
    EditCustomer: function (){
       var base_url = $("#BASE_URL").val();
       var c_id     = $(this).parents("tr").data("c_id");
       window.location.href = base_url + "/customers/EditCustomer/"+c_id;
    },
    DeleteCustomer: function () {
        var customer_id = $(this).parents('tr').data('c_id');
        var base_url    = $("#BASE_URL").val();
        var _token      = $('input[name=_token]').val();
        $.confirm({
            title: 'Alert!',
            content: 'Are you sure you want to delete ?',
            confirm: function () {
                $.ajax
                        ({
                            url: base_url + "/request/DeleteCustomer",
                            data: {
                                customer_id : customer_id,
                                _token      : _token
                            },
                            dataType: "json",
                            type: "POST",
                            success: function (response) {
                                customers_module.DisplayListCustomers();
                            }
                        });
            }
        });
    }
};
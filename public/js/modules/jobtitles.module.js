/**
 *
 */


jobtitle_module = {
	displayListJobTitle : function(){
		var base_url = $('input[name=base_url]').val();
		var _token = $('input[name=_token]').val()
	    var page_number 				= $('input[name=page_number]').val();
	    $.ajax
	    ({
	        url : base_url + "/request/displayListJobTitle",
	        data : { _token : _token , page_number : page_number },
	        dataType : "json",
	        type : "POST",
	        success : function(response){
	            $('.ListJobTitlesGird').html(response.display);
                         $('.group-checkable').change(function() {
                            var set = $('table').find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
                            var checked = $(this).prop("checked");
                            $(set).each(function() {
                                $(this).prop("checked", checked);
                            });
                            $.uniform.update(set);
                        });

                         $('#BenfPagination').twbsPagination({
                             totalPages: response.total_pages,
                             visiblePages: 7,
                             onPageClick: function (event, page) {
                                  $('input[name=page_number]').val(page);
                                  jobtitle_module.displayListJobTitle();
                             }
                         });
	        }
	    });
	},
	AddNewJobTitle : function(){
		var base_url = $("#BASE_URL").val();
      window.location.href = base_url + "/administrator/AddNewJobTitle";
	},
	SaveJobTitleInfo : function(){
		return jobtitle_module.SaveJobTitleSubmitHandler();
	},
	SaveJobTitleSubmitHandler : function(){
		 var JobTitleForm = $('#FORM_SAVE_JOBTITLE');
         var error3 = $('.alert-danger', JobTitleForm);
         var success3 = $('.alert-success', JobTitleForm);

         JobTitleForm.validate({
             errorElement: 'span', //default input error message container
             errorClass: 'help-block help-block-error', // default input error message class
             focusInvalid: false, // do not focus the last invalid input
             ignore: "", // validate all fields including form hidden input
             rules: {
            	 jt_job_title: {
            		 minlength: 4,
            		 required: true
            	 }
             },

             messages: { // custom messages for radio buttons and checkboxes

             },
             errorPlacement: function (error, element) { // render error placement for each input type
                 if (element.parent(".input-group").size() > 0) {
                     error.insertAfter(element.parent(".input-group"));
                 } else if (element.attr("data-error-container")) {
                     error.appendTo(element.attr("data-error-container"));
                 } else if (element.parents('.radio-list').size() > 0) {
                     error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                 } else if (element.parents('.radio-inline').size() > 0) {
                     error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                 } else if (element.parents('.checkbox-list').size() > 0) {
                     error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                 } else if (element.parents('.checkbox-inline').size() > 0) {
                     error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                 } else {
                     error.insertAfter(element); // for other inputs, just perform default behavior
                 }
             },
             invalidHandler: function (event, validator) { //display error alert on form submit
                 success3.hide();
                 error3.show();
             },
             success: function (label) {
                 label
                     .closest('.form-group').removeClass('has-error'); // set success class to the control group
             },
             highlight: function (element) { // hightlight error inputs
                 $(element)
                     .closest('.form-group').addClass('has-error'); // set error class to the control group
             },

             unhighlight: function (element) { // revert the change done by hightlight
                 $(element)
                     .closest('.form-group').removeClass('has-error'); // set error class to the control group
             },
             submitHandler: function (form) {
                success3.show();
                error3.hide();
                var base_url = $('#BASE_URL').val();
    	        var _token = $('input[name=_token]').val();
    	        var str_params = $("#FORM_SAVE_JOBTITLE").serialize();
    	         $.ajax
    	        ({
    	            url : base_url + "/request/SaveJobTitleInfo",
    	            data : str_params,
    	            dataType : "json",
    	            type : "POST",
    	            success : function(response){
    	              if(response.is_error == 0)
    	              {
    	                 window.location.href = base_url + "/administrator/jobtitles";
    	              }
    	            }
    	        });
             }

         });
	},
	DeleteJobTitleData : function(){
		 var jt_id = $(this).parents('tr').data('jt_id');
		bootbox.confirm("Are you sure you want to delete ?", function(result){
			//result
			if(result == true)
			{
			      var base_url = $('#BASE_URL').val();
			      var _token = $('input[name=_token]').val();
			        var str_params ={jt_id : jt_id , _token : _token};
			         $.ajax
			        ({
			            url : base_url + "/request/DeleteJobTitle",
			            data : str_params,
			            dataType : "Json",
			            type : "POST",
			            success : function(response){
			              if(response.is_error == 0)
			              {
			            	  jobtitle_module.displayListJobTitle();
			              }
			            }
			        });
			}
		});




	},
	DisplayEditJobTitleForm : function(){
		var jt_id = $(this).parents('tr').data('jt_id');
	    var base_url = $("#BASE_URL").val();
	    window.location.href = base_url + "/administrator/editJobTitleForm/" + jt_id;
	},
	CancelForm : function(){
		 window.history.back();
	},
        DisplayPrintList: function (){
            var base_url = $("#BASE_URL").val();
	    var url  = base_url + "/administrator/print/jobtitle";
            window.open(url,'_blank');
        },
        ExportToExcell: function(){
            var base_url = $("#BASE_URL").val();
            var url = base_url + "/administrator/exportexcel/jobtitle";
            window.open(url,'_blank');
        }
};
var surveyquestions_module = {
    AjaxViewQuestions: function () {
        
        var base_url = $('input[name=base_url]').val();
        var _token   = $('input[name=_token]').val();
        var page_number = $('input[name=page_number]').val();
        var survey_id   = $("#survey_id").val();
        $.ajax
                ({
                    url: base_url + "/request/AjaxViewQuestions",
                    data: {
                        _token      : _token,
                        page_number : page_number,
                        survey_id   : survey_id
                    },
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        $('.ListQuestions').html(response.display);
                        $('#QuestionsPagination').twbsPagination({
                            totalPages: response.total_pages,
                            visiblePages: 7,
                            onPageClick: function (event, page) {
                                $('input[name=page_number]').val(page);
                                surveyquestions_module.AjaxViewQuestions();
                            }
                        });
                    }
                });

    },
    AddQuestion: function() {
        
        var base_url = $('input[name=base_url]').val();
        var survey_id= $("#survey_id").val();
        window.location.href = base_url + "/surveys/AddQuestion/"+survey_id;
    },
    CheckQuestion: function() {
        var base_url = $('input[name=base_url]').val();
        var survey_id= $("#survey_id").val();
        var question = $("#sq_question").val();
        var sq_id    = $("#sq_id").val();
        var _token   = $('input[name=_token]').val();
        var url      = base_url + "/request/CheckQuestion";
        
        $.ajax({
            url: url,
            data: {
                survey_id: survey_id,
                question : question,
                _token   : _token,
                sq_id    : sq_id
            },
            dataType: "Json",
            type: "POST",
            success: function (response) {
                if (response.is_error == 1) {
                    $('#sq_question').val("");
                    $('.alert-danger').html(response.error_message);
                    $('.alert-danger').show();
                    setTimeout(function () {
                        $('.alert-danger').hide();
                    }, 3000);
                } else {
                    $('.alert-danger').hide();
                    $('.alert-danger').html('You have some form errors. Please check below.');
                }
            }
        });
    },
    SaveQuestion: function(){
       return surveyquestions_module.HandleSaveQuestion();
    },
    HandleSaveQuestion: function(){
        var QuestionForm = $('#FORM_SAVE_QUESTION');
        var error      = $( '.alert-danger' , QuestionForm );
        var success    = $( '.alert-success', QuestionForm );
        
        QuestionForm.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                sq_question: {
                    required: true,
                    rangelength: [10, 100]
                },
                sq_rating: {
                    required: true,
                    min:1,
                    max:5,
                    number:true
                },
                sq_order: {
                    required: true,
                    min:1,
                    number:true
                }
            },

            messages: {
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {

                    error.appendTo("#GenderError");
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            invalidHandler: function (event, validator) {//display error alert on form submit
                success.hide();
                error.show();
            },
            success: function (label) {
                label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            submitHandler: function (form) {
                success.show();
                error.hide();
                var survey_id  = $("#survey_id").val();
                var base_url   = $('#BASE_URL').val();
                var _token     = $('input[name=_token]').val();
                var str_params = $("#FORM_SAVE_QUESTION").serialize();
                $.ajax
                        ({
                            url: base_url + "/request/SaveQuestion",
                            data: str_params,
                            dataType: "json",
                            type: "POST",
                            success: function (response) {
                                if (response.is_error == 0)
                                {
                                    window.location.href = base_url + "/surveys/ViewQuestions/"+survey_id;
                                }
                            }
                        });
            }

        });
    },
    DeleteQuestion: function () {
        var base_url = $('input[name=base_url]').val();
        var sq_id = $(this).parents('tr').data('question_id');
        var _token = $('input[name=_token]').val();
        bootbox.confirm({
            size: "small",
            message: "Are you sure you want to delete ?",
            callback: function (result) {
                if (result == true) {
                    $.ajax
                            ({
                                url: base_url + "/request/DeleteQuestion",
                                data: {
                                    _token: _token,
                                    sq_id : sq_id
                                },
                                dataType: "json",
                                type: "POST",
                                success: function (response) {
                                    if (response.is_error == 0)
                                    {
                                        surveyquestions_module.AjaxViewQuestions();
                                    }
                                }
                            });
                }
            }
        });

    },
    AjaxViewAnswers: function () {
        
        var base_url = $('input[name=base_url]').val();
        var _token   = $('input[name=_token]').val();
        var page_number = $('input[name=page_number]').val();
        var sq_id   = $("#sq_id").val();
        $.ajax
                ({
                    url: base_url + "/request/AjaxViewAnswers",
                    data: {
                        _token      : _token,
                        page_number : page_number,
                        sq_id   : sq_id
                    },
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        $('.ListAnswers').html(response.display);
                        $('#AnswersPagination').twbsPagination({
                            totalPages: response.total_pages,
                            visiblePages: 7,
                            onPageClick: function (event, page) {
                                $('input[name=page_number]').val(page);
                                surveyquestions_module.AjaxViewAnswers();
                            }
                        });
                    }
                });

    }
};
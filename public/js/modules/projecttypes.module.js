/**
 *
 */


project_type_module = {
		displayListProjectTypes : function(){
			var base_url = $('input[name=base_url]').val();
		    var _token = $('input[name=_token]').val()
		    var page_number = $('input[name=page_number]').val();
		    $.ajax
		    ({
		        url : base_url + "/request/displayprojecttypes",
		        data : { _token : _token , page_number : page_number },
		        dataType : "json",
		        type : "POST",
		        success : function(response){
		            $('.ListProjectTypesGird').html(response.display);
                             $('.group-checkable').change(function() {
                                var set = $('table').find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
                                var checked = $(this).prop("checked");
                                $(set).each(function() {
                                    $(this).prop("checked", checked);
                                });
                                $.uniform.update(set);
                            });

                             $('#ProjectTypePagination').twbsPagination({
                                 totalPages: response.total_pages,
                                 visiblePages: 7,
                                 onPageClick: function (event, page) {
                                      $('input[name=page_number]').val(page);
                                      project_type_module.displayListCourseTypes();
                                 }
                             });
		        }
		    });
		},
		AddNewProjectTypeForm : function(){
			var base_url = $("#BASE_URL").val();
	      window.location.href = base_url + "/administrator/AddNewProjectType";
		},
	SaveProjectTypeInfo : function(){
		return project_type_module.SaveProjectTypeSubmitHandler();
	},
	SaveProjectTypeSubmitHandler : function(){
		 var ProjectTForm = $('#FORM_SAVE_PROJECT_TYPE');
         var error3 = $('.alert-danger', ProjectTForm);
         var success3 = $('.alert-success', ProjectTForm);

         ProjectTForm.validate({
             errorElement: 'span', //default input error message container
             errorClass: 'help-block help-block-error', // default input error message class
             focusInvalid: false, // do not focus the last invalid input
             ignore: "", // validate all fields including form hidden input
             rules: {
            	 pt_project_type: {
            		 minlength: 6,
            		 required: true
            	 },
            	 pt_project_description : {
                     minlength: 6,
                     required: true
                   }
             },

             messages: { // custom messages for radio buttons and checkboxes

             },
             errorPlacement: function (error, element) { // render error placement for each input type
                 if (element.parent(".input-group").size() > 0) {
                     error.insertAfter(element.parent(".input-group"));
                 } else if (element.attr("data-error-container")) {
                     error.appendTo(element.attr("data-error-container"));
                 } else if (element.parents('.radio-list').size() > 0) {
                     error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                 } else if (element.parents('.radio-inline').size() > 0) {
                     error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                 } else if (element.parents('.checkbox-list').size() > 0) {
                     error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                 } else if (element.parents('.checkbox-inline').size() > 0) {
                     error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                 } else {
                     error.insertAfter(element); // for other inputs, just perform default behavior
                 }
             },
             invalidHandler: function (event, validator) { //display error alert on form submit
                 success3.hide();
                 error3.show();
             },
             success: function (label) {
                 label
                     .closest('.form-group').removeClass('has-error'); // set success class to the control group
             },
             highlight: function (element) { // hightlight error inputs
                 $(element)
                     .closest('.form-group').addClass('has-error'); // set error class to the control group
             },

             unhighlight: function (element) { // revert the change done by hightlight
                 $(element)
                     .closest('.form-group').removeClass('has-error'); // set error class to the control group
             },
             submitHandler: function (form) {
                success3.show();
                error3.hide();
                var base_url = $('#BASE_URL').val();
    	        var _token = $('input[name=_token]').val();
    	        var str_params = $("#FORM_SAVE_PROJECT_TYPE").serialize();
    	         $.ajax
    	        ({
    	            url : base_url + "/request/saveprojecttype",
    	            data : str_params,
    	            dataType : "json",
    	            type : "POST",
    	            success : function(response){
    	              if(response.is_error == 0)
    	              {
    	                 window.location.href = base_url + "/administrator/projecttypes";
    	              }
    	            }
    	        });
             }

         });
	},
	DeleteProjectTypeData : function(){
		 var pt_id = $(this).parents('tr').data('pt_id');
		bootbox.confirm("Are you sure you want to delete ?", function(result){
			//result
			if(result == true)
			{
			      var base_url = $('#BASE_URL').val();
			      var _token = $('input[name=_token]').val();
			        var str_params ={pt_id : pt_id , _token : _token};
			         $.ajax
			        ({
			            url : base_url + "/request/DeleteProjectType",
			            data : str_params,
			            dataType : "Json",
			            type : "POST",
			            success : function(response){
			              if(response.is_error == 0)
			              {
			            	  project_type_module.displayListProjectTypes();
			              }
			            }
			        });
			}
		});
	},
	DisplayEditProjectTypeForm : function(){
		var pt_id = $(this).parents('tr').data('pt_id');
	    var base_url = $("#BASE_URL").val();
	    window.location.href = base_url + "/administrator/editProjectTypeForm/" + pt_id;
	},
	CancelForm : function(){
		 window.history.back();
	}
};
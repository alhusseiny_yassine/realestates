/**
 *
 */

users_module = {
	displayListUsers : function(){
		var base_url = $('input[name=base_url]').val();
	    var _token = $('input[name=_token]').val();
	    var fb_user_type = $("#FB_USER_TYPE").val();
	    var u_beneficiary = $("#U_BENEFICIARY").val();
	    var u_search_username = $("input[name=u_search_username]").val();
	    var u_search_fullname = $("input[name=u_search_fullname]").val();
	    var u_search_email = $("input[name=u_search_email]").val();
	    var page_number = $('input[name=page_number]').val();
	    $.ajax
	    ({
	        url : base_url + "/request/displayListUsers",
	        data : { _token : _token , fb_user_type : fb_user_type , page_number : page_number , u_beneficiary : u_beneficiary , u_search_username : u_search_username , u_search_fullname : u_search_fullname , u_search_email : u_search_email },
	        dataType : "json",
	        type : "POST",
	        success : function(response){
	            $('.ListUserGirds').html(response.display);
                         $('.group-checkable').change(function() {
                            var set = $('table').find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
                            var checked = $(this).prop("checked");
                            $(set).each(function() {
                                $(this).prop("checked", checked);
                            });
                            $.uniform.update(set);
                        });

                         $('#UsersPagination').twbsPagination({
                             totalPages: response.total_pages,
                             visiblePages: 7,
                             onPageClick: function (event, page) {
                                  $('input[name=page_number]').val(page);
                                  users_module.displayListUsers();
                             }
                         });
	        }
	    });
	},
	DisplayBeneficiaryDepartments : function(){
		var ini_benf_department = $("input[name=ini_benf_department]").val();
		var beneficiary_id 	= $('select[name=fk_beneficiary_id]').val();
		var base_url 			= $('input[name=base_url]').val();
	    var _token	 			= $('input[name=_token]').val();
	    var params = { _token : _token , beneficiary_id : beneficiary_id , ini_benf_department : ini_benf_department };
	    $.ajax
        ({
            url : base_url + "/request/getBeneficiaryDiractorateDropdown",
            data : params,
            dataType : "json",
            type : "POST",
            success : function(response){
            	$('#BeneficiaryDepartment').html(response.display);
            	users_module.DisplayCoordinatorDropdown();
            }
        });
	},
	DisplayCoordinatorDropdown : function(){
		var base_url = $('#BASE_URL').val();
		 var _token = $('input[name=_token]').val();
		 var beneficiary_id 	= $('select[name=fk_beneficiary_id]').val();
		 var ini_coordinator_id 	= $('input[name=ini_coordinator_id]').val();

	        var params = {_token : _token , beneficiary_id : beneficiary_id , ini_coordinator_id : ini_coordinator_id };
	         $.ajax
	        ({
	            url : base_url + "/request/GetCoordinatorsDropdown",
	            data : params,
	            dataType : "json",
	            type : "POST",
	            success : function(response){
	            	$('.CoordinatorDropdown').html(response.display);
	            }
	        });
	},
	GetDistrictsProvince : function(){
		 var base_url = $('#BASE_URL').val();
		 var _token = $('input[name=_token]').val();
	     var fk_province_id = $('select[name=fk_province_id]').val();
	        var params = {_token : _token , province_id : fk_province_id };
	         $.ajax
	        ({
	            url : base_url + "/request/GetProvincesDistricts",
	            data : str_params,
	            dataType : "json",
	            type : "POST",
	            success : function(response){
	            	$('.ProvincesDistrict').html(response.display);
	            }
	        });
	},
	DisplayUserTypeForm : function(){
		var base_url = $('#BASE_URL').val();
		var _token = $('input[name=_token]').val();
		var fb_user_type = $('#FB_USER_TYPE').val();
	    var user_id = $('#USER_ID').val();
        var params = {_token : _token , fb_user_type : fb_user_type , user_id : user_id };
         $.ajax
        ({
            url : base_url + "/request/DisplayUserExtraFields",
            data : params,
            dataType : "json",
            type : "POST",
            success : function(response){
            	$('.ExtraFieldSection').html(response.display);
            	var user_type = $("#FB_USER_TYPE").val();
            	if(user_type == 4)
        		{
            		$("#BTN_TRAINEE_HISTORY").css({display : ""});
            		$("#BTN_ADD_NEW_TRAINEE_REQUEST").css({display : ""});
            		$('.timepicker-default').timepicker({
                        autoclose: true,
                        showSeconds: true,
                        minuteStep: 1
                    });
        		}
            	else
        		{
            		$("#BTN_TRAINEE_HISTORY").css({display : "none"});
            		$("#BTN_ADD_NEW_TRAINEE_REQUEST").css({display : "none"});
        		}
            	  $("input[name=u_employment_date]").datepicker({
            			endDate : '+1d',
            			keepOpen : false,
            			format : "yyyy-mm-dd"
            		});
            	  users_module.DisplayBeneficiaryDepartments();
            }
        });
	},
	OpenTraineeHistoryPage : function(){
		var user_id = $('#USER_ID').val();
		var base_url = $("#BASE_URL").val();
		var url = base_url + "/administrator/traineesHistory/" + user_id;
		var win = window.open(url, '_blank');
		win.focus();
	},
	OpenNewTraineeRequest : function(){
		var user_id = $('#USER_ID').val();
		var base_url = $("#BASE_URL").val();
		var new_request_url = base_url + "/administrator/AddNewRequestForm?trainee_id=" + user_id;
		var win_new = window.open(new_request_url, '_blank');
		win_new.focus();
	},
	GenerateCoordinatorCode : function(){
		var base_url = $('#BASE_URL').val();
		var _token = $('input[name=_token]').val();
        var params = {_token : _token };
         $.ajax
        ({
            url : base_url + "/request/GenerateCoordinatorCode",
            data : params,
            dataType : "json",
            type : "POST",
            success : function(response){
            	$('#U_COORDINATOR_CODE').val(response.code);
            }
        });
	},
	UploadProfilePic : function(event){
		event.stopPropagation(); // Stop stuff happening
        event.preventDefault(); // Totally stop stuff happening
        var base_url 	= $("#BASE_URL").val();




        // Create a formdata object and add the files
        var FormDataFields = $(this);

        var data = new FormData();
        var index = 0;

        $.each($("input[type=file]"), function(i, obj) {
                var name = $(this).attr('name');
                $.each(obj.files,function(j,file){
                        data.append(name, file);
                })
        });

        FormDataFields.find('input,select').each(function(){
                data.append($(this).attr('name'), $(this).val() );
        });

        $.ajax
        ({
            url: base_url + '/uploads/profile',
            data: data,
            async: false,
            cache: false,
            method : 'post',
            contentType: false,
            processData: false,
            dataType : "json",
            beforeSend : function(){
            },
            success: function (response) {
            	$('#PROFILE_PIC').attr('src',response.image_url);
            	$('#MAIN_PROFILE_PIC').attr('src',response.image_url);
            	$('#SECTION_PROFILE_PIC').attr('src',response.image_url);
            }
    });
	},
	SaveUserEducationInfo : function(){
		return users_module.SaveUserEducationInfoHandler();
	},
	SaveUserEducationInfoHandler : function(){
		var EducationInfoForm = $('#FORM_SAVE_UE_INFO');
		var error3 = $('.alert-danger', EducationInfoForm);
		var success3 = $('.alert-success', EducationInfoForm);

		EducationInfoForm.validate({
			errorElement: 'span', //default input error message container
			errorClass: 'help-block help-block-error', // default input error message class
			focusInvalid: false, // do not focus the last invalid input
			ignore: "", // validate all fields including form hidden input
			rules: {
				ue_education_name: {
					required: true
				},
				fk_degree_id: {
					required: true
				},
				ue_education_details: {
					required: true
				}
			},

			messages: { // custom messages for radio buttons and checkboxes

			},
			errorPlacement: function (error, element) { // render error placement for each input type
				if (element.parent(".input-group").size() > 0) {
					error.insertAfter(element.parent(".input-group"));
				} else if (element.attr("data-error-container")) {
					error.appendTo(element.attr("data-error-container"));
				} else if (element.parents('.radio-list').size() > 0) {
					error.appendTo(element.parents('.radio-list').attr("data-error-container"));
				} else if (element.parents('.radio-inline').size() > 0) {
					error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
				} else if (element.parents('.checkbox-list').size() > 0) {
					error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
				} else if (element.parents('.checkbox-inline').size() > 0) {
					error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},
			invalidHandler: function (event, validator) { //display error alert on form submit
				success3.hide();
				error3.show();
			},
			success: function (label) {
				label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			},
			highlight: function (element) { // hightlight error inputs
				$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
			},

			unhighlight: function (element) { // revert the change done by hightlight
				$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
			},
			submitHandler: function (form) {
				success3.show();
				error3.hide();
				var base_url = $('#BASE_URL').val();
				var user_id = $('input[name=user_id]').val();
				var _token = $('input[name=_token]').val();
				var str_params = $("#FORM_SAVE_UE_INFO").serialize();
				$.ajax
				({
					url : base_url + "/request/SaveEducationInfo",
					data : str_params,
					dataType : "json",
					type : "POST",
					success : function(response){
						bootbox.alert(response.error_msg);
						window.location.href = base_url + "/omsar/myprofile";
					}
				});
			}

		});
	},
	SaveAUserEducationInfo : function(){
		return users_module.SaveAUserEducationInfoHandler();
	},
	SaveAUserEducationInfoHandler : function(){
		var EducationInfoForm = $('#FORM_SAVE_UE_INFO');
        var error3 = $('.alert-danger', EducationInfoForm);
        var success3 = $('.alert-success', EducationInfoForm);

        EducationInfoForm.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
            	ue_education_name: {
            		required: true
            	},
            	fk_degree_id: {
            		required: true
            	},
            	ue_education_details: {
            		required: true
            	}
            },

            messages: { // custom messages for radio buttons and checkboxes

            },
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
            },
            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            submitHandler: function (form) {
               success3.show();
               error3.hide();
               var base_url = $('#BASE_URL').val();

   	        var _token = $('input[name=_token]').val();
   	        var str_params = $("#FORM_SAVE_UE_INFO").serialize();
   	         $.ajax
   	        ({
   	            url : base_url + "/request/SaveEducationInfo",
   	            data : str_params,
   	            dataType : "json",
   	            type : "POST",
   	            success : function(response){

   	            	bootbox.alert(response.error_msg);
   	            	var user_id = $('input[name=user_id]').val();
   	            	window.location.href = base_url + "/administrator/editUser/" + user_id;
   	            }
   	        });
            }

        });
	},
	SaveProviderLocationInfo : function(){
		return users_module.SaveProviderLocationInfoHandler();
	},
	SaveProviderLocationInfoHandler : function(){
		var ProviderLocationForm = $('#FORM_SAVE_PL_INFO');
		var error3 = $('.alert-danger', ProviderLocationForm);
		var success3 = $('.alert-success', ProviderLocationForm);

		ProviderLocationForm.validate({
			errorElement: 'span', //default input error message container
			errorClass: 'help-block help-block-error', // default input error message class
			focusInvalid: false, // do not focus the last invalid input
			ignore: "", // validate all fields including form hidden input
			rules: {
				pl_provider_location: {
					required: true
				}
			},

			messages: { // custom messages for radio buttons and checkboxes

			},
			errorPlacement: function (error, element) { // render error placement for each input type
				if (element.parent(".input-group").size() > 0) {
					error.insertAfter(element.parent(".input-group"));
				} else if (element.attr("data-error-container")) {
					error.appendTo(element.attr("data-error-container"));
				} else if (element.parents('.radio-list').size() > 0) {
					error.appendTo(element.parents('.radio-list').attr("data-error-container"));
				} else if (element.parents('.radio-inline').size() > 0) {
					error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
				} else if (element.parents('.checkbox-list').size() > 0) {
					error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
				} else if (element.parents('.checkbox-inline').size() > 0) {
					error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},
			invalidHandler: function (event, validator) { //display error alert on form submit
				success3.hide();
				error3.show();
			},
			success: function (label) {
				label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			},
			highlight: function (element) { // hightlight error inputs
				$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
			},

			unhighlight: function (element) { // revert the change done by hightlight
				$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
			},
			submitHandler: function (form) {
				success3.show();
				error3.hide();
				var base_url = $('#BASE_URL').val();
				var user_id = $('input[name=user_id]').val();
				var _token = $('input[name=_token]').val();
				var str_params = $("#FORM_SAVE_PL_INFO").serialize();
				$.ajax
				({
					url : base_url + "/request/SaveProviderLocationInfo",
					data : str_params,
					dataType : "json",
					type : "POST",
					success : function(response){
						bootbox.alert(response.error_msg,function(){
							var base_url = $('#BASE_URL').val();
							window.location.href = base_url + "/omsar/myprofile";
						});

					}
				});
			}

		});
	},
	SaveAProviderLocationInfo : function(){
		return users_module.SaveAProviderLocationInfoHandler();
	},
	SaveAProviderLocationInfoHandler : function(){
		var ProviderLocationForm = $('#FORM_SAVE_PL_INFO');
        var error3 = $('.alert-danger', ProviderLocationForm);
        var success3 = $('.alert-success', ProviderLocationForm);

        ProviderLocationForm.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
            	pl_provider_location: {
            		required: true
            	}
            },

            messages: { // custom messages for radio buttons and checkboxes

            },
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
            },
            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            submitHandler: function (form) {
               success3.show();
               error3.hide();
               var base_url = $('#BASE_URL').val();

   	        var _token = $('input[name=_token]').val();
   	        var str_params = $("#FORM_SAVE_PL_INFO").serialize();
   	         $.ajax
   	        ({
   	            url : base_url + "/request/SaveProviderLocationInfo",
   	            data : str_params,
   	            dataType : "json",
   	            type : "POST",
   	            success : function(response){
   	            	bootbox.alert(response.error_msg,function(){
   	            		var base_url = $('#BASE_URL').val();
   	   	            	var user_id = $('input[name=user_id]').val();
   	   	            	window.location.href = base_url + "/administrator/editUser/" + user_id;
   	            	});

   	            }
   	        });
            }

        });
	},
	SubmitChangePassword : function(){
		return users_module.SubmitChangePasswordSubmitHandler();
	},
	SubmitChangePasswordSubmitHandler : function(){
		var ProfileForm = $('#FRM_CHANGE_PASSWORD');
        var error3 = $('.alert-danger', ProfileForm);
        var success3 = $('.alert-success', ProfileForm);

        ProfileForm.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
            	u_current_password: {
                    required: true
                  },
                  u_new_password: {
                	  minlength: 8,
                	  required: true
                  },
                  u_re_new_password: {
                    minlength: 8,
                    equalTo: "#U_NEW_PASSWORD",
                    required: true
                  },
            },

            messages: { // custom messages for radio buttons and checkboxes

            },
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
            },
            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            submitHandler: function (form) {
               success3.show();
               error3.hide();
               var base_url = $('#BASE_URL').val();
   	        var _token = $('input[name=_token]').val();
   	        var str_params = $("#FORM_SAVE_UE_INFO").serialize();
   	         $.ajax
   	        ({
   	            url : base_url + "/request/SaveEducationInfo",
   	            data : str_params,
   	            dataType : "json",
   	            type : "POST",
   	            success : function(response){
   	            	var user_id = $('input[name=user_id]').val();
   	            	window.location.href = base_url + '/administrator/editUser/' + user_id;
   	            }
   	        });
            }

        });
	},
	SaveProfileInfo : function(){
		return users_module.SaveProfileInfoSubmitHandler();
	},
	SaveProfileInfoSubmitHandler : function(){
		var ProfileForm = $('#FRM_PERSONAL_INFO');
        var error3 = $('.alert-danger', ProfileForm);
        var success3 = $('.alert-success', ProfileForm);

        ProfileForm.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
           	 username: {
           		 minlength: 4,
           		 required: true
           	 }
            },

            messages: { // custom messages for radio buttons and checkboxes

            },
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
            },
            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            submitHandler: function (form) {
               success3.show();
               error3.hide();
               var base_url = $('#BASE_URL').val();
   	        var _token = $('input[name=_token]').val();
   	        var str_params = $("#FRM_PERSONAL_INFO").serialize();
   	         $.ajax
   	        ({
   	            url : base_url + "/request/saveprofileinfo",
   	            data : str_params,
   	            dataType : "json",
   	            type : "POST",
   	            success : function(response){
   	            	bootbox.alert(response.error_msg);
   	            }
   	        });
            }

        });
	},
	AddUserInfo : function(){
		return users_module.AddUserSubmitHandler();
	},
	AddUserSubmitHandler : function(){
		 var ProfileForm = $('#FORM_SAVE_USER');
         var error3 = $('.alert-danger', ProfileForm);
         var success3 = $('.alert-success', ProfileForm);
         error3.html('<strong>Error!</strong> You have some form errors. Please check below.');

         ProfileForm.validate({
             errorElement: 'span', //default input error message container
             errorClass: 'help-block help-block-error', // default input error message class
             focusInvalid: false, // do not focus the last invalid input
             ignore: "", // validate all fields including form hidden input
             rules: {
            	 username: {
            		 minlength: 4,
            		 required: true
            	 },
            	 u_password: {
            		 minlength: 8,
            		 required: true
            	 },
            	 retype_u_password: {
                     minlength: 8,
                     equalTo: "#U_PASSWORD",
                     required: true
                   },
            	 fb_user_type: {
            		 required: true
            	 },
            	 u_date_birth: {
                     required: true
                 },
                 u_gender : {
                	 required: true
                 },
            	 u_role: {
            		 required: true
            	 },
            	 fk_coordinator_id: {
            		 required: true
            	 },
            	 fk_beneficiary_id : {
            		 required: true
            	 },
            	 fullname: {
            		 minlength: 2,
            		 required: true
            	 },
            	 u_preferred_train_days: {
            		 required: true
            	 },
            	 u_emp_degree: {
                     required: true,
                     number : true
                 },
                 u_email: {
                 	email : true,
                 	 minlength: 5,
                     required: true
                 },
                 website: {
                	 url : true
                 }
             },

             messages: { // custom messages for radio buttons and checkboxes

             },
             errorPlacement: function (error, element) { // render error placement for each input type
                 if (element.parent(".input-group").size() > 0) {
                     error.insertAfter(element.parent(".input-group"));
                 } else if (element.attr("data-error-container")) {
                     error.appendTo(element.attr("data-error-container"));
                 } else if (element.parents('.radio-list').size() > 0) {

                     error.appendTo("#GenderError");
                 } else if (element.parents('.radio-inline').size() > 0) {
                     error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                 } else if (element.parents('.checkbox-list').size() > 0) {
                     error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                 } else if (element.parents('.checkbox-inline').size() > 0) {
                     error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                 } else {
                     error.insertAfter(element); // for other inputs, just perform default behavior
                 }
             },
             invalidHandler: function (event, validator) { //display error alert on form submit
                 success3.hide();
                 error3.show();
             },
             success: function (label) {
                 label
                     .closest('.form-group').removeClass('has-error'); // set success class to the control group
             },
             highlight: function (element) { // hightlight error inputs
                 $(element)
                     .closest('.form-group').addClass('has-error'); // set error class to the control group
             },

             unhighlight: function (element) { // revert the change done by hightlight
                 $(element)
                     .closest('.form-group').removeClass('has-error'); // set error class to the control group
             },
             submitHandler: function (form) {
                success3.show();
                error3.hide();
                var base_url = $('#BASE_URL').val();
    	        var _token = $('input[name=_token]').val();
                
                // Create a formdata object and add the files
                var FormDataFields = $("#FORM_SAVE_USER");
                var data = new FormData();
                var index = 0;
                
                $.each($("input[type=file]"), function (i, obj) {
                    var name = $(this).attr('name');
                    $.each(obj.files, function (j, file) {
                        data.append(name, file);
                    });
                });
                
                FormDataFields.find('input,select,textarea').each(function () {
                    data.append($(this).attr('name'), $(this).val());
                });
                
                $.ajax
                        ({
                            url: base_url + '/request/AddNewUser',
                            data: data,
                            async: false,
                            cache: false,
                            method: 'post',
                            contentType: false,
                            processData: false,
                            dataType: "json",
                            beforeSend: function () {
                            },
                            success: function (response) {
                                if (response.is_error == 0) {
                                    if (response.u_user_type == "4") {
                                        window.location.href = base_url + "/administrator/editUser/" + response.user_id;
                                    } else {
                                        window.location.href = base_url + "/administrator/UsersManagement";
                                    }
                                } else {
                                    error3.html(response.error_msg);
                                    success3.hide();
                                    error3.show();
                                }
                            }
                        });
             }

         });
	},
	EditUserInfo : function(){
		return users_module.EditUserSubmitHandler();
	},
	EditUserSubmitHandler : function(){
		 var ProfileForm = $('#FORM_SAVE_USER');
         var error3 = $('.alert-danger', ProfileForm);
         var success3 = $('.alert-success', ProfileForm);

         ProfileForm.validate({
             errorElement: 'span', //default input error message container
             errorClass: 'help-block help-block-error', // default input error message class
             focusInvalid: false, // do not focus the last invalid input
             ignore: "", // validate all fields including form hidden input
             rules: {
            	 fb_user_type: {
            		 required: true
            	 },
            	 u_role: {
            		 required: true
            	 },
            	 u_date_birth: {
            		 required: true
            	 },
            	 fk_beneficiary_id : {
            		 required: true
            	 },
            	 fullname: {
            		 minlength: 2,
            		 required: true
            	 },
            	 u_date_birth: {
                     required: true
                 },
                 u_gender : {
                	 required: true
                 },
                 u_email: {
                 	email : true,
                 	 minlength: 5,
                     required: true
                 },
                 website: {
                	 url : true
                 },
                 u_emp_degree: {
                     number : true
                 },
                 u_password: {
            		 minlength: 8
            	 },
            	 retype_u_password: {
                     minlength: 8,
                     equalTo: "#U_PASSWORD"
                   }
             },

             messages: { // custom messages for radio buttons and checkboxes

             },
             errorPlacement: function (error, element) { // render error placement for each input type
                 if (element.parent(".input-group").size() > 0) {
                     error.insertAfter(element.parent(".input-group"));
                 } else if (element.attr("data-error-container")) {
                     error.appendTo(element.attr("data-error-container"));
                 } else if (element.parents('.radio-list').size() > 0) {
                     error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                 } else if (element.parents('.radio-inline').size() > 0) {
                     error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                 } else if (element.parents('.checkbox-list').size() > 0) {
                     error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                 } else if (element.parents('.checkbox-inline').size() > 0) {
                     error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                 } else {
                     error.insertAfter(element); // for other inputs, just perform default behavior
                 }
             },
             invalidHandler: function (event, validator) { //display error alert on form submit
                 success3.hide();
                 error3.show();
             },
             success: function (label) {
                 label
                     .closest('.form-group').removeClass('has-error'); // set success class to the control group
             },
             highlight: function (element) { // hightlight error inputs
                 $(element)
                     .closest('.form-group').addClass('has-error'); // set error class to the control group
             },

             unhighlight: function (element) { // revert the change done by hightlight
                 $(element)
                     .closest('.form-group').removeClass('has-error'); // set error class to the control group
             },
             submitHandler: function (form) {
                success3.show();
                error3.hide();
                var base_url = $('#BASE_URL').val();
    	        var _token = $('input[name=_token]').val();
                
                // Create a formdata object and add the files
                var FormDataFields = $("#FORM_SAVE_USER");
                var data = new FormData();
                var index = 0;
                
                $.each($("input[type=file]"), function (i, obj) {
                    var name = $(this).attr('name');
                    $.each(obj.files, function (j, file) {
                        data.append(name, file);
                    });
                });
                
                FormDataFields.find('input,select,textarea').each(function () {
                    data.append($(this).attr('name'), $(this).val());
                });
                
                $.ajax
                        ({
                            url: base_url + '/request/SaveUserInfo',
                            data: data,
                            async: false,
                            cache: false,
                            method: 'post',
                            contentType: false,
                            processData: false,
                            dataType: "json",
                            beforeSend: function () {
                            },
                            success: function (response) {
                                if (response.is_error == 0) {
                                    window.location.href = base_url + "/administrator/UsersManagement";
                                } else {
                                    error3.html(response.error_msg);
                                    success3.hide();
                                    error3.show();
                                }
                            }
                        });
             }

         });
	},
	CheckIfUserExit : function(){
		var base_url = $('#BASE_URL').val();
		var _token = $('input[name=_token]').val();
		var user_name = $('#U_EMAIL').val();
		if(user_name.length == 0)
			return false;
        var params = {_token : _token , user_name : user_name };
         $.ajax
        ({
            url : base_url + "/request/checkUserName",
            data : params,
            dataType : "json",
            type : "POST",
            success : function(response){
            	$('#U_EMAIL').parents('.form-group').removeClass('has-error');
            	$('#U_EMAIL').parents('.form-group').find('.help-block').remove();
            	$('#U_EMAIL').removeClass('SuccessCheck')
            	var field_container = $('#U_EMAIL').parents('.form-group');
            	if(response.exist == 1)
                {
            		$('#U_EMAIL').parents('.form-group').addClass('has-error');
            		$('<span />',{
            			'class' : 'help-block help-block-error'
            		}).appendTo(field_container);
            		$('#U_EMAIL').parents('.form-group').find('.help-block').html('Email Already Exist');
            		$("#BTN_EDIT_USER").attr('disabled','disabled');
                }
            	else
        		{
            		$('#U_EMAIL').addClass('SuccessCheck');
            		$("#BTN_EDIT_USER").removeAttr('disabled');
        		}
            }
        });
	},
	DeleteUserData : function(){
		if(!confirm('Are you sure do you want to delete ?'))
	           return false;
	      var user_id = $(this).parents('tr').data('user_id');
	      var base_url = $('#BASE_URL').val();
	      var _token = $('input[name=_token]').val();
	        var str_params ={user_id : user_id , _token : _token};
	         $.ajax
	        ({
	            url : base_url + "/request/DeleteUser",
	            data : str_params,
	            dataType : "Json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  users_module.displayListUsers();
	              }
	            }
	        });
	},
	DisplayEditUser : function(){
		var user_id = $(this).parents('tr').data('user_id');
	    var base_url = $("#BASE_URL").val();
	    window.location.href = base_url + "/administrator/editUser/" + user_id;
	},
	CancelForm : function(){
		 window.history.back();
	},
	AddMultipleUsersLayout : function(){
		var base_url = $("#BASE_URL").val();
		window.location.href = base_url + "/administrator/AddMultipleUsers";
	},
	AddUserRow : function(){ // get row for user fields to add it into data grid "adding multiple users"
		var base_url = $('#BASE_URL').val();
		var _token = $('input[name=_token]').val();
        var params = {_token : _token };
         $.ajax
        ({
            url : base_url + "/request/getUserRow",
            data : params,
            dataType : "json",
            type : "POST",
            success : function(response){
            	var dataTable = $("#USERTABLE").find('tbody');
            	$(response.display).appendTo(dataTable)
            	//$("#USERTABLE").find('tbody').html(dataTable + response.display);
            }
        });
	},
	DeleteUserRow : function(){
		$(this).parents('tr').fadeOut('fast',function(){
			$(this).remove();
		});
	},
	DisplayUserEducationInfoList :function(){
		var base_url = $('#BASE_URL').val();
		var _token = $('input[name=_token]').val();
		var user_id = $('input[name=user_id]').val();
        var params = { _token : _token , user_id : user_id };

		$.ajax
	       ({
	           url : base_url + "/request/DisplayListUserEduccation",
	           data : params,
	           dataType : "json",
	           type : "POST",
	           success : function(response){
	             if(response.is_error == 0)
	             {
	                $('.EducationInfo').html(response.display);
	             }
	           }
	       });
	},
	OpenEducationInfoPage : function(){
		var base_url = $('#BASE_URL').val();
		var user_id = $('#USER_ID').val();
		window.location.href = base_url + "/User/AddEducationInfo/" + user_id;
	},
	OpenEditEducationInfoPage : function(){
		var base_url = $('#BASE_URL').val();
		var ue_id = $(this).parents('tr').data('ue_id');
		window.location.href = base_url + "/User/EditEducationInfo/" + ue_id;
	},
	DeleteEducationInfo : function(){
		var ue_id = $(this).parents('tr').data('ue_id');
		bootbox.confirm("Are you sure you want to delete ?", function(result){
			var user_id = $('input[name=user_id]').val();
			var _token = $('input[name=_token]').val();

	        var params = { _token : _token , ue_id : ue_id };
			var base_url = $('#BASE_URL').val();
			$.ajax
			({
				url : base_url + "/request/DeleteUserEducation",
				data : params,
				dataType : "json",
				type : "POST",
				success : function(response){
					if(response.is_error == 0)
					{
						users_module.DisplayUserEducationInfoList();
					}
				}
			});
		});
	},
	OpenAEducationInfoPage : function(){
		var base_url = $('#BASE_URL').val();
		var user_id = $('#USER_ID').val();
		window.location.href = base_url + "/administrator/AddEducationInfo/" + user_id;
	},
	OpenAEditEducationInfoPage : function(){
		var base_url = $('#BASE_URL').val();
		var ue_id = $(this).parents('tr').data('ue_id');
		window.location.href = base_url + "/administrator/EditEducationInfo/" + ue_id;
	},
	DeleteEducationInfo : function(){
		var ue_id = $(this).parents('tr').data('ue_id');
		bootbox.confirm("Are you sure you want to delete ?", function(result){
			var user_id = $('input[name=user_id]').val();
			var _token = $('input[name=_token]').val();

	        var params = { _token : _token , ue_id : ue_id };
			var base_url = $('#BASE_URL').val();
			$.ajax
			({
				url : base_url + "/request/DeleteUserEducation",
				data : params,
				dataType : "json",
				type : "POST",
				success : function(response){
					if(response.is_error == 0)
					{
						users_module.DisplayUserEducationInfoList();
					}
				}
			});
		});
	},
	DisplayUserProviderLocationsList :function(){
		var base_url = $('#BASE_URL').val();
		var _token = $('input[name=_token]').val();
		var user_id = $('input[name=user_id]').val();
        var params = { _token : _token , user_id : user_id };

		$.ajax
	       ({
	           url : base_url + "/request/DisplayListProviderLocations",
	           data : params,
	           dataType : "json",
	           type : "POST",
	           success : function(response){
	        	   $('.ProviderLocation').html(response.display);
	           }
	       });
	},
	OpenProviderLocationPage : function(){
		var base_url = $('#BASE_URL').val();
		var user_id = $('#USER_ID').val();
		window.location.href = base_url + "/User/AddProviderLocations/" + user_id;
	},
	OpenEditProviderLocationPage : function(){
		var base_url = $('#BASE_URL').val();
		var pl_id = $(this).parents('tr').data('pl_id');
		window.location.href = base_url + "/User/EditProviderLocations/" + pl_id;
	},
	OpenAProviderLocationPage : function(){
		var base_url = $('#BASE_URL').val();
		var user_id = $('#USER_ID').val();
		window.location.href = base_url + "/Provider/AddProviderLocations/" + user_id;
	},
	OpenAEditProviderLocationPage : function(){
		var base_url = $('#BASE_URL').val();
		var pl_id = $(this).parents('tr').data('pl_id');
		window.location.href = base_url + "/Provider/EditProviderLocations/" + pl_id;
	},
	DeleteProviderLocation : function(){
		var pl_id = $(this).parents('tr').data('pl_id');
		bootbox.confirm("Are you sure you want to delete ?", function(result){
			var user_id = $('input[name=user_id]').val();
			var _token = $('input[name=_token]').val();

	        var params = { _token : _token , pl_id : pl_id };
			var base_url = $('#BASE_URL').val();
			$.ajax
			({
				url : base_url + "/request/DeleteProviderLocation",
				data : params,
				dataType : "json",
				type : "POST",
				success : function(response){
					if(response.is_error == 0)
					{
						users_module.DisplayUserProviderLocationsList();
					}
				}
			});
		});
	},
	SaveMultipleUsers : function(){
		var str_params = $("#Form_ADD_MULTIPLE_USERS").serialize();
		var base_url = $('#BASE_URL').val();
        $.ajax
       ({
           url : base_url + "/request/SaveMultipleUsers",
           data : str_params,
           dataType : "json",
           type : "POST",
           success : function(response){
             if(response.is_error == 0)
             {
                window.location.href = base_url + "/administrator/UsersManagement";
             }
           }
       });
	},
	MultipleDeleteUsersLayout : function(){
		bootbox.confirm("Are you sure you want to delete ?", function(result){
			//result
			if(result == true)
			{
				var users_ids = "";
				$('input[name*=ck_user_]:checked').each(function(n){

					if(n ==0)
					{
						users_ids = $(this).val();
					}
					else
					{
						users_ids = users_ids + "," + $(this).val();
					}
				});
				var base_url = $('#BASE_URL').val();
				var _token = $('input[name=_token]').val();
		        var params = {_token : _token , users_ids : users_ids };
		        $.ajax
		       ({
		           url : base_url + "/request/DeleteMultipleUsers",
		           data : params,
		           dataType : "json",
		           type : "POST",
		           success : function(response){
		             if(response.is_error == 0)
		             {
		                users_module.displayListUsers();
		             }
		           }
		       });
			}


		});

	},
	EditMultipleUsersLayout : function(){

	},
	GenerateEmailDefaultEmailAddress : function(){
		var full_name = $(this).val();
		var FullnameArray = full_name.split(" ");
		var email = "";
		for ( var int = 0; int < FullnameArray.length; int++) {
				if(int == 0)
			    {
					email = FullnameArray[int];
			    }
				else
				{
					email = email + "-" + FullnameArray[int];
				}

		}

		$("#U_EMAIL").val(email + "@omsar.gov.lb");

	},
	DisplayPrintList  : function(){
		var base_url = $("#BASE_URL").val();
		var url  = base_url + "/administrator/print/users";
		window.open(url,'_blank');
	},
        ExportToExcell: function(){
            var base_url = $("#BASE_URL").val();
            var url = base_url + "/administrator/exportexcel/users";
            window.open(url,'_blank');
        }
};
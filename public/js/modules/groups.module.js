var $modelPopup = $('#ModelPopUp');
var groups_module = {
    DisplayGroups: function () {
        var base_url     = $('input[name=base_url]').val();
        var _token       = $('input[name=_token]').val();
        var project_id = $("#select_project").val();
        var course_id  = $("#select_course").val();
        var page_number = $('input[name=group_page_number]').val();
        $.ajax
                ({
                    url: base_url + "/request/DisplayGroups",
                    data: { _token: _token, project_id: project_id, page_number: page_number, course_id: course_id },
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        $('.ListGroups').html(response.display);
                        

                        $('#GroupsPagination').twbsPagination({
                            totalPages: response.total_pages,
                            visiblePages: 7,
                            onPageClick: function (event, page) {
                                $('input[name=group_page_number]').val(page);
                                groups_module.DisplayGroups();
                            }
                        });
                    }
                });
    },
    DisplayProjectCourses: function () {
        var base_url   = $('input[name=base_url]').val();
        var _token     = $('input[name=_token]').val();
        var project_id = $("#select_project").val();
        
        $.ajax
                ({
                    url: base_url + "/request/DisplayProjectCourses",
                    data: { _token: _token, project_id: project_id },
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        $('#select_course').html(response.display);
                        $("#PROJECT_ID").val(project_id);
                    }
                });
    },
    DisplayAddGroup: function () {
       var project_id = $("#PROJECT_ID").val();
       if( project_id == '' ){
           return;
       }
       var base_url = $('input[name=base_url]').val();
       var url      = base_url + "/groups/AddNewGroup/"+project_id;
       
       $('body').modalmanager('loading');
        setTimeout(function(){
        	$modelPopup.load(url, '', function(){
        		$modelPopup.modal();
        		$("input[name=pg_start_date],input[name=pg_end_date]").datepicker({
        			keepOpen : false,
        			format : "yyyy-mm-dd"
        		});
                        $('input[name=pg_trainee_time]').timepicker({
                                showSeconds: true,
                                showMeridian: false,
                                });
                                $(".modal_close").on("click",function(){
                    $modelPopup.modal('toggle')
                });
        		$("#PG_CLASS_PROVIDER").on('change',projects_module.DisplayProviderLocationsDropDown);
        		$("#BTN_CREATE_CLASS").on('click',projects_module.CreateNewClassProject);

          });
        }, 1000);
    },
    DisplayEditGroup: function () {
        var pg_id    = $(this).data('pg_id');
        var base_url = $("#BASE_URL").val();
        var url      = base_url + "/groups/EditGroup/" + pg_id;

        $('body').modalmanager('loading');
        setTimeout(function () {
            $modelPopup.load(url, '', function () {
                $modelPopup.modal();
                $("input[name=pg_start_date],input[name=pg_end_date]").datepicker({
                    keepOpen: false,
                    format: "yyyy-mm-dd"
                });
                $('input[name=pg_trainee_time]').timepicker({
                                showSeconds: true,
                                showMeridian: false,
                                });
                projects_module.DisplayProviderLocationsDropDown();
                $(".modal_close").on("click",function(){
                    $modelPopup.modal('toggle')
                });
                $("#PG_CLASS_PROVIDER").on('change', projects_module.DisplayProviderLocationsDropDown);
                $("#BTN_SAVE_GROUP_INFO").on('click', projects_module.SaveClassProjectInfo);

            });
        }, 1000);
    },
    DeleteGroup: function () {
        var pg_id    = $(this).data('pg_id');
        var base_url = $("#BASE_URL").val();
        var _token   = $('input[name=_token]').val();
        bootbox.confirm('Are You Sure You Want to delete ?', function (result) {
            if (result == true)
            {
                var params = {pg_id: pg_id, _token: _token};
                $.ajax
                        ({
                            url: base_url + "/request/DeleteGroupInfo",
                            data: params,
                            dataType: "json",
                            type: "POST",
                            success: function (response) {
                                groups_module.DisplayGroups();
                            }
                        });
            }
        });

    },
    EditTraineesGroup : function(){
		  var pg_id = $(this).data('pg_id');
		  var base_url = $("#BASE_URL").val();
	        var url = base_url + "/project/EditTraineesGroup/" + pg_id;

	        $('body').modalmanager('loading');
	        setTimeout(function(){
	        	$modelPopup.load(url, '', function(){
	        		$modelPopup.modal();
	        		   $("#BTN_SAVE_TRAINEES_GROUP").on('click',groups_module.SaveGroupTrainees);

	          });
	        }, 1000);
	  },
	  SaveGroupTrainees : function(){
		  var frm_trainees = $("#FRM_TRAINEES_GROUP").serialize();
		  var base_url = $("#BASE_URL").val();
		  $.ajax
	        ({
	            url : base_url + "/request/SaveTraineesGroup",
	            data : frm_trainees,
	            dataType : "json",
	            type : "POST",
	            success : function(response){
	            	$('.modal-scrollable').trigger('click');
	            }
	        });
	  },
};
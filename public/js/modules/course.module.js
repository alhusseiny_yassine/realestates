/**
 *
 */

var $SendRequest = $('#SendRequest');
course_module = {
		displayListCourses : function(){
			var base_url = $('input[name=base_url]').val();
		    var _token = $('input[name=_token]').val()
		    var page_number = $('input[name=page_number]').val();
		    var c_course_code 			= $('input[name=c_course_code]').val();
		    var c_course_name 			= $('input[name=c_course_name]').val();
		    var c_course_description 	= $('input[name=c_course_description]').val();
		    var sl_course_type 	= $('select[name=sl_course_type]').val();
		    var fk_category_id 	= $('select[name=fk_category_id]').val();
		    $.ajax
		    ({
		        url : base_url + "/request/displaylistcourses",
		        data : { _token : _token , page_number : page_number , fk_category_id : fk_category_id , sl_course_type : sl_course_type ,  c_course_code : c_course_code , c_course_name : c_course_name , c_course_description : c_course_description },
		        dataType : "json",
		        type : "POST",
		        success : function(response){
		            $('.ListCourseGird').html(response.display);
                             $('.group-checkable').change(function() {
                                var set = $('table').find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
                                var checked = $(this).prop("checked");
                                $(set).each(function() {
                                    $(this).prop("checked", checked);
                                });
                                $.uniform.update(set);
                            });

                             $('#CoursePagination').twbsPagination({
                                 totalPages: response.total_pages,
                                 visiblePages: 7,
                                 onPageClick: function (event, page) {
                                      $('input[name=page_number]').val(page);
                                      course_module.displayListCourses();
                                 }
                             });
		        }
		    });
		},
		CoursesManagement : function(){
			var base_url = $('input[name=base_url]').val();
			var _token = $('input[name=_token]').val()
			var page_number = $('input[name=page_number]').val();
			var search_query = $('input[name=search_query]').val();
			var c_course_category = $('select[name=c_course_category]').val();
			$.ajax
			({
				url : base_url + "/omsar/coursesearchresults",
				data : { _token : _token , page_number : page_number , search_query : search_query , c_course_category : c_course_category },
				dataType : "json",
				type : "POST",
				success : function(response){
					$('.ListCourseGird').html(response.display);
					$('.group-checkable').change(function() {
						var set = $('table').find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
						var checked = $(this).prop("checked");
						$(set).each(function() {
							$(this).prop("checked", checked);
						});
						$.uniform.update(set);
					});

					$('#CoursesPagination').twbsPagination({
						totalPages: response.total_pages,
						visiblePages: 7,
						onPageClick: function (event, page) {
							$('input[name=page_number]').val(page);
							course_module.CoursesManagement();
						}
					});
				}
			});
		},
		RunningCoursesManagement : function(){
			var base_url = $('input[name=base_url]').val();
		    var _token = $('input[name=_token]').val()
		    var page_number = $('input[name=page_number]').val();
		    var search_query = $('input[name=search_query]').val();
		    var c_course_type = $('select[name=c_course_type]').val();
		    $.ajax
		    ({
		        url : base_url + "/courses/runningcoursesearchresults",
		        data : { _token : _token , page_number : page_number , search_query : search_query , c_course_type : c_course_type },
		        dataType : "json",
		        type : "POST",
		        success : function(response){
		            $('.ListCourseGird').html(response.display);
                             $('.group-checkable').change(function() {
                                var set = $('table').find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
                                var checked = $(this).prop("checked");
                                $(set).each(function() {
                                    $(this).prop("checked", checked);
                                });
                                $.uniform.update(set);
                            });

                             $('#CoursesPagination').twbsPagination({
                                 totalPages: response.total_pages,
                                 visiblePages: 7,
                                 onPageClick: function (event, page) {
                                      $('input[name=page_number]').val(page);
                                      course_module.RunningCoursesManagement();
                                 }
                             });
		        }
		    });
		},
		AddNewCourseForm : function(){
			var base_url = $("#BASE_URL").val();
	      window.location.href = base_url + "/administrator/AddNewCourse";
		},
	SaveCourseInfo : function(){
		return course_module.SaveCourseSubmitHandler();
	},
	SaveCourseSubmitHandler : function(){
		 var CourseForm = $('#FORM_SAVE_COURSE');
         var error3 = $('.alert-danger', CourseForm);
         var success3 = $('.alert-success', CourseForm);

         CourseForm.validate({
             errorElement: 'span', //default input error message container
             errorClass: 'help-block help-block-error', // default input error message class
             focusInvalid: false, // do not focus the last invalid input
             ignore: "", // validate all fields including form hidden input
             rules: {
            	 c_code: {
            		 minlength: 3,
            		 required: true
            	 },
            	 c_course_name: {
            		 required: true
            	 },
            	 fk_ctype_id: {
            		 required: true
            	 },
            	 c_total_hours: {
                   required: true
                 }
             },

             messages: { // custom messages for radio buttons and checkboxes

             },
             errorPlacement: function (error, element) { // render error placement for each input type
                 if (element.parent(".input-group").size() > 0) {
                     error.insertAfter(element.parent(".input-group"));
                 } else if (element.attr("data-error-container")) {
                     error.appendTo(element.attr("data-error-container"));
                 } else if (element.parents('.radio-list').size() > 0) {
                     error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                 } else if (element.parents('.radio-inline').size() > 0) {
                     error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                 } else if (element.parents('.checkbox-list').size() > 0) {
                     error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                 } else if (element.parents('.checkbox-inline').size() > 0) {
                     error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                 } else {
                     error.insertAfter(element); // for other inputs, just perform default behavior
                 }
             },
             invalidHandler: function (event, validator) { //display error alert on form submit
                 success3.hide();
                 error3.show();
             },
             success: function (label) {
                 label
                     .closest('.form-group').removeClass('has-error'); // set success class to the control group
             },
             highlight: function (element) { // hightlight error inputs
                 $(element)
                     .closest('.form-group').addClass('has-error'); // set error class to the control group
             },

             unhighlight: function (element) { // revert the change done by hightlight
                 $(element)
                     .closest('.form-group').removeClass('has-error'); // set error class to the control group
             },
             submitHandler: function (form) {
                success3.show();
                error3.hide();
                var base_url = $('#BASE_URL').val();
    	        var _token = $('input[name=_token]').val();
    	        var str_params = $("#FORM_SAVE_COURSE").serialize();
    	         $.ajax
    	        ({
    	            url : base_url + "/request/savecourse",
    	            data : str_params,
    	            dataType : "json",
    	            type : "POST",
    	            success : function(response){
    	              if(response.is_error == 0)
    	              {
    	                 window.location.href = base_url + "/administrator/Courses";
    	              }
    	            }
    	        });
             }

         });
	},
	DeleteCourseData : function(){
		 var c_id = $(this).parents('tr').data('c_id');
		bootbox.confirm("Are you sure you want to delete ?", function(result){
			//result
			if(result == true)
			{
			      var base_url = $('#BASE_URL').val();
			      var _token = $('input[name=_token]').val();
			        var str_params ={c_id : c_id , _token : _token};
			         $.ajax
			        ({
			            url : base_url + "/request/DeleteCourse",
			            data : str_params,
			            dataType : "Json",
			            type : "POST",
			            success : function(response){
			              if(response.is_error == 0)
			              {
			            	  course_module.displayListCourses();
			              }
			              else
		            	  {
			            	  bootbox.alert(response.error_msg);
		            	  }
			            }
			        });
			}
		});
	},
	DisplayEditCourseForm : function(){
		var c_id = $(this).parents('tr').data('c_id');
	    var base_url = $("#BASE_URL").val();
	    window.location.href = base_url + "/administrator/editCourseForm/" + c_id;
	},
	CancelForm : function(){
		 window.history.back();
	},
	OpenSendRequestPopup : function(){
		 // create the backdrop and wait for next modal to be triggered
        var el = $(this);
        var checked_courses = '';

        if($('input[id*=CK_COURSE_]:checked').size() == 0)
    	{
        	bootbox.alert('Select Course Before you Send Request for training');
        	return false;
    	}

        $('input[id*=CK_COURSE_]:checked').each(function(n){
    		if(n == 0)
			{
    			checked_courses = $(this).val();
			}
    		else
			{
    			checked_courses += "," + $(this).val();
			}
        });

        var base_url = $("#BASE_URL").val();
        var url = base_url + "/requests/SendRequestTraining/" + encodeURIComponent(checked_courses);
        $('body').modalmanager('loading');
        setTimeout(function(){
        	$SendRequest.load(url, '', function(){
        		$SendRequest.modal();
                $("input[name=r_request_train_date]").datepicker({
            		startDate :'+1d',
            		keepOpen : false,
            		format : "yyyy-mm-dd"
            	});

            	$('#BTN_SEND_REQUEST').on('click',course_module.SendCourseRequest)
          });
        }, 1000);
	},
	SendCourseRequest : function(){
		return course_module.SendCourseRequestSubmitHandler();
	},
	SendCourseRequestSubmitHandler : function(){
		var RequestForm = $('#FRM_SEND_REQUEST');
        var error3 = $('.alert-danger', RequestForm);
        var success3 = $('.alert-success', RequestForm);

        RequestForm.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                     r_request_order : {
                    required : true
                },
                r_request_description : {
                    required : true
                }
            },
            messages: { // custom messages for radio buttons and checkboxes
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
            },
            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            submitHandler: function (form) {

               var base_url = $('#BASE_URL').val();
   	        var _token = $('input[name=_token]').val();
   	        var str_params = $("#FRM_SEND_REQUEST").serialize();
   	         $.ajax
   	        ({
   	            url : base_url + "/request/sendtrainrequest",
   	            data : str_params,
   	            dataType : "json",
   	            type : "POST",
   	            success : function(response){
   	            	bootbox.alert(response.error_msg, function(){
   	            		$('.modal-scrollable').trigger('click');
   	            		$('input[type=checkbox]:checked').each(function(){
   	            			$(this).removeAttr('checked');
   	            		})
   	            	});


   	            }
   	        });
            }

        });
	},
	ResetCourseSearch : function(){
		$("input[name=search_query]").val('');
		course_module.CoursesManagement();
	},
	ShowAdvancedSearchPanel : function(){
		$('.AdvancedSearchSection').fadeToggle();
	},
	DisplayCourseCategoriesDropdown : function(){
		var course_type = $(this).val();
		if(course_type == '')
		{
			$('.CoursesCategoriesDropdown').html('');
			course_module.displayListCourses();
			return false;
		}
		 var base_url = $('#BASE_URL').val();
	        var _token = $('input[name=_token]').val();

		params = {course_type : course_type , _token : _token };
		$.ajax
        ({
            url : base_url + "/request/getCourseCategoriesDropdown",
            data : params,
            dataType : "json",
            type : "POST",
            success : function(response){
               $('.CoursesCategoriesDropdown').html(response.display);
               course_module.displayListCourses();
            }
        });
	},
	RDisplayCourseCategoriesDropdown : function(){
		var course_type = $(this).val();
		if(course_type == '')
		{
			$('.CoursesCategoriesDropdown').html('');
			course_module.displayListCourses();
			return false;
		}
		 var base_url = $('#BASE_URL').val();
	        var _token = $('input[name=_token]').val();

		params = {course_type : course_type , _token : _token };
		$.ajax
        ({
            url : base_url + "/request/getCourseCategoriesDropdown",
            data : params,
            dataType : "json",
            type : "POST",
            success : function(response){
               $('.CoursesCategoriesDropdown').html(response.display);
               course_module.RunningCoursesManagement();
            }
        });
	}
};

$SendRequest.on('click', '.update', function(){
	$SendRequest.modal('loading');
	  setTimeout(function(){
	    $modal
	      .modal('loading')
	      .find('.modal-body')
	        .prepend('<div class="alert alert-info fade in">' +
	          'Updated!<button type="button" class="close" data-dismiss="alert">&times;</button>' +
	        '</div>');
	  }, 1000);
	});
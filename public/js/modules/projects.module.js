/**
 *
 */
var $ChangeStatus = $('#ChangeStatus');
var $modelPopup = $('#ModelPopUp');
projects_module = {
    DisplayListAdminProjects: function () {
        var base_url = $('input[name=base_url]').val();
        var frm_search = $('#FRM_PROJECTS_MANAGEMENT').serialize();
        $.ajax
                ({
                    url: base_url + "/request/admin/displaylistProjects",
                    data: frm_search,
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        $('.ListProjects').html(response.display);
                        /**$("#datatables_myrequests").tablesorter();
                         $('.group-checkable').change(function() {
                         var set = $('table').find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
                         var checked = $(this).prop("checked");
                         $(set).each(function() {
                         $(this).prop("checked", checked);
                         });
                         $.uniform.update(set);
                         });*/

                        if (response.total_pages > 1)
                        {
                            $('#ProjectsPagination').twbsPagination({
                                totalPages: response.total_pages,
                                visiblePages: 7,
                                onPageClick: function (event, page) {
                                    $('input[name=page_number]').val(page);
                                    projects_module.DisplayListAdminProjects();
                                }
                            });
                        }

                    }
                });
    },
    DisplayListProjects: function () {
        var base_url = $('input[name=base_url]').val();
        var frm_search = $('#FRM_SEARCH_PROJECTS').serialize();
        $.ajax
                ({
                    url: base_url + "/request/displaylistProjects",
                    data: frm_search,
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        $('.SearchResults').html(response.display);
                        /**$("#datatables_myrequests").tablesorter();
                         $('.group-checkable').change(function() {
                         var set = $('table').find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
                         var checked = $(this).prop("checked");
                         $(set).each(function() {
                         $(this).prop("checked", checked);
                         });
                         $.uniform.update(set);
                         });*/

                        if (response.total_pages > 1)
                        {
                            $('#MyProjectsPagination').twbsPagination({
                                totalPages: response.total_pages,
                                visiblePages: 7,
                                onPageClick: function (event, page) {
                                    $('input[name=project_page_number]').val(page);
                                    projects_module.DisplayListProjects();
                                }
                            });
                        }

                    }
                });
    },
    OpenCreateProjectPage: function () {
        var base_url = $('#BASE_URL').val();
        window.location.href = base_url + "/omsar/projects/CreateNewProject"
    },
    AdminCreateConfigurationManagement: function () {
        var base_url = $('#BASE_URL').val();
        var pp_id = $(this).parents('tr').data('pp_id');
        window.location.href = base_url + "/administrator/projects/ProjectConfiguration/" + pp_id;
    },
    CreateConfigurationManagement: function () {
        var base_url = $('#BASE_URL').val();
        var pp_id = $(this).parents('.search-item').data('pp_id');
        window.location.href = base_url + "/omsar/projects/ProjectConfiguration/" + pp_id;
    },
    AddNewAdminProjectInformation: function () {
        var base_url = $('#BASE_URL').val();
        window.location.href = base_url + "/administrator/projects/CreateNewProject"
    },
    OpenEditProjectPage: function () {
        var pp_id = $(this).parents('.search-item').data('pp_id');
        var base_url = $('#BASE_URL').val();
        window.location.href = base_url + "/omsar/projects/EditProject/" + pp_id;
    },
    EditAdminProjectPage: function () {
        var pp_id = $(this).parents('tr').data('pp_id');
        var base_url = $('#BASE_URL').val();
        window.location.href = base_url + "/administrator/projects/EditProject/" + pp_id;
    },
    ViewProjectInformation: function () {
        var pp_id = $(this).parents('tr').data('pp_id');
        var base_url = $('#BASE_URL').val();
        window.location.href = base_url + "/administrator/projects/ViewProject/" + pp_id;
    },
    OpenChangeStatusPopup: function () {
        var el = $(this);
        var checked_projects = '';
        if ($('input[id*=CK_PROJECT_]:checked').size() == 0)
        {
            bootbox.alert('Select Project Before you You send Rejection');
            return false;
        }

        $('input[id*=CK_PROJECT_]:checked').each(function (n) {
            if (n == 0)
            {
                checked_projects = $(this).val();
            } else
            {
                checked_projects += "," + $(this).val();
            }
        });

        // reset checkbox buttons
        $('input[id*=CK_PROJECT_]').each(function () {
            $(this).removeAttr('checked');
        });


        var base_url = $("#BASE_URL").val();
        var url = base_url + "/request/ChangeProjectStatus/" + encodeURIComponent(checked_projects);
        $('body').modalmanager('loading');
        
        setTimeout(function () {
            $ChangeStatus.load(url, '', function () {
                $ChangeStatus.modal();
                $('#BTN_CHANGE_STATUS').on('click', projects_module.ChangeProjectStatus);
            });
        }, 1000);
    },
    OpenProjectTrainees: function () {
        var base_url = $("#BASE_URL").val();
        var project_id = $("#PROJECT_ID").val();
        var pc_id = $("#PC_ID").val();
        var url = base_url + "/request/SelectProjectTrainees/" + encodeURIComponent(project_id) + "/" + pc_id;
        $('body').modalmanager('loading');

        setTimeout(function () {
            $modelPopup.load(url, '', function () {
                $modelPopup.modal();
                $('#BTN_ADD_TRAINEES').on('click', projects_module.SaveAddTrainees);

            });
        }, 1000);
    },
    OpenGroupInfo: function () {
        var base_url = $("#BASE_URL").val();
        var project_id = $("#PROJECT_ID").val();
        var pc_id = $("#PC_ID").val();
        var pg_id = $(this).data('pg_id');
        var url = base_url + "/request/DisplayGroupInfo/" + pg_id;

        $('body').modalmanager('loading');

        setTimeout(function () {
            $modelPopup.load(url, '', function () {
                $modelPopup.modal();


            });
        }, 1000);
    },
    SaveAddTrainees: function () {
        var params = $("#FRM_PROJECT_TRAINEES").serialize();
        var base_url = $("#BASE_URL").val();
        $.ajax
                ({
                    url: base_url + "/request/AddCourseTrainees",
                    data: params,
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        if (response.is_error == 1)
                        {
                            bootbox.alert(response.error_msg);
                        } else
                        {
                            projects_module.DisplayProjectCourseForm();
                        }
                    }
                });

    },
    DeleteProjectInfo: function () {
        var project_id = $(this).parents('.search-item').data('pp_id');
        bootbox.confirm("Are you sure you want to delete ?", function (result) {
            //result
            if (result == true)
            {
                var base_url = $("#BASE_URL").val();
                var _token = $('input[name=_token]').val();
                var params = {_token: _token, project_ids: project_id};
                $.ajax
                        ({
                            url: base_url + "/request/deleteProject",
                            data: params,
                            dataType: "json",
                            type: "POST",
                            success: function (response) {
                                if (response.is_error == 1)
                                {
                                    bootbox.alert(response.error_msg);
                                } else
                                {
                                    projects_module.DisplayListProjects();
                                }
                            }
                        });
            }
        });
    },
    AdminDeleteProjectInfo: function () {
        var project_id = $(this).parents('tr').data('pp_id');
        bootbox.confirm("Are you sure you want to delete ?", function (result) {
            //result
            if (result == true)
            {
                var base_url = $("#BASE_URL").val();
                var _token = $('input[name=_token]').val();
                var params = {_token: _token, project_ids: project_id};
                $.ajax
                        ({
                            url: base_url + "/request/deleteProject",
                            data: params,
                            dataType: "json",
                            type: "POST",
                            success: function (response) {
                                if (response.is_error == 1)
                                {
                                    bootbox.alert(response.error_msg);
                                } else
                                {
                                    projects_module.DisplayListAdminProjects();
                                }
                            }
                        });
            }
        });
    },
    ChangeProjectStatus: function () {
        /**var str_params = $("#FRM_PROJECT_STATUS").serialize();
         $.ajax
         ({
         url : base_url + "/request/changeprojectstatus",
         data : str_params,
         dataType : "json",
         type : "POST",
         success : function(response){
         if(response.is_error == 0)
         {
         projects_module.DisplayListProjects();
         $('.modal-scrollable').trigger('click');
         }
         }
         });*/
        return projects_module.SubmitChangeProjectStatusHandler();
    },
    SubmitChangeProjectStatusHandler: function () {
        var SaveProjectForm = $('#FRM_PROJECT_STATUS');
        var error3 = $('.alert-danger', SaveProjectForm);
        var success3 = $('.alert-success', SaveProjectForm);

        SaveProjectForm.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                ps_project_status: {
                    required: true
                }
            },

            messages: {// custom messages for radio buttons and checkboxes

            },
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
            },
            success: function (label) {
                label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            submitHandler: function (form) {
                success3.show();
                error3.hide();
                var base_url = $('#BASE_URL').val();
                var str_params = $("#FRM_PROJECT_STATUS").serialize();
                $.ajax
                        ({
                            url: base_url + "/request/changeprojectstatus",
                            data: str_params,
                            dataType: "json",
                            type: "POST",
                            success: function (response) {
                                if (response.is_error == 0)
                                {
                                    var is_admin = $("#is_admin").val();
                                    if(is_admin == 1){
                                        projects_module.DisplayListAdminProjects();
                                    }else{
                                        projects_module.DisplayListProjects();
                                    }
                                    
                                    $('.modal-scrollable').trigger('click');
                                }
                            }
                        });
            }

        });
    },
    SaveProjectInfo: function () {
        return projects_module.SubmitCreateProjectSubmitHandler();
    },
    SaveAdminProjectInfo: function () {
        return projects_module.AdminProjectSubmitHandler();
    },
    SubmitCreateProjectSubmitHandler: function () {
        var SaveProjectForm = $('#FORM_SAVE_PROJECT');
        var error3 = $('.alert-danger', SaveProjectForm);
        var success3 = $('.alert-success', SaveProjectForm);

        SaveProjectForm.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                pp_project_code: {
                    required: true
                },
                pp_project_title: {
                    minlength: 6,
                    required: true
                },
                sl_project_type: {
                    required: true
                },
                pp_project_billing_method: {
                    required: true
                },
                sl_donor: {
                    required: true
                },
                pp_project_budget: {
                    required: true
                },
                pp_project_start_date: {
                    required: true
                },
                pp_project_end_date: {
                    required: true
                },
                pp_contract_date: {
                    required: true
                },
                pc_project_courses: {
                    required: true
                },
                pp_project_providers: {
                    required: true
                }
            },

            messages: {// custom messages for radio buttons and checkboxes

            },
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
            },
            success: function (label) {
                label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            submitHandler: function (form) {
                success3.show();
                error3.hide();
                var base_url = $('#BASE_URL').val();
                var _token = $('input[name=_token]').val();
                var str_params = $("#FORM_SAVE_PROJECT").serialize();
                $.ajax
                        ({
                            url: base_url + "/request/CreateNewProject",
                            data: str_params,
                            dataType: "json",
                            type: "POST",
                            success: function (response) {
                                window.location.href = base_url + "/omsar/projects/ProjectConfiguration/" + response.project_id;
                            }
                        });
            }

        });
    },
    AdminProjectSubmitHandler: function () {
        var SaveProjectForm = $('#FORM_SAVE_PROJECT');
        var error3 = $('.alert-danger', SaveProjectForm);
        var success3 = $('.alert-success', SaveProjectForm);

        SaveProjectForm.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                pp_project_code: {
                    required: true
                },
                pp_project_title: {
                    minlength: 6,
                    required: true
                },
                sl_project_type: {
                    required: true
                },
                sl_donor: {
                    required: true
                },
                pp_project_billing_method: {
                    required: true
                },
                pp_project_budget: {
                    required: true
                },
                pp_project_start_date: {
                    required: true
                },
                pp_project_end_date: {
                    required: true
                },
                pp_contract_date: {
                    required: true
                },
                pc_project_courses: {
                    required: true
                },
                pp_project_requests: {
                    required: true
                }
            },

            messages: {// custom messages for radio buttons and checkboxes

            },
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
            },
            success: function (label) {
                label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            submitHandler: function (form) {
                success3.show();
                error3.hide();
                var base_url = $('#BASE_URL').val();
                var _token = $('input[name=_token]').val();
                var str_params = $("#FORM_SAVE_PROJECT").serialize();
                $.ajax
                        ({
                            url: base_url + "/request/AdminSaveProjectInfo",
                            data: str_params,
                            dataType: "json",
                            type: "POST",
                            success: function (response) {
                                window.location.href = base_url + "/administrator/projects/ProjectConfiguration/" + response.project_id;
                            }
                        });
            }

        });
    },
    CheckAllProjects: function () {
        var checked = $(this).is(":checked") ? true : false;
        $('input[type=checkbox][class=checkbox]').each(function () {
            if (checked == true)
            {
                $(this).attr('checked', 'checked');
            } else
            {
                $(this).removeAttr('checked');
            }
        });
    },
    GetListOfSessionsInProjects: function () {
        var base_url = $("#BASE_URL").val();
        var project_id = $("#PROJECT_ID").val();
        var _token = $("input[name=_token]").val();
        var params = {_token: _token, project_id: project_id};
        $.ajax
                ({
                    url: base_url + "/request/GetListSessionsCalendar",
                    data: params,
                    dataType: "json",

                    type: "POST",
                    success: function (response) {
                        //var session_calendar_path 	= decodeURIComponent(response.session_calendar_path);
                        var base_url = $("#BASE_URL").val();
                        var $project_id = response.project_id;
                        var session_xml_url = base_url + '/resources/projects/calenders/' + $project_id + '/session_calender.xml?v=' + Math.random();
                        var project_id = response.project_id;
                        scheduler.attachEvent("onEventclick", function (id, ev) {
                            return false;
                        });
                        scheduler.attachEvent("onClick", function (id, ev) {
                            return false;
                        });
                        scheduler.attachEvent("onEventChanged", function (id, ev) {
                            var base_url = $("#BASE_URL").val();
                            var _token = $("input[name=_token]").val();
                            var session_id = id;
                            var new_start_date = ev.start_date;
                            var new_end_date = ev.end_date;
                            var params = {_token: _token, session_id: session_id, start_date: new_start_date, end_date: new_end_date};
                            $.ajax
                                    ({
                                        url: base_url + "/request/ChangeSessionTime",
                                        data: params,
                                        dataType: "json",
                                        type: "POST",
                                        success: function (response) {
                                            bootbox.alert(response.error_msg);
                                        }
                                    });
                            return false;
                        });
                        scheduler.attachEvent("onDblClick", function (id, e) {
                            var session_id = id;

                            var base_url = $("#BASE_URL").val();
                            var url = base_url + "/session/EditSessionInformation/" + session_id;
                            $('body').modalmanager('loading');
                            setTimeout(function () {
                                $modelPopup.load(url, '', function () {
                                    $modelPopup.modal();
                                    $("input[id=SC_SESSION_DATE]").datepicker({
                                        orientation: 'bottom',
                                        keepOpen: false,
                                        format: "yyyy-mm-dd"
                                    });
                                    $('input[name=sc_session_time]').timepicker({
                                        showSeconds: true,
                                        showMeridian: false
                                    });
                                    $(".modal_close").on("click", function () {
                                        $modelPopup.modal('toggle');
                                    });
                                    $("#BTN_SAVE_SESSION").on('click', projects_module.SaveSessionInformation);

                                });
                            }, 1000);

                            return false;
                        });
                        scheduler.config.xml_date = "%Y-%m-%d %H:%i";
                        scheduler.init('SessionScheduler', new Date(), "week");
                        scheduler.load(session_xml_url);
                        scheduler.config.dblclick_create = false;

                    }
                });
    },
    DisplayConfigurationPage: function () {
        var tab_name = $("#TAB_NAME").val();
        var base_url = $("#BASE_URL").val();
        var project_id = $("#PROJECT_ID").val();
        var _token = $("input[name=_token]").val();
        var params = {_token: _token, project_id: project_id, tab_name: tab_name};
        $.ajax
                ({
                    url: base_url + "/project/DisplayProjectConfigurationTab",
                    data: params,
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        $('.ConfigurationManager').html(response.display);

                        switch (tab_name)
                        {
                            case "trainees_configuration":
                                {
                                    $('.group-checkable').change(function () {
                                        var set = $('table').find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
                                        var checked = $(this).prop("checked");
                                        $(set).each(function () {
                                            $(this).prop("checked", checked);
                                        });
                                        $.uniform.update(set);
                                    });
                                    projects_module.InitTraineesTable();
                                }
                                break;
                            case "groups_configuration":
                                {

                                    projects_module.DisplayListClasses();
                                }
                                break;
                            case "courses_configuration":
                                {
                                    projects_module.DisplayProjectCourseForm();
                                }
                                break;
                            case "sessions_configuration":
                                {
                                    projects_module.GetListOfSessionsInProjects();
                                }
                                break;
                            case "preview_confirmation":
                                {

                                }
                                break;
                        }


                    }
                });
    },
    InitTraineesTable: function () {
        var table = $('#TraineesTable');
        var oTable = table.dataTable({
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // setup buttons extentension: http://datatables.net/extensions/buttons/
            buttons: [
            ],

            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: {
                details: {

                }
            },

            "order": [
                [0, 'asc']
            ],

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
            // So when dropdowns used the scrollable div should be removed.
            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        });
    },
    ChangeProjectPointer: function (next_tab) {
        $('.mt-step-col').each(function () {
            $(this).removeClass('active');
            $(this).removeClass('done');
        });
        var current_tab_index = 0;
        $('.mt-step-col').each(function (n) {

            if (next_tab == $(this).data('tab_name'))
            {
                $(this).addClass('active');
                current_tab_index = n;
            }
        });

        $('.mt-step-col').each(function (n) {
            if (n < current_tab_index)
            {
                $(this).addClass('done');
            } else if (n == current_tab_index)
            {
                $(this).addClass('active');
            }
        });
    },
    SaveProjectConfigurationInfo: function () {
        var frm_info = $("#FRM_CONFIGURATION").serialize();
        var base_url = $("#BASE_URL").val();
        $.ajax
                ({
                    url: base_url + "/project/SaveProjectConfiguration",
                    data: frm_info,
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        if (response.is_error == 0)
                        {
                            if (response.next_tab != '')
                            {
                                var next_tab = response.next_tab;
                                $('#TAB_NAME').val(next_tab);
                                projects_module.ChangeProjectPointer(next_tab);
                                projects_module.DisplayConfigurationPage();
                            } else
                            {
                                var base_url = $("#BASE_URL").val();
                                window.location.href = base_url + "/omsar/projects";
                            }
                        } else
                        {
                            alert(response.error_msg);
                        }


                    }
                });
    },
    SaveAdminProjectConfigurationInfo: function () {
        var frm_info = $("#FRM_CONFIGURATION").serialize();
        var base_url = $("#BASE_URL").val();
        $.ajax
                ({
                    url: base_url + "/project/SaveAdminProjectConfiguration",
                    data: frm_info,
                    dataType: "json",
                    type: "POST",
                    success: function (response) {

                        if (response.is_error == 0)
                        {
                            if (response.next_tab != '')
                            {
                                var next_tab = response.next_tab;
                                $('#TAB_NAME').val(next_tab);
                                projects_module.ChangeProjectPointer(next_tab);
                                projects_module.DisplayConfigurationPage();
                            } else
                            {
                                var base_url = $("#BASE_URL").val();
                                window.location.href = base_url + "/administrator/projects";
                            }
                        } else
                        {
                            alert(response.error_msg);
                        }
                    }
                });
    },
    SaveCourseInformation: function () {
        var frm_info = $("#FRM_CONFIGURATION").serialize();
        var base_url = $("#BASE_URL").val();
        $.ajax
                ({
                    url: base_url + "/request/SaveProjectCourseInformation",
                    data: frm_info,
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        bootbox.alert(response.error_msg);
                    }
                });
    },
    DisplayProjectCourseForm: function () {
        var base_url = $("#BASE_URL").val();
        var _token = $('input[name=_token]').val();
        var project_id = $("#PROJECT_ID").val();
        var project_course = $("select[name=project_course]").val();

        var params = {_token: _token, project_id: project_id, project_course: project_course};
        $.ajax
                ({
                    url: base_url + "/request/project/DisplayProjectCourseInformation",
                    data: params,
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        $('.CourseFormInformation').html(response.display);
                        $('.course_code').html(" ID = <b>" + response.course_code + "</b>");
                        $('#PC_PROJECT_TRAINEES option').each(function () {
                            $(this).attr('selected', 'selected');
                        });
                    }
                });
    },
    SaveSessionInformation: function () {
        return projects_module.SubmitSaveSessionInformationHandler();
    },
    SubmitSaveSessionInformationHandler: function () {
        var SaveSessionInfo = $('#FRM_SESSION_INFO');
        var error3 = $('.alert-danger', SaveSessionInfo);
        var success3 = $('.alert-success', SaveSessionInfo);

        SaveSessionInfo.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                fk_session_status_id: {
                    required: true
                },
                sc_session_time: {
                    required: true
                },
                sc_session_duration: {
                    required: true
                }

            },

            messages: {// custom messages for radio buttons and checkboxes

            },
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
            },
            success: function (label) {
                label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            submitHandler: function (form) {
                success3.show();
                error3.hide();
                var base_url = $("#BASE_URL").val();
                var frm_info = $("#FRM_SESSION_INFO").serialize();
                $.ajax
                        ({
                            url: base_url + "/session/SaveSessionInformation",
                            data: frm_info,
                            dataType: "json",
                            type: "POST",
                            success: function (response) {
                                window.parent.bootbox.alert(response.error_msg, function () {
                                    $('.modal-scrollable').trigger('click');
                                });
                            }
                        });
            }

        });
    },
    AddNewProjectClass: function () {
        // open new popup to add new group to current project
        var base_url = $("#BASE_URL").val();
        var project_id = $("input[name=project_id]").val();
        var url = base_url + "/administrator/CreateNewProjectClass/" + project_id;

        $('body').modalmanager('loading');
        setTimeout(function () {
            $modelPopup.load(url, '', function () {
                $modelPopup.modal();
                $("input[name=pg_start_date],input[name=pg_end_date]").datepicker({
                    keepOpen: false,
                    format: "yyyy-mm-dd"
                });
                $('input[name=pg_trainee_time]').timepicker({
                    showSeconds: true,
                    showMeridian: false,
                });
                $("#PG_CLASS_PROVIDER").on('change', projects_module.DisplayProviderLocationsDropDown);
                $("#BTN_CREATE_CLASS").on('click', projects_module.CreateNewClassProject);

            });
        }, 1000);
    },
    DisplayProviderLocationsDropDown: function () {
        var base_url = $("#BASE_URL").val();
        var _token = $('input[name=_token]').val();
        var pg_class_provider = $('#PG_CLASS_PROVIDER').val();
        var ini_location = 0;

        if ($('input[name=ini_pg_trainee_location_id]').size() > 0)
        {
            ini_location = $('input[name=ini_pg_trainee_location_id]').val();
        }


        var params = {_token: _token, pg_class_provider: pg_class_provider, ini_location: ini_location};
        $.ajax
                ({
                    url: base_url + "/provider/DisplayProviderLocationDropdown",
                    data: params,
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        $('#providerLocation').html(response.display);
                    }
                });
    },
    CreateNewClassProject: function () {
        return projects_module.CreateNewClassProjectHandler();
    },
    SaveClassProjectInfo: function () {
        return projects_module.CreateNewClassProjectHandler();
    },
    CreateNewClassProjectHandler: function () {
        var NewGroupForm = $('#FRM_NEW_GROUP');
        var error3 = $('.alert-danger', NewGroupForm);
        var success3 = $('.alert-success', NewGroupForm);

        NewGroupForm.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                pg_group_code: {
                    required: true
                },
                pg_group_title: {
                    required: true
                },
                pg_group_description: {
                    required: true
                },
                sl_train_days: {
                    required: true,
                    min: 1
                },
                pg_start_date: {
                    required: true
                },
                pg_end_date: {
                    required: true
                }

            },

            messages: {// custom messages for radio buttons and checkboxes
                sl_train_days: {
                    min: " Select a valid value"
                }

            },
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
            },
            success: function (label) {
                label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            submitHandler: function (form) {
                success3.show();
                error3.hide();
                var base_url = $("#BASE_URL").val();
                var project_id = $("#PROJECT_ID").val();
                var generate_class = $("#generate_class").val();
                var frm_info = $("#FRM_NEW_GROUP").serialize() + "&project_id=" + project_id + "&generate_class=" + generate_class;

                $.ajax
                        ({
                            url: base_url + "/request/CreateNewGroup",
                            data: frm_info,
                            dataType: "json",
                            type: "POST",
                            success: function (response) {
                                window.parent.bootbox.alert(response.error_msg, function () {
                                    projects_module.DisplayListClasses();
                                    $('.modal-scrollable').trigger('click');
                                });
                            }
                        });
            }

        });
    },
    DisplayListClasses: function () {
        projects_module.DisplayListGroups();
    },
    dragMoveListener: function (event) {
        var target = event.target;
        // keep the dragged position in the data-x/data-y attributes
        x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
                y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

        // translate the element
        target.style.webkitTransform =
                target.style.transform =
                'translate(' + x + 'px, ' + y + 'px)';
        // target.style.position = "absolute";
        // update the posiion attributes
        target.setAttribute('data-x', x);
        target.setAttribute('data-y', y);
    },
    EditGroupClass: function () {
        var pg_id = $(this).data('pg_id');
        var base_url = $("#BASE_URL").val();
        var url = base_url + "/administrator/EditProjectGroup/" + pg_id;

        $('body').modalmanager('loading');
        setTimeout(function () {
            $modelPopup.load(url, '', function () {
                $modelPopup.modal();
                $("input[name=pg_start_date],input[name=pg_end_date]").datepicker({
                    keepOpen: false,
                    format: "yyyy-mm-dd"
                });
                $('input[name=pg_trainee_time]').timepicker({
                    showSeconds: true,
                    showMeridian: false,
                });
                projects_module.DisplayProviderLocationsDropDown();
                $("#PG_CLASS_PROVIDER").on('change', projects_module.DisplayProviderLocationsDropDown);
                $("#BTN_SAVE_GROUP_INFO").on('click', projects_module.SaveClassProjectInfo);

            });
        }, 1000);

    },
    EditTraineesGroup: function () {
        var pg_id = $(this).data('pg_id');
        var base_url = $("#BASE_URL").val();
        var url = base_url + "/project/EditTraineesGroup/" + pg_id;

        $('body').modalmanager('loading');
        setTimeout(function () {
            $modelPopup.load(url, '', function () {
                $modelPopup.modal();
                $("#BTN_SAVE_TRAINEES_GROUP").on('click', projects_module.SaveGroupTrainees);

            });
        }, 1000);
    },
    SaveGroupTrainees: function () {
        var frm_trainees = $("#FRM_TRAINEES_GROUP").serialize();
        var base_url = $("#BASE_URL").val();
        $.ajax
                ({
                    url: base_url + "/request/SaveTraineesGroup",
                    data: frm_trainees,
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        $('.modal-scrollable').trigger('click');
                    }
                });
    },
    DeleteGroupClass: function () {
        var pg_id = $(this).data('pg_id');
        var base_url = $("#BASE_URL").val();
        var _token = $('input[name=_token]').val();
        bootbox.confirm('Are You Sure You Want to delete ?', function (result) {
            if (result == true)
            {
                var params = {pg_id: pg_id, _token: _token};
                $.ajax
                        ({
                            url: base_url + "/request/DeleteGroupInfo",
                            data: params,
                            dataType: "json",
                            type: "POST",
                            success: function (response) {
                                projects_module.DisplayConfigurationPage();
                            }
                        });
            }
        });

    },
    DeleteMultipleGroup: function (e) {
        e.preventDefault();
        var pg_ids = "";
        /** get list of selected checkbox of groups */

        $('.checkboxes:checked').each(function (n) {
            if (n == 0)
            {
                pg_ids = $(this).parents('tr').data('pg_id');
            } else
            {
                pg_ids = pg_ids + "," + $(this).parents('tr').data('pg_id');
            }
        })

        var base_url = $("#BASE_URL").val();
        var _token = $('input[name=_token]').val();

        var r = confirm("Are You Sure You Want to delete ?");
        if (r == true) {
            var params = {pg_id: pg_ids, _token: _token};
            $.ajax
                    ({
                        url: base_url + "/request/DeleteGroupInfo",
                        data: params,
                        dataType: "json",
                        type: "POST",
                        success: function (response) {
                            projects_module.DisplayConfigurationPage();
                        }
                    });
        }

    },
    ChangeDisplayGroup: function () {
        var type = 'list';
        $("#PG_LIST_TYPE").val(type);
        projects_module.DisplayListGroups();
    },
    DisplayListGroups: function () {
        var project_id = $("#PROJECT_ID").val();
        var project_course = $("#SL_PROJECT_COURSE").val();
        var display_type = 'list';
        var base_url = $("#BASE_URL").val();
        var _token = $('input[name=_token]').val();

        var params = {project_id: project_id, _token: _token, display_type: display_type, project_course: project_course};
        $.ajax
                ({
                    url: base_url + "/request/DisplayListGroups",
                    data: params,
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        $(".lstGroups").html(response.display);
                    }
                });
    }
};

window.dragMoveListener = projects_module.dragMoveListener;
/**
 *
 */


session_statuses_module = {
		displayListSessionStatuses : function(){
			var base_url = $('input[name=base_url]').val();
		    var _token = $('input[name=_token]').val()
		    var page_number = $('input[name=page_number]').val();
		    $.ajax
		    ({
		        url : base_url + "/request/displaysessionstatuses",
		        data : { _token : _token , page_number : page_number },
		        dataType : "json",
		        type : "POST",
		        success : function(response){
		            $('.ListSessionStatusesGird').html(response.display);
                             $('.group-checkable').change(function() {
                                var set = $('table').find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
                                var checked = $(this).prop("checked");
                                $(set).each(function() {
                                    $(this).prop("checked", checked);
                                });
                                $.uniform.update(set);
                            });

                             $('#SessionStatusPagination').twbsPagination({
                                 totalPages: response.total_pages,
                                 visiblePages: 7,
                                 onPageClick: function (event, page) {
                                      $('input[name=page_number]').val(page);
                                      session_statuses_module.displayListSessionStatuses();
                                 }
                             });
		        }
		    });
		},
		AddNewSessionStatusForm : function(){
			var base_url = $("#BASE_URL").val();
	      window.location.href = base_url + "/administrator/AddNewSessionStatus";
		},
	SaveSessionStatusInfo : function(){
		return session_statuses_module.SaveSessionStatusSubmitHandler();
	},
	SaveSessionStatusSubmitHandler : function(){
		 var SessionSForm = $('#FORM_SAVE_SESSION_STATUS');
         var error3 = $('.alert-danger', SessionSForm);
         var success3 = $('.alert-success', SessionSForm);

         SessionSForm.validate({
             errorElement: 'span', //default input error message container
             errorClass: 'help-block help-block-error', // default input error message class
             focusInvalid: false, // do not focus the last invalid input
             ignore: "", // validate all fields including form hidden input
             rules: {
            	 ss_status_title: {
            		 minlength: 6,
            		 required: true
            	 },
            	 ss_status_color : {
                     minlength: 6,
                     required: true
                   }
             },

             messages: { // custom messages for radio buttons and checkboxes

             },
             errorPlacement: function (error, element) { // render error placement for each input type
                 if (element.parent(".input-group").size() > 0) {
                     error.insertAfter(element.parent(".input-group"));
                 } else if (element.attr("data-error-container")) {
                     error.appendTo(element.attr("data-error-container"));
                 } else if (element.parents('.radio-list').size() > 0) {
                     error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                 } else if (element.parents('.radio-inline').size() > 0) {
                     error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                 } else if (element.parents('.checkbox-list').size() > 0) {
                     error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                 } else if (element.parents('.checkbox-inline').size() > 0) {
                     error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                 } else {
                     error.insertAfter(element); // for other inputs, just perform default behavior
                 }
             },
             invalidHandler: function (event, validator) { //display error alert on form submit
                 success3.hide();
                 error3.show();
             },
             success: function (label) {
                 label
                     .closest('.form-group').removeClass('has-error'); // set success class to the control group
             },
             highlight: function (element) { // hightlight error inputs
                 $(element)
                     .closest('.form-group').addClass('has-error'); // set error class to the control group
             },

             unhighlight: function (element) { // revert the change done by hightlight
                 $(element)
                     .closest('.form-group').removeClass('has-error'); // set error class to the control group
             },
             submitHandler: function (form) {
                success3.show();
                error3.hide();
                var base_url = $('#BASE_URL').val();
    	        var _token = $('input[name=_token]').val();
    	        var str_params = $("#FORM_SAVE_SESSION_STATUS").serialize();
    	         $.ajax
    	        ({
    	            url : base_url + "/request/saveSessionStatus",
    	            data : str_params,
    	            dataType : "json",
    	            type : "POST",
    	            success : function(response){
    	              if(response.is_error == 0)
    	              {
    	                 window.location.href = base_url + "/administrator/sessionstatuses";
    	              }
    	            }
    	        });
             }

         });
	},
	DeleteSessionStatusData : function(){
		 var ss_id = $(this).parents('tr').data('ss_id');
		bootbox.confirm("Are you sure you want to delete ?", function(result){
			//result
			if(result == true)
			{
			      var base_url = $('#BASE_URL').val();
			      var _token = $('input[name=_token]').val();
			        var str_params ={ss_id : ss_id , _token : _token};
			         $.ajax
			        ({
			            url : base_url + "/request/DeleteSessionStatus",
			            data : str_params,
			            dataType : "Json",
			            type : "POST",
			            success : function(response){
			              if(response.is_error == 0)
			              {
			            	  session_statuses_module.displayListSessionStatuses();
			              }
			            }
			        });
			}
		});
	},
	DisplayEditSessionStatusForm : function(){
		var ss_id = $(this).parents('tr').data('ss_id');
	    var base_url = $("#BASE_URL").val();
	    window.location.href = base_url + "/administrator/editSessionStatusForm/" + ss_id;
	},
	CancelForm : function(){
		 window.history.back();
	},
        ExportToExcell: function(){
            var base_url = $("#BASE_URL").val();
            var url = base_url + "/administrator/exportexcel/sessionstatus";
            window.open(url,'_blank');
        }
};
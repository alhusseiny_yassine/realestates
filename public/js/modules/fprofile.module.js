/**
 *
 */

fprofile_module = {
		DisplayProfileDashboard : function(){
			var base_url = $('input[name=base_url]').val();
		    var _token = $('input[name=_token]').val()
		    var params = { _token : _token };
		    $.ajax
	        ({
	            url : base_url + "/trainees/DisplayrofileDashboard",
	            data : params,
	            dataType : "Json",
	            type : "POST",
	            success : function(response){
	            	 $('.profile-content').html(response.display);
	            }
	        });
		},
		DisplayProfileAccountSettings : function(){
			var base_url = $('input[name=base_url]').val();
		    var _token = $('input[name=_token]').val()
		    var params = { _token : _token };
		    $.ajax
	        ({
	            url : base_url + "/trainees/DisplayProfileAccountSettings",
	            data : params,
	            dataType : "Json",
	            type : "POST",
	            success : function(response){
	               $('.profile-content').html(response.display);
	               users_module.DisplayUserEducationInfoList();
	               users_module.DisplayUserProviderLocationsList();

	               $('.EducationInfo').on('click','button[name=btn_add_education_info]',users_module.OpenEducationInfoPage);
	        	   $('.EducationInfo').on('click','a[id*=EDIT_UE_]',users_module.OpenEditEducationInfoPage);
	        	   $('.EducationInfo').on('click','a[id*=DELETE_UE_]',users_module.DeleteEducationInfo);

	          	   $('.ProviderLocation').on('click','button[name=btn_add_provider_location]',users_module.OpenProviderLocationPage);
	        	   $('.ProviderLocation').on('click','a[id*=EDIT_PL_]',users_module.OpenEditProviderLocationPage);
	        	   $('.ProviderLocation').on('click','a[id*=DELETE_PL_]',users_module.DeleteProviderLocation);
	            }
	        });
		},
		UploadProfilePic : function(event){
			event.stopPropagation(); // Stop stuff happening
	        event.preventDefault(); // Totally stop stuff happening
	        var base_url 	= $("#BASE_URL").val();



	        // Create a formdata object and add the files
	        var FormDataFields = $(this);

	        var data = new FormData();
	        var index = 0;

	        $.each($("input[type=file]"), function(i, obj) {
	                var name = $(this).attr('name');
	                $.each(obj.files,function(j,file){
	                        data.append(name, file);
	                })
	        });

	        FormDataFields.find('input,select').each(function(){
	                data.append($(this).attr('name'), $(this).val() );
	        });

	        $.ajax
	        ({
	            url: base_url + '/uploads/fprofile',
	            data: data,
	            async: false,
	            cache: false,
	            method : 'post',
	            contentType: false,
	            processData: false,
	            dataType : "json",
	            beforeSend : function(){
	            },
	            success: function (response) {
	            	$('#PROFILE_PIC').attr('src',response.image_url);
	            	$('#MAIN_PROFILE_PIC').attr('src',response.image_url);
	            	$('#SECTION_PROFILE_PIC').attr('src',response.image_url);
	            }
	    });
		},
		SaveProfileInfo : function(){
			var params = $("#FORM_PROFILE_INFO").serialize();
			var base_url = $('input[name=base_url]').val();
			$.ajax
	        ({
	            url : base_url + "/trainees/SaveProfileInformation",
	            data : params,
	            dataType : "Json",
	            type : "POST",
	            success : function(response){
	            	bootbox.alert(response.error_msg);
	            }
	        });
		},
		ChangePassword : function(){
			var base_url = $('input[name=base_url]').val();
			var params = $("form[name=frm_change_password]").serialize();
			$.ajax
	        ({
	            url : base_url + "/trainees/ChangeUserPassword",
	            data : params,
	            dataType : "Json",
	            type : "POST",
	            success : function(response){
	            	bootbox.alert(response.error_msg);
	            }
	        });
		}
}
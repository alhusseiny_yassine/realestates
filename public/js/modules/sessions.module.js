var $modelPopup = $('#ModelPopUp');
var sessions_module = {
    DisplaySessions: function (){
        var base_url    = $('input[name=base_url]').val();
        var _token      = $('input[name=_token]').val();
        var project_id  = $("#select_project").val();
        var class_id    = $("#select_group").val(); 
        $("#PROJECT_ID").val(project_id);
        $("#CLASS_ID").val(class_id);
        var page_number = $('input[name=session_page_number]').val();
        var display_type= $("#DISPLAY_TYPE").val();
        $.ajax
                ({
                    url: base_url + "/request/DisplaySessions",
                    data: { _token: _token, project_id: project_id, page_number: page_number, class_id: class_id,display_type: display_type},
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        $('#SESSIONS').html(response.display);
                        switch(display_type){
                            case'calendar':
                            {
                                $("#SessionsPagination").css('display','none');
                                projects_module.GetListOfSessionsInProjects();
                            }
                            break;
                            case'list':
                            {
                                $("#SessionsPagination").css('display','block');
                                $('#SessionsPagination').twbsPagination({
                                    totalPages: response.total_pages,
                                    visiblePages: 7,
                                    onPageClick: function (event, page) {
                                        $('input[name=session_page_number]').val(page);
                                        sessions_module.DisplaySessions();
                                    }
                                });
                            }
                            break;
                        }
                    }
                });
    },
    DisplayProjectGroups: function() {
        var base_url    = $('input[name=base_url]').val();
        var _token      = $('input[name=_token]').val();
        var project_id  = $("#select_project").val();
        
        $.ajax({
            url: base_url + "/request/DisplayProjectGroups",
            data: { _token: _token, project_id: project_id },
            dataType: "json",
            type: "POST",
            success: function (response) {
                $('#select_group').html(response.display);
            }
        });
    },
    DisplayAddSession: function() {
        var base_url    = $('input[name=base_url]').val();
        var _token      = $('input[name=_token]').val();
        var project_id  = $("#select_project").val();
        var class_id    = $("#select_group").val(); 
        
        if(project_id == '' || class_id == ''){
            return;
        }
        
        var url = base_url + "/session/DisplayAddSession";
        $('body').modalmanager('loading');
        setTimeout(function () {
            $modelPopup.load(url, '', function () {
                $modelPopup.modal();
                $("input[id=SC_SESSION_DATE]").datepicker({
                    orientation: 'bottom',
                    keepOpen: false,
                    format: "yyyy-mm-dd"
                });
                $('input[name=sc_session_time]').timepicker({
                    showSeconds: true,
                    showMeridian: false,
                });
                $(".modal_close").on("click",function(){
                    $modelPopup.modal('toggle')
                });
                $("#BTN_SAVE_SESSION").on('click', sessions_module.SaveSession);

            });
        }, 1000);
        
    },
    SaveSession: function () {
        return sessions_module.SubmitSaveSession();
    },
    SubmitSaveSession: function () {
        var SaveSessionInfo = $('#FRM_SESSION_INFO');
        var error3 = $('.alert-danger', SaveSessionInfo);
        var success3 = $('.alert-success', SaveSessionInfo);

        SaveSessionInfo.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                fk_session_status_id: {
                    required: true
                },
                sc_session_time: {
                    required: true
                },
                sc_session_duration: {
                    required: true
                },
                fk_session_trainer_id: {
                    required: true
                },
                sc_session_date: {
                    required: true
                }

            },

            messages: {// custom messages for radio buttons and checkboxes

            },
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
            },
            success: function (label) {
                label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            submitHandler: function (form) {
                success3.show();
                error3.hide();
                var class_id = $("#select_group").val();
                $("input[name=class_id]").val(class_id);
                var base_url = $("#BASE_URL").val();
                var frm_info = $("#FRM_SESSION_INFO").serialize();
                $.ajax
                        ({
                            url: base_url + "/request/SaveSession",
                            data: frm_info,
                            dataType: "json",
                            type: "POST",
                            success: function (response) {
                                window.parent.bootbox.alert(response.error_msg, function () {
                                    $('.modal-scrollable').trigger('click');
                                });
                                sessions_module.DisplaySessions();
                            }
                        });
            }

        });
	},
        DisplayEditSession: function () {
            
        var base_url = $('input[name=base_url]').val();
        var session_id = $(this).data('session_id');
        var _token = $('input[name=_token]').val();
        var project_id = $("#select_project").val();
        var class_id = $("#select_group").val();

        if (project_id == '' || class_id == '') {
            return;
        }

        var url = base_url + "/session/DisplayEditSession/"+session_id;
        $('body').modalmanager('loading');
        setTimeout(function () {
            $modelPopup.load(url, '', function () {
                $modelPopup.modal();
                $("input[id=SC_SESSION_DATE]").datepicker({
                    orientation: 'bottom',
                    keepOpen: false,
                    format: "yyyy-mm-dd"
                });
                $('input[name=sc_session_time]').timepicker({
                    showSeconds: true,
                    showMeridian: false,
                });
                $(".modal_close").on("click",function(){
                    $modelPopup.modal('toggle')
                });
                $("#BTN_SAVE_SESSION").on('click', sessions_module.SaveSession);

            });
        }, 1000);
    },
    DeleteSession: function() {
        var base_url   = $('input[name=base_url]').val();
        var session_id = $(this).data('session_id');
        var _token     = $('input[name=_token]').val();
        
        bootbox.confirm('Are You Sure You Want to delete ?', function (result) {
            if (result == true)
            {
                $.ajax({
                    url: base_url + "/request/DeleteSession",
                    data: {_token: _token, session_id: session_id},
                    dataType: "json",
                    type: "POST",
                    success: function (response) {

                        window.parent.bootbox.alert(response.error_msg);
                        sessions_module.DisplaySessions();
                    }
                });
            }
        });
    },
    DisplayListSessions: function () {
        var project_id = $("#select_project").val();
        var class_id = $("#select_group").val();
        
        if (project_id == '' || class_id == '') {
            return;
        }
        
        $("#DISPLAY_TYPE").val('list');
        sessions_module.DisplaySessions();
    },
    DisplayCalendarSessions: function () {
        var project_id = $("#select_project").val();
        var class_id = $("#select_group").val();
        
        if (project_id == '' || class_id == '') {
            return;
        }
        
        $("#DISPLAY_TYPE").val('calendar');
        sessions_module.DisplaySessions();
    },
    DisplayTraineesAttendance: function () {
        
        var base_url = $('input[name=base_url]').val();
        var _token = $('input[name=_token]').val();
        var class_id = $('#class_id').val();
        var session_id = $('#session_id').val();
        var params = {class_id: class_id, session_id: session_id, _token: _token};
        $.ajax
                ({
                    url: base_url + "/request/DisplayListClassTrainees",
                    data: params,
                    dataType: "Json",
                    type: "POST",
                    success: function (response) {
                        $('.ListAttendances').html(response.display);
                    }
                });
    },
    SaveClassAttendances: function () {
        var base_url = $('input[name=base_url]').val();
        var _token = $('input[name=_token]').val();
        var from_params = $("#FRM_TRAINEES_ATTENDANCES").serialize();
        $.ajax
                ({
                    url: base_url + "/request/SaveTraineeAttendances",
                    data: from_params,
                    dataType: "Json",
                    type: "POST",
                    success: function (response) {
                        bootbox.alert(response.error_msg);
                        sessions_module.DisplayTraineesAttendance();
                    }
                });
    }
};
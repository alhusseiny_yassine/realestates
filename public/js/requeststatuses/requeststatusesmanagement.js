/**
 *
 */


$(function(){
	request_statuses_module.displayListRequestStatuses();
	$("#BTN_ADD_REQUEST_STATUS").on('click',request_statuses_module.AddNewRequestStatusForm);
	$(".ListRequestStatusesGird").on('click',"a[id*=EDIT_REQUEST_STATUS_]",request_statuses_module.DisplayEditRequestStatusForm);
	$(".ListRequestStatusesGird").on('click',"a[id*=DELETE_REQUEST_STATUS_]",request_statuses_module.DeleteRequestStatusData);
        $("#PRINT_LIST").on('click',request_statuses_module.DisplayPrintList);
        $("#EXPORT_TO_EXCELL").on('click',request_statuses_module.ExportToExcell);
})
/**
 *
 */


$(function(){
	course_type_module.displayListCourseTypes();
	$("#BTN_ADD_COURSE_TYPE").on('click',course_type_module.AddNewCourseTypeForm);
	$(".ListCourseTypesGird").on('click',"a[id*=EDIT_COURSE_TYPE_]",course_type_module.DisplayEditCourseTypeForm);
	$(".ListCourseTypesGird").on('click',"a[id*=DELETE_COURSE_TYPE_]",course_type_module.DeleteCourseTypeData);
})
$(function () {

    $('#s_date').datetimepicker({
        format: "YYYY-MM-DD HH:mm:ss",
        locale: "en"
    });
    $(".selectpicker").selectpicker({
        liveSearch: "true"
    });
    $("#listbeneficiaryprojects").on("change","#fk_project_id",function(){
        var benf_id = $(this).val();
        var type    = $(this).data("type");
        surveys_module.AddObjectDropdown(benf_id,type);
    });
    $("#listprojectcourses").on("change","#fk_course_id",function(){
        var benf_id = $(this).val();
        var type    = $(this).data("type");
        surveys_module.AddObjectDropdown(benf_id,type);
    });
    $("#ADD_SURVEY").on("click", function () {
        surveys_module.SaveSurvey();
    });
    $("#EDIT_SURVEY").on("click", function () {
        surveys_module.SaveSurvey();
    });

});

/**
 *
 */


$(function(){
	course_category_module.displayListCourseCategories();
	$("#BTN_ADD_COURSE_CATEGORY").on('click',course_category_module.AddNewCourseCategoryForm);
	$(".ListCourseCategoriesGird").on('click',"a[id*=EDIT_COURSE_CATEGORY_]",course_category_module.DisplayEditCourseCategoryForm);
	$(".ListCourseCategoriesGird").on('click',"a[id*=DELETE_COURSE_CATEGORY_]",course_category_module.DeleteCourseCategoryData);
})
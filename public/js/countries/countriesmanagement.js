/**
 * 
 */


$(function(){
	countries_module.displayListCountries();
	$("#BTN_ADD_COUNTRY").on('click',countries_module.AddNewCountryForm);
	$(".ListCountriesGird").on('click',"a[id*=EDIT_COUNTRY_]",countries_module.DisplayEditCountryForm);
	$(".ListCountriesGird").on('click',"a[id*=DELETE_COUNTRY_]",countries_module.DeleteCountryData);
})
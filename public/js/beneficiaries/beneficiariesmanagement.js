/**
 *
 */


$(function(){
	benf_module.displayListBenf();
	$('input[name=sb_beneficiary_name]').on('keyup',benf_module.displayListBenf);
	$('input[name=sb_beneficiary_arabic_name]').on('keyup',benf_module.displayListBenf);
	$('input[name=sb_beneficiary_phone]').on('keyup',benf_module.displayListBenf);
	$('input[name=sb_beneficiary_fax]').on('keyup',benf_module.displayListBenf);
	$('#PRINT_LIST').on('click',benf_module.printBenf);
	$("#BTN_ADD_BENEFICIARIES").on('click',benf_module.AddNewBenficiaryForm);
	$(".ListBenefciariesGird").on('click',"a[id*=EDIT_BENF_]",benf_module.DisplayEditBenficiaryForm);
	$(".ListBenefciariesGird").on('click',"a[id*=DELETE_BENF_]",benf_module.DeleteBenfData);
})
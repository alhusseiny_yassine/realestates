/**
 *
 */

$(function(){
	$("input[name=pp_project_start_date]").datepicker({
		startDate :'+1d',
		keepOpen : false,
		orientation : 'bottom',
		format : "yyyy-mm-dd"
	});
	   $("input[name=pp_project_end_date]").datepicker({
		   startDate :'+1d',
			orientation : 'bottom',
			keepOpen : false,
			format : "yyyy-mm-dd"
		});
	   $("input[name=pp_contract_date]").datepicker({ 
			orientation : 'bottom',
			keepOpen : false,
			format : "yyyy-mm-dd"
		});

	   $('#BTN_SAVE_PROJECT').on('click',projects_module.SaveProjectInfo);
	   $('.bs-select').selectpicker({
	        iconBase: 'fa',
	        tickIcon: 'fa-check'
	    });
})
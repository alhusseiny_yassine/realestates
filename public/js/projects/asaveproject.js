/**
 *
 */

$(function(){
	$("input[name=pp_project_start_date]").datepicker({ 
		keepOpen : false,
		orientation : 'bottom',
		format : "yyyy-mm-dd"
	});
	$("input[name=pp_project_end_date]").datepicker({ 
		orientation : 'bottom',
		keepOpen : false,
		format : "yyyy-mm-dd"
	});
	   $("input[name=pp_contract_date]").datepicker({ 
			orientation : 'bottom',
			keepOpen : false,
			format : "yyyy-mm-dd"
		});
	   $('.timepicker-24').each(function(){
		   $('.timepicker-24').timepicker({
	           autoclose: true,
	           minuteStep: 5,
	           showSeconds: false,
	           showMeridian: false
	       });
	   })
	   
	   $('#BTN_SAVE_PROJECT').on('click',projects_module.SaveAdminProjectInfo)
})
/**
 * 
 */
var ComponentsBootstrapMultiselect = function () {

    return {
        //main function to initiate the module
        init: function () {
        	$('.mt-multiselect').each(function(){
        		var btn_class = $(this).attr('class');
        		var clickable_groups = ($(this).data('clickable-groups')) ? $(this).data('clickable-groups') : false ;
        		var collapse_groups = ($(this).data('collapse-groups')) ? $(this).data('collapse-groups') : false ;
        		var drop_right = ($(this).data('drop-right')) ? $(this).data('drop-right') : false ;
        		var drop_up = ($(this).data('drop-up')) ? $(this).data('drop-up') : false ;
        		var select_all = ($(this).data('select-all')) ? $(this).data('select-all') : false ;
        		var width = ($(this).data('width')) ? $(this).data('width') : '' ;
        		var height = ($(this).data('height')) ? $(this).data('height') : '' ;
        		var filter = ($(this).data('filter')) ? $(this).data('filter') : false ;

        		// advanced functions
        		var onchange_function = function(option, checked, select) {
        			projects_module.DisplayListAdminProjects();
	            }
	            var dropdownshow_function = function(event) {
	               
	            }
	            var dropdownhide_function = function(event) {
	          
	            }

	            // init advanced functions
	            var onchange = ($(this).data('action-onchange') == true) ? onchange_function : '';
	            var dropdownshow = ($(this).data('action-dropdownshow') == true) ? dropdownshow_function : '';
	            var dropdownhide = ($(this).data('action-dropdownhide') == true) ? dropdownhide_function : '';

	            // template functions
	            // init variables
	            var li_template;
	            if ($(this).attr('multiple')){
	            	li_template = '<li class="mt-checkbox-list"><a href="javascript:void(0);"><label class="mt-checkbox"> <span></span></label></a></li>';
        		} else {
        			li_template = '<li><a href="javascript:void(0);"><label></label></a></li>';
         		}

	            // init multiselect
        		$(this).multiselect({
        			enableClickableOptGroups: clickable_groups,
        			enableCollapsibleOptGroups: collapse_groups,
        			disableIfEmpty: true,
        			enableFiltering: filter,
        			includeSelectAllOption: select_all,
        			dropRight: drop_right,
        			buttonWidth: width,
        			maxHeight: height,
        			onChange: onchange,
        			onDropdownShow: dropdownshow,
        			onDropdownHide: dropdownhide,
        			buttonClass: btn_class,
        			//optionClass: function(element) { return "mt-checkbox"; },
        			//optionLabel: function(element) { console.log(element); return $(element).html() + '<span></span>'; },
        			/*templates: {
		                li: li_template,
		            }*/
        		});   
        	});
         	
        }
    };

}();



$(function(){
	projects_module.DisplayListAdminProjects();
	   ComponentsBootstrapMultiselect.init(); 
	$("button[name=btn_advanced_search]").on('click',projects_module.DisplayListAdminProjects);
	$("#PRJ_PROJECT_STATUS").on('change',projects_module.DisplayListAdminProjects);
	$("button[name=btn_reset_search]").on('click',function(){
		$("#PRJ_PROJECT_STATUS").val('0');
		$("#PRJ_PROJECT_BENEFICIARY").val('0');
		$("#PRJ_PROJECT_TITLE").val('');
		projects_module.DisplayListAdminProjects();
	});
        $(".ChangeProjectsStatus").on('click',projects_module.OpenChangeStatusPopup);
	$("#BTN_ADD_ADMIN_PROJECT").on('click',projects_module.AddNewAdminProjectInformation);
	$("#BTN_ADD_ADMIN_PROJECT_BOTTOM").on('click',projects_module.AddNewAdminProjectInformation);
	$('.ListProjects').on('click',"a[id*=EDIT_PROJECT_]",projects_module.EditAdminProjectPage);
	$('.ListProjects').on('click',"a[id*=DELETE_PROJECT_]",projects_module.AdminDeleteProjectInfo);
	$('.ListProjects').on('click',"a[id*=VIEW_PROJECT_]",projects_module.ViewProjectInformation);
	$('.ListProjects').on('click',"a[id*=CONFIG_PROJECT_]",projects_module.AdminCreateConfigurationManagement);
	
 
})
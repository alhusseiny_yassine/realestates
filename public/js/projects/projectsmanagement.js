/**
 *
 */


$(function(){
	projects_module.DisplayListProjects();
	$('#BTN_SEARCH_PROJECT').on('click',projects_module.DisplayListProjects);
	$('#BTN_CREATE_PROJECT').on('click',projects_module.OpenCreateProjectPage);
	$('#BTN_CREATE_PROJECT_BOTTOM').on('click',projects_module.OpenCreateProjectPage);
	$('#CK_ALL_PROJECT').on('click',projects_module.CheckAllProjects);
	$('.ChangeProjectStatus').on('click',projects_module.OpenChangeStatusPopup);
	$('.SearchResults').on('click',"a[id*=EDIT_PROJECT_]",projects_module.OpenEditProjectPage);
	$('.SearchResults').on('click',"a[id*=DELETE_PROJECT_]",projects_module.DeleteProjectInfo);
	$('.SearchResults').on('click',"a[id*=CONFIG_PROJECT_]",projects_module.CreateConfigurationManagement);
})
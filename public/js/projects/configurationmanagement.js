/**
 *
 */

$(function(){
	projects_module.DisplayConfigurationPage();
	$("#BTN_SAVE_INFO").on('click',projects_module.SaveProjectConfigurationInfo);
	$(".ConfigurationManager").on('change',"#PROJECT_COURSE",projects_module.DisplayProjectCourseForm);
	$(".ConfigurationManager").on('change',"#SL_PROJECT_COURSE",projects_module.DisplayListGroups);
	$(".ConfigurationManager").on('click',"#PROJECT_COURSE_TRAINEE",projects_module.OpenProjectTrainees);
	$(".ConfigurationManager").on('click',"#BTN_SAVE_COURSES",projects_module.SaveCourseInformation);
	$(".ConfigurationManager").on('click',"#BTN_ADD_CLASS",projects_module.AddNewProjectClass);
	$(".ConfigurationManager").on('click',".EditGroup",projects_module.EditGroupClass);
	$(".ConfigurationManager").on('click',".DeleteGroup",projects_module.DeleteGroupClass);
	$(".ConfigurationManager").on('click',".GroupInfo",projects_module.OpenGroupInfo);
	$(".ConfigurationManager").on('click',".ChangeDisplay",projects_module.ChangeDisplayGroup);
	$(".ConfigurationManager").on('click',"a[id*=EDIT_GROUP_]",projects_module.EditGroupClass);
	$(".ConfigurationManager").on('click',"a[id*=DELETE_GROUP_]",projects_module.DeleteGroupClass);
	$(".ConfigurationManager").on('click',"a[id*=EDIT_TRAINEE_GROUP_]",projects_module.EditTraineesGroup);
	$(".ConfigurationManager").on('click',".DeleteMultipleGroup",projects_module.DeleteMultipleGroup);
	$(".mt-step-col").on('click',function(){
		var id = $(this).attr('id');
		$("#TAB_NAME").val(id);
		projects_module.ChangeProjectPointer(id);
		projects_module.DisplayConfigurationPage();
	});

});
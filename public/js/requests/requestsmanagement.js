/**
 *
 */

$(function(){

	request_module.DisplayRequestsDashboard();
	request_module.DisplayListMyRequests();
	request_module.DisplayListAllRequests();


	$('.RejectRequests').on('click',request_module.OpenRejectionRequestPopup)
	$('.ApproveRequests').on('click',request_module.ApproveRequests)
	$('.ChangeRequestStatus').on('click',request_module.ChangeRequestStatusPopup);
	$('button[name=btn_search_my_requests]').on('click',request_module.DisplayListMyRequests);
	$('button[name=btn_reset_myrequest]').on('click',request_module.ResetMyRequestsSearchForm);


	//always open by default the first tab of the tabs in requests management
	$(".tabbable-line").find('li:first').addClass('active');
	$(".tab-content").find('.tab-pane:first').addClass('active');
        
        $("ul.nav-tabs > li ").on("click",function(){
            var link   = $(this).find('a');
            var tab_id = link.attr("href");
            
            $("ul.nav-tabs > li").removeClass('active');
            $(this).addClass('active');
            $('.tab-pane').removeClass('active');
            $(tab_id).addClass('active');
        });

});
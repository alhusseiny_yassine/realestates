/**
 *
 */



$(function(){
	$('textarea[id=CM_MEDIA_CAPTION]').ckeditor();
	 $("#BTN_EDIT_IMAGE_ALBUM").on('click',function(){
		 var base_url = $('#BASE_URL').val();
		 var _token = $('input[name=_token]').val();
		 var page_id = $('#PAGE_ID').val();
	        var slider_id = $('#SLIDER_ID').val();
	        var str_params = $("#FORM_EDIT_IA_INFO").serialize();
	        str_params +=  "&_token=" + _token;
	         $.ajax
	        ({
	            url : base_url + "/request/SaveImage",
	            data : str_params,
	            dataType : "json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  window.location.href = base_url + "/administrator/page/slideimages/" + slider_id;
	              }
	            }
	        });
	    });


	 var url = $('#fileupload').data('url');
	    $('#fileupload').fileupload({
	        url: url,
	        dataType: 'json',
	        autoUpload: true,
	        maxFileSize: 5000000, // 5 MB
	        // Enable image resizing, except for Android and Opera,
	        // which actually support image resizing, but fail to
	        // send Blob objects via XHR requests:
	        disableImageResize: /Android(?!.*Chrome)|Opera/
	            .test(window.navigator.userAgent),
	        previewMaxWidth: 100,
	        previewMaxHeight: 100,
	        previewCrop: true
	    }).on('fileuploadadd', function (e, data) {
	        data.context = $('<div/>').appendTo('#files');
	        $.each(data.files, function (index, file) {
	            var node = $('<p/>')
	                    .append($('<span/>').text(file.name));
	            node.appendTo(data.context);
	        });

			$('#progress .progress-bar').css(
	            'width',
	            '0%'
	        );
	    }).on('fileuploadprocessalways', function (e, data) {
	        var index = data.index,
	            file = data.files[index],
	            node = $(data.context.children()[index]);
	        if (file.preview) {
	            node
	                .prepend('<br>')
	                .prepend(file.preview);
	        }
	        if (file.error) {
	            node
	                .append('<br>')
	                .append($('<span class="text-danger"/>').text(file.error));
	        }
	        if (index + 1 === data.files.length) {
	            data.context.find('button')
	                .text('Upload')
	                .prop('disabled', !!data.files.error);
	        }
	    }).on('fileuploadprogressall', function (e, data) {
	        var progress = parseInt(data.loaded / data.total * 100, 10);
	        $('#progress .progress-bar').css(
	            'width',
	            progress + '%'
	        );
	    }).on('fileuploaddone', function (e, data) {
			$("#MEDIA_PICTURE").attr("src",data.result.files.thumbnailUrl);

	    }).on('fileuploadfail', function (e, data) {
	        alert('File upload failed.');
	    }).prop('disabled', !$.support.fileInput)
	        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
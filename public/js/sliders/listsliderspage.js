modules = {
    displayListSliders : function()
    {
            var base_url = $('#BASE_URL').val();
            var _token = $('input[name=_token]').val(); 
            $.ajax
            ({
                url : base_url + "/administrator/displayListSliders",
                data : {_token : _token },
                dataType : "html",
                type : "POST",
                success : function(response){
                    $('.ListSlidersGrid').html(response);
                }
            });

    },
    AddNewSlider : function(){
    	var cms_page = $('input[id=CMS_PAGE]').val();
		var base_url = $("#BASE_URL").val();
		var url = base_url + "/administrator/addSliderInfo";
		$.colorbox({href: url ,width:"680",height:"750",iframe : true,escKey: false,overlayClose: false});
    },
    EditSliderInfo : function(){
	      var base_url 	= $("#BASE_URL").val();
	      var cs_id 	= $(this).parents('tr').data('cs_id');
	      var url = base_url + "/administrator/editSliderInfo/" + cs_id;
	      $.colorbox({href: url ,width:"680",height:"750",iframe : true,escKey: false,overlayClose: false});
    },
    DeleteSliderInfo : function(){
    	if(!confirm('Are you sure do you want to delete ?'))
	           return false;
	      var cs_id 	= $(this).parents('tr').data('cs_id');
	      var base_url 	= $('#BASE_URL').val();
	      var _token = $('input[name=_token]').val();
	        var str_params ={cs_id : cs_id , _token : _token};
	         $.ajax
	        ({
	            url : base_url + "/administrator/DeleteSliderInfo",
	            data : str_params,
	            dataType : "Json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  $.alert({
	                      title: 'Alert!',
	                      content: response.error_msg,
	                      confirm: function(){
	                    	 modules.displayListSliders();
	                      }
	                  });
	              }
	            }
	        });
    }

};
response = {

};

$(function(){
	modules.displayListSliders();
	$("#BTN_ADD_LIDERS").on('click',modules.AddNewSlider);
	$(".ListSlidersGrid").on('click',"img[id*=EDIT_SLIDE_]",modules.EditSliderInfo);
	$(".ListSlidersGrid").on('click',"img[id*=DELETE_SLIDE_]",modules.DeleteSliderInfo);

});
/**
 *
 */


$(function(){
	var url = $('#fileupload').attr('action');
	uploadButton = $('<button/>')
    .addClass('btn btn-primary')
    .prop('disabled', true)
    .text('Processing...')
    .on('click', function () {
        var $this = $(this),
            data = $this.data();
        $this
            .off('click')
            .text('Abort')
            .on('click', function () {
                $this.remove();
                data.abort();
            });
        data.submit().always(function () {
            $this.remove();
        });
    });
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        autoUpload: false,
        maxFileSize: 5000000, // 5 MB
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
        data.context = $('<div/>').appendTo('#files');
        $.each(data.files, function (index, file) {
            var node = $('<p/>')
                    .append($('<span/>').text(file.name));
            if (!index) {
                node
                    .append('<br>')
                    .append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
        file = data.files[index],
        node = $(data.context.children()[index]);
    if (file.preview) {
        node
            .prepend('<br>')
            .prepend(file.preview);
    }
    if (file.error) {
        node
            .append('<br>')
            .append($('<span class="text-danger"/>').text(file.error));
    }
    if (index + 1 === data.files.length) {
        data.context.find('button.start')
            .prop('disabled', !!data.files.error);
    }
}).on('fileuploadprogressall', function (e, data) {
    var progress = parseInt(data.loaded / data.total * 100, 10);
    $('#progress .progress-bar').css(
        'width',
        progress + '%'
    );
}).on('fileuploaddone', function (e, data) {
    	//

		var htm = $("#MEDIA_AREA").html();
		$("#MEDIA_AREA").html(htm + "<img src='" + data.result.image_url + "' height='72' id='MEDIA_" + data.result.cm_id + "' />");

    }).on('fileuploadfail', function (e, data) {
        alert('File upload failed.');
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

});

$('.files').on('click','[id*=DISPLAY_FORM_]',function(){
	var cm_id = $(this).data('cm_id');
	var base_url = $("#BASE_URL").val();
	var _token = $('input[name=_token]').val();
	 $.ajax
    ({
        url : base_url + "/administrator/GetMediaInformation/" + cm_id,
        data : { _token : _token },
        dataType : "json",
        type : "POST",
        success : function(response){
        	var listItem = tmpl('form_template', { cm_id : response.cm_id  , cm_media_title : response.cm_media_title , cm_media_caption : response.cm_media_caption });
        	$('#FORM_INFORMATION').html(listItem);
        }
    });
});
$('.delete').on('click',function(){
	$('#FORM_INFORMATION').html('');
});
$('.files').on('click','.delete',function(){
$('#FORM_INFORMATION').html('');
});
$('#FORM_INFORMATION').on('click','button[id=BTN_SAVE_INFO]',function(){
	var str_params = $('#fileupload').serialize();
	var base_url = $("#BASE_URL").val();
	 $.ajax
     ({
         url : base_url + "/administrator/SaveMediaInformation",
         data : str_params,
         dataType : "json",
         type : "POST",
         success : function(response){
        	 $.alert({
                 title: 'Alert!',
                 content: response.error_msg
             });
         }
     });
});
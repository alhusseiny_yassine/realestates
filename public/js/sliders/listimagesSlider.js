/**
 *
 */

function DisplayImages()
{
	var _token 			= $("input[name=_token]").val();
	var base_url 		= $("#BASE_URL").val();
	var number_pages 	= ( $('input[id=NUMBER_PAGES]').size() > 0 ) ? $('input[id=NUMBER_PAGES]').val() : 0;
	var page_number 	= ( $('input[id=PAGE_NUMBER]').size() > 0 ) ? $('input[id=PAGE_NUMBER]').val() : 1;
    var cms_slide_id 		= $('input[id=CMS_SLIDE_ID]').val();
	$.ajax
    ({
        url : base_url + "/request/displaylistImagesSlider",
        data : { _token : _token , images_type : "slider_images"  , cms_slide_id : cms_slide_id  , page_number : page_number , number_pages : number_pages },
        dataType : "html",
        type : "POST",
        success : function(response){
        	$('.ListAlbumsGrid').html(response);
        	var number_pages = ( $('input[id=NUMBER_PAGES]').size() > 0 ) ? $('input[id=NUMBER_PAGES]').val() : 0;
        	$('#lst-pagination').twbsPagination({
                totalPages: number_pages,
                visiblePages: 7,
                onPageClick: function (event, page) {
                	$('input[id=PAGE_NUMBER]').val(page);
                	DisplayImages();
                }
            });
        }
    });
}
function EditImages()
{
	var selected_ids = '';
	if($('input[name*=a_media_]:checked').size() == 0)
	{
		alert('Please select at least one image to edit information');
		return false;
	}
	$('input[name*=a_media_]:checked').each(function(n){
		if(n == 0)
		{
			selected_ids = $(this).parents('tr').data('cm_id');
		}
		else
		{
			selected_ids = selected_ids + "," + $(this).parents('tr').data('cm_id');
		}
	});
}


function DeleteImages()
{
	var selected_ids = '';
	if($('input[name*=a_media_]:checked').size() == 0)
	{
		alert('Please select at least one image to Delete it');
		return false;
	}
	$('input[name*=a_media_]:checked').each(function(n){
		if(n == 0)
		{
			selected_ids = $(this).parents('tr').data('cm_id');
		}
		else
		{
			selected_ids = selected_ids + "," + $(this).parents('tr').data('cm_id');
		}
	});

	if(!confirm('Are you sure do you want to delete ?'))
        return false;
   var base_url 	= $('#BASE_URL').val();
   var _token = $('input[name=_token]').val();
     var str_params ={selected_ids : selected_ids , _token : _token};
      $.ajax
     ({
         url : base_url + "/DeleteImageAlbum/" + selected_ids,
         data : str_params,
         dataType : "Json",
         type : "POST",
         success : function(response){
           if(response.is_error == 0)
           {
         	  $.alert({
                   title: 'Alert!',
                   content: response.error_msg,
                   confirm: function(){
                 	  DisplayImages();
                   }
               });
           }
         }
     });

}
$(function(){
	DisplayImages();

	$("#SLC_ACTION").on('change',function(){

		if($(this).val() == 0)
			return false;

		var action  = $(this).val();
		switch(action)
		{
			case "edit_image":
			{
				EditImages();
			}
			break;
			case "delete_image":
			{
				DeleteImages();
			}
			break;
		}

		$(this).val(0);
	});
	$("#BTN_ADD_IMAGES").on('click',function(){
	    var cms_slide_id = $('input[id=CMS_SLIDE_ID]').val();
		var base_url = $("#BASE_URL").val();
		window.location.href = base_url + "/AddImagesSlider/" +  cms_slide_id;
	});


	$(".ListAlbumsGrid").on('click',"input[id=CK_ALL_MEDIA]",function(){
		if($('input[id=CK_ALL_MEDIA]').attr('checked')== 'checked')
		{
			$('input[name*=a_media_]').each(function(n){
				$(this).attr('checked','checked');
			})
		}
		else
		{
			$('input[name*=a_media_]').each(function(n){
				$(this).removeAttr('checked');
			})
		}

	});
	$(".ListAlbumsGrid").on('click',"a[id*=EDIT_IMAGE_]",function(){
	      var cm_id = $(this).parents('tr').data('cm_id');
	      var base_url = $("#BASE_URL").val();
	      window.location.href = base_url + "/administrator/EditImageSlider/" + cm_id;
	});

	$(".ListAlbumsGrid").on('click',"a[id*=DELETE_IMAGE_]",function(){
	      if(!confirm('Are you sure do you want to delete ?'))
	           return false;
	      var cm_id 	= $(this).parents('tr').data('cm_id');
	      var base_url 	= $('#BASE_URL').val();
	      var _token = $('input[name=_token]').val();
	        var str_params ={cm_id : cm_id , _token : _token};
	         $.ajax
	        ({
	            url : base_url + "/administrator/DeleteMedia/" + cm_id,
	            data : str_params,
	            dataType : "Json",
	            type : "DELETE",
	            success : function(response){
	            	 DisplayImages();
	            }
	        });
	});
});
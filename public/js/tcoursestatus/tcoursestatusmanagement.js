/**
 *
 */


$(function(){
	project_statuses_module.displayListProjectStatuses();
	$("#BTN_ADD_TCOURSE_STATUS").on('click',project_statuses_module.AddNewProjectStatusForm);
	$(".ListTCourseStatusesGird").on('click',"a[id*=EDIT_TCOURSE_STATUS_]",project_statuses_module.DisplayEditProjectStatusForm);
	$(".ListTCourseStatusesGird").on('click',"a[id*=DELETE_TCOURSE_STATUS_]",project_statuses_module.DeleteProjectStatusData);
})
/**
 *
 */


$(function(){
	donor_module.displayListDonors();
	$("#BTN_ADD_DONOR").on('click',donor_module.AddNewDonorForm);
	$(".ListDonorsGird").on('click',"a[id*=EDIT_DONOR_]",donor_module.DisplayEditDonorForm);
	$(".ListDonorsGird").on('click',"a[id*=DELETE_DONOR_]",donor_module.DeleteDonorData);
})
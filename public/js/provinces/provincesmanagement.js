/**
 *
 */


$(function(){
	provinces_module.displayListProvinces();
	$("#BTN_ADD_PROVINCE").on('click',provinces_module.AddNewProvinceForm);
	$(".ListProvincesGird").on('click',"a[id*=EDIT_PROVINCE_]",provinces_module.DisplayEditProvinceForm);
	$(".ListProvincesGird").on('click',"a[id*=DELETE_PROVINCE_]",provinces_module.DeleteProvinceData);
})
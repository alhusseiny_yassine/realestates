/**
 *
 */


$(function(){
	project_statuses_module.displayListProjectStatuses();
	$("#BTN_ADD_PROJECT_STATUS").on('click',project_statuses_module.AddNewProjectStatusForm);
	$(".ListProjectTypesGird").on('click',"a[id*=EDIT_PROJECT_STATUS_]",project_statuses_module.DisplayEditProjectStatusForm);
	$(".ListProjectTypesGird").on('click',"a[id*=DELETE_PROJECT_STATUS_]",project_statuses_module.DeleteProjectStatusData);
        $("#PRINT_LIST").on('click',project_statuses_module.DisplayPrintList);
        $("#EXPORT_TO_EXCELL").on('click',project_statuses_module.ExportToExcell);
})
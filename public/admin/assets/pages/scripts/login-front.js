var Login = function() {

    var handleLogin = function() {

        $('.login-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                username: {
                    required: true
                },
                password: {
                    required: true
                },
                remember: {
                    required: false
                }
            },

            messages: {
                username: {
                    required: "Username is required."
                },
                password: {
                    required: "Password is required."
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                $('.alert-danger', $('.login-form')).show();
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            submitHandler: function(form) {
            	var str_params = $('.login-form').serialize();
            	var base_url = $('#BASE_URL').val();
            	$.ajax
    	        ({
    	            url : base_url + "/omsar/login",
    	            data : str_params,
    	            dataType : "json",
    	            type : "POST",
    	            success : function(response){
    	              if(response.is_error == 0)
    	              {

    	            	 window.location.href = base_url + "/omsar/dashboard"
    	              }
    	              else
	            	  {
    	            	  bootbox.alert(response.error_msg);
	            	  }
    	            }
    	        });
            }
        });


        $('.registration-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
            	ur_username: {
            		required: true
            	},
            	u_fullname: {
            		required: true
            	},
            	ur_beneficiary_id: {
            		required: true
            	},
            	ur_coordinator_code: {
            		required: true
            	},
            	ur_email: {
            		email : true,
                	 minlength: 5,
                    required: true
                },
                ur_password: {
           		 minlength: 8,
           		 required: true
	           	 },
	           	ur_retype_password : {
                    minlength: 8,
                    equalTo: "#UR_PASSWORD",
                    required: true
                  },
                remember: {
                    required: false
                }
            },

            messages: {

            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                $('.alert-danger', $('.login-form')).show();
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            submitHandler: function(form) {
            	var str_params = $('#REGISTRATION_FORM').serialize();
            	var base_url = $('#BASE_URL').val();
            	$.ajax
    	        ({
    	            url : base_url + "/omsar/registration",
    	            data : str_params,
    	            dataType : "json",
    	            type : "POST",
    	            success : function(response){
    	              if(response.is_error == 0)
    	              {
    	            	  bootbox.alert(response.error_msg);
    	              }
    	              else
	            	  {
    	            	  bootbox.alert(response.error_msg);
	            	  }
    	            }
    	        });
            }
        });

        $('.login-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.login-form').validate().form()) {
                    $('.login-form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });

        $('.forget-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.forget-form').validate().form()) {
                    $('.forget-form').submit();
                }
                return false;
            }
        });

        $('#forget-password').click(function(){
        	$('.login-form').hide();
        	$('.registration-form').hide();
        	$('.forget-form').show();
        });
        $('#user-registration').click(function(){
        	$('.login-form').hide();
            $('.registration-form').show();
            $('.forget-form').hide();
        });

        $('#back-btn').click(function(){
        	$('.login-form').show();
        	$('.forget-form').hide();
        	$('.registration-form').hide();
        });
        $('#back-reg-btn').click(function(){
            $('.login-form').show();
            $('.forget-form').hide();
            $('.registration-form').hide();
        });
    }




    return {
        //main function to initiate the module
        init: function() {
            handleLogin();
            // init background slide images
            $('.login-bg').backstretch(slider_array, {
                  fade: 1000,
                  duration: 8000
                }
            );
            $("select").select2({
                width: null
            });
            $('.forget-form').hide();
            $('.registration-form').hide();

            $('input[name=ur_username]').on('blur',function(){
            	var base_url = $('#BASE_URL').val();
        		var _token = $('input[name=_token]').val();
        		var user_name = $('#UR_USERNAME').val();
        		if(user_name.length == 0)
        			return false;
                var params = {_token : _token , user_name : user_name };
                 $.ajax
                ({
                    url : base_url + "/request/checkUserName",
                    data : params,
                    dataType : "json",
                    type : "POST",
                    success : function(response){
                    	$('#UR_USERNAME').parents('.form-group').removeClass('has-error');
                    	$('#UR_USERNAME').parents('.form-group').find('.help-block').remove();
                    	$('#UR_USERNAME').removeClass('SuccessCheck')
                    	var field_container = $('#UR_USERNAME').parents('.form-group');
                    	if(response.exist == 1)
                        {
                    		$('#UR_USERNAME').parents('.form-group').addClass('has-error');
                    		$('<span />',{
                    			'class' : 'help-block help-block-error'
                    		}).appendTo(field_container);
                    		$('#UR_USERNAME').parents('.form-group').find('.help-block').html('User Already Exist');
                        }
                    	else
                		{
                    		$('#UR_USERNAME').addClass('SuccessCheck');
                		}
                    }
                });
            })
        }

    };

}();
var module = {
		SendResetPassworEmail : function(){
			return module.SendResetPassworEmailHandler();
		},
		SendResetPassworEmailHandler : function(){
			var ForgotPasswordForm = $('#FRM_FORGOT_PASSWORD');
	        var error3 = $('.alert-danger', ForgotPasswordForm);
	        var success3 = $('.alert-success', ForgotPasswordForm);

	        ForgotPasswordForm.validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block help-block-error', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "", // validate all fields including form hidden input
	            rules: {
	            	fuser_email: {
	            		required: true
	            	}
	            },

	            messages: { // custom messages for radio buttons and checkboxes

	            },
	            errorPlacement: function (error, element) { // render error placement for each input type
	                if (element.parent(".input-group").size() > 0) {
	                    error.insertAfter(element.parent(".input-group"));
	                } else if (element.attr("data-error-container")) {
	                    error.appendTo(element.attr("data-error-container"));
	                } else if (element.parents('.radio-list').size() > 0) {
	                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
	                } else if (element.parents('.radio-inline').size() > 0) {
	                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
	                } else if (element.parents('.checkbox-list').size() > 0) {
	                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
	                } else if (element.parents('.checkbox-inline').size() > 0) {
	                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
	                } else {
	                    error.insertAfter(element); // for other inputs, just perform default behavior
	                }
	            },
	            invalidHandler: function (event, validator) { //display error alert on form submit
	                success3.hide();
	                error3.show();
	            },
	            success: function (label) {
	                label
	                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
	            },
	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            unhighlight: function (element) { // revert the change done by hightlight
	                $(element)
	                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
	            },
	            submitHandler: function (form) {
	               success3.show();
	               error3.hide();
	               var base_url = $('#BASE_URL').val();
	   	        var _token = $('input[name=_token]').val();
	   	        var str_params = $("#FRM_FORGOT_PASSWORD").serialize();
	   	         $.ajax
	   	        ({
	   	            url : base_url + "/request/SendResetAccount",
	   	            data : str_params,
	   	            dataType : "json",
	   	            type : "POST",
	   	            success : function(response){
	   	            	bootbox.alert(response.error_msg);
	   	            }
	   	        });
	            }

	        });
		}
};
$(function(){
	Login.init();
	$("#BTN_RESET_PASSWORD").on('click',module.SendResetPassworEmail);
})

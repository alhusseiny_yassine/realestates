/**
 *
 */

$(function(){
	fdashboard_module.DisplayListRequestsNotifications();
	$('.ProjectNotifications').on('click','button[name*=btn_approve_]',fdashboard_module.ApproveUserRequest);
	$('.ProjectNotifications').on('click','button[name*=btn_reject_]',fdashboard_module.RejectUserRequest);
	$('.ProjectNotifications').on('click','button[name*=btn_postpone_]',fdashboard_module.PostPoneUserRequest);
});
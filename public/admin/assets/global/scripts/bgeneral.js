$(function () {
    
    var page = window.location.pathname;
    var page_segment = page.substr(page.lastIndexOf('/') + 1);
    
    $('.sub-menu li a').each(function () {

        if ($(this).data('section') == page_segment)
        {
            $('.active').removeClass('active');
            $(this).parent().parent().parent().addClass('open active');
            $(this).parent().parent().parent().children().children('.arrow').addClass('open');
            $(this).parent().parent().css('display', 'block');
            $(this).parent().addClass('active');
        }

    });
    
    $('select').selectpicker({
        iconBase   : 'fa',
        tickIcon   : 'fa-check',
        liveSearch : true
    });
});
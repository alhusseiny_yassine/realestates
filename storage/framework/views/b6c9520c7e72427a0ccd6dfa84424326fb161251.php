<?php
/***********************************************************
ddproviderlocations.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Apr 6, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

?>

<select name="pg_train_location" id="PG_TRAIN_LOCATION" class="form-control" style="width:100%">
    <option value="0">--Select One--</option>
     <?php $__currentLoopData = $lst_locations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $pl_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
        <option <?php echo e($ini_location == $pl_info->pl_id ? "selected" : ""); ?> value="<?php echo e($pl_info->pl_id); ?>"><?php echo e($pl_info->pl_provider_location); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
</select>
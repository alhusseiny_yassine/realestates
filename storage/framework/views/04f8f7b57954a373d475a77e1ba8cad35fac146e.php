<?php
/***********************************************************
editsessioninformation.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 25, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

?>

<div class="portlet blue box" style="height: 100%;width:800px;">
<div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Edit Session Information </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="javascript:;" class="modal_close"> <i class="fa fa-times" aria-hidden="true"></i> </a>
        </div>
    </div>
    <div class="portlet-body" style="height: 600px;">
        <form name="frm_session_info" id="FRM_SESSION_INFO">
            <span id="hidden_fields">
                  <?php echo csrf_field(); ?>

                  <input type="hidden" name="session_id" value="<?php echo e($session_id); ?>" />
                  <input type="hidden" name="class_id" value="" />
            </span>
            <div class="row">
                 <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label"> Session Code </label>
                        <input type="text" name="sc_session_code" id="SC_SESSION_CODE" class="form-control" readonly="readonly" value="<?php echo e($session_class_info->sc_session_code); ?>" />
                    </div>
                </div>
                 <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label"> Session Type </label>
                        <select name="sc_session_type" id="SC_SESSION_TYPE" class="form-control">
                            <option value="0"> Session Type </option>
                            <?php $__currentLoopData = $list_session_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $sessionType): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                            <option <?php echo e($sessionType->st_id == $session_class_info->sc_session_type ? "selected" : ""); ?> value="<?php echo e($sessionType->st_id); ?>"> <?php echo e($sessionType->st_session_type); ?> </option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        </select>
                    </div>
                </div>
                 <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label"> Session Status </label>
                        <select name="fk_session_status_id" id="FK_SESSION_STATUS_ID" class="form-control">
                            <option value=""> Session Status </option>
                            <?php $__currentLoopData = $sessions_status_info; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $sessionInfo): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                            <option <?php echo e($sessionInfo->ss_id == $session_class_info->fk_session_status_id ? "selected" : ""); ?> value="<?php echo e($sessionInfo->ss_id); ?>"> <?php echo e($sessionInfo->ss_status_name); ?> </option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        </select>
                    </div>
                </div>
                 <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label"> Session Date </label>
                        <input type="text" name="sc_session_date" id="SC_SESSION_DATE" readonly="readonly" class="form-control" value="<?php echo e($session_class_info->sc_session_date); ?>" />
                    </div>
                </div>
                 <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label"> Session TIME </label>
                        <input type="text" name="sc_session_time" maxlength="10" id="SC_SESSION_TIME" class="form-control" value="<?php echo e($session_class_info->sc_session_time); ?>" />
                    </div>
                </div>
                 <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label"> Session Duration </label>
                        <input type="number" min="1" name="sc_session_duration" maxlength="10" id="SC_SESSION_DURATION" class="form-control" value="<?php echo e($session_class_info->sc_session_duration); ?>" />
                    </div>
                </div>
                 <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label"> Session Location </label>

                        <select name="sc_session_location" id="SC_SESSION_LOCATION" class="form-control">
                            <option value=""> Session Status </option>
                            <?php $__currentLoopData = $lst_location; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $locationInfo): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                            <option  <?php echo e($session_class_info->sc_session_location == $locationInfo->pl_id ? "selected" : ""); ?> value="<?php echo e($locationInfo->pl_id); ?>"> <?php echo e($locationInfo->pl_provider_location); ?> </option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        </select>
                    </div>
                </div>
                 <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label"> Session Room Number </label>
                        <input type="number" name="sc_session_room_number" maxlength="500" id="SC_SESSION_ROOM_NUMBER" class="form-control" value="<?php echo e($session_class_info->sc_session_room_number); ?>" />
                    </div>
                </div>
                 <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label"> Session Trainer </label>

                        <select name="fk_session_trainer_id" id="FK_SESSION_TRAINER_ID" class="form-control">
                            <option value=""> Session Trainer </option>
                           <?php
                            foreach ($trainer_info as $index => $info) {
                                ?>
                                    <option <?php echo e($info->id == $session_class_info->fk_session_trainer_id ? "selected" : ""); ?> value="<?php echo e($info->id); ?>"> <?php echo e($info->u_fullname); ?> </option>
                                <?php
                            }
                         ?>


                        </select>
                    </div>
                </div>
                 <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label"> Session Information </label>
                        <textarea name="sc_session_description" style="resize:none" id="sc_session_description" class="form-control"><?php echo e($session_class_info->sc_session_description); ?></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" name="btn_save_info" id="BTN_SAVE_SESSION" class="btn btn-info" >Save Info</button>
                </div>
            </div>
        </form>
    </div>
</div>
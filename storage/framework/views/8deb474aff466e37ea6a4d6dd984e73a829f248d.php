<?php
/* * *********************************************************
  class_pdf.blade.php
  Product :
  Version : 1.0
  Release : 1
  Developed By  : alhusseiny PHP Department

  Page Description :
  {Enter page description Here}
 * ********************************************************* */

?>
<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }

    th, td {
        text-align: left;
        padding: 15px 5px;
        border: 1px solid lightblue;
        text-align: center;
    }

    tr.even {
        background-color: #f2f2f2
    }
    tr.odd {
        background-color: white
    }

    th {
        background-color: #3598dc;
        color: white;
    }
    .header{
        width: 100%;
        background-color: #1c2329;
        padding: 20px;
        text-align: center;
        color: white;
    }
    html{
        font-family :'Verdana';
    }
    footer{
        text-align: center;
        position: absolute;
        bottom: 0;
        padding: 10px;
        width: 100%
    }
    .highlight{
        color: lightblue;
    }
    .left{
        width: 50%;
        float: left;
    }
    .right{
        width: 50%;
        float: right;
    }
    .content{
        height: 50px;
        margin: 30px 0px;
    }
</style>

<div class="header">
    <h2>
        <?php echo $course['c_course_name']; ?> : <?php echo $project_class['cls_start_date']; ?> -  <?php echo $project_class['cls_end_date']; ?>

    </h2>
</div>
<div>
    <div>
        <div class="content">
            <div class="left">
                <div>
                    <span><b> Course : </b> <?php echo $course['c_course_name']; ?> </span>
                </div>
                <div>
                    <span><b> Class  : </b> <?php echo $project_class['cls_code']; ?> </span>
                </div>
                <div>
                    <span><b> Trainer: </b> <?php echo $course_trainer['u_fullname']; ?> </span>
                </div>
            </div>
            <div class="right">
                <div>
                    <span><b> Start Date : </b> <?php echo $project_class['cls_start_date']; ?> </span>
                </div>
                <div>
                    <span><b> End   Date : </b> <?php echo $project_class['cls_end_date']; ?> </span>
                </div>
            </div>
        </div>
    </div>

    <div>
        <div>
            <h2> Sessions </h2><br>
            <table>
                <thead>
                    <tr role="row">
                        <th > Session Code </th>
                        <th width="20%"> Date </th>
                        <th > Time </th>
                        <th > Duration (hours)</th>
                        <th > Trainer </th> 
                    </tr>
                </thead>
                <tbody> 
                		<?php   for ($i = 0; $i < count($class_sessions); $i++) {  ?>
                        <tr class="<?php echo ( $i % 2 == 0 ) ? 'even' : 'odd'; ?>">
                            <td><?php echo isset($class_sessions[$i]) ? $class_sessions[$i]['sc_session_code'] : ""; ?></td>
                            <td><?php echo isset($class_sessions[$i]) ? $class_sessions[$i]['sc_session_date'] : ""; ?></td>
                            <td><?php echo isset($class_sessions[$i]) ? $class_sessions[$i]['sc_session_time'] : ""; ?></td>
                            <td><?php echo isset($class_sessions[$i]) ? $class_sessions[$i]['sc_session_duration'] : ""; ?></td>
                            <td><?php echo ( isset($session_trainers)   ? $course_trainer['u_fullname'] : $class_sessions[$i]['fk_session_trainer_id'] ); ?></td>
                        </tr>
                        <?php   }  ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<footer>
    <div> Copyright © 2017 <span class="highlight">OMSAR</span>  |  Office of the Minister of the State For Administrative Reform </div>
</footer>
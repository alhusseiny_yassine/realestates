<table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
    <thead class="flip-content">
        <tr>
            <th style="width:5%;">#</th>
            <th style="width:20%">Session Code</th>
            <th style="width:10%">Session Status</th>
            <th style="width:15%">Trainer</th>
            <th style="width:10%">Date</th>
            <th style="width:10%">Time</th>
            <th style="width:10%">Duration(hours)</th>
            <th style="width:5%;" nowrap>Edit</th>
            <th style="width:5%;" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php $__currentLoopData = $sessions_info; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $session): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
        <tr  class="odd gradeX" data-session_id="<?php echo e($session->sc_id); ?>">
            <td><?php echo e($session->sc_id); ?></td>
            <td><?php echo e($session->sc_session_code); ?></td>
            <td style="background-color: <?php echo e(( isset( $session_status[$session->fk_session_status_id] ) ) ? $session_status[$session->fk_session_status_id]->ss_status_color : ""); ?>">
                <?php echo e(( isset( $session_status[$session->fk_session_status_id] ) ) ? $session_status[$session->fk_session_status_id]->ss_status_name : "N/A"); ?></td>
            <td><?php echo e(( isset( $trainers[$session->fk_session_trainer_id] ) ) ? $trainers[$session->fk_session_trainer_id]->u_fullname : "N/A"); ?></td>
            <td><?php echo e($session->sc_session_date); ?></td>
            <td><?php echo e($session->sc_session_time); ?></td>
            <td><?php echo e($session->sc_session_duration); ?></td>
            <td>
                <a href="#" data-session_id="<?php echo e($session->sc_id); ?>" id="EDIT_SESSION_<?php echo e($session->sc_id); ?>" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a>
            </td>
            <td>
                <a href="#" data-session_id="<?php echo e($session->sc_id); ?>" id="DELETE_SESSION_<?php echo e($session->sc_id); ?>" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a>
            </td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </tbody>
</table>
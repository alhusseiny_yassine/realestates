<table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
    <thead class="flip-content">
        <tr>
            <th style="width:40%;text-align: left">Question</th>
            <th style="width:60%;">Answer</th>
    </thead>
    <tbody class="ListSurveyAnswers">
        <?php $__currentLoopData = $survey_questions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $question): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
        <tr  class="odd gradeX">
            <td style="text-align: left"><?php echo e($question->sq_question); ?></td>
            <td><?php echo e(( isset( $survey_answers[$question->sq_id] ) ) ? $survey_answers[$question->sq_id]->sa_answer : ''); ?></td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </tbody>
</table>
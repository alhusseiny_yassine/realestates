<?php
/***********************************************************
listrequestsmanagement.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 2, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

?>


<?php $__env->startSection('themes'); ?>
    <link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css')); ?>" rel="stylesheet" type="text/css" />

     <link href="<?php echo e(url('admin/assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')); ?>" rel="stylesheet" type="text/css" />
    <style>
        th{
	       cursor: pointer;
        }
        .modal-footer{
	       width:150px;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('plugins'); ?>

      <script src="<?php echo e(url('admin/assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')); ?>" type="text/javascript"></script>

    <script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('admin/assets/global/plugins/bootbox.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo e(url('admin/assets/global/plugins/jquery.tablesorter/jquery.tablesorter.js')); ?>"></script>

    <script type="text/javascript"  src="<?php echo e(url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')); ?>"></script>
    <script type="text/javascript"  src="<?php echo e(url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js')); ?>"></script>
    <script src="<?php echo e(url('admin/assets/pages/scripts/ecommerce-orders-view.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo e(url('js/modules/requests.module.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('js/requests/requestsmanagement.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="page-fixed-main-content">
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject font-dark sbold uppercase"> Request Management
                        </span>
                    </div>
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <label class="btn btn-transparent blue btn-outline btn-circle btn-sm">
                                <input type="radio" name="options" class="toggle" id="option2">Settings</label>
                        </div>
                        <div class="btn-group">
                            <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                                <i class="fa fa-share"></i>
                                <span class="hidden-xs"> Actions </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <?php if(isset( $role_info['om_reject_existing_request'] ) && $role_info['om_reject_existing_request'] == 'allow'): ?>
                                <li><a class="RejectRequests" href="javascript:;"> Reject Request </a></li>
                                <?php endif; ?>
                                <?php if(isset( $role_info['om_change_request_status'] ) && $role_info['om_change_request_status'] == 'allow'): ?>
                                <li><a class="ChangeRequestStatus" href="javascript:;"> Change Request Status </a></li>
                                <?php endif; ?>
                                <?php if(isset( $role_info['om_approve_existing_request'] ) && $role_info['om_approve_existing_request'] == 'allow'): ?>
                                <li><a class="ApproveRequests" href="javascript:;"> Approve Requests </a></li>
                                <?php endif; ?>
                                <?php if(isset( $role_info['om_create_new_project'] ) && $role_info['om_create_new_project'] == 'allow'): ?>
                                <li><a class="CreateNewProject"  href="javascript:;"> Create New Training Project </a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs nav-tabs-lg">
                            <?php if(isset( $role_info['om_requests_dashboard'] ) && $role_info['om_requests_dashboard'] == 'allow'): ?>
                            <li class="active">
                                <a href="#RequestsDashboard" data-toggle="tab"> Requests Dashboard </a>
                            </li>
                            <?php endif; ?>
                            <?php if(isset( $role_info['om_my_requests'] ) && $role_info['om_my_requests'] == 'allow'): ?>
                            <li>
                                <a href="#MyRequests" data-toggle="tab">
                                   My Requests
                                </a>
                            </li>
                            <?php endif; ?>
                            <?php if(isset( $role_info['om_display_all_requests'] ) && $role_info['om_display_all_requests'] == 'allow'): ?>
                            <li>
                                <a href="#AllRequests" data-toggle="tab">All Requests </a>
                            </li>
                            <?php endif; ?>
                        </ul>
                        <div class="tab-content">
                             <?php if(isset( $role_info['om_requests_dashboard'] ) && $role_info['om_requests_dashboard'] == 'allow'): ?>
                            <div class="tab-pane active" id="RequestsDashboard">
                            </div>
                            <?php endif; ?>
                            <?php if(isset( $role_info['om_my_requests'] ) && $role_info['om_my_requests'] == 'allow'): ?>
                            <div class="tab-pane" id="MyRequests">
                                <span id="hidden_fields">
                                    <input type="hidden" name="myrequests_page_number" id="myrequests_page_number" value="1" />
                                </span>
                                <div class="row">
                                    <div class="col-md-12 cols-lg-12">
                                        <table class="table table-striped table-bordered table-hover" id="datatables_myrequests">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="2px">
                                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                                            <span></span>
                                                        </label>
                                                    </th>
                                                    <th width="20%"> Request Id&nbsp;# </th>
                                                    <th width="20%"> Request Title </th>
                                                    <th width="20%"> Request&nbsp;Date </th>
                                                    <th width="20%"> Status </th>
                                                    <th  width="10%" nowrap="nowrap"></th>
                                                </tr>
                                                <tr role="row" class="filter">
                                                    <td> </td>
                                                    <td>
                                                        <input type="text" class="form-control form-filter input-sm" name="my_request_no"> </td>
                                                    <td>
                                                        <input type="text" class="form-control form-filter input-sm" name="my_request_title"> </td>
                                                    <td>
                                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                                            <input type="text" class="form-control form-filter input-sm" readonly name="my_request_date" placeholder="Request Date">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <select name="my_request_status" class="form-control form-filter input-sm">
                                                            <option value="">Select...</option>
                                                             <?php for($i=0;$i< count($request_statuses);$i++): ?>
                                                              <option value="<?php echo e($request_statuses[$i]->rs_id); ?>"><?php echo e($request_statuses[$i]->rs_status_name); ?></option>
                                                             <?php endfor; ?>
                                                        </select>
                                                    </td>
                                                    <td nowrap="nowrap" style="width:30%">
                                                        <div class="col-md-6" align="left" style="padding-left: 0px;">
                                                        <button name="btn_search_my_requests" class="btn btn-sm yellow filter-submit margin-bottom">
                                                                <i class="fa fa-search"></i> Search</button>
                                                        </div>
                                                        <div class="col-md-1"></div>
                                                        <div class="col-md-5">
                                                        <button type="reset" name="btn_reset_myrequest" class="btn btn-sm red filter-cancel">
                                                            <i class="fa fa-times"></i> Reset</button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody class="ListMyRequests"> </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 cols-lg-12">
                                            <ul id="MyRequestsPagination" class="pagination-sm"></ul>
                                    </div>
                                </div>

                            </div>
                            <?php endif; ?>
                            <?php if(isset( $role_info['om_display_all_requests'] ) && $role_info['om_display_all_requests'] == 'allow'): ?>
                            <div class="tab-pane" id="AllRequests">
                                <div class="table-container">
                                    <span id="hidden_fields">
                                        <input type="hidden" name="allrequests_page_number" id="allrequests_page_number" value="1" />
                                        <input type="hidden" name="is_admin" value="0" />
                                    </span>
                                    <div class="row">
                                        <div class="col-md-12 ListAllRequests">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 cols-lg-12">
                                            <ul id="AllRequestsPagination" class="pagination-sm"></ul>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End: life time stats -->
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
 <div id="SendRequest" class="modal fade" tabindex="-1"> </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
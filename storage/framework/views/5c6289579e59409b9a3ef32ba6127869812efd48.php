<?php
/************************************************************
displaylistcustomers.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 29, 2017
Developed By  : Alhusseiny PHP Department A&H S.A.R.L

Page Description :
--
************************************************************/
?>
<?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $customer): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
<tr  class="odd gradeX" data-c_id="<?php echo e($customer->c_id); ?>">
    <td><?php echo e($customer->c_id); ?></td>
    <td><?php echo e($customer->c_fullname); ?></td>
    <td><?php echo e($customer->c_email); ?></td>
    <td><?php echo e($customer->c_mobile); ?></td>
    <td>
        <a href="javascript:;" id="EDIT_CUSTOMER_<?php echo e($customer->c_id); ?>" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a>
    </td>
    <td>
        <a href="javascript:;" id="DELETE_CUSTOMER_<?php echo e($customer->c_id); ?>" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a>
    </td>
</tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
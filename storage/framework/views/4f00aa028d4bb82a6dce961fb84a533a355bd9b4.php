<?php
/***********************************************************
displaylistmyrequests.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 3, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}

"projects_array" => $projects_array,
            "project_request_array" => $project_request_array,
***********************************************************/

?>

<table class="table table-striped table-bordered table-hover" id="datatable_allrequests">
<thead>
    <tr role="row" class="heading">
        <th width="5%">
            <input type="checkbox" class="group-checkable" data-set="#datatable_allrequests .checkboxes" />
        </th>
        <th width="20%"> Beneficiary </th>
        <th width="20%"> Trainee </th>
        <th width="20%"> Course </th>
        <th width="20%"> Project Title </th>
        <th width="20%"> Request Training Date </th>
        <th width="5%"> Priority </th>
        <th width="15%"> Status </th>
        <?php if(isset( $role_info['om_edit_existing_request'] ) && $role_info['om_edit_existing_request'] == 'allow'): ?>
        <th width="2px" nowrap> Edit </th>
        <?php endif; ?>
        <?php if(isset( $role_info['om_delete_existing_request'] ) && $role_info['om_delete_existing_request'] == 'allow'): ?>
        <th width="2px" nowrap> Delete </th>
        <?php endif; ?>
    </tr>
    </thead>
    <tbody>
        <?php for($i = 0; $i < count($myrequests); $i++): ?>
        <tr data-r_id="<?php echo e($myrequests[$i]->r_id); ?>" style="background-color: <?php echo e($request_statuses_byid[ $myrequests[$i]->fk_status_id ]['status_color']); ?>">
            <td><input type="checkbox" class="checkboxes" name="ck_request_<?php echo e($myrequests[$i]->r_id); ?>" id="CK_REQUEST_<?php echo e($myrequests[$i]->r_id); ?>" value="<?php echo e($myrequests[$i]->r_id); ?>" /></td>
            <td><?php echo e(isset( $benf_data_obj[$myrequests[$i]->fk_beneficiary_id]->sb_name ) ? $benf_data_obj[$myrequests[$i]->fk_beneficiary_id]->sb_name : "N/A"); ?></td>
            <td><?php echo e(isset( $lst_users_data[$myrequests[$i]->fk_trainee_id]['fullname'] ) ? $lst_users_data[$myrequests[$i]->fk_trainee_id]['fullname'] : ""); ?></td>
            <td><?php echo e(isset($request_courses_array[ $myrequests[$i]->r_id ]) ? $request_courses_array[ $myrequests[$i]->r_id ] : "N/A"); ?></td>
            <td><?php echo e(isset( $project_request_array[$myrequests[$i]->r_id] ) ? $projects_array[ $project_request_array[$myrequests[$i]->r_id]->fk_project_id ]->pp_project_title . "-" . $project_status_array[ $projects_array[ $project_request_array[$myrequests[$i]->r_id]->fk_project_id ]->fk_project_status_id ]->ps_status_name : "N/A"); ?></td>
            <td><?php echo e(isset( $myrequests[$i]->r_request_train_date ) ? $myrequests[$i]->r_request_train_date : ""); ?></td>
            <td><?php echo e($myrequests[$i]->r_request_order); ?></td>
            <td><?php echo e($request_statuses_byid[ $myrequests[$i]->fk_status_id ]['status_name']); ?></td>
            <?php if(isset( $role_info['om_edit_existing_request'] ) && $role_info['om_edit_existing_request'] == 'allow'): ?>
            <td> <a href="#"  id="EDIT_REQUEST_<?php echo e($myrequests[$i]->r_id); ?>" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
            <?php endif; ?>
        <?php if(isset( $role_info['om_delete_existing_request'] ) && $role_info['om_delete_existing_request'] == 'allow'): ?>
            <td><a href="#"  id="DELETE_REQUEST_<?php echo e($myrequests[$i]->r_id); ?>" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
            <?php endif; ?>
        </tr>
        <?php endfor; ?>
    </tbody>
</table>
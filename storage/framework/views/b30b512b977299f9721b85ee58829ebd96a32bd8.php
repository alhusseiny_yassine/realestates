<?php
/***********************************************************
editcoursetype.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 27, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


?>




<?php $__env->startSection('themes'); ?>
    <link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('plugins'); ?>
    <script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')); ?>" type="text/javascript"></script>
<script type="text/javascript"  src="<?php echo e(url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')); ?>"></script>
<script type="text/javascript"  src="<?php echo e(url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('js/modules/course.module.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('js/course/editcourse.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<div class="portlet blue box">
<div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Edit Course </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
    <div class="portlet-body">
         <form name="form_save_course" id="FORM_SAVE_COURSE">
    <div class="form-body">
         <span id="hidden_fields">
            <?php echo csrf_field(); ?>

            <input type="hidden" name="c_id" id="C_ID" value="<?php echo e($cur_course_info->c_id); ?>" />
        </span>
        <div class="alert alert-success" style="display:none">
				<strong>Success!</strong> Course Information is saved successfully!
			</div>
			<div class="alert alert-danger" style="display:none">
				<strong>Error!</strong> You have some form errors. Please check below.
			</div>
        <div class="row">
            <div class="col-md-6">
                 <div class="form-group">
                    <label class="control-label">Course Code <span class="required"> * </span></label>
                    <input type="text" name="c_code" id="C_CODE" class="form-control" required="required" maxlength="5"  value="<?php echo e($cur_course_info->c_code); ?>" />
                </div>
            </div>
            <div class="col-md-6">
                 <div class="form-group">
                    <label class="control-label">Course Title <span class="required"> * </span></label>
                    <input type="text" name="c_course_name" id="C_COURSE_NAME" class="form-control" required="required" maxlength="500"  value="<?php echo e($cur_course_info->c_course_name); ?>" />
                </div>
            </div>
            <div class="col-md-6">
                 <div class="form-group">
                    <label class="control-label">Course Total Hours <span class="required"> * </span></label>
                    <input type="text" name="c_total_hours" id="C_TOTAL_HOURS" class="form-control" required="required" maxlength="500"  value="<?php echo e($cur_course_info->c_total_hours); ?>" />
                </div>
            </div>
            <div class="col-md-6">
                 <div class="form-group">
                    <label class="control-label">Course Category <span class="required"> * </span></label>
                      <select class="bs-select form-control" name="fk_category_id" id="FK_CATEGORY_ID" data-actions-box="true">
                            <option value="">No Course Category</option>
                            <?php $__currentLoopData = $course_categories_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cc_index => $cc_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <option <?php echo e($cur_course_info->fk_category_id == $cc_info->cc_id ? "selected" : ""); ?> value="<?php echo e($cc_info->cc_id); ?>"><?php echo e($cc_info->cc_course_category); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                 <div class="form-group">
                    <label class="control-label">Course Description <span class="required"> * </span></label>
                    <textarea name="c_course_description" id="C_COURSE_DESCRIPTION" class="form-control" style="width:100%;resize:none" rows="10"  ><?php echo e($cur_course_info->c_course_description); ?></textarea>
                </div>
            </div>
            <div class="col-md-6">
                 <div class="form-group" style="height: 30px;">
                    <label class="control-label">Enable Course</label>
                     <input type="checkbox" <?php echo e($cur_course_info->c_is_active == 1 ? "checked" : ""); ?> name="c_is_active" value='1' />
                </div>
            </div>

        </div>
       <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-md-9"></div>
            <div class="col-md-3" align="right">
                 <button type="submit" name="btn_save_course" id="BTN_SAVE_COURSE"  class="btn blue">Save</button>
                <button type="reset" name="reset_form" id="RESET_FORM"  class="btn default">Cancel</button>
            </div>
        </div>
    </div>
</form>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.alayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
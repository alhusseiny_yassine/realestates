<?php
/***********************************************************
traineehistory.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 23, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

?>


<?php $__env->startSection('themes'); ?>
    <link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('plugins'); ?>
    <script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript"  src="<?php echo e(url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')); ?>"></script>
    <script type="text/javascript"  src="<?php echo e(url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js')); ?>"></script>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="tabbable-line">
    <ul class="nav nav-tabs ">
        <li class="active">
            <a href="#tabTraineeRequests" data-toggle="tab"> Trainee Requests </a>
        </li>
        <li>
            <a href="#tabTraineeProjects" data-toggle="tab"> Trainee Projects </a>
        </li>
        <li>
            <a href="#tabChangingHistory" data-toggle="tab"> Trainee Changing History </a>
        </li>
        
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tabTraineeRequests">
            <table class="table table-striped table-bordered table-hover" style="width:100%">
                <tr role="row" class="heading"> 
                    <th width="20%"> Request Title </th>
                    <th width="20%"> Request&nbsp;Date </th>
                    <th width="20%"> Status </th>
                </tr>
                 <?php for($i = 0; $i < count($requestsHistory); $i++): ?>
                <tr style="background-color: <?php echo e($request_statuses_byid[ $requestsHistory[$i]->fk_status_id ]['status_color']); ?>">
                    <td><?php echo e($requestsHistory[$i]->r_request_title); ?></td>
                    <td><?php echo e($requestsHistory[$i]->r_request_train_date); ?></td>
                    <td><?php echo e($request_statuses_byid[ $requestsHistory[$i]->fk_status_id ]['status_name']); ?></td>
                </tr>
                <?php endfor; ?>
            </table>
            
            
        </div>
        <div class="tab-pane" id="tabTraineeProjects">
             <table class="table table-striped table-bordered table-hover" style="width:100%">
                <tr role="row" class="heading"> 
                    <th width="5%"></th>
                    <th width="15%">Project Code</th>
                    <th width="15%">Project Title</th>
                    <th width="30%">Project Description</th>
                    <th width="15%">Start Date</th>
                    <th width="15%">Ende Date</th>
                </tr>
                <?php $__currentLoopData = $lst_projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $project_id => $project_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                <?php if( in_array( $project_id , $projects_array ) ): ?>
                <tr>
                    <td><?php echo $project_info['pp_id']; ?></td>
                    <td><?php echo $project_info['pp_project_code']; ?></td>
                    <td><?php echo $project_info['pp_project_title']; ?></td>
                    <td><?php echo $project_info['pp_project_description']; ?></td>
                    <td><?php echo $project_info['pp_project_start_date']; ?></td>
                    <td><?php echo $project_info['pp_project_end_date']; ?></td>
                </tr>
                <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </table>
        </div>
        <div class="tab-pane" id="tabChangingHistory">
             <table class="table table-striped table-bordered table-hover" style="width:100%">
                <tr role="row" class="heading"> 
                    <th width="15%">Date</th>
                    <th width="15%">Benificary</th>
                    <th width="15%">Coordinator</th>
                    <th width="10%">EMP Degree</th>
                    <th width="5%">Goverment Grade</th>
                    <th width="15%">Service</th>
                    <th width="15%">Bureau</th>
                    <th width="30%">Residental Area</th>
                </tr>
                <?php $__currentLoopData = $trainee_history; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $history_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                <tr>
                    <td><?php echo $history_info->th_date_history; ?></td>
                    <td><?php echo ( isset($lst_beneficaries[$history_info->fk_benf_id] ) ) ? $lst_beneficaries[$history_info->fk_benf_id]['sb_name'] : ''; ?></td>
                    <td><?php echo ( isset($lst_coordinators[$history_info->fk_coordinator_id] ) ) ? $lst_coordinators[$history_info->fk_coordinator_id]['u_fullname'] : ''; ?></td>
                    <td><?php echo $history_info->th_emp_degree; ?></td>
                    <td><?php echo $history_info->th_gov_grade; ?></td>
                    <td><?php echo $history_info->th_trainee_service; ?></td>
                    <td><?php echo $history_info->th_trainee_bureau; ?></td>
                    <td><?php echo $history_info->th_residential_area; ?></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </table>
        </div>
        
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.alayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
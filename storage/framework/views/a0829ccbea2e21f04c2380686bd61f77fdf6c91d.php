<?php
/***********************************************************
menu.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 22, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad MantachCOPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

?>

<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
         <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                <li class="nav-item start">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title">Dashboard</span>
                        <span class="selected"></span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item start ">
                            <a data-section="dashboard" href="<?php echo e(url('/administrator/dashboard')); ?>" class="nav-link ">
                                <i class="icon-bar-chart"></i>
                                <span class="title">Main Dashboard</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php if( isset($role_info['om_system_management'] ) && $role_info['om_system_management'] == 'allow'): ?>
                 <li class="nav-item">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-database" aria-hidden="true"></i>
                        <span class="title">System Management</span>
                        <span class="selected"></span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <?php if( isset( $role_info['om_user_management'] ) && $role_info['om_user_management'] == 'allow'): ?>
                        <li class="nav-item start ">
                            <a data-section="UsersManagement" href="<?php echo e(url('administrator/UsersManagement')); ?>" class="nav-link ">
                                <i class="fa fa-users" aria-hidden="true"></i>
                                <span class="title">Users</span>
                            </a>
                        </li>
                        <?php endif; ?>
                        <?php if(isset( $role_info['om_roles_management'] ) && $role_info['om_user_management'] == 'allow'): ?>
                        <li class="nav-item start ">
                            <a data-section="RolesManagement" href="<?php echo e(url('administrator/RolesManagement')); ?>" class="nav-link ">
                                <i class="fa fa-archive" aria-hidden="true"></i>
                                <span class="title">Roles</span>
                            </a>
                        </li>
                        <?php endif; ?>
                        <?php if(isset( $role_info['om_beneficiary_management'] ) && $role_info['om_beneficiary_management'] == 'allow'): ?>
                         <li class="nav-item start ">
                            <a data-section="Beneficiaries" href="<?php echo e(url('administrator/Beneficiaries')); ?>" class="nav-link ">
                                <i class="fa fa-briefcase" aria-hidden="true"></i>
                                <span class="title">Beneficiaries</span>
                            </a>
                        </li>
                        <?php endif; ?>
                        <?php if(isset( $role_info['om_beneficiary_types_management'] ) && $role_info['om_beneficiary_types_management'] == 'allow'): ?>
                         <li class="nav-item start ">
                            <a data-section="BeneficiaryTypes" href="<?php echo e(url('administrator/BeneficiaryTypes')); ?>" class="nav-link ">
                                <i class="fa fa-briefcase" aria-hidden="true"></i>
                                <span class="title">Beneficiary Types</span>
                            </a>
                        </li>
                         <?php endif; ?>
                         <?php if(isset( $role_info['om_beneficiary_departments'] ) && $role_info['om_beneficiary_types_management'] == 'allow'): ?>
                         <li class="nav-item start ">
                            <a data-section="BeneficiaryDepartments" href="<?php echo e(url('administrator/BeneficiaryDepartments')); ?>" class="nav-link ">
                                <i class="fa fa-briefcase" aria-hidden="true"></i>
                                <span class="title">Beneficiary Directorate</span>
                            </a>
                        </li>
                        <?php endif; ?>
                        <?php if(isset( $role_info['om_system_configuration_management'] ) && $role_info['om_beneficiary_types_management'] == 'allow'): ?>
                         <li class="nav-item start ">
                            <a data-section="ConfigurationManagement" href="<?php echo e(url('administrator/ConfigurationManagement')); ?>" class="nav-link ">
                                <i class="fa fa-briefcase" aria-hidden="true"></i>
                                <span class="title">System Configuration</span>
                            </a>
                        </li>
                        <?php endif; ?>
                         <li class="nav-item">
                            <a data-section="jobtitles" href="<?php echo e(url('administrator/jobtitles')); ?>" class="nav-link">
                                <i class="fa fa-briefcase" aria-hidden="true"></i>
                                <span class="title">Job Titles</span>
                            </a>
                        </li>
                         <li class="nav-item">
                            <a data-section="JobRoles" href="<?php echo e(url('administrator/JobRoles')); ?>" class="nav-link">
                                <i class="fa fa-briefcase" aria-hidden="true"></i>
                                <span class="title">Job Roles</span>
                            </a>
                        </li>
                         <li class="nav-item">
                            <a data-section="taskslist" href="<?php echo e(url('administrator/taskslist')); ?>" class="nav-link">
                                <i class="fa fa-briefcase" aria-hidden="true"></i>
                                <span class="title">Tasks List</span>
                            </a>
                        </li>
                         <li class="nav-item end">
                            <a data-section="EducationDegrees" href="<?php echo e(url('administrator/EducationDegrees')); ?>" class="nav-link">
                                <i class="fa fa-briefcase" aria-hidden="true"></i>
                                <span class="title">Education Degree</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php endif; ?>
                <?php if(isset( $role_info['om_course_management'] ) && $role_info['om_course_management'] == 'allow'): ?>
                 <li class="nav-item">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-bullseye" aria-hidden="true"></i>
                        <span class="title">Course Management</span>
                        <span class="selected"></span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <?php if(isset( $role_info['om_course_types'] ) && $role_info['om_course_types'] == 'allow'): ?>
                          <li class="nav-item start ">
                            <a data-section="CourseTypes" href="<?php echo e(url('administrator/CourseTypes')); ?>" class="nav-link ">
                                <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                <span class="title">Course Types</span>
                            </a>
                        </li>
                        <?php endif; ?>
                        <?php if(isset( $role_info['om_course_categories_management'] ) && $role_info['om_course_categories_management'] == 'allow'): ?>
                          <li class="nav-item start ">
                            <a data-section="CourseCategories" href="<?php echo e(url('administrator/CourseCategories')); ?>" class="nav-link ">
                                <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                <span class="title">Course Categories</span>
                            </a>
                        </li>
                        <?php endif; ?>
                         <?php if(isset( $role_info['om_course_management_tab'] ) && $role_info['om_course_management_tab'] == 'allow'): ?>
                         <li class="nav-item start ">
                            <a data-section="Courses" href="<?php echo e(url('administrator/Courses')); ?>" class="nav-link ">
                                <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                <span class="title"> Courses </span>
                            </a>
                        </li>
                         <?php endif; ?>
                    </ul>
                </li>
                 <?php endif; ?>
                 <?php if(isset( $role_info['om_class_management_tab'] ) && $role_info['om_class_management_tab'] == 'allow'): ?>
                 <li class="nav-item">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-bullseye" aria-hidden="true"></i>
                        <span class="title">Class Management</span>
                        <span class="selected"></span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                          <li class="nav-item start ">
                            <a data-section="ClassManagement" href="<?php echo e(url('administrator/ClassManagement')); ?>" class="nav-link ">
                                <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                <span class="title">Class Management</span>
                            </a>
                        </li>
                        <?php if(isset( $role_info['om_class_trainees_attendances'] ) && $role_info['om_class_trainees_attendances'] == 'allow'): ?>
                        <li class="nav-item start ">
                            <a data-section="TraineesAttendances" href="<?php echo e(url('administrator/TraineesAttendances')); ?>" class="nav-link ">
                                <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                <span class="title">Trainees Attendances</span>
                            </a>
                        </li>
                        <?php endif; ?>
                        <?php if(isset( $role_info['om_class_test_result'] ) && $role_info['om_class_test_result'] == 'allow'): ?>
                        <li class="nav-item start ">
                            <a data-section="TraineesTestResults" href="<?php echo e(url('administrator/TraineesTestResults')); ?>" class="nav-link ">
                                <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                <span class="title">Trainees Test Results</span>
                            </a>
                        </li>
                        <?php endif; ?>
                    </ul>
                </li>
                <?php endif; ?>
                 <?php if(isset( $role_info['om_project_management_tab'] ) && $role_info['om_project_management_tab'] == 'allow'): ?>
                 <li class="nav-item">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-building" aria-hidden="true"></i>
                        <span class="title">Projects Management</span>
                        <span class="selected"></span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                         <?php if(isset( $role_info['om_project_status_management'] ) && $role_info['om_project_status_management'] == 'allow'): ?>
                          <li class="nav-item start ">
                            <a data-section="projectstatuses" href="<?php echo e(url('administrator/projectstatuses')); ?>" class="nav-link ">
                                <i class="icon-bar-chart"></i>
                                <span class="title">Project Statuses</span>
                            </a>
                        </li>
                         <?php endif; ?>
                         <?php if(isset( $role_info['om_project_type_management'] ) && $role_info['om_project_type_management'] == 'allow'): ?>
                          <li class="nav-item start ">
                            <a data-section="projecttypes" href="<?php echo e(url('administrator/projecttypes')); ?>" class="nav-link ">
                                <i class="icon-bar-chart"></i>
                                <span class="title">Project Types</span>
                            </a>
                        </li>
                        <?php endif; ?>
                        <?php if(isset( $role_info['om_project_management'] ) && $role_info['om_project_management'] == 'allow'): ?>
                        <li class="nav-item start ">
                            <a data-section="projects" href="<?php echo e(url('administrator/projects')); ?>" class="nav-link ">
                                <i class="fa fa-building" aria-hidden="true"></i>
                                <span class="title"> Projects </span>
                            </a>
                        </li>
                        <?php endif; ?>
                        <?php if(isset( $role_info['om_project_course_status'] ) && $role_info['om_project_course_status'] == 'allow'): ?>
                        <li class="nav-item start ">
                            <a data-section="TraineesCourseStatus" href="<?php echo e(url('administrator/TraineesCourseStatus')); ?>" class="nav-link ">
                                <i class="fa fa-building" aria-hidden="true"></i>
                                <span class="title"> Project Course Status </span>
                            </a>
                        </li>
                        <?php endif; ?>
                        <?php if(isset( $role_info['om_donors_management'] ) && $role_info['om_donors_management'] == 'allow'): ?>
                         <li class="nav-item start ">
                            <a data-section="donors" href="<?php echo e(url('administrator/donors')); ?>" class="nav-link ">
                                <i class="fa fa-odnoklassniki" aria-hidden="true"></i>
                                <span class="title"> Donors </span>
                            </a>
                        </li>
                        <?php endif; ?>
                    </ul>
                </li>
                <?php endif; ?>
                <?php if(isset( $role_info['om_sessions_management'] ) && $role_info['om_sessions_management'] == 'allow'): ?>
                 <li class="nav-item">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-building" aria-hidden="true"></i>
                        <span class="title">Sessions Management</span>
                        <span class="selected"></span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                          <?php if(isset( $role_info['om_sessions_status_management'] ) && $role_info['om_sessions_status_management'] == 'allow'): ?>
                          <li class="nav-item start ">
                            <a data-section="sessionstatuses" href="<?php echo e(url('administrator/sessionstatuses')); ?>" class="nav-link ">
                                <i class="icon-bar-chart"></i>
                                <span class="title">Session Statuses</span>
                            </a>
                        </li>
                         <?php endif; ?>
                    </ul>
                </li>
                <?php endif; ?>
                 <li class="nav-item">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-building" aria-hidden="true"></i>
                        <span class="title">Financial Management</span>
                        <span class="selected"></span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                          <li class="nav-item start ">
                            <a data-section="invoicemanagement" href="<?php echo e(url('administrator/invoicemanagement')); ?>" class="nav-link ">
                                <i class="icon-bar-chart"></i>
                                <span class="title">Invoice Management</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php if(isset( $role_info['om_requests_management_section'] ) && $role_info['om_requests_management_section'] == 'allow'): ?>
                 <li class="nav-item">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-building" aria-hidden="true"></i>
                        <span class="title">Requests Management</span>
                        <span class="selected"></span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                       <?php if(isset( $role_info['om_request_status_management'] ) && $role_info['om_request_status_management'] == 'allow'): ?>
                          <li class="nav-item start ">
                            <a data-section="requeststatuses" href="<?php echo e(url('administrator/requeststatuses')); ?>" class="nav-link ">
                                <i class="icon-bar-chart"></i>
                                <span class="title">Request Statuses</span>
                            </a>
                          </li>
                       <?php endif; ?>
                       <?php if(isset( $role_info['om_requests_management'] ) && $role_info['om_requests_management'] == 'allow'): ?>
                          <li class="nav-item start ">
                            <a data-section="requests" href="<?php echo e(url('administrator/requests')); ?>" class="nav-link ">
                                <i class="icon-bar-chart"></i>
                                <span class="title">Requests Management</span>
                            </a>
                          </li>
                      <?php endif; ?>
                      <?php if(isset( $role_info['om_add_new_request'] ) && $role_info['om_add_new_request'] == 'allow'): ?>
                          <li class="nav-item start ">
                            <a data-section="AddNewRequestForm" href="<?php echo e(url('administrator/AddNewRequestForm')); ?>" class="nav-link">
                                <i class="icon-bar-chart"></i>
                                <span class="title"> Add New Request </span>
                            </a>
                          </li>
                      <?php endif; ?>
                    </ul>
                </li>
                <?php endif; ?>
                <?php if(isset( $role_info['om_country_management_section'] ) && $role_info['om_country_management_section'] == 'allow'): ?>
                <li class="nav-item">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-globe" aria-hidden="true"></i>
                        <span class="title">Country Management</span>
                        <span class="selected"></span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <?php if(isset( $role_info['om_country_management'] ) && $role_info['om_country_management'] == 'allow'): ?>
                         <li class="nav-item start ">
                            <a data-section="countries" href="<?php echo e(url('administrator/countries')); ?>" class="nav-link ">
                                <i class="fa fa-location-arrow" aria-hidden="true"></i>
                                <span class="title"> Countries </span>
                            </a>
                        </li>
                        <?php endif; ?>
                        <?php if(isset( $role_info['om_provinces_management'] ) && $role_info['om_provinces_management'] == 'allow'): ?>
                         <li class="nav-item start ">
                            <a data-section="provinces" href="<?php echo e(url('administrator/provinces')); ?>" class="nav-link ">
                                <i class="fa fa-location-arrow" aria-hidden="true"></i>
                                <span class="title"> Provinces </span>
                            </a>
                        </li>
                         <?php endif; ?>
                        <?php if(isset( $role_info['om_districts_management'] ) && $role_info['om_districts_management'] == 'allow'): ?>
                         <li class="nav-item start ">
                            <a data-section="districts" href="<?php echo e(url('administrator/districts')); ?>" class="nav-link ">
                                <i class="fa fa-location-arrow" aria-hidden="true"></i>
                                <span class="title"> Districts </span>
                            </a>
                        </li>
                        <?php endif; ?>
                    </ul>
                </li>
                <?php endif; ?>
                <?php if(isset( $role_info['om_cms_management'] ) && $role_info['om_cms_management'] == 'allow'): ?>
                <li class="nav-item">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-globe" aria-hidden="true"></i>
                        <span class="title">CMS Management</span>
                        <span class="selected"></span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <?php if(isset( $role_info['om_slider_management'] ) && $role_info['om_slider_management'] == 'allow'): ?>
                         <li class="nav-item start ">
                            <a data-section="slidersManagement" href="<?php echo e(url('administrator/slidersManagement')); ?>" class="nav-link ">
                                <i class="fa fa-location-arrow" aria-hidden="true"></i>
                                <span class="title"> Slider Management </span>
                            </a>
                        </li>
                        <?php endif; ?>
                    </ul>
                </li>
                <?php endif; ?>
                <li class="nav-item">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-globe" aria-hidden="true"></i>
                        <span class="title">Reports Management</span>
                        <span class="selected"></span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                         <li class="nav-item start ">
                            <a data-section="ListCourses" href="<?php echo e(url('reports/ListCourses')); ?>" class="nav-link ">
                                <i class="fa fa-location-arrow" aria-hidden="true"></i>
                                <span class="title"> List Courses </span>
                            </a>
                        </li>
                         <li class="nav-item start ">
                            <a data-section="ListProviders" href="<?php echo e(url('reports/ListProviders')); ?>" class="nav-link ">
                                <i class="fa fa-location-arrow" aria-hidden="true"></i>
                                <span class="title"> List Providers </span>
                            </a>
                        </li>
                         <li class="nav-item start ">
                            <a data-section="ListCoordinators" href="<?php echo e(url('reports/ListCoordinators')); ?>" class="nav-link ">
                                <i class="fa fa-location-arrow" aria-hidden="true"></i>
                                <span class="title"> List Coordinators </span>
                            </a>
                        </li>
                         <li class="nav-item start ">
                            <a data-section="ListTrainees" href="<?php echo e(url('reports/ListTrainees')); ?>" class="nav-link ">
                                <i class="fa fa-location-arrow" aria-hidden="true"></i>
                                <span class="title"> List Trainees </span>
                            </a>
                        </li>
                         <li class="nav-item start ">
                            <a data-section="ListProjects" href="<?php echo e(url('reports/ListProjects')); ?>" class="nav-link ">
                                <i class="fa fa-location-arrow" aria-hidden="true"></i>
                                <span class="title"> List Projects </span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-file-text-o" aria-hidden="true"></i>
                        <span class="title">Surveys Management</span>
                        <span class="selected"></span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                         <li class="nav-item start ">
                            <a data-section="ListSurveys" href="<?php echo e(url('surveys/ListSurveys')); ?>" class="nav-link ">
                                <i class="fa fa-list" aria-hidden="true"></i>
                                <span class="title"> List Surveys </span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="<?php echo e(url('administrator/Logout')); ?>" class="nav-link nav-toggle">
                        <i class="icon-power"></i>
                        <span class="title">Log Out</span>
                    </a>
                </li>

         </ul>
    </div>
</div>
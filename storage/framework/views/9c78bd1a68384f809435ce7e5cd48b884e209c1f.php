<?php
/***********************************************************
sessionsDropdown.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 29, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
Session Dropdown
***********************************************************/

?>


<div>
    <label><b> Sessions : </b></label>
    <select class="bs-select form-control" name="sc_sessions_class" id="SC_SESSIONS_CLASS" data-actions-box="true">
        <option value="">-- Select One -- </option>
        <?php $__currentLoopData = $SessionClass; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sc_index => $sc_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <option value="<?php echo e($sc_info->sc_id); ?>"><?php echo e($sc_info->sc_session_date); ?></option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </select>
</div>
<?php
/***********************************************************
select.blade.php
 Product : 
 Version : 1.0
 Release : 2
 Date Created :  Aug 18, 2017
 Developed By  : Mohamad Mantach   PHP Department
 All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2017
 
 Page Description :
 {Enter page description Here}
 ***********************************************************/
?>
<label>Project : </label>
<select name="<?php echo e($name); ?>" id="<?php echo e($id); ?>" class="form-control" style="width:100%">
    <option value="">--Select One--</option>
     <?php $__currentLoopData = $projects_dropdown_array; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id => $value): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
        <option value="<?php echo e($id); ?>"><?php echo e($value); ?></option>
     <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
</select>
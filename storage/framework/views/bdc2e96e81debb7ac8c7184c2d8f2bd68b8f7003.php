<?php
/***********************************************************
educationinfo.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Mar 17, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

?>

<div >
        <div class="portlet-body flip-scroll" id="LISTEDUCATION">
            <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
                <thead class="flip-content">
                <tr>
                    <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_ue" id="CK_ALL_UE" class="group-checkable" data-set="#LISTEDUCATION .checkboxes" value="1" /></th>
                    <th style="width:2px">#</th>
                    <th>Education Title </th>
                    <th> Specialisation </th>
                    <th style="width:2px" nowrap>Edit</th>
                    <th style="width:2px" nowrap>Delete</th>
                </tr>
            </thead>
            <tbody>
            <?php

            foreach ($userEducationInfo as $index => $ue_info)
            {
                ?>
                   <tr data-ue_id="<?php echo e($ue_info->ue_id); ?>">
                       <td><input type="checkbox" class="checkboxes" name="ck_ue_<?php echo e($ue_info->ue_id); ?>" id="CK_BENF_<?php echo e($ue_info->ue_id); ?>" value="1" /></td>
                       <td><?php echo $ue_info->ue_id; ?></td>
                       <td><?php echo $ue_info->ue_education_name; ?></td>
                       <td><?php echo $ue_info->ue_education_details; ?></td>
                       <td> <a href="#"  id="EDIT_UE_<?php echo e($ue_info->ue_id); ?>" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
                       <td><a href="#"  id="DELETE_UE_<?php echo e($ue_info->ue_id); ?>" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
                   </tr>
                <?php
            }
            ?>
            </tbody>
           </table>
    </div>
 </div>

 <div class="row" style="margin-right:0px;">
    <div class="cols-md-12" align="right">
        <button type="button" name="btn_add_education_info" class="btn btn-warning">Add Education Info</button>
    </div>
 </div>
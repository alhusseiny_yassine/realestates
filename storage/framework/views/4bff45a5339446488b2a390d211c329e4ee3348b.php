<?php
/***********************************************************
results.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 2, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


for ($i = 0; $i < count($total_projects_obj); $i++)
{
    ?>
        <li class="search-item clearfix" data-pp_id="<?php echo e($total_projects_obj[$i]->pp_id); ?>">
            <div class="search-content">
                <div class="row">
                    <div class="col-md-1"><input type="checkbox" class="checkbox" name="ck_project_<?php echo e($total_projects_obj[$i]->pp_id); ?>" id="CK_PROJECT_<?php echo e($total_projects_obj[$i]->pp_id); ?>" value="<?php echo e($total_projects_obj[$i]->pp_id); ?>" /></div>
                    <div class="col-sm-6 col-md-4 col-xs-12">
                        <h2 class="search-title">
                            <a href="javascript:;"><?php echo e($total_projects_obj[$i]->pp_project_code . "-" . $total_projects_obj[$i]->pp_project_title); ?></a>
                        </h2>
                        <!-- <p class="search-desc"> Last Activity:
                            <a href="javascript:;">Bob Robson</a> -
                            <span class="font-grey-salt">25 mins ago</span>
                        </p> -->
                    </div>
                    <div class="col-sm-2 col-md-2 col-xs-4">
                        <p class="search-counter-number"><?php echo e(isset( $nbr_courses[$total_projects_obj[$i]->pp_id] ) ? $nbr_courses[$total_projects_obj[$i]->pp_id] : "0"); ?></p>
                        <p class="search-counter-label uppercase">Courses</p>
                    </div>
                    <div class="col-sm-2 col-md-2 col-xs-4">
                        <p class="search-counter-number"><?php echo e(isset( $nbr_classes[$total_projects_obj[$i]->pp_id] ) ? $nbr_classes[$total_projects_obj[$i]->pp_id] : "0"); ?></p>
                        <p class="search-counter-label uppercase">Classes</p>
                    </div>
                    <div class="col-sm-2 col-md-2 col-xs-4">
                        <p class="search-counter-number"><?php echo e(isset( $nbr_trainees[$total_projects_obj[$i]->pp_id] ) ? $nbr_trainees[$total_projects_obj[$i]->pp_id] : "0"); ?></p>
                        <p class="search-counter-label uppercase">Trainees</p>
                    </div>
                    <div class="col-sm-2 col-md-1 col-xs-4">
                        <table>
                            <tr>
                                <?php if( ( isset($role_info['om_change_project_configuration'])  && $role_info['om_change_project_configuration'] == 'allow' ) ): ?>
                                <td><a href="#"  id="CONFIG_PROJECT_<?php echo e($total_projects_obj[$i]->pp_id); ?>" ><i class="fa fa-cog" aria-hidden="true" height="16" ></i></a></td>
                                <?php endif; ?>
                                <?php if( ( isset($role_info['om_edit_existing_project'])  && $role_info['om_edit_existing_project'] == 'allow' ) ): ?>
                                <td><a href="#"  id="EDIT_PROJECT_<?php echo e($total_projects_obj[$i]->pp_id); ?>" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
                                <?php endif; ?>
                                <?php if( ( isset($role_info['om_delete_existing_project'])  && $role_info['om_delete_existing_project'] == 'allow' ) ): ?>
                                <td><a href="#"  id="DELETE_PROJECT_<?php echo e($total_projects_obj[$i]->pp_id); ?>" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
                                 <?php endif; ?>
                            </tr>
                        </table>
                    </div>
                </div>
        </li>

    <?php
}

?>


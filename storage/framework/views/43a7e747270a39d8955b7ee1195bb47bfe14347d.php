<?php
use App\model\Users\Users;
/***********************************************************
admin_efields.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 23, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
Admin Extra Fields related to OMSAR Administrator Only
***********************************************************/
{
    if(!isset($user_info))
    {
        $user_info = new Users();
    }
}
?>
 <div class="col-md-4">
      <div class="form-group">
      <label  class="control-label">Beneficiaries <span class="required"> * </span></label>
            <select name="fk_beneficiary_id" id="FK_BENEFICIARY_ID" class="form-control" style="width:100%">
                <option value="">-- Select One --</option>
                <?php $__currentLoopData = $beneficiaries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sb_index => $ben_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <option <?php echo e($user_info->fk_beneficiary_id == $ben_info['sb_id'] ? "selected" : ""); ?> value="<?php echo e($ben_info['sb_id']); ?>"><?php echo e($ben_info['sb_name']); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </select>
      </div>
</div>

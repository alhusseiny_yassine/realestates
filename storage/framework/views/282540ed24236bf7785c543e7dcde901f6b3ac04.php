
<?php $__currentLoopData = $questions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $question_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
<?php $remain_stars = 5 - $question_info->sq_rating?>
<tr  class="odd gradeX" data-question_id="<?php echo e($question_info->sq_id); ?>">
    <td><?php echo e($question_info->sq_id); ?></td>
    <td><?php echo e($question_info->sq_question); ?></td>
    <td><div style="color: #2ab4c0;font-size: 18px">
            <?php for($i=1 ; $i <= $question_info->sq_rating ; $i++): ?>
            <i class="fa fa-star" aria-hidden="true"></i>
            <?php endfor; ?>
            <?php for($i=1 ; $i <= $remain_stars ; $i++): ?>
            <i class="fa fa-star-o" aria-hidden="true"></i>
            <?php endfor; ?>
        </div>
    </td>
    <td><?php echo e($question_info->sq_order); ?></td>
    <td style="text-align: center"><a href="<?php echo e(url('/surveys/ViewAnswers/'.$question_info->sq_id )); ?>"><i class="fa fa-eye" aria-hidden="true" height="16" ></i></a></td>
    <td style="text-align: center"><a href="<?php echo e(url('/surveys/EditQuestion/'.$question_info->sq_id )); ?>"  id="EDIT_QUESTION_<?php echo e($question_info->sq_id); ?>" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
    <td style="text-align: center"><a href="#"  id="DELETE_QUESTION_<?php echo e($question_info->sq_id); ?>" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
</tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>

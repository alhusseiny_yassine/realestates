<label class="control-label">Courses<span class="required"> * </span></label>
<input type="hidden" id="selected_project_id" value="<?php echo e($project_id); ?>">
<select data-type="courses" class="form-control selectpicker" name="fk_course_id" id="fk_course_id">
    <option value="" >Select</option>
    <?php $__currentLoopData = $lst_courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $course): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
    <?php if( in_array( $course->c_id , $courses_array ) ): ?>
    <option value="<?php echo e($course->c_id); ?>" ><?php echo e($course->c_course_name); ?></option>
    <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
</select>
<script type="text/javascript">
    $(".selectpicker").selectpicker({
        liveSearch: "true"
    });
</script>
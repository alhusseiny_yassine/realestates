<?php
/***********************************************************
educationinfo.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Mar 17, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

?>

<div >
        <div class="portlet-body flip-scroll" id="LISTPLOCATIONS">
            <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
                <thead class="flip-content">
                <tr>
                    <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_pl" id="CK_ALL_PL" class="group-checkable" data-set="#LISTPLOCATIONS .checkboxes" value="1" /></th>
                    <th style="width:2px">#</th>
                    <th>Provider Location </th>
                    <th style="width:2px" nowrap>Edit</th>
                    <th style="width:2px" nowrap>Delete</th>
                </tr>
            </thead>
            <tbody>
            <?php

            foreach ($providerlocations as $index => $pl_info)
            {
                ?>
                   <tr data-pl_id="<?php echo e($pl_info->pl_id); ?>">
                       <td><input type="checkbox" class="checkboxes" name="ck_pl_<?php echo e($pl_info->pl_id); ?>" id="CK_PL_<?php echo e($pl_info->pl_id); ?>" value="1" /></td>
                       <td><?php echo $pl_info->pl_id; ?></td>
                       <td><?php echo $pl_info->pl_provider_location; ?></td>
                       <td> <a href="#"  id="EDIT_PL_<?php echo e($pl_info->pl_id); ?>" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
                       <td><a href="#"  id="DELETE_PL_<?php echo e($pl_info->pl_id); ?>" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
                   </tr>
                <?php
            }
            ?>
            </tbody>
           </table>
    </div>
 </div>

 <div class="row" style="margin-right:0px;">
    <div class="cols-md-12" align="right">
        <button type="button" name="btn_add_provider_location" class="btn btn-warning">Add Provider Location</button>
    </div>
 </div>
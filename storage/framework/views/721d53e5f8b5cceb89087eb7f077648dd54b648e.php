<?php
/***********************************************************
coursesdropdown.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Dec 24, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/
 
?>
<label><b>Course :</b></label>
<select name="pp_course" id="PP_COURSE" class="form-control">
<option value="0">--Select Course--</option>
<?php $__currentLoopData = $project_course_info; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
<option value="<?php echo e($index); ?>"><?php echo e($info); ?></option>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
</select>
<?php
/***********************************************************
lstnotifications.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Apr 27, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
Display List of all notifications with time
***********************************************************/

?>




<?php $__env->startSection('content'); ?>


<ul class="list-group">
	 <a href="#" class="list-group-item active">
    Notifications
  </a>
  <?php $__currentLoopData = $LstNotifications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notif): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
  <li class="list-group-item"><?php echo $notif->nt_notification; ?><span class="badge"><?php echo e(\Carbon\Carbon::parse($notif->nt_notification_date)->diffForHumans()); ?></span></li>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
  <?php $__currentLoopData = $RqstNotifications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notif): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
  <li class="list-group-item"><?php echo $notif->nr_notification_text; ?><span class="badge"><?php echo e(\Carbon\Carbon::parse($notif->nr_notification_date)->diffForHumans()); ?></span></li>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
</ul>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php
/************************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>
<div class="portlet-body flip-scroll" id="LISTBENEFICIARYTYPES">
    <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
        <thead class="flip-content">
        <tr>
            <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_benftype" id="CK_ALL_BENFTYPES" class="group-checkable" data-set="LISTBENEFICIARYTYPES .checkboxes" value="1" /></th>
            <th style="width:2px">#</th>
            <th>Beneficiary Type Name </th>
            <th>Beneficiary Type Description</th>
            <th style="width:2px" nowrap>Edit</th>
            <th style="width:2px" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($beneficiary_types as $index => $benft_info)
        {
            ?>
               <tr data-bt_id="<?php echo e($benft_info->bt_id); ?>">
                   <td><input type="checkbox" class="checkboxes" name="ck_benft_<?php echo e($benft_info->bt_id); ?>" id="CK_BENFT_<?php echo e($benft_info->bt_id); ?>" value="1" /></td>
                   <td><?php echo $benft_info->bt_id; ?></td>
                   <td><?php echo $benft_info->bt_name; ?></td>
                   <td><?php echo substr($benft_info->bt_description, 0,10); ?></td>
                   <td> <a href="#"  id="EDIT_BENFT_<?php echo e($benft_info->bt_id); ?>" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
                   <td><a href="#"  id="DELETE_BENFT_<?php echo e($benft_info->bt_id); ?>" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>
</div>
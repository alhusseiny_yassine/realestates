<?php
/* * *********************************************************
  traineesInfo.blade.php
  Product :
  Version : 1.0
  Release : 2
  Date Created : Dec 24, 2016
  Developed By  : Mohamad Mantach   PHP Department
  All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

  Page Description :
  {Enter page description Here}
 * ********************************************************* */
$session_user_id = session()->get('user_id');
?>
<div class="portlet-body flip-scroll" id="">
    <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
        <thead class="flip-content">
            <tr>
                <th style="width:2px;">#</th>
                <th>Email</th>
                <th>Full Name</th>
                <th>beneficiary</th>
                <th></th>
            </tr>
        </thead>
        <tbody class="">

            <?php $__currentLoopData = $lst_trainees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $trainee_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <?php if( in_array( $trainee_info['id'] , $trainees_array ) ): ?>

            <tr>
                <td><?php echo $trainee_info['id']; ?></td>
                <td><?php echo $trainee_info['u_email']; ?></td>
                <td><?php echo $trainee_info['u_fullname']; ?></td>
                <td><?php echo ( isset($trainee_info['fk_beneficiary_id']) ) ? $benf_info[$trainee_info['fk_beneficiary_id']]->sb_name : "";; ?></td>
                <td>
                    <?php if($trainee_info->isOnline()): ?>
                        <img src="<?php echo e(url('imgs/online_user.png')); ?>" />
                    <?php else: ?>
                        <img src="<?php echo e(url('imgs/offline_user.png')); ?>" />
                    <?php endif; ?>
                </td>
            </tr>
            <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?> 
        </tbody>
    </table>
</div>
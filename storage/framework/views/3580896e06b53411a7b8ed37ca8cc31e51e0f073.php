<?php
/************************************************************
users.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 6, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
view of display usermanagement section
************************************************************/

?>


<?php $__env->startSection('themes'); ?>
<link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('plugins'); ?>
<script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/bootbox.min.js')); ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo e(url('js/modules/users.module.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('js/users/usersmanagement.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="portlet blue box">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> User Management </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
     <div class="portlet-body">
       
        <span id="hidden_fields">
            <input type="hidden" name="page_number" value="1" />
        </span>
        <div class="row">
            <div class="col-md-12" align="right">
                    <div class="btn-group pull-right blue">
                        <button class="btn green-sharp btn-outline dropdown-toggle" data-toggle="dropdown">Actions
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="javascript:;" id="PRINT_LIST"> Print </a>
                            </li>
                            <li>
                                <a href="javascript:;" id="MULTIPLE_DELETE_USERS" > Delete </a>
                            </li>
                            <li>
                                <a href="javascript:;" id="MULTIPLE_USERS" > Add Multiple Users </a>
                            </li>
                            <li>
                                <a href="javascript:;" id="EXPORT_TO_EXCELL"> Export to Excel </a>
                            </li>
                        </ul>
                  </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" align="left">
                <h4>Filter By</h4>
            </div>
            <div class="col-md-3">
                <label> User Type </label>
                <select class="bs-select form-control" name="fb_user_type" id="FB_USER_TYPE" data-actions-box="true">
                    <option value=''>All Users</option>
                    <option value='1'>OMSAR Administrator</option>
                    <option value='2'>Coordinators</option>
                    <option value='3'>Providers</option>
                    <option value='4'>Trainees</option>
                    <option value='5'>Trainers</option>
                </select>
            </div>
            <div class="col-md-3">
                <label>Beneficiaries</label>
                <select class="bs-select form-control" name="u_beneficiary" id="U_BENEFICIARY">
                   <option value="">All Beneficiary</option>
                    <?php $__currentLoopData = $beneficiaries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $benf_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <option value="<?php echo e($benf_info->sb_id); ?>"><?php echo e($benf_info->sb_name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                </select>
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-3"></div>
        </div>
        <div class="row" style="height:15px;"><div class="col-md-12"></div></div>
         <div class="row">
             <div class="col-md-10"></div>
             <div class="col-md-2" align="right">
             <button name="btn_add_user" id="BTN_ADD_USER" class="btn green capitalize"  type="button">Add</button>
             </div>
         </div>
        <div class="row">
             <div class="col-md-12">&nbsp;</div>
         </div>
        <div class="row">
            <div class="col-md-12">
                <div style="left:20%">
                    <div class="portlet-body flip-scroll" id="LISTUSERS">
                        <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
                            <thead class="flip-content">
                                <tr>
                                    <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_users" id="CK_ALL_USERS" class="group-checkable" data-set="#LISTUSERS .checkboxes" value="1" /></th>
                                    <th style="width:2px;">#</th>
                                    <th>Email</th>
                                    <th>Full Name</th>
                                    <th>beneficiary</th>
                                    <th></th>
                                    <th style="width:2px;" nowrap>Edit</th>
                                    <th style="width:2px;" nowrap>Delete</th>
                                </tr>
                                <tr>
                                    <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_users" id="CK_ALL_USERS" class="group-checkable" data-set="#LISTUSERS .checkboxes" value="1" /></th>
                                    <th style="width:2px;"></th>
                                    <th><input type="text" class="form-control" name="u_search_username" id="u_search_username" value="" /></th>
                                    <th><input type="text" class="form-control" name="u_search_fullname" id="u_search_fullname" value="" /></th>
                                    <th></th>
                                    <th></th>
                                    <th style="width:2px;" nowrap></th>
                                    <th style="width:2px;" nowrap></th>
                                </tr>
                            </thead>
                            <tbody class="ListUserGirds">
                            </tbody>
                        </table>
                     </div>
                </div>
             </div>
         </div>
         <div class="row">
             <div class="col-md-10" align="left">
                <ul id="UsersPagination" class="pagination-sm"></ul>
             </div>
             <div class="col-md-2" align="right">

             </div>
         </div>
         <div class="row">
             <div class="col-md-10"></div>
             <div class="col-md-2" align="right">
             <button name="btn_add_user" id="BTN_ADD_USER_BOTTOM" class="btn green capitalize"  type="button">Add</button>
             </div>
         </div>
     </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.alayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
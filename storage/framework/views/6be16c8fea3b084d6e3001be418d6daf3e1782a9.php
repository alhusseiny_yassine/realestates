<?php
/***********************************************************
adminrequestsmanagement.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 18, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
Control all requests sent to the system
***********************************************************/



?>



<?php $__env->startSection('themes'); ?>
    <link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('plugins'); ?>
        <script type="text/javascript" src="<?php echo e(url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js')); ?>" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo e(url('admin/assets/global/plugins/bootbox.min.js')); ?>" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js')); ?>" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js')); ?>" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo e(url('admin/assets/global/plugins/jquery.tablesorter/jquery.tablesorter.js')); ?>"></script>
        <script type="text/javascript" src="<?php echo e(url('js/modules/requests.module.js')); ?>"></script>
        <script type="text/javascript" src="<?php echo e(url('js/requests/arequestsmanagement.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="portlet blue box">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Requests Management </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
     <div class="portlet-body">
   <span id="hidden_fields">
            <input type="hidden" name="allrequests_page_number" value="1" />
            <input type="hidden" name="is_admin" value="1" />
        </span>
        <div class="row">
            <div class="col-md-12" align="right">
                    <div class="btn-group pull-right blue">
                        <button class="btn blue btn-outline dropdown-toggle" data-toggle="dropdown">Actions
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                              <li><a class="RejectRequests" href="javascript:;"> Reject Request </a></li>
                                <li><a class="ChangeRequestStatus" href="javascript:;"> Change Request Status </a></li>
                        </ul>
                  </div>
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">&nbsp;</div>
        </div>
         <div class="row">
            <div class="col-md-12">
                     <div class="search-filter">
                        <div class="search-label uppercase">Search By : </div>
                        
                        <div class="col-md-12">
                             <div class="form-group">
                                    <label class="control-label"> Courses </label><br/>
                                        
                                    <select class="form-control" name="sl_courses" id="SL_COURSES" data-label="left" data-select-all="true" data-width="100%" data-filter="true" data-action-onchange="true">
                                                <option value="0">--Select One--</option>
                                                  <?php $__currentLoopData = $lst_courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $course_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                            <option value="<?php echo e($course_info->c_id); ?>"><?php echo e($course_info->c_course_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                                </select>
                                </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                                <label class="control-label">Request Trainee </label>
                                <select  name="rq_request_trainee" id="RQ_REQUEST_TRAINEE" class="form-control bs-select">
                                    <option value="0">--Select One--</option>
                                    <?php $__currentLoopData = $lst_trainees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $tr_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <option  value="<?php echo e($tr_info->id); ?>"><?php echo e($tr_info->u_fullname); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                                <label class="control-label">Request Benficiaries </label>
                                 <select  name="sb_benf" id="SB_BENF"  class="form-control bs-select">
                                    <option value="0">--Select One--</option>
                                    <?php $__currentLoopData = $lst_benfs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $bn_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <option value="<?php echo e($bn_info->sb_id); ?>"><?php echo e($bn_info->sb_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                                <label class="control-label">Request Status </label>
                                 <select  name="rq_request_status" id="RQ_REQUEST_STATUS"  class="form-control bs-select">
                                    <option value="0">--Select One--</option>
                                    <?php $__currentLoopData = $lst_request_statuses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $rq_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <option value="<?php echo e($rq_info->rs_id); ?>"><?php echo e($rq_info->rs_status_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <button type="button" name="btn_reset_search" class="btn grey bold uppercase btn-block">Reset Search</button>
                            </div>
                            <div class="col-xs-6">
                                <button type="button" name="btn_advanced_search" class="btn grey-cararra font-blue bold btn-block">Advanced Search</button>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">&nbsp;</div>
        </div>
        <div class="row">
                 <div class="col-md-12">
                     <div style="left:20%" class="ListAllRequests">

                    </div>
                 </div>
             </div>
              <div class="row">
             <div class="col-md-10" align="left">
                <ul id="AllRequestsPagination" class="pagination-sm"></ul>
             </div>
             <div class="col-md-2" align="right">

             </div>
         </div>
             <div class="row">
                 <div class="col-md-8"></div>
                 <div class="col-md-4" align="right">
                    <button  name="btn_add_new_request" id="BTN_ADD_NEW_REQUEST" type="button" class="btn blue capitalize" >ADD New Request</button>
                 </div>
             </div>
     </div>
</div>
 <div id="SendRequest" class="modal fade" tabindex="-1"> </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.alayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
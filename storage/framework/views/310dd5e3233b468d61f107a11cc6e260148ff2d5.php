<?php
/************************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 6, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/
$session_user_id = session()->get('user_id');


foreach ($users as $index => $user_info)
{

    ?>
       <tr  class="odd gradeX" data-user_id="<?php echo $user_info['id']; ?>">
           <td><input type="checkbox" name="ck_user_<?php echo $user_info['id']; ?>" id="CK_USER_<?php echo $user_info['id']; ?>" class="checkboxes" value="<?php echo $user_info['id']; ?>" /></td>
           <td><?php echo $user_info['id']; ?></td>
           <td><?php echo $user_info['u_email']; ?></td>
           <td><?php echo $user_info['u_fullname']; ?></td>

           <td><?php echo isset( $user_info['fk_beneficiary_id'] ) ? $benf_info[ $user_info['fk_beneficiary_id'] ]->sb_name : "" ; ?></td>
           <td>
             <?php if ($user_info->isOnline()) { ?>
                    <img src="<?php echo e(url('imgs/online_user.png')); ?>" />
              <?php }else{ ?>
                    <img src="<?php echo e(url('imgs/offline_user.png')); ?>" />
              <?php }?>
           </td>
           <td>
           <a href="#"  id="EDIT_USER_<?php echo $user_info['id']; ?>" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a>
           </td>
           <?php
            $display = ($session_user_id == $user_info['id']) ? "display:none;" : "";
           ?>
           <td>
            <a href="#" style="<?php echo e($display); ?>"  id="DELETE_USER_<?php echo $user_info['id']; ?>" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a>
          </td>
       </tr>
    <?php
}
?>
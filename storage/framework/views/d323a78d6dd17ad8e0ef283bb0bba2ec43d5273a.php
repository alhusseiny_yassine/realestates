<?php
/***********************************************************
editcoursetype.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 27, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/


?>



<?php $__env->startSection('themes'); ?>
  <link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css')); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo e(url('admin/assets/global/plugins/jquery-minicolors/jquery.minicolors.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('plugins'); ?>
<script type="text/javascript"  src="<?php echo e(url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')); ?>"></script>
<script type="text/javascript"  src="<?php echo e(url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js')); ?>"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/jquery-minicolors/jquery.minicolors.min.js')); ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo e(url('js/modules/requeststatuses.module.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('js/requeststatuses/editrequeststatuses.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<div class="portlet blue box">
<div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Edit Request Status </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
    <div class="portlet-body">
         <form name="form_save_request_status" id="FORM_SAVE_REQUEST_STATUS">
            <div class="form-body">
                 <span id="hidden_fields">
                    <?php echo csrf_field(); ?>

                    <input type="hidden" name="rs_id" id="RS_ID" value="<?php echo e($cur_request_status_info->rs_id); ?>">
                </span>
                <div class="alert alert-success" style="display:none">
        				<strong>Success!</strong> Request Status Information is saved successfully!
        			</div>
        			<div class="alert alert-danger" style="display:none">
        				<strong>Error!</strong> You have some form errors. Please check below.
        			</div>
                <div class="row">
                    <div class="col-md-4">
                         <div class="form-group">
                            <label class="control-label">Request Status Name <span class="required"> * </span></label>
                            <input type="text" name="rs_status_name" id="RS_STATUS_NAME" class="form-control" required="required" maxlength="500"  value="<?php echo e($cur_request_status_info->rs_status_name); ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                         <div class="form-group">
                            <label class="control-label">Request Status Color </label>
                            <input type="text" name="rs_status_color" data-control="brightness" id="RS_STATUS_COLOR" class="form-control" required="required" maxlength="8"  value="<?php echo e($cur_request_status_info->rs_status_color); ?>" />
                        </div>
                    </div>
                </div>
               <div class="row" style="height:5px;"></div>
                <div class="row">
                    <div class="col-md-9"></div>
                    <div class="col-md-3" align="right">
                          <button type="submit" name="btn_save_request_status" id="BTN_SAVE_REQUEST_STATUS"  class="btn blue">Save</button>
                        <button type="reset" name="reset_form" id="RESET_FORM"  class="btn default">Cancel</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.alayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
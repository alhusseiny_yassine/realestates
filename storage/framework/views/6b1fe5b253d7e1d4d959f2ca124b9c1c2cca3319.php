<?php
/***********************************************************
trainees_efields.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 23, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :

***********************************************************/
{

}
?>
 <div class="col-md-4">
      <div class="form-group">
      <label  class="control-label">Beneficiary <span class="required"> * </span></label>
            <select name="fk_beneficiary_id" id="FK_BENEFICIARY_ID" class="form-control" style="width:100%">
                <option value="">-- Select One --</option>
                <?php $__currentLoopData = $beneficiaries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sb_index => $ben_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <option <?php echo e($user_info->fk_beneficiary_id == $ben_info['sb_id'] ? "selected" : ""); ?> value="<?php echo e($ben_info['sb_id']); ?>"><?php echo e($ben_info['sb_name']); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </select>
            <input type="hidden" name="ini_beneficiary_id" value="<?php echo e($user_info->fk_beneficiary_id); ?>" />
      </div>
</div>
 <div class="col-md-4" style="display: none">
    <div class="form-group">
        <label class="control-label">Employment Date</label>
        <input type="text" name="u_employment_date" maxlength="10" id="U_EMPLOYMENT_DATE" class="form-control" value="<?php echo e($user_info->u_employment_date == '' ? date('Y-m-d') : $user_info->u_employment_date); ?>" />
    </div>
</div>


 <div class="col-md-4">
      <div class="form-group">
      <label  class="control-label">Coordinator <span class="required"> * </span></label>
            <span class="CoordinatorDropdown">
                <select name="fk_coordinator_id" id="FK_COORDINATOR_ID" class="form-control" required="required"  style="width:100%">
                    <option value="">-- Select One --</option>
                    <?php $__currentLoopData = $coordinators; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $u_index => $u_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <option <?php echo e($user_info->fk_coordinator_id == $u_info['id'] ? "selected" : ""); ?> value="<?php echo e($u_info['id']); ?>"><?php echo e($u_info['u_fullname']); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                </select>
            </span>
            <input type="hidden" name="ini_coordinator_id" value="<?php echo e($user_info->fk_coordinator_id); ?>" />
      </div>
</div>

 <div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Coordinator Code <span class="required"> * </span> </label>
        <input type="text" name="u_coordinator_code" id="U_COORDINATOR_CODE" maxlength="32" class="form-control" required="required"  value="<?php echo e($user_info->u_coordinator_code); ?>" />
    </div>
</div>
 <div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Employment Degree</label>
        <input type="number" name="u_emp_degree" id="U_EMP_DEGREE" class="form-control" value="<?php echo e($user_info->u_emp_degree); ?>" />
        <input type="hidden" name="ini_emp_degree" value="<?php echo e($user_info->u_emp_degree); ?>" />
    </div>
</div>


 <div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Directorate General</label>
        <input type="text" name="u_trainee_section" id="U_TRAINEE_SECTION" maxlength="255" class="form-control" value="<?php echo e($user_info->u_trainee_section == 'NULL' ? '' : $user_info->u_trainee_section); ?>" />
        <input type="hidden" name="ini_trainee_section" value="<?php echo e($user_info->u_trainee_section); ?>" />
    </div>
</div>
 <div class="col-md-4">
      <div class="form-group">
      <label  class="control-label"> Directorate  </label>
           <span id="BeneficiaryDepartment"></span>
           <input type="hidden" name="ini_benf_department" value="<?php echo e($user_info->fk_benf_dep_id); ?>" />
      </div>
</div>
 <div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Service</label>
        <input type="text" name="u_trainee_service" id="U_TRAINEE_SERVICE" maxlength="255" class="form-control" value="<?php echo e($user_info->u_trainee_service); ?>" />
        <input type="hidden" name="ini_trainee_service" value="<?php echo e($user_info->u_trainee_service); ?>" />
    </div>
</div>

<div class="col-md-4">
      <div class="form-group">
      <label  class="control-label">Job Title <span class="required"> * </span></label>
             <select name="u_job_title" id="U_JOB_TITLE" class="form-control" required="required"  style="width:100%">
                    <option value="">-- Select One --</option>
                    <?php $__currentLoopData = $jobTitles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jt_index => $jt_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <option <?php echo e($user_info->u_job_title == $jt_info->jt_id ? "selected" : ""); ?> value="<?php echo e($jt_info->jt_id); ?>"><?php echo e($jt_info->jt_job_title); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                </select>
      </div>
</div>

 <div class="col-md-4">
      <div class="form-group">
      <label  class="control-label">Job Role <span class="required"> * </span></label>
            <select name="u_job_role" id="U_JOB_ROLE" class="form-control" required="required"  style="width:100%">
                    <option value="">-- Select One --</option>
                    <?php $__currentLoopData = $jobRoles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jr_index => $jr_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <?php echo e(print_r($jr_info)); ?>

                        <option <?php echo e($user_info->u_job_title == $jr_info->jr_id ? "selected" : ""); ?> value="<?php echo e($jr_info->jr_id); ?>"><?php echo e($jr_info->jr_job_role); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                </select>
      </div>
</div>

 <div class="col-md-4">
    <div class="form-group">
        <label class="control-label"> Bureau </label>
        <input type="text" name="u_trainee_bureau" id="U_TRAINEE_BUREAU" maxlength="255" class="form-control"  value="<?php echo e($user_info->u_trainee_bureau); ?>" />
        <input type="hidden" name="ini_trainee_bureau" value="<?php echo e($user_info->u_trainee_bureau); ?>" />
    </div>
</div>
 <div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Grade</label>
        <input type="text" name="u_gov_grade" id="U_GOV_GRADE" maxlength="45" class="form-control" value="<?php echo e($user_info->u_gov_grade); ?>" />
        <input type="hidden" name="ini_gov_grade" value="<?php echo e($user_info->u_gov_grade); ?>" />
    </div>
</div>
 <div class="col-md-4">
    <div class="form-group">
        <label class="control-label"> Grade Type </label>
        <input type="text" name="u_grade_type" id="U_GRADE_TYPE" maxlength="255" class="form-control"  value="<?php echo e($user_info->u_grade_type); ?>" />
    </div>
</div>
 <div class="col-md-4">
      <div class="form-group">
      <label  class="control-label">Preferred Days <span class="required"> * </span></label>
            <select name="u_preferred_train_days" id="u_preferred_train_days" class="form-control"  required="required"  style="width:100%">
                <option value="">-- Select One --</option>
                <?php $__currentLoopData = $preferred_days; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $d_index => $days_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <option <?php echo e($user_info->fk_coordinator_id == $days_info['ID'] ? "selected" : ""); ?> value="<?php echo e($days_info['ID']); ?>"><?php echo e($days_info['Name']); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </select>
      </div>
</div>
 <div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Preferred Location</label>
       <!-- <input type="text" name="u_preferred_train_location" id="U_PREFERRED_TRAIN_LOCATION" maxlength="255" class="form-control" value="<?php echo e($user_info->u_preferred_train_location); ?>" />-->

       <select name="u_preferred_train_location" id="U_PREFERRED_TRAIN_LOCATION" class="form-control" style="width:100%">
            <option value="">--Select One--</option>
            <?php
                foreach ($lst_districts as $key => $dst_info) {

                    ?>
                        <option <?php echo e($user_info->u_preferred_train_location == $dst_info->sd_id ? "selected" : ""); ?> value="<?php echo $dst_info->sd_id; ?>"><?php echo $dst_info->sd_district_name; ?></option>
                    <?php
                }
            ?>
        </select>
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Preferred Training Time</label>
       <select name="u_preferred_train_time" id="U_PREFERRED_TRAIN_TIME" class="form-control" style="width:100%">
            <option value="">--Select One--</option>
            <option <?php echo e($user_info->u_preferred_train_time == '8:00:00' ? "selected" : ""); ?> value="8:00:00">Morning</option>
            <option <?php echo e($user_info->u_preferred_train_time == '14:00:00' ? "selected" : ""); ?> value="14:00:00">Afternoon</option>
            <option <?php echo e($user_info->u_preferred_train_time == '20:00:00' ? "selected" : ""); ?> value="20:00:00">Evening</option>
        </select>
    </div>
</div>


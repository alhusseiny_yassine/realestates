<?php
/************************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>
<div class="portlet-body flip-scroll" id="LISTPROVINCES">
    <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
        <thead class="flip-content">
        <tr>
            <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_provinces" id="CK_ALL_PROVINCES" class="group-checkable" data-set="#LISTPROVINCES .checkboxes" value="1" /></th>
            <th style="width:2px">#</th>
            <th>Province Name </th>
            <th style="width:2px" nowrap>Edit</th>
            <th style="width:2px" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($provinces as $index => $prov_info)
        {
            ?>
               <tr data-sp_id="<?php echo e($prov_info->sp_id); ?>">
                   <td><input type="checkbox" class="checkboxes" name="ck_province_<?php echo e($prov_info->sp_id); ?>" id="CK_PROVINCE_<?php echo e($prov_info->sp_id); ?>" value="1" /></td>
                   <td><?php echo $prov_info->sp_id; ?></td>
                   <td><?php echo $prov_info->sp_provinces_name; ?></td>
                   <td> <a href="#"  id="EDIT_PROVINCE_<?php echo e($prov_info->sp_id); ?>" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
                   <td><a href="#"  id="DELETE_PROVINCE_<?php echo e($prov_info->sp_id); ?>" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>
</div>
<?php
/* * *********************************************************
  viewInfo.blade.php
  Product :
  Version : 1.0
  Release : 2
  Date Created : Dec 22, 2016
  Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
  All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2016

  Page Description :
  {Enter page description Here}
 * ********************************************************* */
?>
<form name="form_save_project" id="FORM_SAVE_PROJECT">
    <div class="row"></div>
    <div class="row col-md-12">
        <div class="col-md-4 col-xs-12 col-sm-6">
            <div class="form-group">
                <label  class="control-label"><b>Project Code : </b></label><br><?php echo e($project_info->pp_project_code); ?>

            </div>
        </div>
        <div class="col-md-4 col-xs-12 col-sm-6">
            <div class="form-group">
                <label  class="control-label"><b>Project Name  : </b></label><br><?php echo e($project_info->pp_project_title); ?>

            </div>
        </div>
        <div class="col-md-4 col-xs-12 col-sm-6">
            <div class="form-group">
                <label  class="control-label"><b>Project Type :</b></label><br><?php echo e($project_types[ $project_info->fk_ptype_id ]->pt_project_type); ?>

            </div>
        </div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-4 col-xs-12 col-sm-6">
            <div class="form-group">
                <label  class="control-label"><b>Donors :</b></label><br><?php echo e($lst_donors[ $project_info->fk_donor_id ]->d_donor_name); ?>

            </div>
        </div>
        <div class="col-md-4 col-xs-12 col-sm-6">
            <div class="form-group">
                <label  class="control-label"><b>Project Budget : </b></label><br><?php echo e($project_info->pp_project_budget); ?>

            </div>
        </div>
        <div class="col-md-4 col-xs-12 col-sm-6">
            <div class="form-group">
                <label  class="control-label"><b>Project Contract Number : </b></label><br><?php echo e($project_info->pp_contract_number); ?>

            </div>
        </div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-4 col-xs-12 col-sm-6">
            <div class="form-group">
                <label  class="control-label"><b>Project Start Date :</b></label><br><?php echo e($project_info->pp_project_start_date); ?>

            </div>
        </div>
        <div class="col-md-4 col-xs-12 col-sm-6">
            <div class="form-group">
                <label  class="control-label"><b>Project End Date :</b></label><br><?php echo e($project_info->pp_project_end_date); ?>

            </div>
        </div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-12 col-xs-12 col-sm-6">
            <div class="form-group">
                <label  class="control-label"><b>Project Description : </b></label><br><?php echo e($project_info->pp_project_description); ?>

            </div>
        </div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-4 col-xs-12 col-sm-6">
            <div class="form-group">
                <label  class="control-label"><b>Project Courses :</b></label><br/>
                <ul>
                    <?php $__currentLoopData = $lst_courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $course_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <?php if(in_array($course_info->c_id,$courses_array)): ?>
                    <li><?php echo e($course_info->c_course_name); ?></li>
                    <?php endif; ?> 
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                </ul>
            </div>
        </div>  
        <div class="col-md-4 col-xs-12 col-sm-6">
            <div class="form-group">
                <label  class="control-label"><b>Project Providers :</b></label><br/>
                <ul>
                    <?php $__currentLoopData = $lst_providers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $provider_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <?php if(in_array($provider_info->fk_provider_id,$providers_array)): ?>
                    <li><?php echo e($provider_info->u_fullname); ?></li>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                </ul>
            </div>
        </div>
        <div class="col-md-4 col-xs-12 col-sm-6">
            <div class="form-group">
                <label  class="control-label"><b>Accepted Request : </b></label><br/>
                <ul>
                    <?php $__currentLoopData = $lst_requests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $request_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <?php if(in_array($request_info->r_id,$requests_array)): ?>
                    <li><?php echo e($request_info->r_request_title); ?></li>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                </ul>
            </div>
        </div>
        <div class="col-md-12 col-xs-12 col-sm-12" align="right" style="clear: both;">
            <button type="button" name="btn_back" id="BTN_BACK" class="btn btn-default BtnBack">Back </button>
        </div>
    </div>

</form>
<?php
/***********************************************************
changerequeststatus.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 16, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
change request status
***********************************************************/

?>

<div class="portlet blue box" style="height: 100%;width:500px;">
<div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Change Request Status </div>
        <div class="tools">
        </div>
    </div>
    <div class="portlet-body">
        <form name="frm_request_status" id="FRM_REQUEST_STATUS">
            <span id="hidden_fields">
                  <?php echo csrf_field(); ?>

                  <input type="hidden" name="request_ids" value="<?php echo e($request_ids); ?>" />
            </span>
            <div class="row">
                 <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">Request Status <span class="required"> * </span></label>
                        <select name="rs_request_status" id="RS_REQUEST_STATUS" class="form-control">
                              <option value="">No Status</option>
                              <?php $__currentLoopData = $requests_statuses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $status_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                               <option value="<?php echo e($status_info->rs_id); ?>"><?php echo e($status_info->rs_status_name); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" name="btn_change_status" id="BTN_CHANGE_STATUS" class="btn btn-info" >Change Status</button>
                </div>
            </div>
        </form>
    </div>
</div>
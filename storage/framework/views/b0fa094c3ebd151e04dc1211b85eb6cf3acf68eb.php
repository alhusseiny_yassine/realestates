<?php
/***********************************************************
invoices.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Jul 3, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

?>




<?php $__env->startSection('plugins'); ?>
<script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/bootbox.min.js')); ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo e(url('js/modules/invoices.module.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('js/financial/invoicemanagement.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="portlet blue box">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Invoices Management </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
     <div class="portlet-body">
   <span id="hidden_fields">
            <input type="hidden" name="page_number" value="1" />
        </span>
        <div class="row">
            <div class="col-md-12" align="right">
                    <div class="btn-group pull-right blue">
                        <button class="btn blue btn-outline dropdown-toggle" data-toggle="dropdown">Actions
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                        </ul>
                  </div>
            </div>
        </div>
         <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Beneficiary : </label>
                    <span>
                        <select name="fk_beneficiary_id" id="FK_BENEFICIARY_ID" class="form-control">
                            <option value="0">Select Beneficiary</option>
                            <?php for($i = 0; $i < count($beneficiaries_array); $i++): ?>
                            <option value="<?php echo e($beneficiaries_array[$i]['sb_id']); ?>"><?php echo e($beneficiaries_array[$i]['sb_name'] . " - " . $beneficiaries_array[$i]['sb_code']); ?></option>
                            <?php endfor; ?>
                        </select>
                    </span>
                </div>
            </div>
            <div class="col-md-4 ProjectDropDown" ></div>
            <div class="col-md-4">&nbsp;</div>
        </div>
        <div class="row">
                 <div class="col-md-12">
                     <div style="left:20%" class="ListInvoicesGrid">

                    </div>
                 </div>
             </div>
              <div class="row">
             <div class="col-md-10" align="left">
                <ul id="InvoicePagination" class="pagination-sm"></ul>
             </div>
             <div class="col-md-2" align="right">

             </div>
         </div>
     </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.alayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
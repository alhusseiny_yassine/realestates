<?php
/***********************************************************
lst_groups.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 29, 2017
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

?>

<div class="portlet-body flip-scroll" id="LISTGROUPS">
    <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
        <thead class="flip-content">
        <tr>
            <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_groups" id="CK_ALL_GROUPS" class="group-checkable" data-set="#LISTGROUPS .checkboxes" value="1" /></th>
            <th>Group Id </th>
            <th>Group Code </th>
            <th>Course Namee </th>
            <th>Day </th>
            <th>Time </th>
            <th style="width:15px" nowrap>Edit Trainees</th>
            <th style="width:2px" nowrap>Edit</th>
            <th style="width:2px" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($project_group_info as $index => $pg_info )
        {
            ?>
               <tr data-pg_id="<?php echo e($pg_info->pg_id); ?>">
                   <td><input type="checkbox" class="checkboxes" name="ck_group_<?php echo e($pg_info->pg_id); ?>" id="CK_GROUP_<?php echo e($pg_info->pg_id); ?>" value="1" /></td>
                   <td><?php echo $pg_info->pg_id; ?></td>
                   <td><?php echo $pg_info->pg_group_code; ?></td> 
                   <td><?php echo $pg_info->pg_group_title; ?></td> 
                   <td><?php echo ($pg_info->pg_train_days > 0) ? $sys_days[$pg_info->pg_train_days]->Name : "N/A"; ?></td> 
                   <td><?php echo $pg_info->pg_trainee_time; ?></td> 
                   <td align="center"> <a href="#"   data-pg_id="<?php echo e($pg_info->pg_id); ?>" id="EDIT_TRAINEE_GROUP_<?php echo e($pg_info->pg_id); ?>" ><i class="fa fa-eye" aria-hidden="true" height="16"></i></a></td>
                   <td> <a href="#"   data-pg_id="<?php echo e($pg_info->pg_id); ?>" id="EDIT_GROUP_<?php echo e($pg_info->pg_id); ?>" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
                   <td><a href="#"   data-pg_id="<?php echo e($pg_info->pg_id); ?>" id="DELETE_GROUP_<?php echo e($pg_info->pg_id); ?>" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>
</div>
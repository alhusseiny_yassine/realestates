<table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
    <thead class="flip-content">
        <tr>
            <th style="width:5%;">#</th>
            <th style="width:25%;">Trainee</th>
            <th style="width:20%;">View</th>
    </thead>
    <tbody class="ListSurveyAnswers">
        <?php $__currentLoopData = $lst_trainees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $trainee_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
        <?php if( in_array( $trainee_info->id , $trainees_array ) ): ?>
        <tr  class="odd gradeX" data-trainee_id="<?php echo e($trainee_info->id); ?>">
            <td><?php echo e($trainee_info->id); ?></td>
            <td><?php echo e($trainee_info->u_fullname); ?></td>
            <td style="text-align: center"><a name="view_questions_answers_<?php echo e($trainee_info->id); ?>" id="VIEW_QUESTIONS_ANSWERS_<?php echo e($trainee_info->id); ?>" data-survey_id="<?php echo e($survey_id); ?>" href="#"><i class="fa fa-eye" aria-hidden="true" height="16" ></i></a></td>
        </tr>
        <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </tbody>
</table>
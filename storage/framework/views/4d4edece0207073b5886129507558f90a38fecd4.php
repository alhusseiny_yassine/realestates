<?php
/***********************************************************
trainees.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Mar 25, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/
$session_user_id = session()->get('user_id');

?>
<div class="portlet-body flip-scroll" id="LISTUSERS">
<table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
    <thead class="flip-content">
        <tr>
            <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_users" id="CK_ALL_USERS" class="group-checkable" data-set="#LISTUSERS .checkboxes" value="1" /></th>
            <th style="width:2px;">#</th>
            <th>Email</th>
            <th>Full Name</th>
            <th>beneficiary</th>
            <th>OnLine/Offline</th>
        </tr>
    </thead>
    <tbody class="ListUserGirds">
        <?php
        foreach ($most_recent_trainees as $index => $user_info)
        {

            ?>
               <tr  class="odd gradeX" data-user_id="<?php echo $user_info['id']; ?>">
                   <td><input type="checkbox" name="ck_user_<?php echo $user_info['id']; ?>" id="CK_USER_<?php echo $user_info['id']; ?>" class="checkboxes" value="<?php echo $user_info['id']; ?>" /></td>
                   <td><?php echo $user_info['id']; ?></td>
                   <td><?php echo $user_info['u_email']; ?></td>
                   <td><?php echo $user_info['u_fullname']; ?></td>

                   <td><?php echo isset( $user_info['fk_beneficiary_id'] ) ? $benf_info[ $user_info['fk_beneficiary_id'] ]->sb_name : "" ; ?></td>
                   <td>
                     <?php if ($user_info->isOnline()) { ?>
                            <img src="<?php echo e(url('imgs/online_user.png')); ?>" />
                      <?php }else{ ?>
                            <img src="<?php echo e(url('imgs/offline_user.png')); ?>" />
                      <?php }?>
                   </td>
               </tr>
            <?php
        }

        ?>
    </tbody>
</table>
</div>
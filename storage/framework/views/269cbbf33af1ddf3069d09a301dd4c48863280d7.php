<?php
/************************************************************
customers.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 29, 2017
Developed By  : Alhusseiny Yassine  PHP Department A&H S.A.R.L

Page Description :
view of display customersmanagement section
************************************************************/

?>

<?php $__env->startSection('themes'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('plugins'); ?>
<script type="text/javascript" src="<?php echo e(url('js/modules/customers.module.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('js/customers/customersmanagement.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="portlet blue box">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-users"></i> Customers Management 
        </div>
    </div>
     <div class="portlet-body">
        <span id="hidden_fields">
            <input type="hidden" id="page_number" name="page_number" value="1" />
        </span>
        <div class="row">
            <div class="col-md-12" align="right">
                    <div class="btn-group pull-right blue">
                        <button class="btn green-sharp btn-outline dropdown-toggle" data-toggle="dropdown">Actions
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="<?php echo e(url('/administrator/GeneratePDF/customers')); ?>" target="_blank" id="PRINT_LIST"> Print PDF </a>
                            </li>
                            <li>
                                <a href="<?php echo e(url('/administrator/GenerateExcelSheet/customers')); ?>" id="EXPORT_TO_EXCELL"> Export to Excel </a>
                            </li>
                        </ul>
                  </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" align="left">
                <h4>Filter By</h4>
            </div>
            <div class="col-md-4">
                <label> Customers Type </label>
                <select class="bs-select form-control" name="customer_type" id="CUSTOMER_TYPE" data-actions-box="true">
                    <option value=''>All Customers</option>
                    <option value='renter'>Renters</option>
                    <option value='buyer'>Buyers</option>
                </select>
            </div>
            <div class="col-md-4"></div>
            <div class="col-md-4"></div>
        </div>
        <div class="row" style="height:15px;"><div class="col-md-12"></div></div>
        <div class="row">
             <div class="col-md-12">&nbsp;</div>
         </div>
        <div class="row">
            <div class="col-md-12">
                <div style="left:20%">
                    <div class="portlet-body flip-scroll" id="LISTCUSTOMERS">
                        <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
                            <thead class="flip-content">
                                <tr>
                                    <th style="width:2px;">#</th>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th style="width:2px;" nowrap>Edit</th>
                                    <th style="width:2px;" nowrap>Delete</th>
                                </tr>
                                <tr>
                                    <th style="width:2px;"></th>
                                    <th><input type="text" class="form-control" name="c_search_fullname" id="c_search_fullname" value="" /></th>
                                    <th><input type="text" class="form-control" name="c_search_email"    id="c_search_email" value="" /></th>
                                    <th><input type="text" class="form-control" name="c_search_mobile"   id="c_search_mobile" value="" /></th>
                                    <th style="width:2px;" nowrap></th>
                                    <th style="width:2px;" nowrap></th>
                                </tr>
                            </thead>
                            <tbody class="ListCustomers">
                            </tbody>
                        </table>
                     </div>
                </div>
             </div>
         </div>
         <div class="row">
             <div class="col-md-10" align="center"><ul id="CustomersPagination" class="pagination-sm"></ul></div>
             <div class="col-md-2" align="right"></div>
         </div>
         <div class="row">
             <div class="col-md-10"></div>
             <div class="col-md-2" align="right">
             <button name="btn_add_customer" id="BTN_ADD_CUSTOMER" class="btn green capitalize"  type="button">Add Customer</button>
             </div>
         </div>
     </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.alayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php
/***********************************************************
displayprojectcourses.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Nov 13, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

$project_course_info = $project_course_info[0];
 
?>
<input type="hidden" name="pc_id" id="PC_ID" value="<?php echo e($project_course_info->pc_id); ?>" />
<div class="row">
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="form-group">
            <label  class="control-label">Maximum Number of Trainees In Class <span class="required"> * </span></label>
            <input type="number" name="pc_max_nbr_trainees" maxlength="4" id="PC_MAX_NBR_TRAINEES" class="form-control" value="<?php echo e($project_course_info->pc_max_nbr_trainees); ?>" />
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="form-group">
            <label  class="control-label">Course Weight <span class="required"> * </span></label>
            <input type="number" name="pc_course_weight" maxlength="4" id="PC_COURSE_WEIGHT" class="form-control" value="<?php echo e($project_course_info->pc_course_weight); ?>" />
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="form-group">
            <label  class="control-label">Course Cost (USD) <span class="required"> * </span></label>
            <input type="number" name="pc_course_cost" maxlength="4" id="PC_COURSE_COST" class="form-control" value="<?php echo e($project_course_info->pc_course_cost); ?>" />
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="form-group">
            <label  class="control-label">Number Of Sessions <span class="required"> * </span></label>
            <input type="number" name="pc_nbr_sessions" maxlength="4" id="PC_NBR_SESSION" class="form-control" value="<?php echo e($project_course_info->pc_nbr_sessions); ?>" />
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12" style="height:10px;">
         
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
         <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
                            <thead class="flip-content">
                                <tr>
                                    <th style="width:2px;">#</th>
                                    <th>Email</th>
                                    <th>Full Name</th>
                                    <th>beneficiary</th>
 
                                </tr>
                                </thead>
                                <tbody>
         <?php 
         foreach ($lst_trainees as $index => $user_info)
         {
         
             ?>
                <tr  class="odd gradeX">
                    <td><?php echo $user_info->id; ?></td>
                    <td><?php echo $user_info->u_email; ?></td>
                    <td><?php echo $user_info->u_fullname; ?></td>
         
                    <td><?php echo isset( $user_info->fk_beneficiary_id ) ? $benfs_array[ $user_info->fk_beneficiary_id ]->sb_name : "" ; ?></td>
                </tr>
             <?php
         }
         ?>
         </tbody>
         </table>
    </div>
    
</div>
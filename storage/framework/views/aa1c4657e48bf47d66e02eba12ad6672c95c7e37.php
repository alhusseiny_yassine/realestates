<?php
/***********************************************************
under_review_requests.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Mar 25, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

?>


<table class="table table-striped table-bordered table-hover" id="datatable_allrequests">
<thead>
    <tr role="row" class="heading">
        <th width="5%">
            <input type="checkbox" class="group-checkable" data-set="#datatable_allrequests .checkboxes" />
        </th>
        <th width="20%"> Beneficiary </th>
        <th width="20%"> Trainee </th>
        <th width="20%"> Course </th>
        <th width="20%"> Request Training Date </th>
        <th width="5%"> Priority </th>
        <th width="15%"> Status </th>
    </tr>
    </thead>
    <tbody>
        <?php $__currentLoopData = $most_recent_requests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $request_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
        <tr data-r_id="<?php echo e($request_info->r_id); ?>" style="background-color: <?php echo e($request_statuses_byid[ $request_info->fk_status_id ]['status_color']); ?>">
            <td><input type="checkbox" class="checkboxes" name="ck_request_<?php echo e($request_info->r_id); ?>" id="CK_REQUEST_<?php echo e($request_info->r_id); ?>" value="<?php echo e($request_info->r_id); ?>" /></td>
            <td><?php echo e(isset( $benf_info[$request_info->fk_beneficiary_id]->sb_name ) ? $benf_info[$request_info->fk_beneficiary_id]->sb_name : "N/A"); ?></td>
            <td><?php echo e(isset( $lst_users_data[$request_info->fk_trainee_id]['fullname'] ) ? $lst_users_data[$request_info->fk_trainee_id]['fullname'] : "N/A"); ?></td>
            <td><?php echo e(isset($request_courses_array[ $request_info->r_id ]) ? $request_courses_array[ $request_info->r_id ] : "N/A"); ?></td>
            <td><?php echo e($request_info->r_request_train_date); ?></td>
            <td><?php echo e($request_info->r_request_order); ?></td>
            <td><?php echo e($request_statuses_byid[ $request_info->fk_status_id ]['status_name']); ?></td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </tbody>
</table>
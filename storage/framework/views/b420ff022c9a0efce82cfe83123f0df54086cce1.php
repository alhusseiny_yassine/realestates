<?php
/* * *********************************************************
  coursesInfo.blade.php
  Product :
  Version : 1.0
  Release : 2
  Date Created : Aug 8, 2017
  Developed By  : alhusseiny yassine   PHP Department

  Page Description :
  {Enter page description Here}
 * ********************************************************* */
?>
<div class="portlet-body flip-scroll" id="">
    <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
        <thead class="flip-content">
            <tr>
                <th style="width:2px;">#</th>
                <th>Course Code</th>
                <th>Course Name</th>
                <th>Description</th>
                <th>Total Hours</th>
                <th>Category</th>
            </tr>
        </thead>
        <tbody class="">

            <?php $__currentLoopData = $lst_courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $course_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <?php if( in_array( $course_info['c_id'] , $courses_array ) ): ?>

            <tr>
                <td><?php echo $course_info['c_id']; ?></td>
                <td><?php echo $course_info['c_code']; ?></td>
                <td><?php echo $course_info['c_course_name']; ?></td>
                <td><?php echo $course_info['c_course_description']; ?></td>
                <td><?php echo $course_info['c_total_hours']; ?></td>
                <td><?php echo ( isset( $category_array[$course_info['fk_category_id']] ) ) ? $category_array[$course_info['fk_category_id']]->cc_course_category : ""; ?></td>
                <td></td>
            </tr>
            
            <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?> 
        </tbody>
    </table>
</div>
<?php
/************************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

foreach ($educationdegree as $index => $ed_info)
{
    ?>
       <tr data-ed_id="<?php echo e($ed_info->ed_id); ?>">
           <td><input type="checkbox" class="checkboxes" name="ck_edegree_<?php echo e($ed_info->ed_id); ?>" id="CK_EDEGREE_<?php echo e($ed_info->ed_id); ?>" value="1" /></td>
           <td><?php echo $ed_info->ed_id; ?></td>
           <td><?php echo $ed_info->ed_degree_name; ?></td>
           <td> <a href="#"  id="EDIT_EDEGREE_<?php echo e($ed_info->ed_id); ?>" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
           <td><a href="#"  id="DELETE_EDEGREE_<?php echo e($ed_info->ed_id); ?>" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
       </tr>
    <?php
}
?>
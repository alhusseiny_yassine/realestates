<?php
/***********************************************************
settings.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Dec 28, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">
                    <i class="icon-globe theme-font hide"></i>
                    <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                </div>
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_user_info" data-toggle="tab">Personal Info</a>
                    </li>
                    <li>
                        <a href="#tab_change_avatar" data-toggle="tab">Change Avatar</a>
                    </li>
                    <li>
                        <a href="#tab_change_password" data-toggle="tab">Change Password</a>
                    </li>
                    <?php if(Session('ut_user_type') == 4): ?>
                    <li>
                        <a href="#tab_education_info" data-toggle="tab"> Education Info </a>
                    </li>
                    <?php endif; ?>
                    <?php if(Session('ut_user_type') == 3): ?>
                    <li>
                        <a href="#tab_provider_location" data-toggle="tab"> Provider Location </a>
                    </li>
                   <?php endif; ?>
                </ul>
            </div>
            <div class="portlet-body">
                <div class="tab-content">
                    <!-- PERSONAL INFO TAB -->
                    <div class="tab-pane active" id="tab_user_info">
                        <form role="form" name="form_profile_info" id="FORM_PROFILE_INFO" action="#">
                            <span class="hiddenFields">
                             <?php echo csrf_field(); ?>

                            </span>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Full Name</label>
                                        <input type="text" placeholder="John" name="u_fullname" class="form-control" value="<?php echo e($front_user_info->u_fullname); ?>" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">User Name</label>
                                        <input type="text" placeholder="JDoe" name="u_username" value="<?php echo e($front_user_info->u_username); ?>"  class="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Mobile Number</label>
                                        <input type="text" name="u_mobile" placeholder="+1 646 580 DEMO (6284)" value="<?php echo e($front_user_info->u_mobile); ?>" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Email Address</label>
                                        <input type="text" name="u_email" placeholder="demo@something.com" value="<?php echo e($front_user_info->u_email); ?>" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Phone Number</label>
                                        <input type="text" name="u_phone" placeholder="+1 646 580 DEMO (6284)" value="<?php echo e($front_user_info->u_phone); ?>" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Website Url</label>
                                        <input type="text" placeholder="http://www.mywebsite.com" name="u_website" value="<?php echo e($front_user_info->u_website); ?>"  class="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                     <div class="form-group">
                                        <label class="control-label">Address</label>
                                        <textarea rows="" name="u_address" style="resize:none;" cols="" class="form-control" ><?php echo e($front_user_info->u_address); ?></textarea>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Employment Degree</label>
                                        <input type="number" name="u_emp_degree" id="U_EMP_DEGREE" class="form-control" value="<?php echo e($front_user_info->u_emp_degree); ?>" />
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Directorate General</label>
                                        <input type="text" name="u_trainee_section" id="U_TRAINEE_SECTION" maxlength="255" class="form-control" value="<?php echo e($front_user_info->u_trainee_section == 'NULL' ? '' : $front_user_info->u_trainee_section); ?>" />
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Service</label>
                                        <input type="text" name="u_trainee_service" id="U_TRAINEE_SERVICE" maxlength="255" class="form-control" value="<?php echo e($front_user_info->u_trainee_service); ?>" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"> Bureau </label>
                                        <input type="text" name="u_trainee_bureau" id="U_TRAINEE_BUREAU" maxlength="255" class="form-control"  value="<?php echo e($front_user_info->u_trainee_bureau); ?>" />
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Grade</label>
                                        <input type="text" name="u_gov_grade" id="U_GOV_GRADE" maxlength="45" class="form-control" value="<?php echo e($front_user_info->u_gov_grade); ?>" />
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"> Grade Type </label>
                                        <input type="text" name="u_grade_type" id="U_GRADE_TYPE" maxlength="255" class="form-control"  value="<?php echo e($front_user_info->u_grade_type); ?>" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                  <label  class="control-label">Preferred Days <span class="required"> * </span></label>
                                        <select name="u_preferred_train_days" id="u_preferred_train_days" class="form-control"  required="required"  style="width:100%">
                                            <option value="">-- Select One --</option>
                                            <?php $__currentLoopData = $preferred_days; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $d_index => $days_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                <option <?php echo e($front_user_info->fk_coordinator_id == $days_info['ID'] ? "selected" : ""); ?> value="<?php echo e($days_info['ID']); ?>"><?php echo e($days_info['Name']); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                        </select>
                                  </div>
                            </div>
                             <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Preferred Location</label>
                                   <select name="u_preferred_train_location" id="U_PREFERRED_TRAIN_LOCATION" class="form-control" style="width:100%">
                                        <option value="">--Select One--</option>
                                        <?php
                                            foreach ($lst_districts as $key => $dst_info) {

                                                ?>
                                                    <option <?php echo e($front_user_info->u_preferred_train_location == $dst_info->sd_id ? "selected" : ""); ?> value="<?php echo $dst_info->sd_id; ?>"><?php echo $dst_info->sd_district_name; ?></option>
                                                <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                             <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Preferred Training Time</label>
                                   <select name="u_preferred_train_time" id="U_PREFERRED_TRAIN_TIME" class="form-control" style="width:100%">
                                        <option value="">--Select One--</option>
                                        <option <?php echo e($front_user_info->u_preferred_train_time == '8:00:00' ? "selected" : ""); ?> value="8:00:00">Morning</option>
                                        <option <?php echo e($front_user_info->u_preferred_train_time == '14:00:00' ? "selected" : ""); ?> value="14:00:00">Afternoon</option>
                                        <option <?php echo e($front_user_info->u_preferred_train_time == '20:00:00' ? "selected" : ""); ?> value="20:00:00">Evening</option>
                                    </select>
                                </div>
                            </div>

                                <div class="col-md-12 margiv-top-10" align="right">
                                <a href="javascript:;" class="btn green btnSaveInfo"> Save Changes </a>
                            </div>
                            </div>
                        </form>
                    </div>
                    <!-- END PERSONAL INFO TAB -->
                    <!-- CHANGE AVATAR TAB -->
                    <div class="tab-pane" id="tab_change_avatar">
                        <p></p>
                        <form action="#" id="FRM_PROFILE_PIC" name="frm_profile_pic" role="form">
                        <?php echo csrf_field(); ?>

                            <div class="form-group">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                        <img id="PROFILE_PIC"  src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                    <div>
                                        <span class="btn default btn-file">
                                            <span class="fileinput-new"> Select image </span>
                                            <span class="fileinput-exists"> Change </span>
                                            <input type="file" id="U_PROFILE_PIC" name="u_profile_pic"> </span>
                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                    </div>
                                </div>
                                <div class="clearfix margin-top-10">
                                    <span class="label label-danger">NOTE! </span>
                                    <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                </div>
                            </div>
                            <div class="margin-top-10">
                                <button name="btn_upload_profile"  class="btn green"  >Submit</button>
                                <button name="btn_back"   class="btn default"> Cancel </button>
                            </div>
                        </form>
                    </div>
                    <!-- END CHANGE AVATAR TAB -->
                    <!-- CHANGE PASSWORD TAB -->
                    <div class="tab-pane" id="tab_change_password">
                        <form action="#" name="frm_change_password" id="frm_change_password">
                        <span class="hiddenFields">
                             <?php echo csrf_field(); ?>

                            </span>
                            <div class="form-group">
                                <label class="control-label">Current Password</label>
                                <input type="password" name="current_password" class="form-control" /> </div>
                            <div class="form-group">
                                <label class="control-label">New Password</label>
                                <input type="password" name="new_password_password" class="form-control" /> </div>
                            <div class="form-group">
                                <label class="control-label">Re-type New Password</label>
                                <input type="password" name="confirm_new_password_password" class="form-control" /> </div>
                            <div class="margin-top-10">
                                <a href="javascript:;" id="BTN_CHANGE_PASSWORD" class="btn green"> Change Password </a>
                            </div>
                        </form>
                    </div>
                    <!-- END CHANGE PASSWORD TAB -->
                    <!-- PRIVACY SETTINGS TAB -->
                    <?php if(Session('ut_user_type') == 4): ?>
                    <div class="tab-pane" id="tab_education_info">
                        <div class="EducationInfo"></div>
                    </div>
                    <?php endif; ?>
                    <!-- END PRIVACY SETTINGS TAB -->
                    <?php if(Session('ut_user_type') == 3): ?>
                    <div class="tab-pane" id="tab_provider_location">
                          <div class="ProviderLocation"></div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
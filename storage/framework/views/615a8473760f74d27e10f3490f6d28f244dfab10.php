<?php
/***********************************************************
class.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Dec 24, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

?>




<?php $__env->startSection('themes'); ?>
<link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(url('admin/assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')); ?>" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?php echo e(url('js/dhtmlxScheduler/codebase/dhtmlxscheduler.css')); ?>" type="text/css" media="screen" title="no title" charset="utf-8">
<style>
th{
    cursor: pointer;
}
#ModelPopUp{
	width:800px;
}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('plugins'); ?>

<script src="<?php echo e(url('js/dhtmlxScheduler/codebase/dhtmlxscheduler.js')); ?>" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo e(url('admin/assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')); ?>" type="text/javascript"></script>

<script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/bootbox.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js')); ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo e(url('admin/assets/global/plugins/jquery.tablesorter/jquery.tablesorter.js')); ?>"></script>

<script type="text/javascript"  src="<?php echo e(url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')); ?>"></script>
<script type="text/javascript"  src="<?php echo e(url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js')); ?>"></script>
<script src="<?php echo e(url('admin/assets/pages/scripts/ecommerce-orders-view.js')); ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo e(url('js/modules/projects.module.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('js/modules/class.module.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('js/classes/classesmanagement.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<form name="frm_projects_management" id="FRM_PROJECTS_MANAGEMENT">
    <div class="portlet blue box">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i> Classes Management </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
                <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                <a href="javascript:;" class="reload"> </a>
            </div>
        </div>
        <div class="portlet-body">
            <span id="hidden_fields">
                <input type="hidden" name="page_number" value="1" />
                <input type="hidden" name="display_type" value="list" />
                <?php echo csrf_field(); ?>

            </span>
            <div class="row">
                <div class="col-md-12" align="right">
                        <div class="btn-group pull-right blue">
                            <button class="btn blue btn-outline dropdown-toggle" data-toggle="dropdown">Actions
                                <i class="fa fa-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                    <!--  <li><a class="ChangeProjectsStatus" href="javascript:;"> Change Project Status </a></li> -->
                            </ul>
                      </div>
                </div>
            </div>
             <div class="row">
                <div class="col-md-12">&nbsp;</div>
            </div>
             <div class="row">
                <div class="col-md-4">
                    <label><b> Project : </b></label>
                    <select name="pp_project" id="PP_PROJECT" class="form-control">
                    <option value="0">--Select Project--</option>
                    <?php for($i = 0; $i < count($lst_projects); $i++): ?>
                    <option value="<?php echo e($lst_projects[$i]->pp_id); ?>"><?php echo e($lst_projects[$i]->pp_project_title); ?></option>
                    <?php endfor; ?>
                    </select>
                </div>
                <div class="col-md-4 CoursesDropdown">
                   
                </div>
                <div class="col-md-4">
                
                </div>
            </div>
             <div class="row">
                <div class="col-md-12">&nbsp;</div>
            </div>
             <div class="row">
                <div class="col-md-2">
                    <ul class="listDisplayType">
                        <li>
                            <a href="#" class='ChangeType' data-type='list'><i class="fa fa-list" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href="#" class='ChangeType' data-type='calendar'><i class="fa fa-calendar" aria-hidden="true"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-10">
                </div>
            </div>
            <div class="row">
                 <div class="col-md-12">
                     <div style="left:20%" class="ListClasses">

                    </div>
                 </div>
             </div>
            </div>
              <div class="row">
                 <div class="col-md-10" align="left">
                    <ul id="ClassesPagination" class="pagination-sm"></ul>
                 </div>
                 <div class="col-md-2" align="right">
    
                 </div>
            </div>
             <div class="row">
                 <div class="col-md-8"></div>
                 <div class="col-md-4" align="right">
                    <!-- <button  name="btn_add_class" id="BTN_ADD_CLASS" type="button" class="btn blue capitalize" >ADD New Class</button> -->
                 </div>
             </div>
     </div>
</div>

</form>
 <div id="ClassManagement" class="modal fade" tabindex="-1"> </div>
  <div id="ModelPopUp" class="modal fade" tabindex="-1"></div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.alayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
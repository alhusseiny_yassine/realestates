<?php
/***********************************************************
class.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Dec 29, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

?>





<?php $__env->startSection('themes'); ?>
<link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(url('admin/assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')); ?>" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?php echo e(url('js/dhtmlxScheduler/codebase/dhtmlxscheduler.css')); ?>" type="text/css" media="screen" title="no title" charset="utf-8">
<style>
th{
    cursor: pointer;
}
#ModelPopUp{
	width:800px;
}
.listDisplayType {
    list-style-type: none;
    position: relative;
}
.listDisplayType li{
    float: left;
}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('plugins'); ?>

<script src="<?php echo e(url('js/dhtmlxScheduler/codebase/dhtmlxscheduler.js')); ?>" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo e(url('admin/assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')); ?>" type="text/javascript"></script>

<script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/bootbox.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js')); ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo e(url('admin/assets/global/plugins/jquery.tablesorter/jquery.tablesorter.js')); ?>"></script>

<script type="text/javascript"  src="<?php echo e(url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')); ?>"></script>
<script type="text/javascript"  src="<?php echo e(url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js')); ?>"></script>
<script src="<?php echo e(url('admin/assets/pages/scripts/ecommerce-orders-view.js')); ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo e(url('js/modules/projects.module.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('js/modules/class.module.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('js/classes/classesmanagement.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<form name="frm_submit_survey" id="FRM_SUBMIT_SURVEY">
    <div class="portlet blue box">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i> Submit </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
                <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                <a href="javascript:;" class="reload"> </a>
            </div>
        </div>
        <div class="portlet-body">
            <span id="hidden_fields">
                <input type="hidden" name="page_number" value="1" />
                <input type="hidden" name="s_id" value="<?php echo e($s_id); ?>" />
                <?php echo csrf_field(); ?>

            </span>
            <div class="alert alert-success" style="display:none">
                                <strong>Success!</strong> Thank You Fro Your Answers!
                            </div>
                            <div class="alert alert-danger" style="display:none">
                                <strong>Error!</strong> You have some form errors. Please check below.
                            </div>
             <div class="row">
                <div class="col-md-12">&nbsp;</div>
            </div>
            <div class="row">
                 <div class="col-md-12">
                     <div style="left:20%" class="Questions">
                         <?php $__currentLoopData = $survey_questions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $question): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                         <div class=" form-group row col-md-12">
                         <label class="control-label"> <?php echo e($question->sq_question); ?> </label>
                         <textarea style="resize: none" rows="3" name="sq_question[<?php echo e($question->sq_id); ?>]" class="form-control sq_question" ></textarea>
                         </div>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    </div>
                 </div>
                <div class="col-md-8"></div>
                 <div class="col-md-4" align="right">
                    <button  name="btn_submit" id="BTN_SUBMIT" type="submit" class="btn blue capitalize" >Submit</button>
                 </div>
             </div>
            </div>
             <div class="row">
                 
             </div>
     </div>
</div>

</form>
 <div id="ClassManagement" class="modal fade" tabindex="-1"> </div>
  <div id="ModelPopUp" class="modal fade" tabindex="-1"></div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
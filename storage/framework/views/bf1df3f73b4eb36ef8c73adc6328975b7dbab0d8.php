<?php
/***********************************************************
classesDropdown.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Jan 29, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

?>
<div>
    <label><b> Classes : </b></label>
    <select class="bs-select form-control" name="fk_class_id" id="FK_CLASS_ID" data-actions-box="true">
        <option value="">-- Select One -- </option>
        <?php $__currentLoopData = $ProjectClasses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pc_index => $pc_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <option value="<?php echo e($pc_info->cls_id); ?>"><?php echo e($pc_info->cls_code); ?></option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </select>
</div>

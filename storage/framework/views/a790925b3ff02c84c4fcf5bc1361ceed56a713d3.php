<?php
/* * **********************************************************
  addsurvey.blade.php
  Product :
  Version : 1.0
  Release : 0
  Date Created : Aug 9, 2017
  Developed By  : Alhusseiny Yassine  PHP Department Softweb S.A.R.L
  All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2017

  Page Description :
  view of display surveys section
 * ********************************************************** */
?>


<?php $__env->startSection('themes'); ?>
<link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-datetimepicker_1/bootstrap-datetimepicker.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css')); ?>" rel="stylesheet" type="text/css" />

<?php $__env->stopSection(); ?>
<?php $__env->startSection('plugins'); ?>
<script src="<?php echo e(url('admin/assets/global/plugins/moment/moment.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/moment/moment-with-locale.js')); ?>"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-datetimepicker_1/bootstrap-datetimepicker.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')); ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo e(url('js/modules/surveys.module.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('js/surveys/savesurvey.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php
$selected = "";
$project_id="";
?>
<?php $__env->startSection('content'); ?>
<div class="portlet blue box">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Edit Survey </div>
    </div>
    <div class="portlet-body">
        <span id="hidden_fields">

        </span>
        <div class="row" style="height:15px;"><div class="col-md-12"></div></div>
        <div class="row">
            <div class="col-md-12">
                <div style="left:20%">
                    <div class="portlet-body flip-scroll">
                        <form name="form_save_survey" id="FORM_SAVE_SURVEY">
                            <?php echo csrf_field(); ?>

                            <input type="hidden" id="survey_id" name="survey_id" value="<?php echo e($survey->s_id); ?>">

                            <div class="alert alert-success" style="display:none">
                                <strong>Success!</strong> Survey Information is saved successfully!
                            </div>
                            <div class="alert alert-danger" style="display:none">
                                <strong>Error!</strong> You have some form errors. Please check below.
                            </div>
                            <div class="row">
                                <div class="col-md-4 form-group" id="listbeneficiaryprojects">
                                    <label class="control-label">Projects<span class="required"> * </span></label>
                                    <select data-type="projects" class="form-control selectpicker" name="fk_project_id" id="fk_project_id">
                                        <option value="" >Select</option>
                                        <?php $__currentLoopData = $lst_projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $project): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <option value="<?php echo e($project->pp_id); ?>" <?php echo e(( $survey->fk_project_id == $index ) ? "selected" : ""); ?> ><?php echo e($project->pp_project_title); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                    </select>
                                </div>
                                <div class="col-md-4 form-group" id="listprojectcourses">
                                    <label class="control-label">Courses<span class="required"> * </span></label>
                                    <input type="hidden" id="selected_project_id" value="<?php echo e($survey->fk_project_id); ?>">
                                    <select data-type="courses" class="form-control selectpicker" name="fk_course_id" id="fk_course_id">
                                        <option value="" >Select</option>
                                        <?php $__currentLoopData = $lst_courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $course): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <option value="<?php echo e($course->c_id); ?>" <?php echo e(( $survey->fk_course_id == $index ) ? "selected" : ""); ?> ><?php echo e($course->c_course_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                    </select>
                                </div>
                                <div class="col-md-4 form-group" id="coursetrainer">
                                    <label class="control-label">Trainer<span class="required"> * </span></label>
                                    <?php $__currentLoopData = $lst_trainers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $trainer): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <?php if( $survey->fk_trainer_id == $index ): ?>
                                    <input type="hidden" value="<?php echo e($trainer->id); ?>" name="fk_trainer_id" id="fk_trainer_id">
                                    <input type="text" disabled data-type="trainer" class="form-control" value="<?php echo e($trainer->u_fullname); ?>" name="trainer" id="trainer">
                                    <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                </div>
                            </div>
                            <div class="row" style="height: 15px"></div>                          
                            <div class="row">
                                <div class="col-md-8"></div>
                                <div class="col-md-4" align="right">
                                    <button type="button" id="BACK_FORM" name="back_form" class="btn default">Back</button>
                                    <button name="edit_survey" id="EDIT_SURVEY" class="btn green capitalize"  type="submit">Edit Survey</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.alayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
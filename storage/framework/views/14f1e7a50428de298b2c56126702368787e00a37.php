<?php
/***********************************************************
trainers_efields.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Mar 9, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here} $providers
***********************************************************/

?>

 <div class="col-md-4">
      <div class="form-group">
      <label  class="control-label">Provider </label>
            <select name="fk_provider_id" id="fk_provider_id" class="form-control" required="required"  style="width:100%">
                <option value="">-- Select One --</option>
                <?php $__currentLoopData = $providers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $u_index => $u_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <option <?php echo e($user_info->fk_provider_id == $u_info['id'] ? "selected" : ""); ?> value="<?php echo e($u_info['id']); ?>"><?php echo e($u_info['u_fullname']); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </select>
      </div>
</div>
<?php
/***********************************************************
viewSession.blade.php
 Product : 
 Version : 1.0
 Release : 2
 Date Created :  Aug 7, 2017
 Developed By  : Mohamad Mantach   PHP Department
 All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2017
 
 Page Description :
 View Session information in Popup
 ***********************************************************/

?>
<div class="portlet blue box" style="width:600px;">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> View Session </div>
            <div class="tools">
                <a href="javascript:;" class="modal_close"> <i class="fa fa-times" aria-hidden="true"></i> </a>
            </div>
    </div>
    <div class="portlet-body" style="background-color: white;height: 600px;">
        <div class="row">
            <div class="col-md-12">
                <h3><u>Project Info</u> :</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label  class="control-label"><b>Project Name</b> : </label><br>
                    <span><?php echo e($ProjectInfo->pp_project_title); ?></span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label  class="control-label"><b>Project Start Date</b> : </label><br>
                    <span><?php echo e($ProjectInfo->pp_project_start_date); ?></span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label  class="control-label"><b>Project End Date</b> : </label><br>
                    <span><?php echo e($ProjectInfo->pp_project_end_date); ?></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3><u>Session Info</u> :</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label  class="control-label"><b>Course Name</b> : </label><br>
                    <span><?php echo e($course_info->c_course_name); ?></span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label  class="control-label"><b>Session  Date</b> : </label><br>
                    <span><?php echo e($SessionInfo->sc_session_date); ?></span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label  class="control-label"><b>Session Time</b> : </label><br>
                    <span><?php echo e($SessionInfo->sc_session_time); ?></span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label  class="control-label"><b>Session Duration</b> : </label><br>
                    <span><?php echo e($SessionInfo->sc_session_duration); ?> hours</span>
                </div>
            </div>
            <?php if( $SessionInfo->sc_session_room_number != '' ): ?>
            <div class="col-md-4">
                <div class="form-group">
                    <label  class="control-label"><b>Room Number</b> : </label><br>
                    <span><?php echo e($SessionInfo->sc_session_room_number); ?></span>
                </div>
            </div>
            <?php endif; ?>
        </div>
        <div class="row" style="position: absolute;bottom: 10%;right: 5%;">
            <div class="col-md-12" align="right">
                <a href="<?php echo e(url("/omsar/TakeAttendance/".$SessionInfo->sc_id)); ?>" type="button" name="btn_take_attendence" id="BTN_TAKE_ATTENDENCE" class="btn btn-info">Take Attendance</a>
            </div>
        </div>
    </div>
</div>
<?php
/************************************************************
listsurveys.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 9, 2017
Developed By  : Alhusseiny Yassine PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2017

Page Description :
--
************************************************************/
?>
<?php $__currentLoopData = $surveys; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $survey_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
<tr  class="odd gradeX" data-survey_id="<?php echo e($survey_info->s_id); ?>">
    <td><?php echo e($survey_info->s_id); ?></td>
    <td><?php echo e(( isset( $lst_cources[ $survey_info->fk_course_id ] ) ) ? $lst_cources[ $survey_info->fk_course_id ]['c_course_name'] : ''); ?></td>
    <td><?php echo e(( isset( $lst_projects[ $survey_info->fk_project_id ] ) ) ? $lst_projects[ $survey_info->fk_project_id ]['pp_project_title'] : ''); ?></td>
    <td><?php echo e(( isset( $lst_trainers[ $survey_info->fk_trainer_id ] ) ) ? $lst_trainers[ $survey_info->fk_trainer_id ]['u_fullname'] : ''); ?></td>
    <td><?php echo e($survey_info->s_date); ?></td>
    <td style="text-align: center"><a href="<?php echo e(url('/surveys/ViewQuestions/'.$survey_info->s_id )); ?>"><i class="fa fa-eye" aria-hidden="true" height="16" ></i></a></td>
    <td style="text-align: center"><a href="<?php echo e(url('/surveys/ViewListSurveyAnswers/'.$survey_info->s_id )); ?>"><i class="fa fa-eye" aria-hidden="true" height="16" ></i></a></td>
    <td style="text-align: center"><a href="#"  id="EDIT_SURVEY_<?php echo e($survey_info->s_id); ?>" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
    <td style="text-align: center"><a href="#"  id="DELETE_SURVEY_<?php echo e($survey_info->s_id); ?>" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
</tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>

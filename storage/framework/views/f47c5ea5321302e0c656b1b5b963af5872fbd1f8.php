<?php
/************************************************************
listconfigurations.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Sep 3, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>



<?php $__env->startSection('plugins'); ?>
<script type="text/javascript" src="<?php echo e(url('admin/assets/global/plugins/bootbox/bootbox.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('js/utilities/configuration.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="portlet light">
    <div class="portlet-title">
    	<div class="caption">
    		<i class="fa fa-cogs font-green-sharp"></i>
    		<span class="caption-subject font-green-sharp bold uppercase">Configurations</span>
    		<span class="caption-helper"></span>
    	</div>
    </div>
    <div id="MSG_BOX" style="display:none" class="alert alert-success">
    </div>
    <div class="portlet-body flip-scroll">
    <form name="form_save_configurations" id="FORM_SAVE_CONFIGURATIONS">
         <table  class="table table-bordered table-striped table-condensed flip-content" style="width:100%">
            <thead class="flip-content">
                <tr>
                    <th style="width:2%">#</th>
                    <th style="width:30%">Index</th>
                    <th style="width:20%">Value</th>
                    <th style="width:48%">Description</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    foreach ($sys_configurations as $index => $config) {
                         ?>
                        <tr>
                           <td>
                           <input type="hidden" name="sa_id[]" value="<?php echo $config->sa_id; ?>" />
                           <input type="hidden" name="sa_config_index[]" value="<?php echo $config->sa_config_index; ?>" />
                            <?php echo $config->sa_id ?>
                           </td>
                           <td>
                                <?php echo $config->sa_config_index ?>
                           </td>
                           <td>
                                <input type="text" name="sa_config_value[]" class="form-control" value="<?php echo $config->sa_config_value; ?>" />
                           </td>
                           <td>
                                <input type="text" name="sa_config_description[]" class="form-control" value="<?php echo $config->sa_config_description ?>" />
                           </td>
                       </tr>
                        <?php
                    }
                ?>
            </tbody>
        </table>
    </form>
    </div>
    <div class="row">
        <div class="col-md-12" align="right">
            <button id="BTN_SAVE_CONFIGURATION" name="btn_save_configuration" type="button" class="btn btn-info"  >Save</button>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.alayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
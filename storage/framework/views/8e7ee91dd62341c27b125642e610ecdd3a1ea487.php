<?php
/***********************************************************
login.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 29, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

?>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>OMSAR - Training Management System - Office of the Minister of the State For Administrative Reform </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="OMSAR - Training Management System - Office of the Minister of the State For Administrative Reform"  name="description" />
        <meta content="OMSAR" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="<?php echo e(url('admin/assets/global/plugins/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo e(url('admin/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo e(url('admin/assets/global/plugins/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')); ?>" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo e(url('admin/assets/global/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo e(url('admin/assets/global/plugins/select2/css/select2-bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo e(url('admin/assets/global/css/components-md.min.css')); ?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo e(url('admin/assets/global/css/plugins-md.min.css')); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo e(url('admin/assets/layouts/layout/css/custom.min.css')); ?>" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?php echo e(url('admin/assets/pages/css/login-front.min.css')); ?>" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />


        <script type="text/javascript">
var slider_array = [];
<?php
foreach ($slideImages as $index => $media_info)
{
  ?>
  slider_array.push("<?php echo url('/') . "/" . Config::get( 'constants.ALBUMS_PATH') . $media_info->cm_media_base_dir . $media_info->cm_media_file_name  . "." . $media_info->cm_media_file_extention ; ?>");
 <?php
}
?>
</script>
        </head>
    <!-- END HEAD -->

    <body class=" login">
    <span id="hidden_fields">
        <input type="hidden" name="base_url" id="BASE_URL" value="<?php echo e(url('/')); ?>" />
    </span>
        <!-- BEGIN : LOGIN PAGE 5-1 -->
        <div class="user-login-5">
            <div class="row bs-reset">
                <div class="col-md-6 bs-reset mt-login-5-bsfix">
                    <div class="login-bg" style="background-image:url('<?php echo e(url('imgs/logo.jpg')); ?>')">
                        <img class="login-logo" src="<?php echo e(url('imgs/logo.jpg')); ?>" /> </div>
                </div>
                <div class="col-md-6 login-container bs-reset mt-login-5-bsfix">
                    <div class="login-content">
                        <h1>OMSAR Trainee Login</h1>
                        <p>  </p>
                        <form name='login_form' id='LOGIN_FORM' class="login-form">
                            <span id="hidden_fields">
                                <?php echo csrf_field(); ?>

                                <input type="hidden" name="ut_user_type" id="UT_USER_TYPE" value="<?php echo e(Crypt::encrypt( App\model\Users\UserTypes::TRAINEES_USER_TYPE_ID )); ?>" />
                            </span>
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                <span class='Msg'>Enter any username and password. </span>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" name="ut_username" type="text" autocomplete="off" placeholder="Username" name="username" required/> </div>
                                <div class="col-xs-6">
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" name="ut_password" type="password" autocomplete="off" placeholder="Password" name="password" required/> </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="rem-password">
                                        <label class="rememberme mt-checkbox mt-checkbox-outline">
                                            <input type="checkbox" name="ut_remember" value="1" /> Remember me
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-8 text-right">
                                    <div class="forgot-password">
                                        <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
                                        <a href="javascript:;" id="user-registration" class="forget-password"> Registration </a>
                                    </div>
                                    <button name="btn_login" id="BTN_LOGIN" class="btn green" type="submit">Sign In</button>
                                </div>
                            </div>
                        </form>
                        <!-- BEGIN FORGOT PASSWORD FORM -->
                        <form class="forget-form" name="frm_forgot_password" id="FRM_FORGOT_PASSWORD" action="javascript:;" method="post" style="display:none">
                            <h3 class="font-green">Forgot Password ?</h3>
                            <p> Enter your e-mail address below to reset your password. </p>
                             <?php echo csrf_field(); ?>

                            <div class="form-group">
                                <input name="fuser_email" class="form-control placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Email" /> </div>
                            <div class="form-actions">
                                <button type="button" id="back-btn" class="btn green btn-outline">Back</button>
                                <button name="btn_reset_password" id="BTN_RESET_PASSWORD" type="submit" class="btn btn-success uppercase pull-right">Submit</button>
                            </div>
                        </form>
                        <!-- END FORGOT PASSWORD FORM -->
                        <!-- BEGIN REGISTRATION FORM -->
                        <form class="registration-form" style="margin-bottom:50px" id="REGISTRATION_FORM" action="javascript:;" method="post" style="display:none">
                            <div  class="form-body">
                                <span id="hidden_fields">
                                    <?php echo csrf_field(); ?>

                                </span>
                                    <h3 class="font-green">Registration Form</h3>
                                    <p></p>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="form-field">Email <span class="required"> * </span> </label>
                                                    <input class="form-control" type="text" autocomplete="off" name="ur_email" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="form-field">Password <span class="required"> * </span></label>
                                                     <input class="form-control" id="UR_PASSWORD" type="password" autocomplete="off" name="ur_password" />
                                                 </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                             <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="form-field">Retype - Password <span class="required"> * </span></label>
                                                    <input class="form-control" type="password" autocomplete="off" name="ur_retype_password" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="form-field">Full Name <span class="required"> * </span></label>
                                                     <input class="form-control" type="text" autocomplete="off" name="u_fullname" />
                                                </div>
                                            </div>
                                         </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="form-field"> Beneficiary  <span class="required"> * </span> </label>
                                                     <select name="ur_beneficiary_id" id="UR_BENEFICIARY_ID" class="form-control" style="width:100%">
                                                        <option value="">-- Select One --</option>
                                                        <?php $__currentLoopData = $beneficiaries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sb_index => $ben_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                            <option  value="<?php echo e($ben_info['sb_id']); ?>"><?php echo e($ben_info['sb_name']); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                                    </select>
                                                 </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="form-field">Coordinator Code <span class="required"> * </span></label>
                                                    <input type="text" name="ur_coordinator_code" id="UR_COORDINATOR_CODE" maxlength="32" class="form-control" value="" />
                                               </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <div class="g-recaptcha" data-sitekey="6LcQ_AcUAAAAACMKKAxe-lAp4nMs8YlVP62blBZG"></div>
                                               </div>
                                            </div>
                                        </div>
                                     </div>
                                    <div class="form-actions">
                                        <button type="button" id="back-reg-btn" class="btn green btn-outline">Back</button>
                                       <button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
                                    </div>
                            </div>
                        </form>
                        <!-- END REGISTRATION FORM -->
                    </div>
                    <div class="login-footer">
                        <div class="row bs-reset">
                            <div class="col-xs-5 bs-reset">
                                
                            </div>
                            <div class="col-xs-7 bs-reset">
                                <div class="login-copyright text-right">
                                    <p>Copyright &copy; OMSAR <?php echo e(date('Y')); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END : LOGIN PAGE 5-1 -->
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script>
<script src="../assets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/jquery.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/bootstrap/js/bootstrap.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/js.cookie.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/jquery.blockui.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')); ?>" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo e(url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/select2/js/select2.full.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/backstretch/jquery.backstretch.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/bootbox.min.js')); ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo e(url('admin/assets/global/scripts/app.min.js')); ?>" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo e(url('admin/assets/global/plugins/select2/js/select2.full.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/pages/scripts/login-front.js')); ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>
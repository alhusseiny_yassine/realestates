<?php
/***********************************************************
provider_efields.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 23, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

?>

 <div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Provider Code</label>
        <input type="text" name="u_registration_code" maxlength="100" id="U_REGISTRATION_CODE" class="form-control" value="" />
    </div>
</div>
 <div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Provider Contact Person</label>
        <input type="text" name="u_contact_person" maxlength="100" id="U_CONTACT_PERSON" class="form-control" value="" />
    </div>
</div>
<?php
/***********************************************************
login.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 22, 2016
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
Login page for Admin Section
***********************************************************/

?>

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>OMSAR - Administrator</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
 <script src="<?php echo e(url('admin/assets/global/plugins/jquery.min.js')); ?>" type="text/javascript"></script>
<link href="<?php echo e(url('admin/assets/global/plugins/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(url('admin/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(url('admin/assets/global/plugins/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo e(url('admin/assets/pages/css/login-admin.css')); ?>" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<!-- DOC: To use 'material design' style just load 'components-md.css' stylesheet instead of 'components.css' in the below style tag -->

<link href="<?php echo e(url('admin/assets/global/css/components-md.css')); ?>" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(url('admin/assets/layouts/layout/css/custom.css')); ?>" rel="stylesheet" type="text/css"/>

<!-- <link rel="shortcut icon" href="favicon.ico"/> -->

</script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class=" login">
    <span id="hidden_fields">
        <input type="hidden" name="base_url" id="BASE_URL" value="<?php echo e(url('/')); ?>" />
    </span>
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="http://www.omsar.gov.lb/" target="_blank">
                <img src="<?php echo e(url('imgs/minstry-logo.gif')); ?>" alt="Office of the Minister of the State For Administrative Reform" /> </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" name="form_login" method="post">
                 <span id="hidden_fields">
                      <?php echo csrf_field(); ?>

                      <input type="hidden" name="user_type" id="USER_TYPE" value="<?php echo e(Crypt::encrypt( App\model\Users\UserTypes::ADMIN_USER_TYPE_ID )); ?>" />
                </span>
                <div class="row">
                    <div class="col-md-12" align="center">
                        <h3 style="font-size: 21px" class="form-title"> OMSAR - ADMINISTRATION </h3>
                    </div>
                </div>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter any username and password. </span>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" /> </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>
                </div>
                <div class="form-actions">
                    <label class="rememberme mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="ua_remember" value="1" /> Remember me
                        <span></span>
                    </label>
                    <button type="submit" name="" class="btn green pull-right"> Login </button>
                </div>
                <div class="forget-password">
                    <h4>Forgot your password ?</h4>
                    <p> no worries, click
                        <a href="javascript:;" id="forget-password"> here </a> to reset your password. </p>
                </div>
            </form>
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <form class="forget-form" name="frm_forgot_password" id="FRM_FORGOT_PASSWORD" action="javascript:;" method="post" style="display:none">
                <h3 class="font-green">Forgot Password ?</h3>
                <p> Enter your e-mail address below to reset your password. </p>
                <?php echo csrf_field(); ?>

                <div class="form-group">
                    <input name="fuser_email" class="form-control placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Email" /> </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn green btn-outline">Back</button>
                    <button name="btn_reset_password" id="BTN_RESET_PASSWORD" type="submit" class="btn btn-success uppercase pull-right">Submit</button>
                </div>
            </form>
            <!-- END FORGOT PASSWORD FORM -->
        </div>
        <!-- END LOGIN -->
        <!--[if lt IE 9]>
<script src="<?php echo e(url('admin/assets/global/plugins/respond.min.js')); ?>"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/excanvas.min.js')); ?>"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/ie8.fix.min.js')); ?>"></script>
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo e(url('admin/assets/global/plugins/bootstrap/js/bootstrap.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/js.cookie.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/bootbox.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/jquery.blockui.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')); ?>" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo e(url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/select2/js/select2.full.min.js')); ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo e(url('admin/assets/global/scripts/app.min.js')); ?>" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo e(url('admin/assets/pages/scripts/login.js')); ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
   </html>
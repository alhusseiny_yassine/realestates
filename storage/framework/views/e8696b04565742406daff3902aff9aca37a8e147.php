<?php $__currentLoopData = $groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $group): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
<tr  class="odd gradeX" data-group_id="<?php echo e($group->pd_id); ?>">
    <td><?php echo e($group->pg_id); ?></td>
    <td><?php echo e($group->pg_group_title); ?></td>
    <td><?php echo e($group->pg_group_description); ?></td>
    <td><?php echo e($group->pg_start_date); ?></td>
    <td><?php echo e($group->pg_end_date); ?></td>
    <td><a href="#"  id="EDIT_TRAINEE_GROUP_<?php echo e($group->pd_id); ?>" data-pg_id="<?php echo e($group->pg_id); ?>" ><i class="fa fa-eye" aria-hidden="true" height="16" ></i></a></td>
    <td><a href="#"  id="EDIT_GROUP_<?php echo e($group->pd_id); ?>" data-pg_id="<?php echo e($group->pg_id); ?>" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
    <td><a href="#"  id="DELETE_GROUP_<?php echo e($group->pd_id); ?>" data-pg_id="<?php echo e($group->pg_id); ?>" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
</tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
<?php
/***********************************************************
lstmysessions.blade.php
 Version : 1.0
 Developed By  : alhusseiny   PHP Department
 
 Page Description :
 {Enter page description Here}
 ***********************************************************/

?>

<table class="table table-striped table-bordered table-hover" id="datatable_allrequests">
<thead>
    <tr role="row" class="heading">
        <th width="10%"> Session ID </th>
        <th width="30%"> Session Code </th>
        <th width="20%"> Date </th>  
        <th width="20%"> Time </th> 
        <th width="10%"> Duration (hours) </th>
    </tr>
    </thead>
    <tbody>
        <?php for($i = 0; $i < count($class_sessions); $i++): ?>
        <tr data-sc_id="<?php echo e($class_sessions[$i]['sc_id']); ?>">
            <td><?php echo e($class_sessions[$i]['sc_id']); ?></td>
            <td><?php echo e($class_sessions[$i]['sc_session_code']); ?></td>
            <td><?php echo e($class_sessions[$i]['sc_session_date']); ?></td>
            <td><?php echo e($class_sessions[$i]['sc_session_time']); ?></td>
            <td><?php echo e($class_sessions[$i]['sc_session_duration']); ?></td>
        </tr>
        <?php endfor; ?>
    </tbody>
</table>
<?php
/************************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>
<div class="portlet-body flip-scroll" id="LISTDONORS">
    <table  class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
        <thead class="flip-content">
        <tr>
            <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_donors" id="CK_ALL_DONORS" class="group-checkable" data-set="#LISTDONORS .checkboxes" value="1" /></th>
            <th style="width:2px">#</th>
            <th>Donor Name </th>
            <th>Number of Projects</th>
            <th style="width:2px" nowrap>Edit</th>
            <th style="width:2px" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($donors as $index => $donor)
        {
            ?>
               <tr data-d_id="<?php echo e($donor->d_id); ?>">
                   <td><input type="checkbox" class="checkboxes" name="ck_donor_<?php echo e($donor->d_id); ?>" id="CK_DONOR_<?php echo e($donor->d_id); ?>" value="1" /></td>
                   <td><?php echo $donor->d_id; ?></td>
                   <td><?php echo $donor->d_donor_name; ?></td>
                   <td><?php echo $donor->d_nbr_project; ?></td>
                   <td> <a href="#"  id="EDIT_DONOR_<?php echo e($donor->d_id); ?>" ><i class="fa fa-pencil-square-o" aria-hidden="true" height="16" ></i></a></td>
                   <td><a href="#"  id="DELETE_DONOR_<?php echo e($donor->d_id); ?>" ><i class="fa fa-minus-circle" aria-hidden="true" height="16" ></i></a></td>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>
</div>
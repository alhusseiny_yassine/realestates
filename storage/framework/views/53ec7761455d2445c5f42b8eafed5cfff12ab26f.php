<?php
/***********************************************************
thumbmyprojects.blade.php
 Product : 
 Version : 1.0
 Release : 2
 Date Created :  Jul 28, 2017
 Developed By  : Mohamad Mantach   PHP Department
 All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2017
 
 Page Description :
 {Enter page description Here}
 ***********************************************************/
?>

<div style="width:100%">
<ul class="GalleryItems">
	<?php for($i = 0; $i < count($projects_info); $i++): ?>
		<li  class='ProjectItem'   data-pp_id="<?php echo e($projects_info[$i]['pp_id']); ?>" >
			<img src="<?php echo e(url('imgs/project.jpg')); ?>" style="" />
			<div class="ProjectTitle">
			<a class='ViewClasses' href="#"><?php echo e($projects_info[$i]['pp_project_title']); ?></a>
			</div>
		</li>
    <?php endfor; ?>
</ul>
</div>

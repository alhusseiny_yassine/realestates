<?php
/***********************************************************
myprofile.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 14, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
My profile page related to to trainee , coordinator  , trainer or provider
***********************************************************/

{/** INITIALISATION BLOCK */
    $ut_user_profile_url    = Session('ut_user_profile_url');
    $ut_user_fullname       = Session('ut_user_fullname');
    $u_about                = $user_info->u_about;
    $u_website              = $user_info->u_website;
}






?>



<?php $__env->startSection('themes'); ?>
    <link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(url('admin/assets/pages/css/profile.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('plugins'); ?>
    <script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/jquery.sparkline.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/global/plugins/gmaps/gmaps.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('js/modules/users.module.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('js/modules/fprofile.module.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('js/users/fmyprofile.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(url('admin/assets/pages/scripts/timeline.min.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="row">
        <div class="col-md-12">
            <input type="hidden" name="user_id" id="USER_ID" value="<?php echo e(Session('ut_user_id')); ?>" />
            <!-- BEGIN PROFILE SIDEBAR -->
            <div class="profile-sidebar">
                <!-- PORTLET MAIN -->
                <div class="portlet light profile-sidebar-portlet bordered">
                    <!-- SIDEBAR USERPIC -->
                    <div class="profile-userpic">
                        <img id="SECTION_PROFILE_PIC" src="<?php echo e($ut_user_profile_url); ?>" class="img-responsive" alt="<?php echo e($ut_user_fullname); ?>"> </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name"> <?php echo e($ut_user_fullname); ?> </div>
                        <div class="profile-usertitle-job"> Developer </div>
                    </div>
                    <!-- END SIDEBAR USER TITLE -->
                    <!-- SIDEBAR BUTTONS -->

                    <!-- END SIDEBAR BUTTONS -->
                    <!-- SIDEBAR MENU -->
                    <div class="profile-usermenu">
                        <ul class="nav">
                            <li class="active">
                                <a class="MyprofileDashboard" href="#">
                                    <i class="icon-home"></i> Overview </a>
                            </li>
                            <li>
                                <a class="MyProfileAccountSettings" href="#">
                                    <i class="icon-settings"></i> Account Settings </a>
                            </li>
                        </ul>
                    </div>
                    <!-- END MENU -->
                </div>
                <!-- END PORTLET MAIN -->
                <!-- PORTLET MAIN -->
                <div class="portlet light bordered">
                    <!-- STAT -->
                    <div class="row list-separated profile-stat">
                        <div class="col-md-4 col-sm-4 col-xs-6">
                            <div class="uppercase profile-stat-title"> <?php echo e($info['requests']); ?> </div>
                            <div class="uppercase profile-stat-text">  Requests </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-6">
                            <div class="uppercase profile-stat-title"> <?php echo e($info['classes']); ?> </div>
                            <div class="uppercase profile-stat-text"> Classes </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-6">
                            <div class="uppercase profile-stat-title"> <?php echo e($info['sessions']); ?> </div>
                            <div class="uppercase profile-stat-text"> Sessions </div>
                        </div>
                    </div>
                    <!-- END STAT -->
                    <div>
                        <h4 class="profile-desc-title">About <?php echo e($ut_user_fullname); ?></h4>
                        <span class="profile-desc-text"> <?php echo e($u_about); ?> </span>
                        <div class="margin-top-20 profile-desc-link">
                            <i class="fa fa-globe"></i>
                            <a href="<?php echo e($u_website); ?>" target="_blank"><?php echo e($u_website); ?></a>
                        </div>
                    </div>
                </div>
                <!-- END PORTLET MAIN -->
            </div>
            <!-- END BEGIN PROFILE SIDEBAR -->
            <!-- BEGIN PROFILE CONTENT -->
            <div class="profile-content">
                <?php echo view('frontend.profile.dashboard'); ?>

            </div>
            <!-- END PROFILE CONTENT -->
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
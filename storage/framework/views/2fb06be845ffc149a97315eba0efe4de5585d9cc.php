<?php
/***********************************************************
requests_action.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Apr 10, 2017
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

?>

<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title tabbable-line">
                                    <div class="caption">
                                        <i class=" icon-social-twitter font-dark hide"></i>
                                        <span class="caption-subject font-dark bold uppercase">Notifications Requests </span>
                                    </div>
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#tab_actions_pending" data-toggle="tab"> Pending </a>
                                        </li>
                                        <li>
                                            <a href="#tab_actions_completed" data-toggle="tab"> Completed </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="portlet-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_actions_pending">
                                            <!-- BEGIN: Actions -->
                                            <?php $__currentLoopData = $projects_notifications_pending; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $nt_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                            <?php
                                             $user_id = $nt_info->fk_created_by;


                                             $profile_path     = public_path().'/'.Config::get('constants.PROFILE_PICTURE_PATH') . $users_array[$user_id]->u_avatar_base_src . $users_array[$user_id]->u_avatar_filename . "." . $users_array[$user_id]->u_avatar_extentions;
                                             $profile_url = url('/').'/'.Config::get('constants.PROFILE_PICTURE_PATH') . $users_array[$user_id]->u_avatar_base_src . $users_array[$user_id]->u_avatar_filename . "." . $users_array[$user_id]->u_avatar_extentions;
                                             if(!is_file($profile_path))
                                             {
                                                 $profile_url = url('imgs/avatar.png');
                                             }
                                             $date = date('Y-m-d',strtotime($nt_info->nr_notification_date));
                                             $time = date('H:i:s',strtotime($nt_info->nr_notification_date));
                                            ?>
                                            <div data-nr_id="<?php echo e($nt_info->nr_id); ?>" class="mt-actions NotificationRequest">
                                                <div class="mt-action">
                                                    <div class="mt-action-img">
                                                        <img style="height: 64px" src="<?php echo e($profile_url); ?>" /> </div>
                                                    <div class="mt-action-body">
                                                        <div class="mt-action-row">
                                                            <div class="mt-action-info ">
                                                                <div class="mt-action-icon ">
                                                                </div>
                                                                <div class="mt-action-details ">
                                                                    <span class="mt-action-author"><?php echo e($users_array[$user_id]->u_fullname); ?></span>
                                                                    <p class="mt-action-desc"><?php echo $nt_info->nr_notification_text; ?></p>
                                                                </div>
                                                            </div>
                                                            <div class="mt-action-datetime ">
                                                                <span class="mt-action-date"><?php echo $date; ?></span>
                                                                <span class="mt-action-dot bg-green"></span>
                                                                <span class="mt-action-time"><?php echo $time; ?></span>
                                                            </div>
                                                            <div class="mt-action-buttons ">
                                                                <div class="btn-group btn-group-circle" style="white-space: nowrap;width:200px;">
                                                                    <button type="button" name="btn_approve_<?php echo e($nt_info->nr_id); ?>" class="btn btn-outline green btn-sm">Confirm </button>
                                                                    <button type="button" name="btn_postpone_<?php echo e($nt_info->nr_id); ?>" class="btn btn-outline red btn-sm">Postpone</button>
                                                                    <button type="button" name="btn_reject_<?php echo e($nt_info->nr_id); ?>" class="btn btn-outline red btn-sm">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                            <!-- END: Actions -->
                                        </div>
                                        <div class="tab-pane" id="tab_actions_completed">
                                            <!-- BEGIN: Actions -->
                                            <?php $__currentLoopData = $projects_notifications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $nt_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                            <?php
                                             $user_id = $nt_info->fk_created_by;


                                             $profile_path     = public_path().'/'.Config::get('constants.PROFILE_PICTURE_PATH') . $users_array[$user_id]->u_avatar_base_src . $users_array[$user_id]->u_avatar_filename . "." . $users_array[$user_id]->u_avatar_extentions;
                                             $profile_url = url('/').'/'.Config::get('constants.PROFILE_PICTURE_PATH') . $users_array[$user_id]->u_avatar_base_src . $users_array[$user_id]->u_avatar_filename . "." . $users_array[$user_id]->u_avatar_extentions;
                                             if(!is_file($profile_path))
                                             {
                                                 $profile_url = url('imgs/avatar.png');
                                             }
                                             $date = date('Y-m-d',strtotime($nt_info->nr_notification_date));
                                             $time = date('H:i:s',strtotime($nt_info->nr_notification_date));
                                            ?>
                                            <div class="mt-actions">
                                                <div class="mt-action">
                                                    <div class="mt-action-img">
                                                        <img style="height: 64px" src="<?php echo e($profile_url); ?>" /> </div>
                                                    <div class="mt-action-body">
                                                        <div class="mt-action-row">
                                                            <div class="mt-action-info ">
                                                                <div class="mt-action-icon ">
                                                                </div>
                                                                <div class="mt-action-details ">
                                                                    <span class="mt-action-author"><?php echo e($users_array[$user_id]->u_fullname); ?></span>
                                                                    <p class="mt-action-desc"><?php echo $nt_info->nr_notification_text; ?></p>
                                                                </div>
                                                            </div>
                                                            <div class="mt-action-datetime ">
                                                                <span class="mt-action-date"><?php echo $date; ?></span>
                                                                <span class="mt-action-dot bg-green"></span>
                                                                <span class="mt-action-time"><?php echo $time; ?></span>
                                                            </div>
                                                            <div class="mt-action-buttons ">
                                                                <?php if($nt_info->nr_notification_status == 1): ?>
                                                                    <span="green">Approved</span>
                                                                <?php elseif($nt_info->nr_notification_status == 2): ?>
                                                                    <span="red">Rejected</span>
                                                                <?php elseif($nt_info->nr_notification_status == 3): ?>
                                                                    <span="red">PostPone</span>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                            <!-- END: Actions -->


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
<label class="control-label">Projects<span class="required"> * </span></label>
<select data-type="projects" class="form-control selectpicker" name="fk_project_id" id="fk_project_id">
    <option value="" >Select</option>
    <?php $__currentLoopData = $lst_projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $project): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
    <?php if( in_array( $project->pp_id , $projects_array ) ): ?>
    <option value="<?php echo e($project->pp_id); ?>" ><?php echo e($project->pp_project_title); ?></option>
    <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
</select>
<script type="text/javascript">
    $(".selectpicker").selectpicker({
        liveSearch: "true"
    });
</script>
<?php
/***********************************************************
projects.blade.php
Product : 
Version : 1.0
Release : 2
Date Created : Mar 2, 2017
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2017

Page Description :
{Enter page description Here}
***********************************************************/

?>




<?php $__env->startSection('themes'); ?>
<link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css')); ?>" rel="stylesheet" type="text/css" />

<link href="<?php echo e(url('admin/assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')); ?>" rel="stylesheet" type="text/css" />
<style>
th{
    cursor: pointer;
}
a[id*=PROJECT]{
	display:none;
}
.heading th[nowrap]{
	display:none;
}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('plugins'); ?>

<script src="<?php echo e(url('admin/assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')); ?>" type="text/javascript"></script>

<script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/bootbox.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js')); ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo e(url('admin/assets/global/plugins/jquery.tablesorter/jquery.tablesorter.js')); ?>"></script>

<script type="text/javascript"  src="<?php echo e(url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')); ?>"></script>
<script type="text/javascript"  src="<?php echo e(url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js')); ?>"></script>
<script src="<?php echo e(url('admin/assets/pages/scripts/ecommerce-orders-view.js')); ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo e(url('js/modules/projects.module.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('js/projects/adminprojectsmanagement.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<form name="frm_projects_management" id="FRM_PROJECTS_MANAGEMENT">
    <div class="portlet blue box">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i> Projects Report </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
                <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                <a href="javascript:;" class="reload"> </a>
            </div>
        </div>
        <div class="portlet-body">
            <span id="hidden_fields">
                <input type="hidden" name="page_number" value="1" />
                <?php echo csrf_field(); ?>

            </span>
             <div class="row">
                <div class="col-md-12">&nbsp;</div>
            </div>
             <div class="row">
                <div class="col-md-12">&nbsp;</div>
            </div>
            <div class="row">
                 <div class="col-md-12">
                     <div style="left:20%" class="ListProjects">

                    </div>
                 </div>
             </div>
              <div class="row">
                 <div class="col-md-12" align="left">
                    <form name="quick_edit_form" id="QUICK_EDIT_FORM">
                    
                    </form>
                 </div>
            </div>
              <div class="row">
                 <div class="col-md-10" align="left">
                    <ul id="ProjectsPagination" class="pagination-sm"></ul>
                 </div>
                 <div class="col-md-2" align="right">
    
                 </div>
            </div>
     </div>
</div>

</form>

 <div id="ProjectManagement" class="modal fade" tabindex="-1"> </div>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.alayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
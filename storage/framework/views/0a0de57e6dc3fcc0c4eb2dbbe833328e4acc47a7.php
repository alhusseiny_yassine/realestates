<?php
/***********************************************************
addrequest.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 19, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
Add New Request Form
***********************************************************/

?>



rq_request_status
<?php $__env->startSection('themes'); ?>
    <link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css')); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo e(url('admin/assets/global/plugins/jquery-minicolors/jquery.minicolors.css')); ?>" rel="stylesheet" type="text/css" />
   <link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css')); ?>" rel="stylesheet" type="text/css" />
   <link href="<?php echo e(url('admin/assets/global/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" />
    <style>

    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('plugins'); ?>
    <script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript"  src="<?php echo e(url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')); ?>"></script>
    <script type="text/javascript"  src="<?php echo e(url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js')); ?>"></script>
    <script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('admin/assets/global/plugins/select2/js/select2.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo e(url('js/modules/requests.module.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('js/requests/saverequest.js')); ?>"></script>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<div class="portlet blue box" style="width:100%;">
<div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Create a New Training Request </div>
        <div class="tools">
        </div>
    </div>
    <div class="portlet-body">
        <form name="frm_save_request" id="FRM_SAVE_REQUEST">
            <span id="hidden_fields">
                  <?php echo csrf_field(); ?>

            </span>
            <div class="alert alert-success" style="display:none">
        				<strong>Success!</strong> Request Information is saved successfully!
        			</div>
        			<div class="alert alert-danger" style="display:none">
        				<strong>Error!</strong> You have some form errors. Please check below.
        			</div>
            <div class="row">
                 <div class="col-md-4" style="display:none">
                    <div class="form-group">
                        <label class="control-label">Training Title <span class="required"> * </span></label>
                        <input type="text" name="r_request_title" maxlength="150" id="R_REQUEST_TITLE" class="form-control" value="" />
                    </div>
                </div>
                 <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Training Date <span class="required"> * </span></label>
                        <input type="text" name="r_request_train_date" id="R_REQUEST_TRAIN_DATE" class="form-control date-picker" value="<?php echo e(date('Y-m-d')); ?>" />
                    </div>
                </div>
                 <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Trainee <span class="required"> * </span></label>
                        <select  name="rq_request_trainee" id="RQ_REQUEST_TRAINEE" class="form-control">
                            <option value="">--Select One--</option>
                            <?php $__currentLoopData = $trainees_array; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $value): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <option  value="<?php echo e($index); ?>"><?php echo e($value); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        </select>
                    </div>
                </div>
                 <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Status <span class="required"> * </span></label>
                        <select  name="rq_request_status" id="RQ_REQUEST_STATUS" required="required" class="form-control bs-select">
                            <?php $__currentLoopData = $lst_rq_statuses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $rq_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <option value="<?php echo e($rq_info->rs_id); ?>"><?php echo e($rq_info->rs_status_name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        </select>
                    </div>
                </div>
                 <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Courses <span class="required"> * </span></label><br/>

                        <select class="mt-multiselect btn btn-default" multiple="multiple" name="sl_courses[]" id="SL_COURSES" data-label="left" data-select-all="true" data-width="100%" data-filter="true" data-action-onchange="true">
                                      <?php $__currentLoopData = $lst_courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $course_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <option value="<?php echo e($course_info->c_id); ?>"><?php echo e($course_info->c_course_name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                    </select>
                    </div>
                </div>
                 <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Priority </label><br/>
                        <input type="number" name="r_request_order" class="form-control" min="1" max="5" step="1" value="" />
                    </div>
                </div>
                 <div class="col-md-8">
                    <div class="form-group">
                        <label class="control-label">Career Relevance</label>
                           <textarea style="width:100%;height:150px;" class="form-control" name="r_request_description"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" name="btn_save_request" id="BTN_SAVE_REQUEST" class="btn btn-info" >Create Request</button>&nbsp;&nbsp;<button type="button" id="BACK_FORM" name="back_form" class="btn default">Back</button>
                </div>
            </div>
        </form>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.alayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php
/***********************************************************
editproject.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 8, 2016
Developed By  : Mohamad Mantach   PHP Department
All Rights Reserved ,    Mohamad Mantach COPYRIGHT 2016

Page Description :
{Enter page description Here}
***********************************************************/

{

}


?>




<?php $__env->startSection('themes'); ?>
   <link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>" rel="stylesheet" type="text/css" />
   <link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')); ?>" rel="stylesheet" type="text/css" />
   <link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css')); ?>" rel="stylesheet" type="text/css" />
   <link href="<?php echo e(url('admin/assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
   <link href="<?php echo e(url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
   <link href="<?php echo e(url('admin/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')); ?>" rel="stylesheet" type="text/css" />
   <style>
       th{
	      cursor: pointer;
       }
       .container-lf-space{
	       padding-left:2px;
       	    padding-right:2px;
       }
       .portlet-body{
	       min-height:600px;
       }
   </style>
    <link href="<?php echo e(url('admin/assets/pages/css/search.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('plugins'); ?>
    <script src="<?php echo e(url('admin/assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('admin/assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')); ?>" type="text/javascript"></script>

    <script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-twbs-pagination/jquery.twbsPagination.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('admin/assets/global/plugins/bootbox.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo e(url('admin/assets/global/plugins/jquery.tablesorter/jquery.tablesorter.js')); ?>"></script>

    <script type="text/javascript"  src="<?php echo e(url('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')); ?>"></script>
    <script type="text/javascript"  src="<?php echo e(url('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js')); ?>"></script>
    <script src="<?php echo e(url('admin/assets/pages/scripts/ecommerce-orders-view.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('admin/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('admin/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo e(url('js/modules/projects.module.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('js/projects/asaveproject.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="portlet blue box">
<div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Edit Project <?php echo e($project_info->pp_project_code); ?> </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
        </div>
    </div>
    <div class="portlet-body">
        <form name="form_save_project" id="FORM_SAVE_PROJECT">
             <span id="hidden_fields">
                <?php echo csrf_field(); ?>

                <input type="hidden" name="project_status_id" id="PROJECT_STATUS_ID" value="<?php echo e(encrypt($project_info->fk_project_status_id)); ?>" />
                <input type="hidden" name="p_id" id="p_id" value="<?php echo e(encrypt($project_info->pp_id)); ?>" />
            </span>
                    <div class="alert alert-success" style="display:none">
        				<strong>Success!</strong> Project Information is saved successfully!
        			</div>
        			<div class="alert alert-danger" style="display:none">
        				<strong>Error!</strong> You have some form errors. Please check below.
        			</div>
                <div class="row">
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Project Code <span class="required"> * </span></label>
                            <input type="text" name="pp_project_code" maxlength="30" id="PP_PROJECT_CODE" class="form-control" value="<?php echo e($project_info->pp_project_code); ?>" />
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Project Name <span class="required"> * </span></label>
                            <input type="text" name="pp_project_title" maxlength="150" id="PP_PROJECT_TITLE" class="form-control" value="<?php echo e($project_info->pp_project_title); ?>" />
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Project Type <span class="required"> * </span></label>
                            <select name="sl_project_type" id="SL_PROJECT_TYPE" class="form-control bs-select">
                                <option value="">No Project Type</option>
                                <?php $__currentLoopData = $project_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pt_id => $pt_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <option <?php echo e($project_info->fk_ptype_id == $pt_info->pt_id ? "selected" : ""); ?> value="<?php echo e($pt_info->pt_id); ?>"><?php echo e($pt_info->pt_project_type); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Donors <span class="required"> * </span></label>
                            <select name="sl_donor" id="SL_DONOR" class="form-control bs-select">
                                <option value="">No Donor</option>
                                <?php $__currentLoopData = $lst_donors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $d_id => $d_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <option <?php echo e($project_info->fk_donor_id == $d_info->d_id ? "selected" : ""); ?> value="<?php echo e($d_info->d_id); ?>"><?php echo e($d_info->d_donor_name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label"> Project Billing Method <span class="required"> * </span></label>
                            <select name="pp_project_billing_method" id="PP_PROJECT_BILLING_METHOD" class="form-control bs-select">
                                <option <?php echo e($project_info->pp_project_billing_method == "" ? "selected" : ""); ?> value=""> No Billing Method </option>
                                <option <?php echo e($project_info->pp_project_billing_method == 1 ? "selected" : ""); ?> value="1">Lump Sum</option>
                                <option <?php echo e($project_info->pp_project_billing_method == 2 ? "selected" : ""); ?> value="2">Per Trainee</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Project Budget <span class="required"> * </span></label>
                            <input type="number" name="pp_project_budget" maxlength="20" id="PP_PROJECT_BUDGET" class="form-control" value="<?php echo e($project_info->pp_project_budget); ?>"  min="1" step="any" />
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Project Start Date <span class="required"> * </span></label>
                            <input type="text" name="pp_project_start_date" maxlength="10" id="PP_RPOJECT_START_DATE" class="form-control" value="<?php echo e($project_info->pp_project_start_date); ?>" />
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Project End Date <span class="required"> * </span></label>
                            <input type="text" name="pp_project_end_date" maxlength="150" id="PP_PROJECT_END_DATE" class="form-control" value="<?php echo e($project_info->pp_project_end_date); ?>" />
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Project Contract Date <span class="required"> * </span></label>
                            <input type="text" name="pp_contract_date" maxlength="150" id="PP_CONTRACT_DATE" class="form-control" value="<?php echo e($project_info->pp_contract_date); ?>" />
                        </div>
                    </div>
                     <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label"><b>Project Contract Number : </b></label>
                              <input type="text" name="pp_contract_number" maxlength="150" id="PP_CONTRACT_NUMBER" class="form-control" value="<?php echo e($project_info->pp_contract_number); ?>" />

                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Project Courses <span class="required"> * </span></label>
                            <select name="pc_project_courses[]" class="form-control" id="PC_PROJECT_COURSES" multiple="multiple" required="true" style="width:100%;height:200px;">
                                <?php $__currentLoopData = $lst_courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $course_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <option <?php echo e(in_array($course_info->c_id,$courses_array) ? "selected" : ""); ?> value="<?php echo e($course_info->c_id); ?>"><?php echo e($course_info->c_course_name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Project Beneficiaries</label>
                            <select name="pc_project_benficiaries[]" class="form-control mt-multiselect" id="PC_PROJECT_BENEFICIARIES" required="true" multiple="multiple" style="width:100%;height:200px;">
                                <?php $__currentLoopData = $lst_beneficiaries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $benf_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <option <?php echo e(in_array($benf_info->sb_id,$benf_array) ? "selected" : ""); ?> value="<?php echo e($benf_info->sb_id); ?>"><?php echo e($benf_info->sb_name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Project Providers <span class="required"> * </span></label>
                            <select name="pp_project_providers[]" class="form-control" id="PC_PROJECT_PROVIDERS" multiple="multiple" required="true" style="width:100%;height:200px;">
                                <?php $__currentLoopData = $lst_providers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $provider_info): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <option <?php echo e(in_array($provider_info->fk_provider_id,$providers_array) ? "selected" : ""); ?>  value="<?php echo e($provider_info->id); ?>"><?php echo e($provider_info->u_fullname); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label  class="control-label">Project Description </label>
                            <textarea name="pp_project_description"  id="PP_PROJECT_DESCRIPTION" class="form-control" style="width:100%;height:200px;resize:none" ><?php echo e($project_info->pp_project_description); ?></textarea>
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12" align="right" style="clear: both;">
                        <button type="submit" name="btn_save_project" id="BTN_SAVE_PROJECT" class="btn btn-info">Save</button>
                        <button type="button" id="BACK_FORM" name="back_form" class="btn default">Back</button>
                    </div>
                </div>

        </form>
    </div>
 </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.alayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
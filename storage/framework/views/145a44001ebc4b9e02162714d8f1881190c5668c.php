<?php
/***********************************************************
search_results.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Sep 30, 2016
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2016

Page Description :
Result of course search in the list of course in front end
***********************************************************/
 
?>
<div class="portlet-body flip-scroll" id="LISTCOURSE">
    <table   class="table table-striped table-bordered table-hover table-checkable order-column" style="width:100%">
        <thead class="bg-blue">
            <tr>
                <th  class="table-checkbox" style="width:2px;"><input type="checkbox" name="ck_all_courses" id="CK_ALL_COURSES" class="group-checkable" data-set="#LISTCOURSE .checkboxes" value="1" /></th>
                <th class="table-title">
                    <a href="javascript:;">Course Code</a>
                </th>
                <th class="table-title">
                    <a href="javascript:;">Course Name</a>
                </th>
                <th class="table-title">
                    <a href="javascript:;">Course Category</a>
                </th>
                <th class="table-title">
                    <a href="javascript:;">Total Hours</a>
                </th>

            </tr>
        </thead>
        <tbody>
        <?php 
            foreach ($courses as $c_index => $course_info) { 
                
         ?>
            <tr data-c_id="<?php echo e($course_info->c_id); ?>">
                <td><input type="checkbox" class="checkboxes" name="ck_course_<?php echo e($course_info->c_id); ?>" id="CK_COURSE_<?php echo e($course_info->c_id); ?>" value="<?php echo e($course_info->c_id); ?>" /></td>
                <td class="table-title">
                    <?php echo e($course_info->c_code); ?>

                </td>
                <td class="table-title">
                     <?php echo e($course_info->c_course_name); ?>

                </td>
                <td class="table-title">
                    <?php echo e(isset( $lstCourseCategories[ $course_info->fk_category_id ] ) ?  $lstCourseCategories[ $course_info->fk_category_id ]  : "N/A"); ?>

                </td>
                <td class="table-title">
                    <?php echo e($course_info->c_total_hours); ?>

                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>